﻿using Abp.Modules;
using MMB.ePay.Test.Base;

namespace MMB.ePay.Tests
{
    [DependsOn(typeof(ePayTestBaseModule))]
    public class ePayTestModule : AbpModule
    {
       
    }
}
