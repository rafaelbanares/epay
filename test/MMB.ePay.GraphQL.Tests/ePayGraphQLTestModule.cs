﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using MMB.ePay.Configure;
using MMB.ePay.Startup;
using MMB.ePay.Test.Base;

namespace MMB.ePay.GraphQL.Tests
{
    [DependsOn(
        typeof(ePayGraphQLModule),
        typeof(ePayTestBaseModule))]
    public class ePayGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayGraphQLTestModule).GetAssembly());
        }
    }
}