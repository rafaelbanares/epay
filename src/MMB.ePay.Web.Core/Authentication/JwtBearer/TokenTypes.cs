﻿namespace MMB.ePay.Web.Authentication.JwtBearer
{
    public enum TokenType
    {
        AccessToken,
        RefreshToken
    }
}
