﻿using System.Threading.Tasks;
using MMB.ePay.Sessions.Dto;

namespace MMB.ePay.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
