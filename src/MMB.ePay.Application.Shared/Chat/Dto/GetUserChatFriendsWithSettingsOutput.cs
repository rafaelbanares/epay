﻿using System;
using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;
using MMB.ePay.Friendships.Dto;

namespace MMB.ePay.Chat.Dto
{
    public class GetUserChatFriendsWithSettingsOutput
    {
        public DateTime ServerTime { get; set; }
        
        public List<FriendDto> Friends { get; set; }

        public GetUserChatFriendsWithSettingsOutput()
        {
            Friends = new EditableList<FriendDto>();
        }
    }
}