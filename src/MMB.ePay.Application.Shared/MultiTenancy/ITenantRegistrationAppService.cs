using System.Threading.Tasks;
using Abp.Application.Services;
using MMB.ePay.Editions.Dto;
using MMB.ePay.MultiTenancy.Dto;

namespace MMB.ePay.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}