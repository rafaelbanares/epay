﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace MMB.ePay.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
