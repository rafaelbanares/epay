﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MMB.ePay.MultiTenancy.Payments.PayPal.Dto;

namespace MMB.ePay.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}
