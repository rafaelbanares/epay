﻿namespace MMB.ePay.MultiTenancy.Dto
{
    public class PaymentInfoInput
    {
        public int? UpgradeEditionId { get; set; }
    }
}
