﻿namespace MMB.ePay.MultiTenancy.HostDashboard.Dto
{
    public enum ChartDateInterval
    {
        Daily = 1,
        Weekly = 2,
        Monthly = 3
    }
}