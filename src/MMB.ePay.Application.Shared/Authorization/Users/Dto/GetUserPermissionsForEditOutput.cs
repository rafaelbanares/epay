﻿using System.Collections.Generic;
using MMB.ePay.Authorization.Permissions.Dto;

namespace MMB.ePay.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}