﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Authorization.Permissions.Dto;

namespace MMB.ePay.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
