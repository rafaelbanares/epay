﻿using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}