﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MMB.ePay.Authorization.Accounts.Dto;

namespace MMB.ePay.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<int?> GetTenancyNameByUsernameOrEmailAddress(string userNameOrEmail);

        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<int?> ResolveTenantId(ResolveTenantIdInput input);

        Task<RegisterOutput> Register(RegisterInput input);

        Task SendPasswordResetCode(SendPasswordResetCodeInput input);

        Task<ResetPasswordOutput> ResetPassword(ResetPasswordInput input);

        Task SendEmailActivationLink(SendEmailActivationLinkInput input);

        Task ActivateEmail(ActivateEmailInput input);

        Task<ImpersonateOutput> Impersonate(ImpersonateInput input);

        Task<ImpersonateOutput> BackToImpersonator();

        Task<SwitchToLinkedAccountOutput> SwitchToLinkedAccount(SwitchToLinkedAccountInput input);
    }
}
