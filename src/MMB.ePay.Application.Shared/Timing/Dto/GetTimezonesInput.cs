﻿using Abp.Configuration;

namespace MMB.ePay.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
