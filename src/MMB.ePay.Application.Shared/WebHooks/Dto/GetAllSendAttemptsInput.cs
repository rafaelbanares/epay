﻿using MMB.ePay.Dto;

namespace MMB.ePay.WebHooks.Dto
{
    public class GetAllSendAttemptsInput : PagedInputDto
    {
        public string SubscriptionId { get; set; }
    }
}
