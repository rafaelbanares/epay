﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Persons.Dto;
using System.Threading.Tasks;

namespace MMB.ePay.Persons
{
    public interface IPersonAppService : IApplicationService
    {
        ListResultDto<PersonListDto> GetPeople(GetPeopleInput input);

        Task CreatePerson(CreatePersonInput input);
    }
}
