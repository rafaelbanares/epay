﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEmployeeLoansAppService : IApplicationService
    {
        Task<PagedResultDto<GetEmployeeLoansForViewDto>> GetAll(GetAllEmployeeLoansInput input);

        Task<GetEmployeeLoansForViewDto> GetEmployeeLoansForView(Guid id);

        Task<GetEmployeeLoansForEditOutput> GetEmployeeLoansForEdit(EntityDto<Guid> input);

        Task CreateOrEdit(CreateOrEditEmployeeLoansDto input);

        Task Delete(EntityDto<Guid> input);
    }
}