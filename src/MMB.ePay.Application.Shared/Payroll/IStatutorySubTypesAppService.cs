﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IStatutorySubTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetStatutorySubTypesForViewDto>> GetAll(GetAllStatutorySubTypesInput input);

        Task<GetStatutorySubTypesForViewDto> GetStatutorySubTypesForView(int id);

        Task<GetStatutorySubTypesForEditOutput> GetStatutorySubTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStatutorySubTypesDto input);

        Task Delete(EntityDto input);
    }
}