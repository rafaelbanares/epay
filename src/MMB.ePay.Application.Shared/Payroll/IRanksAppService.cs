﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IRanksAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetRankList();

        Task<PagedResultDto<GetRanksForViewDto>> GetAll(GetAllRanksInput input);

        Task<GetRanksForViewDto> GetRanksForView(int id);

        Task<GetRanksForEditOutput> GetRanksForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditRanksDto input);

        Task Delete(EntityDto input);
    }
}