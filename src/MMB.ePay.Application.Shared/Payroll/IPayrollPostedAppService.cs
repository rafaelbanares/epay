﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IPayrollPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetPayrollPostedForViewDto>> GetAll(GetAllPayrollPostedInput input);

        Task<GetPayrollPostedForViewDto> GetPayrollPostedForView(int id);

        Task<GetPayrollPostedForEditOutput> GetPayrollPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPayrollPostedDto input);

        Task Delete(EntityDto input);
    }
}