﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;

namespace MMB.ePay.Payroll
{
    public interface IRecurDeductionsAppService : IApplicationService
    {
        Task<PagedResultDto<GetRecurDeductionsForViewDto>> GetAll(GetAllRecurDeductionsInput input);

        Task<GetRecurDeductionsForViewDto> GetRecurDeductionsForView(int id);

        Task<GetRecurDeductionsForEditOutput> GetRecurDeductionsForCreate(int employeeId);

        Task<GetRecurDeductionsForEditOutput> GetRecurDeductionsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditRecurDeductionsDto input);

        Task Delete(EntityDto input);
    }
}