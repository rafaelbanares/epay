﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;

namespace MMB.ePay.Payroll
{
    public interface IRecurEarningsAppService : IApplicationService
    {
        Task<PagedResultDto<GetRecurEarningsForViewDto>> GetAll(GetAllRecurEarningsInput input);

        Task<GetRecurEarningsForViewDto> GetRecurEarningsForView(int id);

        Task<GetRecurEarningsForEditOutput> GetRecurEarningsForCreate(int employeeId);

        Task<GetRecurEarningsForEditOutput> GetRecurEarningsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditRecurEarningsDto input);

        Task Delete(EntityDto input);
    }
}