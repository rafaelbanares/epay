﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILoansAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetLoanList();

        Task<PagedResultDto<GetLoansForViewDto>> GetAll(GetAllLoansInput input);

        Task<GetLoansForViewDto> GetLoansForView(int id);

        Task<GetLoansForEditOutput> GetLoansForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLoansDto input);
    }
}