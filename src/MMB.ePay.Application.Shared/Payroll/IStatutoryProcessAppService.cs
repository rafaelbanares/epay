﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IStatutoryProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetStatutoryProcessForViewDto>> GetAll(GetAllStatutoryProcessInput input);

        Task<GetStatutoryProcessForViewDto> GetStatutoryProcessForView(int id);

        Task<GetStatutoryProcessForEditOutput> GetStatutoryProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStatutoryProcessDto input);

        Task Delete(EntityDto input);
    }
}