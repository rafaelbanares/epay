﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEarningPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetEarningPostedForViewDto>> GetAll(GetAllEarningPostedInput input);

        Task<GetEarningPostedForViewDto> GetEarningPostedForView(int id);

        Task<GetEarningPostedForEditOutput> GetEarningPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEarningPostedDto input);

        Task Delete(EntityDto input);
    }
}