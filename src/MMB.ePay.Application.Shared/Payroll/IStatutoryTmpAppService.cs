﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IStatutoryTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetStatutoryTmpForViewDto>> GetAll(GetAllStatutoryTmpInput input);

        Task<GetStatutoryTmpForViewDto> GetStatutoryTmpForView(int id);

        Task<GetStatutoryTmpForEditOutput> GetStatutoryTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStatutoryTmpDto input);

        Task Delete(EntityDto input);
    }
}