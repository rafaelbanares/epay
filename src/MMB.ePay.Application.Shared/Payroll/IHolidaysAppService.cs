﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IHolidaysAppService : IApplicationService
    {
        Task<PagedResultDto<GetHolidaysForViewDto>> GetAll(GetAllHolidaysInput input);

        Task<GetHolidaysForViewDto> GetHolidaysForView(int id);

        Task<GetHolidaysForEditOutput> GetHolidaysForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditHolidaysDto input);

        Task Delete(EntityDto input);
    }
}