﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IPayrollTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetPayrollTmpForViewDto>> GetAll(GetAllPayrollTmpInput input);

        Task<GetPayrollTmpForViewDto> GetPayrollTmpForView(int id);

        Task<GetPayrollTmpForEditOutput> GetPayrollTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPayrollTmpDto input);

        Task Delete(EntityDto input);
    }
}