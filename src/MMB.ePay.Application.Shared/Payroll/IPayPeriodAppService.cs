﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IPayPeriodAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetYearList();

        IList<SelectListItem> GetPayrollStatusList();

        IList<SelectListItem> GetPayrollYearList();

        Task<IList<SelectListItem>> GetPayPeriodListByYear(int year, int month);

        Task<IList<SelectListItem>> GetPayPeriodList();

        Task<IList<SelectListItem>> GetApplicableYearList();

        Task<GetPayPeriodForProcessDto> GetPayPeriodForProcess(GetPayPeriodForProcessInput input);
        
        Task<PagedResultDto<GetPayPeriodForViewDto>> GetAll(GetAllPayPeriodInput input);

        Task<GetPayPeriodForViewDto> GetPayPeriodForView(int id);

        Task<GetPayPeriodForEditOutput> GetPayPeriodForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPayPeriodDto input);
    }
}