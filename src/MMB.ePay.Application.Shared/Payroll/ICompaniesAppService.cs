﻿using Abp.Application.Services;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ICompaniesAppService : IApplicationService
    {
        //Task<PagedResultDto<GetCompaniesForViewDto>> GetAll(GetAllCompaniesInput input);

        //Task<GetCompaniesForViewDto> GetCompaniesForView(int id);

        //Task<GetCompaniesForEditOutput> GetCompaniesForEdit(EntityDto input);
        Task<GetCompaniesForEditOutput> GetCompaniesForEdit();

        Task CreateOrEdit(CreateOrEditCompaniesDto input);

        //Task Delete(EntityDto input);
    }
}