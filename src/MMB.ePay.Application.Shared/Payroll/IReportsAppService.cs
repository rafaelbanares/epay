﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IReportsAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetReportList(string reportTypeId);
        Task<string> GetReportName(int id);
        Task<GetReportsForViewDto> GetReportsForView(int id);
        
        //Periodic
        Task<GetReportsForBankAdvise> GetPeriodicBankAdvise(ReportsDto dto);
        Task<GetReportsForCashAdvise> GetPeriodicCashAdvise(ReportsDto dto);
        Task<GetReportsForChequeAdvise> GetPeriodicChequeAdvise(ReportsDto dto);
        Task<GetReportsForDeductionRegister> GetPeriodicDeductionRegister(ReportsDto dto);
        Task<GetReportsForEarningRegister> GetPeriodicEarningRegister(ReportsDto dto);
        Task<GetReportsForEmployeePayslip> GetPeriodicEmployeePayslip(ReportsDto dto);
        Task<GetReportsForGrossAndTax> GetPeriodicGrossAndTax(ReportsDto dto);
        Task<GetReportsForPagibigContribution> GetPeriodicPagibigContribution(ReportsDto dto);
        Task<GetReportsForPayrollRegister> GetPeriodicPayrollRegister(ReportsDto dto);
        Task<GetReportsForPhilhealthContribution> GetPeriodicPhilhealthContribution(ReportsDto dto);
        Task<GetReportsForSssContribution> GetPeriodicSssContribution(ReportsDto dto);
        Task<GetReportsForTimesheetRegister> GetPeriodicTimesheetRegister(ReportsDto dto);

        //Monthly
        Task<GetReportsForDeductionRegister> GetMonthlyDeductionRegister(ReportsDto dto);
        Task<GetReportsForEarningRegister> GetMonthlyEarningRegister(ReportsDto dto);
        Task<GetReportsForGrossAndTax> GetMonthlyGrossAndTax(ReportsDto dto);
        Task<GetReportsForLoanDetailRegister> GetMonthlyLoanDetailRegister(ReportsDto dto);
        Task<GetReportsForOvertimeRegister> GetMonthlyOvertimeRegister(ReportsDto dto);
        Task<GetReportsForPagibigM11> GetMonthlyPagibigM11(ReportsDto dto);
        Task<GetReportsForPagibigMRCRF> GetMonthlyPagibigMRCRF(ReportsDto dto);
        Task<GetReportsForPayrollRegister> GetMonthlyPayrollRegister(ReportsDto dto);
        Task<GetReportsForPhilhealthER2> GetMonthlyPhilhealthER2(ReportsDto dto);
        Task<GetReportsForPhilhealthRF1> GetMonthlyPhilhealthRF1(ReportsDto dto);
        Task<GetReportsForPhilhealthContribution> GetMonthlyPhilhealthContribution(ReportsDto dto);
        Task<GetReportsForSssContribution> GetMonthlySssContribution(ReportsDto dto);
        Task<GetReportsForSSSML2> GetMonthlySSSML2(ReportsDto dto);

        //Quarterly
        Task<GetReportsForSSSR3> GetQuarterlySSSR3(ReportsDto dto);

        //Annual
        //Task<GetReportsForAlphalist> GetAnnualAlphalist(ReportsDto dto);
        Task<GetReportsForAlphalistNonMWE1> GetAnnualAlphalistNonMWE1(ReportsDto dto);
        Task<GetReportsForAlphalistNonMWE2> GetAnnualAlphalistNonMWE2(ReportsDto dto);
        Task<GetReportsForAlphalistNonMWE3> GetAnnualAlphalistNonMWE3(ReportsDto dto);
        Task<GetReportsForAlphalistMWE1> GetAnnualAlphalistMWE1(ReportsDto dto);
        Task<GetReportsForAlphalistMWE2> GetAnnualAlphalistMWE2(ReportsDto dto);
        Task<GetReportsForAlphalistMWE3> GetAnnualAlphalistMWE3(ReportsDto dto);
        Task<GetReportsForBIR2316> GetAnnualBIR2316(ReportsDto dto);
    }
}
