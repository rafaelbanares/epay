﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IPayCodesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetPayCodeList();

        Task<PagedResultDto<GetPayCodesForViewDto>> GetAll(GetAllPayCodesInput input);

        Task<GetPayCodesForViewDto> GetPayCodesForView(Guid id);

        Task<GetPayCodesForEditOutput> GetPayCodesForEdit(EntityDto<Guid> input);

        Task CreateOrEdit(CreateOrEditPayCodesDto input);
    }
}