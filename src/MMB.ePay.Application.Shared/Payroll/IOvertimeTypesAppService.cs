﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IOvertimeTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetOvertimeTypesListFilter();

        Task<IList<SelectListItem>> GetOvertimeTypesList();

        Task<GetOvertimeTypesForViewDto> GetAll();

        Task Update(IList<decimal> input);
    }
}