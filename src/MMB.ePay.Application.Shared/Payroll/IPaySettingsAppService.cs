﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IPaySettingsAppService : IApplicationService
    {
        Task<PagedResultDto<GetPaySettingsForViewDto>> GetAll(GetAllPaySettingsInput input);

        Task<GetPaySettingsForViewDto> GetPaySettingsForView(int id);

        Task<GetPaySettingsForEditOutput> GetPaySettingsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPaySettingsDto input);

        Task Delete(EntityDto input);
    }
}