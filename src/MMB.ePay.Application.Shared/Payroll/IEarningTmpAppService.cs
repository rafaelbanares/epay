﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEarningTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetEarningTmpForViewDto>> GetAll(GetAllEarningTmpInput input);

        Task<GetEarningTmpForViewDto> GetEarningTmpForView(int id);

        Task<GetEarningTmpForEditOutput> GetEarningTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEarningTmpDto input);

        Task Delete(EntityDto input);
    }
}