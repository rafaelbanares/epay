﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IDependentsAppService : IApplicationService
    {
        Task<PagedResultDto<GetDependentsForViewDto>> GetAll(GetAllDependentsInput input);

        Task<GetDependentsForViewDto> GetDependentsForView(int id);

        Task<GetDependentsForEditOutput> GetDependentsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDependentsDto input);

        Task Delete(EntityDto input);
    }
}