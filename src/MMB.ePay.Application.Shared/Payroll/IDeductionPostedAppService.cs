﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IDeductionPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetDeductionPostedForViewDto>> GetAll(GetAllDeductionPostedInput input);

        Task<GetDeductionPostedForViewDto> GetDeductionPostedForView(int id);

        Task<GetDeductionPostedForEditOutput> GetDeductionPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeductionPostedDto input);

        Task Delete(EntityDto input);
    }
}