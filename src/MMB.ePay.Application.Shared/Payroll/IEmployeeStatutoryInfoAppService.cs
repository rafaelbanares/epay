﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEmployeeStatutoryInfoAppService : IApplicationService
    {
        //Task<GetEmployeeStatutoryInfoForEditOutput> GetEmployeeStatutoryInfoForEdit(EntityDto input);
        Task CreateOrEdit(GetEmployeeStatutoryInfoForEditOutput input);
        Task<IList<string>> GetEmployeeStatutoryInfoForEdit(EntityDto input);
    }
}