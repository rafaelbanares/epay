﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IOvertimeSubTypesAppService : IApplicationService
    {
        Task<IList<string>> GetOvertimeSubTypesMenu();

        Task<PagedResultDto<GetOvertimeSubTypesForViewDto>> GetAll(GetAllOvertimeSubTypesInput input);

        Task<GetOvertimeSubTypesForViewDto> GetOvertimeSubTypesForView(int id);

        Task<GetOvertimeSubTypesForEditOutput> GetOvertimeSubTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOvertimeSubTypesDto input);

        Task Delete(EntityDto input);
    }
}