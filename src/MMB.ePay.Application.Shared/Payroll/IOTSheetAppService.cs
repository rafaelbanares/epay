﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;
using System.Collections.Generic;

namespace MMB.ePay.Payroll
{
    public interface IOTSheetAppService : IApplicationService
    {
        Task<IList<decimal>> GetAll(GetAllOTSheetInput input);

        Task CreateOrEdit(CreateOrEditOTSheetDto input);
    }
}