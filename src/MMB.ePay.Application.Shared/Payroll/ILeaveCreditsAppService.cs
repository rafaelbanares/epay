﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILeaveCreditsAppService : IApplicationService
    {
        Task<PagedResultDto<GetLeaveCreditsForViewDto>> GetAll(GetAllLeaveCreditsInput input);

        Task<GetLeaveCreditsForViewDto> GetLeaveCreditsForView(int id);

        Task<GetLeaveCreditsForEditOutput> GetLeaveCreditsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeaveCreditsDto input);

        Task Delete(EntityDto input);
    }
}