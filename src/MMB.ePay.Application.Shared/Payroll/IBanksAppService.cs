﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IBanksAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetBankList();

        IList<SelectListItem> GetBankAccountTypeList();

        Task<PagedResultDto<GetBanksForViewDto>> GetAll(GetAllBanksInput input);

        Task<GetBanksForViewDto> GetBanksForView(string id);

        Task<GetBanksForEditOutput> GetBanksForEdit(EntityDto<string> input);

        Task CreateOrEdit(CreateOrEditBanksDto input);
    }
}