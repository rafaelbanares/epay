﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEmployersAppService : IApplicationService
    {
        Task<PagedResultDto<GetEmployersForViewDto>> GetAll(GetAllEmployersInput input);

        Task<GetEmployersForViewDto> GetEmployersForView(int id);

        Task<GetEmployersForEditOutput> GetEmployersForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmployersDto input);

        Task Delete(EntityDto input);
    }
}