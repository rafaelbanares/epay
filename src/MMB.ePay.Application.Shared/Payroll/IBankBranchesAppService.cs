﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IBankBranchesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetBankBranchList(string bankId);

        Task<IList<SelectListItem>> GetBranchList(string bankId);

        Task<PagedResultDto<GetBankBranchesForViewDto>> GetAll(GetAllBankBranchesInput input);

        Task<GetBankBranchesForViewDto> GetBankBranchesForView(int id);

        Task<GetBankBranchesForEditOutput> GetBankBranchesForCreate(string bankId);

        Task<GetBankBranchesForEditOutput> GetBankBranchesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBankBranchesDto input);
    }
}