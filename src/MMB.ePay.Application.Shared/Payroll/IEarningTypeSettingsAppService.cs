﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEarningTypeSettingsAppService : IApplicationService
    {
        Task<PagedResultDto<GetEarningTypeSettingsForViewDto>> GetAll(GetAllEarningTypeSettingsInput input);

        Task<GetEarningTypeSettingsForViewDto> GetEarningTypeSettingsForView(int id);

        Task<GetEarningTypeSettingsForEditOutput> GetEarningTypeSettingsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEarningTypeSettingsDto input);

        Task Delete(EntityDto input);
    }
}