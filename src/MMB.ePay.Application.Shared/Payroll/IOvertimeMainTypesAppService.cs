﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IOvertimeMainTypesAppService : IApplicationService
    {
        Task<IList<Tuple<string, string>>> GetOvertimeMainTypesMenu();

        Task<PagedResultDto<GetOvertimeMainTypesForViewDto>> GetAll(GetAllOvertimeMainTypesInput input);

        Task<GetOvertimeMainTypesForViewDto> GetOvertimeMainTypesForView(int id);

        Task<GetOvertimeMainTypesForEditOutput> GetOvertimeMainTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOvertimeMainTypesDto input);

        Task Delete(EntityDto input);
    }
}