﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ITimesheetTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetTimesheetTypesListFilter();

        Task<IList<SelectListItem>> GetTimesheetTypesList();

        Task<IList<string>> GetTimesheetTypesMenu();

        Task<PagedResultDto<GetTimesheetTypesForViewDto>> GetAll(GetAllTimesheetTypesInput input);

        Task<GetTimesheetTypesForViewDto> GetTimesheetTypesForView(int id);

        Task<GetTimesheetTypesForEditOutput> GetTimesheetTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTimesheetTypesDto input);
    }
}