﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IStatutoryTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetStatutoryTypeList();

        Task<IList<string>> GetStatutoryTypeCodeList();

        Task<PagedResultDto<GetStatutoryTypesForViewDto>> GetAll(GetAllStatutoryTypesInput input);

        Task<GetStatutoryTypesForViewDto> GetStatutoryTypesForView(int id);

        Task<GetStatutoryTypesForEditOutput> GetStatutoryTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStatutoryTypesDto input);

        Task Delete(EntityDto input);
    }
}