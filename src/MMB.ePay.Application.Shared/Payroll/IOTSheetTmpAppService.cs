﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IOTSheetTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetOTSheetTmpForViewDto>> GetAll(GetAllOTSheetTmpInput input);

        Task<GetOTSheetTmpForViewDto> GetOTSheetTmpForView(int id);

        Task<GetOTSheetTmpForEditOutput> GetOTSheetTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOTSheetTmpDto input);

        Task Delete(EntityDto input);
    }
}