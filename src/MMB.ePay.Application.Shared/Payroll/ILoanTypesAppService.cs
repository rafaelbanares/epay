﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILoanTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetLoanTypeList();

        Task<PagedResultDto<GetLoanTypesForViewDto>> GetAll(GetAllLoanTypesInput input);

        Task<GetLoanTypesForViewDto> GetLoanTypesForView(int id);

        Task<GetLoanTypesForEditOutput> GetLoanTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLoanTypesDto input);
    }
}