﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IDeductionTypesAppService : IApplicationService
    {
        //Task<IList<SelectListItem>> GetDeductionTypeList();

        Task<IList<SelectListItem>> GetDeductionTypeListForDeductions(int? deductionId, int? employeeId);

        Task<IList<SelectListItem>> GetDeductionTypeListForRecurDeductions(int? recurDeductionId, int? employeeId);

        Task<PagedResultDto<GetDeductionTypesForViewDto>> GetAll(GetAllDeductionTypesInput input);

        Task<GetDeductionTypesForViewDto> GetDeductionTypesForView(int id);

        Task<GetDeductionTypesForEditOutput> GetDeductionTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeductionTypesDto input);
    }
}