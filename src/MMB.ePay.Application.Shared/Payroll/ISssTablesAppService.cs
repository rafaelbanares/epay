﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ISssTablesAppService : IApplicationService
    {
        Task<PagedResultDto<GetSssTablesForViewDto>> GetAll(GetAllSssTablesInput input);

        Task<GetSssTablesForViewDto> GetSssTablesForView(int id);

        Task<GetSssTablesForEditOutput> GetSssTablesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSssTablesDto input);

        Task Delete(EntityDto input);
    }
}