﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ITimesheetPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetTimesheetPostedForViewDto>> GetAll(GetAllTimesheetPostedInput input);

        Task<GetTimesheetPostedForViewDto> GetTimesheetPostedForView(int id);

        Task<GetTimesheetPostedForEditOutput> GetTimesheetPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTimesheetPostedDto input);

        Task Delete(EntityDto input);
    }
}