﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;

namespace MMB.ePay.Payroll
{
    public interface IContactPersonsAppService : IApplicationService
    {
        Task<PagedResultDto<GetContactPersonsForViewDto>> GetAll(GetAllContactPersonsInput input);

        Task<GetContactPersonsForViewDto> GetContactPersonsForView(int id);

        Task<GetContactPersonsForEditOutput> GetContactPersonsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditContactPersonsDto input);
    }
}