﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEarningTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetEarningTypesList();

        Task<IList<SelectListItem>> GetEarningTypeListForEarnings(int? earningId, int? employeeId);

        Task<IList<SelectListItem>> GetEarningTypeListForRecurEarnings(int? recurEarningId, int? employeeId);

        Task<PagedResultDto<GetEarningTypesForViewDto>> GetAll(GetAllEarningTypesInput input);

        Task<GetEarningTypesForViewDto> GetEarningTypesForView(int id);

        Task<GetEarningTypesForEditOutput> GetEarningTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEarningTypesDto input);
    }
}