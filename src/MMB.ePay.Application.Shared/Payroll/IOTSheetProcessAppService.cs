﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IOTSheetProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetOTSheetProcessForViewDto>> GetAll(GetAllOTSheetProcessInput input);

        Task<GetOTSheetProcessForViewDto> GetOTSheetProcessForView(int id);

        Task<GetOTSheetProcessForEditOutput> GetOTSheetProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOTSheetProcessDto input);

        Task Delete(EntityDto input);
    }
}