﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IOTSheetPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetOTSheetPostedForViewDto>> GetAll(GetAllOTSheetPostedInput input);

        Task<GetOTSheetPostedForViewDto> GetOTSheetPostedForView(int id);

        Task<GetOTSheetPostedForEditOutput> GetOTSheetPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditOTSheetPostedDto input);

        Task Delete(EntityDto input);
    }
}