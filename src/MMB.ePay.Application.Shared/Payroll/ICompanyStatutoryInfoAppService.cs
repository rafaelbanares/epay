﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ICompanyStatutoryInfoAppService : IApplicationService
    {
        Task<PagedResultDto<GetCompanyStatutoryInfoForViewDto>> GetAll(GetAllCompanyStatutoryInfoInput input);

        Task<GetCompanyStatutoryInfoForViewDto> GetCompanyStatutoryInfoForView(string id);

        Task<GetCompanyStatutoryInfoForEditOutput> GetCompanyStatutoryInfoForEdit(string id);

        Task CreateOrEdit(CreateOrEditCompanyStatutoryInfoDto input);
    }
}