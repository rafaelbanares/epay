﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILeaveTypesAppService : IApplicationService
    {
        Task<PagedResultDto<GetLeaveTypesForViewDto>> GetAll(GetAllLeaveTypesInput input);

        Task<GetLeaveTypesForViewDto> GetLeaveTypesForView(int id);

        Task<GetLeaveTypesForEditOutput> GetLeaveTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeaveTypesDto input);

        Task Delete(EntityDto input);
    }
}