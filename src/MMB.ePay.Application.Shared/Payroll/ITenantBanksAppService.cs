﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;

namespace MMB.ePay.Payroll
{
    public interface ITenantBanksAppService : IApplicationService
    {
        Task<PagedResultDto<GetTenantBanksForViewDto>> GetAll(GetAllTenantBanksInput input);

        Task<GetTenantBanksForViewDto> GetTenantBanksForView(int id);

        Task<GetTenantBanksForEditOutput> GetTenantBanksForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTenantBanksDto input);
    }
}