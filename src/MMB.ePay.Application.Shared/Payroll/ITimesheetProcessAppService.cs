﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ITimesheetProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetTimesheetProcessForViewDto>> GetAll(GetAllTimesheetProcessInput input);

        Task<GetTimesheetProcessForViewDto> GetTimesheetProcessForView(int id);

        Task<GetTimesheetProcessForEditOutput> GetTimesheetProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTimesheetProcessDto input);

        Task Delete(EntityDto input);
    }
}