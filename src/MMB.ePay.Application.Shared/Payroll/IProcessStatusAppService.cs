﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IProcessStatusAppService : IApplicationService
    {
        Task<PagedResultDto<GetProcessStatusForViewDto>> GetAll(GetAllProcessStatusInput input);

        Task<GetProcessStatusForViewDto> GetProcessStatusForView(int id);

        Task<GetProcessStatusForEditOutput> GetProcessStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProcessStatusDto input);

        Task Delete(EntityDto input);
    }
}