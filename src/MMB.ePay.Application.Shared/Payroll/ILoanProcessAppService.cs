﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILoanProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetLoanProcessForViewDto>> GetAll(GetAllLoanProcessInput input);

        Task<GetLoanProcessForViewDto> GetLoanProcessForView(int id);

        Task<GetLoanProcessForEditOutput> GetLoanProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLoanProcessDto input);

        Task Delete(EntityDto input);
    }
}