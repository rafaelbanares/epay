﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILoanPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetLoanPostedForViewDto>> GetAll(GetAllLoanPostedInput input);

        Task<GetLoanPostedForViewDto> GetLoanPostedForView(int id);

        Task<GetLoanPostedForEditOutput> GetLoanPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLoanPostedDto input);

        Task Delete(EntityDto input);
    }
}