﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;

namespace MMB.ePay.Payroll
{
    public interface IDeductionsAppService : IApplicationService
    {
        Task<PagedResultDto<GetDeductionsForViewDto>> GetAll(GetAllDeductionsInput input);

        Task<GetDeductionsForViewDto> GetDeductionsForView(int id);

        Task<GetDeductionsForEditOutput> GetDeductionsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeductionsDto input);

        Task Delete(EntityDto input);
    }
}