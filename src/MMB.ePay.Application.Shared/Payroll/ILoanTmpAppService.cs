﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ILoanTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetLoanTmpForViewDto>> GetAll(GetAllLoanTmpInput input);

        Task<GetLoanTmpForViewDto> GetLoanTmpForView(int id);

        Task<GetLoanTmpForEditOutput> GetLoanTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLoanTmpDto input);

        Task Delete(EntityDto input);
    }
}