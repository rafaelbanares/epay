﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MMB.ePay.Payroll
{
    public interface IHelperAppService : IApplicationService
    {
        IList<SelectListItem> GetMonthList();

        IList<SelectListItem> GetQuarterList();

        IList<SelectListItem> GetPaymentTypeList();

        IList<SelectListItem> GetFrequencyList();

        IList<SelectListItem> GetTaxFrequencyList();

        IList<SelectListItem> GetTaxMethodList();

        string GetFrequencyByCode(string code);

        string GetTaxFrequencyByCode(string code);

        string GetTaxMethodByCode(string code);
    }
}