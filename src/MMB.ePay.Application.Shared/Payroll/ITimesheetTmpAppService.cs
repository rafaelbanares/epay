﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ITimesheetTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetTimesheetTmpForViewDto>> GetAll(GetAllTimesheetTmpInput input);

        Task<GetTimesheetTmpForViewDto> GetTimesheetTmpForView(int id);

        Task<GetTimesheetTmpForEditOutput> GetTimesheetTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTimesheetTmpDto input);

        Task Delete(EntityDto input);
    }
}