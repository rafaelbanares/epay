﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEmployeePayrollAppService : IApplicationService
    {
        Task<string> GetPayFreqByEmployeeId(int employeeId);

        Task<PagedResultDto<GetEmployeePayrollForViewDto>> GetAll(GetAllEmployeePayrollInput input);

        Task<GetEmployeePayrollForViewDto> GetEmployeePayrollForView(int id);

        Task<GetEmployeePayrollForEditOutput> GetEmployeePayrollForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmployeePayrollDto input);

        Task Delete(EntityDto input);
    }
}