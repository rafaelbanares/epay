﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IDeductionProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetDeductionProcessForViewDto>> GetAll(GetAllDeductionProcessInput input);

        Task<GetDeductionProcessForViewDto> GetDeductionProcessForView(int id);

        Task<GetDeductionProcessForEditOutput> GetDeductionProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeductionProcessDto input);

        Task Delete(EntityDto input);
    }
}