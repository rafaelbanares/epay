﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IReviewersAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetReviewerTypeList();

        Task<PagedResultDto<GetReviewersForViewDto>> GetAll(GetAllReviewersInput input);

        Task<GetReviewersForViewDto> GetReviewersForView(int id);

        Task<GetReviewersForEditOutput> GetReviewersForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditReviewersDto input);
    }
}