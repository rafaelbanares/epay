﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IEarningProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetEarningProcessForViewDto>> GetAll(GetAllEarningProcessInput input);

        Task<GetEarningProcessForViewDto> GetEarningProcessForView(int id);

        Task<GetEarningProcessForEditOutput> GetEarningProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEarningProcessDto input);

        Task Delete(EntityDto input);
    }
}