﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IProcessAppService : IApplicationService
    {
        IList<SelectListItem> GetProcessTypeList();

        Task<GetProcessForProcessingViewDto> GetProcessForProcessingView(int id);

        Task<GetProcessForPayrollEntryStepDto> GetPayrollEntryStep(int id);

        Task<GetProcessForProcessingStepDto> GetProcessingStep(int id);

        Task<GetProcessForReviewStepDto> GetReviewStep(int id);

        Task<GetProcessForPostingStepDto> GetPostingStep(int id);

        Task UpdateProcessToNextStatus(int id);

        Task<GetPayrollProcessForViewDto> GetProcesses();

        Task<PagedResultDto<GetProcessForViewDto>> GetAll(GetAllProcessInput input);

        Task<GetProcessForViewDto> GetProcessForView(int id);

        Task<GetProcessForCreateOutput> GetProcessForCreate();

        Task<GetProcessForEditOutput> GetProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProcessDto input);

        Task Delete(EntityDto input);
    }
}