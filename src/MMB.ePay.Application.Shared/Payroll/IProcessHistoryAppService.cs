﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IProcessHistoryAppService : IApplicationService
    {
        Task<PagedResultDto<GetProcessHistoryForViewDto>> GetAll(GetAllProcessHistoryInput input);

        Task<GetProcessHistoryForViewDto> GetProcessHistoryForView(int id);

        Task<GetProcessHistoryForEditOutput> GetProcessHistoryForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProcessHistoryDto input);

        Task Delete(EntityDto input);
    }
}