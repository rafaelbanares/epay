﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ITaxTablesAppService : IApplicationService
    {
        IList<SelectListItem> GetYearList();

        IList<SelectListItem> GetCodeList();

        Task<PagedResultDto<GetTaxTablesForViewDto>> GetAll(GetAllTaxTablesInput input);

        Task<GetTaxTablesForViewDto> GetTaxTablesForView(int id);

        Task<GetTaxTablesForEditOutput> GetTaxTablesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTaxTablesDto input);
    }
}