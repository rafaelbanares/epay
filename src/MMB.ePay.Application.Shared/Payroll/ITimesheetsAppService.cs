﻿using Abp.Application.Services;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ITimesheetsAppService : IApplicationService
    {
        Task<GetTimesheetsForViewDto> GetAll(GetAllTimesheetsInput input);
        //Task<PagedResultDto<GetTimesheetsForViewDto>> GetAll(GetAllTimesheetsInput input);

        Task CreateOrEdit(CreateOrEditTimesheetsDto input);
    }
}