﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Dto;

namespace MMB.ePay.Payroll
{
    public interface IEarningsAppService : IApplicationService
    {
        Task<PagedResultDto<GetEarningsForViewDto>> GetAll(GetAllEarningsInput input);

        Task<GetEarningsForViewDto> GetEarningsForView(int id);

        Task<GetEarningsForEditOutput> GetEarningsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEarningsDto input);

        Task Delete(EntityDto input);
    }
}