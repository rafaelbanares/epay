﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IPayrollProcessAppService : IApplicationService
    {
        Task<PagedResultDto<GetPayrollProcessForViewDto>> GetAll(GetAllPayrollProcessInput input);

        Task<GetPayrollProcessForViewDto> GetPayrollProcessForView(int id);

        Task<GetPayrollProcessForEditOutput> GetPayrollProcessForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPayrollProcessDto input);

        Task Delete(EntityDto input);
    }
}