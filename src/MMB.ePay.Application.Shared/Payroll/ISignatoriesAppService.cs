﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface ISignatoriesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetSignatoryList();
    }
}