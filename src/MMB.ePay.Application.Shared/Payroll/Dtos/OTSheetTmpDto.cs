﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class OTSheetTmpDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public string OvertimeType { get; set; }

        public int? CostCenter { get; set; }

        public decimal Hrs { get; set; }

        public int Mins { get; set; }

        public int EmployeeId { get; set; }

        public Guid SessionId { get; set; }

    }
}