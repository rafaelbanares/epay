﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllPayrollTmpInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public decimal? MaxSalaryFilter { get; set; }
        public decimal? MinSalaryFilter { get; set; }

        public decimal? MaxDailyRateFilter { get; set; }
        public decimal? MinDailyRateFilter { get; set; }

        public decimal? MaxHourlyRateFilter { get; set; }
        public decimal? MinHourlyRateFilter { get; set; }

        public decimal? MaxTimesheetFilter { get; set; }
        public decimal? MinTimesheetFilter { get; set; }

        public decimal? MaxOvertimeFilter { get; set; }
        public decimal? MinOvertimeFilter { get; set; }

        public decimal? MaxTxEarningAmtFilter { get; set; }
        public decimal? MinTxEarningAmtFilter { get; set; }

        public decimal? MaxNtxEarningAmtFilter { get; set; }
        public decimal? MinNtxEarningAmtFilter { get; set; }

        public decimal? MaxDedTaxAmtFilter { get; set; }
        public decimal? MinDedTaxAmtFilter { get; set; }

        public decimal? MaxDeductionAmtFilter { get; set; }
        public decimal? MinDeductionAmtFilter { get; set; }

        public decimal? MaxLoanAmtFilter { get; set; }
        public decimal? MinLoanAmtFilter { get; set; }

        public decimal? MaxBasicPayFilter { get; set; }
        public decimal? MinBasicPayFilter { get; set; }

        public decimal? MaxGrossPayFilter { get; set; }
        public decimal? MinGrossPayFilter { get; set; }

        public decimal? MaxNetPayFilter { get; set; }
        public decimal? MinNetPayFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxPayCodeIdFilter { get; set; }
        public int? MinPayCodeIdFilter { get; set; }

        public int? MaxBankBranchIdFilter { get; set; }
        public int? MinBankBranchIdFilter { get; set; }

        public Guid? SessionIdFilter { get; set; }

    }
}