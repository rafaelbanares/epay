﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllTaxTablesForExcelInput
    {
        public string Filter { get; set; }

        public int? MaxYearFilter { get; set; }
        public int? MinYearFilter { get; set; }

        public string PayFreqFilter { get; set; }

        public string CodeFilter { get; set; }

        public decimal? MaxBracket1Filter { get; set; }
        public decimal? MinBracket1Filter { get; set; }

        public decimal? MaxBracket2Filter { get; set; }
        public decimal? MinBracket2Filter { get; set; }

        public decimal? MaxBracket3Filter { get; set; }
        public decimal? MinBracket3Filter { get; set; }

        public decimal? MaxBracket4Filter { get; set; }
        public decimal? MinBracket4Filter { get; set; }

        public decimal? MaxBracket5Filter { get; set; }
        public decimal? MinBracket5Filter { get; set; }

        public decimal? MaxBracket6Filter { get; set; }
        public decimal? MinBracket6Filter { get; set; }

        public decimal? MaxBracket7Filter { get; set; }
        public decimal? MinBracket7Filter { get; set; }

    }
}