﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetProcessStatusForViewDto
    {
        public ProcessStatusDto ProcessStatus { get; set; }

    }
}