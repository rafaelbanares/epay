﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetRecurDeductionsForViewDto
    {
        public RecurDeductionsDto RecurDeductions { get; set; }

    }
}