﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllOTSheetInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeIdFilter { get; set; }
    }
}