﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEmployersForExcelInput
    {
        public string Filter { get; set; }

        public string EmployerNameFilter { get; set; }

        public DateTime? MaxEmployedFromFilter { get; set; }
        public DateTime? MinEmployedFromFilter { get; set; }

        public DateTime? MaxEmployedToFilter { get; set; }
        public DateTime? MinEmployedToFilter { get; set; }

        public string TINFilter { get; set; }

        public string Address1Filter { get; set; }

        public string Address2Filter { get; set; }

        public decimal? MaxBasicPayFilter { get; set; }
        public decimal? MinBasicPayFilter { get; set; }

        public decimal? MaxDeminimisFilter { get; set; }
        public decimal? MinDeminimisFilter { get; set; }

        public decimal? MaxGrossPayFilter { get; set; }
        public decimal? MinGrossPayFilter { get; set; }

        public decimal? MaxTxBonusFilter { get; set; }
        public decimal? MinTxBonusFilter { get; set; }

        public decimal? MaxTxOtherIncomeFilter { get; set; }
        public decimal? MinTxOtherIncomeFilter { get; set; }

        public decimal? MaxNtxBonusFilter { get; set; }
        public decimal? MinNtxBonusFilter { get; set; }

        public decimal? MaxNtxOtherIncomeFilter { get; set; }
        public decimal? MinNtxOtherIncomeFilter { get; set; }

        public decimal? MaxWTaxFilter { get; set; }
        public decimal? MinWTaxFilter { get; set; }

        public decimal? MaxSSSFilter { get; set; }
        public decimal? MinSSSFilter { get; set; }

        public decimal? MaxPhilhealthFilter { get; set; }
        public decimal? MinPhilhealthFilter { get; set; }

        public decimal? MaxPagibigFilter { get; set; }
        public decimal? MinPagibigFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

    }
}