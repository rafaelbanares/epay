﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEarningTypeSettingsForViewDto
    {
        public EarningTypeSettingsDto EarningTypeSettings { get; set; }

    }
}