﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class DeductionTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public string Currency { get; set; }

    }
}