﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class LoansDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string LoanType { get; set; }

        public string Currency { get; set; }

        public string RefCode { get; set; }

        public string AcctCode { get; set; }

    }
}