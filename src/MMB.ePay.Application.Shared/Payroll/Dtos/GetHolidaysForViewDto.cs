﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetHolidaysForViewDto
    {
        public HolidaysDto Holidays { get; set; }

    }
}