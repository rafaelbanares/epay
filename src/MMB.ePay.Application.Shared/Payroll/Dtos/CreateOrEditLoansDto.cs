﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditLoansDto : EntityDto<int?>
    {

        [StringLength(LoansConsts.MaxRefCodeLength, MinimumLength = LoansConsts.MinRefCodeLength)]
        public string RefCode { get; set; }

        [Required]
        [StringLength(LoansConsts.MaxDisplayNameLength, MinimumLength = LoansConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(LoansConsts.MaxDescriptionLength, MinimumLength = LoansConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Range(1, int.MaxValue)]
        public int LoanTypeId { get; set; }

        [Required]
        [StringLength(LoansConsts.MaxCurrencyLength, MinimumLength = LoansConsts.MinCurrencyLength)]
        public string Currency { get; set; }

        [StringLength(LoansConsts.MaxAcctCodeLength, MinimumLength = LoansConsts.MinAcctCodeLength)]
        public string AcctCode { get; set; }

    }
}