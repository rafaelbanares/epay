﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class TimesheetTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string Multiplier { get; set; }

        public string SysCode { get; set; }

        public bool Taxable { get; set; }

        public string AcctCode { get; set; }

        public string Category { get; set; }

    }
}