﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetDeductionTypesForViewDto
    {
        public DeductionTypesDto DeductionTypes { get; set; }

    }
}