﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEarningsForViewDto
    {
        public EarningsDto Earnings { get; set; }

    }
}