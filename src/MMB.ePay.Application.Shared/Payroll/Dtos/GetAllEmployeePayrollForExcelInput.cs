﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEmployeePayrollForExcelInput
    {
        public string Filter { get; set; }

        public string PayrollTypeFilter { get; set; }

        public decimal? MaxSalaryFilter { get; set; }
        public decimal? MinSalaryFilter { get; set; }

        public string PagibigNumberFilter { get; set; }

        public decimal? MaxPagibigAddFilter { get; set; }
        public decimal? MinPagibigAddFilter { get; set; }

        public decimal? MaxPagibigRateFilter { get; set; }
        public decimal? MinPagibigRateFilter { get; set; }

        public string SSSNumberFilter { get; set; }

        public string PhilhealthNumberFilter { get; set; }

        public string PaymentTypeFilter { get; set; }

        public string BankAccountNoFilter { get; set; }

        public string TINFilter { get; set; }

        public decimal? MaxTaxRateFilter { get; set; }
        public decimal? MinTaxRateFilter { get; set; }

        public string EmploymentTypeFilter { get; set; }

        public string RankFilter { get; set; }

        public int? OT_exemptFilter { get; set; }

        public int? SSS_exemptFilter { get; set; }

        public int? PH_exemptFilter { get; set; }

        public int? PAG_exemptFilter { get; set; }

        public int? Tax_exemptFilter { get; set; }

        public int? ComputeAsDailyFilter { get; set; }

    }
}