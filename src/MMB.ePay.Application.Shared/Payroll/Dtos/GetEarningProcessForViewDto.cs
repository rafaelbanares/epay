﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEarningProcessForViewDto
    {
        public EarningProcessDto EarningProcess { get; set; }

    }
}