﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetReviewersForViewDto
    {
        public ReviewersDto Reviewers { get; set; }

    }
}