﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class BanksDto : EntityDto<string>
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

    }
}