﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditProcessHistoryDto : EntityDto<int?>
    {
        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public int ProcessStatusId { get; set; }

        [StringLength(ProcessHistoryConsts.MaxRemarksLength, MinimumLength = ProcessHistoryConsts.MinRemarksLength)]
        public string Remarks { get; set; }
    }
}