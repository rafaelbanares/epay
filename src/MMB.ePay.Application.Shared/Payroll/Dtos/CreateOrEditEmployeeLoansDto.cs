﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEmployeeLoansDto : EntityDto<Guid?>
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Employee Field is required.")]
        public int EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Loan Field is required.")]
        public int LoanId { get; set; }

        [Required]
        [DisplayName("Approved Date")]
        public DateTime? ApprovedDate { get; set; }

        [Required]
        [DisplayName("Effect Start")]
        public DateTime? EffectStart { get; set; }

        [Required]
        public decimal? Principal { get; set; }

        [Required]
        [DisplayName("Loan Amount")]
        public decimal? LoanAmount { get; set; }

        [Required]
        public decimal? Amortization { get; set; }

        [StringLength(EmployeeLoansConsts.MaxFrequencyLength, MinimumLength = EmployeeLoansConsts.MinFrequencyLength)]
        public string Frequency { get; set; }

        [StringLength(EmployeeLoansConsts.MaxRemarksLength, MinimumLength = EmployeeLoansConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public DateTime? HoldStart { get; set; }

        public DateTime? HoldEnd { get; set; }

        public string Status { get; set; }

        public decimal TotalDeducted { get; set; }

        public decimal Balance { get { return LoanAmount ?? 0 - TotalDeducted; } }

        public string PayFreq { get; set; }

        public bool FirstPeriod { get; set; }
        public bool SecondPeriod { get; set; }
        public bool ThirdPeriod { get; set; }
        public bool FourthPeriod { get; set; }
        public bool LastPeriod { get; set; }
    }
}