﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetEarningPostedForEditOutput
    {
        public CreateOrEditEarningPostedDto EarningPosted { get; set; }

    }
}