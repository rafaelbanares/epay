﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetCompanyStatutoryInfoForEditOutput
    {
        public CreateOrEditCompanyStatutoryInfoDto CompanyStatutoryInfo { get; set; }

    }
}