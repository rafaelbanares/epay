﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetDependentsForViewDto
    {
        public DependentsDto Dependents { get; set; }

    }
}