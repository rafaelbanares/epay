﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class EmployeePayrollDto : EntityDto
    {
        public string PayrollType { get; set; }

        public decimal Salary { get; set; }

        public string PagibigNumber { get; set; }

        public decimal? PagibigAdd { get; set; }

        public decimal? PagibigRate { get; set; }

        public string SSSNumber { get; set; }

        public string PhilhealthNumber { get; set; }

        public string PaymentType { get; set; }

        public string BankAccountNo { get; set; }

        public string TIN { get; set; }

        public decimal? TaxRate { get; set; }

        public string EmploymentType { get; set; }

        public string Rank { get; set; }

        public bool OT_exempt { get; set; }

        public bool SSS_exempt { get; set; }

        public bool PH_exempt { get; set; }

        public bool PAG_exempt { get; set; }

        public bool Tax_exempt { get; set; }

        public bool ComputeAsDaily { get; set; }

    }
}