﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class PayPeriodDto : EntityDto
    {
        public string PayrollFreq { get; set; }

        public string ProcessType { get; set; }

        public DateTime PeriodFrom { get; set; }

        public DateTime PeriodTo { get; set; }

        public DateTime PayDate { get; set; }

        public int ApplicableMonth { get; set; }

        public int ApplicableYear { get; set; }

        public int? ManDays { get; set; }

        public DateTime CutOffFrom { get; set; }

        public DateTime CutOffTo { get; set; }

        public string Currency { get; set; }

        public decimal? ExchangeRate { get; set; }

        public int FiscalYear { get; set; }

        public int? PayrollWeekNo { get; set; }

        public string WeekNo { get; set; }

        public string PayrollStatus { get; set; }

        public string Remarks { get; set; }

    }
}