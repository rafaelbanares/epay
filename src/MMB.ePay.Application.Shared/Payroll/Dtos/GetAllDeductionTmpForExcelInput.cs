﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllDeductionTmpForExcelInput
    {
        public string Filter { get; set; }

        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public decimal? MaxAmountFilter { get; set; }
        public decimal? MinAmountFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxDeductionTypeIdFilter { get; set; }
        public int? MinDeductionTypeIdFilter { get; set; }

        public int? MaxRecurDeductionIdFilter { get; set; }
        public int? MinRecurDeductionIdFilter { get; set; }

        public Guid? SessionIdFilter { get; set; }

    }
}