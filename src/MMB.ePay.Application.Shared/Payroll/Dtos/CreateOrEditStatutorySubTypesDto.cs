﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditStatutorySubTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(StatutorySubTypesConsts.MaxDisplayNameLength, MinimumLength = StatutorySubTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

    }
}