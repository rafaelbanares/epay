﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetTenantBanksForEditOutput
    {
        public CreateOrEditTenantBanksDto TenantBanks { get; set; }

    }
}