﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllRecurEarningsForExcelInput
    {
        public string Filter { get; set; }

        public decimal? MaxAmountFilter { get; set; }
        public decimal? MinAmountFilter { get; set; }

        public string FrequencyFilter { get; set; }

        public DateTime? MaxStartDateFilter { get; set; }
        public DateTime? MinStartDateFilter { get; set; }

        public DateTime? MaxEndDateFilter { get; set; }
        public DateTime? MinEndDateFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxEarningTypeIdFilter { get; set; }
        public int? MinEarningTypeIdFilter { get; set; }

    }
}