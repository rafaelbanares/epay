﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllLoanTypesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string SysCodeFilter { get; set; }

        public string DescriptionFilter { get; set; }

    }
}