﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditDeductionTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(DeductionTypesConsts.MaxDisplayNameLength, MinimumLength = DeductionTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(DeductionTypesConsts.MaxDescriptionLength, MinimumLength = DeductionTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        [StringLength(DeductionTypesConsts.MaxCurrencyLength, MinimumLength = DeductionTypesConsts.MinCurrencyLength)]
        public string Currency { get; set; }
    }
}