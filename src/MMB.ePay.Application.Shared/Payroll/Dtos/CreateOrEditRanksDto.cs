﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditRanksDto : EntityDto<int?>
    {

        [Required]
        [StringLength(RanksConsts.MaxDescriptionLength, MinimumLength = RanksConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}