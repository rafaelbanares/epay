﻿using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetEmployeeStatutoryInfoForEditOutput
    {
        public int EmployeeId { get; set; }

        public string[] StatutoryAccountNo { get; set; }
    }
}