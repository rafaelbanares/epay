﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditOTSheetDto : EntityDto<int?>
    {
        public CreateOrEditOTSheetDto()
        {
            Rate = new List<decimal?>();
        }

        public int EmployeeId { get; set; }

        public IList<decimal?> Rate { get; set; }
    }
}