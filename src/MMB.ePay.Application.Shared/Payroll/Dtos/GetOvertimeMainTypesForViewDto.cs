﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetOvertimeMainTypesForViewDto
    {
        public OvertimeMainTypesDto OvertimeMainTypes { get; set; }

    }
}