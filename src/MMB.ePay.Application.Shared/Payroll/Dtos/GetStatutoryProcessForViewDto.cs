﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetStatutoryProcessForViewDto
    {
        public StatutoryProcessDto StatutoryProcess { get; set; }

    }
}