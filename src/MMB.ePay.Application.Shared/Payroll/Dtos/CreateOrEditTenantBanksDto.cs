﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditTenantBanksDto : EntityDto<int?>
    {
        [Required]
        public string BankId { get; set; }

        [Required]
        [StringLength(TenantBanksConsts.MaxBankAccountNumberLength, MinimumLength = TenantBanksConsts.MinBankAccountNumberLength)]
        public string BankAccountNumber { get; set; }

        [Required]
        [StringLength(TenantBanksConsts.MaxBankAccountTypeLength, MinimumLength = TenantBanksConsts.MinBankAccountTypeLength)]
        public string BankAccountType { get; set; }

        [StringLength(TenantBanksConsts.MaxBranchNumberLength, MinimumLength = TenantBanksConsts.MinBranchNumberLength)]
        public string BranchNumber { get; set; }

        [Range(0, int.MaxValue)]
        public int BankBranchId { get; set; }

        public string CorporateId { get; set; }

    }
}