﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class EmployeeLoansDto : EntityDto<Guid>
    {
        public int EmployeeId { get; set; }

        public string Employee { get; set; }

        public string Loan { get; set; }

        public decimal Balance { get; set; }

        public DateTime ApprovedDate { get; set; }

        public DateTime EffectStart { get; set; }

        public decimal Principal { get; set; }

        public decimal LoanAmount { get; set; }

        public decimal Amortization { get; set; }

        public string Frequency { get; set; }

        public string Remarks { get; set; }

        public DateTime? HoldStart { get; set; }

        public DateTime? HoldEnd { get; set; }

        public string Status { get; set; }
    }
}