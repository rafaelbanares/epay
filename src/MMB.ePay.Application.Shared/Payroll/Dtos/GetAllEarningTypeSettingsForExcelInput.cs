﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEarningTypeSettingsForExcelInput
    {
        public string Filter { get; set; }

        public string KeyFilter { get; set; }

        public string ValueFilter { get; set; }

        public int? MaxEarningTypeIdFilter { get; set; }
        public int? MinEarningTypeIdFilter { get; set; }

    }
}