﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetRecurEarningsForEditOutput
    {
        public CreateOrEditRecurEarningsDto RecurEarnings { get; set; }

    }
}