﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditLeaveTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(LeaveTypesConsts.MaxDisplayNameLength, MinimumLength = LeaveTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(LeaveTypesConsts.MaxDescriptionLength, MinimumLength = LeaveTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public bool WithPay { get; set; }

    }
}