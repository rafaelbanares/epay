﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class OvertimeTypesDto : EntityDto<string>
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public decimal Rate { get; set; }

    }
}