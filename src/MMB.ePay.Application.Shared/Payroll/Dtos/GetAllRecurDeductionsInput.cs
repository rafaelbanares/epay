﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllRecurDeductionsInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeIdFilter { get; set; }
    }
}