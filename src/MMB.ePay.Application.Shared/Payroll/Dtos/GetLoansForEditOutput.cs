﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetLoansForEditOutput
    {
        public CreateOrEditLoansDto Loans { get; set; }

    }
}