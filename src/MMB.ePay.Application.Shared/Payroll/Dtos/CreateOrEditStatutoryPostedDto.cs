﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditStatutoryPostedDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public int StatutoryTypeId { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

    }
}