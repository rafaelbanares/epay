﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditHolidaysDto : EntityDto<int?>
    {

        [Required]
        [StringLength(HolidaysConsts.MaxDisplayNameLength, MinimumLength = HolidaysConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public bool HalfDay { get; set; }

        [Required]
        [StringLength(HolidaysConsts.MaxHolidayTypeIdLength, MinimumLength = HolidaysConsts.MinHolidayTypeIdLength)]
        public string HolidayTypeId { get; set; }

    }
}