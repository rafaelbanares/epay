﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetCompanyStatutoryInfoForViewDto
    {
        public CompanyStatutoryInfoDto CompanyStatutoryInfo { get; set; }

    }
}
