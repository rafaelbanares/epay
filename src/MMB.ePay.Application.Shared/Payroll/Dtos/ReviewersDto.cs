﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class ReviewersDto : EntityDto
    {
        public string ReviewerType { get; set; }

        public string ReviewedBy { get; set; }

    }
}