﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayrollPostedForViewDto
    {
        public PayrollPostedDto PayrollPosted { get; set; }

    }
}