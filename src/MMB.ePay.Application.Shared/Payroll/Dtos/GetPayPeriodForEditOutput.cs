﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayPeriodForEditOutput
    {
        public CreateOrEditPayPeriodDto PayPeriod { get; set; }

    }
}