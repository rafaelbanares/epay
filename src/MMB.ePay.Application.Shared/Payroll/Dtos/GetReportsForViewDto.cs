﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetReportsForViewDto
    {
        public GetReportsForViewDto()
        {
            ReportList = new List<SelectListItem>();
            PayCodeList = new List<SelectListItem>();
            YearList = new List<SelectListItem>();
        }

        public ReportsDto Reports { get; set; }

        public IList<SelectListItem> ReportList { get; set; }
        public IList<SelectListItem> PayCodeList { get; set; }
        public IList<SelectListItem> YearList { get; set; }
    }

    public class ReportsEmployerDto
    {
        public string EmployerName { get; set; }
        public string EmployerAddress { get; set; }
        public string TIN { get; set; }
    }

    public class ReportsSignatoryDto
    {
        public string SignatoryName { get; set; }
        public string Position { get; set; }
        public string ContactNo { get; set; }
    }

    public class PayrollRegisterHeaderReportViewModel
    {
        public bool IsPosted { get; set; }
        public string CompanyName { get; set; }
        public string PayrollPeriod { get; set; }
        public string CutoffPeriod { get; set; }
    }

    public class GetReportsForBankAdvise
    {
        public GetReportsForBankAdvise()
        {
            BankBranches = new List<BankBranchTable>();
        }

        public string TenantName { get; set; }


        public string SignatoryName { get; set; }

        public string SignatoryPosition { get; set; }

        public IList<BankBranchTable> BankBranches { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
    }

    public class BankBranchTable
    {
        public BankBranchTable()
        {
            BankAdvise = new List<AdviseTable>();
        }

        public string BankName { get; set; }

        public string BranchName { get; set; }

        public string TenantAccountNo { get; set; }

        public IList<AdviseTable> BankAdvise { get; set; }
    }

    public class AdviseTable
    {
        [DisplayName("EmployeeName")]
        public string EmployeeName { get; set; }

        [DisplayName("BankAccountNo")]
        public string BankAccountNo { get; set; }

        [DisplayName("Amount")]
        public decimal Amount { get; set; }
    }

    public class GetReportsForCashAdvise
    {
        public IList<AdviseTable> CashAdvise { get; set; }
    }

    public class GetReportsForChequeAdvise
    {
        public IList<AdviseTable> ChequeAdvise { get; set; }
    }

    public class GetReportsForDeductionRegister
    {
        public string Grouping { get; set; }

        public IList<DeductionRegisterGroup> DeductionRegisterGroup { get; set; }
    }

    public class DeductionRegisterGroup
    {
        public string GroupBy { get; set; }

        public IList<DeductionRegisterTable> DeductionRegister { get; set; }
    }

    public class DeductionRegisterTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date")]
        public DateTime? Date { get; set; }

        [DisplayName("Deduction Code")]
        public string DeductionCode { get; set; }

        [DisplayName("Amount")]
        public decimal Amount { get; set; }
    }

    public class GetReportsForEarningRegister
    {
        public string Grouping { get; set; }

        public IList<EarningRegisterGroup> EarningRegisterGroup { get; set; }
    }

    public class EarningRegisterGroup
    {
        public string GroupBy { get; set; }

        public IList<EarningRegisterTable> EarningRegister { get; set; }
    }

    public class EarningCodeList
    {
        public string EarningCode { get; set; }

        public IList<EarningRegisterTable> EarningRegister { get; set; }
    }

    public class EarningRegisterTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date")]
        public DateTime? Date { get; set; }

        [DisplayName("Deduction Code")]
        public string EarningCode { get; set; }

        [DisplayName("Amount")]
        public decimal Amount { get; set; }
    }

    public class GetReportsForLoanDetailRegister
    {
        public string Grouping { get; set; }

        public IList<LoanDetailRegisterGroup> LoanDetailRegisterGroup { get; set; }
    }

    public class LoanDetailRegisterGroup
    {
        public string GroupBy { get; set; }

        public IList<LoanDetailRegisterTable> LoanDetailRegister { get; set; }
    }

    public class LoanDetailRegisterTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date Start")]
        public DateTime? DateStart { get; set; }

        [DisplayName("Loan Code")]
        public string LoanCode { get; set; }

        [DisplayName("Loan Payment")]
        public decimal LoanPayment { get; set; }

        [DisplayName("Balance")]
        public decimal Balance { get; set; }
    }

    public class GetReportsForOvertimeRegister
    {
        public string Grouping { get; set; }

        public IList<OvertimeRegisterGroup> OvertimeRegisterGroup { get; set; }
        public IList<string> OvertimeTypes { get; set; }
    }

    public class OvertimeRegisterGroup
    {
        public string GroupBy { get; set; }
        public IList<OvertimeRegisterTable> OvertimeRegister { get; set; }
        public IList<Tuple<string, decimal>> Amounts { get; set; }
    }


    public class OvertimeRegisterTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Overtime Code")]
        public string OvertimeCode { get; set; }


        //[DisplayName("Amount")]
        //public decimal Amount { get; set; }
    }

    public class GetReportsForEmployeePayslip
    {
        public IList<EmployeePayslipTable> EmployeePayslip { get; set; }
    }

    public class EmployeePayslipTable
    {
        public string CompanyName { get; set; }

        public string Employee { get; set; }

        public string Period { get; set; }

        public string CutOffPeriod { get; set; }

        public DateTime PayDate { get; set; }

        public IList<Tuple<string, string, decimal>> Income { get; set; }

        public IList<Tuple<string, decimal>> Deductions { get; set; }

        public IList<Tuple<string, decimal>> NetPay { get; set; }

        public IList<Tuple<string, decimal>> YtdSummary { get; set; }

        public IList<Tuple<string, decimal, decimal>> LoanBalances { get; set; }
    }

    public class GetReportsForGrossAndTax
    {
        public IList<GrossAndTaxTable> GrossAndTax { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
    }

    public class GrossAndTaxTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("TIN")]
        public string TIN { get; set; }

        [DisplayName("GrossPay")]
        public decimal GrossPay { get; set; }

        [DisplayName("Total")]
        public decimal Total { get; set; }

        [DisplayName("TaxableInc")]
        public decimal TaxableInc { get; set; }

        [DisplayName("WTax")]
        public decimal WTax { get; set; }
    }

    public class GetReportsForPagibigContribution
    {
        public string Person { get; set; }

        public string Position { get; set; }

        public IList<PagibigContributionTable> PagibigContribution { get; set; }
    }

    public class PagibigContributionTable
    {
        public string EmployeeId { get; set; }

        public string PagibigNo { get; set; }

        public string EmployeeName { get; set; }

        public decimal Employer { get; set; }

        public decimal Employee { get; set; }

        public decimal Total { get; set; }
    }

    public class GetReportsForPagibigM11
    {
        public IList<PagibigM11Table> PagibigM11 { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
        public ReportsEmployerDto Employer { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
    }

    public class PagibigM11Table
    {
        public string PagibigNo { get; set; }
        public string BorrowerName { get; set; }
        public decimal Employee { get; set; }
        public decimal Employer { get; set; }
    }

    public class GetReportsForPagibigMRCRF
    {
        public IList<PagibigMRCRFTable> PagibigMRCRF { get; set; }
        public ReportsEmployerDto Employer { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
        public string PagibigEmployerNo { get; set; }
    }

    public class PagibigMRCRFTable
    {
        public string PagibigNo { get; set; }
        public string AcctNo { get; set; }
        public string MembershipProgram { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string PeriodCovered { get; set; }
        public decimal MonthlyCompensation { get; set; }
        public decimal EEShare { get; set; }
        public decimal ERShare { get; set; }
        public string Remarks { get; set; }
    }

    public class GetReportsForPayrollRegister
    {
        public IList<PayrollRegisterTable> PayrollRegister { get; set; }
    }

    public class PayrollRegisterTable
    {
        public PayrollRegisterTable()
        {
            PayrollRegisterTimesheet = new List<PayrollRegisterStatutory>();
            PayrollRegisterOvertime = new List<PayrollRegisterStatutory>();
            PayrollRegisterEarnings = new List<PayrollRegisterStatutory>();
            PayrollRegisterLoans = new List<PayrollRegisterStatutory>();
            PayrollRegisterDeductions = new List<PayrollRegisterStatutory>();
        }

        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public decimal Timesheet { get; set; }
        public decimal Overtime { get; set; }
        public decimal NtxIncome { get; set; }
        public decimal GrossPay { get; set; }
        public decimal WTax { get; set; }
        public decimal Sss { get; set; }
        public decimal Philhealth { get; set; }
        public decimal Pagibig { get; set; }
        public decimal SssEr { get; set; }
        public decimal Ecc { get; set; }
        public decimal PhEr { get; set; }
        public decimal PagEr { get; set; }
        public decimal TotalLoans { get; set; }
        public decimal TotalDed { get; set; }
        public decimal NetPay { get; set; }

        public IList<PayrollRegisterStatutory> PayrollRegisterTimesheet { get; set; }
        public IList<PayrollRegisterStatutory> PayrollRegisterOvertime { get; set; }
        public IList<PayrollRegisterStatutory> PayrollRegisterEarnings { get; set; }
        public IList<PayrollRegisterStatutory> PayrollRegisterLoans { get; set; }
        public IList<PayrollRegisterStatutory> PayrollRegisterDeductions { get; set; }
    }

    public class PayrollRegisterStatutory
    {
        public string StatutoryName { get; set; }

        public decimal Value { get; set; }
    }

    public class GetReportsForPhilhealthER2
    {
        public IList<PhilhealthER2Table> PhilhealthER2 { get; set; }
        public ReportsEmployerDto Employer { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
        public string PhilhealtEmployerNo { get; set; }
    }

    public class PhilhealthER2Table
    {
        public string PhilhealthNo { get; set; }
        public string EmployeeName { get; set; }
        public string Position { get; set; }
        public decimal Salary { get; set; }
        public DateTime? DateOfEmployment { get; set; }
        public string PreviousEmployer { get; set; }
    }

    public class GetReportsForPhilhealthRF1
    {
        public IList<PhilhealthRF1Table> PhilhealthRF1 { get; set; }
    }

    public class PhilhealthRF1Table
    {
        public string PhilhealthNo { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Sex { get; set; }
        public string MSB { get; set; }
        public decimal PS { get; set; }
        public decimal ES { get; set; }
        public string EmployeeStatus { get; set; }
    }

    public class GetReportsForPhilhealthContribution
    {
        public IList<PhilhealthContributionTable> PhilhealthContribution { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
    }

    public class PhilhealthContributionTable
    {
        public string EmployeeId { get; set; }

        public string PhilhealthNo { get; set; }

        public string EmployeeName { get; set; }

        public decimal Employer { get; set; }

        public decimal Employee { get; set; }

        public decimal Total { get; set; }
    }

    public class GetReportsForSssContribution
    {
        public IList<SssContributionTable> SssContribution { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
    }

    public class SssContributionTable
    {
        public string EmployeeId { get; set; }

        public string SssNo { get; set; }

        public string EmployeeName { get; set; }

        public decimal Employer { get; set; }

        public decimal Employee { get; set; }

        public decimal Total { get; set; }
    }

    public class GetReportsForTimesheetRegister
    {
        public IList<TimesheetRegisterHeader> TimesheetRegisterHeader { get; set; }

        public IList<TimesheetRegisterTable> TimesheetRegister { get; set; }
    }

    public class TimesheetRegisterHeader
    {
        public string SysCode { get; set; }

        public string HeaderName { get; set; }
    }

    public class TimesheetRegisterTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        public IList<TimesheetRegisterValue> TimesheetRegisterValue { get; set; }
    }

    public class TimesheetRegisterValue
    {
        public string SysCode { get; set; }

        public decimal Hours { get; set; }

        public decimal Value { get; set; }
    }

    public class GetReportsForSSSML2
    {
        public string Month { get; set; }
        public int Year { get; set; }
        public string SSSEmployerNo { get; set; }
        public IList<SSSML2Table> SSSML2 { get; set; }
        public ReportsEmployerDto Employer { get; set; }
        public ReportsSignatoryDto Signatory { get; set; }
    }

    public class SSSML2Table
    {
        public string SSNo { get; set; }
        public string BorrowerName { get; set; }
        public string LoanType { get; set; }
        public DateTime? DateGranted { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal CurrentAmountDue { get; set; }
        public decimal OverdueAmountDue { get; set; }
        public string Remarks { get; set; }
    }

    public class GetReportsForSSSR3
    {
        public IList<SSSR3Table> SSSR3 { get; set; }
    }

    public class SSSR3Table
    {
        public string SSSNo { get; set; }
        public string NameOfMember { get; set; }
        public decimal SS1stMonth { get; set; }
        public decimal SS2ndMonth { get; set; }
        public decimal SS3rdMonth { get; set; }
        public decimal Compensation1stMonth { get; set; }
        public decimal Compensation2ndMonth { get; set; }
        public decimal Compensation3rdMonth { get; set; }
        public DateTime? SeparationDate { get; set; }
    }

    public class GetReportsForAlphalist
    {
        public IList<AlphalistTable> Alphalist { get; set; }
    }

    public class AlphalistTable
    {
        public string TaxpayerIdNo { get; set; }
        public string NameOfEmployees { get; set; }
        public decimal Gross { get; set; }
        public decimal Ntx13thMonth { get; set; }
        public decimal NtxDeminimis { get; set; }
        public decimal NtxSSS { get; set; }
        public decimal Salaries { get; set; }
        public decimal NtxExempt { get; set; }
        public decimal TxBasic { get; set; }
        public decimal Tx13thMonth { get; set; }
        public decimal TxSalaries { get; set; }
        public decimal TxIncome { get; set; }
        public string ExemptionCode { get; set; }
        public decimal ExemptionAmount { get; set; }
        public decimal Premium { get; set; }
        public decimal NetTxIncome { get; set; }
        public decimal TaxDue { get; set; }
        public decimal TaxWithheld { get; set; }
        public decimal WithheldAmount { get; set; }
        public decimal OverWithheldTax { get; set; }
        public decimal WithheldTax { get; set; }
    }

    public class GetReportsForAlphalistNonMWE1
    {
        public IList<AlphalistNonMWE1Table> Alphalist { get; set; }
    }

    public class AlphalistNonMWE1Table
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }

    public class GetReportsForAlphalistNonMWE2
    {
        public IList<AlphalistNonMWE2Table> Alphalist { get; set; }
    }

    public class AlphalistNonMWE2Table
    {
        public string NameOfEmployees { get; set; }
    }

    public class GetReportsForAlphalistNonMWE3
    {
        public IList<AlphalistNonMWE3Table> Alphalist { get; set; }
    }

    public class AlphalistNonMWE3Table
    {
        public string NameOfEmployees { get; set; }
    }

    public class GetReportsForAlphalistMWE1
    {
        public IList<AlphalistMWE1Table> Alphalist { get; set; }
    }

    public class AlphalistMWE1Table
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string EmploymentStatus { get; set; }
        public string RegionNo { get; set; }
        public DateTime? EmploymentDateStart { get; set; }
        public DateTime? EmploymentDateEnd { get; set; }
        public string ReasonOfSeparation { get; set; }
        public decimal GrossCompensationIncome { get; set; }
        public decimal NtxBasicSMWPerDay { get; set; }
        public decimal NtxBasicSMWPerMonth { get; set; }
        public decimal NtxBasicSMWPerYear { get; set; }
        public decimal NtxFactorUsed { get; set; }
        public decimal NtxActualBasicSMW { get; set; }
        public decimal NtxHolidayPay { get; set; }
        public decimal NtxOvertimePay { get; set; }
        public decimal NtxNightShift { get; set; }
        public decimal NtxHazardPay { get; set; }
        public decimal NtxOtherBenefits { get; set; }
        public decimal NtxDeMinimisBenefits { get; set; }
        public decimal NtxUnionDues { get; set; }
        public decimal NtxOtherCompensation { get; set; }
        public decimal NtxTotalCompensation { get; set; }
        public decimal TxOtherBenefits { get; set; }
        public decimal TxOtherCompensation { get; set; }
        public decimal TxTotalCompensation { get; set; }
    }

    public class GetReportsForAlphalistMWE2
    {
        public IList<AlphalistMWE2Table> Alphalist { get; set; }
    }

    public class AlphalistMWE2Table
    {
        public string NameOfEmployees { get; set; }
    }

    public class GetReportsForAlphalistMWE3
    {
        public IList<AlphalistMWE3Table> Alphalist { get; set; }
    }

    public class AlphalistMWE3Table
    {
        public string NameOfEmployees { get; set; }
    }

    public class GetReportsForBIR2316
    {
        public IList<BIR2316Table> BIR2316 { get; set; }
    }

    public class BIR2316Table
    {
        public BIR2316Table()
        {
            Employer = new BIREmployerInfo();
            PrevEmployer = new BIREmployerInfo();
        }

        public short Year { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }

        //Part I - Employee Information
        public string TIN { get; set; }
        public string EmployeeName { get; set; }
        public string RDOCode { get; set; }
        public string RegisteredAddress { get; set; }
        public string RegisteredZipCode { get; set; }
        public string LocalAddress { get; set; }
        public string LocalZipCode { get; set; }
        public string ForeignAddress { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ContactNumber { get; set; }
        public decimal? StatutoryMinimumWagePerDay { get; set; }
        public decimal? StatutoryMinimumWagePerMonth { get; set; }
        public bool IsMWE { get; set; }
        public BIREmployerInfo Employer { get; set; }
        public string TypeOfEmployer { get; set; }
        public BIREmployerInfo PrevEmployer { get; set; }

        //Part IVA - Summary
        public decimal? GrossCompensationIncome { get; set; }
        public decimal? ExcemptCompensationIncome { get; set; }
        public decimal? GrossTaxableCompensationIncome { get; set; }
        public decimal? TaxDue { get; set; }
        public decimal? TotalTaxesWithheldAdjusted { get; set; }
        public decimal? TaxCredit { get; set; }
        public decimal? TotalTaxesWithheld { get; set; }

        //Part IV-B Details of Compensation Income & Tax Withheld from Present Employer
        public decimal? NTxBasicSalary { get; set; }
        public decimal? NTxHolidayPay { get; set; }
        public decimal? NTxOvertimePay { get; set; }
        public decimal? NTxNightShiftDifferential { get; set; }
        public decimal? NTxHazardPay { get; set; }
        public decimal? NTxOtherBenefits { get; set; }
        public decimal? NTxDeMinimis { get; set; }
        public decimal? NTxUnionDues { get; set; }
        public decimal? NTxOtherCompensation { get; set; }
        public decimal? NTxExemptCompensation { get; set; }

        public decimal? TxBasicSalary { get; set; }
        public decimal? TxRepresentation { get; set; }
        public decimal? TxTransportation { get; set; }
        public decimal? TxCostOfLiving { get; set; }
        public decimal? TxFixedHousing { get; set; }
        public decimal? TxOthers1 { get; set; }
        public decimal? TxOthers2 { get; set; }
        public decimal? TxOthers3 { get; set; }
        public decimal? TxOthers4 { get; set; }
        public decimal? TxCommission { get; set; }
        public decimal? TxProfitSharing { get; set; }
        public decimal? TxDirectorFees { get; set; }
        public decimal? Tx13thMonthBenefits { get; set; }
        public decimal? TxHazardPay { get; set; }
        public decimal? TxOvertimePay { get; set; }
        public decimal? TxOthers5 { get; set; }
        public decimal? TxOthers6 { get; set; }
        public decimal? TxOthers7 { get; set; }
        public decimal? TxOthers8 { get; set; }
        public decimal? TxCompensationIncome { get; set; }
    }

    public class BIREmployerInfo
    {
        public string TIN { get; set; }
        public string EmployerName { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public decimal? TaxableCompensationIncome { get; set; }
        public decimal? TaxesWithheld { get; set; }
    }


    public class GetReportsForPayslip
    {
        public GetReportsForPayslip()
        {
            Earnings = new List<Tuple<string, string, decimal>>();

            Deductions = new List<Tuple<string, decimal>>();

            YTDSummaries = new List<Tuple<string, decimal>>();

            LoanBalances = new List<Tuple<string, decimal>>();

            LeaveBalances = new List<Tuple<string, decimal>>();
        }

        public string CompanyName { get; set; }

        public string PayrollPeriod { get; set; }

        public string CutoffPeriod { get; set; }

        public string EmployeeCode { get; set; }

        public string EmployeeName { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PayDate { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfJoining { get; set; }

        public IList<Tuple<string, string, decimal>> Earnings { get; set; }

        public IList<Tuple<string, decimal>> Deductions { get; set; }

        public IList<Tuple<string, decimal>> YTDSummaries { get; set; }

        public IList<Tuple<string, decimal>> LoanBalances { get; set; }

        public IList<Tuple<string, decimal>> LeaveBalances { get; set; }

        public decimal? TotalEarnings { get; set; }

        public decimal? TotalDeductions { get; set; }

        public decimal? NetPay { get; set; }
    }
}
