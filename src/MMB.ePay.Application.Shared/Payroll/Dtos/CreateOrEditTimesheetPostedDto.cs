﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditTimesheetPostedDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        public int? CostCenter { get; set; }

        [Required]
        public decimal Days { get; set; }

        [Required]
        public decimal Hrs { get; set; }

        [Required]
        public int Mins { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int TimesheetTypeId { get; set; }

    }
}