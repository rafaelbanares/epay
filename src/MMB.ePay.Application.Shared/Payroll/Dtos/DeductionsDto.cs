﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class DeductionsDto : EntityDto
    {
        public string DeductionType { get; set; }

        public int PayrollNo { get; set; }

        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int DeductionTypeId { get; set; }

    }
}