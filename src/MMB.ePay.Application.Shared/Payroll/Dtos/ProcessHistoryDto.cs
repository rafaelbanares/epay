﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class ProcessHistoryDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public int ProcessStatusId { get; set; }

        public string Remarks { get; set; }

    }
}