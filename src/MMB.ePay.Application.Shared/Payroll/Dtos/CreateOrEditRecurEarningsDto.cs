﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditRecurEarningsDto : EntityDto<int?>
    {

        [Required]
        public decimal? Amount { get; set; }

        public string Frequency { get; set; }

        [Required]
        public DateTime? StartDate { get; set; }

        [Required]
        public DateTime? EndDate { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "The Earning Type field is required.")]
        public int EarningTypeId { get; set; }

        public string PayFreq { get; set; }

        public bool FirstPeriod { get; set; }
        public bool SecondPeriod { get; set; }
        public bool ThirdPeriod { get; set; }
        public bool FourthPeriod { get; set; }
        public bool LastPeriod { get; set; }
    }
}