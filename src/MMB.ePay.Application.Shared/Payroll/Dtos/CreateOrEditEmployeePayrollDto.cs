﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEmployeePayrollDto : EntityDto<int?>
    {
        [Required]
        public string PayCodeId { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "The Bank Branch field is required.")]
        public int BankBranchId { get; set; }

        [Required]
        [StringLength(EmployeePayrollConsts.MaxPayrollTypeLength, MinimumLength = EmployeePayrollConsts.MinPayrollTypeLength)]
        public string PayrollType { get; set; }

        [Required]
        public decimal? Salary { get; set; }

        [StringLength(EmployeePayrollConsts.MaxPagibigNumberLength, MinimumLength = EmployeePayrollConsts.MinPagibigNumberLength)]
        public string PagibigNumber { get; set; }

        public decimal? PagibigAdd { get; set; }

        public decimal? PagibigRate { get; set; }

        [Required]
        [StringLength(EmployeePayrollConsts.MaxSSSNumberLength, MinimumLength = EmployeePayrollConsts.MinSSSNumberLength)]
        public string SSSNumber { get; set; }

        [StringLength(EmployeePayrollConsts.MaxPhilhealthNumberLength, MinimumLength = EmployeePayrollConsts.MinPhilhealthNumberLength)]
        public string PhilhealthNumber { get; set; }

        [Required]
        [StringLength(EmployeePayrollConsts.MaxPaymentTypeLength, MinimumLength = EmployeePayrollConsts.MinPaymentTypeLength)]
        public string PaymentType { get; set; }

        [StringLength(EmployeePayrollConsts.MaxBankAccountNoLength, MinimumLength = EmployeePayrollConsts.MinBankAccountNoLength)]
        public string BankAccountNo { get; set; }

        [StringLength(EmployeePayrollConsts.MaxTINLength, MinimumLength = EmployeePayrollConsts.MinTINLength)]
        public string TIN { get; set; }

        public decimal? TaxRate { get; set; }

        [StringLength(EmployeePayrollConsts.MaxEmploymentTypeLength, MinimumLength = EmployeePayrollConsts.MinEmploymentTypeLength)]
        public string EmploymentType { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "The Rank field is required.")]
        public int RankId { get; set; }

        [Required]
        public bool OT_exempt { get; set; }

        [Required]
        public bool SSS_exempt { get; set; }

        [Required]
        public bool PH_exempt { get; set; }

        [Required]
        public bool PAG_exempt { get; set; }

        [Required]
        public bool Tax_exempt { get; set; }

        [Required]
        public bool ComputeAsDaily { get; set; }

    }
}