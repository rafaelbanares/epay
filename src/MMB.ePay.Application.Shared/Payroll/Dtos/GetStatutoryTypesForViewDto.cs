﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetStatutoryTypesForViewDto
    {
        public StatutoryTypesDto StatutoryTypes { get; set; }

    }
}