﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetBankBranchesForViewDto
    {
        public BankBranchesDto BankBranches { get; set; }

    }
}