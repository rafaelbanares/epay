﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayrollProcessForEditOutput
    {
        public CreateOrEditPayrollProcessDto PayrollProcess { get; set; }
    }
}