﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllOvertimeTypesForExcelInput
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public decimal? MaxRateFilter { get; set; }
        public decimal? MinRateFilter { get; set; }

    }
}