﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetProcessForViewDto
    {
        public ProcessDto Process { get; set; }
    }

    public class GetProcessForProcessingViewDto
    {
        public int Id { get; set; }
        public int Step { get; set; }
        public string ProcessStatus { get; set; }
        public bool IsProcessor { get; set; }
        public string PayrollPeriod { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PayDate { get; set; }
    }

    public class GetProcessForPayrollEntryStepDto
    {
        public int Step { get; set; }
        public string Status { get; set; }
        public string ProcessStatus { get; set; }
    }

    public class GetProcessForProcessingStepDto
    {
        public int Step { get; set; }
        public string Status { get; set; }
        public int ProcessStatusId { get; set; }

        public string NextStatus { get; set; }
        public string ElapsedTime { get; set; }
    }

    public class GetProcessForReviewStepDto
    {
        public GetProcessForReviewStepDto()
        {
            Statutories = new List<Tuple<string, decimal>>();
        }

        public int Step { get; set; }
        public string Status { get; set; }
        public string SysCode { get; set; }

        public int HeadCount { get; set; }
        public decimal BasicPay { get; set; }
        public decimal Taxable { get; set; }
        public decimal NonTaxable { get; set; }
        public decimal Loan { get; set; }
        public decimal GrossPay { get; set; }
        public decimal NetPay { get; set; }

        public bool IsApprover { get; set; }
        public IList<Tuple<string, decimal>> Statutories { get; set; }

    }

    public class GetProcessForPostingStepDto
    {
        public int Step { get; set; }
        public string Status { get; set; }
        public string SysCode { get; set; }
    }
}