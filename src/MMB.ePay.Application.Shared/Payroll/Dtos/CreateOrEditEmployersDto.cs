﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEmployersDto : EntityDto<int?>
    {

        [Required]
        [StringLength(EmployersConsts.MaxEmployerNameLength, MinimumLength = EmployersConsts.MinEmployerNameLength)]
        public string EmployerName { get; set; }

        [Required]
        public DateTime? EmployedFrom { get; set; }

        public DateTime? EmployedTo { get; set; }

        [StringLength(EmployersConsts.MaxTINLength, MinimumLength = EmployersConsts.MinTINLength)]
        public string TIN { get; set; }

        [StringLength(EmployersConsts.MaxAddress1Length, MinimumLength = EmployersConsts.MinAddress1Length)]
        public string Address1 { get; set; }

        [StringLength(EmployersConsts.MaxAddress2Length, MinimumLength = EmployersConsts.MinAddress2Length)]
        public string Address2 { get; set; }

        [Required]
        public decimal? BasicPay { get; set; }

        [Required]
        public decimal? Deminimis { get; set; }

        [Required]
        public decimal? GrossPay { get; set; }

        [Required]
        public decimal? TxBonus { get; set; }

        [Required]
        public decimal? TxOtherIncome { get; set; }

        [Required]
        public decimal? NtxBonus { get; set; }

        [Required]
        public decimal? NtxOtherIncome { get; set; }

        [Required]
        public decimal? WTax { get; set; }

        [Required]
        public decimal? SSS { get; set; }

        [Required]
        public decimal? Philhealth { get; set; }

        [Required]
        public decimal? Pagibig { get; set; }

        public int EmployeeId { get; set; }

    }
}