﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class HolidaysDto : EntityDto
    {
        public string DisplayName { get; set; }

        public DateTime Date { get; set; }

        public bool HalfDay { get; set; }

        public string HolidayType { get; set; }

    }
}