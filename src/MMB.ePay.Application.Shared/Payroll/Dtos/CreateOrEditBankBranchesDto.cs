﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditBankBranchesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(BankBranchesConsts.MaxBankIdLength, MinimumLength = BankBranchesConsts.MinBankIdLength)]
        public string BankId { get; set; }

        public string DisplayName { get; set; }

        [StringLength(BankBranchesConsts.MaxBranchCodeLength, MinimumLength = BankBranchesConsts.MinBranchCodeLength)]
        public string BranchCode { get; set; }

        [Required]
        [StringLength(BankBranchesConsts.MaxBranchNameLength, MinimumLength = BankBranchesConsts.MinBranchNameLength)]
        public string BranchName { get; set; }

        [StringLength(BankBranchesConsts.MaxAddress1Length, MinimumLength = BankBranchesConsts.MinAddress1Length)]
        public string Address1 { get; set; }

        [StringLength(BankBranchesConsts.MaxAddress2Length, MinimumLength = BankBranchesConsts.MinAddress2Length)]
        public string Address2 { get; set; }

    }
}