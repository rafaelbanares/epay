﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetOTSheetTmpForViewDto
    {
        public OTSheetTmpDto OTSheetTmp { get; set; }

    }
}