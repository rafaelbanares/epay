﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class LoanTmpDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int EmployeeLoanId { get; set; }

        public Guid SessionId { get; set; }

    }
}