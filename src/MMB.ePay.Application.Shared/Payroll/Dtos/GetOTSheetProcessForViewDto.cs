﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetOTSheetProcessForViewDto
    {
        public OTSheetProcessDto OTSheetProcess { get; set; }

    }
}