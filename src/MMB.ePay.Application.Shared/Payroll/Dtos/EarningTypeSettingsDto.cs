﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class EarningTypeSettingsDto : EntityDto
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public int EarningTypeId { get; set; }

    }
}