﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayrollTmpForEditOutput
    {
        public CreateOrEditPayrollTmpDto PayrollTmp { get; set; }

    }
}