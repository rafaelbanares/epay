﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllProcessHistoryForExcelInput
    {
        public string Filter { get; set; }

        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public string StatusCodeFilter { get; set; }

        public string RemarksFilter { get; set; }

    }
}