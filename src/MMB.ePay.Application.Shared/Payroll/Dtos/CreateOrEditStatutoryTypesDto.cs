﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditStatutoryTypesDto : EntityDto<int?>
    {
        [Required]
        [StringLength(StatutoryTypesConsts.MaxCodeLength, MinimumLength = StatutoryTypesConsts.MinCodeLength)]
        public string Code { get; set; }

        [Required]
        [StringLength(StatutoryTypesConsts.MaxDescriptionLength, MinimumLength = StatutoryTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }
    }
}