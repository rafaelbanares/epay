﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetDeductionProcessForViewDto
    {
        public DeductionProcessDto DeductionProcess { get; set; }

    }
}