﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEmployeeLoansForEditOutput
    {
        public CreateOrEditEmployeeLoansDto EmployeeLoans { get; set; }
    }
}