﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEmployersForViewDto
    {
        public EmployersDto Employers { get; set; }

    }
}