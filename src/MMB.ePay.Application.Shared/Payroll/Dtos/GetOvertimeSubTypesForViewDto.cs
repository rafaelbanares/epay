﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetOvertimeSubTypesForViewDto
    {
        public OvertimeSubTypesDto OvertimeSubTypes { get; set; }

    }
}