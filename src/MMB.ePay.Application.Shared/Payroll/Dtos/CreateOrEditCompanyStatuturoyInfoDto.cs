﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditCompanyStatutoryInfoDto
    {
        public string Id { get; set; }

        [Required]
        public int SignatoryId { get; set; }

        [Required]
        [StringLength(CompanyStatutoryInfoConsts.MaxStatutoryAcctNoLength, MinimumLength = CompanyStatutoryInfoConsts.MinStatutoryAcctNoLength)]
        public string StatutoryAcctNo { get; set; }

        [Required]
        [StringLength(CompanyStatutoryInfoConsts.MaxBranchLength, MinimumLength = CompanyStatutoryInfoConsts.MinBranchLength)]
        public string Branch { get; set; }

        [Required]
        [StringLength(CompanyStatutoryInfoConsts.MaxStatutoryTypeLength, MinimumLength = CompanyStatutoryInfoConsts.MinStatutoryTypeLength)]
        public string StatutoryType { get; set; }
    }
}
