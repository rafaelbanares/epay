﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class ProcessStatusDto : EntityDto
    {
        public string DisplayName { get; set; }

        public int Step { get; set; }

        public string NextStatus { get; set; }

        public string Responsible { get; set; }

    }
}