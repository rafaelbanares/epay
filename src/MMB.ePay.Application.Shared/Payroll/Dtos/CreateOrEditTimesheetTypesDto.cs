﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditTimesheetTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(TimesheetTypesConsts.MaxDisplayNameLength, MinimumLength = TimesheetTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(TimesheetTypesConsts.MaxDescriptionLength, MinimumLength = TimesheetTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [StringLength(TimesheetTypesConsts.MaxSysCodeLength, MinimumLength = TimesheetTypesConsts.MinSysCodeLength)]
        public string SysCode { get; set; }

        [Required]
        public decimal Multiplier { get; set; }

        [Required]
        public bool Taxable { get; set; }

        [StringLength(TimesheetTypesConsts.MaxAcctCodeLength, MinimumLength = TimesheetTypesConsts.MinAcctCodeLength)]
        public string AcctCode { get; set; }

    }
}