﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetProcessForEditOutput
    {
        public CreateOrEditProcessDto Process { get; set; }
    }

    public class GetProcessForCreateOutput
    {
        public CreateOrEditProcessDto Process { get; set; }
    }
}