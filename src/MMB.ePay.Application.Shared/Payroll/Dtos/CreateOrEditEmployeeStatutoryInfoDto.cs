﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEmployeeStatutoryInfoDto : EntityDto<int?>
    {
        [Required]
        public string StatutoryType { get; set; }

        [Required]
        public string StatutoryAcctNo { get; set; }
    }
}