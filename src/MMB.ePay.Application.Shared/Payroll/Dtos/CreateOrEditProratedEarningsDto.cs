﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditProratedEarningsDto : EntityDto<int?>
    {
        [Required]
        public int? EarningTypeId { get; set; }

        public int? InTimesheetTypeId { get; set; }

        public int? InOvertimeTypeId { get; set; }

        [Required]
        public int? OutEarningTypeId { get; set; }
    }
}