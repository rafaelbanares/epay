﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class StatutoryTypesDto : EntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }

    }
}