﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditLeaveCreditsDto : EntityDto<int?>
    {

        [Required]
        public int AppYear { get; set; }

        [Required]
        public decimal LeaveDays { get; set; }

        [StringLength(LeaveCreditsConsts.MaxRemarksLength, MinimumLength = LeaveCreditsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int EmployeeId { get; set; }

        public int LeaveTypeId { get; set; }

    }
}