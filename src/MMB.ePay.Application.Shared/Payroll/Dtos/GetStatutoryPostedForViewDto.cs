﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetStatutoryPostedForViewDto
    {
        public StatutoryPostedDto StatutoryPosted { get; set; }

    }
}