﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllDeductionsInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeIdFilter { get; set; }
    }
}