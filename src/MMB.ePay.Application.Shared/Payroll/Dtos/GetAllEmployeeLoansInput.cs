﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEmployeeLoansInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string StatusIdFilter { get; set; }
        public int YearIdFilter { get; set; }
    }
}