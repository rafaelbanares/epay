﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEarningTypesForViewDto
    {
        public EarningTypesDto EarningTypes { get; set; }

    }
}