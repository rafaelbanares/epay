﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllReviewersInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string ReviewerTypeFilter { get; set; }

        public int? MaxReviewedByFilter { get; set; }
        public int? MinReviewedByFilter { get; set; }

    }
}