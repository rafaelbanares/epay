﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class SssTablesDto : EntityDto
    {
        public int MonthlySalCredit { get; set; }

        public decimal LB { get; set; }

        public decimal UB { get; set; }

        public decimal EmployerSSS { get; set; }

        public decimal EmployerECC { get; set; }

        public decimal EmployeeSSS { get; set; }

    }
}