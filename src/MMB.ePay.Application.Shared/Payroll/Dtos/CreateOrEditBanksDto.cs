﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditBanksDto : EntityDto<string>
    {
        [Required]
        [StringLength(BanksConsts.MaxDisplayNameLength, MinimumLength = BanksConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(BanksConsts.MaxDescriptionLength, MinimumLength = BanksConsts.MinDescriptionLength)]
        public string Description { get; set; }
    }
}