﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class StatutoryTmpDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public int StatutorySubTypeId { get; set; }

        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public Guid SessionId { get; set; }

    }
}