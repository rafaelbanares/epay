﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllPayPeriodInput : PagedAndSortedResultRequestDto
    {
        public string PayrollFreqFilter { get; set; }
        public int ApplicableYearFilter { get; set; }
    }

    public class GetPayPeriodForProcessInput
    {
        public string ProcessType { get; set; }
        public string PayrollFreq { get; set; }
        public int ApplicableYear { get; set; }
    }
}