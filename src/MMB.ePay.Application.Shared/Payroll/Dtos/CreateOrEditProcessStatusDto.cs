﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditProcessStatusDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ProcessStatusConsts.MaxDisplayNameLength, MinimumLength = ProcessStatusConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        public int Step { get; set; }

        [StringLength(ProcessStatusConsts.MaxNextStatusLength, MinimumLength = ProcessStatusConsts.MinNextStatusLength)]
        public string NextStatus { get; set; }

        [Required]
        [StringLength(ProcessStatusConsts.MaxResponsibleLength, MinimumLength = ProcessStatusConsts.MinResponsibleLength)]
        public string Responsible { get; set; }

    }
}