﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetContactPersonsForViewDto
    {
        public ContactPersonsDto ContactPersons { get; set; }

    }
}