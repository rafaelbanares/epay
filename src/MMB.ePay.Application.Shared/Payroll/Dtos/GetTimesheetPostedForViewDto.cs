﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetTimesheetPostedForViewDto
    {
        public TimesheetPostedDto TimesheetPosted { get; set; }

    }
}