﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllTenantBanksInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string BankAccountNumberFilter { get; set; }

        public string BankAccountTypeFilter { get; set; }

        public string BranchNumberFilter { get; set; }

        public string BankDiskTypeFilter { get; set; }

        public int? MaxBankBranchIdFilter { get; set; }
        public int? MinBankBranchIdFilter { get; set; }

        public string CorporateIdFilter { get; set; }

    }
}