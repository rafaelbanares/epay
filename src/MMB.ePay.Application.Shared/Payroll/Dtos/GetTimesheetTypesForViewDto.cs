﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetTimesheetTypesForViewDto
    {
        public TimesheetTypesDto TimesheetTypes { get; set; }

    }
}