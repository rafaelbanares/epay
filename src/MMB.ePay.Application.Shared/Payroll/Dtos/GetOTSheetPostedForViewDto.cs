﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetOTSheetPostedForViewDto
    {
        public OTSheetPostedDto OTSheetPosted { get; set; }

    }
}