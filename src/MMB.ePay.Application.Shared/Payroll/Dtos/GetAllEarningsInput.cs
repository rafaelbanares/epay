﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEarningsInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeIdFilter { get; set; }
    }
}