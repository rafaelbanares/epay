﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditPayrollPostedDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public decimal Salary { get; set; }

        [Required]
        public decimal DailyRate { get; set; }

        [Required]
        public decimal HourlyRate { get; set; }

        [Required]
        public decimal Timesheet { get; set; }

        [Required]
        public decimal Overtime { get; set; }

        [Required]
        public decimal TxEarningAmt { get; set; }

        [Required]
        public decimal NtxEarningAmt { get; set; }

        [Required]
        public decimal DedTaxAmt { get; set; }

        [Required]
        public decimal DeductionAmt { get; set; }

        [Required]
        public decimal LoanAmt { get; set; }

        [Required]
        public decimal BasicPay { get; set; }

        [Required]
        public decimal GrossPay { get; set; }

        [Required]
        public decimal NetPay { get; set; }

        public int EmployeeId { get; set; }

        public int PayCodeId { get; set; }

        public int BankBranchId { get; set; }

    }
}