﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllLoansInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string RefCodeFilter { get; set; }

        public string DisplayNameFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public string CurrencyFilter { get; set; }

        public string AcctCodeFilter { get; set; }

    }
}