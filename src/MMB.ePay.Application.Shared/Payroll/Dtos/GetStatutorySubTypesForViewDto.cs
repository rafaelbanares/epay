﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetStatutorySubTypesForViewDto
    {
        public StatutorySubTypesDto StatutorySubTypes { get; set; }

    }
}