﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class LeaveTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public bool WithPay { get; set; }

    }
}