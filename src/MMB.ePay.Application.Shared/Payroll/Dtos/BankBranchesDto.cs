﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class BankBranchesDto : EntityDto
    {
        public string BankId { get; set; }

        public string BranchCode { get; set; }

        public string BranchName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

    }
}