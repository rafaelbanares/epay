﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllBankBranchesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string BankId { get; set; }

        public string BankIdFilter { get; set; }

        public string BranchCodeFilter { get; set; }

        public string BranchNameFilter { get; set; }

        public string Address1Filter { get; set; }

        public string Address2Filter { get; set; }

    }
}