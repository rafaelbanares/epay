﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllOTSheetProcessInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public int? OvertimeTypeFilter { get; set; }

        public int? MaxCostCenterFilter { get; set; }
        public int? MinCostCenterFilter { get; set; }

        public decimal? MaxHrsFilter { get; set; }
        public decimal? MinHrsFilter { get; set; }

        public int? MaxMinsFilter { get; set; }
        public int? MinMinsFilter { get; set; }

        public decimal? MaxAmountFilter { get; set; }
        public decimal? MinAmountFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

    }
}