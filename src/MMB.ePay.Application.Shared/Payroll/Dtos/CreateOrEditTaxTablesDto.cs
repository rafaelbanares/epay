﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditTaxTablesDto : EntityDto<int?>
    {

        [Required]
        public int Year { get; set; }

        [Required]
        [StringLength(TaxTablesConsts.MaxPayFreqLength, MinimumLength = TaxTablesConsts.MinPayFreqLength)]
        public string PayFreq { get; set; }

        [Required]
        [StringLength(TaxTablesConsts.MaxCodeLength, MinimumLength = TaxTablesConsts.MinCodeLength)]
        public string Code { get; set; }

        [Required]
        public decimal? Bracket1 { get; set; }

        [Required]
        public decimal? Bracket2 { get; set; }

        [Required]
        public decimal? Bracket3 { get; set; }

        [Required]
        public decimal? Bracket4 { get; set; }

        [Required]
        public decimal? Bracket5 { get; set; }

        [Required]
        public decimal? Bracket6 { get; set; }

        [Required]
        public decimal? Bracket7 { get; set; }

    }
}