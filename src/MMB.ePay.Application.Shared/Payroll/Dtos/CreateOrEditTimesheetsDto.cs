﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditTimesheetsDto : EntityDto<int?>
    {
        public CreateOrEditTimesheetsDto()
        {
            Days = new List<decimal?>();
            Hrs = new List<decimal?>();
        }

        public int EmployeeId { get; set; }

        public IList<decimal?> Days { get; set; }

        public IList<decimal?> Hrs { get; set; }

        //public IList<int> Hrs { get; set; }

        //public ICollection<TimesheetValues> TimesheetValues { get; set; }

        //[Required]
        //public int PayrollNo { get; set; }

        //public int? CostCenter { get; set; }

        //[Required]
        //public decimal Days { get; set; }

        //[Required]
        //public decimal Hrs { get; set; }

        //[Required]
        //public int Mins { get; set; }

        //public int EmployeeId { get; set; }

        //public int TimesheetTypeId { get; set; }
    }

    //public class TimesheetValues
    //{
    //    [Required]
    //    public decimal? Days { get; set; }

    //    [Required]
    //    public decimal? Hrs { get; set; }
    //}
}