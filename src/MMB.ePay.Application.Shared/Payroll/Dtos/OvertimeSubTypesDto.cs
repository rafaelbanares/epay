﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class OvertimeSubTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }
    }
}