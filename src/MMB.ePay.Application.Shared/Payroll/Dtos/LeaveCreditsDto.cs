﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class LeaveCreditsDto : EntityDto
    {
        public int AppYear { get; set; }

        public decimal LeaveDays { get; set; }

        public string Remarks { get; set; }

        public int EmployeeId { get; set; }

        public int LeaveTypeId { get; set; }

    }
}