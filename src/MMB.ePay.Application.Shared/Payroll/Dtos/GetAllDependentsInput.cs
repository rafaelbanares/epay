﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllDependentsInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeFilterId { get; set; }
    }
}