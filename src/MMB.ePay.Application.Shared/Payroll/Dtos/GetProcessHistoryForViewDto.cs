﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetProcessHistoryForViewDto
    {
        public ProcessHistoryDto ProcessHistory { get; set; }

    }
}