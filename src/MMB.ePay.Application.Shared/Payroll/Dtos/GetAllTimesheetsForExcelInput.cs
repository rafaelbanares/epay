﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllTimesheetsForExcelInput
    {
        public string Filter { get; set; }

        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public int? MaxCostCenterFilter { get; set; }
        public int? MinCostCenterFilter { get; set; }

        public decimal? MaxDaysFilter { get; set; }
        public decimal? MinDaysFilter { get; set; }

        public decimal? MaxHrsFilter { get; set; }
        public decimal? MinHrsFilter { get; set; }

        public int? MaxMinsFilter { get; set; }
        public int? MinMinsFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxTimesheetTypeIdFilter { get; set; }
        public int? MinTimesheetTypeIdFilter { get; set; }

    }
}