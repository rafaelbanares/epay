﻿namespace MMB.ePay.Payroll.Dtos
{
    public class ReportsDto
    {
        //[Range(1, int.MaxValue, ErrorMessage = "The Report field is required.")]
        public int ReportId { get; set; }

        //[Required]
        public string PayCodeId { get; set; }

        //[Range(1, int.MaxValue, ErrorMessage = "The Employee field is required.")]
        public int EmployeeId { get; set; }

        public int? PayPeriodId { get; set; }

        public string EmployeeName { get; set; }

        //[Range(1, int.MaxValue, ErrorMessage = "The Year field is required.")]
        public int Year { get; set; }

        public int? Month { get; set; }

        public int PayrollNo { get; set; }

        public string Grouping { get; set; }

        public string SortOrder { get; set; }

        public string Output { get; set; }

        public bool IsLandscape { get; set; }

        public string Format { get; set; }

        public string ActionName { get; set; }

        public string PeriodName { get; set; }

        public string HeaderName { get; set; }
    }

    public class PeriodicReportHeaderDto
    {
        public string TenancyName { get; set; }
        public string ReportName { get; set; }
        public string Period { get; set; }
    }

    public class MonthlyReportHeaderDto
    {
        public string TenancyName { get; set; }
        public string ReportName { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
    }
}
