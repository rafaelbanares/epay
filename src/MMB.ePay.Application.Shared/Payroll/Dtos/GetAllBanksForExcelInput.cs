﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllBanksForExcelInput
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

    }
}