﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetSssTablesForViewDto
    {
        public SssTablesDto SssTables { get; set; }

    }
}