﻿using System;
using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetOvertimeTypesForViewDto
    {
        public GetOvertimeTypesForViewDto()
        {
            Group1 = new List<Tuple<string, string>>();
            Group2 = new List<string>();
            OvertimeTypes = new List<decimal>();
        }

        //public OvertimeTypesDto OvertimeTypes { get; set; }

        public IList<Tuple<string, string>> Group1 { get; set; }
        public IList<string> Group2 { get; set; }
        public IList<decimal> OvertimeTypes { get; set; }

    }
}