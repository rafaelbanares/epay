﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEarningPostedForViewDto
    {
        public EarningPostedDto EarningPosted { get; set; }

    }
}