﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllSssTablesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? MaxMonthlySalCreditFilter { get; set; }
        public int? MinMonthlySalCreditFilter { get; set; }

        public decimal? MaxLBFilter { get; set; }
        public decimal? MinLBFilter { get; set; }

        public decimal? MaxUBFilter { get; set; }
        public decimal? MinUBFilter { get; set; }

        public decimal? MaxEmployerSSSFilter { get; set; }
        public decimal? MinEmployerSSSFilter { get; set; }

        public decimal? MaxEmployerECCFilter { get; set; }
        public decimal? MinEmployerECCFilter { get; set; }

        public decimal? MaxEmployeeSSSFilter { get; set; }
        public decimal? MinEmployeeSSSFilter { get; set; }

    }
}