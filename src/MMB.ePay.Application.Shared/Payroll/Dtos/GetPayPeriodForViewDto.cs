﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayPeriodForViewDto
    {
        public PayPeriodDto PayPeriod { get; set; }

    }

    public class GetPayPeriodForProcessDto
    {
        public int Id { get; set; }
        public string PeriodRange { get; set; }
    }
}