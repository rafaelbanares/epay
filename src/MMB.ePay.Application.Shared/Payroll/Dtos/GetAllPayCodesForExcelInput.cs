﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllPayCodesForExcelInput
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public string PayFreqFilter { get; set; }

        public string TaxFreqFilter { get; set; }

        public string TaxMethodFilter { get; set; }

        public decimal? MaxWorkingDaysFilter { get; set; }
        public decimal? MinWorkingDaysFilter { get; set; }

        public decimal? MaxHrsPerDayFilter { get; set; }
        public decimal? MinHrsPerDayFilter { get; set; }

        public int? MaxPayPeriodPerYearFilter { get; set; }
        public int? MinPayPeriodPerYearFilter { get; set; }

        public int? MaxDaysPerWeekFilter { get; set; }
        public int? MinDaysPerWeekFilter { get; set; }

        public int? RequireTimesheetFilter { get; set; }

        public int? IncBonTaxDivFilter { get; set; }

        public int? TaxableOTFilter { get; set; }

        public decimal? MaxNd1RateFilter { get; set; }
        public decimal? MinNd1RateFilter { get; set; }

        public decimal? MaxNd2RateFilter { get; set; }
        public decimal? MinNd2RateFilter { get; set; }

        public int? MinimumWagerFilter { get; set; }

    }
}