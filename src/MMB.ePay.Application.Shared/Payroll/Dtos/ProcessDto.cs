﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class ProcessDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public string SysCode { get; set; }

    }
}