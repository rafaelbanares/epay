﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllPaySettingsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string KeyFilter { get; set; }

        public string ValueFilter { get; set; }

        public string DataTypeFilter { get; set; }

        public string CaptionFilter { get; set; }

        public int? MaxDisplayOrderFilter { get; set; }
        public int? MinDisplayOrderFilter { get; set; }

        public int? GroupSettingIdFilter { get; set; }
    }
}