﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class TimesheetProcessDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public int? CostCenter { get; set; }

        public decimal Days { get; set; }

        public decimal Hrs { get; set; }

        public int Mins { get; set; }

        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int TimesheetTypeId { get; set; }

    }
}