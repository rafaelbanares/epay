﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditContactPersonsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(ContactPersonsConsts.MaxFullNameLength, MinimumLength = ContactPersonsConsts.MinFullNameLength)]
        public string FullName { get; set; }

        [Required]
        [StringLength(ContactPersonsConsts.MaxContactNoLength, MinimumLength = ContactPersonsConsts.MinContactNoLength)]
        public string ContactNo { get; set; }

        [Required]
        [StringLength(ContactPersonsConsts.MaxPositionLength, MinimumLength = ContactPersonsConsts.MinPositionLength)]
        public string Position { get; set; }
    }
}
