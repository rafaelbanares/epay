﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditOvertimeMainTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(OvertimeMainTypesConsts.MaxDisplayNameLength, MinimumLength = OvertimeMainTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(OvertimeMainTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeMainTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public int DisplayOrder { get; set; }

    }
}