﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetStatutoryTypesForEditOutput
    {
        public CreateOrEditStatutoryTypesDto StatutoryTypes { get; set; }
    }
}