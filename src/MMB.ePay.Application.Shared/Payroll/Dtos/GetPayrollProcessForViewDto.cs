﻿using System;
using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayrollProcessForViewDto
    {
        public PayrollProcessDto PayrollProcess { get; set; }

        public bool IsReviewer { get; set; }
        public IList<ProcessInfoDto> MyProcesses { get; set; }
        public IList<ProcessInfoDto> MyReviews { get; set; }
        public IList<ProcessInfoDto> OtherProcesses { get; set; }
    }

    public class ProcessInfoDto
    {
        public int Id { get; set; }

        public int PayrollNo { get; set; }

        public string ProcessType { get; set; }

        public string Period { get; set; }

        public DateTime PayDate { get; set; }

        public string Status { get; set; }

        public string Owner { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}