﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllTaxTablesInput : PagedAndSortedResultRequestDto
    {
        public int YearFilter { get; set; }
        public string PayFreqFilter { get; set; }
    }
}