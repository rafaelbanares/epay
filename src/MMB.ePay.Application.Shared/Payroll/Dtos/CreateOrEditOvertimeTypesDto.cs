﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditOvertimeTypesDto : EntityDto<string>
    {

        [Required]
        [StringLength(OvertimeTypesConsts.MaxDisplayNameLength, MinimumLength = OvertimeTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(OvertimeTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public decimal Rate { get; set; }

    }
}