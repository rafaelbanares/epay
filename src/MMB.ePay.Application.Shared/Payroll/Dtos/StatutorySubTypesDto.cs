﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class StatutorySubTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

    }
}