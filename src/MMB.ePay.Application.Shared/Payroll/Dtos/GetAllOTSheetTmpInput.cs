﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllOTSheetTmpInput : PagedAndSortedResultRequestDto
    {
        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public int? OvertimeTypeFilter { get; set; }

        public int? MaxCostCenterFilter { get; set; }
        public int? MinCostCenterFilter { get; set; }

        public decimal? MaxHrsFilter { get; set; }
        public decimal? MinHrsFilter { get; set; }

        public int? MaxMinsFilter { get; set; }
        public int? MinMinsFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public Guid? SessionIdFilter { get; set; }

    }
}