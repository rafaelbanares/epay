﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class ContactPersonsDto : EntityDto
    {
        public string FullName { get; set; }

        public string ContactNo { get; set; }

        public string Position { get; set; }
    }
}
