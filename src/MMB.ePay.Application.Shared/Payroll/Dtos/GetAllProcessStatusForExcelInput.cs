﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllProcessStatusForExcelInput
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public int? MaxStepFilter { get; set; }
        public int? MinStepFilter { get; set; }

        public string NextStatusFilter { get; set; }

        public string ResponsibleFilter { get; set; }

    }
}