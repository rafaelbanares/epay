﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetContactPersonsForEditOutput
    {
        public CreateOrEditContactPersonsDto ContactPersons { get; set; }

    }
}