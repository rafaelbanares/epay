﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllTimesheetsInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeIdFilter { get; set; }
    }
}