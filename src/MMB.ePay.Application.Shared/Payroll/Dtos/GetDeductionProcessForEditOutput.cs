﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetDeductionProcessForEditOutput
    {
        public CreateOrEditDeductionProcessDto DeductionProcess { get; set; }

    }
}