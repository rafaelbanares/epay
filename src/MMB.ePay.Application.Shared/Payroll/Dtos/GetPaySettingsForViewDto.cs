﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetPaySettingsForViewDto
    {
        public PaySettingsDto PaySettings { get; set; }

    }
}