﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEmployeeLoansForViewDto
    {
        public EmployeeLoansDto EmployeeLoans { get; set; }

    }
}