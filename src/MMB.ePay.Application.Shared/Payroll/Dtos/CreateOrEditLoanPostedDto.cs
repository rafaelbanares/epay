﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditLoanPostedDto : EntityDto<int?>
    {

        public int? PayrollNo { get; set; }

        [Required]
        public DateTime PayDate { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [StringLength(LoanPostedConsts.MaxRemarksLength, MinimumLength = LoanPostedConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

    }
}