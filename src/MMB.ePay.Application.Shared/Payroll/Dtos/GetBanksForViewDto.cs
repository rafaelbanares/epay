﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetBanksForViewDto
    {
        public BanksDto Banks { get; set; }

    }
}