﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEmployersInput : PagedAndSortedResultRequestDto
    {
        public int EmployeeFilterId { get; set; }
    }
}