﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEarningTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(EarningTypesConsts.MaxDisplayNameLength, MinimumLength = EarningTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(EarningTypesConsts.MaxDescriptionLength, MinimumLength = EarningTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public bool Taxable { get; set; }

        [Required]
        [StringLength(EarningTypesConsts.MaxCurrencyLength, MinimumLength = EarningTypesConsts.MinCurrencyLength)]
        public string Currency { get; set; }

    }
}