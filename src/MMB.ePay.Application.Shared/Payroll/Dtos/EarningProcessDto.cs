﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class EarningProcessDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int EarningTypeId { get; set; }

        public string RecurEarningId { get; set; }

    }
}