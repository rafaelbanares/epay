﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEarningProcessForExcelInput
    {
        public string Filter { get; set; }

        public int? MaxPayrollNoFilter { get; set; }
        public int? MinPayrollNoFilter { get; set; }

        public decimal? MaxAmountFilter { get; set; }
        public decimal? MinAmountFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxEarningTypeIdFilter { get; set; }
        public int? MinEarningTypeIdFilter { get; set; }

        public string RecurEarningIdFilter { get; set; }

    }
}