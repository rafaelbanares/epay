﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class EmployersDto : EntityDto
    {
        public string EmployeeName { get; set; }

        public string EmployerName { get; set; }

        public DateTime EmployedFrom { get; set; }

        public DateTime? EmployedTo { get; set; }

        public string TIN { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public decimal BasicPay { get; set; }

        public decimal Deminimis { get; set; }

        public decimal GrossPay { get; set; }

        public decimal TxBonus { get; set; }

        public decimal TxOtherIncome { get; set; }

        public decimal NtxBonus { get; set; }

        public decimal NtxOtherIncome { get; set; }

        public decimal WTax { get; set; }

        public decimal SSS { get; set; }

        public decimal Philhealth { get; set; }

        public decimal Pagibig { get; set; }

        public int EmployeeId { get; set; }

    }
}