﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllEmployeeLoansForExcelInput
    {
        public string Filter { get; set; }

        public DateTime? MaxApprovedDateFilter { get; set; }
        public DateTime? MinApprovedDateFilter { get; set; }

        public DateTime? MaxEffectStartFilter { get; set; }
        public DateTime? MinEffectStartFilter { get; set; }

        public decimal? MaxPrincipalFilter { get; set; }
        public decimal? MinPrincipalFilter { get; set; }

        public decimal? MaxLoanAmountFilter { get; set; }
        public decimal? MinLoanAmountFilter { get; set; }

        public decimal? MaxAmortizationFilter { get; set; }
        public decimal? MinAmortizationFilter { get; set; }

        public string FrequencyFilter { get; set; }

        public string RemarksFilter { get; set; }

        public DateTime? MaxHoldStartFilter { get; set; }
        public DateTime? MinHoldStartFilter { get; set; }

        public DateTime? MaxHoldEndFilter { get; set; }
        public DateTime? MinHoldEndFilter { get; set; }

        public string StatusFilter { get; set; }

    }
}