﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditStatutoryTmpDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        [StringLength(StatutoryTmpConsts.MaxStatutoryCodeLength, MinimumLength = StatutoryTmpConsts.MinStatutoryCodeLength)]
        public string StatutoryCode { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public Guid SessionId { get; set; }

    }
}