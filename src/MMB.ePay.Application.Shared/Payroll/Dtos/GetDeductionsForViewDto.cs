﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetDeductionsForViewDto
    {
        public DeductionsDto Deductions { get; set; }

    }
}