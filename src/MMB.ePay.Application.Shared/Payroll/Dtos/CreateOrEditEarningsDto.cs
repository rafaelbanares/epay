﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEarningsDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public decimal? Amount { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "The Earning Type field is required.")]
        public int EarningTypeId { get; set; }

    }
}