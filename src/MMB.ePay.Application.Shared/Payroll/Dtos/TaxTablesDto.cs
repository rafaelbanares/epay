﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class TaxTablesDto : EntityDto
    {
        public int Year { get; set; }

        public string PayFreq { get; set; }

        public string Code { get; set; }

        public decimal Bracket1 { get; set; }

        public decimal Bracket2 { get; set; }

        public decimal Bracket3 { get; set; }

        public decimal Bracket4 { get; set; }

        public decimal Bracket5 { get; set; }

        public decimal Bracket6 { get; set; }

        public decimal Bracket7 { get; set; }

    }
}