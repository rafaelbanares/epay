﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllProcessStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public int? MaxStepFilter { get; set; }
        public int? MinStepFilter { get; set; }

        public int? NextStatusFilter { get; set; }

        public string ResponsibleFilter { get; set; }

    }
}