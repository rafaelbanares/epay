﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllPayPeriodForExcelInput
    {
        public string Filter { get; set; }

        public string PayrollFreqFilter { get; set; }

        public string ProcessTypeFilter { get; set; }

        public DateTime? MaxPeriodFromFilter { get; set; }
        public DateTime? MinPeriodFromFilter { get; set; }

        public DateTime? MaxPeriodToFilter { get; set; }
        public DateTime? MinPeriodToFilter { get; set; }

        public DateTime? MaxPayDateFilter { get; set; }
        public DateTime? MinPayDateFilter { get; set; }

        public int? MaxApplicableMonthFilter { get; set; }
        public int? MinApplicableMonthFilter { get; set; }

        public int? MaxApplicableYearFilter { get; set; }
        public int? MinApplicableYearFilter { get; set; }

        public int? MaxManDaysFilter { get; set; }
        public int? MinManDaysFilter { get; set; }

        public DateTime? MaxCutOffFromFilter { get; set; }
        public DateTime? MinCutOffFromFilter { get; set; }

        public DateTime? MaxCutOffToFilter { get; set; }
        public DateTime? MinCutOffToFilter { get; set; }

        public string CurrencyFilter { get; set; }

        public decimal? MaxExchangeRateFilter { get; set; }
        public decimal? MinExchangeRateFilter { get; set; }

        public int? MaxFiscalYearFilter { get; set; }
        public int? MinFiscalYearFilter { get; set; }

        public int? MaxPayrollWeekNoFilter { get; set; }
        public int? MinPayrollWeekNoFilter { get; set; }

        public string WeekNoFilter { get; set; }

        public string PayrollStatusFilter { get; set; }

        public string RemarksFilter { get; set; }

    }
}