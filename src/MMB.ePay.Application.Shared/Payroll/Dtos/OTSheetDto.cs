﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class OTSheetDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public string OTCode { get; set; }

        public int? CostCenter { get; set; }

        public decimal Hrs { get; set; }

        public int Mins { get; set; }

        public int EmployeeId { get; set; }

    }
}