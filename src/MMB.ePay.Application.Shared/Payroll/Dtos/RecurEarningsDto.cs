﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class RecurEarningsDto : EntityDto
    {
        public string EarningType { get; set; }

        public decimal Amount { get; set; }

        public string Frequency { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int EmployeeId { get; set; }

        public int EarningTypeId { get; set; }
    }
}