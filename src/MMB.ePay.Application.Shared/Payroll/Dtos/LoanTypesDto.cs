﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class LoanTypesDto : EntityDto
    {
        public string SysCode { get; set; }

        public string Description { get; set; }

    }
}