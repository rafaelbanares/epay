﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class StatutoryProcessDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public string StatutoryType { get; set; }

        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

    }
}