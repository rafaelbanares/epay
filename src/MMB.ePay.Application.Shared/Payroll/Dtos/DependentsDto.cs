﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class DependentsDto : EntityDto
    {
        public string DependentName { get; set; }

        public DateTime BirthDate { get; set; }

        public int EmployeeId { get; set; }

    }
}