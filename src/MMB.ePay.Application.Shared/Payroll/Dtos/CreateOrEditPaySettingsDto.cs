﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditPaySettingsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(PaySettingsConsts.MaxKeyLength, MinimumLength = PaySettingsConsts.MinKeyLength)]
        public string Key { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxValueLength, MinimumLength = PaySettingsConsts.MinValueLength)]
        public string Value { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxDataTypeLength, MinimumLength = PaySettingsConsts.MinDataTypeLength)]
        public string DataType { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxCaptionLength, MinimumLength = PaySettingsConsts.MinCaptionLength)]
        public string Caption { get; set; }

        public int? DisplayOrder { get; set; }

        [Required]
        public int? GroupSettingId { get; set; }
    }
}