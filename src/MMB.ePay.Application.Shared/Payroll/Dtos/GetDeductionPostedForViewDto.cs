﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetDeductionPostedForViewDto
    {
        public DeductionPostedDto DeductionPosted { get; set; }

    }
}