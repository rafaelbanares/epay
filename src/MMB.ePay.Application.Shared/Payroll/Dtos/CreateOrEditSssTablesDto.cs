﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditSssTablesDto : EntityDto<int?>
    {

        [Required]
        public int MonthlySalCredit { get; set; }

        [Required]
        public decimal LB { get; set; }

        [Required]
        public decimal UB { get; set; }

        [Required]
        public decimal EmployerSSS { get; set; }

        [Required]
        public decimal EmployerECC { get; set; }

        [Required]
        public decimal EmployeeSSS { get; set; }

    }
}