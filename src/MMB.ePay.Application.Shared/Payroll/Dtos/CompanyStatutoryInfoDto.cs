﻿namespace MMB.ePay.Payroll.Dtos
{
    public class CompanyStatutoryInfoDto
    {
        public string Id { get; set; }

        public string SignatoryName { get; set; }

        public string StatutoryAcctNo { get; set; }

        public string Branch { get; set; }

        public string StatutoryType { get; set; }
    }
}
