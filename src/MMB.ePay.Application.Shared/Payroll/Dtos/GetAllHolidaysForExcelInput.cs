﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllHolidaysForExcelInput
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public DateTime? MaxDateFilter { get; set; }
        public DateTime? MinDateFilter { get; set; }

        public int? HalfDayFilter { get; set; }

        public string HolidayTypeFilter { get; set; }

    }
}