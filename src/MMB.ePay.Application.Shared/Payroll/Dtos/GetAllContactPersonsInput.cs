﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllContactPersonsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
