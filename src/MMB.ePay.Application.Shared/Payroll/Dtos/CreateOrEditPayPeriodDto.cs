﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditPayPeriodDto : EntityDto<int?>
    {

        [Required]
        [StringLength(PayPeriodConsts.MaxPayrollFreqLength, MinimumLength = PayPeriodConsts.MinPayrollFreqLength)]
        public string PayrollFreq { get; set; }

        [Required]
        [StringLength(PayPeriodConsts.MaxProcessTypeLength, MinimumLength = PayPeriodConsts.MinProcessTypeLength)]
        public string ProcessType { get; set; }

        [Required]
        public DateTime? PeriodFrom { get; set; }

        [Required]
        public DateTime? PeriodTo { get; set; }

        [Required]
        public DateTime? PayDate { get; set; }

        [Required]
        public int ApplicableMonth { get; set; }

        [Required]
        public int? ApplicableYear { get; set; }

        public int? ManDays { get; set; }

        [Required]
        public DateTime? CutOffFrom { get; set; }

        [Required]
        public DateTime? CutOffTo { get; set; }

        [StringLength(PayPeriodConsts.MaxCurrencyLength, MinimumLength = PayPeriodConsts.MinCurrencyLength)]
        public string Currency { get; set; }

        public decimal? ExchangeRate { get; set; }

        [Required]
        public int? FiscalYear { get; set; }

        public int? PayrollWeekNo { get; set; }

        [Required]
        [StringLength(PayPeriodConsts.MaxWeekNoLength, MinimumLength = PayPeriodConsts.MinWeekNoLength)]
        public string WeekNo { get; set; }

        [StringLength(PayPeriodConsts.MaxPayrollStatusLength, MinimumLength = PayPeriodConsts.MinPayrollStatusLength)]
        public string PayrollStatus { get; set; }

        [StringLength(PayPeriodConsts.MaxRemarksLength, MinimumLength = PayPeriodConsts.MinRemarksLength)]
        public string Remarks { get; set; }

    }
}