﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditLoanTmpDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int EmployeeLoanId { get; set; }

        public Guid SessionId { get; set; }

    }
}