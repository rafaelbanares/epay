﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllLeaveCreditsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? MaxAppYearFilter { get; set; }
        public int? MinAppYearFilter { get; set; }

        public decimal? MaxLeaveDaysFilter { get; set; }
        public decimal? MinLeaveDaysFilter { get; set; }

        public string RemarksFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxLeaveTypeIdFilter { get; set; }
        public int? MinLeaveTypeIdFilter { get; set; }

    }
}