﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllTimesheetTypesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public string SysCodeFilter { get; set; }

        public int? MaxDisplayOrderFilter { get; set; }
        public int? MinDisplayOrderFilter { get; set; }

        public decimal? MaxMultiplierFilter { get; set; }
        public decimal? MinMultiplierFilter { get; set; }

        public int? TaxableFilter { get; set; }

        public string AcctCodeFilter { get; set; }

        public int? ActiveFilter { get; set; }

        public string CategoryFilter { get; set; }

    }
}