﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class LoanPostedDto : EntityDto
    {
        public int? PayrollNo { get; set; }

        public DateTime PayDate { get; set; }

        public decimal Amount { get; set; }

        public string Remarks { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}