﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetAllCompanyStatutoryInfoInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
