﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class RanksDto : EntityDto<int>
    {
        public string Description { get; set; }
    }
}