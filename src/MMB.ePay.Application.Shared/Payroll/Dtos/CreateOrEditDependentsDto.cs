﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditDependentsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(DependentsConsts.MaxDependentNameLength, MinimumLength = DependentsConsts.MinDependentNameLength)]
        public string DependentName { get; set; }

        [Required]
        public DateTime? BirthDate { get; set; }

        [Required]
        public int EmployeeId { get; set; }

    }
}