﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetPayCodesForViewDto
    {
        public PayCodesDto PayCodes { get; set; }

    }
}