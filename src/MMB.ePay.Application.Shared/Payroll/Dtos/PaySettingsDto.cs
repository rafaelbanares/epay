﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class PaySettingsDto : EntityDto
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string DataType { get; set; }

        public string Caption { get; set; }

        public int? DisplayOrder { get; set; }

        public string GroupSetting { get; set; }

    }
}