﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEarningTypeSettingsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(EarningTypeSettingsConsts.MaxKeyLength, MinimumLength = EarningTypeSettingsConsts.MinKeyLength)]
        public string Key { get; set; }

        [Required]
        [StringLength(EarningTypeSettingsConsts.MaxValueLength, MinimumLength = EarningTypeSettingsConsts.MinValueLength)]
        public string Value { get; set; }

        public int EarningTypeId { get; set; }

    }
}