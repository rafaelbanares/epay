﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class PayCodesDto : EntityDto<Guid>
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string PayFreq { get; set; }

        public string TaxFreq { get; set; }

        public string TaxMethod { get; set; }

        public decimal WorkingDays { get; set; }

        public decimal HrsPerDay { get; set; }

        public int PayPeriodPerYear { get; set; }

        public int? DaysPerWeek { get; set; }

        public bool RequireTimesheet { get; set; }

        public bool IncBonTaxDiv { get; set; }

        public bool TaxableOT { get; set; }

        public decimal? Nd1Rate { get; set; }

        public decimal? Nd2Rate { get; set; }

        public bool MinimumWager { get; set; }

    }
}