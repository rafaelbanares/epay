﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class ProratedEarningsDto : EntityDto
    {
        public string EarningType { get; set; }
        public string InTimesheet { get; set; }
        public string InOvertimeType { get; set; }
        public string OutEarningType { get; set; }
    }
}