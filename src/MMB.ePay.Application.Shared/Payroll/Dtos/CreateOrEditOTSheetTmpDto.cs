﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditOTSheetTmpDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public int? OvertimeTypeId { get; set; }

        public int? CostCenter { get; set; }

        [Required]
        public decimal Hrs { get; set; }

        [Required]
        public int Mins { get; set; }

        public int EmployeeId { get; set; }

        public Guid SessionId { get; set; }

    }
}