﻿using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetOTSheetForViewDto
    {
        public GetOTSheetForViewDto()
        {
            Columns = new List<string>();
            Data = new List<string>();
        }

        public OTSheetDto OTSheet { get; set; }

        public IList<string> Columns { get; set; }

        public IList<string> Data { get; set; }
    }
}