﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditLoanTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(LoanTypesConsts.MaxSysCodeLength, MinimumLength = LoanTypesConsts.MinSysCodeLength)]
        public string SysCode { get; set; }

        [Required]
        [StringLength(LoanTypesConsts.MaxDescriptionLength, MinimumLength = LoanTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}