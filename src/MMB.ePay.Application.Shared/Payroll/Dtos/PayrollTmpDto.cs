﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class PayrollTmpDto : EntityDto
    {
        public int PayrollNo { get; set; }

        public decimal Salary { get; set; }

        public decimal DailyRate { get; set; }

        public decimal HourlyRate { get; set; }

        public decimal Timesheet { get; set; }

        public decimal Overtime { get; set; }

        public decimal TxEarningAmt { get; set; }

        public decimal NtxEarningAmt { get; set; }

        public decimal DedTaxAmt { get; set; }

        public decimal DeductionAmt { get; set; }

        public decimal LoanAmt { get; set; }

        public decimal BasicPay { get; set; }

        public decimal GrossPay { get; set; }

        public decimal NetPay { get; set; }

        public int EmployeeId { get; set; }

        public int PayCodeId { get; set; }

        public int BankBranchId { get; set; }

        public Guid SessionId { get; set; }

    }
}