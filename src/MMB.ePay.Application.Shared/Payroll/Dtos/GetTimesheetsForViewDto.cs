﻿using System.Collections.Generic;

namespace MMB.ePay.Payroll.Dtos
{
    public class GetTimesheetsForViewDto
    {
        public TimesheetsDto Timesheets { get; set; }

        public IList<decimal> Days { get; set; }
        public IList<decimal> Hrs { get; set; }

    }
}