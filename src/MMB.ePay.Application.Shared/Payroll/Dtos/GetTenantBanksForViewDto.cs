﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetTenantBanksForViewDto
    {
        public TenantBanksDto TenantBanks { get; set; }

    }
}