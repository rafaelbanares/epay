﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditReviewersDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ReviewersConsts.MaxReviewerTypeLength, MinimumLength = ReviewersConsts.MinReviewerTypeLength)]
        public string ReviewerType { get; set; }

        [Required]
        public int ReviewedBy { get; set; }

    }
}