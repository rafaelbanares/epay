﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditOvertimeSubTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(OvertimeSubTypesConsts.MaxDisplayNameLength, MinimumLength = OvertimeSubTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(OvertimeSubTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeSubTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public int DisplayOrder { get; set; }

    }
}