﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditPayCodesDto : EntityDto<Guid?>
    {
        [Required]
        [StringLength(PayCodesConsts.MaxDisplayNameLength, MinimumLength = PayCodesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(PayCodesConsts.MaxDescriptionLength, MinimumLength = PayCodesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required(ErrorMessage = "The Pay Frequency field is required.")]
        public string PayFreq { get; set; }

        public string TaxFreq { get; set; }

        [Required(ErrorMessage = "The Tax Method field is required.")]
        public string TaxMethod { get; set; }

        [Required(ErrorMessage = "The Working Days field is required.")]
        public decimal? WorkingDays { get; set; }

        [Required(ErrorMessage = "The Hours per Day field is required.")]
        public decimal? HrsPerDay { get; set; }

        [Required(ErrorMessage = "The Pay Period per Year field is required.")]
        public int? PayPeriodPerYear { get; set; }

        public int? DaysPerWeek { get; set; }

        [Required]
        public bool RequireTimesheet { get; set; }

        [Required]
        public bool IncBonTaxDiv { get; set; }

        [Required]
        public bool TaxableOT { get; set; }

        public decimal? Nd1Rate { get; set; }

        public decimal? Nd2Rate { get; set; }

        [Required]
        public bool MinimumWager { get; set; }

    }
}