﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetProratedEarningsForViewDto
    {
        public ProratedEarningsDto ProratedEarnings { get; set; }

    }

    public class GetProratedEarningsForProcessDto
    {
        public int Id { get; set; }
        public string PeriodRange { get; set; }
    }
}