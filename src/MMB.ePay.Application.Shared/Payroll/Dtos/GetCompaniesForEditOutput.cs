﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetCompaniesForEditOutput
    {
        public CreateOrEditCompaniesDto Companies { get; set; }
    }
}