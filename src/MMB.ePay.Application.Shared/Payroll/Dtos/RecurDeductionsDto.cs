﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class RecurDeductionsDto : EntityDto
    {
        public string DeductionType { get; set; }

        public decimal Amount { get; set; }

        public string Frequency { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int EmployeeId { get; set; }

        public int DeductionTypeId { get; set; }

    }
}