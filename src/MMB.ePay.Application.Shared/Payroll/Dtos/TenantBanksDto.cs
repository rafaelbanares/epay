﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Payroll.Dtos
{
    public class TenantBanksDto : EntityDto
    {
        public string BranchName { get; set; }

        public string BankAccountNumber { get; set; }

        public string BankAccountType { get; set; }

        public string BranchNumber { get; set; }

        public string BankBranch { get; set; }

        public string CorporateId { get; set; }
    }
}