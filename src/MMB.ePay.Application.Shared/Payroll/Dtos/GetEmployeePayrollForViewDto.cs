﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetEmployeePayrollForViewDto
    {
        public EmployeePayrollDto EmployeePayroll { get; set; }

    }
}