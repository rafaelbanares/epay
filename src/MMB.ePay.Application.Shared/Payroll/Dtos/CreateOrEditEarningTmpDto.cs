﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Payroll.Dtos
{
    public class CreateOrEditEarningTmpDto : EntityDto<int?>
    {

        [Required]
        public int PayrollNo { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int EmployeeId { get; set; }

        public int EarningTypeId { get; set; }

        public int RecurEarningId { get; set; }

        public Guid SessionId { get; set; }

    }
}