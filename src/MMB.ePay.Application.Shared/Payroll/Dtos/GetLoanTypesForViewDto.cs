﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetLoanTypesForViewDto
    {
        public LoanTypesDto LoanTypes { get; set; }

    }
}