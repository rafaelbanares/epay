﻿namespace MMB.ePay.Payroll.Dtos
{
    public class GetTaxTablesForViewDto
    {
        public TaxTablesDto TaxTables { get; set; }

    }
}