﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IStatutoryPostedAppService : IApplicationService
    {
        Task<PagedResultDto<GetStatutoryPostedForViewDto>> GetAll(GetAllStatutoryPostedInput input);

        Task<GetStatutoryPostedForViewDto> GetStatutoryPostedForView(int id);

        Task<GetStatutoryPostedForEditOutput> GetStatutoryPostedForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStatutoryPostedDto input);

        Task Delete(EntityDto input);
    }
}