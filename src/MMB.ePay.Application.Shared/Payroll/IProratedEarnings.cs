﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IProratedEarningsAppService : IApplicationService
    {
        Task<PagedResultDto<GetProratedEarningsForViewDto>> GetAll(GetAllProratedEarningsInput input);

        Task<GetProratedEarningsForViewDto> GetProratedEarningsForView(int id);

        Task<GetProratedEarningsForEditOutput> GetProratedEarningsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProratedEarningsDto input);
    }
}