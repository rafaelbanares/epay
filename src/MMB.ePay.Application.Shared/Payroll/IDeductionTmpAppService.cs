﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public interface IDeductionTmpAppService : IApplicationService
    {
        Task<PagedResultDto<GetDeductionTmpForViewDto>> GetAll(GetAllDeductionTmpInput input);

        Task<GetDeductionTmpForViewDto> GetDeductionTmpForView(int id);

        Task<GetDeductionTmpForEditOutput> GetDeductionTmpForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDeductionTmpDto input);

        Task Delete(EntityDto input);
    }
}