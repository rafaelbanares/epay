﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    public interface IEmployeesAppService : IApplicationService
    {
        IList<SelectListItem> GetEmployeeStatusList();

        IList<SelectListItem> GetCurrencyList();

        Task<IList<SelectListItem>> GetEmployeeUserList();

        Task<IList<SelectListItem>> GetEmployeeList();

        Task<IList<SelectListItem>> GetEmployeeFilterList();

        Task<PagedResultDto<GetEmployeesForViewDto>> GetAll(GetAllEmployeesInput input);

        Task<GetEmployeesForViewDto> GetEmployeesForView(int id);

        Task<GetEmployeesForEditOutput> GetEmployeesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmployeesDto input);

        Task Delete(EntityDto input);
    }
}