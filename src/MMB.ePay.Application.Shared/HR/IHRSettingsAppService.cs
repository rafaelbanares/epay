﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.HR.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    public interface IHRSettingsAppService : IApplicationService
    {
        Task<PagedResultDto<GetHRSettingsForViewDto>> GetAll(GetAllHRSettingsInput input);

        Task<GetHRSettingsForViewDto> GetHRSettingsForView(int id);

        Task<GetHRSettingsForEditOutput> GetHRSettingsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditHRSettingsDto input);

        Task Delete(EntityDto input);
    }
}