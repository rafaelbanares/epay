﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.HR.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    public interface IGroupSettingsAppService : IApplicationService
    {
        Task<PagedResultDto<GetGroupSettingsForViewDto>> GetAll(GetAllGroupSettingsInput input);

        Task<GetGroupSettingsForViewDto> GetGroupSettingsForView(int id);

        Task<GetGroupSettingsForEditOutput> GetGroupSettingsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditGroupSettingsDto input);

        Task Delete(EntityDto input);
    }
}