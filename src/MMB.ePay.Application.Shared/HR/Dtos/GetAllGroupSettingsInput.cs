﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.HR.Dtos
{
    public class GetAllGroupSettingsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public short? MaxOrderNoFilter { get; set; }
        public short? MinOrderNoFilter { get; set; }

    }
}