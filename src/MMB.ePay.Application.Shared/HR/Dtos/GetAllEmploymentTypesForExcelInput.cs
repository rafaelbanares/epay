﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.HR.Dtos
{
    public class GetAllEmploymentTypesForExcelInput
    {
        public string Filter { get; set; }

        public string DescriptionFilter { get; set; }

    }
}