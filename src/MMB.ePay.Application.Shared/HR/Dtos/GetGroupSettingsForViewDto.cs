﻿namespace MMB.ePay.HR.Dtos
{
    public class GetGroupSettingsForViewDto
    {
        public GroupSettingsDto GroupSettings { get; set; }

    }
}