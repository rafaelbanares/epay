﻿namespace MMB.ePay.HR.Dtos
{
    public class GetEmploymentTypesForViewDto
    {
        public EmploymentTypesDto EmploymentTypes { get; set; }

    }
}