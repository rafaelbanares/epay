﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class CreateOrEditEmploymentTypesDto : EntityDto<string>
    {

        [Required]
        [StringLength(EmploymentTypesConsts.MaxDescriptionLength, MinimumLength = EmploymentTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}