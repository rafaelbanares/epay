﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class CreateOrEditHRSettingsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(HRSettingsConsts.MaxKeyLength, MinimumLength = HRSettingsConsts.MinKeyLength)]
        public string Key { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxValueLength, MinimumLength = HRSettingsConsts.MinValueLength)]
        public string Value { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxDataTypeLength, MinimumLength = HRSettingsConsts.MinDataTypeLength)]
        public string DataType { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxCaptionLength, MinimumLength = HRSettingsConsts.MinCaptionLength)]
        public string Caption { get; set; }

        [StringLength(HRSettingsConsts.MaxDescriptionLength, MinimumLength = HRSettingsConsts.MinDescriptionLength)]
        public string Description { get; set; }

        public int? DisplayOrder { get; set; }

        [Required]
        public int GroupSettingId { get; set; }

    }
}