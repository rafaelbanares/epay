﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class GetEmployeesForEditOutput
    {
        public CreateOrEditEmployeesDto Employees { get; set; }

    }
}