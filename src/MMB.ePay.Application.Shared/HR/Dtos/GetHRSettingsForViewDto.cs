﻿namespace MMB.ePay.HR.Dtos
{
    public class GetHRSettingsForViewDto
    {
        public HRSettingsDto HRSettings { get; set; }

    }
}