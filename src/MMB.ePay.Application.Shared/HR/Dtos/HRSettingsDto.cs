﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class HRSettingsDto : EntityDto
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string DataType { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public int? DisplayOrder { get; set; }

        public string GroupSetting { get; set; }

    }
}