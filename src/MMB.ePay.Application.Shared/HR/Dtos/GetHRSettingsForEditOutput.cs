﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class GetHRSettingsForEditOutput
    {
        public CreateOrEditHRSettingsDto HRSettings { get; set; }

    }
}