﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MMB.ePay.HR.Dtos
{
    public class CreateOrEditEmployeesDto : EntityDto<int?>
    {
        public CreateOrEditEmployeesDto()
        {
            StatutoryTypes = new List<string>();
        }

        [Required]
        [StringLength(EmployeesConsts.MaxEmployeeCodeLength, MinimumLength = EmployeesConsts.MinEmployeeCodeLength)]
        public string EmployeeCode { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxLastNameLength, MinimumLength = EmployeesConsts.MinLastNameLength)]
        public string LastName { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxFirstNameLength, MinimumLength = EmployeesConsts.MinFirstNameLength)]
        public string FirstName { get; set; }

        [StringLength(EmployeesConsts.MaxMiddleNameLength, MinimumLength = EmployeesConsts.MinMiddleNameLength)]
        public string MiddleName { get; set; }

        [StringLength(EmployeesConsts.MaxEmailLength, MinimumLength = EmployeesConsts.MinEmailLength)]
        public string Email { get; set; }

        [StringLength(EmployeesConsts.MaxAddressLength, MinimumLength = EmployeesConsts.MinAddressLength)]
        public string Address { get; set; }

        [StringLength(EmployeesConsts.MaxPositionLength, MinimumLength = EmployeesConsts.MinPositionLength)]
        public string Position { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? DateEmployed { get; set; }

        public DateTime? DateTerminated { get; set; }

        [StringLength(EmployeesConsts.MaxGenderLength, MinimumLength = EmployeesConsts.MinGenderLength)]
        public string Gender { get; set; }

        [StringLength(EmployeesConsts.MaxEmployeeStatusLength, MinimumLength = EmployeesConsts.MinEmployeeStatusLength)]
        public string EmployeeStatus { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmploymentTypeIdLength, MinimumLength = EmployeesConsts.MinEmploymentTypeIdLength)]
        public string EmploymentTypeId { get; set; }

        public IList<string> StatutoryTypes { get; set; }
    }
}