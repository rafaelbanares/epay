﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class GetAllEmployeesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}