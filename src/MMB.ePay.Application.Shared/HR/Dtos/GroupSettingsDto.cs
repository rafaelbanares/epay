﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class GroupSettingsDto : EntityDto
    {
        public string DisplayName { get; set; }

        public short OrderNo { get; set; }

    }
}