﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    public interface IEmploymentTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetEmploymentTypeList();

        Task<PagedResultDto<GetEmploymentTypesForViewDto>> GetAll(GetAllEmploymentTypesInput input);

        Task<GetEmploymentTypesForViewDto> GetEmploymentTypesForView(string id);

        Task<GetEmploymentTypesForEditOutput> GetEmploymentTypesForEdit(EntityDto<string> input);

        Task CreateOrEdit(CreateOrEditEmploymentTypesDto input);

        Task Delete(EntityDto<string> input);
    }
}