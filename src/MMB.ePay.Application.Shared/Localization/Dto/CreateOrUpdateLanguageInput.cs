﻿using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}