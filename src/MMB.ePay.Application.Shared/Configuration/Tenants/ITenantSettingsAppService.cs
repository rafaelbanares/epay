﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MMB.ePay.Configuration.Tenants.Dto;

namespace MMB.ePay.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
