﻿using Abp.Auditing;
using MMB.ePay.Configuration.Dto;

namespace MMB.ePay.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}