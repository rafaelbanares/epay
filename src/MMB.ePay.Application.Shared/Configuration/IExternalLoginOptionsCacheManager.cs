﻿namespace MMB.ePay.Configuration
{
    public interface IExternalLoginOptionsCacheManager
    {
        void ClearCache();
    }
}
