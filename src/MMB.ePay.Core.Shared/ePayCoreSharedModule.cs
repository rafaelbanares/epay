﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MMB.ePay
{
    public class ePayCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayCoreSharedModule).GetAssembly());
        }
    }
}