﻿namespace MMB.ePay.Payroll
{
    public class ReviewersConsts
    {

        public const int MinReviewerTypeLength = 0;
        public const int MaxReviewerTypeLength = 20;

    }
}