﻿namespace MMB.ePay.Payroll
{
    public class BankBranchesConsts
    {

        public const int MinBankIdLength = 0;
        public const int MaxBankIdLength = 10;

        public const int MinBranchCodeLength = 0;
        public const int MaxBranchCodeLength = 30;

        public const int MinBranchNameLength = 0;
        public const int MaxBranchNameLength = 50;

        public const int MinAddress1Length = 0;
        public const int MaxAddress1Length = 100;

        public const int MinAddress2Length = 0;
        public const int MaxAddress2Length = 100;

    }
}