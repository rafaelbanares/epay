﻿namespace MMB.ePay.Payroll
{
    public class TimesheetTypesConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

        public const int MinSysCodeLength = 0;
        public const int MaxSysCodeLength = 10;

        public const int MinAcctCodeLength = 0;
        public const int MaxAcctCodeLength = 20;

        public const int MinCategoryLength = 0;
        public const int MaxCategoryLength = 10;

    }
}