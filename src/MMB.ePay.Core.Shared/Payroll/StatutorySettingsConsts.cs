﻿namespace MMB.ePay.Payroll
{
    public class StatutorySettingsConsts
    {
        public const int MinStatutoryTypeLength = 0;
        public const int MaxStatutoryTypeLength = 10;

        public const int MinBasisLength = 0;
        public const int MaxBasisLength = 50;

        public const int MinFrequencyLength = 0;
        public const int MaxFrequencyLength = 5;
    }
}