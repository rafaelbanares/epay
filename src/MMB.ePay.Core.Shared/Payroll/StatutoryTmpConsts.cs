﻿namespace MMB.ePay.Payroll
{
    public class StatutoryTmpConsts
    {

        public const int MinStatutoryCodeLength = 0;
        public const int MaxStatutoryCodeLength = 10;

    }
}