﻿namespace MMB.ePay.Payroll
{
    public class CompanyStatutoryInfoConsts
    {
        public const int MinStatutoryAcctNoLength = 0;
        public const int MaxStatutoryAcctNoLength = 50;

        public const int MinBranchLength = 0;
        public const int MaxBranchLength = 30;

        public const int MinStatutoryTypeLength = 0;
        public const int MaxStatutoryTypeLength = 10;
    }
}