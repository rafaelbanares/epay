﻿namespace MMB.ePay.Payroll
{
    public class HolidaysConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinHolidayTypeIdLength = 0;
        public const int MaxHolidayTypeIdLength = 2;

    }
}