﻿namespace MMB.ePay.Payroll
{
    public class LoanTypesConsts
    {

        public const int MinSysCodeLength = 0;
        public const int MaxSysCodeLength = 10;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 50;

    }
}