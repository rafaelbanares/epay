﻿namespace MMB.ePay.Payroll
{
    public class EmnployeeStatutoryInfoConsts
    {
        public const int MinStatutoryTypeLength = 0;
        public const int MaxStatutoryTypeLength = 10;

        public const int MinStatutoryAcctNoLength = 0;
        public const int MaxStatutoryAcctNoLength = 50;
    }
}