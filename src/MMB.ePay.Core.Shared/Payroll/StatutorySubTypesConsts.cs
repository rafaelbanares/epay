﻿namespace MMB.ePay.Payroll
{
    public class StatutorySubTypesConsts
    {
        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinSysCodeLength = 0;
        public const int MaxSysCodeLength = 10;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;
    }
}