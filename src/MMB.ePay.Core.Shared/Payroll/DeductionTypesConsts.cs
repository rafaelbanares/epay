﻿namespace MMB.ePay.Payroll
{
    public class DeductionTypesConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

        public const int MinCurrencyLength = 0;
        public const int MaxCurrencyLength = 3;

    }
}