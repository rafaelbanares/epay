﻿namespace MMB.ePay.Payroll
{
    public class SignatoriesConsts
    {
        public const int MinSignatoryNameLength = 0;
        public const int MaxSignatoryNameLength = 50;

        public const int MinPositionLength = 0;
        public const int MaxPositionLength = 100;
    }
}