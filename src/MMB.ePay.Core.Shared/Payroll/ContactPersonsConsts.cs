﻿namespace MMB.ePay.Payroll
{
    public class ContactPersonsConsts
    {
        public const int MinFullNameLength = 0;
        public const int MaxFullNameLength = 100;

        public const int MinContactNoLength = 0;
        public const int MaxContactNoLength = 50;

        public const int MinPositionLength = 0;
        public const int MaxPositionLength = 50;
    }
}