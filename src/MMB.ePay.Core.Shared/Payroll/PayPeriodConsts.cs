﻿namespace MMB.ePay.Payroll
{
    public class PayPeriodConsts
    {

        public const int MinPayrollFreqLength = 0;
        public const int MaxPayrollFreqLength = 1;

        public const int MinProcessTypeLength = 0;
        public const int MaxProcessTypeLength = 20;

        public const int MinCurrencyLength = 0;
        public const int MaxCurrencyLength = 3;

        public const int MinWeekNoLength = 0;
        public const int MaxWeekNoLength = 1;

        public const int MinPayrollStatusLength = 0;
        public const int MaxPayrollStatusLength = 10;

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

    }
}