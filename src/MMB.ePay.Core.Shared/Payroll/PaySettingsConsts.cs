﻿namespace MMB.ePay.Payroll
{
    public class PaySettingsConsts
    {

        public const int MinKeyLength = 0;
        public const int MaxKeyLength = 50;

        public const int MinValueLength = 0;
        public const int MaxValueLength = 500;

        public const int MinDataTypeLength = 0;
        public const int MaxDataTypeLength = 20;

        public const int MinCaptionLength = 0;
        public const int MaxCaptionLength = 200;
    }
}