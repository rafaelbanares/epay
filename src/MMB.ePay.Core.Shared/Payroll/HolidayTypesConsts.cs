﻿namespace MMB.ePay.Payroll
{
    public class HolidayTypesConsts
    {
        public const int MinIdLength = 0;
        public const int MaxIdLength = 2;

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 50;
    }
}