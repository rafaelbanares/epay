﻿namespace MMB.ePay.Payroll
{
    public class EmployersConsts
    {

        public const int MinEmployerNameLength = 0;
        public const int MaxEmployerNameLength = 100;

        public const int MinTINLength = 0;
        public const int MaxTINLength = 30;

        public const int MinAddress1Length = 0;
        public const int MaxAddress1Length = 100;

        public const int MinAddress2Length = 0;
        public const int MaxAddress2Length = 100;

    }
}