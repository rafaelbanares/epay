﻿namespace MMB.ePay.Payroll
{
    public class EarningTypeSettingsConsts
    {

        public const int MinKeyLength = 0;
        public const int MaxKeyLength = 15;

        public const int MinValueLength = 0;
        public const int MaxValueLength = 50;

    }
}