﻿namespace MMB.ePay.Payroll
{
    public class TaxTablesConsts
    {

        public const int MinPayFreqLength = 0;
        public const int MaxPayFreqLength = 1;

        public const int MinCodeLength = 0;
        public const int MaxCodeLength = 4;

    }
}