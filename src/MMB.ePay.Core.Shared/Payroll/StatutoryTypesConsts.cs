﻿namespace MMB.ePay.Payroll
{
    public class StatutoryTypesConsts
    {
        public const int MinCodeLength = 0;
        public const int MaxCodeLength = 10;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 50;
    }
}