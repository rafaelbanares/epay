﻿namespace MMB.ePay.Payroll
{
    public class ReportsConsts
    {
        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinControllerNameLength = 0;
        public const int MaxControllerNameLength = 50;

        public const int MinReportTypeIdLength = 0;
        public const int MaxReportTypeIdLength = 1;

        public const int MinFormatLength = 0;
        public const int MaxFormatLength = 10;

        public const int MinOutputLength = 0;
        public const int MaxOutputLength = 1;
    }
}