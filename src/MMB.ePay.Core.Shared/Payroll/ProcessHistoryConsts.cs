﻿namespace MMB.ePay.Payroll
{
    public class ProcessHistoryConsts
    {
        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 100;
    }
}