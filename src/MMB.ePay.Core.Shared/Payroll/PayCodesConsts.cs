﻿namespace MMB.ePay.Payroll
{
    public class PayCodesConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

        public const int MinPayFreqLength = 0;
        public const int MaxPayFreqLength = 1;

        public const int MinTaxFreqLength = 0;
        public const int MaxTaxFreqLength = 1;

        public const int MinTaxMethodLength = 0;
        public const int MaxTaxMethodLength = 10;

    }
}