﻿namespace MMB.ePay.Payroll
{
    public class DependentsConsts
    {
        public const int MinDependentNameLength = 0;
        public const int MaxDependentNameLength = 50;
    }
}