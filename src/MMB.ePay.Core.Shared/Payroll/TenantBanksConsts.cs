﻿namespace MMB.ePay.Payroll
{
    public class TenantBanksConsts
    {
        public const int MinBankAccountNumberLength = 0;
        public const int MaxBankAccountNumberLength = 20;

        public const int MinBankAccountTypeLength = 0;
        public const int MaxBankAccountTypeLength = 2;

        public const int MinBranchNumberLength = 0;
        public const int MaxBranchNumberLength = 10;

        public const int MinCorporateIdLength = 0;
        public const int MaxCorporateIdLength = 20;
    }
}