﻿namespace MMB.ePay.Payroll
{
    public class BanksConsts
    {
        public const int MinIdLength = 0;
        public const int MaxIdLength = 10;

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;
    }
}