﻿namespace MMB.ePay.Payroll
{
    public class LoansConsts
    {

        public const int MinRefCodeLength = 0;
        public const int MaxRefCodeLength = 10;

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

        public const int MinCurrencyLength = 0;
        public const int MaxCurrencyLength = 3;

        public const int MinAcctCodeLength = 0;
        public const int MaxAcctCodeLength = 20;

    }
}