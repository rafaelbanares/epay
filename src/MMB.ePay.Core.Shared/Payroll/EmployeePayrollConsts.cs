﻿namespace MMB.ePay.Payroll
{
    public class EmployeePayrollConsts
    {
        public const int MinPayrollTypeLength = 0;
        public const int MaxPayrollTypeLength = 1;

        public const int MinPagibigNumberLength = 0;
        public const int MaxPagibigNumberLength = 20;

        public const int MinSSSNumberLength = 0;
        public const int MaxSSSNumberLength = 20;

        public const int MinPhilhealthNumberLength = 0;
        public const int MaxPhilhealthNumberLength = 20;

        public const int MinPaymentTypeLength = 0;
        public const int MaxPaymentTypeLength = 10;

        public const int MinBankAccountNoLength = 0;
        public const int MaxBankAccountNoLength = 20;

        public const int MinTINLength = 0;
        public const int MaxTINLength = 20;

        public const int MinEmploymentTypeLength = 0;
        public const int MaxEmploymentTypeLength = 450;
    }
}