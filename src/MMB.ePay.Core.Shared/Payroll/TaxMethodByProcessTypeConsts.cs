﻿namespace MMB.ePay.Payroll
{
    public class TaxMethodByProcessTypeConsts
    {
        public const int MinProcessTypeLength = 0;
        public const int MaxProcessTypeLength = 20;

        public const int MinTaxMethodLength = 0;
        public const int MaxTaxMethodLength = 20;
    }
}