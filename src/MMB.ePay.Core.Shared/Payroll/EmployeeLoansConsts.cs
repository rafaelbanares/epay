﻿namespace MMB.ePay.Payroll
{
    public class EmployeeLoansConsts
    {

        public const int MinFrequencyLength = 0;
        public const int MaxFrequencyLength = 5;

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 100;

        public const int MinStatusLength = 0;
        public const int MaxStatusLength = 6;

    }
}