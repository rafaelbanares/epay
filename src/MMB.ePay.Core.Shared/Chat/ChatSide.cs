﻿namespace MMB.ePay.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}