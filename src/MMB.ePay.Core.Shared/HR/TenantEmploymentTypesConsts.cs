﻿namespace MMB.ePay.HR
{
    public class TenantEmploymentTypesConsts
    {
        public const int MinEmploymentTypeIdLength = 0;
        public const int MaxEmploymentTypeIdLength = 1;
    }
}