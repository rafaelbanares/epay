﻿namespace MMB.ePay.HR
{
    public class GroupSettingsConsts
    {
        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinSysCodeLength = 0;
        public const int MaxSysCodeLength = 10;
    }
}