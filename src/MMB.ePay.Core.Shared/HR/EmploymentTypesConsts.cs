﻿namespace MMB.ePay.HR
{
    public class EmploymentTypesConsts
    {
        public const int MinIdLength = 0;
        public const int MaxIdLength = 1;

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;
    }
}