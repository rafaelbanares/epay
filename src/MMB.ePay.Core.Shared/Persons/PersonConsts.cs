﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMB.ePay.Persons
{
    public class PersonConsts
    {
        public const int MaxNameLength = 32;
        public const int MaxSurnameLength = 32;
        public const int MaxEmailAddressLength = 255;
    }
}
