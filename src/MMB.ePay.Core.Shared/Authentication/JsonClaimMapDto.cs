﻿namespace MMB.ePay.Authentication
{
    public class JsonClaimMapDto
    {
        public string Claim { get; set; }

        public string Key { get; set; }
    }
}