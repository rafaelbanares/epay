﻿using Abp.AutoMapper;
using MMB.ePay.Sessions.Dto;

namespace MMB.ePay.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}