﻿using System.Collections.Generic;
using MMB.ePay.Editions.Dto;
using MMB.ePay.MultiTenancy.Payments;

namespace MMB.ePay.Web.Models.Payment
{
    public class ExtendEditionViewModel
    {
        public EditionSelectDto Edition { get; set; }

        public List<PaymentGatewayModel> PaymentGateways { get; set; }
    }
}