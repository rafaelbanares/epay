﻿using System.Collections.Generic;
using MMB.ePay.Editions;
using MMB.ePay.Editions.Dto;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.MultiTenancy.Payments.Dto;

namespace MMB.ePay.Web.Models.Payment
{
    public class BuyEditionViewModel
    {
        public SubscriptionStartType? SubscriptionStartType { get; set; }

        public EditionSelectDto Edition { get; set; }

        public decimal? AdditionalPrice { get; set; }

        public EditionPaymentType EditionPaymentType { get; set; }

        public List<PaymentGatewayModel> PaymentGateways { get; set; }
    }
}
