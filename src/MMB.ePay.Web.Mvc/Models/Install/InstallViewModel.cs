﻿using System.Collections.Generic;
using Abp.Localization;
using MMB.ePay.Install.Dto;

namespace MMB.ePay.Web.Models.Install
{
    public class InstallViewModel
    {
        public List<ApplicationLanguage> Languages { get; set; }

        public AppSettingsJsonDto AppSettingsJson { get; set; }
    }
}
