﻿using Abp.AutoMapper;
using MMB.ePay.MultiTenancy.Dto;

namespace MMB.ePay.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(RegisterTenantOutput))]
    public class TenantRegisterResultViewModel : RegisterTenantOutput
    {
        public string TenantLoginAddress { get; set; }
    }
}