﻿using MMB.ePay.Editions;
using MMB.ePay.Editions.Dto;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.Security;
using MMB.ePay.MultiTenancy.Payments.Dto;

namespace MMB.ePay.Web.Models.TenantRegistration
{
    public class TenantRegisterViewModel
    {
        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }

        public int? EditionId { get; set; }

        public SubscriptionStartType? SubscriptionStartType { get; set; }

        public EditionSelectDto Edition { get; set; }

        public EditionPaymentType EditionPaymentType { get; set; }
    }
}
