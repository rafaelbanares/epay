﻿using Abp.AutoMapper;
using MMB.ePay.MultiTenancy.Dto;

namespace MMB.ePay.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(EditionsSelectOutput))]
    public class EditionsSelectViewModel : EditionsSelectOutput
    {
    }
}
