﻿using Abp.Application.Navigation;
using Abp.Authorization;
using Abp.Localization;
using MMB.ePay.Authorization;

namespace MMB.ePay.Web.Areas.App.Startup
{
    public class AppNavigationProvider : NavigationProvider
    {
        public const string MenuName = "App";

        public override void SetNavigation(INavigationProviderContext context)
        {
            var menu = context.Manager.Menus[MenuName] = new MenuDefinition(MenuName, new FixedLocalizableString("Main Menu"));

            menu
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Home,
                        L("Dashboard"),
                        url: "App/Welcome",
                        icon: "fas fa-layer-group"
                    )
                )
                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Tenant.Dashboard,
                //        L("Dashboard"),
                //        url: "App/TenantDashboard",
                //        icon: "fas fa-layer-group",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenant_Dashboard)
                //    )
                //)
                //.AddItem(new MenuItemDefinition(
                //    AppPageNames.Host.Dashboard,
                //    L("Dashboard"),
                //    url: "App/HostDashboard",
                //    icon: "fas fa-layer-group",
                //    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Dashboard))
                //)


                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.Holidays,
                //        L("Holidays"),
                //        url: "App/Holidays",
                //        icon: "flaticon-more",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Holidays)
                //    )
                //)

                ////Payroll Start
                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.Types,
                //        L("Types"),
                //        icon: "flaticon-interface-8"
                //    ).AddItem(new MenuItemDefinition(
                //            AppPageNames.Common.TimesheetTypez,
                //            L("TimesheetTypez"),
                //            url: "App/TimesheetTypez",
                //            icon: "flaticon-suitcase"
                //        //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Types_TimesheetTypez)
                //        )
                //    )
                ////Payroll End

                //REPORTS
                .AddItem(new MenuItemDefinition(
                    AppPageNames.Common.Reports,
                    L("Reports"),
                    icon: "flaticon-line-graph"
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Periodic,
                        L("Periodic"),
                        url: "App/Reports/Periodic",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reports))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Monthly,
                        L("Monthly"),
                        url: "App/Reports/Monthly",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reports))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Quarterly,
                        L("Quarterly"),
                        url: "App/Reports/Quarterly",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reports))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Annual,
                        L("Annual"),
                        url: "App/Reports/Annual",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reports))
                    )
                )

                //TRANSACTIONS
                .AddItem(new MenuItemDefinition(
                    AppPageNames.Common.Transactions,
                    L("Transactions"),
                    icon: "flaticon-line-graph"
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Process,
                        L("Process"),
                        url: "App/Process",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Process))
                    //).AddItem(new MenuItemDefinition(
                    //    AppPageNames.Common.Reports,
                    //    L("Reports"),
                    //    url: "App/Reports",
                    //    icon: "flaticon-calendar-with-a-clock-time-tools",
                    //    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reports))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.PayrollTransactions,
                        L("PayrollTransactions"),
                        url: "App/PayrollTransactions",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Timesheets))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.EmployeeLoans,
                        L("EmployeeLoans"),
                        url: "App/EmployeeLoans",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_EmployeeLoans))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.PayrollProcess,
                        L("PayrollProcess"),
                        url: "App/PayrollProcess",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_PayrollProcess))
                    )
                )

                ////MAINTENANCE
                //.AddItem(new MenuItemDefinition(
                //    AppPageNames.Common.Maintenance,
                //    L("Maintenance"),
                //    icon: "fas fa-cog"
                //    ).AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.Employees,
                //        L("Employees"),
                //        url: "App/Employees",
                //        icon: "fas fa-user-friends",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Employees))
                //    ).AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.PayPeriod,
                //        L("PayPeriod"),
                //        url: "App/PayPeriod",
                //        icon: "flaticon-clock-2",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_PayPeriod))
                //    ).AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.Reviewers,
                //        L("Reviewers"),
                //        url: "App/Reviewers",
                //        icon: "fas fa-user-check",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reviewers))
                //    ).AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.TenantBanks,
                //        L("TenantBanks"),
                //        url: "App/TenantBanks",
                //        icon: "fas fa-balance-scale",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_TenantBanks))
                //    )
                //)

                //TYPES
                .AddItem(new MenuItemDefinition(
                    AppPageNames.Common.Types,
                    L("Types"),
                    icon: "fas fa-keyboard"
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.TimesheetTypes,
                        L("TimesheetTypes"),
                        url: "App/TimesheetTypes",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_TimesheetTypes))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.OvertimeTypes,
                        L("OvertimeTypes"),
                        url: "App/OvertimeTypes",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OvertimeTypes))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.EarningTypes,
                        L("EarningTypes"),
                        url: "App/EarningTypes",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_EarningTypes))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.DeductionTypes,
                        L("DeductionTypes"),
                        url: "App/DeductionTypes",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_DeductionTypes))

                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.LoanTypes,
                        L("LoanTypes"),
                        url: "App/LoanTypes",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LoanTypes))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Banks,
                        L("Banks"),
                        url: "App/Banks",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Banks))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.PayCodes,
                        L("PayCodes"),
                        url: "App/PayCodes",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_PayCodes))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.OvertimeFormats,
                        L("OvertimeFormats"),
                        url: "App/OvertimeFormats",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OvertimeFormats))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Loans,
                        L("Loans"),
                        url: "App/Loans",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Loans))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.TaxTables,
                        L("TaxTables"),
                        url: "App/TaxTables",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_TaxTables))
                    )
                )

                .AddItem(new MenuItemDefinition(
                    AppPageNames.Host.Tenants,
                    L("Tenants"),
                    url: "App/Tenants",
                    icon: "flaticon-list-3",
                    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenants)
                    )
                )
                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Host.Editions,
                //        L("Editions"),
                //        url: "App/Editions",
                //        icon: "flaticon-app",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Editions)
                //    )
                //)
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Administration,
                        L("Administration"),
                        icon: "flaticon-interface-8"

                        ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Employees,
                        L("Employees"),
                        url: "App/Employees",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Employees))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.ProratedEarnings,
                        L("ProratedEarnings"),
                        url: "App/ProratedEarnings",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ProratedEarnings))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.PayPeriod,
                        L("PayPeriod"),
                        url: "App/PayPeriod",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_PayPeriod))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Reviewers,
                        L("Reviewers"),
                        url: "App/Reviewers",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reviewers))
                    ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.TenantBanks,
                        L("TenantBanks"),
                        url: "App/TenantBanks",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_TenantBanks))
                    


                        ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.OrganizationUnits,
                            L("OrganizationUnits"),
                            url: "App/OrganizationUnits",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_OrganizationUnits)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Companies,
                            L("Companies"),
                            url: "App/Companies",
                            icon: "flaticon2-hexagonal"
                            //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Companies)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Roles,
                            L("Roles"),
                            url: "App/Roles",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Roles)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Users,
                            L("Users"),
                            url: "App/Users",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Users)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Common.Languages,
                    //        L("Languages"),
                    //        url: "App/Languages",
                    //        icon: "flaticon-tabs",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Languages)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.AuditLogs,
                            L("AuditLogs"),
                            url: "App/AuditLogs",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_AuditLogs)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Maintenance,
                            L("Maintenance"),
                            url: "App/Maintenance",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Maintenance)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.SubscriptionManagement,
                            L("Subscription"),
                            url: "App/SubscriptionManagement",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.UiCustomization,
                            L("VisualSettings"),
                            url: "App/UiCustomization",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_UiCustomization)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.WebhookSubscriptions,
                            L("WebhookSubscriptions"),
                            url: "App/WebhookSubscription",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_WebhookSubscription)
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            AppPageNames.Common.DynamicProperties,
                            L("DynamicProperties"),
                            url: "App/DynamicProperty",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_DynamicProperties)
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Settings,
                            L("Settings"),
                            url: "App/HostSettings",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Settings)
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Settings,
                            L("Settings"),
                            url: "App/Settings",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Tenant_Settings)
                        )
                    )
                );
                
                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.DemoUiComponents,
                //        L("DemoUiComponents"),
                //        url: "App/DemoUiComponents",
                //        icon: "flaticon-shapes",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_DemoUiComponents)
                //    )
                //);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ePayConsts.LocalizationSourceName);
        }
    }
}