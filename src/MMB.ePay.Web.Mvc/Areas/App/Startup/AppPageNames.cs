﻿namespace MMB.ePay.Web.Areas.App.Startup
{
    public class AppPageNames
    {
        public static class Common
        {
            public const string Holidays = "Test.Holidays";
            public const string Reports = "Reports";
            public const string Transactions = "Transactions";
            public const string Maintenance = "Maintenance";
            public const string Types = "Types";

            public const string PayrollTransactions = "Payroll.PayrollTransactions";
            public const string OvertimeFormats = "Payroll.OvertimeFormats";

            public const string TimesheetTypes = "Payroll.TimesheetTypes";
            public const string EmploymentTypes = "HR.EmploymentTypes";
            public const string Banks = "Payroll.Banks";
            public const string StatutoryTypes = "Payroll.StatutoryTypes";
            public const string ProcessStatus = "Payroll.ProcessStatus";
            public const string GroupSettings = "HR.GroupSettings";
            public const string OvertimeTypes = "Payroll.OvertimeTypes";
            public const string Ranks = "Payroll.Ranks";
            public const string TimesheetTmp = "Payroll.TimesheetTmp";
            public const string Timesheets = "Payroll.Timesheets";
            public const string TimesheetProcess = "Payroll.TimesheetProcess";
            public const string TimesheetPosted = "Payroll.TimesheetPosted";
            public const string TaxTables = "Payroll.TaxTables";
            public const string StatutoryTmp = "Payroll.StatutoryTmp";
            public const string StatutoryProcess = "Payroll.StatutoryProcess";
            public const string StatutoryPosted = "Payroll.StatutoryPosted";
            public const string SssTables = "Payroll.SssTables";
            public const string Reviewers = "Payroll.Reviewers";
            public const string RecurEarnings = "Payroll.RecurEarnings";
            public const string RecurDeductions = "Payroll.RecurDeductions";
            public const string ProcessHistory = "Payroll.ProcessHistory";
            public const string Process = "Payroll.Process";

            public const string Periodic = "Payroll.Periodic";
            public const string Monthly = "Payroll.Monthly";
            public const string Quarterly = "Payroll.Quarterly";
            public const string Annual = "Payroll.Annual";

            public const string PaySettings = "Payroll.PaySettings";
            public const string PayrollTmp = "Payroll.PayrollTmp";
            public const string PayrollProcess = "Payroll.PayrollProcess";
            public const string PayrollPosted = "Payroll.PayrollPosted";
            public const string PayPeriod = "Payroll.PayPeriod";
            public const string ProratedEarnings = "Payroll.ProratedEarnings";
            public const string OvertimeSubTypes = "Payroll.OvertimeSubTypes";
            public const string OvertimeMainTypes = "Payroll.OvertimeMainTypes";
            public const string OTSheetTmp = "Payroll.OTSheetTmp";
            public const string OTSheetProcess = "Payroll.OTSheetProcess";
            public const string OTSheetPosted = "Payroll.OTSheetPosted";
            public const string OTSheet = "Payroll.OTSheet";
            public const string LoanTmp = "Payroll.LoanTmp";
            public const string LoanProcess = "Payroll.LoanProcess";
            public const string LeaveCredits = "Payroll.LeaveCredits";
            public const string Employers = "Payroll.Employers";
            public const string EarningTypeSettings = "Payroll.EarningTypeSettings";
            public const string EarningTmp = "Payroll.EarningTmp";
            public const string Earnings = "Payroll.Earnings";
            public const string EarningProcess = "Payroll.EarningProcess";
            public const string EarningPosted = "Payroll.EarningPosted";
            public const string DeductionTmp = "Payroll.DeductionTmp";
            public const string Deductions = "Payroll.Deductions";
            public const string DeductionProcess = "Payroll.DeductionProcess";
            public const string DeductionPosted = "Payroll.DeductionPosted";
            public const string TenantBanks = "Payroll.TenantBanks";
            public const string ContactPersons = "Payroll.ContactPersons";
            public const string CompanyStatutoryInfo = "Payroll.CompanyStatutoryInfo";
            public const string BankBranches = "Payroll.BankBranches";
            public const string HRSettings = "HR.HRSettings";
            public const string PayCodes = "Payroll.PayCodes";
            public const string EmployeePayroll = "Payroll.EmployeePayroll";
            public const string EarningTypes = "Payroll.EarningTypes";
            public const string EmployeeLoans = "Payroll.EmployeeLoans";
            public const string LoanPosted = "Payroll.LoanPosted";
            public const string Loans = "Payroll.Loans";
            public const string Employees = "HR.Employees";
            public const string LoanTypes = "Payroll.LoanTypes";
            public const string DeductionTypes = "Payroll.DeductionTypes";
            public const string LeaveTypes = "Payroll.LeaveTypes";
            public const string Administration = "Administration";
            public const string Companies = "Administration.Companies";
            public const string Roles = "Administration.Roles";
            public const string Users = "Administration.Users";
            public const string AuditLogs = "Administration.AuditLogs";
            public const string OrganizationUnits = "Administration.OrganizationUnits";
            public const string Languages = "Administration.Languages";
            public const string DemoUiComponents = "Administration.DemoUiComponents";
            public const string UiCustomization = "Administration.UiCustomization";
            public const string WebhookSubscriptions = "Administration.WebhookSubscriptions";
            public const string DynamicProperties = "Administration.DynamicProperties";
            public const string DynamicEntityProperties = "Administration.DynamicEntityProperties";
        }

        public static class Host
        {
            public const string Tenants = "Tenants";
            public const string Editions = "Editions";
            public const string Maintenance = "Administration.Maintenance";
            public const string Settings = "Administration.Settings.Host";
            public const string Dashboard = "Dashboard";
        }

        public static class Tenant
        {
            public const string Home = "Payroll.Home";

            public const string Dashboard = "Dashboard.Tenant";
            public const string Settings = "Administration.Settings.Tenant";
            public const string SubscriptionManagement = "Administration.SubscriptionManagement.Tenant";
            public const string ePay = "Administation.ePay.Tenant";
        }
    }
}