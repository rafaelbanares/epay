﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.OTSheetPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OTSheetPosted)]
    public class OTSheetPostedController : ePayControllerBase
    {
        private readonly IOTSheetPostedAppService _otSheetPostedAppService;

        public OTSheetPostedController(IOTSheetPostedAppService otSheetPostedAppService)
        {
            _otSheetPostedAppService = otSheetPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new OTSheetPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_OTSheetPosted_Create, AppPermissions.Pages_OTSheetPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetOTSheetPostedForEditOutput getOTSheetPostedForEditOutput;

            if (id.HasValue)
            {
                getOTSheetPostedForEditOutput = await _otSheetPostedAppService.GetOTSheetPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getOTSheetPostedForEditOutput = new GetOTSheetPostedForEditOutput
                {
                    OTSheetPosted = new CreateOrEditOTSheetPostedDto()
                };
            }

            var viewModel = new CreateOrEditOTSheetPostedModalViewModel()
            {
                OTSheetPosted = getOTSheetPostedForEditOutput.OTSheetPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOTSheetPostedModal(int id)
        {
            var getOTSheetPostedForViewDto = await _otSheetPostedAppService.GetOTSheetPostedForView(id);

            var model = new OTSheetPostedViewModel()
            {
                OTSheetPosted = getOTSheetPostedForViewDto.OTSheetPosted
            };

            return PartialView("_ViewOTSheetPostedModal", model);
        }

    }
}