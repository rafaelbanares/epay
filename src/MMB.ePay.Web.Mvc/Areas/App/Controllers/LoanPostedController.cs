﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.LoanPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LoanPosted)]
    public class LoanPostedController : ePayControllerBase
    {
        private readonly ILoanPostedAppService _loanPostedAppService;

        public LoanPostedController(ILoanPostedAppService loanPostedAppService)
        {
            _loanPostedAppService = loanPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new LoanPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LoanPosted_Create, AppPermissions.Pages_LoanPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLoanPostedForEditOutput getLoanPostedForEditOutput;

            if (id.HasValue)
            {
                getLoanPostedForEditOutput = await _loanPostedAppService.GetLoanPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLoanPostedForEditOutput = new GetLoanPostedForEditOutput
                {
                    LoanPosted = new CreateOrEditLoanPostedDto()
                };
                getLoanPostedForEditOutput.LoanPosted.PayDate = DateTime.Now;
                getLoanPostedForEditOutput.LoanPosted.CreatedDate = DateTime.Now;
            }

            var viewModel = new CreateOrEditLoanPostedModalViewModel()
            {
                LoanPosted = getLoanPostedForEditOutput.LoanPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLoanPostedModal(int id)
        {
            var getLoanPostedForViewDto = await _loanPostedAppService.GetLoanPostedForView(id);

            var model = new LoanPostedViewModel()
            {
                LoanPosted = getLoanPostedForViewDto.LoanPosted
            };

            return PartialView("_ViewLoanPostedModal", model);
        }

    }
}