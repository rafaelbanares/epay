﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.TimesheetTypes;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TimesheetTypes)]
    public class TimesheetTypesController : ePayControllerBase
    {
        private readonly ITimesheetTypesAppService _timesheetTypesAppService;

        public TimesheetTypesController(ITimesheetTypesAppService timesheetTypesAppService)
        {
            _timesheetTypesAppService = timesheetTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new TimesheetTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TimesheetTypes_Create, AppPermissions.Pages_TimesheetTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetTimesheetTypesForEditOutput getTimesheetTypesForEditOutput;

            if (id.HasValue)
            {
                getTimesheetTypesForEditOutput = await _timesheetTypesAppService.GetTimesheetTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getTimesheetTypesForEditOutput = new GetTimesheetTypesForEditOutput
                {
                    TimesheetTypes = new CreateOrEditTimesheetTypesDto()
                };
            }

            var viewModel = new CreateOrEditTimesheetTypesModalViewModel()
            {
                TimesheetTypes = getTimesheetTypesForEditOutput.TimesheetTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewTimesheetTypesModal(int id)
        {
            var getTimesheetTypesForViewDto = await _timesheetTypesAppService.GetTimesheetTypesForView(id);

            var model = new TimesheetTypesViewModel()
            {
                TimesheetTypes = getTimesheetTypesForViewDto.TimesheetTypes
            };

            return PartialView("_ViewTimesheetTypesModal", model);
        }

    }
}