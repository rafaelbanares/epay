﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.OvertimeSubTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeSubTypes)]
    public class OvertimeSubTypesController : ePayControllerBase
    {
        private readonly IOvertimeSubTypesAppService _overtimeSubTypesAppService;

        public OvertimeSubTypesController(IOvertimeSubTypesAppService overtimeSubTypesAppService)
        {
            _overtimeSubTypesAppService = overtimeSubTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new OvertimeSubTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_OvertimeSubTypes_Create, AppPermissions.Pages_OvertimeSubTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetOvertimeSubTypesForEditOutput getOvertimeSubTypesForEditOutput;

            if (id.HasValue)
            {
                getOvertimeSubTypesForEditOutput = await _overtimeSubTypesAppService.GetOvertimeSubTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getOvertimeSubTypesForEditOutput = new GetOvertimeSubTypesForEditOutput
                {
                    OvertimeSubTypes = new CreateOrEditOvertimeSubTypesDto()
                };
            }

            var viewModel = new CreateOrEditOvertimeSubTypesModalViewModel()
            {
                OvertimeSubTypes = getOvertimeSubTypesForEditOutput.OvertimeSubTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOvertimeSubTypesModal(int id)
        {
            var getOvertimeSubTypesForViewDto = await _overtimeSubTypesAppService.GetOvertimeSubTypesForView(id);

            var model = new OvertimeSubTypesViewModel()
            {
                OvertimeSubTypes = getOvertimeSubTypesForViewDto.OvertimeSubTypes
            };

            return PartialView("_ViewOvertimeSubTypesModal", model);
        }

    }
}