﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.TimesheetPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TimesheetPosted)]
    public class TimesheetPostedController : ePayControllerBase
    {
        private readonly ITimesheetPostedAppService _timesheetPostedAppService;

        public TimesheetPostedController(ITimesheetPostedAppService timesheetPostedAppService)
        {
            _timesheetPostedAppService = timesheetPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new TimesheetPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TimesheetPosted_Create, AppPermissions.Pages_TimesheetPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetTimesheetPostedForEditOutput getTimesheetPostedForEditOutput;

            if (id.HasValue)
            {
                getTimesheetPostedForEditOutput = await _timesheetPostedAppService.GetTimesheetPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getTimesheetPostedForEditOutput = new GetTimesheetPostedForEditOutput
                {
                    TimesheetPosted = new CreateOrEditTimesheetPostedDto()
                };
            }

            var viewModel = new CreateOrEditTimesheetPostedModalViewModel()
            {
                TimesheetPosted = getTimesheetPostedForEditOutput.TimesheetPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewTimesheetPostedModal(int id)
        {
            var getTimesheetPostedForViewDto = await _timesheetPostedAppService.GetTimesheetPostedForView(id);

            var model = new TimesheetPostedViewModel()
            {
                TimesheetPosted = getTimesheetPostedForViewDto.TimesheetPosted
            };

            return PartialView("_ViewTimesheetPostedModal", model);
        }

    }
}