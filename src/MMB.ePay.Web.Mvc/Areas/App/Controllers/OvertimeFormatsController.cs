﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.OvertimeMainTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeMainTypes)]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeSubTypes)]
    public class OvertimeFormatsController : ePayControllerBase
    {
        private readonly IOvertimeMainTypesAppService _overtimeMainTypesAppService;

        public OvertimeFormatsController(IOvertimeMainTypesAppService overtimeMainTypesAppService)
        {
            _overtimeMainTypesAppService = overtimeMainTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new OvertimeMainTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_OvertimeMainTypes_Create, AppPermissions.Pages_OvertimeMainTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetOvertimeMainTypesForEditOutput getOvertimeMainTypesForEditOutput;

            if (id.HasValue)
            {
                getOvertimeMainTypesForEditOutput = await _overtimeMainTypesAppService.GetOvertimeMainTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getOvertimeMainTypesForEditOutput = new GetOvertimeMainTypesForEditOutput
                {
                    OvertimeMainTypes = new CreateOrEditOvertimeMainTypesDto()
                };
            }

            var viewModel = new CreateOrEditOvertimeMainTypesModalViewModel()
            {
                OvertimeMainTypes = getOvertimeMainTypesForEditOutput.OvertimeMainTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOvertimeMainTypesModal(int id)
        {
            var getOvertimeMainTypesForViewDto = await _overtimeMainTypesAppService.GetOvertimeMainTypesForView(id);

            var model = new OvertimeMainTypesViewModel()
            {
                OvertimeMainTypes = getOvertimeMainTypesForViewDto.OvertimeMainTypes
            };

            return PartialView("_ViewOvertimeMainTypesModal", model);
        }

    }
}