﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.SssTables;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_SssTables)]
    public class SssTablesController : ePayControllerBase
    {
        private readonly ISssTablesAppService _sssTablesAppService;

        public SssTablesController(ISssTablesAppService sssTablesAppService)
        {
            _sssTablesAppService = sssTablesAppService;
        }

        public ActionResult Index()
        {
            var model = new SssTablesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_SssTables_Create, AppPermissions.Pages_SssTables_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetSssTablesForEditOutput getSssTablesForEditOutput;

            if (id.HasValue)
            {
                getSssTablesForEditOutput = await _sssTablesAppService.GetSssTablesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getSssTablesForEditOutput = new GetSssTablesForEditOutput
                {
                    SssTables = new CreateOrEditSssTablesDto()
                };
            }

            var viewModel = new CreateOrEditSssTablesModalViewModel()
            {
                SssTables = getSssTablesForEditOutput.SssTables,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewSssTablesModal(int id)
        {
            var getSssTablesForViewDto = await _sssTablesAppService.GetSssTablesForView(id);

            var model = new SssTablesViewModel()
            {
                SssTables = getSssTablesForViewDto.SssTables
            };

            return PartialView("_ViewSssTablesModal", model);
        }

    }
}