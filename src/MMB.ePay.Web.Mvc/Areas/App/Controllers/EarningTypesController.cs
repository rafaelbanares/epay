﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.EarningTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EarningTypes)]
    public class EarningTypesController : ePayControllerBase
    {
        private readonly IEarningTypesAppService _earningTypesAppService;
        private readonly IEmployeesAppService _employeesAppService;

        public EarningTypesController(IEarningTypesAppService earningTypesAppService, IEmployeesAppService employeesAppService)
        {
            _earningTypesAppService = earningTypesAppService;
            _employeesAppService = employeesAppService;
        }

        public ActionResult Index()
        {
            var model = new EarningTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EarningTypes_Create, AppPermissions.Pages_EarningTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEarningTypesForEditOutput getEarningTypesForEditOutput;

            if (id.HasValue)
            {
                getEarningTypesForEditOutput = await _earningTypesAppService.GetEarningTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEarningTypesForEditOutput = new GetEarningTypesForEditOutput
                {
                    EarningTypes = new CreateOrEditEarningTypesDto()
                };
            }

            var viewModel = new CreateOrEditEarningTypesModalViewModel()
            {
                EarningTypes = getEarningTypesForEditOutput.EarningTypes,
                CurrencyList = _employeesAppService.GetCurrencyList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEarningTypesModal(int id)
        {
            var getEarningTypesForViewDto = await _earningTypesAppService.GetEarningTypesForView(id);

            var model = new EarningTypesViewModel()
            {
                EarningTypes = getEarningTypesForViewDto.EarningTypes
            };

            return PartialView("_ViewEarningTypesModal", model);
        }

    }
}