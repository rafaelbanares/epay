﻿using Microsoft.AspNetCore.Mvc;
using MMB.ePay.HR;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Companies;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    //[AbpMvcAuthorize(AppPermissions.Pages_Companies)]
    public class CompaniesController : ePayControllerBase
    {
        //private readonly ICompaniesAppService _companiesAppService;
        private readonly IEmploymentTypesAppService _employmentTypesAppService;
        private readonly IPayCodesAppService _payCodesAppService;
        private readonly IBanksAppService _banksAppService;
        private readonly IBankBranchesAppService _bankBranchesAppService;
        private readonly IRanksAppService _ranksAppService;

        private readonly IHelperAppService _helperAppService;
        private readonly IEmployeePayrollAppService _employeePayrollAppService;
        private readonly IEmployersAppService _employersAppService;
        //private readonly IDependentsAppService _dependetsAppService;

        private readonly ICompaniesAppService _companiesAppService;

        public CompaniesController(
            //ICompaniesAppService employeesAppService,
            IEmploymentTypesAppService employmentTypesAppService,
            IPayCodesAppService payCodesAppService,
            IBanksAppService banksAppService,
            IBankBranchesAppService bankBranchesAppService,
            IRanksAppService ranksAppService,
            IHelperAppService helperAppService,
            IEmployeePayrollAppService employeePayrollAppService,
            IEmployersAppService employersAppService,
            ICompaniesAppService companiesAppService
            //IDependentsAppService dependentsAppService
            )
        {
            //_employeesAppService = employeesAppService;
            _employmentTypesAppService = employmentTypesAppService;
            _payCodesAppService = payCodesAppService;
            _banksAppService = banksAppService;
            _bankBranchesAppService = bankBranchesAppService;
            _ranksAppService = ranksAppService;
            _helperAppService = helperAppService;
            _employeePayrollAppService = employeePayrollAppService;
            _employersAppService = employersAppService;
            _companiesAppService = companiesAppService;
            //_dependetsAppService = dependentsAppService;
        }

        public async Task<IActionResult> Index()
        {
            var getCompaniesForEditOutput = await _companiesAppService.GetCompaniesForEdit();

            return View(new CompaniesViewModel
            {
                Companies = getCompaniesForEditOutput.Companies
            });
        }

        //[AbpMvcAuthorize(AppPermissions.Pages_Companies)]
        //public async Task<IActionResult> Index(int id, int? view)
        //{
        //    var dto = new EntityDto { Id = id };

        //    var getCompaniesForEditOutput = await _employeesAppService.GetCompaniesForEdit(dto);
        //    var getEmployeePayrollForEditOutput = await _employeePayrollAppService.GetEmployeePayrollForEdit(dto);
        //    var getEmployersForEditOutput = await _employersAppService.GetEmployersForEdit(dto);

        //    var viewModel = new ProfileCompaniesModalViewModel
        //    {
        //        Companies = getCompaniesForEditOutput.Companies,
        //        EmployeeStatusList = _employeesAppService.GetEmployeeStatusList(),
        //        EmploymentTypeList = await _employmentTypesAppService.GetEmploymentTypeList(),
        //        PayCodeList = await _payCodesAppService.GetPayCodeList(),
        //        BankList = await _banksAppService.GetBankList(),
        //        PaymentTypeList =  _helperAppService.GetPaymentTypeList(),
        //        RankList = await _ranksAppService.GetRankList()
        //    };

        //    viewModel.BankBranchList = await _bankBranchesAppService.GetBankBranchList(viewModel.BankList[0].Value);

        //    if (getEmployeePayrollForEditOutput.EmployeePayroll != null)
        //    {
        //        viewModel.EmployeePayroll = getEmployeePayrollForEditOutput.EmployeePayroll;
        //    }

        //    if (getEmployersForEditOutput.Employers != null)
        //    {
        //        viewModel.Employers = getEmployersForEditOutput.Employers;
        //    }

        //    viewModel.HasView = view == 1;
        //    return View(viewModel);
        //}

        //[AbpMvcAuthorize(AppPermissions.Pages_Companies_Create, AppPermissions.Pages_Companies_Edit)]
        //public async Task<PartialViewResult> CreateOrEditModal(int? id)
        //{
        //    GetCompaniesForEditOutput getCompaniesForEditOutput;

        //    if (id.HasValue)
        //    {
        //        getCompaniesForEditOutput = await _employeesAppService.GetCompaniesForEdit(new EntityDto { Id = (int)id });
        //    }
        //    else
        //    {
        //        getCompaniesForEditOutput = new GetCompaniesForEditOutput
        //        {
        //            Companies = new CreateOrEditCompaniesDto()
        //        };
        //    }

        //    var viewModel = new CreateOrEditCompaniesModalViewModel()
        //    {
        //        Companies = getCompaniesForEditOutput.Companies,
        //        EmploymentTypeList = await _employmentTypesAppService.GetEmploymentTypeList()
        //    };

        //    return PartialView("_CreateOrEditModal", viewModel);
        //}

        //public async Task<PartialViewResult> ViewCompaniesModal(int id)
        //{
        //    var getCompaniesForViewDto = await _employeesAppService.GetCompaniesForView(id);

        //    var model = new CompaniesViewModel()
        //    {
        //        Companies = getCompaniesForViewDto.Companies
        //    };

        //    return PartialView("_ViewCompaniesModal", model);
        //}

    }
}