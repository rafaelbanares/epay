﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Process;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Process)]
    public class ProcessController : ePayControllerBase
    {
        private readonly IProcessAppService _processAppService;

        public ProcessController(IProcessAppService processAppService)
        {
            _processAppService = processAppService;
        }

        public ActionResult Index()
        {
            var model = new ProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Process_Create, AppPermissions.Pages_Process_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetProcessForEditOutput getProcessForEditOutput;

            if (id.HasValue)
            {
                getProcessForEditOutput = await _processAppService.GetProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getProcessForEditOutput = new GetProcessForEditOutput
                {
                    Process = new CreateOrEditProcessDto()
                };
            }

            var viewModel = new CreateOrEditProcessModalViewModel()
            {
                Process = getProcessForEditOutput.Process,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewProcessModal(int id)
        {
            var getProcessForViewDto = await _processAppService.GetProcessForView(id);

            var model = new ProcessViewModel()
            {
                Process = getProcessForViewDto.Process
            };

            return PartialView("_ViewProcessModal", model);
        }

    }
}