﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.DeductionPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_DeductionPosted)]
    public class DeductionPostedController : ePayControllerBase
    {
        private readonly IDeductionPostedAppService _deductionPostedAppService;

        public DeductionPostedController(IDeductionPostedAppService deductionPostedAppService)
        {
            _deductionPostedAppService = deductionPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new DeductionPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_DeductionPosted_Create, AppPermissions.Pages_DeductionPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetDeductionPostedForEditOutput getDeductionPostedForEditOutput;

            if (id.HasValue)
            {
                getDeductionPostedForEditOutput = await _deductionPostedAppService.GetDeductionPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getDeductionPostedForEditOutput = new GetDeductionPostedForEditOutput
                {
                    DeductionPosted = new CreateOrEditDeductionPostedDto()
                };
            }

            var viewModel = new CreateOrEditDeductionPostedModalViewModel()
            {
                DeductionPosted = getDeductionPostedForEditOutput.DeductionPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewDeductionPostedModal(int id)
        {
            var getDeductionPostedForViewDto = await _deductionPostedAppService.GetDeductionPostedForView(id);

            var model = new DeductionPostedViewModel()
            {
                DeductionPosted = getDeductionPostedForViewDto.DeductionPosted
            };

            return PartialView("_ViewDeductionPostedModal", model);
        }

    }
}