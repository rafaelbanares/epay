﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.CompanyStatutoryInfo;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_CompanyStatutoryInfo)]
    public class CompanyStatutoryInfoController : ePayControllerBase
    {
        private readonly ICompanyStatutoryInfoAppService _companyStatutoryInfoAppService;
        private readonly ISignatoriesAppService _signatoriesAppService;
        private readonly IStatutoryTypesAppService _statutoryTypesAppService;

        public CompanyStatutoryInfoController(ICompanyStatutoryInfoAppService companyStatutoryInfoAppService,
            ISignatoriesAppService signatoriesAppService,
            IStatutoryTypesAppService statutoryTypesAppService)
        {
            _companyStatutoryInfoAppService = companyStatutoryInfoAppService;
            _signatoriesAppService = signatoriesAppService;
            _statutoryTypesAppService = statutoryTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new CompanyStatutoryInfoViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_CompanyStatutoryInfo_Create, AppPermissions.Pages_CompanyStatutoryInfo_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(string id)
        {
            GetCompanyStatutoryInfoForEditOutput getCompanyStatutoryInfoForEditOutput;

            var viewModel = new CreateOrEditCompanyStatutoryInfoModalViewModel()
            {
                SignatoryList = await _signatoriesAppService.GetSignatoryList(),
                StatutoryTypeList = await _statutoryTypesAppService.GetStatutoryTypeList(),
            };

            if (!string.IsNullOrEmpty(id))
            {
                getCompanyStatutoryInfoForEditOutput = await _companyStatutoryInfoAppService
                    .GetCompanyStatutoryInfoForEdit(id);
            }
            else
            {
                getCompanyStatutoryInfoForEditOutput = new GetCompanyStatutoryInfoForEditOutput
                {
                    CompanyStatutoryInfo = new CreateOrEditCompanyStatutoryInfoDto()
                };
            }

            viewModel.CompanyStatutoryInfo = getCompanyStatutoryInfoForEditOutput.CompanyStatutoryInfo;

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewCompanyStatutoryInfoModal(string id)
        {
            var getCompanyStatutoryInfoForViewDto = await _companyStatutoryInfoAppService.GetCompanyStatutoryInfoForView(id);

            var model = new CompanyStatutoryInfoViewModel()
            {
                CompanyStatutoryInfo = getCompanyStatutoryInfoForViewDto.CompanyStatutoryInfo
            };

            return PartialView("_ViewCompanyStatutoryInfoModal", model);
        }

    }
}