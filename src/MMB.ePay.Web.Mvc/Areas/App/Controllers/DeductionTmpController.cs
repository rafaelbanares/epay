﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.DeductionTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_DeductionTmp)]
    public class DeductionTmpController : ePayControllerBase
    {
        private readonly IDeductionTmpAppService _deductionTmpAppService;

        public DeductionTmpController(IDeductionTmpAppService deductionTmpAppService)
        {
            _deductionTmpAppService = deductionTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new DeductionTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_DeductionTmp_Create, AppPermissions.Pages_DeductionTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetDeductionTmpForEditOutput getDeductionTmpForEditOutput;

            if (id.HasValue)
            {
                getDeductionTmpForEditOutput = await _deductionTmpAppService.GetDeductionTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getDeductionTmpForEditOutput = new GetDeductionTmpForEditOutput
                {
                    DeductionTmp = new CreateOrEditDeductionTmpDto()
                };
            }

            var viewModel = new CreateOrEditDeductionTmpModalViewModel()
            {
                DeductionTmp = getDeductionTmpForEditOutput.DeductionTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewDeductionTmpModal(int id)
        {
            var getDeductionTmpForViewDto = await _deductionTmpAppService.GetDeductionTmpForView(id);

            var model = new DeductionTmpViewModel()
            {
                DeductionTmp = getDeductionTmpForViewDto.DeductionTmp
            };

            return PartialView("_ViewDeductionTmpModal", model);
        }

    }
}