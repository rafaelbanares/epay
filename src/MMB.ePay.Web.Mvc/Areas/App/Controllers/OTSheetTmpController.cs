﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.OTSheetTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OTSheetTmp)]
    public class OTSheetTmpController : ePayControllerBase
    {
        private readonly IOTSheetTmpAppService _otSheetTmpAppService;

        public OTSheetTmpController(IOTSheetTmpAppService otSheetTmpAppService)
        {
            _otSheetTmpAppService = otSheetTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new OTSheetTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_OTSheetTmp_Create, AppPermissions.Pages_OTSheetTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetOTSheetTmpForEditOutput getOTSheetTmpForEditOutput;

            if (id.HasValue)
            {
                getOTSheetTmpForEditOutput = await _otSheetTmpAppService.GetOTSheetTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getOTSheetTmpForEditOutput = new GetOTSheetTmpForEditOutput
                {
                    OTSheetTmp = new CreateOrEditOTSheetTmpDto()
                };
            }

            var viewModel = new CreateOrEditOTSheetTmpModalViewModel()
            {
                OTSheetTmp = getOTSheetTmpForEditOutput.OTSheetTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOTSheetTmpModal(int id)
        {
            var getOTSheetTmpForViewDto = await _otSheetTmpAppService.GetOTSheetTmpForView(id);

            var model = new OTSheetTmpViewModel()
            {
                OTSheetTmp = getOTSheetTmpForViewDto.OTSheetTmp
            };

            return PartialView("_ViewOTSheetTmpModal", model);
        }

    }
}