﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.StatutoryTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_StatutoryTmp)]
    public class StatutoryTmpController : ePayControllerBase
    {
        private readonly IStatutoryTmpAppService _statutoryTmpAppService;

        public StatutoryTmpController(IStatutoryTmpAppService statutoryTmpAppService)
        {
            _statutoryTmpAppService = statutoryTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new StatutoryTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_StatutoryTmp_Create, AppPermissions.Pages_StatutoryTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetStatutoryTmpForEditOutput getStatutoryTmpForEditOutput;

            if (id.HasValue)
            {
                getStatutoryTmpForEditOutput = await _statutoryTmpAppService.GetStatutoryTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getStatutoryTmpForEditOutput = new GetStatutoryTmpForEditOutput
                {
                    StatutoryTmp = new CreateOrEditStatutoryTmpDto()
                };
            }

            var viewModel = new CreateOrEditStatutoryTmpModalViewModel()
            {
                StatutoryTmp = getStatutoryTmpForEditOutput.StatutoryTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewStatutoryTmpModal(int id)
        {
            var getStatutoryTmpForViewDto = await _statutoryTmpAppService.GetStatutoryTmpForView(id);

            var model = new StatutoryTmpViewModel()
            {
                StatutoryTmp = getStatutoryTmpForViewDto.StatutoryTmp
            };

            return PartialView("_ViewStatutoryTmpModal", model);
        }

    }
}