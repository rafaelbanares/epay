﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.EarningProcess;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EarningProcess)]
    public class EarningProcessController : ePayControllerBase
    {
        private readonly IEarningProcessAppService _earningProcessAppService;

        public EarningProcessController(IEarningProcessAppService earningProcessAppService)
        {
            _earningProcessAppService = earningProcessAppService;
        }

        public ActionResult Index()
        {
            var model = new EarningProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EarningProcess_Create, AppPermissions.Pages_EarningProcess_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEarningProcessForEditOutput getEarningProcessForEditOutput;

            if (id.HasValue)
            {
                getEarningProcessForEditOutput = await _earningProcessAppService.GetEarningProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEarningProcessForEditOutput = new GetEarningProcessForEditOutput
                {
                    EarningProcess = new CreateOrEditEarningProcessDto()
                };
            }

            var viewModel = new CreateOrEditEarningProcessModalViewModel()
            {
                EarningProcess = getEarningProcessForEditOutput.EarningProcess,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEarningProcessModal(int id)
        {
            var getEarningProcessForViewDto = await _earningProcessAppService.GetEarningProcessForView(id);

            var model = new EarningProcessViewModel()
            {
                EarningProcess = getEarningProcessForViewDto.EarningProcess
            };

            return PartialView("_ViewEarningProcessModal", model);
        }

    }
}