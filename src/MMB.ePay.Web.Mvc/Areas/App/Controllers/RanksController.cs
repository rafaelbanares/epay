﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Ranks;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Ranks)]
    public class RanksController : ePayControllerBase
    {
        private readonly IRanksAppService _ranksAppService;

        public RanksController(IRanksAppService ranksAppService)
        {
            _ranksAppService = ranksAppService;
        }

        public ActionResult Index()
        {
            var model = new RanksViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Ranks_Create, AppPermissions.Pages_Ranks_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetRanksForEditOutput getRanksForEditOutput;

            if (id.HasValue)
            {
                getRanksForEditOutput = await _ranksAppService.GetRanksForEdit(new EntityDto { Id = id.Value });
            }
            else
            {
                getRanksForEditOutput = new GetRanksForEditOutput
                {
                    Ranks = new CreateOrEditRanksDto()
                };
            }

            var viewModel = new CreateOrEditRanksModalViewModel()
            {
                Ranks = getRanksForEditOutput.Ranks,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRanksModal(int id)
        {
            var getRanksForViewDto = await _ranksAppService.GetRanksForView(id);

            var model = new RanksViewModel()
            {
                Ranks = getRanksForViewDto.Ranks
            };

            return PartialView("_ViewRanksModal", model);
        }

    }
}