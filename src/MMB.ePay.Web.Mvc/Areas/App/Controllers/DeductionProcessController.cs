﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.DeductionProcess;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_DeductionProcess)]
    public class DeductionProcessController : ePayControllerBase
    {
        private readonly IDeductionProcessAppService _deductionProcessAppService;

        public DeductionProcessController(IDeductionProcessAppService deductionProcessAppService)
        {
            _deductionProcessAppService = deductionProcessAppService;
        }

        public ActionResult Index()
        {
            var model = new DeductionProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_DeductionProcess_Create, AppPermissions.Pages_DeductionProcess_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetDeductionProcessForEditOutput getDeductionProcessForEditOutput;

            if (id.HasValue)
            {
                getDeductionProcessForEditOutput = await _deductionProcessAppService.GetDeductionProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getDeductionProcessForEditOutput = new GetDeductionProcessForEditOutput
                {
                    DeductionProcess = new CreateOrEditDeductionProcessDto()
                };
            }

            var viewModel = new CreateOrEditDeductionProcessModalViewModel()
            {
                DeductionProcess = getDeductionProcessForEditOutput.DeductionProcess,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewDeductionProcessModal(int id)
        {
            var getDeductionProcessForViewDto = await _deductionProcessAppService.GetDeductionProcessForView(id);

            var model = new DeductionProcessViewModel()
            {
                DeductionProcess = getDeductionProcessForViewDto.DeductionProcess
            };

            return PartialView("_ViewDeductionProcessModal", model);
        }

    }
}