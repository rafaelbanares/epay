﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Persons;
using MMB.ePay.Persons.Dto;
using MMB.ePay.Web.Areas.App.Models.Persons;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_ePay)]
    public class ePayController : ePayControllerBase
    {
        private readonly IPersonAppService _personAppService;

        public ePayController(IPersonAppService personAppService)
        {
            _personAppService = personAppService;
        }

        public IActionResult Index(GetPeopleInput input)
        {
            var output = _personAppService.GetPeople(input);
            var model = ObjectMapper.Map<PersonsIndexViewModel>(output);

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_ePay_CreatePerson)]
        public PartialViewResult CreatePersonModal()
        {
            return PartialView("_CreatePersonModal");
        }
    }
}
