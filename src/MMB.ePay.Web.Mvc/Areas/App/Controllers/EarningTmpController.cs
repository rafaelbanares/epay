﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.EarningTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EarningTmp)]
    public class EarningTmpController : ePayControllerBase
    {
        private readonly IEarningTmpAppService _earningTmpAppService;

        public EarningTmpController(IEarningTmpAppService earningTmpAppService)
        {
            _earningTmpAppService = earningTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new EarningTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EarningTmp_Create, AppPermissions.Pages_EarningTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEarningTmpForEditOutput getEarningTmpForEditOutput;

            if (id.HasValue)
            {
                getEarningTmpForEditOutput = await _earningTmpAppService.GetEarningTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEarningTmpForEditOutput = new GetEarningTmpForEditOutput
                {
                    EarningTmp = new CreateOrEditEarningTmpDto()
                };
            }

            var viewModel = new CreateOrEditEarningTmpModalViewModel()
            {
                EarningTmp = getEarningTmpForEditOutput.EarningTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEarningTmpModal(int id)
        {
            var getEarningTmpForViewDto = await _earningTmpAppService.GetEarningTmpForView(id);

            var model = new EarningTmpViewModel()
            {
                EarningTmp = getEarningTmpForViewDto.EarningTmp
            };

            return PartialView("_ViewEarningTmpModal", model);
        }

    }
}