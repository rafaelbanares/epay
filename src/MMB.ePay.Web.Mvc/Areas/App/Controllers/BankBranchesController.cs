﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.BankBranches;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_BankBranches)]
    public class BankBranchesController : ePayControllerBase
    {
        private readonly IBankBranchesAppService _bankBranchesAppService;

        public BankBranchesController(IBankBranchesAppService bankBranchesAppService)
        {
            _bankBranchesAppService = bankBranchesAppService;
        }

        public ActionResult Index(string id)
        {
            var model = new BankBranchesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_BankBranches_Create, AppPermissions.Pages_BankBranches_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, string bankId)
        {
            GetBankBranchesForEditOutput getBankBranchesForEditOutput;

            if (id.HasValue)
            {
                getBankBranchesForEditOutput = await _bankBranchesAppService.GetBankBranchesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getBankBranchesForEditOutput = await _bankBranchesAppService.GetBankBranchesForCreate(bankId);

                //getBankBranchesForEditOutput = new GetBankBranchesForEditOutput
                //{
                //    BankBranches = new CreateOrEditBankBranchesDto{ BankId = bankId }
                //};
            }

            var viewModel = new CreateOrEditBankBranchesModalViewModel()
            {
                BankBranches = getBankBranchesForEditOutput.BankBranches,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewBankBranchesModal(int id)
        {
            var getBankBranchesForViewDto = await _bankBranchesAppService.GetBankBranchesForView(id);

            var model = new BankBranchesViewModel()
            {
                BankBranches = getBankBranchesForViewDto.BankBranches
            };

            return PartialView("_ViewBankBranchesModal", model);
        }

    }
}