﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.Loans;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;
using MMB.ePay.HR;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Loans)]
    public class LoansController : ePayControllerBase
    {
        private readonly ILoansAppService _loansAppService;
        private readonly ILoanTypesAppService _loanTypesAppService;
        private readonly IEmployeesAppService _employeesAppService;

        public LoansController(ILoansAppService loansAppService, 
            ILoanTypesAppService loanTypesAppService, 
            IEmployeesAppService employeesAppService)
        {
            _loansAppService = loansAppService;
            _loanTypesAppService = loanTypesAppService;
            _employeesAppService = employeesAppService;
        }

        public ActionResult Index()
        {
            var model = new LoansViewModel();

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Loans_Create, AppPermissions.Pages_Loans_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLoansForEditOutput getLoansForEditOutput;

            if (id.HasValue)
            {
                getLoansForEditOutput = await _loansAppService.GetLoansForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLoansForEditOutput = new GetLoansForEditOutput
                {
                    Loans = new CreateOrEditLoansDto()
                };
            }

            var viewModel = new CreateOrEditLoansModalViewModel()
            {
                Loans = getLoansForEditOutput.Loans,
                LoanTypeList = await _loanTypesAppService.GetLoanTypeList(),
                CurrencyList = _employeesAppService.GetCurrencyList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLoansModal(int id)
        {
            var getLoansForViewDto = await _loansAppService.GetLoansForView(id);

            var model = new LoansViewModel()
            {
                Loans = getLoansForViewDto.Loans
            };

            return PartialView("_ViewLoansModal", model);
        }

    }
}