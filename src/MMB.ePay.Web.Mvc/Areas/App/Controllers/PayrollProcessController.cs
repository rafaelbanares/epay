﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.PayrollProcess;
using MMB.ePay.Web.Areas.App.Models.Process;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_PayrollProcess)]
    public class PayrollProcessController : ePayControllerBase
    {
        private readonly IProcessAppService _processAppService;
        private readonly IPayPeriodAppService _payPeriodAppService;
        private readonly IHelperAppService _helperAppService;

        public PayrollProcessController(
            IProcessAppService processAppService,
            IPayPeriodAppService payPeriodAppService, 
            IHelperAppService helperAppService)
        {
            _processAppService = processAppService;
            _payPeriodAppService = payPeriodAppService;
            _helperAppService = helperAppService;
        }

        [HttpGet]
        public async Task<JsonResult> GetPayrollNo(string processType, string payrollFreq, int applicableYear)
        {
            var input = new GetPayPeriodForProcessInput
            {
                ProcessType = processType, 
                PayrollFreq = payrollFreq,
                ApplicableYear = applicableYear
            };

            return Json(await _payPeriodAppService.GetPayPeriodForProcess(input));
        }

        public async Task<IActionResult> Index()
        {
            var processes = await _processAppService.GetProcesses();
            var model = new PayrollProcessViewModel
            {
                IsReviewer = processes.IsReviewer,
                MyProcesses = processes.MyProcesses,
                MyReviews = processes.MyReviews,
                OtherProcesses = processes.OtherProcesses
            };

            return View(model);
        }

        public async Task<IActionResult> Processing(int id)
        {
            var model = new PayrollProcessProcessingViewModel
            {
                Process = await _processAppService.GetProcessForProcessingView(id)
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Process_Create)]
        public async Task<PartialViewResult> CreateOrEditModal()
        {
            var getProcessForCreateOutput = await _processAppService.GetProcessForCreate();

            var viewModel = new CreateOrEditProcessModalViewModel()
            {
                Process = getProcessForCreateOutput.Process,
                PayrollFreqList = _helperAppService.GetFrequencyList(),
                ProcessTypeList = _processAppService.GetProcessTypeList(),
                YearList = await _payPeriodAppService.GetApplicableYearList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        //public async Task<PartialViewResult> ViewPayrollProcessModal(int id)
        //{
        //    var getPayrollProcessForViewDto = await _payrollProcessAppService.GetPayrollProcessForView(id);

        //    var model = new PayrollProcessViewModel()
        //    {
        //        PayrollProcess = getPayrollProcessForViewDto.PayrollProcess
        //    };

        //    return PartialView("_ViewPayrollProcessModal", model);
        //}

    }
}