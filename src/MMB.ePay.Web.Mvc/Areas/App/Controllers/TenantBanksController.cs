﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.TenantBanks;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TenantBanks)]
    public class TenantBanksController : ePayControllerBase
    {
        private readonly ITenantBanksAppService _tenantBanksAppService;
        private readonly IBanksAppService _banksAppService;
        private readonly IBankBranchesAppService _bankBranchesAppService;

        public TenantBanksController(ITenantBanksAppService tenantBanksAppService,
            IBanksAppService banksAppService,
            IBankBranchesAppService bankBranchesAppService)
        {
            _tenantBanksAppService = tenantBanksAppService;
            _banksAppService = banksAppService;
            _bankBranchesAppService = bankBranchesAppService;
        }

        [HttpGet]   
        public async Task<JsonResult> PopulateBankBranches(string bankId)
        {
            return Json(await _bankBranchesAppService.GetBankBranchList(bankId));
        }

        public ActionResult Index()
        {
            var model = new TenantBanksViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TenantBanks_Create, AppPermissions.Pages_TenantBanks_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetTenantBanksForEditOutput getTenantBanksForEditOutput;

            var viewModel = new CreateOrEditTenantBanksModalViewModel()
            {
                BankList = await _banksAppService.GetBankList(),
                BankAccountTypeList = _banksAppService.GetBankAccountTypeList()
            };

            if (id.HasValue)
            {
                getTenantBanksForEditOutput = await _tenantBanksAppService.GetTenantBanksForEdit(new EntityDto { Id = (int)id });

                viewModel.BankBranchList = await _bankBranchesAppService
                    .GetBankBranchList(getTenantBanksForEditOutput.TenantBanks.BankId);
            }
            else
            {
                getTenantBanksForEditOutput = new GetTenantBanksForEditOutput
                {
                    TenantBanks = new CreateOrEditTenantBanksDto()
                };
            }

            viewModel.TenantBanks = getTenantBanksForEditOutput.TenantBanks;

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewTenantBanksModal(int id)
        {
            var getTenantBanksForViewDto = await _tenantBanksAppService.GetTenantBanksForView(id);

            var model = new TenantBanksViewModel()
            {
                TenantBanks = getTenantBanksForViewDto.TenantBanks
            };

            return PartialView("_ViewTenantBanksModal", model);
        }

    }
}