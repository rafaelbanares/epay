﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.RecurEarnings;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_RecurEarnings)]
    public class RecurEarningsController : ePayControllerBase
    {
        private readonly IRecurEarningsAppService _recurEarningsAppService;
        private readonly IEarningTypesAppService _earningTypesAppService;

        public RecurEarningsController(IRecurEarningsAppService recurEarningsAppService, IEarningTypesAppService earningTypesAppService)
        {
            _recurEarningsAppService = recurEarningsAppService;
            _earningTypesAppService = earningTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new RecurEarningsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_RecurEarnings_Create, AppPermissions.Pages_RecurEarnings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? employeeId)
        {
            GetRecurEarningsForEditOutput getRecurEarningsForCreateOrEditOutput;

            if (id.HasValue)
            {
                getRecurEarningsForCreateOrEditOutput = await _recurEarningsAppService.GetRecurEarningsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getRecurEarningsForCreateOrEditOutput = await _recurEarningsAppService.GetRecurEarningsForCreate(employeeId.Value);
            }

            var viewModel = new CreateOrEditRecurEarningsModalViewModel()
            {
                RecurEarnings = getRecurEarningsForCreateOrEditOutput.RecurEarnings,
                EarningTypeList = await _earningTypesAppService.GetEarningTypeListForRecurEarnings(id, employeeId)
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRecurEarningsModal(int id)
        {
            var getRecurEarningsForViewDto = await _recurEarningsAppService.GetRecurEarningsForView(id);

            var model = new RecurEarningsViewModel()
            {
                RecurEarnings = getRecurEarningsForViewDto.RecurEarnings
            };

            return PartialView("_ViewRecurEarningsModal", model);
        }

    }
}