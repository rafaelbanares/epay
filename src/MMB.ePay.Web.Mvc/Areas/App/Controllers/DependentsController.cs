﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Dependents;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Dependents)]
    public class DependentsController : ePayControllerBase
    {
        private readonly IDependentsAppService _dependentsAppService;

        public DependentsController(IDependentsAppService dependentsAppService)
        {
            _dependentsAppService = dependentsAppService;
        }

        public ActionResult Index()
        {
            var model = new DependentsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Dependents_Create, AppPermissions.Pages_Dependents_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? employeeId)
        {
            GetDependentsForEditOutput getDependentsForEditOutput;

            if (id.HasValue)
            {
                getDependentsForEditOutput = await _dependentsAppService.GetDependentsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getDependentsForEditOutput = new GetDependentsForEditOutput
                {
                    Dependents = new CreateOrEditDependentsDto
                    {
                        EmployeeId = employeeId.Value
                    }
                };
            }

            var viewModel = new CreateOrEditDependentsModalViewModel()
            {
                Dependents = getDependentsForEditOutput.Dependents,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewDependentsModal(int id)
        {
            var getDependentsForViewDto = await _dependentsAppService.GetDependentsForView(id);

            var model = new DependentsViewModel()
            {
                Dependents = getDependentsForViewDto.Dependents
            };

            return PartialView("_ViewDependentsModal", model);
        }

    }
}