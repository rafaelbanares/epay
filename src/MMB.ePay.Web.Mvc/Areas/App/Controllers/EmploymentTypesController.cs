﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Web.Areas.App.Models.EmploymentTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EmploymentTypes)]
    public class EmploymentTypesController : ePayControllerBase
    {
        private readonly IEmploymentTypesAppService _employmentTypesAppService;

        public EmploymentTypesController(IEmploymentTypesAppService employmentTypesAppService)
        {
            _employmentTypesAppService = employmentTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new EmploymentTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EmploymentTypes_Create, AppPermissions.Pages_EmploymentTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(string id)
        {
            GetEmploymentTypesForEditOutput getEmploymentTypesForEditOutput;

            if (!string.IsNullOrWhiteSpace(id))
            {
                getEmploymentTypesForEditOutput = await _employmentTypesAppService.GetEmploymentTypesForEdit(new EntityDto<string> { Id = id });
            }
            else
            {
                getEmploymentTypesForEditOutput = new GetEmploymentTypesForEditOutput
                {
                    EmploymentTypes = new CreateOrEditEmploymentTypesDto()
                };
            }

            var viewModel = new CreateOrEditEmploymentTypesModalViewModel()
            {
                EmploymentTypes = getEmploymentTypesForEditOutput.EmploymentTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmploymentTypesModal(string id)
        {
            var getEmploymentTypesForViewDto = await _employmentTypesAppService.GetEmploymentTypesForView(id);

            var model = new EmploymentTypesViewModel()
            {
                EmploymentTypes = getEmploymentTypesForViewDto.EmploymentTypes
            };

            return PartialView("_ViewEmploymentTypesModal", model);
        }

    }
}