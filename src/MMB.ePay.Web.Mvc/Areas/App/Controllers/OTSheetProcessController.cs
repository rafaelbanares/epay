﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.OTSheetProcess;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OTSheetProcess)]
    public class OTSheetProcessController : ePayControllerBase
    {
        private readonly IOTSheetProcessAppService _otSheetProcessAppService;

        public OTSheetProcessController(IOTSheetProcessAppService otSheetProcessAppService)
        {
            _otSheetProcessAppService = otSheetProcessAppService;
        }

        public ActionResult Index()
        {
            var model = new OTSheetProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_OTSheetProcess_Create, AppPermissions.Pages_OTSheetProcess_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetOTSheetProcessForEditOutput getOTSheetProcessForEditOutput;

            if (id.HasValue)
            {
                getOTSheetProcessForEditOutput = await _otSheetProcessAppService.GetOTSheetProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getOTSheetProcessForEditOutput = new GetOTSheetProcessForEditOutput
                {
                    OTSheetProcess = new CreateOrEditOTSheetProcessDto()
                };
            }

            var viewModel = new CreateOrEditOTSheetProcessModalViewModel()
            {
                OTSheetProcess = getOTSheetProcessForEditOutput.OTSheetProcess,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOTSheetProcessModal(int id)
        {
            var getOTSheetProcessForViewDto = await _otSheetProcessAppService.GetOTSheetProcessForView(id);

            var model = new OTSheetProcessViewModel()
            {
                OTSheetProcess = getOTSheetProcessForViewDto.OTSheetProcess
            };

            return PartialView("_ViewOTSheetProcessModal", model);
        }

    }
}