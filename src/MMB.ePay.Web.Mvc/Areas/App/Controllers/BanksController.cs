﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.Banks;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Banks)]
    public class BanksController : ePayControllerBase
    {
        private readonly IBanksAppService _banksAppService;

        public BanksController(IBanksAppService banksAppService)
        {
            _banksAppService = banksAppService;
        }

        public ActionResult Index()
        {
            var model = new BanksViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Banks_Create, AppPermissions.Pages_Banks_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(string id)
        {
            GetBanksForEditOutput getBanksForEditOutput;

            if (!id.IsNullOrWhiteSpace())
            {
                getBanksForEditOutput = await _banksAppService.GetBanksForEdit(new EntityDto<string> { Id = (string)id });
            }
            else
            {
                getBanksForEditOutput = new GetBanksForEditOutput
                {
                    Banks = new CreateOrEditBanksDto()
                };
            }

            var viewModel = new CreateOrEditBanksModalViewModel()
            {
                Banks = getBanksForEditOutput.Banks,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewBanksModal(string id)
        {
            var getBanksForViewDto = await _banksAppService.GetBanksForView(id);

            var model = new BanksViewModel()
            {
                Banks = getBanksForViewDto.Banks
            };

            return PartialView("_ViewBanksModal", model);
        }

    }
}