﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.PayrollPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_PayrollPosted)]
    public class PayrollPostedController : ePayControllerBase
    {
        private readonly IPayrollPostedAppService _payrollPostedAppService;

        public PayrollPostedController(IPayrollPostedAppService payrollPostedAppService)
        {
            _payrollPostedAppService = payrollPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new PayrollPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_PayrollPosted_Create, AppPermissions.Pages_PayrollPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetPayrollPostedForEditOutput getPayrollPostedForEditOutput;

            if (id.HasValue)
            {
                getPayrollPostedForEditOutput = await _payrollPostedAppService.GetPayrollPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getPayrollPostedForEditOutput = new GetPayrollPostedForEditOutput
                {
                    PayrollPosted = new CreateOrEditPayrollPostedDto()
                };
            }

            var viewModel = new CreateOrEditPayrollPostedModalViewModel()
            {
                PayrollPosted = getPayrollPostedForEditOutput.PayrollPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewPayrollPostedModal(int id)
        {
            var getPayrollPostedForViewDto = await _payrollPostedAppService.GetPayrollPostedForView(id);

            var model = new PayrollPostedViewModel()
            {
                PayrollPosted = getPayrollPostedForViewDto.PayrollPosted
            };

            return PartialView("_ViewPayrollPostedModal", model);
        }

    }
}