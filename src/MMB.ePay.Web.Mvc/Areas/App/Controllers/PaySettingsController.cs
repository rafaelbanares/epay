﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.PaySettings;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_PaySettings)]
    public class PaySettingsController : ePayControllerBase
    {
        private readonly IPaySettingsAppService _paySettingsAppService;

        public PaySettingsController(IPaySettingsAppService paySettingsAppService)
        {
            _paySettingsAppService = paySettingsAppService;
        }

        public ActionResult Index()
        {
            var model = new PaySettingsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_PaySettings_Create, AppPermissions.Pages_PaySettings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetPaySettingsForEditOutput getPaySettingsForEditOutput;

            if (id.HasValue)
            {
                getPaySettingsForEditOutput = await _paySettingsAppService.GetPaySettingsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getPaySettingsForEditOutput = new GetPaySettingsForEditOutput
                {
                    PaySettings = new CreateOrEditPaySettingsDto()
                };
            }

            var viewModel = new CreateOrEditPaySettingsModalViewModel()
            {
                PaySettings = getPaySettingsForEditOutput.PaySettings,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewPaySettingsModal(int id)
        {
            var getPaySettingsForViewDto = await _paySettingsAppService.GetPaySettingsForView(id);

            var model = new PaySettingsViewModel()
            {
                PaySettings = getPaySettingsForViewDto.PaySettings
            };

            return PartialView("_ViewPaySettingsModal", model);
        }

    }
}