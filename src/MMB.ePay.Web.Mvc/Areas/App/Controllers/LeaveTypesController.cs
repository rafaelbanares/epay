﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.LeaveTypes;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LeaveTypes)]
    public class LeaveTypesController : ePayControllerBase
    {
        private readonly ILeaveTypesAppService _leaveTypesAppService;

        public LeaveTypesController(ILeaveTypesAppService leaveTypesAppService)
        {
            _leaveTypesAppService = leaveTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new LeaveTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveTypes_Create, AppPermissions.Pages_LeaveTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLeaveTypesForEditOutput getLeaveTypesForEditOutput;

            if (id.HasValue)
            {
                getLeaveTypesForEditOutput = await _leaveTypesAppService.GetLeaveTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLeaveTypesForEditOutput = new GetLeaveTypesForEditOutput
                {
                    LeaveTypes = new CreateOrEditLeaveTypesDto()
                };
            }

            var viewModel = new CreateOrEditLeaveTypesModalViewModel()
            {
                LeaveTypes = getLeaveTypesForEditOutput.LeaveTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLeaveTypesModal(int id)
        {
            var getLeaveTypesForViewDto = await _leaveTypesAppService.GetLeaveTypesForView(id);

            var model = new LeaveTypesViewModel()
            {
                LeaveTypes = getLeaveTypesForViewDto.LeaveTypes
            };

            return PartialView("_ViewLeaveTypesModal", model);
        }

    }
}