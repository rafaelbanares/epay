﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.LoanProcess;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LoanProcess)]
    public class LoanProcessController : ePayControllerBase
    {
        private readonly ILoanProcessAppService _loanProcessAppService;

        public LoanProcessController(ILoanProcessAppService loanProcessAppService)
        {
            _loanProcessAppService = loanProcessAppService;
        }

        public ActionResult Index()
        {
            var model = new LoanProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LoanProcess_Create, AppPermissions.Pages_LoanProcess_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLoanProcessForEditOutput getLoanProcessForEditOutput;

            if (id.HasValue)
            {
                getLoanProcessForEditOutput = await _loanProcessAppService.GetLoanProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLoanProcessForEditOutput = new GetLoanProcessForEditOutput
                {
                    LoanProcess = new CreateOrEditLoanProcessDto()
                };
            }

            var viewModel = new CreateOrEditLoanProcessModalViewModel()
            {
                LoanProcess = getLoanProcessForEditOutput.LoanProcess,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLoanProcessModal(int id)
        {
            var getLoanProcessForViewDto = await _loanProcessAppService.GetLoanProcessForView(id);

            var model = new LoanProcessViewModel()
            {
                LoanProcess = getLoanProcessForViewDto.LoanProcess
            };

            return PartialView("_ViewLoanProcessModal", model);
        }

    }
}