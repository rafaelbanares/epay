﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.TimesheetTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TimesheetTmp)]
    public class TimesheetTmpController : ePayControllerBase
    {
        private readonly ITimesheetTmpAppService _timesheetTmpAppService;

        public TimesheetTmpController(ITimesheetTmpAppService timesheetTmpAppService)
        {
            _timesheetTmpAppService = timesheetTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new TimesheetTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TimesheetTmp_Create, AppPermissions.Pages_TimesheetTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetTimesheetTmpForEditOutput getTimesheetTmpForEditOutput;

            if (id.HasValue)
            {
                getTimesheetTmpForEditOutput = await _timesheetTmpAppService.GetTimesheetTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getTimesheetTmpForEditOutput = new GetTimesheetTmpForEditOutput
                {
                    TimesheetTmp = new CreateOrEditTimesheetTmpDto()
                };
            }

            var viewModel = new CreateOrEditTimesheetTmpModalViewModel()
            {
                TimesheetTmp = getTimesheetTmpForEditOutput.TimesheetTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewTimesheetTmpModal(int id)
        {
            var getTimesheetTmpForViewDto = await _timesheetTmpAppService.GetTimesheetTmpForView(id);

            var model = new TimesheetTmpViewModel()
            {
                TimesheetTmp = getTimesheetTmpForViewDto.TimesheetTmp
            };

            return PartialView("_ViewTimesheetTmpModal", model);
        }

    }
}