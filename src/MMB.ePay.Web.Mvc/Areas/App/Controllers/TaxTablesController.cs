﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.TaxTables;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TaxTables)]
    public class TaxTablesController : ePayControllerBase
    {
        private readonly ITaxTablesAppService _taxTablesAppService;
        private readonly IHelperAppService _helperAppService;

        public TaxTablesController(ITaxTablesAppService taxTablesAppService, IHelperAppService helperAppService)
        {
            _taxTablesAppService = taxTablesAppService;
            _helperAppService = helperAppService;
        }

        public ActionResult Index()
        {
            var model = new TaxTablesViewModel
            {
                YearList = _taxTablesAppService.GetYearList(),
                PayFreqList = _helperAppService.GetFrequencyList()
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TaxTables_Create, AppPermissions.Pages_TaxTables_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetTaxTablesForEditOutput getTaxTablesForEditOutput;

            if (id.HasValue)
            {
                getTaxTablesForEditOutput = await _taxTablesAppService.GetTaxTablesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getTaxTablesForEditOutput = new GetTaxTablesForEditOutput
                {
                    TaxTables = new CreateOrEditTaxTablesDto()
                };
            }

            var viewModel = new CreateOrEditTaxTablesModalViewModel()
            {
                TaxTables = getTaxTablesForEditOutput.TaxTables,
                YearList = _taxTablesAppService.GetYearList(),
                PayFreqList = _helperAppService.GetFrequencyList(),
                CodeList = _taxTablesAppService.GetCodeList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewTaxTablesModal(int id)
        {
            var getTaxTablesForViewDto = await _taxTablesAppService.GetTaxTablesForView(id);

            var model = new TaxTablesViewModel()
            {
                TaxTables = getTaxTablesForViewDto.TaxTables
            };

            return PartialView("_ViewTaxTablesModal", model);
        }

    }
}