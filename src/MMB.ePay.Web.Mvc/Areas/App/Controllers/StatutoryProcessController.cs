﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.StatutoryProcess;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_StatutoryProcess)]
    public class StatutoryProcessController : ePayControllerBase
    {
        private readonly IStatutoryProcessAppService _statutoryProcessAppService;

        public StatutoryProcessController(IStatutoryProcessAppService statutoryProcessAppService)
        {
            _statutoryProcessAppService = statutoryProcessAppService;
        }

        public ActionResult Index()
        {
            var model = new StatutoryProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_StatutoryProcess_Create, AppPermissions.Pages_StatutoryProcess_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetStatutoryProcessForEditOutput getStatutoryProcessForEditOutput;

            if (id.HasValue)
            {
                getStatutoryProcessForEditOutput = await _statutoryProcessAppService.GetStatutoryProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getStatutoryProcessForEditOutput = new GetStatutoryProcessForEditOutput
                {
                    StatutoryProcess = new CreateOrEditStatutoryProcessDto()
                };
            }

            var viewModel = new CreateOrEditStatutoryProcessModalViewModel()
            {
                StatutoryProcess = getStatutoryProcessForEditOutput.StatutoryProcess,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewStatutoryProcessModal(int id)
        {
            var getStatutoryProcessForViewDto = await _statutoryProcessAppService.GetStatutoryProcessForView(id);

            var model = new StatutoryProcessViewModel()
            {
                StatutoryProcess = getStatutoryProcessForViewDto.StatutoryProcess
            };

            return PartialView("_ViewStatutoryProcessModal", model);
        }

    }
}