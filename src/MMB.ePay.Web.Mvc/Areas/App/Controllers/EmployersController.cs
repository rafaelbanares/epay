﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Employers;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Employers)]
    public class EmployersController : ePayControllerBase
    {
        private readonly IEmployersAppService _employersAppService;

        public EmployersController(IEmployersAppService employersAppService)
        {
            _employersAppService = employersAppService;
        }

        public ActionResult Index()
        {
            var model = new EmployersViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Employers_Create, AppPermissions.Pages_Employers_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? employeeId)
        {
            GetEmployersForEditOutput getEmployersForEditOutput;

            if (id.HasValue)
            {
                getEmployersForEditOutput = await _employersAppService.GetEmployersForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEmployersForEditOutput = new GetEmployersForEditOutput
                {
                    Employers = new CreateOrEditEmployersDto
                    {
                        EmployeeId = employeeId.Value
                    }
                };
            }

            var viewModel = new CreateOrEditEmployersModalViewModel()
            {
                Employers = getEmployersForEditOutput.Employers,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployersModal(int id)
        {
            var getEmployersForViewDto = await _employersAppService.GetEmployersForView(id);

            var model = new EmployersViewModel()
            {
                Employers = getEmployersForViewDto.Employers
            };

            return PartialView("_ViewEmployersModal", model);
        }

    }
}