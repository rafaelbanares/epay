﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.PayPeriod;
using MMB.ePay.Web.Controllers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_PayPeriod)]
    public class PayPeriodController : ePayControllerBase
    {
        private readonly IPayPeriodAppService _payPeriodAppService;
        private readonly IHelperAppService _helperAppService;

        public PayPeriodController(IPayPeriodAppService payPeriodAppService, IHelperAppService helperAppService)
        {
            _payPeriodAppService = payPeriodAppService;
            _helperAppService = helperAppService;
        }

        public async Task<ActionResult> Index()
        {
            var model = new PayPeriodViewModel
            {
                ApplicableYearList = await _payPeriodAppService.GetApplicableYearList(),
                PayrollFreqList = _helperAppService.GetFrequencyList()
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_PayPeriod_Create, AppPermissions.Pages_PayPeriod_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetPayPeriodForEditOutput getPayPeriodForEditOutput;

            if (id.HasValue)
            {
                getPayPeriodForEditOutput = await _payPeriodAppService.GetPayPeriodForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getPayPeriodForEditOutput = new GetPayPeriodForEditOutput
                {
                    PayPeriod = new CreateOrEditPayPeriodDto()
                };
            }

            var currentMonth = DateTime.UtcNow.Month.ToString();
            var appMonthList = _helperAppService.GetMonthList();
            appMonthList = appMonthList.Select(e =>
            {
                e.Selected = e.Value == currentMonth;
                return e;
            }).ToList();

            var viewModel = new CreateOrEditPayPeriodModalViewModel()
            {
                PayPeriod = getPayPeriodForEditOutput.PayPeriod,
                PayrollFreqList = _helperAppService.GetFrequencyList(),
                AppMonthList = _helperAppService.GetMonthList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewPayPeriodModal(int id)
        {
            var getPayPeriodForViewDto = await _payPeriodAppService.GetPayPeriodForView(id);

            var model = new PayPeriodViewModel()
            {
                PayPeriod = getPayPeriodForViewDto.PayPeriod
            };

            return PartialView("_ViewPayPeriodModal", model);
        }

    }
}