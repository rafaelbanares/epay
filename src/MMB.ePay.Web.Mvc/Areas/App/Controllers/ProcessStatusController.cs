﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.ProcessStatus;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ProcessStatus)]
    public class ProcessStatusController : ePayControllerBase
    {
        private readonly IProcessStatusAppService _processStatusAppService;

        public ProcessStatusController(IProcessStatusAppService processStatusAppService)
        {
            _processStatusAppService = processStatusAppService;
        }

        public ActionResult Index()
        {
            var model = new ProcessStatusViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ProcessStatus_Create, AppPermissions.Pages_ProcessStatus_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetProcessStatusForEditOutput getProcessStatusForEditOutput;

            if (id.HasValue)
            {
                getProcessStatusForEditOutput = await _processStatusAppService.GetProcessStatusForEdit(new EntityDto { Id = id.Value });
            }
            else
            {
                getProcessStatusForEditOutput = new GetProcessStatusForEditOutput
                {
                    ProcessStatus = new CreateOrEditProcessStatusDto()
                };
            }

            var viewModel = new CreateOrEditProcessStatusModalViewModel()
            {
                ProcessStatus = getProcessStatusForEditOutput.ProcessStatus,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewProcessStatusModal(int id)
        {
            var getProcessStatusForViewDto = await _processStatusAppService.GetProcessStatusForView(id);

            var model = new ProcessStatusViewModel()
            {
                ProcessStatus = getProcessStatusForViewDto.ProcessStatus
            };

            return PartialView("_ViewProcessStatusModal", model);
        }

    }
}