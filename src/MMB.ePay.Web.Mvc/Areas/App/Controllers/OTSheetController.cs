﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Web.Areas.App.Models.OTSheet;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OTSheet)]
    public class OTSheetController : ePayControllerBase
    {
        private readonly IOTSheetAppService _otSheetAppService;

        public OTSheetController(IOTSheetAppService otSheetAppService)
        {
            _otSheetAppService = otSheetAppService;
        }

        public ActionResult Index()
        {
            var model = new OTSheetViewModel
            {
                FilterText = ""
            };

            return View(model);
        }
    }
}