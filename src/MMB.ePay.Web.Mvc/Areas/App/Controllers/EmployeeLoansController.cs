﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.EmployeeLoans;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EmployeeLoans)]
    public class EmployeeLoansController : ePayControllerBase
    {
        private readonly IEmployeeLoansAppService _employeeLoansAppService;
        private readonly ILoansAppService _loansAppService;
        private readonly IPayPeriodAppService _payPeriodAppService;

        public EmployeeLoansController(
            IEmployeeLoansAppService employeeLoansAppService, 
            ILoansAppService loansAppService,
            IPayPeriodAppService payPeriodAppService)
        {
            _employeeLoansAppService = employeeLoansAppService;
            _loansAppService = loansAppService;
            _payPeriodAppService = payPeriodAppService;
        }

        public ActionResult Index()
        {
            return View(new EmployeeLoansViewModel
            {
                StatusList = _payPeriodAppService.GetPayrollStatusList(),
                YearList = _payPeriodAppService.GetPayrollYearList()
            });
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EmployeeLoans_Create, AppPermissions.Pages_EmployeeLoans_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(Guid? id)
        {
            GetEmployeeLoansForEditOutput getEmployeeLoansForEditOutput;

            if (id.HasValue)
            {
                getEmployeeLoansForEditOutput = await _employeeLoansAppService.GetEmployeeLoansForEdit(new EntityDto<Guid> { Id = (Guid)id });
            }
            else
            {
                getEmployeeLoansForEditOutput = new GetEmployeeLoansForEditOutput
                {
                    EmployeeLoans = new CreateOrEditEmployeeLoansDto()
                };
            }

            var employeeLoans = getEmployeeLoansForEditOutput.EmployeeLoans;

            var viewModel = new CreateOrEditEmployeeLoansModalViewModel()
            {
                EmployeeLoans = employeeLoans,
                LoanList = await _loansAppService.GetLoanList()
            };
            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployeeLoansModal(Guid id)
        {
            var getEmployeeLoansForViewDto = await _employeeLoansAppService.GetEmployeeLoansForView(id);

            var model = new EmployeeLoansViewModel()
            {
                EmployeeLoans = getEmployeeLoansForViewDto.EmployeeLoans
            };

            return PartialView("_ViewEmployeeLoansModal", model);
        }

    }
}