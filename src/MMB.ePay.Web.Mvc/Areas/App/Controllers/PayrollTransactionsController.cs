﻿using Microsoft.AspNetCore.Mvc;
using MMB.ePay.HR;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.PayrollTransactions;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    public class PayrollTransactionsController : ePayControllerBase
    {
        private readonly IEmployeesAppService _employeesAppService;
        private readonly ITimesheetsAppService _timesheetsAppService;
        private readonly ITimesheetTypesAppService _timesheetTypesAppService;
        private readonly IOTSheetAppService _otsheetAppService;
        private readonly IOvertimeMainTypesAppService _overtimeMainTypesAppService;
        private readonly IOvertimeSubTypesAppService _overtimeSubTypesAppService;

        public PayrollTransactionsController(IEmployeesAppService employeesAppService,
            ITimesheetsAppService timesheetsAppService,
            ITimesheetTypesAppService timesheetTypesAppService,
            IOTSheetAppService otsheetAppService,
            IOvertimeMainTypesAppService overtimeMainTypesAppService,
            IOvertimeSubTypesAppService overtimeSubTypesAppService)
        {
            _employeesAppService = employeesAppService;
            _timesheetsAppService = timesheetsAppService;
            _timesheetTypesAppService = timesheetTypesAppService;
            _otsheetAppService = otsheetAppService;
            _overtimeMainTypesAppService = overtimeMainTypesAppService;
            _overtimeSubTypesAppService = overtimeSubTypesAppService;
        }

        [HttpGet]
        public async Task<JsonResult> PopulateOTSheet(int employeeId)
        {
            return Json(await _otsheetAppService.GetAll(new GetAllOTSheetInput { EmployeeIdFilter = employeeId }));
        }

        [HttpGet]
        public async Task<JsonResult> PopulateTimesheets(int employeeId)
        {
            return Json(await _timesheetsAppService.GetAll(new GetAllTimesheetsInput { EmployeeIdFilter = employeeId }));
        }

        public async Task<IActionResult> Index()
        {
            return View(new PayrollTransactionsViewModel
            {
                EmployeeList = await _employeesAppService.GetEmployeeList(),
                TimesheetTypesMenu = await _timesheetTypesAppService.GetTimesheetTypesMenu(),
                OvertimeMainTypesMenu = await _overtimeMainTypesAppService.GetOvertimeMainTypesMenu(),
                OvertimeSubTypesMenu = await _overtimeSubTypesAppService.GetOvertimeSubTypesMenu()
            });
        }
    }
}
