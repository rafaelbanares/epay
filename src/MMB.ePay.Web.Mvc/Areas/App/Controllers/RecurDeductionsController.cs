﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.RecurDeductions;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_RecurDeductions)]
    public class RecurDeductionsController : ePayControllerBase
    {
        private readonly IRecurDeductionsAppService _recurDeductionsAppService;
        private readonly IDeductionTypesAppService _deductionTypesAppService;

        public RecurDeductionsController(IRecurDeductionsAppService recurDeductionsAppService, 
            IDeductionTypesAppService deductionTypesAppService)
        {
            _recurDeductionsAppService = recurDeductionsAppService;
            _deductionTypesAppService = deductionTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new RecurDeductionsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_RecurDeductions_Create, AppPermissions.Pages_RecurDeductions_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? employeeId)
        {
            GetRecurDeductionsForEditOutput getRecurDeductionsForEditOutput;

            if (id.HasValue)
            {
                getRecurDeductionsForEditOutput = await _recurDeductionsAppService.GetRecurDeductionsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getRecurDeductionsForEditOutput = await _recurDeductionsAppService.GetRecurDeductionsForCreate(employeeId.Value);

                //getRecurDeductionsForEditOutput = new GetRecurDeductionsForEditOutput
                //{
                //    RecurDeductions = new CreateOrEditRecurDeductionsDto
                //    {
                //        EmployeeId = employeeId.Value
                //    }
                //};
                //getRecurDeductionsForEditOutput.RecurDeductions.StartDate = DateTime.Now;
                //getRecurDeductionsForEditOutput.RecurDeductions.EndDate = DateTime.Now;
            }

            var viewModel = new CreateOrEditRecurDeductionsModalViewModel()
            {
                RecurDeductions = getRecurDeductionsForEditOutput.RecurDeductions,
                DeductionTypeList = await _deductionTypesAppService.GetDeductionTypeListForRecurDeductions(id, employeeId)
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRecurDeductionsModal(int id)
        {
            var getRecurDeductionsForViewDto = await _recurDeductionsAppService.GetRecurDeductionsForView(id);

            var model = new RecurDeductionsViewModel()
            {
                RecurDeductions = getRecurDeductionsForViewDto.RecurDeductions
            };

            return PartialView("_ViewRecurDeductionsModal", model);
        }

    }
}