﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Web.Areas.App.Models.OvertimeTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeTypes)]
    public class OvertimeTypesController : ePayControllerBase
    {
        private readonly IOvertimeTypesAppService _overtimeTypesAppService;

        public OvertimeTypesController(IOvertimeTypesAppService overtimeTypesAppService)
        {
            _overtimeTypesAppService = overtimeTypesAppService;
        }

        public async Task<IActionResult> Index(int id = 0)
        {
            var overtimeTypes = await _overtimeTypesAppService.GetAll();
            var model = new OvertimeTypesViewModel
            {
                FilterText = "",
                OvertimeTypes = overtimeTypes.OvertimeTypes,
                Group1 = overtimeTypes.Group1,
                Group2 = overtimeTypes.Group2,
                IsEditMode = id == 1
            };

            return View(model);
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_OvertimeTypes_Edit)]
        public async Task<ActionResult> Edit(OvertimeTypesViewModel viewModel)
        {
            await _overtimeTypesAppService.Update(viewModel.OvertimeTypes);

            return RedirectToAction(nameof(Index), new { id = 1 });
        }
    }
}