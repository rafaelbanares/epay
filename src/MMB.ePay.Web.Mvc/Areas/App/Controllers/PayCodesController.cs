﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.PayCodes;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_PayCodes)]
    public class PayCodesController : ePayControllerBase
    {
        private readonly IPayCodesAppService _payCodesAppService;
        private readonly IHelperAppService _helperAppService;

        public PayCodesController(IPayCodesAppService payCodesAppService, IHelperAppService helperAppService)
        {
            _payCodesAppService = payCodesAppService;
            _helperAppService = helperAppService;
        }

        public ActionResult Index()
        {
            var model = new PayCodesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_PayCodes_Create, AppPermissions.Pages_PayCodes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(Guid? id)
        {
            GetPayCodesForEditOutput getPayCodesForEditOutput;

            if (id.HasValue)
            {
                getPayCodesForEditOutput = await _payCodesAppService.GetPayCodesForEdit(new EntityDto<Guid> { Id = (Guid)id });
            }
            else
            {
                getPayCodesForEditOutput = new GetPayCodesForEditOutput
                {
                    PayCodes = new CreateOrEditPayCodesDto()
                };
            }

            var viewModel = new CreateOrEditPayCodesModalViewModel()
            {
                PayCodes = getPayCodesForEditOutput.PayCodes,
                PayFrequencyList = _helperAppService.GetFrequencyList(),
                TaxFrequencyList = _helperAppService.GetTaxFrequencyList(),
                TaxMethodList = _helperAppService.GetTaxMethodList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewPayCodesModal(Guid id)
        {
            var getPayCodesForViewDto = await _payCodesAppService.GetPayCodesForView(id);

            var model = new PayCodesViewModel()
            {
                PayCodes = getPayCodesForViewDto.PayCodes
            };

            return PartialView("_ViewPayCodesModal", model);
        }

    }
}