﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.LoanTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LoanTmp)]
    public class LoanTmpController : ePayControllerBase
    {
        private readonly ILoanTmpAppService _loanTmpAppService;

        public LoanTmpController(ILoanTmpAppService loanTmpAppService)
        {
            _loanTmpAppService = loanTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new LoanTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LoanTmp_Create, AppPermissions.Pages_LoanTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLoanTmpForEditOutput getLoanTmpForEditOutput;

            if (id.HasValue)
            {
                getLoanTmpForEditOutput = await _loanTmpAppService.GetLoanTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLoanTmpForEditOutput = new GetLoanTmpForEditOutput
                {
                    LoanTmp = new CreateOrEditLoanTmpDto()
                };
            }

            var viewModel = new CreateOrEditLoanTmpModalViewModel()
            {
                LoanTmp = getLoanTmpForEditOutput.LoanTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLoanTmpModal(int id)
        {
            var getLoanTmpForViewDto = await _loanTmpAppService.GetLoanTmpForView(id);

            var model = new LoanTmpViewModel()
            {
                LoanTmp = getLoanTmpForViewDto.LoanTmp
            };

            return PartialView("_ViewLoanTmpModal", model);
        }

    }
}