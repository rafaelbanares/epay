﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.StatutoryTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_StatutoryTypes)]
    public class StatutoryTypesController : ePayControllerBase
    {
        private readonly IStatutoryTypesAppService _statutoryTypesAppService;

        public StatutoryTypesController(IStatutoryTypesAppService statutoryTypesAppService)
        {
            _statutoryTypesAppService = statutoryTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new StatutoryTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_StatutoryTypes_Create, AppPermissions.Pages_StatutoryTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetStatutoryTypesForEditOutput getStatutoryTypesForEditOutput;

            if (id.HasValue)
            {
                getStatutoryTypesForEditOutput = await _statutoryTypesAppService.GetStatutoryTypesForEdit(new EntityDto { Id = id.Value });
            }
            else
            {
                getStatutoryTypesForEditOutput = new GetStatutoryTypesForEditOutput
                {
                    StatutoryTypes = new CreateOrEditStatutoryTypesDto()
                };
            }

            var viewModel = new CreateOrEditStatutoryTypesModalViewModel()
            {
                StatutoryTypes = getStatutoryTypesForEditOutput.StatutoryTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewStatutoryTypesModal(int id)
        {
            var getStatutoryTypesForViewDto = await _statutoryTypesAppService.GetStatutoryTypesForView(id);

            var model = new StatutoryTypesViewModel()
            {
                StatutoryTypes = getStatutoryTypesForViewDto.StatutoryTypes
            };

            return PartialView("_ViewStatutoryTypesModal", model);
        }

    }
}