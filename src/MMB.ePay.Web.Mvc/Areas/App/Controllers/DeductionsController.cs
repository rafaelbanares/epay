﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Deductions;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Deductions)]
    public class DeductionsController : ePayControllerBase
    {
        private readonly IDeductionsAppService _deductionsAppService;
        private readonly IDeductionTypesAppService _deductionTypesAppService;

        public DeductionsController(IDeductionsAppService deductionsAppService, 
            IDeductionTypesAppService deductionTypesAppService)
        {
            _deductionsAppService = deductionsAppService;
            _deductionTypesAppService = deductionTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new DeductionsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Deductions_Create, AppPermissions.Pages_Deductions_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? employeeId)
        {
            GetDeductionsForEditOutput getDeductionsForEditOutput;

            if (id.HasValue)
            {
                getDeductionsForEditOutput = await _deductionsAppService.GetDeductionsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getDeductionsForEditOutput = new GetDeductionsForEditOutput
                {
                    Deductions = new CreateOrEditDeductionsDto
                    {
                        EmployeeId = employeeId.Value
                    }
                };
            }

            var viewModel = new CreateOrEditDeductionsModalViewModel()
            {
                Deductions = getDeductionsForEditOutput.Deductions,
                DeductionTypeList = await _deductionTypesAppService.GetDeductionTypeListForDeductions(id, employeeId)
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewDeductionsModal(int id)
        {
            var getDeductionsForViewDto = await _deductionsAppService.GetDeductionsForView(id);

            var model = new DeductionsViewModel()
            {
                Deductions = getDeductionsForViewDto.Deductions
            };

            return PartialView("_ViewDeductionsModal", model);
        }

    }
}