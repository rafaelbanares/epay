﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.HRSettings;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_HRSettings)]
    public class HRSettingsController : ePayControllerBase
    {
        private readonly IHRSettingsAppService _hrSettingsAppService;

        public HRSettingsController(IHRSettingsAppService hrSettingsAppService)
        {
            _hrSettingsAppService = hrSettingsAppService;
        }

        public ActionResult Index()
        {
            var model = new HRSettingsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_HRSettings_Create, AppPermissions.Pages_HRSettings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetHRSettingsForEditOutput getHRSettingsForEditOutput;

            if (id.HasValue)
            {
                getHRSettingsForEditOutput = await _hrSettingsAppService.GetHRSettingsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getHRSettingsForEditOutput = new GetHRSettingsForEditOutput
                {
                    HRSettings = new CreateOrEditHRSettingsDto()
                };
            }

            var viewModel = new CreateOrEditHRSettingsModalViewModel()
            {
                HRSettings = getHRSettingsForEditOutput.HRSettings,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewHRSettingsModal(int id)
        {
            var getHRSettingsForViewDto = await _hrSettingsAppService.GetHRSettingsForView(id);

            var model = new HRSettingsViewModel()
            {
                HRSettings = getHRSettingsForViewDto.HRSettings
            };

            return PartialView("_ViewHRSettingsModal", model);
        }

    }
}