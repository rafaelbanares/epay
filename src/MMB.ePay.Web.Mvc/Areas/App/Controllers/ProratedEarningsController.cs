﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.ProratedEarnings;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ProratedEarnings)]
    public class ProratedEarningsController : ePayControllerBase
    {
        private readonly IProratedEarningsAppService _proratedEarningsAppService;
        private readonly IEarningTypesAppService _earningTypesAppService;
        private readonly ITimesheetTypesAppService _timesheetTypesAppService;
        private readonly IOvertimeTypesAppService _overtimeTypesAppService;


        public ProratedEarningsController(
            IProratedEarningsAppService proratedEarningsAppService, 
            IEarningTypesAppService earningTypesAppService,
            ITimesheetTypesAppService timesheetTypesAppService,
            IOvertimeTypesAppService overtimeTypesAppService
            )
        {
            _proratedEarningsAppService = proratedEarningsAppService;
            _earningTypesAppService = earningTypesAppService;
            _timesheetTypesAppService = timesheetTypesAppService;
            _overtimeTypesAppService = overtimeTypesAppService;

        }

        public ActionResult Index() => View(new ProratedEarningsViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_ProratedEarnings_Create, AppPermissions.Pages_ProratedEarnings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetProratedEarningsForEditOutput getProratedEarningsForEditOutput;

            if (id.HasValue)
            {
                getProratedEarningsForEditOutput = await _proratedEarningsAppService.GetProratedEarningsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getProratedEarningsForEditOutput = new GetProratedEarningsForEditOutput
                {
                    ProratedEarnings = new CreateOrEditProratedEarningsDto()
                };
            }

            var viewModel = new CreateOrEditProratedEarningsModalViewModel()
            {
                ProratedEarnings = getProratedEarningsForEditOutput.ProratedEarnings,
                EarningTypeList = await _earningTypesAppService.GetEarningTypesList(),
                TimesheetTypeList = await _timesheetTypesAppService.GetTimesheetTypesListFilter(),
                OvertimeTypeList = await _overtimeTypesAppService.GetOvertimeTypesListFilter(),
                OutEarningTypeList = await _earningTypesAppService.GetEarningTypesList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewProratedEarningsModal(int id)
        {
            var getProratedEarningsForViewDto = await _proratedEarningsAppService.GetProratedEarningsForView(id);

            var model = new ProratedEarningsViewModel()
            {
                ProratedEarnings = getProratedEarningsForViewDto.ProratedEarnings
            };

            return PartialView("_ViewProratedEarningsModal", model);
        }

    }
}