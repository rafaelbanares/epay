﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.ProcessHistory;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ProcessHistory)]
    public class ProcessHistoryController : ePayControllerBase
    {
        private readonly IProcessHistoryAppService _processHistoryAppService;

        public ProcessHistoryController(IProcessHistoryAppService processHistoryAppService)
        {
            _processHistoryAppService = processHistoryAppService;
        }

        public ActionResult Index()
        {
            var model = new ProcessHistoryViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ProcessHistory_Create, AppPermissions.Pages_ProcessHistory_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetProcessHistoryForEditOutput getProcessHistoryForEditOutput;

            if (id.HasValue)
            {
                getProcessHistoryForEditOutput = await _processHistoryAppService.GetProcessHistoryForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getProcessHistoryForEditOutput = new GetProcessHistoryForEditOutput
                {
                    ProcessHistory = new CreateOrEditProcessHistoryDto()
                };
            }

            var viewModel = new CreateOrEditProcessHistoryModalViewModel()
            {
                ProcessHistory = getProcessHistoryForEditOutput.ProcessHistory,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewProcessHistoryModal(int id)
        {
            var getProcessHistoryForViewDto = await _processHistoryAppService.GetProcessHistoryForView(id);

            var model = new ProcessHistoryViewModel()
            {
                ProcessHistory = getProcessHistoryForViewDto.ProcessHistory
            };

            return PartialView("_ViewProcessHistoryModal", model);
        }

    }
}