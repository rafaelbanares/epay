﻿using Abp.AspNetCore.Mvc.Authorization;
using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Reports)]
    public class AnnualReportsController : ePayControllerBase
    {
        private readonly TenantManager _tenantManager;
        private readonly IReportsAppService _reportsAppService;

        public AnnualReportsController(
            TenantManager tenantManager,
            IReportsAppService reportsAppService)
        {
            _tenantManager = tenantManager;
            _reportsAppService = reportsAppService;
        }

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        //public async Task<IActionResult> Alphalist(ReportsDto model)
        //{
        //    if (model.Output == "P")
        //    {
        //        model.HeaderName = "NoHeader";
        //        var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

        //        HttpContext.JsReportFeature()
        //            .Recipe(Recipe.ChromePdf)
        //            .Configure((r) => r.Options.Timeout = 120000)
        //            .Configure((r) => r.Template.Chrome = new Chrome
        //            {
        //                HeaderTemplate = "<div></div>",
        //                FooterTemplate = "<div></div>",
        //                DisplayHeaderFooter = true,
        //                MarginTop = "0.5cm",
        //                MarginLeft = "0.5cm",
        //                MarginBottom = "0.5cm",
        //                MarginRight = "0.5cm",
        //                Landscape = model.IsLandscape,
        //                Format = model.Format
        //            });
        //    }
        //    else if (model.Output == "E")
        //    {
        //        HttpContext.JsReportFeature()
        //            .Recipe(Recipe.HtmlToXlsx)
        //            .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
        //    }

        //    var output = await _reportsAppService.GetAnnualAlphalist(model);

        //    return View(output);
        //}

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AlphalistNonMWE1(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "0.5cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "0.5cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualAlphalistNonMWE1(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AlphalistNonMWE2(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "0.5cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "0.5cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualAlphalistNonMWE2(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AlphalistNonMWE3(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "0.5cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "0.5cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualAlphalistNonMWE3(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AlphalistMWE1(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "0.5cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "0.5cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualAlphalistMWE1(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AlphalistMWE2(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "0.5cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "0.5cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualAlphalistMWE2(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AlphalistMWE3(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "0.5cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "0.5cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualAlphalistMWE3(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> BIR2316(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "0.5cm",
                        MarginLeft = "1cm",
                        MarginBottom = "0.5cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetAnnualBIR2316(model);

            return View(output);
        }
    }
}
