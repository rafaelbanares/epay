﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Earnings;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Earnings)]
    public class EarningsController : ePayControllerBase
    {
        private readonly IEarningsAppService _earningsAppService;
        private readonly IEarningTypesAppService _earningTypesAppService;

        public EarningsController(IEarningsAppService earningsAppService, IEarningTypesAppService earningTypesAppService)
        {
            _earningsAppService = earningsAppService;
            _earningTypesAppService = earningTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new EarningsViewModel
            {
                FilterText = ""
            };

            return PartialView("_Index", model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Earnings_Create, AppPermissions.Pages_Earnings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? employeeId)
        {
            GetEarningsForEditOutput getEarningsForEditOutput;

            if (id.HasValue)
            {
                getEarningsForEditOutput = await _earningsAppService.GetEarningsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEarningsForEditOutput = new GetEarningsForEditOutput
                {
                    Earnings = new CreateOrEditEarningsDto
                    {
                        EmployeeId = employeeId.Value
                    }
                };
            }

            var viewModel = new CreateOrEditEarningsModalViewModel()
            {
                Earnings = getEarningsForEditOutput.Earnings,
                EarningTypeList = await _earningTypesAppService.GetEarningTypeListForEarnings(id, employeeId)
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEarningsModal(int id)
        {
            var getEarningsForViewDto = await _earningsAppService.GetEarningsForView(id);

            var model = new EarningsViewModel()
            {
                Earnings = getEarningsForViewDto.Earnings
            };

            return PartialView("_ViewEarningsModal", model);
        }

    }
}