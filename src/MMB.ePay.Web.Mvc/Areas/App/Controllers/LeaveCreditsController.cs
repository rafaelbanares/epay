﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.LeaveCredits;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LeaveCredits)]
    public class LeaveCreditsController : ePayControllerBase
    {
        private readonly ILeaveCreditsAppService _leaveCreditsAppService;

        public LeaveCreditsController(ILeaveCreditsAppService leaveCreditsAppService)
        {
            _leaveCreditsAppService = leaveCreditsAppService;
        }

        public ActionResult Index()
        {
            var model = new LeaveCreditsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveCredits_Create, AppPermissions.Pages_LeaveCredits_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLeaveCreditsForEditOutput getLeaveCreditsForEditOutput;

            if (id.HasValue)
            {
                getLeaveCreditsForEditOutput = await _leaveCreditsAppService.GetLeaveCreditsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLeaveCreditsForEditOutput = new GetLeaveCreditsForEditOutput
                {
                    LeaveCredits = new CreateOrEditLeaveCreditsDto()
                };
            }

            var viewModel = new CreateOrEditLeaveCreditsModalViewModel()
            {
                LeaveCredits = getLeaveCreditsForEditOutput.LeaveCredits,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLeaveCreditsModal(int id)
        {
            var getLeaveCreditsForViewDto = await _leaveCreditsAppService.GetLeaveCreditsForView(id);

            var model = new LeaveCreditsViewModel()
            {
                LeaveCredits = getLeaveCreditsForViewDto.LeaveCredits
            };

            return PartialView("_ViewLeaveCreditsModal", model);
        }

    }
}