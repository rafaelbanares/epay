﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.EmployeePayroll;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EmployeePayroll)]
    public class EmployeePayrollController : ePayControllerBase
    {
        private readonly IEmployeePayrollAppService _employeePayrollAppService;

        public EmployeePayrollController(IEmployeePayrollAppService employeePayrollAppService)
        {
            _employeePayrollAppService = employeePayrollAppService;
        }

        public ActionResult Index()
        {
            var model = new EmployeePayrollViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EmployeePayroll_Create, AppPermissions.Pages_EmployeePayroll_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEmployeePayrollForEditOutput getEmployeePayrollForEditOutput;

            if (id.HasValue)
            {
                getEmployeePayrollForEditOutput = await _employeePayrollAppService.GetEmployeePayrollForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEmployeePayrollForEditOutput = new GetEmployeePayrollForEditOutput
                {
                    EmployeePayroll = new CreateOrEditEmployeePayrollDto()
                };
            }

            var viewModel = new CreateOrEditEmployeePayrollModalViewModel()
            {
                EmployeePayroll = getEmployeePayrollForEditOutput.EmployeePayroll,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployeePayrollModal(int id)
        {
            var getEmployeePayrollForViewDto = await _employeePayrollAppService.GetEmployeePayrollForView(id);

            var model = new EmployeePayrollViewModel()
            {
                EmployeePayroll = getEmployeePayrollForViewDto.EmployeePayroll
            };

            return PartialView("_ViewEmployeePayrollModal", model);
        }

    }
}