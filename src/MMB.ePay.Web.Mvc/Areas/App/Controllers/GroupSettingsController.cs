﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Web.Areas.App.Models.GroupSettings;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_GroupSettings)]
    public class GroupSettingsController : ePayControllerBase
    {
        private readonly IGroupSettingsAppService _groupSettingsAppService;

        public GroupSettingsController(IGroupSettingsAppService groupSettingsAppService)
        {
            _groupSettingsAppService = groupSettingsAppService;
        }

        public ActionResult Index()
        {
            var model = new GroupSettingsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_GroupSettings_Create, AppPermissions.Pages_GroupSettings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetGroupSettingsForEditOutput getGroupSettingsForEditOutput;

            if (id.HasValue)
            {
                getGroupSettingsForEditOutput = await _groupSettingsAppService.GetGroupSettingsForEdit(new EntityDto { Id = id .Value});
            }
            else
            {
                getGroupSettingsForEditOutput = new GetGroupSettingsForEditOutput
                {
                    GroupSettings = new CreateOrEditGroupSettingsDto()
                };
            }

            var viewModel = new CreateOrEditGroupSettingsModalViewModel()
            {
                GroupSettings = getGroupSettingsForEditOutput.GroupSettings,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewGroupSettingsModal(int id)
        {
            var getGroupSettingsForViewDto = await _groupSettingsAppService.GetGroupSettingsForView(id);

            var model = new GroupSettingsViewModel()
            {
                GroupSettings = getGroupSettingsForViewDto.GroupSettings
            };

            return PartialView("_ViewGroupSettingsModal", model);
        }

    }
}