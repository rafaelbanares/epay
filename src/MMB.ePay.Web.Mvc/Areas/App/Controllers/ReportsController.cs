﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Organizations;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Reports;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Reports)]
    public class ReportsController : ePayControllerBase
    {
        private readonly IReportsAppService _reportsAppService;
        private readonly IPayCodesAppService _payCodesAppService;
        private readonly IPayPeriodAppService _payPeriodAppService;
        private readonly IHelperAppService _helperAppService;
        private readonly IOrganizationUnitAppService _organizationUnitAppService;

        public ReportsController(
            IReportsAppService reportsAppService,
            IPayCodesAppService payCodesAppService,
            IPayPeriodAppService payPeriodAppService,
            IHelperAppService helperAppService,
            IOrganizationUnitAppService organizationUnitAppService)
        {
            _reportsAppService = reportsAppService;
            _payCodesAppService = payCodesAppService;
            _payPeriodAppService = payPeriodAppService;
            _helperAppService = helperAppService;
            _organizationUnitAppService = organizationUnitAppService;
        }

        public async Task<IActionResult> Periodic()
        {
            var currentDate = DateTime.Today;

            var model = new PeriodicReportsViewModel
            {
                Reports = new ReportsDto
                {
                    Month = currentDate.Month,
                    Year = currentDate.Year,
                    Grouping = "None",
                    SortOrder = "EmployeeId",
                    Output = "P",
                },
                ReportList = await _reportsAppService.GetReportList("P"),
                PayCodeList = await _payCodesAppService.GetPayCodeList(),
                YearList = await _payPeriodAppService.GetYearList(),
                MonthList = _helperAppService.GetMonthList(),
                DepartmentList = await _organizationUnitAppService.GetDepartmentList()
            };

            model.PayPeriodList = await _payPeriodAppService.GetPayPeriodListByYear(
                model.Reports.Year,
                model.Reports.Month.Value);

            return View(model);
        }

        public async Task<IActionResult> Monthly()
        {
            var currentDate = DateTime.Today;

            var model = new MonthlyReportsViewModel
            {
                Reports = new ReportsDto
                {
                    Month = currentDate.Month,
                    Year = currentDate.Year,
                    Grouping = "None",
                    SortOrder = "EmployeeId",
                    Output = "P",
                },
                ReportList = await _reportsAppService.GetReportList("M"),
                PayCodeList = await _payCodesAppService.GetPayCodeList(),
                YearList = await _payPeriodAppService.GetYearList(),
                MonthList = _helperAppService.GetMonthList(),
                DepartmentList = await _organizationUnitAppService.GetDepartmentList()
            };

            return View(model);
        }

        public async Task<IActionResult> Quarterly()
        {
            var model = new QuarterlyReportsViewModel
            {
                Reports = new ReportsDto
                {
                    Year = DateTime.Today.Year,
                    Grouping = "None",
                    SortOrder = "EmployeeId",
                    Output = "P",
                },
                ReportList = await _reportsAppService.GetReportList("Q"),
                PayCodeList = await _payCodesAppService.GetPayCodeList(),
                YearList = await _payPeriodAppService.GetYearList(),
                QuarterList = _helperAppService.GetQuarterList()
            };

            return View(model);
        }

        public async Task<IActionResult> Annual()
        {
            var model = new AnnualReportsViewModel
            {
                Reports = new ReportsDto
                {
                    Year = DateTime.Today.Year,
                    Grouping = "None",
                    SortOrder = "EmployeeId",
                    Output = "P",
                },
                ReportList = await _reportsAppService.GetReportList("A"),
                PayCodeList = await _payCodesAppService.GetPayCodeList(),
                YearList = await _payPeriodAppService.GetYearList()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Periodic(PeriodicReportsViewModel model)
        {
            var payPeriod = (await _payPeriodAppService.GetPayPeriodForView(model.Reports.PayPeriodId.Value)).PayPeriod;
            var output = await _reportsAppService.GetReportsForView(model.Reports.ReportId);

            output.Reports.Grouping = model.Reports.Grouping;
            output.Reports.Output = model.Reports.Output;
            output.Reports.EmployeeId = model.Reports.EmployeeId;
            output.Reports.PeriodName = string.Format("{0} - {1}",
                payPeriod.PeriodFrom.ToString("MM/dd/yyyy"), payPeriod.PeriodTo.ToString("MM/dd/yyyy"));

            return RedirectToAction(output.Reports.ActionName, "PeriodicReports", output.Reports);
        }

        [HttpPost]
        public async Task<IActionResult> Monthly(MonthlyReportsViewModel model)
        {
            var output = await _reportsAppService.GetReportsForView(model.Reports.ReportId);
            output.Reports.Grouping = model.Reports.Grouping;
            output.Reports.Output = model.Reports.Output;
            output.Reports.EmployeeId = model.Reports.EmployeeId;
            output.Reports.Month = model.Reports.Month;
            output.Reports.Year = model.Reports.Year;

            return RedirectToAction(output.Reports.ActionName, "MonthlyReports", output.Reports);
        }

        [HttpPost]
        public async Task<IActionResult> Quarterly(QuarterlyReportsViewModel model)
        {
            var output = await _reportsAppService.GetReportsForView(model.Reports.ReportId);
            output.Reports.Grouping = model.Reports.Grouping;
            output.Reports.Output = model.Reports.Output;
            output.Reports.EmployeeId = model.Reports.EmployeeId;

            return RedirectToAction(output.Reports.ActionName, "QuarterlyReports", output.Reports);
        }

        [HttpPost]
        public async Task<IActionResult> Annual(AnnualReportsViewModel model)
        {
            var output = await _reportsAppService.GetReportsForView(model.Reports.ReportId);
            output.Reports.Grouping = model.Reports.Grouping;
            output.Reports.Output = model.Reports.Output;
            output.Reports.EmployeeId = model.Reports.EmployeeId;

            return RedirectToAction(output.Reports.ActionName, "AnnualReports", output.Reports);
        }
    }
}
