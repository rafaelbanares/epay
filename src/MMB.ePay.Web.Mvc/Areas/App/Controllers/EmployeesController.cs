﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Payroll;
using MMB.ePay.Web.Areas.App.Models.Employees;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Employees)]
    public class EmployeesController : ePayControllerBase
    {
        private readonly IEmployeesAppService _employeesAppService;
        private readonly IEmploymentTypesAppService _employmentTypesAppService;
        private readonly IPayCodesAppService _payCodesAppService;
        private readonly IBanksAppService _banksAppService;
        private readonly IBankBranchesAppService _bankBranchesAppService;
        private readonly IRanksAppService _ranksAppService;
        private readonly IStatutoryTypesAppService _statutoryTypesAppService;
        private readonly IEmployeeStatutoryInfoAppService _employeeStatutoryInfoAppService;

        private readonly IHelperAppService _helperAppService;
        private readonly IEmployeePayrollAppService _employeePayrollAppService;
        private readonly IEmployersAppService _employersAppService;
        //private readonly IDependentsAppService _dependetsAppService;

        public EmployeesController(IEmployeesAppService employeesAppService,
            IEmploymentTypesAppService employmentTypesAppService,
            IPayCodesAppService payCodesAppService,
            IBanksAppService banksAppService,
            IBankBranchesAppService bankBranchesAppService,
            IRanksAppService ranksAppService,
            IStatutoryTypesAppService statutoryTypesAppService,
            IEmployeeStatutoryInfoAppService employeeStatutoryInfoAppService,
            IHelperAppService helperAppService,
            IEmployeePayrollAppService employeePayrollAppService,
            IEmployersAppService employersAppService
            //IDependentsAppService dependentsAppService
            )
        {
            _employeesAppService = employeesAppService;
            _employmentTypesAppService = employmentTypesAppService;
            _payCodesAppService = payCodesAppService;
            _banksAppService = banksAppService;
            _bankBranchesAppService = bankBranchesAppService;
            _ranksAppService = ranksAppService;
            _statutoryTypesAppService = statutoryTypesAppService;
            _employeeStatutoryInfoAppService = employeeStatutoryInfoAppService;
            _helperAppService = helperAppService;
            _employeePayrollAppService = employeePayrollAppService;
            _employersAppService = employersAppService;
            //_dependetsAppService = dependentsAppService;
        }

        public ActionResult Index()
        {
            var model = new EmployeesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        public async Task<IActionResult> Profile(int id, int? view)
        {
            var dto = new EntityDto { Id = id };

            var getEmployeesForEditOutput = await _employeesAppService.GetEmployeesForEdit(dto);
            var getEmployeePayrollForEditOutput = await _employeePayrollAppService.GetEmployeePayrollForEdit(dto);
            var getEmployeeStatutoryInfoForEditOutput = await _employeeStatutoryInfoAppService.GetEmployeeStatutoryInfoForEdit(dto);

            var getEmployersForEditOutput = await _employersAppService.GetEmployersForEdit(dto);
            //var getDependentsForEditOutput = await _dependentsAppService.GetDependentsForEdit(dto);

            var viewModel = new ProfileEmployeesModalViewModel
            {
                Employees = getEmployeesForEditOutput.Employees,
                EmployeeStatusList = _employeesAppService.GetEmployeeStatusList(),
                EmploymentTypeList = await _employmentTypesAppService.GetEmploymentTypeList(),
                PayCodeList = await _payCodesAppService.GetPayCodeList(),
                BankList = await _banksAppService.GetBankList(),
                PaymentTypeList =  _helperAppService.GetPaymentTypeList(),
                RankList = await _ranksAppService.GetRankList(),
                StatutoryTypeList = await _statutoryTypesAppService.GetStatutoryTypeCodeList()
            };

            viewModel.BankBranchList = await _bankBranchesAppService.GetBankBranchList(viewModel.BankList[0].Value);

            if (getEmployeePayrollForEditOutput.EmployeePayroll != null)
            {
                viewModel.EmployeePayroll = getEmployeePayrollForEditOutput.EmployeePayroll;
            }

            if (getEmployersForEditOutput.Employers != null)
            {
                viewModel.Employers = getEmployersForEditOutput.Employers;
            }

            if (getEmployeeStatutoryInfoForEditOutput.Count > 0)
            {
                viewModel.StatutoryTypes = getEmployeeStatutoryInfoForEditOutput;
            }

            //if (getDependentsForEditOutput != null)
            //{
            //    viewModel.Dependents = getDependentsForEditOutput.Dependets;
            //}

            viewModel.HasView = view == 1;
            return View(viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Employees_Create, AppPermissions.Pages_Employees_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEmployeesForEditOutput getEmployeesForEditOutput;

            if (id.HasValue)
            {
                getEmployeesForEditOutput = await _employeesAppService.GetEmployeesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEmployeesForEditOutput = new GetEmployeesForEditOutput
                {
                    Employees = new CreateOrEditEmployeesDto()
                };
            }

            var viewModel = new CreateOrEditEmployeesModalViewModel()
            {
                Employees = getEmployeesForEditOutput.Employees,
                EmploymentTypeList = await _employmentTypesAppService.GetEmploymentTypeList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployeesModal(int id)
        {
            var getEmployeesForViewDto = await _employeesAppService.GetEmployeesForView(id);

            var model = new EmployeesViewModel()
            {
                Employees = getEmployeesForViewDto.Employees
            };

            return PartialView("_ViewEmployeesModal", model);
        }

    }
}