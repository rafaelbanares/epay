﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Holidays;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Holidays)]
    public class HolidaysController : ePayControllerBase
    {
        private readonly IHolidaysAppService _holidaysAppService;

        public HolidaysController(IHolidaysAppService holidaysAppService)
        {
            _holidaysAppService = holidaysAppService;
        }

        public ActionResult Index()
        {
            var model = new HolidaysViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Holidays_Create, AppPermissions.Pages_Holidays_Edit)]
        public async Task<ActionResult> CreateOrEdit(int? id)
        {
            GetHolidaysForEditOutput getHolidaysForEditOutput;

            if (id.HasValue)
            {
                getHolidaysForEditOutput = await _holidaysAppService.GetHolidaysForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getHolidaysForEditOutput = new GetHolidaysForEditOutput
                {
                    Holidays = new CreateOrEditHolidaysDto()
                };
                getHolidaysForEditOutput.Holidays.Date = DateTime.Now;
            }

            var viewModel = new CreateOrEditHolidaysViewModel()
            {
                Holidays = getHolidaysForEditOutput.Holidays,
            };

            return View(viewModel);
        }

        public async Task<ActionResult> ViewHolidays(int id)
        {
            var getHolidaysForViewDto = await _holidaysAppService.GetHolidaysForView(id);

            var model = new HolidaysViewModel()
            {
                Holidays = getHolidaysForViewDto.Holidays
            };

            return View(model);
        }

    }
}