﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Web.Areas.App.Models.Timesheets;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Timesheets)]
    public class TimesheetsController : ePayControllerBase
    {
        private readonly ITimesheetsAppService _timesheetsAppService;

        public TimesheetsController(ITimesheetsAppService timesheetsAppService)
        {
            _timesheetsAppService = timesheetsAppService;
        }

        public ActionResult Index()
        {
            var model = new TimesheetsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }
    }
}