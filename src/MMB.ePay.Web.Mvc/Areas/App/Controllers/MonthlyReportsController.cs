﻿using Abp.AspNetCore.Mvc.Authorization;
using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Controllers;
using System.Globalization;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Reports)]
    public class MonthlyReportsController : ePayControllerBase
    {
        private readonly TenantManager _tenantManager;
        private readonly IJsReportMVCService _jsReportMVCService;
        private readonly IReportsAppService _reportsAppService;

        public MonthlyReportsController(
            TenantManager tenantManager,
            IJsReportMVCService jsReportMVCService,
            IReportsAppService reportsAppService)
        {
            _tenantManager = tenantManager;
            _jsReportMVCService = jsReportMVCService;
            _reportsAppService = reportsAppService;
        }

        private async Task HttpContextSettings(ReportsDto model)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            //var reportName = Regex.Replace(model.ControllerName, "([A-Z])", " $1").Trim() + " Report";
            var reportName = await _reportsAppService.GetReportName(model.ReportId);

            var headerModel = new MonthlyReportHeaderDto
            {
                TenancyName = tenant.TenancyName,
                ReportName = reportName,
                Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(model.Month.Value),
                Year = model.Year
            };

            var header = await _jsReportMVCService.RenderViewToStringAsync(HttpContext, RouteData, model.HeaderName, headerModel);

            HttpContext.JsReportFeature()
                .Recipe(Recipe.ChromePdf)
                .Configure((r) => r.Options.Timeout = 120000)
                .Configure((r) => r.Template.Chrome = new Chrome
                {
                    HeaderTemplate = header,
                    FooterTemplate = "<div></div>",
                    DisplayHeaderFooter = true,
                    MarginTop = "2cm",
                    MarginLeft = "1cm",
                    MarginBottom = "1cm",
                    MarginRight = "1cm",
                    Landscape = model.IsLandscape,
                    Format = model.Format
                });
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> DeductionRegister(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyDeductionRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> EarningRegister(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyEarningRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> GrossAndTax(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyGrossAndTax(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> LoanDetailRegister(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyLoanDetailRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> OvertimeRegister(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyOvertimeRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PagibigM11(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "1cm",
                        MarginLeft = "1cm",
                        MarginBottom = "1cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyPagibigM11(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PagibigMRCRF(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "1cm",
                        MarginLeft = "1cm",
                        MarginBottom = "1cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyPagibigMRCRF(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PayrollRegister(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyPayrollRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PhilhealthER2(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "1cm",
                        MarginLeft = "1cm",
                        MarginBottom = "1cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyPhilhealthER2(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PhilhealthRF1(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "1cm",
                        MarginLeft = "1cm",
                        MarginBottom = "1cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyPhilhealthRF1(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PhilhealthContribution(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlyPhilhealthContribution(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> SssContribution(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlySssContribution(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> SSSML2(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = "<div></div>",
                        FooterTemplate = "<div></div>",
                        DisplayHeaderFooter = true,
                        MarginTop = "1cm",
                        MarginLeft = "1cm",
                        MarginBottom = "1cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetMonthlySSSML2(model);

            return View(output);
        }
    }
}
