﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.Reviewers;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Reviewers)]
    public class ReviewersController : ePayControllerBase
    {
        private readonly IReviewersAppService _reviewersAppService;
        private readonly IEmployeesAppService _employeesAppService;

        public ReviewersController(IReviewersAppService reviewersAppService, IEmployeesAppService employeesAppService)
        {
            _reviewersAppService = reviewersAppService;
            _employeesAppService = employeesAppService;
        }

        public ActionResult Index()
        {
            var model = new ReviewersViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Reviewers_Create, AppPermissions.Pages_Reviewers_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetReviewersForEditOutput getReviewersForEditOutput;

            if (id.HasValue)
            {
                getReviewersForEditOutput = await _reviewersAppService.GetReviewersForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getReviewersForEditOutput = new GetReviewersForEditOutput
                {
                    Reviewers = new CreateOrEditReviewersDto()
                };
            }

            var viewModel = new CreateOrEditReviewersModalViewModel()
            {
                Reviewers = getReviewersForEditOutput.Reviewers,
                ReviewedByList = await _employeesAppService.GetEmployeeUserList(),
                ReviewerTypeList = await _reviewersAppService.GetReviewerTypeList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewReviewersModal(int id)
        {
            var getReviewersForViewDto = await _reviewersAppService.GetReviewersForView(id);

            var model = new ReviewersViewModel()
            {
                Reviewers = getReviewersForViewDto.Reviewers
            };

            return PartialView("_ViewReviewersModal", model);
        }

    }
}