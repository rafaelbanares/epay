﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.StatutoryPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_StatutoryPosted)]
    public class StatutoryPostedController : ePayControllerBase
    {
        private readonly IStatutoryPostedAppService _statutoryPostedAppService;

        public StatutoryPostedController(IStatutoryPostedAppService statutoryPostedAppService)
        {
            _statutoryPostedAppService = statutoryPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new StatutoryPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_StatutoryPosted_Create, AppPermissions.Pages_StatutoryPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetStatutoryPostedForEditOutput getStatutoryPostedForEditOutput;

            if (id.HasValue)
            {
                getStatutoryPostedForEditOutput = await _statutoryPostedAppService.GetStatutoryPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getStatutoryPostedForEditOutput = new GetStatutoryPostedForEditOutput
                {
                    StatutoryPosted = new CreateOrEditStatutoryPostedDto()
                };
            }

            var viewModel = new CreateOrEditStatutoryPostedModalViewModel()
            {
                StatutoryPosted = getStatutoryPostedForEditOutput.StatutoryPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewStatutoryPostedModal(int id)
        {
            var getStatutoryPostedForViewDto = await _statutoryPostedAppService.GetStatutoryPostedForView(id);

            var model = new StatutoryPostedViewModel()
            {
                StatutoryPosted = getStatutoryPostedForViewDto.StatutoryPosted
            };

            return PartialView("_ViewStatutoryPostedModal", model);
        }

    }
}