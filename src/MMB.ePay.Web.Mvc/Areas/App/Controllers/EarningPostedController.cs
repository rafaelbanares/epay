﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.EarningPosted;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EarningPosted)]
    public class EarningPostedController : ePayControllerBase
    {
        private readonly IEarningPostedAppService _earningPostedAppService;

        public EarningPostedController(IEarningPostedAppService earningPostedAppService)
        {
            _earningPostedAppService = earningPostedAppService;
        }

        public ActionResult Index()
        {
            var model = new EarningPostedViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EarningPosted_Create, AppPermissions.Pages_EarningPosted_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEarningPostedForEditOutput getEarningPostedForEditOutput;

            if (id.HasValue)
            {
                getEarningPostedForEditOutput = await _earningPostedAppService.GetEarningPostedForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEarningPostedForEditOutput = new GetEarningPostedForEditOutput
                {
                    EarningPosted = new CreateOrEditEarningPostedDto()
                };
            }

            var viewModel = new CreateOrEditEarningPostedModalViewModel()
            {
                EarningPosted = getEarningPostedForEditOutput.EarningPosted,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEarningPostedModal(int id)
        {
            var getEarningPostedForViewDto = await _earningPostedAppService.GetEarningPostedForView(id);

            var model = new EarningPostedViewModel()
            {
                EarningPosted = getEarningPostedForViewDto.EarningPosted
            };

            return PartialView("_ViewEarningPostedModal", model);
        }

    }
}