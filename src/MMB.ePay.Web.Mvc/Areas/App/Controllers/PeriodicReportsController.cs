﻿using Abp.AspNetCore.Mvc.Authorization;
using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Reports)]
    public class PeriodicReportsController : ePayControllerBase
    {
        private readonly TenantManager _tenantManager;
        private readonly IJsReportMVCService _jsReportMVCService;
        private readonly IReportsAppService _reportsAppService;

        public PeriodicReportsController(
            TenantManager tenantManager,
            IJsReportMVCService jsReportMVCService,
            IReportsAppService reportsAppService)
        {
            _tenantManager = tenantManager;
            _jsReportMVCService = jsReportMVCService;
            _reportsAppService = reportsAppService;
        }

        private async Task HttpContextSettings(ReportsDto model)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            //var reportName = Regex.Replace(model.ControllerName, "([A-Z])", " $1").Trim() + " Report";
            var reportName = await _reportsAppService.GetReportName(model.ReportId);

            var headerModel = new PeriodicReportHeaderDto
            {
                TenancyName = tenant.TenancyName,
                ReportName = reportName,
                Period = model.PeriodName
            };

            var header = await _jsReportMVCService.RenderViewToStringAsync(HttpContext, RouteData, model.HeaderName, headerModel);

            HttpContext.JsReportFeature()
                .Recipe(Recipe.ChromePdf)
                .Configure((r) => r.Options.Timeout = 120000)
                .Configure((r) => r.Template.Chrome = new Chrome
                {
                    HeaderTemplate = header,
                    FooterTemplate = "<div></div>",
                    DisplayHeaderFooter = true,
                    MarginTop = "2cm",
                    MarginLeft = "1cm",
                    MarginBottom = "1cm",
                    MarginRight = "1cm",
                    Landscape = model.IsLandscape,
                    Format = model.Format
                });
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> BankAdvise(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "AdviseHeader";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicBankAdvise(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> CashAdvise(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "AdviseHeader";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicCashAdvise(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> ChequeAdvise(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "AdviseHeader";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicChequeAdvise(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> DeductionRegister(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicDeductionRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> EarningRegister(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicEarningRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> EmployeePayslip(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "NoHeader";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicEmployeePayslip(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> GrossAndTax(ReportsDto model)
        {
            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicGrossAndTax(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PagibigContribution(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicPagibigContribution(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PayrollRegister(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicPayrollRegister(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> PhilhealthContribution(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicPhilhealthContribution(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> SssContribution(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicSssContribution(model);

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> TimesheetRegister(ReportsDto model)
        {

            if (model.Output == "P")
            {
                model.HeaderName = "Header";
                await HttpContextSettings(model);
            }
            else if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }

            var output = await _reportsAppService.GetPeriodicTimesheetRegister(model);

            return View(output);
        }

        //private async Task PayslipHttpContextSettings()
        //{
        //    var header = await _jsReportMVCService.RenderViewToStringAsync(HttpContext, RouteData, "PayslipHeader", new { });
        //    var footer = await _jsReportMVCService.RenderViewToStringAsync(HttpContext, RouteData, "Footer", new { });

        //    HttpContext.JsReportFeature()
        //        .Recipe(Recipe.ChromePdf)
        //        .Configure((r) => r.Template.Chrome = new Chrome
        //        {
        //            HeaderTemplate = header,
        //            FooterTemplate = footer,
        //            DisplayHeaderFooter = true,
        //            MarginTop = "4cm",
        //            MarginLeft = "1cm",
        //            MarginBottom = "0.5cm",
        //            MarginRight = "1cm",
        //            Landscape = false,
        //            Format = "Letter"
        //        });
        //}

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        //public async Task<IActionResult> Payslip(int employeeId, int payrollNo)
        //{
        //    var model = await _reportsAppService.GetPeriodicPayslip(employeeId, payrollNo);

        //    await PayslipHttpContextSettings();

        //    return View(model);
        //}
    }
}
