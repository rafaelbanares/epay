﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.TimesheetProcess;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TimesheetProcess)]
    public class TimesheetProcessController : ePayControllerBase
    {
        private readonly ITimesheetProcessAppService _timesheetProcessAppService;

        public TimesheetProcessController(ITimesheetProcessAppService timesheetProcessAppService)
        {
            _timesheetProcessAppService = timesheetProcessAppService;
        }

        public ActionResult Index()
        {
            var model = new TimesheetProcessViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_TimesheetProcess_Create, AppPermissions.Pages_TimesheetProcess_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetTimesheetProcessForEditOutput getTimesheetProcessForEditOutput;

            if (id.HasValue)
            {
                getTimesheetProcessForEditOutput = await _timesheetProcessAppService.GetTimesheetProcessForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getTimesheetProcessForEditOutput = new GetTimesheetProcessForEditOutput
                {
                    TimesheetProcess = new CreateOrEditTimesheetProcessDto()
                };
            }

            var viewModel = new CreateOrEditTimesheetProcessModalViewModel()
            {
                TimesheetProcess = getTimesheetProcessForEditOutput.TimesheetProcess,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewTimesheetProcessModal(int id)
        {
            var getTimesheetProcessForViewDto = await _timesheetProcessAppService.GetTimesheetProcessForView(id);

            var model = new TimesheetProcessViewModel()
            {
                TimesheetProcess = getTimesheetProcessForViewDto.TimesheetProcess
            };

            return PartialView("_ViewTimesheetProcessModal", model);
        }

    }
}