﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.EarningTypeSettings;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EarningTypeSettings)]
    public class EarningTypeSettingsController : ePayControllerBase
    {
        private readonly IEarningTypeSettingsAppService _earningTypeSettingsAppService;

        public EarningTypeSettingsController(IEarningTypeSettingsAppService earningTypeSettingsAppService)
        {
            _earningTypeSettingsAppService = earningTypeSettingsAppService;
        }

        public ActionResult Index()
        {
            var model = new EarningTypeSettingsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_EarningTypeSettings_Create, AppPermissions.Pages_EarningTypeSettings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetEarningTypeSettingsForEditOutput getEarningTypeSettingsForEditOutput;

            if (id.HasValue)
            {
                getEarningTypeSettingsForEditOutput = await _earningTypeSettingsAppService.GetEarningTypeSettingsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getEarningTypeSettingsForEditOutput = new GetEarningTypeSettingsForEditOutput
                {
                    EarningTypeSettings = new CreateOrEditEarningTypeSettingsDto()
                };
            }

            var viewModel = new CreateOrEditEarningTypeSettingsModalViewModel()
            {
                EarningTypeSettings = getEarningTypeSettingsForEditOutput.EarningTypeSettings,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEarningTypeSettingsModal(int id)
        {
            var getEarningTypeSettingsForViewDto = await _earningTypeSettingsAppService.GetEarningTypeSettingsForView(id);

            var model = new EarningTypeSettingsViewModel()
            {
                EarningTypeSettings = getEarningTypeSettingsForViewDto.EarningTypeSettings
            };

            return PartialView("_ViewEarningTypeSettingsModal", model);
        }

    }
}