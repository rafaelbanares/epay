﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.DeductionTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_DeductionTypes)]
    public class DeductionTypesController : ePayControllerBase
    {
        private readonly IDeductionTypesAppService _deductionTypesAppService;
        private readonly IEmployeesAppService _employeesAppService;

        public DeductionTypesController(IDeductionTypesAppService deductionTypesAppService, IEmployeesAppService employeesAppService)
        {
            _deductionTypesAppService = deductionTypesAppService;
            _employeesAppService = employeesAppService;
        }

        public ActionResult Index()
        {
            var model = new DeductionTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_DeductionTypes_Create, AppPermissions.Pages_DeductionTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetDeductionTypesForEditOutput getDeductionTypesForEditOutput;

            if (id.HasValue)
            {
                getDeductionTypesForEditOutput = await _deductionTypesAppService.GetDeductionTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getDeductionTypesForEditOutput = new GetDeductionTypesForEditOutput
                {
                    DeductionTypes = new CreateOrEditDeductionTypesDto()
                };
            }

            var viewModel = new CreateOrEditDeductionTypesModalViewModel()
            {
                DeductionTypes = getDeductionTypesForEditOutput.DeductionTypes,
                CurrencyList = _employeesAppService.GetCurrencyList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewDeductionTypesModal(int id)
        {
            var getDeductionTypesForViewDto = await _deductionTypesAppService.GetDeductionTypesForView(id);

            var model = new DeductionTypesViewModel()
            {
                DeductionTypes = getDeductionTypesForViewDto.DeductionTypes
            };

            return PartialView("_ViewDeductionTypesModal", model);
        }

    }
}