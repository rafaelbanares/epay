﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.PayrollTmp;
using MMB.ePay.Web.Controllers;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using Abp.Application.Services.Dto;
using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_PayrollTmp)]
    public class PayrollTmpController : ePayControllerBase
    {
        private readonly IPayrollTmpAppService _payrollTmpAppService;

        public PayrollTmpController(IPayrollTmpAppService payrollTmpAppService)
        {
            _payrollTmpAppService = payrollTmpAppService;
        }

        public ActionResult Index()
        {
            var model = new PayrollTmpViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_PayrollTmp_Create, AppPermissions.Pages_PayrollTmp_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetPayrollTmpForEditOutput getPayrollTmpForEditOutput;

            if (id.HasValue)
            {
                getPayrollTmpForEditOutput = await _payrollTmpAppService.GetPayrollTmpForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getPayrollTmpForEditOutput = new GetPayrollTmpForEditOutput
                {
                    PayrollTmp = new CreateOrEditPayrollTmpDto()
                };
            }

            var viewModel = new CreateOrEditPayrollTmpModalViewModel()
            {
                PayrollTmp = getPayrollTmpForEditOutput.PayrollTmp,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewPayrollTmpModal(int id)
        {
            var getPayrollTmpForViewDto = await _payrollTmpAppService.GetPayrollTmpForView(id);

            var model = new PayrollTmpViewModel()
            {
                PayrollTmp = getPayrollTmpForViewDto.PayrollTmp
            };

            return PartialView("_ViewPayrollTmpModal", model);
        }

    }
}