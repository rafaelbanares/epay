﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.LoanTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LoanTypes)]
    public class LoanTypesController : ePayControllerBase
    {
        private readonly ILoanTypesAppService _loanTypesAppService;

        public LoanTypesController(ILoanTypesAppService loanTypesAppService)
        {
            _loanTypesAppService = loanTypesAppService;
        }

        public ActionResult Index()
        {
            var model = new LoanTypesViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LoanTypes_Create, AppPermissions.Pages_LoanTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetLoanTypesForEditOutput getLoanTypesForEditOutput;

            if (id.HasValue)
            {
                getLoanTypesForEditOutput = await _loanTypesAppService.GetLoanTypesForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getLoanTypesForEditOutput = new GetLoanTypesForEditOutput
                {
                    LoanTypes = new CreateOrEditLoanTypesDto()
                };
            }

            var viewModel = new CreateOrEditLoanTypesModalViewModel()
            {
                LoanTypes = getLoanTypesForEditOutput.LoanTypes,
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLoanTypesModal(int id)
        {
            var getLoanTypesForViewDto = await _loanTypesAppService.GetLoanTypesForView(id);

            var model = new LoanTypesViewModel()
            {
                LoanTypes = getLoanTypesForViewDto.LoanTypes
            };

            return PartialView("_ViewLoanTypesModal", model);
        }

    }
}