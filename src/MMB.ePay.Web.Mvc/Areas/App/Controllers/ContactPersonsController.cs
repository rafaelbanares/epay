﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Web.Areas.App.Models.ContactPersons;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ContactPersons)]
    public class ContactPersonsController : ePayControllerBase
    {
        private readonly IContactPersonsAppService _contactPersonsAppService;

        public ContactPersonsController(IContactPersonsAppService contactPersonsAppService)
        {
            _contactPersonsAppService = contactPersonsAppService;
        }

        public ActionResult Index()
        {
            var model = new ContactPersonsViewModel
            {
                FilterText = ""
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ContactPersons_Create, AppPermissions.Pages_ContactPersons_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            GetContactPersonsForEditOutput getContactPersonsForEditOutput;

            var viewModel = new CreateOrEditContactPersonsModalViewModel();

            if (id.HasValue)
            {
                getContactPersonsForEditOutput = await _contactPersonsAppService.GetContactPersonsForEdit(new EntityDto { Id = (int)id });
            }
            else
            {
                getContactPersonsForEditOutput = new GetContactPersonsForEditOutput
                {
                    ContactPersons = new CreateOrEditContactPersonsDto()
                };
            }

            viewModel.ContactPersons = getContactPersonsForEditOutput.ContactPersons;

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewContactPersonsModal(int id)
        {
            var getContactPersonsForViewDto = await _contactPersonsAppService.GetContactPersonsForView(id);

            var model = new ContactPersonsViewModel()
            {
                ContactPersons = getContactPersonsForViewDto.ContactPersons
            };

            return PartialView("_ViewContactPersonsModal", model);
        }

    }
}