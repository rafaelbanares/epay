﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Ranks
{
    public class RanksViewModel : GetRanksForViewDto
    {
        public string FilterText { get; set; }
    }
}