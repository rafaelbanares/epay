﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Ranks
{
    public class CreateOrEditRanksModalViewModel
    {
        public CreateOrEditRanksDto Ranks { get; set; }

        public bool IsEditMode => Ranks.Id.HasValue;
    }
}