﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeTypes
{
    public class CreateOrEditOvertimeTypesModalViewModel
    {
        public CreateOrEditOvertimeTypesDto OvertimeTypes { get; set; }

        public bool IsEditMode => !OvertimeTypes.Id.IsNullOrWhiteSpace();
    }
}