﻿using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeTypes
{
    public class OvertimeTypesViewModel : GetOvertimeTypesForViewDto
    {
        public string FilterText { get; set; }

        public bool IsEditMode { get; set; }
    }
}