﻿using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Employees
{
    public class EmployeesViewModel : GetEmployeesForViewDto
    {
        public string FilterText { get; set; }
    }
}