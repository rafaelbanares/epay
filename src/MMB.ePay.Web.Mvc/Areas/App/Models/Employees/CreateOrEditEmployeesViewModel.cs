﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Employees
{
    public class CreateOrEditEmployeesModalViewModel
    {
        public CreateOrEditEmployeesDto Employees { get; set; }

        public IList<SelectListItem> EmploymentTypeList { get; set; }

        public bool IsEditMode => Employees.Id.HasValue;
    }

    public class ProfileEmployeesModalViewModel
    {
        public ProfileEmployeesModalViewModel()
        {
            Employees = new CreateOrEditEmployeesDto();
            EmployeePayroll = new CreateOrEditEmployeePayrollDto();
            Employers = new CreateOrEditEmployersDto();
            //Dependents = new CreateOrEditDependentsDto();
            EmploymentTypeList = new List<SelectListItem>();
            PayCodeList = new List<SelectListItem>();
            BankList = new List<SelectListItem>();
            PaymentTypeList = new List<SelectListItem>();
            RankList = new List<SelectListItem>();
            BankBranchList = new List<SelectListItem>();
            FrequencyList = new List<SelectListItem>();
            ShiftTypeList = new List<SelectListItem>();
            StatutoryTypeList = new List<string>();
            StatutoryTypes = new List<string>();
        }

        public bool HasView { get; set; }

        public CreateOrEditEmployeesDto Employees { get; set; }
        public CreateOrEditEmployeePayrollDto EmployeePayroll { get; set; }
        public CreateOrEditEmployersDto Employers { get; set; }
        //public CreateOrEditDependentsDto Dependents { get; set; }

        public bool HasPayroll => EmployeePayroll.Id.HasValue;
        public bool HasEmployers => Employers.Id.HasValue;
        //public bool HasDependents => Dependents.Id.HasValue;

        public IList<SelectListItem> EmployeeStatusList { get; set; }
        public IList<SelectListItem> EmploymentTypeList { get; set; }
        public IList<SelectListItem> PayCodeList { get; set; }
        public IList<SelectListItem> BankList { get; set; }
        public IList<SelectListItem> PaymentTypeList { get; set; }
        public IList<SelectListItem> RankList { get; set; }
        public IList<SelectListItem> BankBranchList { get; set; }
        public IList<SelectListItem> FrequencyList { get; set; }
        public IList<SelectListItem> ShiftTypeList { get; set; }
        public IList<string> StatutoryTypeList { get; set; }
        public IList<string> StatutoryTypes { get; set; }
    }
}