﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LoanTypes
{
    public class LoanTypesViewModel : GetLoanTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}