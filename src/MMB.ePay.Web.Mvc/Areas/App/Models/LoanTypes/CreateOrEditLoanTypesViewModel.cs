﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LoanTypes
{
    public class CreateOrEditLoanTypesModalViewModel
    {
        public CreateOrEditLoanTypesDto LoanTypes { get; set; }

        public bool IsEditMode => LoanTypes.Id.HasValue;
    }
}