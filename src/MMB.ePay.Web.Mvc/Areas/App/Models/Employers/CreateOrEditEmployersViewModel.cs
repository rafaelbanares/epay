﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.Employers
{
    public class CreateOrEditEmployersModalViewModel
    {
        public CreateOrEditEmployersDto Employers { get; set; }

        public bool IsEditMode => Employers.Id.HasValue;
    }
}