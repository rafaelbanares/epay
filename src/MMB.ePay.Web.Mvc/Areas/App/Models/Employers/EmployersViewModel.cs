﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Employers
{
    public class EmployersViewModel : GetEmployersForViewDto
    {
        public string FilterText { get; set; }
    }
}