﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.PayCodes
{
    public class CreateOrEditPayCodesModalViewModel
    {
        public CreateOrEditPayCodesModalViewModel()
        {
            PayFrequencyList = new List<SelectListItem>();
            TaxFrequencyList = new List<SelectListItem>();
            TaxMethodList = new List<SelectListItem>();
        }

        public IList<SelectListItem> PayFrequencyList { get; set; }

        public IList<SelectListItem> TaxFrequencyList { get; set; }

        public IList<SelectListItem> TaxMethodList { get; set; }

        public CreateOrEditPayCodesDto PayCodes { get; set; }

        public bool IsEditMode => PayCodes.Id.HasValue;
    }
}