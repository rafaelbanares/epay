﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.PayCodes
{
    public class PayCodesViewModel : GetPayCodesForViewDto
    {
        public string FilterText { get; set; }
    }
}