﻿using Abp.Application.Services.Dto;
using Abp.Webhooks;
using MMB.ePay.WebHooks.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Webhooks
{
    public class CreateOrEditWebhookSubscriptionViewModel
    {
        public WebhookSubscription WebhookSubscription { get; set; }

        public ListResultDto<GetAllAvailableWebhooksOutput> AvailableWebhookEvents { get; set; }
    }
}
