﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.RecurEarnings
{
    public class RecurEarningsViewModel : GetRecurEarningsForViewDto
    {
        public string FilterText { get; set; }
    }
}