﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.RecurEarnings
{
    public class CreateOrEditRecurEarningsModalViewModel
    {
        public CreateOrEditRecurEarningsModalViewModel()
        {
            EarningTypeList = new List<SelectListItem>();
        }

        public CreateOrEditRecurEarningsDto RecurEarnings { get; set; }

        public IList<SelectListItem> EarningTypeList { get; set; }

        public bool IsEditMode => RecurEarnings.Id.HasValue;
    }
}