﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OTSheetProcess
{
    public class OTSheetProcessViewModel : GetOTSheetProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}