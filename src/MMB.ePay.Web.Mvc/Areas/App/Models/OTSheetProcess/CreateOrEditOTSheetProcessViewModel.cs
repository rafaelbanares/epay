﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OTSheetProcess
{
    public class CreateOrEditOTSheetProcessModalViewModel
    {
        public CreateOrEditOTSheetProcessModalViewModel()
        {
            OvertimeTypeList = new List<SelectListItem>();
        }

        public CreateOrEditOTSheetProcessDto OTSheetProcess { get; set; }

        public IList<SelectListItem> OvertimeTypeList { get; set; }

        public bool IsEditMode => OTSheetProcess.Id.HasValue;
    }
}