﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetTmp
{
    public class TimesheetTmpViewModel : GetTimesheetTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}