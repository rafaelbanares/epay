﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetTmp
{
    public class CreateOrEditTimesheetTmpModalViewModel
    {
        public CreateOrEditTimesheetTmpDto TimesheetTmp { get; set; }

        public bool IsEditMode => TimesheetTmp.Id.HasValue;
    }
}