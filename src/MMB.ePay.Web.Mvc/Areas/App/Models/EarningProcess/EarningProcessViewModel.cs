﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EarningProcess
{
    public class EarningProcessViewModel : GetEarningProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}