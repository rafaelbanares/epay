﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.EarningProcess
{
    public class CreateOrEditEarningProcessModalViewModel
    {
        public CreateOrEditEarningProcessDto EarningProcess { get; set; }

        public bool IsEditMode => EarningProcess.Id.HasValue;
    }
}