﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MMB.ePay.Persons.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Persons
{
    [AutoMapFrom(typeof(ListResultDto<PersonListDto>))]
    public class PersonsIndexViewModel : ListResultDto<PersonListDto>
    {

    }
}
