﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ContactPersons
{
    public class ContactPersonsViewModel : GetContactPersonsForViewDto
    {
        public string FilterText { get; set; }
    }
}
