﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.ContactPersons
{
    public class CreateOrEditContactPersonsModalViewModel
    {
        //public CreateOrEditContactPersonsModalViewModel()
        //{
        //    BankList = new List<SelectListItem>();
        //    BankBranchList = new List<SelectListItem>();
        //    BankAccountTypeList = new List<SelectListItem>();
        //}

        public CreateOrEditContactPersonsDto ContactPersons { get; set; }

        //public IList<SelectListItem> BankList { get; set; }

        //public IList<SelectListItem> BankBranchList { get; set; }

        //public IList<SelectListItem> BankAccountTypeList { get; set; }

        public bool IsEditMode => ContactPersons.Id.HasValue;
    }
}
