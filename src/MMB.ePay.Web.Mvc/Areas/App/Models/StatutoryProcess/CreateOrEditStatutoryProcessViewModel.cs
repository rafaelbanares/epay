﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryProcess
{
    public class CreateOrEditStatutoryProcessModalViewModel
    {
        public CreateOrEditStatutoryProcessDto StatutoryProcess { get; set; }

        public IList<SelectListItem> StatutoryTypeList { get; set; }

        public bool IsEditMode => StatutoryProcess.Id.HasValue;
    }
}