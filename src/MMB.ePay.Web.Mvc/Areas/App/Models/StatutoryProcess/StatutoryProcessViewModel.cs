﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryProcess
{
    public class StatutoryProcessViewModel : GetStatutoryProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}