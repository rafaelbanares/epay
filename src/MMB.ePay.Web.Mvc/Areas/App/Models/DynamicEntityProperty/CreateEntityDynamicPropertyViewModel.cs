﻿using System.Collections.Generic;
using MMB.ePay.DynamicEntityProperties.Dto;

namespace MMB.ePay.Web.Areas.App.Models.DynamicEntityProperty
{
    public class CreateEntityDynamicPropertyViewModel
    {
        public string EntityFullName { get; set; }

        public List<string> AllEntities { get; set; }

        public List<DynamicPropertyDto> DynamicProperties { get; set; }
    }
}
