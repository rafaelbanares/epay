﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LoanProcess
{
    public class LoanProcessViewModel : GetLoanProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}