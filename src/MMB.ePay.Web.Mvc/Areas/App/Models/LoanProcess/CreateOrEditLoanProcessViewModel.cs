﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LoanProcess
{
    public class CreateOrEditLoanProcessModalViewModel
    {
        public CreateOrEditLoanProcessDto LoanProcess { get; set; }

        public bool IsEditMode => LoanProcess.Id.HasValue;
    }
}