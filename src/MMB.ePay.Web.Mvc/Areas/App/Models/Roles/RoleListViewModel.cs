﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using MMB.ePay.Authorization.Permissions.Dto;
using MMB.ePay.Web.Areas.App.Models.Common;

namespace MMB.ePay.Web.Areas.App.Models.Roles
{
    public class RoleListViewModel : IPermissionsEditViewModel
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}