﻿using Abp.AutoMapper;
using MMB.ePay.Authorization.Roles.Dto;
using MMB.ePay.Web.Areas.App.Models.Common;

namespace MMB.ePay.Web.Areas.App.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class CreateOrEditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool IsEditMode => Role.Id.HasValue;
    }
}