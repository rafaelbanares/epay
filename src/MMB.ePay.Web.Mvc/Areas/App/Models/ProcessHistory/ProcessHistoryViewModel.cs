﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ProcessHistory
{
    public class ProcessHistoryViewModel : GetProcessHistoryForViewDto
    {
        public string FilterText { get; set; }
    }
}