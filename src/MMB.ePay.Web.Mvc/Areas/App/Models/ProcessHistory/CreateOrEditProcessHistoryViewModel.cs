﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.ProcessHistory
{
    public class CreateOrEditProcessHistoryModalViewModel
    {
        public CreateOrEditProcessHistoryDto ProcessHistory { get; set; }

        public bool IsEditMode => ProcessHistory.Id.HasValue;

        public IList<SelectListItem> ProcessStatusList { get; set; }
    }
}