using Abp.AutoMapper;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Authorization.Users.Dto;
using MMB.ePay.Web.Areas.App.Models.Common;

namespace MMB.ePay.Web.Areas.App.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; set; }
    }
}