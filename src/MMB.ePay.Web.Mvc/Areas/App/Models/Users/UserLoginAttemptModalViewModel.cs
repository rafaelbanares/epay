using System.Collections.Generic;
using MMB.ePay.Authorization.Users.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Users
{
    public class UserLoginAttemptModalViewModel
    {
        public List<UserLoginAttemptDto> LoginAttempts { get; set; }
    }
}