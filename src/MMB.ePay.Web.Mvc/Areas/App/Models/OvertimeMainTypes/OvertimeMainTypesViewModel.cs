﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeMainTypes
{
    public class OvertimeMainTypesViewModel : GetOvertimeMainTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}