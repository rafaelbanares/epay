﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeMainTypes
{
    public class CreateOrEditOvertimeMainTypesModalViewModel
    {
        public CreateOrEditOvertimeMainTypesDto OvertimeMainTypes { get; set; }

        public bool IsEditMode => OvertimeMainTypes.Id.HasValue;
    }
}