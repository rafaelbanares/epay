﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetProcess
{
    public class TimesheetProcessViewModel : GetTimesheetProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}