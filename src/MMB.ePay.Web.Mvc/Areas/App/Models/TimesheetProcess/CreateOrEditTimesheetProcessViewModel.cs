﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetProcess
{
    public class CreateOrEditTimesheetProcessModalViewModel
    {
        public CreateOrEditTimesheetProcessDto TimesheetProcess { get; set; }

        public bool IsEditMode => TimesheetProcess.Id.HasValue;
    }
}