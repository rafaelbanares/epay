﻿using System.Collections.Generic;
using MMB.ePay.DynamicEntityProperties.Dto;

namespace MMB.ePay.Web.Areas.App.Models.DynamicProperty
{
    public class CreateOrEditDynamicPropertyViewModel
    {
        public DynamicPropertyDto DynamicPropertyDto { get; set; }

        public List<string> AllowedInputTypes { get; set; }
    }
}
