﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.DeductionTmp
{
    public class CreateOrEditDeductionTmpModalViewModel
    {
        public CreateOrEditDeductionTmpDto DeductionTmp { get; set; }

        public bool IsEditMode => DeductionTmp.Id.HasValue;
    }
}