﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.DeductionTmp
{
    public class DeductionTmpViewModel : GetDeductionTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}