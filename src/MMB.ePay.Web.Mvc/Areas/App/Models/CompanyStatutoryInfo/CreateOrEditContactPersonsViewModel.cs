﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.CompanyStatutoryInfo
{
    public class CreateOrEditCompanyStatutoryInfoModalViewModel
    {
        public CreateOrEditCompanyStatutoryInfoModalViewModel()
        {
            SignatoryList = new List<SelectListItem>();
            StatutoryTypeList = new List<SelectListItem>();
        }

        public CreateOrEditCompanyStatutoryInfoDto CompanyStatutoryInfo { get; set; }

        public IList<SelectListItem> SignatoryList { get; set; }

        public IList<SelectListItem> StatutoryTypeList { get; set; }

        public bool IsEditMode => CompanyStatutoryInfo.Id != null;
    }
}
