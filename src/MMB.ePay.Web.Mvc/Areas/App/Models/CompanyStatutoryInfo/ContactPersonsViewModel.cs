﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.CompanyStatutoryInfo
{
    public class CompanyStatutoryInfoViewModel : GetCompanyStatutoryInfoForViewDto
    {
        public string FilterText { get; set; }
    }
}
