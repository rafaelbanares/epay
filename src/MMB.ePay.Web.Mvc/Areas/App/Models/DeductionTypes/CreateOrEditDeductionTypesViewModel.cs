﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.DeductionTypes
{
    public class CreateOrEditDeductionTypesModalViewModel
    {
        public CreateOrEditDeductionTypesModalViewModel()
        {
            CurrencyList = new List<SelectListItem>();
        }

        public CreateOrEditDeductionTypesDto DeductionTypes { get; set; }

        public IList<SelectListItem> CurrencyList { get; set; }

        public bool IsEditMode => DeductionTypes.Id.HasValue;
    }
}