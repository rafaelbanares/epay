﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.DeductionTypes
{
    public class DeductionTypesViewModel : GetDeductionTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}