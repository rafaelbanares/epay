﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OTSheet
{
    public class OTSheetViewModel : GetOTSheetForViewDto
    {
        public string FilterText { get; set; }
    }
}