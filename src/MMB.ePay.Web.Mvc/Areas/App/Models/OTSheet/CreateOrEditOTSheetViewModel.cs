﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OTSheet
{
    public class CreateOrEditOTSheetModalViewModel
    {
        public CreateOrEditOTSheetDto OTSheet { get; set; }

        public bool IsEditMode => OTSheet.Id.HasValue;
    }
}