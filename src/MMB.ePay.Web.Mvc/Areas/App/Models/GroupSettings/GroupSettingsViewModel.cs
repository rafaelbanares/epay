﻿using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.GroupSettings
{
    public class GroupSettingsViewModel : GetGroupSettingsForViewDto
    {
        public string FilterText { get; set; }
    }
}