﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LeaveCredits
{
    public class LeaveCreditsViewModel : GetLeaveCreditsForViewDto
    {
        public string FilterText { get; set; }
    }
}