﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LeaveCredits
{
    public class CreateOrEditLeaveCreditsModalViewModel
    {
        public CreateOrEditLeaveCreditsDto LeaveCredits { get; set; }

        public bool IsEditMode => LeaveCredits.Id.HasValue;
    }
}