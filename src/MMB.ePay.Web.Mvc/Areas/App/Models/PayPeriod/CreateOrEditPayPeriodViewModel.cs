﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.PayPeriod
{
    public class CreateOrEditPayPeriodModalViewModel
    {
        public CreateOrEditPayPeriodModalViewModel()
        {
            PayrollFreqList = new List<SelectListItem>();
            AppMonthList = new List<SelectListItem>();
        }

        public CreateOrEditPayPeriodDto PayPeriod { get; set; }

        public IList<SelectListItem> PayrollFreqList { get; set; }
        public IList<SelectListItem> AppMonthList { get; set; }

        public bool IsEditMode => PayPeriod.Id.HasValue;
    }
}