﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.PayPeriod
{
    public class PayPeriodViewModel : GetPayPeriodForViewDto
    {
        public PayPeriodViewModel()
        {
            ApplicableYearList = new List<SelectListItem>();
            PayrollFreqList = new List<SelectListItem>();
        }

        public IList<SelectListItem> ApplicableYearList { get; set; }
        public IList<SelectListItem> PayrollFreqList { get; set; }
    }
}