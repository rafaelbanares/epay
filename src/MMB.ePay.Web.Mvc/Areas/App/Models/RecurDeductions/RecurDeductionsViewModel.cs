﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.RecurDeductions
{
    public class RecurDeductionsViewModel : GetRecurDeductionsForViewDto
    {
        public string FilterText { get; set; }
    }
}