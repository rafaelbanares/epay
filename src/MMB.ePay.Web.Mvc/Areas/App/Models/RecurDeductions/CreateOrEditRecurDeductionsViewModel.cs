﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.RecurDeductions
{
    public class CreateOrEditRecurDeductionsModalViewModel
    {
        public CreateOrEditRecurDeductionsModalViewModel()
        {
            DeductionTypeList = new List<SelectListItem>();
        }

        public CreateOrEditRecurDeductionsDto RecurDeductions { get; set; }

        public IList<SelectListItem> DeductionTypeList { get; set; }

        public bool IsEditMode => RecurDeductions.Id.HasValue;
    }
}