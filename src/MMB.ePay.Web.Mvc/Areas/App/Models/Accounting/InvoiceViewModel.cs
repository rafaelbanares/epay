﻿using MMB.ePay.MultiTenancy.Accounting.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Accounting
{
    public class InvoiceViewModel
    {
        public InvoiceDto Invoice { get; set; }
    }
}
