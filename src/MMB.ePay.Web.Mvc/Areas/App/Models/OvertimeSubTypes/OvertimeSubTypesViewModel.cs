﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeSubTypes
{
    public class OvertimeSubTypesViewModel : GetOvertimeSubTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}