﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeSubTypes
{
    public class CreateOrEditOvertimeSubTypesModalViewModel
    {
        public CreateOrEditOvertimeSubTypesDto OvertimeSubTypes { get; set; }

        public bool IsEditMode => OvertimeSubTypes.Id.HasValue;
    }
}