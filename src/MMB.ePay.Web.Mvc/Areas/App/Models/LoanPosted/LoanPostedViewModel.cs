﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LoanPosted
{
    public class LoanPostedViewModel : GetLoanPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}