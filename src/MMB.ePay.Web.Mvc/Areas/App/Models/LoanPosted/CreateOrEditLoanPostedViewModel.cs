﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LoanPosted
{
    public class CreateOrEditLoanPostedModalViewModel
    {
        public CreateOrEditLoanPostedDto LoanPosted { get; set; }

        public bool IsEditMode => LoanPosted.Id.HasValue;
    }
}