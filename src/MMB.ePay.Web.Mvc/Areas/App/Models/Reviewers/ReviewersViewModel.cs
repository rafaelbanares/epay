﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Reviewers
{
    public class ReviewersViewModel : GetReviewersForViewDto
    {
        public string FilterText { get; set; }
    }
}