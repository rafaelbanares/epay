﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Reviewers
{
    public class CreateOrEditReviewersModalViewModel
    {
        public CreateOrEditReviewersModalViewModel()
        {
            ReviewerTypeList = new List<SelectListItem>();
            ReviewedByList = new List<SelectListItem>();
        }

        public CreateOrEditReviewersDto Reviewers { get; set; }

        public IList<SelectListItem> ReviewerTypeList { get; set; }

        public IList<SelectListItem> ReviewedByList { get; set; }

        public bool IsEditMode => Reviewers.Id.HasValue;
    }
}