﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.PayrollProcess
{
    public class CreateOrEditPayrollProcessModalViewModel
    {
        public CreateOrEditPayrollProcessDto PayrollProcess { get; set; }

        public bool IsEditMode => PayrollProcess.Id.HasValue;
    }
}