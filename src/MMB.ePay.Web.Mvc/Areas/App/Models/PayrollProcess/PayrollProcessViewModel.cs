﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.PayrollProcess
{
    public class PayrollProcessViewModel : GetPayrollProcessForViewDto
    {

    }

    public class PayrollProcessProcessingViewModel
    {
        public GetProcessForProcessingViewDto Process { get; set; }
    }
}