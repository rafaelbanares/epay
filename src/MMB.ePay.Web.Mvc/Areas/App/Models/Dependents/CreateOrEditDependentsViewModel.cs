﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Dependents
{
    public class CreateOrEditDependentsModalViewModel
    {
        public CreateOrEditDependentsDto Dependents { get; set; }

        public bool IsEditMode => Dependents.Id.HasValue;
    }
}