﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Dependents
{
    public class DependentsViewModel : GetDependentsForViewDto
    {
        public string FilterText { get; set; }
    }
}