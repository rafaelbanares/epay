﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.EarningTypes
{
    public class CreateOrEditEarningTypesModalViewModel
    {
        public CreateOrEditEarningTypesModalViewModel()
        {
            CurrencyList = new List<SelectListItem>();
        }

        public CreateOrEditEarningTypesDto EarningTypes { get; set; }

        public IList<SelectListItem> CurrencyList { get; set; }

        public bool IsEditMode => EarningTypes.Id.HasValue;
    }
}