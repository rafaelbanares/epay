﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EarningTypes
{
    public class EarningTypesViewModel : GetEarningTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}