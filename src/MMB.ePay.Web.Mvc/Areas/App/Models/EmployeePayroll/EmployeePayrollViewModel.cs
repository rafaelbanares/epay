﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EmployeePayroll
{
    public class EmployeePayrollViewModel : GetEmployeePayrollForViewDto
    {
        public string FilterText { get; set; }
    }
}