﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.EmployeePayroll
{
    public class CreateOrEditEmployeePayrollModalViewModel
    {
        public CreateOrEditEmployeePayrollDto EmployeePayroll { get; set; }

        public bool IsEditMode => EmployeePayroll.Id.HasValue;
    }
}