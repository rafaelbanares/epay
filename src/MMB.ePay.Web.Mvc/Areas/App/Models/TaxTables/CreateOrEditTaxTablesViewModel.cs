﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.TaxTables
{
    public class CreateOrEditTaxTablesModalViewModel
    {
        public CreateOrEditTaxTablesModalViewModel()
        {
            YearList = new List<SelectListItem>();
            PayFreqList = new List<SelectListItem>();
            CodeList = new List<SelectListItem>();
        }

        public CreateOrEditTaxTablesDto TaxTables { get; set; }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> PayFreqList { get; set; }
        public IList<SelectListItem> CodeList { get; set; }


        public bool IsEditMode => TaxTables.Id.HasValue;
    }
}