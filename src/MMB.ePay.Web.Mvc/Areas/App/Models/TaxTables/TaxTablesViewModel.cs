﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.TaxTables
{
    public class TaxTablesViewModel : GetTaxTablesForViewDto
    {
        public TaxTablesViewModel()
        {
            YearList = new List<SelectListItem>();
            PayFreqList = new List<SelectListItem>();
        }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> PayFreqList { get; set; }
    }
}