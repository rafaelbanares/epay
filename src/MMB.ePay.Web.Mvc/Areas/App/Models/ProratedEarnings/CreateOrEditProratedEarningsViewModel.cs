﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.ProratedEarnings
{
    public class CreateOrEditProratedEarningsModalViewModel
    {
        public CreateOrEditProratedEarningsModalViewModel()
        {
            EarningTypeList = new List<SelectListItem>();
            TimesheetTypeList = new List<SelectListItem>();
            OvertimeTypeList = new List<SelectListItem>();
            OutEarningTypeList = new List<SelectListItem>();
        }

        public CreateOrEditProratedEarningsDto ProratedEarnings { get; set; }
        
        public IList<SelectListItem> EarningTypeList { get; set; }
        
        public IList<SelectListItem> TimesheetTypeList { get; set; }
        
        public IList<SelectListItem> OvertimeTypeList { get; set; }
        
        public IList<SelectListItem> OutEarningTypeList { get; set; }
        
        public bool IsEditMode => ProratedEarnings.Id.HasValue;
    }
}