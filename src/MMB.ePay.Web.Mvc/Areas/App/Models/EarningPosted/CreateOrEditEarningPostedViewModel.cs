﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.EarningPosted
{
    public class CreateOrEditEarningPostedModalViewModel
    {
        public CreateOrEditEarningPostedDto EarningPosted { get; set; }

        public bool IsEditMode => EarningPosted.Id.HasValue;
    }
}