﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EarningPosted
{
    public class EarningPostedViewModel : GetEarningPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}