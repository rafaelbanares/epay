﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.DeductionPosted
{
    public class CreateOrEditDeductionPostedModalViewModel
    {
        public CreateOrEditDeductionPostedDto DeductionPosted { get; set; }

        public bool IsEditMode => DeductionPosted.Id.HasValue;
    }
}