﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.DeductionPosted
{
    public class DeductionPostedViewModel : GetDeductionPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}