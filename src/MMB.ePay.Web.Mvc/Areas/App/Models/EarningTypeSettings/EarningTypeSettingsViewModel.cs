﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EarningTypeSettings
{
    public class EarningTypeSettingsViewModel : GetEarningTypeSettingsForViewDto
    {
        public string FilterText { get; set; }
    }
}