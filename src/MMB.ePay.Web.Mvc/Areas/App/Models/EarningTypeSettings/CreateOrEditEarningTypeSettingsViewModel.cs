﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.EarningTypeSettings
{
    public class CreateOrEditEarningTypeSettingsModalViewModel
    {
        public CreateOrEditEarningTypeSettingsDto EarningTypeSettings { get; set; }

        public bool IsEditMode => EarningTypeSettings.Id.HasValue;
    }
}