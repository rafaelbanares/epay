﻿using System.Collections.Generic;
using MMB.ePay.Caching.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}