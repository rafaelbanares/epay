﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetTypes
{
    public class TimesheetTypesViewModel : GetTimesheetTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}