﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetTypes
{
    public class CreateOrEditTimesheetTypesModalViewModel
    {
        public CreateOrEditTimesheetTypesDto TimesheetTypes { get; set; }

        public bool IsEditMode => TimesheetTypes.Id.HasValue;
    }
}