﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryTmp
{
    public class CreateOrEditStatutoryTmpModalViewModel
    {
        public CreateOrEditStatutoryTmpDto StatutoryTmp { get; set; }

        public bool IsEditMode => StatutoryTmp.Id.HasValue;
    }
}