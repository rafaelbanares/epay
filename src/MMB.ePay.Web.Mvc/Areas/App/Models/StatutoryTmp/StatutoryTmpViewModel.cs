﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryTmp
{
    public class StatutoryTmpViewModel : GetStatutoryTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}