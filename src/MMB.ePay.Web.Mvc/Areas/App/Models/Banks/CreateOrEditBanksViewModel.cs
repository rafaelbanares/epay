﻿using Abp.Extensions;
using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Banks
{
    public class CreateOrEditBanksModalViewModel
    {
        public CreateOrEditBanksDto Banks { get; set; }

        public bool IsEditMode => !Banks.Id.IsNullOrWhiteSpace();
    }
}