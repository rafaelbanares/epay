﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Banks
{
    public class BanksViewModel : GetBanksForViewDto
    {
        public string FilterText { get; set; }
    }
}