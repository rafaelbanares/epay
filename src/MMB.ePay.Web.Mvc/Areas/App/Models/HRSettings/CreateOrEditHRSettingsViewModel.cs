﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.HRSettings
{
    public class CreateOrEditHRSettingsModalViewModel
    {
        public CreateOrEditHRSettingsModalViewModel()
        {
            GroupSettingList = new List<SelectListItem>();
        }

        public CreateOrEditHRSettingsDto HRSettings { get; set; }
        public IList<SelectListItem> GroupSettingList { get; set; }
        public bool IsEditMode => HRSettings.Id.HasValue;
    }
}