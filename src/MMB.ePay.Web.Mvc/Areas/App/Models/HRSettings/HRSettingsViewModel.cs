﻿using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.HRSettings
{
    public class HRSettingsViewModel : GetHRSettingsForViewDto
    {
        public string FilterText { get; set; }
    }
}