﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Loans
{
    public class CreateOrEditLoansModalViewModel
    {
        public CreateOrEditLoansModalViewModel()
        {
            LoanTypeList = new List<SelectListItem>();
            CurrencyList = new List<SelectListItem>();
        }

        public CreateOrEditLoansDto Loans { get; set; }

        public IList<SelectListItem> LoanTypeList { get; set; }

        public IList<SelectListItem> CurrencyList { get; set; }

        public bool IsEditMode => Loans.Id.HasValue;
    }
}