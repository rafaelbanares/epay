﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Companies
{
    //public class CompaniesViewModel : GetCompaniesForViewDto
    //{
    //    public string FilterText { get; set; }
    //}

    public class CompaniesViewModel
    {
        public CreateOrEditCompaniesDto Companies { get; set; }
    }
}