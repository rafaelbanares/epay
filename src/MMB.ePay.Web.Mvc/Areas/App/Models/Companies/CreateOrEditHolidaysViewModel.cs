﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Companies
{
    public class CreateOrEditCompaniesViewModel
    {
        public CreateOrEditCompaniesDto Companies { get; set; }

        public bool IsEditMode => Companies.Id.HasValue;
    }
}