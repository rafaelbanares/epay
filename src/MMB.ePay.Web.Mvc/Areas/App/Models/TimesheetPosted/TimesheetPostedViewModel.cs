﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetPosted
{
    public class TimesheetPostedViewModel : GetTimesheetPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}