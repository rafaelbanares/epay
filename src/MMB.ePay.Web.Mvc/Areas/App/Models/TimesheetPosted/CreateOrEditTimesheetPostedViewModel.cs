﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.TimesheetPosted
{
    public class CreateOrEditTimesheetPostedModalViewModel
    {
        public CreateOrEditTimesheetPostedDto TimesheetPosted { get; set; }

        public bool IsEditMode => TimesheetPosted.Id.HasValue;
    }
}