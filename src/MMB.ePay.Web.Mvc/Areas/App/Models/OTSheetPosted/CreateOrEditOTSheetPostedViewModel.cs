﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OTSheetPosted
{
    public class CreateOrEditOTSheetPostedModalViewModel
    {
        public CreateOrEditOTSheetPostedModalViewModel()
        {
            OvertimeTypeList = new List<SelectListItem>();
        }

        public CreateOrEditOTSheetPostedDto OTSheetPosted { get; set; }

        public IList<SelectListItem> OvertimeTypeList { get; set; }

        public bool IsEditMode => OTSheetPosted.Id.HasValue;
    }
}