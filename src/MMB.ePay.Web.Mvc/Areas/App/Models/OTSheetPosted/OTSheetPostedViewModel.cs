﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OTSheetPosted
{
    public class OTSheetPostedViewModel : GetOTSheetPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}