﻿using MMB.ePay.Sessions.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Editions
{
    public class SubscriptionDashboardViewModel
    {
        public GetCurrentLoginInformationsOutput LoginInformations { get; set; }
    }
}
