﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.SssTables
{
    public class CreateOrEditSssTablesModalViewModel
    {
        public CreateOrEditSssTablesDto SssTables { get; set; }

        public bool IsEditMode => SssTables.Id.HasValue;
    }
}