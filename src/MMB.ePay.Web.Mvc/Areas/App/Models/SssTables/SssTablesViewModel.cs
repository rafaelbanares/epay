﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.SssTables
{
    public class SssTablesViewModel : GetSssTablesForViewDto
    {
        public string FilterText { get; set; }
    }
}