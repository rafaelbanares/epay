﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.PaySettings
{
    public class PaySettingsViewModel : GetPaySettingsForViewDto
    {
        public string FilterText { get; set; }
    }
}