﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.PaySettings
{
    public class CreateOrEditPaySettingsModalViewModel
    {
        public CreateOrEditPaySettingsModalViewModel()
        {
            GroupSettingList = new List<SelectListItem>();
        }

        public CreateOrEditPaySettingsDto PaySettings { get; set; }

        public IList<SelectListItem> GroupSettingList { get; set; }

        public bool IsEditMode => PaySettings.Id.HasValue;
    }
}