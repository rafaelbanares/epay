﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.PayrollTransactions
{
    public class PayrollTransactionsViewModel
    {
        public PayrollTransactionsViewModel()
        {
            EmployeeList = new List<SelectListItem>();
            TimesheetTypesMenu = new List<string>();
            OvertimeSubTypesMenu = new List<string>();
            OvertimeMainTypesMenu = new List<Tuple<string, string>>();
        }

        public IList<SelectListItem> EmployeeList { get; set; }
        public IList<string> TimesheetTypesMenu { get; set; }
        public IList<string> OvertimeSubTypesMenu { get; set; }
        public IList<Tuple<string, string>> OvertimeMainTypesMenu { get; set; }
    }
}
