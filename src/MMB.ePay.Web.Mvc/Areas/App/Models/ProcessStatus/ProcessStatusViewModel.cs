﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ProcessStatus
{
    public class ProcessStatusViewModel : GetProcessStatusForViewDto
    {
        public string FilterText { get; set; }
    }
}