﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ProcessStatus
{
    public class CreateOrEditProcessStatusModalViewModel
    {
        public CreateOrEditProcessStatusDto ProcessStatus { get; set; }

        public bool IsEditMode => ProcessStatus.Id.HasValue;
    }
}