﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LoanTmp
{
    public class LoanTmpViewModel : GetLoanTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}