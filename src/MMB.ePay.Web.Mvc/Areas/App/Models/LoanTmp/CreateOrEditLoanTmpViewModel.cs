﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LoanTmp
{
    public class CreateOrEditLoanTmpModalViewModel
    {
        public CreateOrEditLoanTmpDto LoanTmp { get; set; }

        public bool IsEditMode => LoanTmp.Id.HasValue;
    }
}