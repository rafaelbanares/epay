﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OTSheetTmp
{
    public class CreateOrEditOTSheetTmpModalViewModel
    {
        public CreateOrEditOTSheetTmpModalViewModel()
        {
            OvertimeTypeList = new List<SelectListItem>();
        }

        public CreateOrEditOTSheetTmpDto OTSheetTmp { get; set; }

        public IList<SelectListItem> OvertimeTypeList { get; set; }

        public bool IsEditMode => OTSheetTmp.Id.HasValue;
    }
}