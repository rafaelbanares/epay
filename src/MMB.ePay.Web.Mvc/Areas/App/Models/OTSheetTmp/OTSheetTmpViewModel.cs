﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OTSheetTmp
{
    public class OTSheetTmpViewModel : GetOTSheetTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}