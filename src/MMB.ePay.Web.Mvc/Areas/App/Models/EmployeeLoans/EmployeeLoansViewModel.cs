﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.EmployeeLoans
{
    public class EmployeeLoansViewModel : GetEmployeeLoansForViewDto
    {
        public EmployeeLoansViewModel()
        {
            StatusList = new List<SelectListItem>();

            YearList = new List<SelectListItem>();
        }

        public IList<SelectListItem> StatusList { get; set; }

        public IList<SelectListItem> YearList { get; set; }
    }
}