﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.EmployeeLoans
{
    public class CreateOrEditEmployeeLoansModalViewModel
    {
        public CreateOrEditEmployeeLoansModalViewModel()
        {
            LoanList = new List<SelectListItem>();
        }

        public bool IsEditMode => EmployeeLoans.Id.HasValue;

        public CreateOrEditEmployeeLoansDto EmployeeLoans { get; set; }

        public IList<SelectListItem> LoanList { get; set; }
    }
}