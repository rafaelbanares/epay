using System.Collections.Generic;
using Abp.Application.Services.Dto;
using MMB.ePay.Editions.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}