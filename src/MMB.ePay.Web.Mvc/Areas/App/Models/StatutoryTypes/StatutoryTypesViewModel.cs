﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryTypes
{
    public class StatutoryTypesViewModel : GetStatutoryTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}