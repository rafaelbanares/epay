﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryTypes
{
    public class CreateOrEditStatutoryTypesModalViewModel
    {
        public CreateOrEditStatutoryTypesDto StatutoryTypes { get; set; }

        public bool IsEditMode => StatutoryTypes.Id.HasValue;
    }
}