using Abp.AutoMapper;
using MMB.ePay.MultiTenancy;
using MMB.ePay.MultiTenancy.Dto;
using MMB.ePay.Web.Areas.App.Models.Common;

namespace MMB.ePay.Web.Areas.App.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }
    }
}