﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Earnings
{
    public class CreateOrEditEarningsModalViewModel
    {
        public CreateOrEditEarningsModalViewModel()
        {
            EarningTypeList = new List<SelectListItem>();
        }

        public CreateOrEditEarningsDto Earnings { get; set; }

        public IList<SelectListItem> EarningTypeList { get; set; }

        public bool IsEditMode => Earnings.Id.HasValue;
    }
}