﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Earnings
{
    public class EarningsViewModel : GetEarningsForViewDto
    {
        public string FilterText { get; set; }
    }
}