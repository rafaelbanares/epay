﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.TenantBanks
{
    public class TenantBanksViewModel : GetTenantBanksForViewDto
    {
        public string FilterText { get; set; }
    }
}