﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Reports
{
    public class ReportsViewModel : GetReportsForViewDto
    {

    }

    public class PeriodicReportsViewModel : GetReportsForViewDto
    {
        public PeriodicReportsViewModel()
        {
            MonthList = new List<SelectListItem>();
            PayPeriodList = new List<SelectListItem>();
            DepartmentList = new List<SelectListItem>();
        }

        public IList<SelectListItem> MonthList { get; set; }
        public IList<SelectListItem> PayPeriodList { get; set; }
        public IList<SelectListItem> DepartmentList { get; set; }
    }

    public class MonthlyReportsViewModel : GetReportsForViewDto
    {
        public MonthlyReportsViewModel()
        {
            MonthList = new List<SelectListItem>();
            DepartmentList = new List<SelectListItem>();
        }

        public IList<SelectListItem> MonthList { get; set; }
        public IList<SelectListItem> DepartmentList { get; set; }
    }

    public class QuarterlyReportsViewModel : GetReportsForViewDto
    {
        public QuarterlyReportsViewModel()
        {
            QuarterList = new List<SelectListItem>();
        }

        public IList<SelectListItem> QuarterList { get; set; }
    }

    public class AnnualReportsViewModel : GetReportsForViewDto
    {

    }
}