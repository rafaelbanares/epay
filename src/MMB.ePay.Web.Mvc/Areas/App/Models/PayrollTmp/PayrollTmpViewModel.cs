﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.PayrollTmp
{
    public class PayrollTmpViewModel : GetPayrollTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}