﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.PayrollTmp
{
    public class CreateOrEditPayrollTmpModalViewModel
    {
        public CreateOrEditPayrollTmpDto PayrollTmp { get; set; }

        public bool IsEditMode => PayrollTmp.Id.HasValue;
    }
}