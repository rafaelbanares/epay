﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.BankBranches
{
    public class BankBranchesViewModel : GetBankBranchesForViewDto
    {
        public string FilterText { get; set; }
    }
}