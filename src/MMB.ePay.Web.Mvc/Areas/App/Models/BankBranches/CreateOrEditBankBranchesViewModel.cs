﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.BankBranches
{
    public class CreateOrEditBankBranchesModalViewModel
    {
        public CreateOrEditBankBranchesDto BankBranches { get; set; }

        public bool IsEditMode => BankBranches.Id.HasValue;
    }
}