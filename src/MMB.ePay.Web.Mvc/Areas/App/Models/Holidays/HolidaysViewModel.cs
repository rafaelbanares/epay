﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Holidays
{
    public class HolidaysViewModel : GetHolidaysForViewDto
    {
        public string FilterText { get; set; }
    }
}