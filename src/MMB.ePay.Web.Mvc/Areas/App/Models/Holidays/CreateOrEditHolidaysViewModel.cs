﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Holidays
{
    public class CreateOrEditHolidaysViewModel
    {
        public CreateOrEditHolidaysDto Holidays { get; set; }

        public IList<SelectListItem> HolidayTypeList { get; set; }

        public bool IsEditMode => Holidays.Id.HasValue;
    }
}