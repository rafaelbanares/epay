﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EarningTmp
{
    public class EarningTmpViewModel : GetEarningTmpForViewDto
    {
        public string FilterText { get; set; }
    }
}