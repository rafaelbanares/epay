﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.EarningTmp
{
    public class CreateOrEditEarningTmpModalViewModel
    {
        public CreateOrEditEarningTmpDto EarningTmp { get; set; }

        public bool IsEditMode => EarningTmp.Id.HasValue;
    }
}