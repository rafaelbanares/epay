﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Deductions
{
    public class CreateOrEditDeductionsModalViewModel
    {
        public CreateOrEditDeductionsModalViewModel()
        {
            DeductionTypeList = new List<SelectListItem>();
        }

        public CreateOrEditDeductionsDto Deductions { get; set; }

        public IList<SelectListItem> DeductionTypeList { get; set; }

        public bool IsEditMode => Deductions.Id.HasValue;
    }
}