﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Deductions
{
    public class DeductionsViewModel : GetDeductionsForViewDto
    {
        public string FilterText { get; set; }
    }
}