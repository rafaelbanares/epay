﻿using Abp.Extensions;
using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EmploymentTypes
{
    public class CreateOrEditEmploymentTypesModalViewModel
    {
        public CreateOrEditEmploymentTypesDto EmploymentTypes { get; set; }

        public bool IsEditMode => !EmploymentTypes.Id.IsNullOrWhiteSpace();
    }
}