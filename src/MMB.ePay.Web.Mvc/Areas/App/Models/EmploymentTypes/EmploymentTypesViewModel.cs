﻿using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EmploymentTypes
{
    public class EmploymentTypesViewModel : GetEmploymentTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}