﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.PayrollPosted
{
    public class PayrollPostedViewModel : GetPayrollPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}