﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.PayrollPosted
{
    public class CreateOrEditPayrollPostedModalViewModel
    {
        public CreateOrEditPayrollPostedDto PayrollPosted { get; set; }

        public bool IsEditMode => PayrollPosted.Id.HasValue;
    }
}