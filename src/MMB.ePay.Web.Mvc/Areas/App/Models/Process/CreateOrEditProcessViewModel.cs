﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Process
{
    public class CreateOrEditProcessModalViewModel
    {
        public CreateOrEditProcessModalViewModel()
        {
            ProcessTypeList = new List<SelectListItem>();
            PayrollFreqList = new List<SelectListItem>();
            YearList = new List<SelectListItem>();
        }

        public CreateOrEditProcessDto Process { get; set; }
        public IList<SelectListItem> ProcessTypeList { get; set; }
        public IList<SelectListItem> PayrollFreqList { get; set; }
        public IList<SelectListItem> YearList { get; set; }
    }
}



