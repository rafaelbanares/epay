﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Process
{
    public class ProcessViewModel : GetProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}