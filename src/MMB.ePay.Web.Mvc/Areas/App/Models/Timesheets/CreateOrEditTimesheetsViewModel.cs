﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.Timesheets
{
    public class CreateOrEditTimesheetsModalViewModel
    {
        public CreateOrEditTimesheetsDto Timesheets { get; set; }

        public bool IsEditMode => Timesheets.Id.HasValue;
    }
}