﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Timesheets
{
    public class TimesheetsViewModel : GetTimesheetsForViewDto
    {
        public string FilterText { get; set; }
    }
}