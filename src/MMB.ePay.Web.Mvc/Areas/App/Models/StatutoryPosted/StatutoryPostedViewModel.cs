﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryPosted
{
    public class StatutoryPostedViewModel : GetStatutoryPostedForViewDto
    {
        public string FilterText { get; set; }
    }
}