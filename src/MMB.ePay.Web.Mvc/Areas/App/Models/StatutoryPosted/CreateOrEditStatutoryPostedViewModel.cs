﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.StatutoryPosted
{
    public class CreateOrEditStatutoryPostedModalViewModel
    {
        public CreateOrEditStatutoryPostedDto StatutoryPosted { get; set; }

        public IList<SelectListItem> StatutoryTypeList { get; set; }

        public bool IsEditMode => StatutoryPosted.Id.HasValue;
    }
}