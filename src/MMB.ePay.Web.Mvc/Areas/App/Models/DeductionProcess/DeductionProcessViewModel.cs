﻿using MMB.ePay.Payroll.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.DeductionProcess
{
    public class DeductionProcessViewModel : GetDeductionProcessForViewDto
    {
        public string FilterText { get; set; }
    }
}