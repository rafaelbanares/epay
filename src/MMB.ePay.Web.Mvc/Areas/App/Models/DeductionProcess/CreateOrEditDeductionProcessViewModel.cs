﻿using MMB.ePay.Payroll.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.DeductionProcess
{
    public class CreateOrEditDeductionProcessModalViewModel
    {
        public CreateOrEditDeductionProcessDto DeductionProcess { get; set; }

        public bool IsEditMode => DeductionProcess.Id.HasValue;
    }
}