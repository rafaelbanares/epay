﻿namespace MMB.ePay.Web.Areas.App.Models.Layout
{
    public class QuickThemeSelectionViewModel
    {
        public string CssClass { get; set; }
    }
}
