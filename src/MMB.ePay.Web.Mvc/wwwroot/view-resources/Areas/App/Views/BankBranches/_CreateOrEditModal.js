(function($) {
    app.modals.CreateOrEditBankBranchesModal = function() {
        var _bankBranchesService = abp.services.app.bankBranches;
        var _modalManager;
        var _$bankBranchesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$bankBranchesInformationForm = _modalManager.getModal().find('form[name=BankBranchesInformationsForm]');
            _$bankBranchesInformationForm.validate();
        };
        this.save = function() {
            if (!_$bankBranchesInformationForm.valid()) {
                return;
            }
            var bankBranches = _$bankBranchesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _bankBranchesService.createOrEdit(
                bankBranches
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditBankBranchesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);