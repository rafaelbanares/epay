﻿(function () {
    $(function () {
        var url = window.location.href;
        var bankId = url.substring(url.lastIndexOf('/') + 1);

        var _$bankBranchesTable = $('#BankBranchesTable');
        var _bankBranchesService = abp.services.app.bankBranches;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.BankBranches.Create'),
            edit: abp.auth.hasPermission('Pages.BankBranches.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/BankBranches/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/BankBranches/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditBankBranchesModal'
                });
                   

		 var _viewBankBranchesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/BankBranches/ViewbankBranchesModal',
            modalClass: 'ViewBankBranchesModal'
        });

        var dataTable = _$bankBranchesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _bankBranchesService.getAll,
                inputFilter: function () {
                    return {
                    bankId: bankId
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewBankBranchesModal.open({ id: data.record.bankBranches.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.bankBranches.id, bankId: bankId });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "bankBranches.branchName",
						 name: "branchName"   
					},
					{
						targets: 3,
						 data: "bankBranches.address1",
						 name: "address1"   
					},
					{
						targets: 4,
						 data: "bankBranches.address2",
						 name: "address2"   
					}
            ]
        });

        function getBankBranches() {
            dataTable.ajax.reload();
        }

        $('#CreateNewBankBranchesButton').click(function () {
            _createOrEditModal.open({ bankId: bankId });
        });        

        abp.event.on('app.createOrEditBankBranchesModalSaved', function () {
            getBankBranches();
        });

		$('#GetBankBranchesButton').click(function (e) {
            e.preventDefault();
            getBankBranches();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getBankBranches();
		  }
		});
    });
})();
