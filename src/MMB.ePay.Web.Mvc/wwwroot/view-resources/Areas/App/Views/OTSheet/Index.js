(function () {
    $(function () {
        var _otsheetService = abp.services.app.oTSheet;

        getOvertime();

        function getOvertime() {
            var employeeId = $('#SearchFilterId').val();

            $.ajax({
                url: "/App/PayrollTransactions/PopulateOTSheet",
                type: "GET",
                dataType: "json",
                data: { employeeId: employeeId },
                success: function (items) {
                    $('input[name=rate]').each(function (index) {
                        $(this).val(items['result'][index]);
                    });
                }
            });
        }

        abp.event.on('app.createOrEditOTSheetModalSaved', function () {
            getOvertime();
        });

        $('#btnOvertimeTab').click(function (e) {
            e.preventDefault();
            getOvertime();
        });

        //$('#EmployeeIdFilterId').change(function () {
        //    var activeTab = $('ul.nav-tabs li a.active').data('id');
        //    if (activeTab == 'overtime') {
        //        getOvertime();
        //    }
        //});

        var _$otsheetForm = $('form[name=OTSheetInformationsForm]');
        _$otsheetForm.validate();

        function saveOTSheet(successCallback) {
            if (!_$otsheetForm.valid()) {
                return;
            }

            var rate = [];
            $('input[name=rate]').each(function () {
                rate.push($(this).val());
            });

            var otsheet = {};
            otsheet["employeeId"] = $('#SearchFilterId').val();
            otsheet["rate"] = rate;

            abp.ui.setBusy();

            _otsheetService.createOrEdit(
                otsheet
            ).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                abp.event.trigger('app.createOrEditOTSheetModalSaved');
                if (typeof (successCallback) === 'function') {
                    successCallback();
                }
            }).always(function () {
                abp.ui.clearBusy();
            });
        }

        $('#saveBtnOTSheet').click(function () {
            saveOTSheet(function () {
                getOvertime();
            });
        });
    });
})();