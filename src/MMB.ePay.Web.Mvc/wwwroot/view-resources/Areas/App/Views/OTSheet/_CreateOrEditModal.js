(function($) {
    app.modals.CreateOrEditOTSheetModal = function() {
        var _otSheetService = abp.services.app.otSheet;
        var _modalManager;
        var _$otSheetInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$otSheetInformationForm = _modalManager.getModal().find('form[name=OTSheetInformationsForm]');
            _$otSheetInformationForm.validate();
        };
        this.save = function() {
            if (!_$otSheetInformationForm.valid()) {
                return;
            }
            var otSheet = _$otSheetInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _otSheetService.createOrEdit(
                otSheet
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOTSheetModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);