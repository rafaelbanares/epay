(function($) {
    app.modals.CreateOrEditPayPeriodModal = function() {
        var _payPeriodService = abp.services.app.payPeriod;
        var _modalManager;
        var _$payPeriodInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$payPeriodInformationForm = _modalManager.getModal().find('form[name=PayPeriodInformationsForm]');
            _$payPeriodInformationForm.validate();
        };
        this.save = function() {
            if (!_$payPeriodInformationForm.valid()) {
                return;
            }
            var payPeriod = _$payPeriodInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _payPeriodService.createOrEdit(
                payPeriod
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditPayPeriodModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);