(function() {
    $(function() {
        var _$payPeriodTable = $('#PayPeriodTable');
        var _payPeriodService = abp.services.app.payPeriod;
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });
        var _permissions = {
            create: abp.auth.hasPermission('Pages.PayPeriod.Create'),
            edit: abp.auth.hasPermission('Pages.PayPeriod.Edit')
        };
        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PayPeriod/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PayPeriod/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditPayPeriodModal'
        });
        var _viewPayPeriodModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PayPeriod/ViewpayPeriodModal',
            modalClass: 'ViewPayPeriodModal'
        });

        var dataTable = _$payPeriodTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _payPeriodService.getAll,
                inputFilter: function() {
                    return {
                        payrollFreqFilter: $('#PayrollFreqFilterId').val(),
                        applicableYearFilter: $('#ApplicableYearFilterId').val()
                    };
                }
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewPayPeriodModal.open({
                                        id: data.record.payPeriod.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal.open({
                                        id: data.record.payPeriod.id
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "payPeriod.periodFrom",
                    name: "periodFrom",
                    render: function(periodFrom) {
                        if (periodFrom) {
                            return moment(periodFrom).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 3,
                    data: "payPeriod.periodTo",
                    name: "periodTo",
                    render: function(periodTo) {
                        if (periodTo) {
                            return moment(periodTo).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "payPeriod.payDate",
                    name: "payDate",
                    render: function(payDate) {
                        if (payDate) {
                            return moment(payDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "payPeriod.payrollFreq",
                    name: "payrollFreq"
                },
                {
                    targets: 6,
                    data: "payPeriod.processType",
                    name: "processType"
                },
                {
                    targets: 7,
                    data: "payPeriod.remarks",
                    name: "remarks"
                },
                {
                    targets: 8,
                    data: "payPeriod.payrollStatus",
                    name: "payrollStatus"
                }
                
            ]
        });

        function getPayPeriod() {
            dataTable.ajax.reload();
        }

        $('#CreateNewPayPeriodButton').click(function() {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditPayPeriodModalSaved', function() {
            getPayPeriod();
        });
        $('#GetPayPeriodButton').click(function(e) {
            e.preventDefault();
            getPayPeriod();
        });

        $('#PayrollFreqFilterId, #ApplicableYearFilterId').change(function () {
            getPayPeriod();
        });
    });
})();