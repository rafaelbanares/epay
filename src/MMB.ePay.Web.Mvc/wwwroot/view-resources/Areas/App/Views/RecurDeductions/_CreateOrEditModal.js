(function($) {
    app.modals.CreateOrEditRecurDeductionsModal = function() {
        var _recurDeductionsService = abp.services.app.recurDeductions;
        var _modalManager;
        var _$recurDeductionsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$recurDeductionsInformationForm = _modalManager.getModal().find('form[name=RecurDeductionsInformationsForm]');
            _$recurDeductionsInformationForm.validate();
        };
        this.save = function() {
            if (!_$recurDeductionsInformationForm.valid()) {
                return;
            }
            var recurDeductions = _$recurDeductionsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _recurDeductionsService.createOrEdit(
                recurDeductions
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditRecurDeductionsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);