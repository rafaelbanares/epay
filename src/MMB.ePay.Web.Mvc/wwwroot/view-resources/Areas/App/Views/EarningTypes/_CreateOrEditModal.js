(function($) {
    app.modals.CreateOrEditEarningTypesModal = function() {
        var _earningTypesService = abp.services.app.earningTypes;
        var _modalManager;
        var _$earningTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$earningTypesInformationForm = _modalManager.getModal().find('form[name=EarningTypesInformationsForm]');
            _$earningTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$earningTypesInformationForm.valid()) {
                return;
            }
            var earningTypes = _$earningTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _earningTypesService.createOrEdit(
                earningTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEarningTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);