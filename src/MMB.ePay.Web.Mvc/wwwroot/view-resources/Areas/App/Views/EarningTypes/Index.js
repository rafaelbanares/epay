﻿(function () {
    $(function () {

        var _$earningTypesTable = $('#EarningTypesTable');
        var _earningTypesService = abp.services.app.earningTypes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.EarningTypes.Create'),
            edit: abp.auth.hasPermission('Pages.EarningTypes.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EarningTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/EarningTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditEarningTypesModal'
        });
                   
		 var _viewEarningTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EarningTypes/ViewearningTypesModal',
            modalClass: 'ViewEarningTypesModal'
        });

        var dataTable = _$earningTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _earningTypesService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewEarningTypesModal.open({ id: data.record.earningTypes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.earningTypes.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "earningTypes.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "earningTypes.description",
						 name: "description"   
					},
                    {
						targets: 4,
						 data: "earningTypes.currency",
						 name: "currency"   
					},
					{
						targets: 5,
						 data: "earningTypes.taxable",
						 name: "taxable"  ,
						render: function (taxable) {
							if (taxable) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					}
					
            ]
        });

        function getEarningTypes() {
            dataTable.ajax.reload();
        }

        $('#CreateNewEarningTypesButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditEarningTypesModalSaved', function () {
            getEarningTypes();
        });

		$('#GetEarningTypesButton').click(function (e) {
            e.preventDefault();
            getEarningTypes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getEarningTypes();
		  }
		});
    });
})();
