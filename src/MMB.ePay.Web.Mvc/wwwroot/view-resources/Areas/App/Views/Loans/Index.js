﻿(function () {
    $(function () {

        var _$loansTable = $('#LoansTable');
        var _loansService = abp.services.app.loans;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Loans.Create'),
            edit: abp.auth.hasPermission('Pages.Loans.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/Loans/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Loans/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditLoansModal'
                });
                   

		 var _viewLoansModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Loans/ViewloansModal',
            modalClass: 'ViewLoansModal'
        });

        var dataTable = _$loansTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _loansService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLoansModal.open({ id: data.record.loans.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.loans.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteLoans(data.record.loans);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "loans.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "loans.description",
						 name: "description"   
					},
                    {
						targets: 4,
						 data: "loans.loanType",
						 name: "loanType"   
					},
					{
						targets: 5,
						 data: "loans.currency",
						 name: "currency"   
					}
            ]
        });

        function getLoans() {
            dataTable.ajax.reload();
        }

        function deleteLoans(loans) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _loansService.delete({
                            id: loans.id
                        }).done(function () {
                            getLoans(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewLoansButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditLoansModalSaved', function () {
            getLoans();
        });

		$('#GetLoansButton').click(function (e) {
            e.preventDefault();
            getLoans();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getLoans();
		  }
		});
		
		
		
    });
})();
