(function($) {
    app.modals.CreateOrEditLoansModal = function() {
        var _loansService = abp.services.app.loans;
        var _modalManager;
        var _$loansInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$loansInformationForm = _modalManager.getModal().find('form[name=LoansInformationsForm]');
            _$loansInformationForm.validate();
        };
        this.save = function() {
            if (!_$loansInformationForm.valid()) {
                return;
            }
            var loans = _$loansInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _loansService.createOrEdit(
                loans
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLoansModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);