(function($) {
    app.modals.CreateOrEditTimesheetProcessModal = function() {
        var _timesheetProcessService = abp.services.app.timesheetProcess;
        var _modalManager;
        var _$timesheetProcessInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$timesheetProcessInformationForm = _modalManager.getModal().find('form[name=TimesheetProcessInformationsForm]');
            _$timesheetProcessInformationForm.validate();
        };
        this.save = function() {
            if (!_$timesheetProcessInformationForm.valid()) {
                return;
            }
            var timesheetProcess = _$timesheetProcessInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _timesheetProcessService.createOrEdit(
                timesheetProcess
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTimesheetProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);