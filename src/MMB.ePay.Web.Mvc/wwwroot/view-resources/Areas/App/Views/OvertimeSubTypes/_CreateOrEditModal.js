(function($) {
    app.modals.CreateOrEditOvertimeSubTypesModal = function() {
        var _overtimeSubTypesService = abp.services.app.overtimeSubTypes;
        var _modalManager;
        var _$overtimeSubTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$overtimeSubTypesInformationForm = _modalManager.getModal().find('form[name=OvertimeSubTypesInformationsForm]');
            _$overtimeSubTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$overtimeSubTypesInformationForm.valid()) {
                return;
            }
            var overtimeSubTypes = _$overtimeSubTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _overtimeSubTypesService.createOrEdit(
                overtimeSubTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeSubTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);