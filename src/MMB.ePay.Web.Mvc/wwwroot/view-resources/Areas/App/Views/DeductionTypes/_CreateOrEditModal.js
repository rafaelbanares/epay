(function($) {
    app.modals.CreateOrEditDeductionTypesModal = function() {
        var _deductionTypesService = abp.services.app.deductionTypes;
        var _modalManager;
        var _$deductionTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$deductionTypesInformationForm = _modalManager.getModal().find('form[name=DeductionTypesInformationsForm]');
            _$deductionTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$deductionTypesInformationForm.valid()) {
                return;
            }
            var deductionTypes = _$deductionTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _deductionTypesService.createOrEdit(
                deductionTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditDeductionTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);