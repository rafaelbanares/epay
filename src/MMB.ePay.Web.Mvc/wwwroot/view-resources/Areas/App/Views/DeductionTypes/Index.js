﻿(function () {
    $(function () {

        var _$deductionTypesTable = $('#DeductionTypesTable');
        var _deductionTypesService = abp.services.app.deductionTypes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.DeductionTypes.Create'),
            edit: abp.auth.hasPermission('Pages.DeductionTypes.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/DeductionTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/DeductionTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditDeductionTypesModal'
        });

		 var _viewDeductionTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/DeductionTypes/ViewdeductionTypesModal',
            modalClass: 'ViewDeductionTypesModal'
        });

        var dataTable = _$deductionTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _deductionTypesService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewDeductionTypesModal.open({ id: data.record.deductionTypes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.deductionTypes.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "deductionTypes.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "deductionTypes.description",
						 name: "description"   
					},
					{
						targets: 4,
						 data: "deductionTypes.currency",
						 name: "currency"   
					}
            ]
        });

        function getDeductionTypes() {
            dataTable.ajax.reload();
        }

        $('#CreateNewDeductionTypesButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditDeductionTypesModalSaved', function () {
            getDeductionTypes();
        });

		$('#GetDeductionTypesButton').click(function (e) {
            e.preventDefault();
            getDeductionTypes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getDeductionTypes();
		  }
		});
    });
})();
