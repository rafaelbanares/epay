(function($) {
    app.modals.CreateOrEditDeductionPostedModal = function() {
        var _deductionPostedService = abp.services.app.deductionPosted;
        var _modalManager;
        var _$deductionPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$deductionPostedInformationForm = _modalManager.getModal().find('form[name=DeductionPostedInformationsForm]');
            _$deductionPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$deductionPostedInformationForm.valid()) {
                return;
            }
            var deductionPosted = _$deductionPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _deductionPostedService.createOrEdit(
                deductionPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditDeductionPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);