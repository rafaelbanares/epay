(function() {
    $(function() {
        var _$deductionPostedTable = $('#DeductionPostedTable');
        var _deductionPostedService = abp.services.app.deductionPosted;
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });
        var _permissions = {
            create: abp.auth.hasPermission('Pages.DeductionPosted.Create'),
            edit: abp.auth.hasPermission('Pages.DeductionPosted.Edit'),
            'delete': abp.auth.hasPermission('Pages.DeductionPosted.Delete')
        };
        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/DeductionPosted/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/DeductionPosted/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditDeductionPostedModal'
        });
        var _viewDeductionPostedModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/DeductionPosted/ViewdeductionPostedModal',
            modalClass: 'ViewDeductionPostedModal'
        });
        var getDateFilter = function(element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z");
        }
        var getMaxDateFilter = function(element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z");
        }
        var dataTable = _$deductionPostedTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _deductionPostedService.getAll,
                inputFilter: function() {
                    return {
                        filter: $('#DeductionPostedTableFilter').val(),
                        minPayrollNoFilter: $('#MinPayrollNoFilterId').val(),
                        maxPayrollNoFilter: $('#MaxPayrollNoFilterId').val(),
                        minAmountFilter: $('#MinAmountFilterId').val(),
                        maxAmountFilter: $('#MaxAmountFilterId').val(),
                        minEmployeeIdFilter: $('#MinEmployeeIdFilterId').val(),
                        maxEmployeeIdFilter: $('#MaxEmployeeIdFilterId').val(),
                        minDeductionTypeIdFilter: $('#MinDeductionTypeIdFilterId').val(),
                        maxDeductionTypeIdFilter: $('#MaxDeductionTypeIdFilterId').val(),
                        minRecurDeductionIdFilter: $('#MinRecurDeductionIdFilterId').val(),
                        maxRecurDeductionIdFilter: $('#MaxRecurDeductionIdFilterId').val()
                    };
                }
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewDeductionPostedModal.open({
                                        id: data.record.deductionPosted.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal.open({
                                        id: data.record.deductionPosted.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function() {
                                    return _permissions.delete;
                                },
                                action: function(data) {
                                    deleteDeductionPosted(data.record.deductionPosted);
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "deductionPosted.payrollNo",
                    name: "payrollNo"
                },
                {
                    targets: 3,
                    data: "deductionPosted.amount",
                    name: "amount"
                },
                {
                    targets: 4,
                    data: "deductionPosted.employeeId",
                    name: "employeeId"
                },
                {
                    targets: 5,
                    data: "deductionPosted.deductionTypeId",
                    name: "deductionTypeId"
                },
                {
                    targets: 6,
                    data: "deductionPosted.recurDeductionId",
                    name: "recurDeductionId"
                }
            ]
        });

        function getDeductionPosted() {
            dataTable.ajax.reload();
        }

        function deleteDeductionPosted(deductionPosted) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function(isConfirmed) {
                    if (isConfirmed) {
                        _deductionPostedService.delete({
                            id: deductionPosted.id
                        }).done(function() {
                            getDeductionPosted(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }
        $('#ShowAdvancedFiltersSpan').click(function() {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });
        $('#HideAdvancedFiltersSpan').click(function() {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });
        $('#CreateNewDeductionPostedButton').click(function() {
            _createOrEditModal.open();
        });
        $('#ExportToExcelButton').click(function() {
            _deductionPostedService
                .getDeductionPostedToExcel({
                    filter: $('#DeductionPostedTableFilter').val(),
                    minPayrollNoFilter: $('#MinPayrollNoFilterId').val(),
                    maxPayrollNoFilter: $('#MaxPayrollNoFilterId').val(),
                    minAmountFilter: $('#MinAmountFilterId').val(),
                    maxAmountFilter: $('#MaxAmountFilterId').val(),
                    minEmployeeIdFilter: $('#MinEmployeeIdFilterId').val(),
                    maxEmployeeIdFilter: $('#MaxEmployeeIdFilterId').val(),
                    minDeductionTypeIdFilter: $('#MinDeductionTypeIdFilterId').val(),
                    maxDeductionTypeIdFilter: $('#MaxDeductionTypeIdFilterId').val(),
                    minRecurDeductionIdFilter: $('#MinRecurDeductionIdFilterId').val(),
                    maxRecurDeductionIdFilter: $('#MaxRecurDeductionIdFilterId').val()
                })
                .done(function(result) {
                    app.downloadTempFile(result);
                });
        });
        abp.event.on('app.createOrEditDeductionPostedModalSaved', function() {
            getDeductionPosted();
        });
        $('#GetDeductionPostedButton').click(function(e) {
            e.preventDefault();
            getDeductionPosted();
        });
        $(document).keypress(function(e) {
            if (e.which === 13) {
                getDeductionPosted();
            }
        });
    });
})();