(function($) {
    app.modals.CreateOrEditOvertimeMainTypesModal = function () {
        var _overtimeMainTypesService = abp.services.app.overtimeMainTypes;
        var _modalManager;
        var _$overtimeMainTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$overtimeMainTypesInformationForm = _modalManager.getModal().find('form[name=OvertimeMainTypesInformationsForm]');
            _$overtimeMainTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$overtimeMainTypesInformationForm.valid()) {
                return;
            }
            var overtimeMainTypes = _$overtimeMainTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _overtimeMainTypesService.createOrEdit(
                overtimeMainTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeMainTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);