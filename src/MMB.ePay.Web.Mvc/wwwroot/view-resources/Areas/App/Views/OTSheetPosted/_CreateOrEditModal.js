(function($) {
    app.modals.CreateOrEditOTSheetPostedModal = function() {
        var _otSheetPostedService = abp.services.app.otSheetPosted;
        var _modalManager;
        var _$otSheetPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$otSheetPostedInformationForm = _modalManager.getModal().find('form[name=OTSheetPostedInformationsForm]');
            _$otSheetPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$otSheetPostedInformationForm.valid()) {
                return;
            }
            var otSheetPosted = _$otSheetPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _otSheetPostedService.createOrEdit(
                otSheetPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOTSheetPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);