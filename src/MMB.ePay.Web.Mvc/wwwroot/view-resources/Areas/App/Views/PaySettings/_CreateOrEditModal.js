(function($) {
    app.modals.CreateOrEditPaySettingsModal = function() {
        var _paySettingsService = abp.services.app.paySettings;
        var _modalManager;
        var _$paySettingsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$paySettingsInformationForm = _modalManager.getModal().find('form[name=PaySettingsInformationsForm]');
            _$paySettingsInformationForm.validate();
        };
        this.save = function() {
            if (!_$paySettingsInformationForm.valid()) {
                return;
            }
            var paySettings = _$paySettingsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _paySettingsService.createOrEdit(
                paySettings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditPaySettingsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);