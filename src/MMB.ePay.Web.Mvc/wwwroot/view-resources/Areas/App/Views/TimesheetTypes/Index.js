﻿(function () {
    $(function () {
        var _$timesheetTypesTable = $('#TimesheetTypesTable');
        var _timesheetTypesService = abp.services.app.timesheetTypes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.TimesheetTypes.Create'),
            edit: abp.auth.hasPermission('Pages.TimesheetTypes.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/TimesheetTypes/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/TimesheetTypes/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditTimesheetTypesModal'
                });

		 var _viewTimesheetTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TimesheetTypes/ViewtimesheetTypesModal',
            modalClass: 'ViewTimesheetTypesModal'
        });

        var dataTable = _$timesheetTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _timesheetTypesService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewTimesheetTypesModal.open({ id: data.record.timesheetTypes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.timesheetTypes.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "timesheetTypes.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "timesheetTypes.description",
						 name: "description"   
					},
					{
						targets: 4,
						 data: "timesheetTypes.multiplier",
						 name: "multiplier"   
					},
					{
						targets: 5,
						 data: "timesheetTypes.taxable",
						 name: "taxable"  ,
						render: function (taxable) {
							if (taxable) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
					}
            ]
        });

        function getTimesheetTypes() {
            dataTable.ajax.reload();
        }

        $('#CreateNewTimesheetTypesButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditTimesheetTypesModalSaved', function () {
            getTimesheetTypes();
        });

		$('#GetTimesheetTypesButton').click(function (e) {
            e.preventDefault();
            getTimesheetTypes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getTimesheetTypes();
		  }
		});
		
		
		
    });
})();
