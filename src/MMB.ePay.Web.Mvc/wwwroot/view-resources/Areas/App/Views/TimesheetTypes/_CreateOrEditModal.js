(function($) {
    app.modals.CreateOrEditTimesheetTypesModal = function() {
        var _timesheetTypesService = abp.services.app.timesheetTypes;
        var _modalManager;
        var _$timesheetTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$timesheetTypesInformationForm = _modalManager.getModal().find('form[name=TimesheetTypesInformationsForm]');
            _$timesheetTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$timesheetTypesInformationForm.valid()) {
                return;
            }
            var timesheetTypes = _$timesheetTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _timesheetTypesService.createOrEdit(
                timesheetTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTimesheetTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);