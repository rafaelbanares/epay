(function($) {
    app.modals.CreateOrEditContactPersonsModal = function () {
        var _contactPersonsService = abp.services.app.contactPersons;
        var _modalManager;
        var _$contactPersonsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$contactPersonsInformationForm = _modalManager.getModal().find('form[name=ContactPersonsInformationsForm]');
            _$contactPersonsInformationForm.validate();
        };
        this.save = function() {
            if (!_$contactPersonsInformationForm.valid()) {
                return;
            }
            var contactPersons = _$contactPersonsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _contactPersonsService.createOrEdit(
                contactPersons
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditContactPersonsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);