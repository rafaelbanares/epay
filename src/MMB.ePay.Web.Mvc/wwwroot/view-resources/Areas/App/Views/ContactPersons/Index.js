﻿(function () {
    $(function () {

        var _$contactPersonsTable = $('#ContactPersonsTable');
        var _contactPersonsService = abp.services.app.contactPersons;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.ContactPersons.Create'),
            edit: abp.auth.hasPermission('Pages.ContactPersons.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/ContactPersons/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ContactPersons/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditContactPersonsModal'
                });
                   

		 var _viewContactPersonsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ContactPersons/ViewcontactPersonsModal',
            modalClass: 'ViewContactPersonsModal'
        });

        var dataTable = _$contactPersonsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _contactPersonsService.getAll,
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewContactPersonsModal.open({ id: data.record.contactPersons.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.contactPersons.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						data: "contactPersons.fullName",
                        name: "fullName"
					},
                    {
						targets: 3,
						data: "contactPersons.contactNo",
                        name: "contactNo"
					},
					{
						targets: 4,
						data: "contactPersons.position",
                        name: "position"
					}
            ]
        });

        function getContactPersons() {
            dataTable.ajax.reload();
        }

        $('#CreateNewContactPersonsButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditContactPersonsModalSaved', function () {
            getContactPersons();
        });

		$('#GetContactPersonsButton').click(function (e) {
            e.preventDefault();
            getContactPersons();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getContactPersons();
		  }
		});
    });
})();
