(function($) {
    app.modals.CreateOrEditDeductionProcessModal = function() {
        var _deductionProcessService = abp.services.app.deductionProcess;
        var _modalManager;
        var _$deductionProcessInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$deductionProcessInformationForm = _modalManager.getModal().find('form[name=DeductionProcessInformationsForm]');
            _$deductionProcessInformationForm.validate();
        };
        this.save = function() {
            if (!_$deductionProcessInformationForm.valid()) {
                return;
            }
            var deductionProcess = _$deductionProcessInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _deductionProcessService.createOrEdit(
                deductionProcess
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditDeductionProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);