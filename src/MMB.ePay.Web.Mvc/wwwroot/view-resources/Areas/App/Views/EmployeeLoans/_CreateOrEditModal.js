(function ($) {
    app.modals.CreateOrEditEmployeeLoansModal = function () {
        var _employeeLoansService = abp.services.app.employeeLoans;
        var _employeePayrollService = abp.services.app.employeePayroll;
        var _employeesService = abp.services.app.employees;

        var _modalManager;
        var _$employeeLoansInformationForm = null;
        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$employeeLoansInformationForm = _modalManager.getModal().find('form[name=EmployeeLoansInformationsForm]');
            _$employeeLoansInformationForm.validate();
        };

        function addCheckbox(name) {
            var container = $('#cblist');
            var inputs = container.find('input');
            var id = inputs.length + 1;

            $('<input />', { type: 'checkbox', id: 'cb' + id, value: name }).appendTo(container);
            $('<label />', { 'for': 'cb' + id, text: name }).appendTo(container);
        }

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $("#EmployeeLoans_EmployeeName").autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeLoans_EmployeeName').val(ui.item.label);
                        $('#EmployeeLoans_EmployeeId').val(ui.item.id);

                        ChangeEmployee();

                        return false;
                    },
                    minLength: 3
                });

                $("#EmployeeLoans_EmployeeName").autocomplete("option", "appendTo", ".form-validation");
            });

        function ChangeEmployee() {
            var employeeId = $('#EmployeeLoans_EmployeeId').val();

            if (employeeId == '0') {
                $('#chkFrequency').html('');
                $('#divFreq').hide();
            }
            else {
                _modalManager.setBusy(true);
                _employeePayrollService.getPayFreqByEmployeeId(employeeId)
                    .done(function (payFreq) {

                        var checkBoxHtml = '';

                        if (payFreq != null) {
                            $('#divFreq').show();
                        }
                        else {
                            $('#divFreq').hide();
                        }

                        if (payFreq == 'M' || payFreq == 'S' || payFreq == 'W') {
                            checkBoxHtml += '<label class="checkbox">' +
                                '<input type="checkbox" name="firstPeriod" class="form-control" value=true />' +
                                '<span></span>1st Period</label>';
                        }

                        if (payFreq == 'S' || payFreq == 'W') {
                            checkBoxHtml += '<label class="checkbox">' +
                                '<input type="checkbox" name="secondPeriod" class="form-control" value=true />' +
                                '<span></span>2nd Period</label>';
                        }

                        if (payFreq == 'W') {
                            checkBoxHtml += '<label class="checkbox">' +
                                '<input type="checkbox" name="thirdPeriod" class="form-control" value=true />' +
                                '<span></span>3rd Period</label>' +
                                '<label class="checkbox">' +
                                '<input type="checkbox" name="fourthPeriod" class="form-control" value=true />' +
                                '<span></span>4th Period</label>'
                            '<label class="checkbox">' +
                                '<input type="checkbox" name="lastPeriod" class="form-control" value=true />' +
                                '<span></span>Last Period</label>';
                        }

                        $('#chkFrequency').html(checkBoxHtml);
                    })
                    .always(function () {
                        _modalManager.setBusy(false);
                    });
            }
        }

        this.save = function () {
            if (!_$employeeLoansInformationForm.valid()) {
                return;
            }
            var employeeLoans = _$employeeLoansInformationForm.serializeFormToObject();

            _modalManager.setBusy(true);
            _employeeLoansService.createOrEdit(
                employeeLoans
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEmployeeLoansModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);