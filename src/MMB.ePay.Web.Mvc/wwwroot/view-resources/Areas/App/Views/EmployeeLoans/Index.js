﻿(function () {
    $(function () {
        var _$employeeLoansTable = $('#EmployeeLoansTable');
        var _employeeLoansService = abp.services.app.employeeLoans;
        var _employeesService = abp.services.app.employees;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.EmployeeLoans.Create'),
            edit: abp.auth.hasPermission('Pages.EmployeeLoans.Edit'),
            'delete': abp.auth.hasPermission('Pages.EmployeeLoans.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/EmployeeLoans/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/EmployeeLoans/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditEmployeeLoansModal'
                });
                   

		 var _viewEmployeeLoansModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmployeeLoans/ViewemployeeLoansModal',
            modalClass: 'ViewEmployeeLoansModal'
        });

        var dataTable = _$employeeLoansTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _employeeLoansService.getAll,
                inputFilter: function () {
                    return {
                        filter: $('#SearchFilterId').val(),
                        statusIdFilter: $('#StatusIdFilterId').val(),
                        yearIdFilter: $('#YearIdFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewEmployeeLoansModal.open({ id: data.record.employeeLoans.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.employeeLoans.id });                                
                            }
                        }]
                    }
                },
                {
                    targets: 2,
                    data: "employeeLoans.employee",
                    name: "employee"
                },
                {
                    targets: 3,
                    data: "employeeLoans.loan",
                    name: "loan"
                },
                {
                    targets: 4,
                    data: "employeeLoans.approvedDate",
                    name: "approvedDate",
                    render: function (approvedDate) {
                        if (approvedDate) {
                            return moment(approvedDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "employeeLoans.loanAmount",
                    name: "loanAmount"
                },
                {
                    targets: 6,
                    data: "employeeLoans.amortization",
                    name: "amortization"
                },
                {
                    targets: 7,
                    data: "employeeLoans.balance",
                    name: "balance"
                },
                {
                    targets: 8,
                    data: "employeeLoans.frequency",
                    name: "frequency"
                },
                {
                    targets: 9,
                    data: "employeeLoans.status",
                    name: "status"
                }
            ]
        });

        function getEmployeeLoans() {
            dataTable.ajax.reload();
        }

        $('#CreateNewEmployeeLoansButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditEmployeeLoansModalSaved', function () {
            getEmployeeLoans();
        });

		$('#GetEmployeeLoansButton').click(function (e) {
            e.preventDefault();
            getEmployeeLoans();
        });
		
        $('#SearchFilterId').keyup(function () {
            getEmployeeLoans();
        });

        $('#StatusIdFilterId, #YearIdFilterId').change(function () {
            getEmployeeLoans();
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push(data[index].text);
                });

                $("#SearchFilterId").autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#SearchFilterId').val(ui.item.label);
                        getEmployeeLoans();
                        return false;
                    },
                    minLength: 3
                });
            });
    });
})();
