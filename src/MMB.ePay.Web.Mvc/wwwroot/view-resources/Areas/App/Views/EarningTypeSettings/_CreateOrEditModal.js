(function($) {
    app.modals.CreateOrEditEarningTypeSettingsModal = function() {
        var _earningTypeSettingsService = abp.services.app.earningTypeSettings;
        var _modalManager;
        var _$earningTypeSettingsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$earningTypeSettingsInformationForm = _modalManager.getModal().find('form[name=EarningTypeSettingsInformationsForm]');
            _$earningTypeSettingsInformationForm.validate();
        };
        this.save = function() {
            if (!_$earningTypeSettingsInformationForm.valid()) {
                return;
            }
            var earningTypeSettings = _$earningTypeSettingsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _earningTypeSettingsService.createOrEdit(
                earningTypeSettings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEarningTypeSettingsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);