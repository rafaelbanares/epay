(function($) {
    app.modals.CreateOrEditPayrollProcessModal = function () {
        $('#Process_ProcessType, #Process_PayrollFreq, #Process_ApplicableYear').change(function () {
            var processType = $('#Process_ProcessType').val();
            var payrollFreq = $('#Process_PayrollFreq').val();
            var applicableYear = $('#Process_ApplicableYear').val();

            $.ajax({
                url: "/App/PayrollProcess/GetPayrollNo",
                type: "GET",
                dataType: "json",
                data: { processType: processType, payrollFreq: payrollFreq, applicableYear: applicableYear },
                success: function (items) {
                    $('input[name=payrollNo]').val(items['result'].id);
                    $('#Process_PayrollNo').val(items['result'].periodRange);
                }
            });
        });

        var _processService = abp.services.app.process;
        var _modalManager;
        var _$processInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$processInformationForm = _modalManager.getModal().find('form[name=ProcessInformationsForm]');
            _$processInformationForm.validate();
        };
        this.save = function() {
            if (!_$processInformationForm.valid()) {
                return;
            }
            var process = _$processInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _processService.createOrEdit(
                process
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
                window.location = "/App/PayrollProcess";
            });
        };
    };
})(jQuery);