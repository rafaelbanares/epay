(function() {
    $(function () {
        abp.event.on('app.createOrEditProcessModalSaved');

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PayrollProcess/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PayrollProcess/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditPayrollProcessModal'
        });

        $('#CreateNewPayrollProcessButton').click(function() {
            _createOrEditModal.open();

        });
    });
})();