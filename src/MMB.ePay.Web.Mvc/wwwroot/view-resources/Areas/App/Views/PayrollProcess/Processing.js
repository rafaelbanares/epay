(function () {
    $(function () {
        var _processService = abp.services.app.process;
        var id = $('#id').val();
        var initStep = $('#step').val();
        var isProcessor = $('#isProcessor').val();
        var _wizardEl = KTUtil.getById('kt_wizard_v3');

        var _wizard = new KTWizard(_wizardEl, {
            startStep: initStep, 
            clickableSteps: false  
        });

        _wizard.on('beforeNext', function () {
            _wizard.goNext();
            KTUtil.scrollTop();
            _wizard.stop();  
        });

        _wizard.on('change', function () {
            KTUtil.scrollTop();
        });

        if (isProcessor) {
            $('#divActions').show();
            $('#btnBackToPayrollEntry').prop('disabled', false);
        } else {
            $('#divActions').hide();
            $('#btnBackToPayrollEntry').prop('disabled', true);
        }

        $('#btnBackToPayrollEntry').click(function () {
            if (isProcessor) {
                _processService.resetProcess(id)
                    .done(function () {
                        GetPayrollEntryStep();
                        _wizard.goTo(1);
                    });
            }
        });

        switch (initStep) {
            case '1':
                GetPayrollEntryStep();
                break;
            case '2':
                GetProcessingStep();
                break;
            case '3':
                GetReviewStep();
                break;
            case '4':
                GetPostingStep();
                break;
            default:
                GetPayrollEntryStep();
        }

        //PAYROLL ENTRY STEP 1
        function GetPayrollEntryStep() {
            

            $('#btnBackToPayrollEntry').hide();
            $('[data-wizard-type=action-next]').show();
            $('[data-wizard-type=action-next]').prop("disabled", true);
            $('[data-wizard-type=action-next]').val('Next');

            _processService.getPayrollEntryStep(id)
                .done(function (result) {
                    $('#step').val(result.step);
                    $('#statusCode').val(result.statusCode);

                    $('#PE-Status').text(result.status);
                });
        }

        $('#PayrollTransChecked, #LoansChecked, #EmployeesChecked').click(function () {

            if ($('#PayrollTransChecked').is(':checked')
                && $('#LoansChecked').is(':checked')
                && $('#EmployeesChecked').is(':checked')) {

                $('[data-wizard-type=action-next]').prop('disabled', false);

            } else {

                $('[data-wizard-type=action-next]').prop("disabled", true);
            }
        });

        $('[data-wizard-type=action-next]').click(function () {
            if ($('#step').val() == '1') {
                GetProcessingStep();
            } 

            if ($('#step').val() == '3') {
                _processService.updateProcessToNextStatus(id)
                    .done(function () {
                        GetPostingStep();
                        _wizard.goNext();
                    })
            }
        });

        //PROCESSING STEP 2
        function GetProcessingStep() {
            $('#btnBackToPayrollEntry').show();
            $('[data-wizard-type=action-next]').hide();

            _processService.getProcessingStep(id)
                .done(function (result) {
                    $('#step').val(result.step);
                    $('#statusCode').val(result.statusCode);

                    $('#PR-Status').text(result.status);
                    $('#PR-ElapsedTime').text(result.elapsedTime);
                    $('#btnProcessingUpdate').val(result.nextStatus);

                    if (result.statusCode == 'OP') {
                        $('#btnBackToPayrollEntry').prop('disabled', true);
                    } else {
                        $('#btnBackToPayrollEntry').prop('disabled', false);
                    }

                    var reloadTimer;

                    if (result.statusCode == 'WTP' || result.status == 'OP') {
                        $('#divElapsedTime').show();

                        reloadTimer = setInterval(function () {
                            GetProcessingStep();
                        }, 20000);

                    } else {
                        $('#divElapsedTime').hide();
                        clearInterval(reloadTimer);
                    }

                });

        }

        $('#btnProcessingUpdate').click(function () {
            _processService.updateProcessToNextStatus(id)
                .done(function () {

                    if ($('#statusCode').val() == 'OP') {
                        GetReviewStep();
                        _wizard.goNext();
                    } else {
                        GetProcessingStep();
                    }

                });
        });

        //REVIEW STEP 3
        function GetReviewStep() {
            $('#btnBackToPayrollEntry').show();
            $('[data-wizard-type=action-next]').show();
            $('[data-wizard-type=action-next]').prop('disabled', true);
            $('[data-wizard-type=action-next]').val('Ready For Posting');

            _processService.getReviewStep(id)
                .done(function (result) {

                    if (result.isApprover || isProcessor) {
                        $('#divActions').show();
                    } else {
                        $('#divActions').hide();
                    }

                    if (result.statusCode == 'FIR' && isProcessor) {

                        $('#divConfirm').show();
                        $('#divForNextReview').show();
                        $('#btnForNextReview').prop('disabled', true);

                    } else if (result.statusCode == 'FSR' && result.isApprover) {

                        $('#divConfirm').show();
                        $('#divForNextReview').show();
                        $('#btnForNextReview').prop('disabled', true);

                    } else if (result.statusCode == 'FFR' && result.isApprover) {

                        $('#divConfirm').show();
                        $('#divForNextReview').hide();

                    } else {
                        $('#divConfirm').hide();
                        $('#divForNextReview').hide();
                    }


                    $('#step').val(result.step);
                    $('#statusCode').val(result.statusCode);

                    $('#RV-Status').text(result.status);
                    $('#RV-HeadCount').text(result.headCount);
                    $('#RV-BasicPay').text(result.basicPay);
                    $('#RV-Taxable').text(result.taxable);
                    $('#RV-NonTaxable').text(result.nonTaxable);
                    $('#RV-Loan').text(result.loan);
                    $('#RV-GrossPay').text(result.grossPay);
                    $('#RV-NetPay').text(result.netPay);

                    $('#divStatutories').empty();
                    var htmlStatutories = '';

                    $.each(result.statutories, function (index) {
                        htmlStatutories += '<div class="form-group row"><div class="col-5">'
                            + result.statutories[index].item1
                            + '</div><div class="col-7">'
                            + result.statutories[index].item2
                            + '</div></div>';
                    });

                    $('#divStatutories').html(htmlStatutories);
                });
        }

        $('#chkConfirm').click(function () {

            var statusCode = $('#statusCode').val();
            var button;

            if (statusCode == 'FIR' || statusCode == 'FSR') {
                button = $('#btnForNextReview');
            } else if (statusCode == 'FFR') {
                button = $('[data-wizard-type=action-next]');
            }

            if ($('#chkConfirm').is(':checked')) {
                button.prop('disabled', false);
            }
            else {
                button.prop('disabled', true);
            }
            
        });

        $('#btnForNextReview').click(function () {
            _processService.updateProcessToNextStatus(id)
                .done(function () {
                    GetReviewStep();
                });
        });

        //POSTING STEP 4
        function GetPostingStep() {
            $('#btnBackToPayrollEntry').show();
            $('[data-wizard-type=action-submit]').prop("disabled", true);

            _processService.getPostingStep(id)
                .done(function (result) {
                    $('#PO-Status').text(result.status);
                    $('#step').val(result.step);
                });
        }
    });
})();