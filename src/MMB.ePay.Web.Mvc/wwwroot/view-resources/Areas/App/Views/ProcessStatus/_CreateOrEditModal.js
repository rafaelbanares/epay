(function($) {
    app.modals.CreateOrEditProcessStatusModal = function() {
        var _processStatusService = abp.services.app.processStatus;
        var _modalManager;
        var _$processStatusInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$processStatusInformationForm = _modalManager.getModal().find('form[name=ProcessStatusInformationsForm]');
            _$processStatusInformationForm.validate();
        };
        this.save = function() {
            if (!_$processStatusInformationForm.valid()) {
                return;
            }
            var processStatus = _$processStatusInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _processStatusService.createOrEdit(
                processStatus
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditProcessStatusModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);