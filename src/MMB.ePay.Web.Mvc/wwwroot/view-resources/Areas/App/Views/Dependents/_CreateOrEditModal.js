(function($) {
    app.modals.CreateOrEditDependentsModal = function() {
        var _dependentsService = abp.services.app.dependents;
        var _modalManager;
        var _$dependentsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$dependentsInformationForm = _modalManager.getModal().find('form[name=DependentsInformationsForm]');
            _$dependentsInformationForm.validate();
        };
        this.save = function() {
            if (!_$dependentsInformationForm.valid()) {
                return;
            }
            var dependents = _$dependentsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _dependentsService.createOrEdit(
                dependents
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditDependentsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);