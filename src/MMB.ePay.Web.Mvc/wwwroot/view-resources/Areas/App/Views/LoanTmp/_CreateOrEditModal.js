(function($) {
    app.modals.CreateOrEditLoanTmpModal = function() {
        var _loanTmpService = abp.services.app.loanTmp;
        var _modalManager;
        var _$loanTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$loanTmpInformationForm = _modalManager.getModal().find('form[name=LoanTmpInformationsForm]');
            _$loanTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$loanTmpInformationForm.valid()) {
                return;
            }
            var loanTmp = _$loanTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _loanTmpService.createOrEdit(
                loanTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLoanTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);