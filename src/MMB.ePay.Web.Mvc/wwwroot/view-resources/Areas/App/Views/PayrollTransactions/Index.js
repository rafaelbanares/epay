(function () {
    $(function () {
        var _employeesService = abp.services.app.employees;
        var _earningsService = abp.services.app.earnings;
        var _recurEarningsService = abp.services.app.recurEarnings;
        var _deductionsService = abp.services.app.deductions;
        var _recurDeductionsService = abp.services.app.recurDeductions;

        var _$earningsTable = $('#EarningsTable');
        var _$recurEarningsTable = $('#RecurEarningsTable');
        var _$deductionsTable = $('#DeductionsTable');
        var _$recurDeductionsTable = $('#RecurDeductionsTable');

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        //Earnings
        var _permissionsEarnings = {
            create: abp.auth.hasPermission('Pages.Earnings.Create'),
            edit: abp.auth.hasPermission('Pages.Earnings.Edit'),
            'delete': abp.auth.hasPermission('Pages.Earnings.Delete')
        };

        var _createOrEditModalEarnings = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Earnings/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Earnings/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditEarningsModal'
        });

        var _viewEarningsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Earnings/ViewearningsModal',
            modalClass: 'ViewEarningsModal'
        });

        var dtEarnings = _$earningsTable.DataTable({
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _earningsService.getAll,
                inputFilter: function () {
                    return {
                        employeeIdFilter: $('#SearchFilterId').val()
                    };
                }
            },
            columnDefs: [{
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                width: 120,
                targets: 1,
                data: null,
                orderable: false,
                autoWidth: false,
                defaultContent: '',
                rowAction: {
                    cssClass: 'btn btn-brand dropdown-toggle',
                    text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                    items: [{
                        text: app.localize('View'),
                        iconStyle: 'far fa-eye mr-2',
                        action: function (data) {
                            _viewEarningsModal.open({
                                id: data.record.earnings.id
                            });
                        }
                    },
                    {
                        text: app.localize('Edit'),
                        iconStyle: 'far fa-edit mr-2',
                        visible: function () {
                            return _permissionsEarnings.edit;
                        },
                        action: function (data) {
                            _createOrEditModalEarnings.open({
                                id: data.record.earnings.id
                            });
                        }
                    },
                    {
                        text: app.localize('Delete'),
                        iconStyle: 'far fa-trash-alt mr-2',
                        visible: function () {
                            return _permissionsEarnings.delete;
                        },
                        action: function (data) {
                            deleteEarnings(data.record.earnings);
                        }
                    }
                    ]
                }
            },
            {
                targets: 2,
                data: "earnings.earningType",
                name: "earningType"
            },
            {
                targets: 3,
                data: "earnings.amount",
                name: "amount"
            }]
        });

        function deleteEarnings(earnings) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _earningsService.delete({
                            id: earnings.id
                        }).done(function () {
                            dtEarnings.ajax.reload();
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewEarningsButton').click(function () {
            _createOrEditModalEarnings.open({
                employeeId: $('#SearchFilterId').val()
            });
        });

        abp.event.on('app.createOrEditEarningsModalSaved', function () {
            dtEarnings.ajax.reload();
        });

        //RecurEarnings
        var _permissionsRecurEarnings = {
            create: abp.auth.hasPermission('Pages.RecurEarnings.Create'),
            edit: abp.auth.hasPermission('Pages.RecurEarnings.Edit'),
            'delete': abp.auth.hasPermission('Pages.RecurEarnings.Delete')
        };

        var _createOrEditModalRecurEarnings = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RecurEarnings/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RecurEarnings/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRecurEarningsModal'
        });

        var _viewRecurEarningsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RecurEarnings/ViewrecurEarningsModal',
            modalClass: 'ViewRecurEarningsModal'
        });

        var dtRecurEarnings = _$recurEarningsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _recurEarningsService.getAll,
                inputFilter: function () {
                    return {
                        employeeIdFilter: $('#SearchFilterId').val()
                    };
                }
            },
            columnDefs: [{
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                width: 120,
                targets: 1,
                data: null,
                orderable: false,
                autoWidth: false,
                defaultContent: '',
                rowAction: {
                    cssClass: 'btn btn-brand dropdown-toggle',
                    text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                    items: [{
                        text: app.localize('View'),
                        iconStyle: 'far fa-eye mr-2',
                        action: function (data) {
                            _viewRecurEarningsModal.open({
                                id: data.record.recurEarnings.id
                            });
                        }
                    },
                    {
                        text: app.localize('Edit'),
                        iconStyle: 'far fa-edit mr-2',
                        visible: function () {
                            return _permissionsRecurEarnings.edit;
                        },
                        action: function (data) {
                            _createOrEditModalRecurEarnings.open({
                                id: data.record.recurEarnings.id
                            });
                        }
                    },
                    {
                        text: app.localize('Delete'),
                        iconStyle: 'far fa-trash-alt mr-2',
                        visible: function () {
                            return _permissionsRecurEarnings.delete;
                        },
                        action: function (data) {
                            deleteRecurEarnings(data.record.recurEarnings);
                        }
                    }]
                }
            },
            {
                targets: 2,
                data: "recurEarnings.earningType",
                name: "amount"
            },
            {
                targets: 3,
                data: "recurEarnings.amount",
                name: "frequency"
            },
            {
                targets: 4,
                data: "recurEarnings.startDate",
                name: "startDate",
                render: function (startDate) {
                    if (startDate) {
                        return moment(startDate).format('L');
                    }
                    return "";
                }
            },
            {
                targets: 5,
                data: "recurEarnings.endDate",
                name: "endDate",
                render: function (endDate) {
                    if (endDate) {
                        return moment(endDate).format('L');
                    }
                    return "";
                }
            }]
        });

        function deleteRecurEarnings(recurEarnings) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _recurEarningsService.delete({
                            id: recurEarnings.id
                        }).done(function () {
                            dtRecurEarnings.ajax.reload();
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewRecurEarningsButton').click(function () {
            _createOrEditModalRecurEarnings.open({
                employeeId: $('#SearchFilterId').val()
            });
        });

        abp.event.on('app.createOrEditRecurEarningsModalSaved', function () {
            dtRecurEarnings.ajax.reload();
        });

        //Deductions
        var _permissionsDeductions = {
            create: abp.auth.hasPermission('Pages.Deductions.Create'),
            edit: abp.auth.hasPermission('Pages.Deductions.Edit'),
            'delete': abp.auth.hasPermission('Pages.Deductions.Delete')
        };

        var _createOrEditModalDeductions = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Deductions/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Deductions/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditDeductionsModal'
        });

        var _viewDeductionsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Deductions/ViewdeductionsModal',
            modalClass: 'ViewDeductionsModal'
        });

        var dtDeductions = _$deductionsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _deductionsService.getAll,
                inputFilter: function () {
                    return {
                        employeeIdFilter: $('#SearchFilterId').val()
                    };
                }
            },
            columnDefs: [{
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                width: 120,
                targets: 1,
                data: null,
                orderable: false,
                autoWidth: false,
                defaultContent: '',
                rowAction: {
                    cssClass: 'btn btn-brand dropdown-toggle',
                    text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                    items: [{
                        text: app.localize('View'),
                        iconStyle: 'far fa-eye mr-2',
                        action: function (data) {
                            _viewDeductionsModal.open({
                                id: data.record.deductions.id
                            });
                        }
                    },
                    {
                        text: app.localize('Edit'),
                        iconStyle: 'far fa-edit mr-2',
                        visible: function () {
                            return _permissionsDeductions.edit;
                        },
                        action: function (data) {
                            _createOrEditModalDeductions.open({
                                id: data.record.deductions.id
                            });
                        }
                    },
                    {
                        text: app.localize('Delete'),
                        iconStyle: 'far fa-trash-alt mr-2',
                        visible: function () {
                            return _permissionsDeductions.delete;
                        },
                        action: function (data) {
                            deleteDeductions(data.record.deductions);
                        }
                    }
                    ]
                }
            },
            {
                targets: 2,
                data: "deductions.deductionType",
                name: "payrollNo"
            },
            {
                targets: 3,
                data: "deductions.amount",
                name: "amount"
            }]
        });

        function deleteDeductions(deductions) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _deductionsService.delete({
                            id: deductions.id
                        }).done(function () {
                            dtDeductions.ajax.reload();
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewDeductionsButton').click(function () {
            _createOrEditModalDeductions.open({
                employeeId: $('#SearchFilterId').val()
            });
        });

        abp.event.on('app.createOrEditDeductionsModalSaved', function () {
            dtDeductions.ajax.reload();
        });

        //RecurDeductions
        var _permissionsRecurDeductions = {
            create: abp.auth.hasPermission('Pages.RecurDeductions.Create'),
            edit: abp.auth.hasPermission('Pages.RecurDeductions.Edit'),
            'delete': abp.auth.hasPermission('Pages.RecurDeductions.Delete')
        };

        var _createOrEditModalRecurDeductions = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RecurDeductions/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RecurDeductions/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRecurDeductionsModal'
        });

        var _viewRecurDeductionsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RecurDeductions/ViewrecurDeductionsModal',
            modalClass: 'ViewRecurDeductionsModal'
        });

        var dtRecurDeductions = _$recurDeductionsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _recurDeductionsService.getAll,
                inputFilter: function () {
                    return {
                        employeeIdFilter: $('#SearchFilterId').val()
                    };
                }
            },
            columnDefs: [{
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                width: 120,
                targets: 1,
                data: null,
                orderable: false,
                autoWidth: false,
                defaultContent: '',
                rowAction: {
                    cssClass: 'btn btn-brand dropdown-toggle',
                    text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                    items: [{
                        text: app.localize('View'),
                        iconStyle: 'far fa-eye mr-2',
                        action: function (data) {
                            _viewRecurDeductionsModal.open({
                                id: data.record.recurDeductions.id
                            });
                        }
                    },
                    {
                        text: app.localize('Edit'),
                        iconStyle: 'far fa-edit mr-2',
                        visible: function () {
                            return _permissionsRecurDeductions.edit;
                        },
                        action: function (data) {
                            _createOrEditModalRecurDeductions.open({
                                id: data.record.recurDeductions.id
                            });
                        }
                    },
                    {
                        text: app.localize('Delete'),
                        iconStyle: 'far fa-trash-alt mr-2',
                        visible: function () {
                            return _permissionsRecurDeductions.delete;
                        },
                        action: function (data) {
                            deleteRecurDeductions(data.record.recurDeductions);
                        }
                    }
                    ]
                }
            },
            {
                targets: 2,
                data: "recurDeductions.deductionType",
                name: "amount"
            },
            {
                targets: 3,
                data: "recurDeductions.amount",
                name: "frequency"
            },
            {
                targets: 4,
                data: "recurDeductions.startDate",
                name: "startDate",
                render: function (startDate) {
                    if (startDate) {
                        return moment(startDate).format('L');
                    }
                    return "";
                }
            },
            {
                targets: 5,
                data: "recurDeductions.endDate",
                name: "endDate",
                render: function (endDate) {
                    if (endDate) {
                        return moment(endDate).format('L');
                    }
                    return "";
                }
            }]
        });

        function deleteRecurDeductions(recurDeductions) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _recurDeductionsService.delete({
                            id: recurDeductions.id
                        }).done(function () {
                            dtRecurDeductions.ajax.reload();
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewRecurDeductionsButton').click(function () {
            _createOrEditModalRecurDeductions.open({
                employeeId: $('#SearchFilterId').val()
            });
        });

        abp.event.on('app.createOrEditRecurDeductionsModalSaved', function () {
            dtRecurDeductions.ajax.reload();
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $("#SearchFilterName").autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#SearchFilterName').val(ui.item.label);
                        $('#SearchFilterId').val(ui.item.id);

                        var activeTab = $('ul.nav-tabs li a.active').data('id');

                        if ($('#SearchFilterId').val() != '0') {
                            $('#saveBtnTimesheets').prop('disabled', false);
                            $('#saveBtnOTSheet').prop('disabled', false);
                            $('#CreateNewEarningsButton').prop('disabled', false);
                            $('#CreateNewRecurEarningsButton').prop('disabled', false);
                            $('#CreateNewDeductionsButton').prop('disabled', false);
                            $('#CreateNewRecurDeductionsButton').prop('disabled', false);
                        } 

                        if (activeTab == 'timesheet') {

                            SearchTimesheet();
                        }
                        else if (activeTab == 'overtime') {
                            $('#saveBtnOTSheet').prop('disabled', $('#SearchFilterId').val() == '0');

                            SearchOTSheet();
                        }
                        else if (activeTab == 'income') {
                            dtEarnings.ajax.reload();
                            dtRecurEarnings.ajax.reload();
                        }
                        else if (activeTab == 'deduction') {
                            dtDeductions.ajax.reload();
                            dtRecurDeductions.ajax.reload();
                        }

                        return false;
                    },
                    minLength: 3
                });
            });

        function SearchTimesheet() {
            $.ajax({
                url: "/App/PayrollTransactions/PopulateTimesheets",
                type: "GET",
                dataType: "json",
                data: { employeeId: $('#SearchFilterId').val() },
                success: function (items) {
                    $('input[name=days]').each(function (index) {
                        $(this).val(items['result']['days'][index]);
                    });

                    $('input[name=hrs]').each(function (index) {
                        $(this).val(items['result']['hrs'][index]);
                    });
                }
            });
        }

        function SearchOTSheet() {
            $.ajax({
                url: "/App/PayrollTransactions/PopulateOTSheet",
                type: "GET",
                dataType: "json",
                data: { employeeId: $('#SearchFilterId').val() },
                success: function (items) {
                    $('input[name=rate]').each(function (index) {
                        $(this).val(items['result'][index]);
                    });
                }
            });
        }

        $("#btnOvertimeTab").one("click", function (e) {
            e.preventDefault();
            $.getScript("/view-resources/Areas/App/Views/OTSheet/Index.min.js");
        });

        $('#btnIncomeTab').click(function () {
            dtEarnings.ajax.reload();
            dtRecurEarnings.ajax.reload();
        });

        $('#btnDeductionTab').click(function () {
            dtDeductions.ajax.reload();
            dtRecurDeductions.ajax.reload();
        });

        //$('#btnIncomeTab').one("click", function (e) {
        //    e.preventDefault();
        //    $.getScript("/view-resources/Areas/App/Views/Earnings/Index.min.js");
        //    $.getScript("/view-resources/Areas/App/Views/RecurEarnings/Index.min.js");
        //});

        //$("#btnDeductionTab").one("click", function (e) {
        //    e.preventDefault();
        //    $.getScript("/view-resources/Areas/App/Views/Deductions/Index.min.js");
        //    $.getScript("/view-resources/Areas/App/Views/RecurDeductions/Index.min.js");
        //});
    });
})();