(function($) {
    app.modals.CreateOrEditOTSheetTmpModal = function() {
        var _otSheetTmpService = abp.services.app.otSheetTmp;
        var _modalManager;
        var _$otSheetTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$otSheetTmpInformationForm = _modalManager.getModal().find('form[name=OTSheetTmpInformationsForm]');
            _$otSheetTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$otSheetTmpInformationForm.valid()) {
                return;
            }
            var otSheetTmp = _$otSheetTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _otSheetTmpService.createOrEdit(
                otSheetTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOTSheetTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);