(function($) {
    app.modals.CreateOrEditTimesheetPostedModal = function() {
        var _timesheetPostedService = abp.services.app.timesheetPosted;
        var _modalManager;
        var _$timesheetPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$timesheetPostedInformationForm = _modalManager.getModal().find('form[name=TimesheetPostedInformationsForm]');
            _$timesheetPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$timesheetPostedInformationForm.valid()) {
                return;
            }
            var timesheetPosted = _$timesheetPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _timesheetPostedService.createOrEdit(
                timesheetPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTimesheetPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);