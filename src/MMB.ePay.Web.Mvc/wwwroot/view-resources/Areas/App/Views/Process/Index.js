(function() {
    $(function() {
        var _$processTable = $('#ProcessTable');
        var _processService = abp.services.app.process;
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });
        var _permissions = {
            create: abp.auth.hasPermission('Pages.Process.Create'),
            edit: abp.auth.hasPermission('Pages.Process.Edit'),
            'delete': abp.auth.hasPermission('Pages.Process.Delete')
        };
        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Process/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Process/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditProcessModal'
        });
        var _viewProcessModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Process/ViewprocessModal',
            modalClass: 'ViewProcessModal'
        });
        var getDateFilter = function(element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z");
        }
        var getMaxDateFilter = function(element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z");
        }
        var dataTable = _$processTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _processService.getAll,
                inputFilter: function() {
                    return {
                        filter: $('#ProcessTableFilter').val(),
                        minPayrollNoFilter: $('#MinPayrollNoFilterId').val(),
                        maxPayrollNoFilter: $('#MaxPayrollNoFilterId').val(),
                        statusCodeFilter: $('#StatusCodeFilterId').val()
                    };
                }
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewProcessModal.open({
                                        id: data.record.process.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal.open({
                                        id: data.record.process.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function() {
                                    return _permissions.delete;
                                },
                                action: function(data) {
                                    deleteProcess(data.record.process);
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "process.payrollNo",
                    name: "payrollNo"
                },
                {
                    targets: 3,
                    data: "process.statusCode",
                    name: "statusCode"
                }
            ]
        });

        function getProcess() {
            dataTable.ajax.reload();
        }

        function deleteProcess(process) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function(isConfirmed) {
                    if (isConfirmed) {
                        _processService.delete({
                            id: process.id
                        }).done(function() {
                            getProcess(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }
        $('#ShowAdvancedFiltersSpan').click(function() {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });
        $('#HideAdvancedFiltersSpan').click(function() {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });
        $('#CreateNewProcessButton').click(function() {
            _createOrEditModal.open();
        });
        $('#ExportToExcelButton').click(function() {
            _processService
                .getProcessToExcel({
                    filter: $('#ProcessTableFilter').val(),
                    minPayrollNoFilter: $('#MinPayrollNoFilterId').val(),
                    maxPayrollNoFilter: $('#MaxPayrollNoFilterId').val(),
                    statusCodeFilter: $('#StatusCodeFilterId').val()
                })
                .done(function(result) {
                    app.downloadTempFile(result);
                });
        });
        abp.event.on('app.createOrEditProcessModalSaved', function() {
            getProcess();
        });
        $('#GetProcessButton').click(function(e) {
            e.preventDefault();
            getProcess();
        });
        $(document).keypress(function(e) {
            if (e.which === 13) {
                getProcess();
            }
        });
    });
})();