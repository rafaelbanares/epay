(function($) {
    app.modals.CreateOrEditRanksModal = function() {
        var _ranksService = abp.services.app.ranks;
        var _modalManager;
        var _$ranksInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$ranksInformationForm = _modalManager.getModal().find('form[name=RanksInformationsForm]');
            _$ranksInformationForm.validate();
        };
        this.save = function() {
            if (!_$ranksInformationForm.valid()) {
                return;
            }
            var ranks = _$ranksInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _ranksService.createOrEdit(
                ranks
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditRanksModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);