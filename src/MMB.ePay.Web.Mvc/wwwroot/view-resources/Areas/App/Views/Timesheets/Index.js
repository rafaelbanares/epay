(function() {
    $(function () {
        var _timesheetsService = abp.services.app.timesheets;

        function getTimesheets() {
            var employeeId = $('#SearchFilterId').val();

            $.ajax({
                url: "/App/PayrollTransactions/PopulateTimesheets",
                type: "GET",
                dataType: "json",
                data: { employeeId: employeeId },
                success: function (items) {
                    $('input[name=days]').each(function (index) {
                        $(this).val(items['result']['days'][index]);
                    });

                    $('input[name=hrs]').each(function (index) {
                        $(this).val(items['result']['hrs'][index]);
                    });
                }
            })

        }

        abp.event.on('app.createOrEditTimesheetsModalSaved', function() {
            getTimesheets();
        });

        $('#GetTimesheetsButton').click(function (e) {
            e.preventDefault();
            getTimesheets();
        });

        $('#btnTimesheetTab').click(function (e) {
            e.preventDefault();
            getTimesheets();
        });

        //$('#EmployeeIdFilterId').change(function () {
        //    var activeTab = $('ul.nav-tabs li a.active').data('id');
        //    if (activeTab == 'timesheet') {
        //        getTimesheets();
        //    }
        //});

        var _$timesheetsForm = $('form[name=TimesheetsInformationsForm]');
        _$timesheetsForm.validate();

        function saveTimesheets(successCallback) {
            if (!_$timesheetsForm.valid()) {
                return;
            }

            var days = [];
            $('input[name=days]').each(function () {
                days.push($(this).val());
            });

            var hrs = [];
            $('input[name=hrs]').each(function () {
                hrs.push($(this).val());
            });

            var timesheets = {};
            timesheets["employeeId"] = $('#SearchFilterId').val();
            timesheets["days"] = days;
            timesheets["hrs"] = hrs;

            abp.ui.setBusy();

            _timesheetsService.createOrEdit(
                timesheets
            ).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                abp.event.trigger('app.createOrEditTimesheetsModalSaved');
                if (typeof (successCallback) === 'function') {
                    successCallback();
                }
            }).always(function () {
                abp.ui.clearBusy();
            });
        }

        $('#saveBtnTimesheets').click(function () {
            saveTimesheets(function () {
                getTimesheets();
            });
        });
    });
})();