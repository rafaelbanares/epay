(function($) {
    app.modals.CreateOrEditDeductionTmpModal = function() {
        var _deductionTmpService = abp.services.app.deductionTmp;
        var _modalManager;
        var _$deductionTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$deductionTmpInformationForm = _modalManager.getModal().find('form[name=DeductionTmpInformationsForm]');
            _$deductionTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$deductionTmpInformationForm.valid()) {
                return;
            }
            var deductionTmp = _$deductionTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _deductionTmpService.createOrEdit(
                deductionTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditDeductionTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);