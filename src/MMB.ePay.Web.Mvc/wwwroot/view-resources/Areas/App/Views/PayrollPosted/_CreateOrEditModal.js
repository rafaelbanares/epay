(function($) {
    app.modals.CreateOrEditPayrollPostedModal = function() {
        var _payrollPostedService = abp.services.app.payrollPosted;
        var _modalManager;
        var _$payrollPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$payrollPostedInformationForm = _modalManager.getModal().find('form[name=PayrollPostedInformationsForm]');
            _$payrollPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$payrollPostedInformationForm.valid()) {
                return;
            }
            var payrollPosted = _$payrollPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _payrollPostedService.createOrEdit(
                payrollPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditPayrollPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);