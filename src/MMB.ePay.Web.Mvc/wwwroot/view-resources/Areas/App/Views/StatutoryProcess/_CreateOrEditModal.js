(function($) {
    app.modals.CreateOrEditStatutoryProcessModal = function() {
        var _statutoryProcessService = abp.services.app.statutoryProcess;
        var _modalManager;
        var _$statutoryProcessInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$statutoryProcessInformationForm = _modalManager.getModal().find('form[name=StatutoryProcessInformationsForm]');
            _$statutoryProcessInformationForm.validate();
        };
        this.save = function() {
            if (!_$statutoryProcessInformationForm.valid()) {
                return;
            }
            var statutoryProcess = _$statutoryProcessInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _statutoryProcessService.createOrEdit(
                statutoryProcess
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditStatutoryProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);