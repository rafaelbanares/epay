﻿(function () {
    $(function () {
        var _companiesService = abp.services.app.companies;
        var _tenantBanksService = abp.services.app.tenantBanks;
        var _contactPersonsService = abp.services.app.contactPersons;
        var _companyStatutoryInfoService = abp.services.app.companyStatutoryInfo;
        var _$generalForm = $('form[name=GeneralForm]');

        var _$bankInfoTable = $('#BankInfoTable');
        var _$contactPersonTable = $('#ContactPersonTable');
        var _$statutoryInfoTable = $('#StatutoryInfoTable');

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        function save(successCallback) {
            if (!_$generalForm.valid()) {
                return;
            }

            var general = _$generalForm.serializeFormToObject();

            abp.ui.setBusy();
            _companiesService.createOrEdit(
                general
            ).done(function () {

                if (typeof (successCallback) === 'function') {
                    successCallback();
                }

            }).always(function () {
                abp.ui.clearBusy();
            });
        }

        $('#btnGeneralSave').click(function () {
            save(function () {
                abp.notify.info(app.localize('Updated Successfully'));
            });
        });

        var _bankInfoPermissions = {
            create: abp.auth.hasPermission('Pages.TenantBanks.Create'),
            edit: abp.auth.hasPermission('Pages.TenantBanks.Edit')
        };
        var _createOrEditTenantBanksModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TenantBanks/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/TenantBanks/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditTenantBanksModal'
        });
        var _viewTenantBanksModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TenantBanks/ViewtenantBanksModal',
            modalClass: 'ViewTenantBanksModal'
        });

        var tenantBanksTable = _$bankInfoTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _tenantBanksService.getAll,
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                            text: app.localize('View'),
                            iconStyle: 'far fa-eye mr-2',
                            action: function (data) {
                                _viewTenantBanksModal.open({
                                    id: data.record.tenantBanks.id
                                });
                            }
                        },
                        {
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _bankInfoPermissions.edit;
                            },
                            action: function (data) {
                                _createOrEditTenantBanksModal.open({
                                    id: data.record.tenantBanks.id
                                });
                            }
                        }]
                    }
                },
                {
                    targets: 1,
                    data: "tenantBanks.branchName",
                    name: "branchName"
                },
                {
                    targets: 2,
                    data: "tenantBanks.bankAccountNumber",
                    name: "bankAccountNumber"
                },
                {
                    targets: 3,
                    data: "tenantBanks.bankAccountType",
                    name: "bankAccountType"
                },
                {
                    targets: 4,
                    data: "tenantBanks.branchNumber",
                    name: "branchNumber"
                },
                {
                    targets: 5,
                    data: "tenantBanks.corporateId",
                    name: "corporateId"
                }
            ]
        });

        function getTenantBanks() {
            tenantBanksTable.ajax.reload();
        }

        abp.event.on('app.createOrEditTenantBanksModalSaved', function () {
            getTenantBanks();
        });

        $('#CreateNewBankInfoButton').click(function () {
            _createOrEditTenantBanksModal.open();
        });

        var _contactPersonPermissions = {
            create: abp.auth.hasPermission('Pages.ContactPersons.Create'),
            edit: abp.auth.hasPermission('Pages.ContactPersons.Edit')
        };
        var _createOrEditContactPersonsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ContactPersons/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ContactPersons/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditContactPersonsModal'
        });
        var _viewContactPersonsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ContactPersons/ViewcontactPersonsModal',
            modalClass: 'ViewContactPersonsModal'
        });

        var contactPersonsTable = _$contactPersonTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _contactPersonsService.getAll,
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                            text: app.localize('View'),
                            iconStyle: 'far fa-eye mr-2',
                            action: function (data) {
                                _viewContactPersonsModal.open({
                                    id: data.record.contactPersons.id
                                });
                            }
                        },
                        {
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _contactPersonPermissions.edit;
                            },
                            action: function (data) {
                                _createOrEditContactPersonsModal.open({
                                    id: data.record.contactPersons.id
                                });
                            }
                        }]
                    }
                },
                {
                    targets: 1,
                    data: "contactPersons.fullName",
                    name: "fullName"
                },
                {
                    targets: 2,
                    data: "contactPersons.contactNo",
                    name: "contactNo"
                },
                {
                    targets: 3,
                    data: "contactPersons.position",
                    name: "position"
                }
            ]
        });

        function getContactPersons() {
            contactPersonsTable.ajax.reload();
        }

        abp.event.on('app.createOrEditContactPersonsModalSaved', function () {
            getContactPersons();
        });

        $('#CreateNewContactPersonsButton').click(function () {
            _createOrEditContactPersonsModal.open();
        });

        var _companyStatutoryInfoPermissions = {
            create: abp.auth.hasPermission('Pages.CompanyStatutoryInfo.Create'),
            edit: abp.auth.hasPermission('Pages.CompanyStatutoryInfo.Edit')
        };
        var _createOrEditCompanyStatutoryInfoModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/CompanyStatutoryInfo/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/CompanyStatutoryInfo/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditCompanyStatutoryInfoModal'
        });
        var _viewCompanyStatutoryInfoModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/CompanyStatutoryInfo/ViewcompanyStatutoryInfoModal',
            modalClass: 'ViewCompanyStatutoryInfoModal'
        });

        var statutoryInfoTable = _$statutoryInfoTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _companyStatutoryInfoService.getAll,
            },
            columnDefs: [
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                            text: app.localize('View'),
                            iconStyle: 'far fa-eye mr-2',
                            action: function (data) {
                                _viewCompanyStatutoryInfoModal.open({
                                    id: data.record.companyStatutoryInfo.id
                                });
                            }
                        },
                        {
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _companyStatutoryInfoPermissions.edit;
                            },
                            action: function (data) {
                                _createOrEditCompanyStatutoryInfoModal.open({
                                    id: data.record.companyStatutoryInfo.id
                                });
                            }
                        }]
                    }
                },
                {
                    targets: 1,
                    data: "companyStatutoryInfo.statutoryType",
                    name: "statutoryType",
                    orderable: false
                },
                {
                    targets: 2,
                    data: "companyStatutoryInfo.statutoryAcctNo",
                    name: "statutoryAcctNo",
                    orderable: false
                },
                {
                    targets: 3,
                    data: "companyStatutoryInfo.signatoryName",
                    name: "signatories.signatoryName",
                    orderable: false
                },
                {
                    targets: 4,
                    data: "companyStatutoryInfo.branch",
                    name: "branch",
                    orderable: false
                }
            ]
        });

        function getStatutoryInfo() {
            statutoryInfoTable.ajax.reload();
        }

        abp.event.on('app.createOrEditCompanyStatutoryInfoModalSaved', function () {
            getStatutoryInfo();
        });
    });
})();
