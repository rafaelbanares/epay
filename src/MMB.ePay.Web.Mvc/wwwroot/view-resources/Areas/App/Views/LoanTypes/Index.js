﻿(function () {
    $(function () {

        var _$loanTypesTable = $('#LoanTypesTable');
        var _loanTypesService = abp.services.app.loanTypes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.LoanTypes.Create'),
            edit: abp.auth.hasPermission('Pages.LoanTypes.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/LoanTypes/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LoanTypes/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditLoanTypesModal'
                });
                   

		 var _viewLoanTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LoanTypes/ViewloanTypesModal',
            modalClass: 'ViewLoanTypesModal'
        });

        var dataTable = _$loanTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _loanTypesService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLoanTypesModal.open({ id: data.record.loanTypes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.loanTypes.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "loanTypes.sysCode",
						 name: "sysCode"   
					},
					{
						targets: 3,
						 data: "loanTypes.description",
						 name: "description"   
					}
            ]
        });

        function getLoanTypes() {
            dataTable.ajax.reload();
        }

        $('#CreateNewLoanTypesButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditLoanTypesModalSaved', function () {
            getLoanTypes();
        });

		$('#GetLoanTypesButton').click(function (e) {
            e.preventDefault();
            getLoanTypes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getLoanTypes();
		  }
		});
		
		
		
    });
})();
