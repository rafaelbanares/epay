(function($) {
    app.modals.CreateOrEditLoanTypesModal = function() {
        var _loanTypesService = abp.services.app.loanTypes;
        var _modalManager;
        var _$loanTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$loanTypesInformationForm = _modalManager.getModal().find('form[name=LoanTypesInformationsForm]');
            _$loanTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$loanTypesInformationForm.valid()) {
                return;
            }
            var loanTypes = _$loanTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _loanTypesService.createOrEdit(
                loanTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLoanTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);