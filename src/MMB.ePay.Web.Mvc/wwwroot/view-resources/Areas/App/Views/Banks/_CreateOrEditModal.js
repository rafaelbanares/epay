(function($) {
    app.modals.CreateOrEditBanksModal = function() {
        var _banksService = abp.services.app.banks;
        var _modalManager;
        var _$banksInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$banksInformationForm = _modalManager.getModal().find('form[name=BanksInformationsForm]');
            _$banksInformationForm.validate();
        };
        this.save = function() {
            if (!_$banksInformationForm.valid()) {
                return;
            }
            var banks = _$banksInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _banksService.createOrEdit(
                banks
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditBanksModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);