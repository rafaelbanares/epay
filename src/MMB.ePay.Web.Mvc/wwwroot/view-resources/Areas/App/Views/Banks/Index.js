﻿(function () {
    $(function () {

        var _$banksTable = $('#BanksTable');
        var _banksService = abp.services.app.banks;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Banks.Create'),
            edit: abp.auth.hasPermission('Pages.Banks.Edit'),
            bankBranches: abp.auth.hasPermission('Pages.BankBranches')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/Banks/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Banks/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditBanksModal'
         });

		 var _viewBanksModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Banks/ViewbanksModal',
            modalClass: 'ViewBanksModal'
        });

        var dataTable = _$banksTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _banksService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewBanksModal.open({ id: data.record.banks.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                                _createOrEditModal.open({ id: data.record.banks.id });                                
                            }
                        },
                        {
                            text: app.localize('BankBranches'),
                            iconStyle: 'fas fa-piggy-bank mr-2',
                            visible: function () {
                                return _permissions.bankBranches;
                            },
                            action: function (data) {
                                window.location.href = 'BankBranches/Index/' + data.record.banks.id;
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "banks.id",
						 name: "id"   
					},
					{
						targets: 3,
						 data: "banks.displayName",
                         name: "displayName"
					},
					{
						targets: 4,
						 data: "banks.description",
                         name: "description"
					}
            ]
        });

        function getBanks() {
            dataTable.ajax.reload();
        }

        $('#CreateNewBanksButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditBanksModalSaved', function () {
            getBanks();
        });

		$('#GetBanksButton').click(function (e) {
            e.preventDefault();
            getBanks();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getBanks();
		  }
		});
		
		
		
    });
})();
