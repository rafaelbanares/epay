(function($) {
    app.modals.CreateOrEditEmployersModal = function() {
        var _employersService = abp.services.app.employers;
        var _modalManager;
        var _$employersInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$employersInformationForm = _modalManager.getModal().find('form[name=EmployersInformationsForm]');
            _$employersInformationForm.validate();
        };
        this.save = function() {
            if (!_$employersInformationForm.valid()) {
                return;
            }
            var employers = _$employersInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _employersService.createOrEdit(
                employers
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEmployersModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);