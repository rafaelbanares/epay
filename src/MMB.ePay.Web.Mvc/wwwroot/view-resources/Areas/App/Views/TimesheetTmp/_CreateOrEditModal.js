(function($) {
    app.modals.CreateOrEditTimesheetTmpModal = function() {
        var _timesheetTmpService = abp.services.app.timesheetTmp;
        var _modalManager;
        var _$timesheetTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$timesheetTmpInformationForm = _modalManager.getModal().find('form[name=TimesheetTmpInformationsForm]');
            _$timesheetTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$timesheetTmpInformationForm.valid()) {
                return;
            }
            var timesheetTmp = _$timesheetTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _timesheetTmpService.createOrEdit(
                timesheetTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTimesheetTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);