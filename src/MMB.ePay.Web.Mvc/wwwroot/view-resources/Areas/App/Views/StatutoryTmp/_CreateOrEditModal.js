(function($) {
    app.modals.CreateOrEditStatutoryTmpModal = function() {
        var _statutoryTmpService = abp.services.app.statutoryTmp;
        var _modalManager;
        var _$statutoryTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$statutoryTmpInformationForm = _modalManager.getModal().find('form[name=StatutoryTmpInformationsForm]');
            _$statutoryTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$statutoryTmpInformationForm.valid()) {
                return;
            }
            var statutoryTmp = _$statutoryTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _statutoryTmpService.createOrEdit(
                statutoryTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditStatutoryTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);