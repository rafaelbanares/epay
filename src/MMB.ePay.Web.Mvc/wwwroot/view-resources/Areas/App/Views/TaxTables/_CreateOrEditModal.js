(function($) {
    app.modals.CreateOrEditTaxTablesModal = function() {
        var _taxTablesService = abp.services.app.taxTables;
        var _modalManager;
        var _$taxTablesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$taxTablesInformationForm = _modalManager.getModal().find('form[name=TaxTablesInformationsForm]');
            _$taxTablesInformationForm.validate();
        };
        this.save = function() {
            if (!_$taxTablesInformationForm.valid()) {
                return;
            }
            var taxTables = _$taxTablesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _taxTablesService.createOrEdit(
                taxTables
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTaxTablesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);