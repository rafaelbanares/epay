(function() {
    $(function () {

        var _$taxTablesTable = $('#TaxTablesTable');
        var _taxTablesService = abp.services.app.taxTables;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.TaxTables.Create'),
            edit: abp.auth.hasPermission('Pages.TaxTables.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TaxTables/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/TaxTables/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditTaxTablesModal'
        });

        var _viewTaxTablesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TaxTables/ViewtaxTablesModal',
            modalClass: 'ViewTaxTablesModal'
        });
        //var getDateFilter = function(element) {
        //    if (element.data("DateTimePicker").date() == null) {
        //        return null;
        //    }
        //    return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z");
        //}
        //var getMaxDateFilter = function(element) {
        //    if (element.data("DateTimePicker").date() == null) {
        //        return null;
        //    }
        //    return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z");
        //}
        var dataTable = _$taxTablesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _taxTablesService.getAll,
                inputFilter: function() {
                    return {
                        yearFilter: $('#YearFilterId').val(),
                        payFreqFilter: $('#PayFreqFilterId').val(),
                        codeFilter: $('#CodeFilterId').val()
                    };
                }
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewTaxTablesModal.open({
                                        id: data.record.taxTables.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal.open({
                                        id: data.record.taxTables.id
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "taxTables.code",
                    name: "code"
                },
                {
                    targets: 3,
                    data: "taxTables.bracket1",
                    name: "bracket1"
                },
                {
                    targets: 4,
                    data: "taxTables.bracket2",
                    name: "bracket2"
                },
                {
                    targets: 5,
                    data: "taxTables.bracket3",
                    name: "bracket3"
                },
                {
                    targets: 6,
                    data: "taxTables.bracket4",
                    name: "bracket4"
                },
                {
                    targets: 7,
                    data: "taxTables.bracket5",
                    name: "bracket5"
                },
                {
                    targets: 8,
                    data: "taxTables.bracket6",
                    name: "bracket6"
                },
                {
                    targets: 9,
                    data: "taxTables.bracket7",
                    name: "bracket7"
                }
            ]
        });

        function getTaxTables() {
            dataTable.ajax.reload();
        }

        $('#CreateNewTaxTablesButton').click(function() {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditTaxTablesModalSaved', function() {
            getTaxTables();
        });

        $('#GetTaxTablesButton').click(function(e) {
            e.preventDefault();
            getTaxTables();
        });

        $('#YearFilterId, #PayFreqFilterId').change(function () {
            getTaxTables();
        });
    });
})();