(function($) {
    app.modals.CreateOrEditTenantBanksModal = function () {
        $('#TenantBanks_BankId').change(function () {
            var bankId = $(this).val()
            $('#divBankBranch').find('option').remove();

            $.ajax({
                url: "/App/TenantBanks/PopulateBankBranches",
                type: "GET",
                dataType: "json",
                data: { bankId: bankId },
                success: function (items) {
                    $.each(items['result'], function (i, item) {
                        $('#TenantBanks_BankBranchId').append($('<option>', {
                            value: item.value,
                            text: item.text
                        }));
                    });
                }
            });

            if (bankId == '') {
                $('#divBankBranch').hide();
            } else {
                $('#divBankBranch').show();
            }
        });

        var _tenantBanksService = abp.services.app.tenantBanks;
        var _modalManager;
        var _$tenantBanksInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$tenantBanksInformationForm = _modalManager.getModal().find('form[name=TenantBanksInformationsForm]');
            _$tenantBanksInformationForm.validate();
        };
        this.save = function() {
            if (!_$tenantBanksInformationForm.valid()) {
                return;
            }
            var tenantBanks = _$tenantBanksInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _tenantBanksService.createOrEdit(
                tenantBanks
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTenantBanksModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);