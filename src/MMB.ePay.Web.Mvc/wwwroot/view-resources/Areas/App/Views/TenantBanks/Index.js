﻿(function () {
    $(function () {

        var _$tenantBanksTable = $('#TenantBanksTable');
        var _tenantBanksService = abp.services.app.tenantBanks;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.TenantBanks.Create'),
            edit: abp.auth.hasPermission('Pages.TenantBanks.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/TenantBanks/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/TenantBanks/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditTenantBanksModal'
                });
                   

		 var _viewTenantBanksModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TenantBanks/ViewtenantBanksModal',
            modalClass: 'ViewTenantBanksModal'
        });

        var dataTable = _$tenantBanksTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _tenantBanksService.getAll,
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewTenantBanksModal.open({ id: data.record.tenantBanks.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.tenantBanks.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						data: "tenantBanks.branchName",
                        name: "branchName"   
					},
                    {
						targets: 3,
						 data: "tenantBanks.bankAccountNumber",
						 name: "bankAccountNumber"   
					},
					{
						targets: 4,
						 data: "tenantBanks.bankAccountType",
						 name: "bankAccountType"   
					},
					{
						targets: 5,
						 data: "tenantBanks.branchNumber",
						 name: "branchNumber"   
					},
					{
						targets: 6,
						 data: "tenantBanks.corporateId",
						 name: "corporateId"   
					}
            ]
        });

        function getTenantBanks() {
            dataTable.ajax.reload();
        }

        $('#CreateNewTenantBanksButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditTenantBanksModalSaved', function () {
            getTenantBanks();
        });

		$('#GetTenantBanksButton').click(function (e) {
            e.preventDefault();
            getTenantBanks();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getTenantBanks();
		  }
		});
    });
})();
