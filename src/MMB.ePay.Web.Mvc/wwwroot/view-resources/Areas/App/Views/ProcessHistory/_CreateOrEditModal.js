(function($) {
    app.modals.CreateOrEditProcessHistoryModal = function() {
        var _processHistoryService = abp.services.app.processHistory;
        var _modalManager;
        var _$processHistoryInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$processHistoryInformationForm = _modalManager.getModal().find('form[name=ProcessHistoryInformationsForm]');
            _$processHistoryInformationForm.validate();
        };
        this.save = function() {
            if (!_$processHistoryInformationForm.valid()) {
                return;
            }
            var processHistory = _$processHistoryInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _processHistoryService.createOrEdit(
                processHistory
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditProcessHistoryModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);