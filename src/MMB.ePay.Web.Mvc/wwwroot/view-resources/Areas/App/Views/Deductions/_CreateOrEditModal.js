(function($) {
    app.modals.CreateOrEditDeductionsModal = function() {
        var _deductionsService = abp.services.app.deductions;
        var _modalManager;
        var _$deductionsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$deductionsInformationForm = _modalManager.getModal().find('form[name=DeductionsInformationsForm]');
            _$deductionsInformationForm.validate();
        };
        this.save = function() {
            if (!_$deductionsInformationForm.valid()) {
                return;
            }
            var deductions = _$deductionsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _deductionsService.createOrEdit(
                deductions
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditDeductionsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);