(function($) {
    app.modals.CreateOrEditPayrollTmpModal = function() {
        var _payrollTmpService = abp.services.app.payrollTmp;
        var _modalManager;
        var _$payrollTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$payrollTmpInformationForm = _modalManager.getModal().find('form[name=PayrollTmpInformationsForm]');
            _$payrollTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$payrollTmpInformationForm.valid()) {
                return;
            }
            var payrollTmp = _$payrollTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _payrollTmpService.createOrEdit(
                payrollTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditPayrollTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);