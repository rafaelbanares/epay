(function() {
    $(function() {
        var _reportsService = abp.services.app.reports;
        var _payPeriodService = abp.services.app.payPeriod;
        var _employeesService = abp.services.app.employees;

        $('#Reports_Year, #Reports_Month').change(function () {
            var year = $('#Reports_Year').val();
            var month = $('#Reports_Month').val();

            _payPeriodService.getPayPeriodListByYear(year, month)
                .done(function (data) {
                    $('#Reports_PayPeriodId').empty();

                    $(data).each(function (index) {
                        $('#Reports_PayPeriodId').append(
                            '<option value="' + data[index].value + '">' + data[index].text + '</option>'
                        );
                    });
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {

                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $("#Reports_EmployeeName").autocomplete({
                    source: employees,
                    focus: function (event, ui) {
                        $("#Reports_EmployeeName").val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        $("#Reports_EmployeeName").val(ui.item.label);
                        $('#Reports_EmployeeId').val(ui.item.id);
                        return false;
                    },
                    minLength: 3,
                    messages: {
                        noResults: 'No results found.'
                    }
                });
            });

        //$('#btnGenerate').click(function () {
        //    var reportType = $('#Reports_ReportTypeId').val();
        //    var employeeId = $('#Reports_EmployeeId').val();
        //    var payrollNo = $('#Reports_PayrollNo').val();

        //    var url = "/App/Reports/" + reportType
        //        + '?employeeId=' + employeeId
        //        + '&payrollNo=' + payrollNo;

        //    window.open(url, '_blank');
        //}); 

    });
})();