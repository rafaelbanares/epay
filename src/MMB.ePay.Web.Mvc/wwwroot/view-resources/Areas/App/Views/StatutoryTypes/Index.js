﻿(function () {
    $(function () {

        var _$statutoryTypesTable = $('#StatutoryTypesTable');
        var _statutoryTypesService = abp.services.app.statutoryTypes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.StatutoryTypes.Create'),
            edit: abp.auth.hasPermission('Pages.StatutoryTypes.Edit'),
            'delete': abp.auth.hasPermission('Pages.StatutoryTypes.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/StatutoryTypes/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/StatutoryTypes/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditStatutoryTypesModal'
                });
                   

		 var _viewStatutoryTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/StatutoryTypes/ViewstatutoryTypesModal',
            modalClass: 'ViewStatutoryTypesModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$statutoryTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _statutoryTypesService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#StatutoryTypesTableFilter').val(),
					displayNameFilter: $('#DisplayNameFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewStatutoryTypesModal.open({ id: data.record.statutoryTypes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.statutoryTypes.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteStatutoryTypes(data.record.statutoryTypes);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "statutoryTypes.displayName",
						 name: "displayName"   
					}
            ]
        });

        function getStatutoryTypes() {
            dataTable.ajax.reload();
        }

        function deleteStatutoryTypes(statutoryTypes) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _statutoryTypesService.delete({
                            id: statutoryTypes.id
                        }).done(function () {
                            getStatutoryTypes(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewStatutoryTypesButton').click(function () {
            _createOrEditModal.open();
        });        

		$('#ExportToExcelButton').click(function () {
            _statutoryTypesService
                .getStatutoryTypesToExcel({
				filter : $('#StatutoryTypesTableFilter').val(),
					displayNameFilter: $('#DisplayNameFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditStatutoryTypesModalSaved', function () {
            getStatutoryTypes();
        });

		$('#GetStatutoryTypesButton').click(function (e) {
            e.preventDefault();
            getStatutoryTypes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getStatutoryTypes();
		  }
		});
		
		
		
    });
})();
