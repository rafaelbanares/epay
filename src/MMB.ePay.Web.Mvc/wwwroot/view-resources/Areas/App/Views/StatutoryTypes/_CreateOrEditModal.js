(function($) {
    app.modals.CreateOrEditStatutoryTypesModal = function() {
        var _statutoryTypesService = abp.services.app.statutoryTypes;
        var _modalManager;
        var _$statutoryTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$statutoryTypesInformationForm = _modalManager.getModal().find('form[name=StatutoryTypesInformationsForm]');
            _$statutoryTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$statutoryTypesInformationForm.valid()) {
                return;
            }
            var statutoryTypes = _$statutoryTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _statutoryTypesService.createOrEdit(
                statutoryTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditStatutoryTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);