(function($) {
    app.modals.CreateOrEditRecurEarningsModal = function() {
        var _recurEarningsService = abp.services.app.recurEarnings;
        var _modalManager;
        var _$recurEarningsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$recurEarningsInformationForm = _modalManager.getModal().find('form[name=RecurEarningsInformationsForm]');
            _$recurEarningsInformationForm.validate();
        };
        this.save = function() {
            if (!_$recurEarningsInformationForm.valid()) {
                return;
            }
            var recurEarnings = _$recurEarningsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _recurEarningsService.createOrEdit(
                recurEarnings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditRecurEarningsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);