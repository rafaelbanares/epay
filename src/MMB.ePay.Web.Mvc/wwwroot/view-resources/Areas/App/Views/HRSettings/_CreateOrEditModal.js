(function($) {
    app.modals.CreateOrEditHRSettingsModal = function() {
        var _hrSettingsService = abp.services.app.hrSettings;
        var _modalManager;
        var _$hrSettingsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$hrSettingsInformationForm = _modalManager.getModal().find('form[name=HRSettingsInformationsForm]');
            _$hrSettingsInformationForm.validate();
        };
        this.save = function() {
            if (!_$hrSettingsInformationForm.valid()) {
                return;
            }
            var hrSettings = _$hrSettingsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _hrSettingsService.createOrEdit(
                hrSettings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditHRSettingsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);