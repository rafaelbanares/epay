﻿(function () {
    $(function () {

        var _$payCodesTable = $('#PayCodesTable');
        var _payCodesService = abp.services.app.payCodes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.PayCodes.Create'),
            edit: abp.auth.hasPermission('Pages.PayCodes.Edit')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/PayCodes/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/PayCodes/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditPayCodesModal'
                });
                   
		 var _viewPayCodesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/PayCodes/ViewpayCodesModal',
            modalClass: 'ViewPayCodesModal'
        });

        var dataTable = _$payCodesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _payCodesService.getAll,
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewPayCodesModal.open({ id: data.record.payCodes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.payCodes.id });                                
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "payCodes.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "payCodes.description",
						 name: "description"   
					},
					{
						targets: 4,
						 data: "payCodes.payFreq",
						 name: "payFreq"   
					},
					{
						targets: 5,
						 data: "payCodes.taxFreq",
						 name: "taxFreq"   
					},
					{
						targets: 6,
						 data: "payCodes.taxMethod",
						 name: "taxMethod"   
					},
					{
						targets: 7,
						 data: "payCodes.nd1Rate",
						 name: "nd1Rate"   
					},
					{
						targets: 8,
						 data: "payCodes.nd2Rate",
						 name: "nd2Rate"   
					}
            ]
        });

        function getPayCodes() {
            dataTable.ajax.reload();
        }

        $('#CreateNewPayCodesButton').click(function () {
            _createOrEditModal.open();
        });        

        abp.event.on('app.createOrEditPayCodesModalSaved', function () {
            getPayCodes();
        });

		$('#GetPayCodesButton').click(function (e) {
            e.preventDefault();
            getPayCodes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getPayCodes();
		  }
		});
		
		
		
    });
})();
