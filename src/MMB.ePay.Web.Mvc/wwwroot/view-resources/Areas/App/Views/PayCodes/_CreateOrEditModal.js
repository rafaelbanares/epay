(function($) {
    app.modals.CreateOrEditPayCodesModal = function() {
        var _payCodesService = abp.services.app.payCodes;
        var _modalManager;
        var _$payCodesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$payCodesInformationForm = _modalManager.getModal().find('form[name=PayCodesInformationsForm]');
            _$payCodesInformationForm.validate();
        };
        this.save = function() {
            if (!_$payCodesInformationForm.valid()) {
                return;
            }
            var payCodes = _$payCodesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _payCodesService.createOrEdit(
                payCodes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditPayCodesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);