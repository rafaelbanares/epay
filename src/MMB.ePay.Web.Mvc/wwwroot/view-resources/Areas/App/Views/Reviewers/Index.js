(function() {
    $(function() {
        var _$reviewersTable = $('#ReviewersTable');
        var _reviewersService = abp.services.app.reviewers;
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });
        var _permissions = {
            create: abp.auth.hasPermission('Pages.Reviewers.Create'),
            edit: abp.auth.hasPermission('Pages.Reviewers.Edit')
        };
        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Reviewers/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Reviewers/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditReviewersModal'
        });
        var _viewReviewersModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Reviewers/ViewreviewersModal',
            modalClass: 'ViewReviewersModal'
        });

        var dataTable = _$reviewersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _reviewersService.getAll
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewReviewersModal.open({
                                        id: data.record.reviewers.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal.open({
                                        id: data.record.reviewers.id
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "reviewers.reviewerType",
                    name: "reviewerType"
                },
                {
                    targets: 3,
                    data: "reviewers.reviewedBy",
                    name: "reviewedBy"
                }
            ]
        });

        function getReviewers() {
            dataTable.ajax.reload();
        }

        function deleteReviewers(reviewers) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function(isConfirmed) {
                    if (isConfirmed) {
                        _reviewersService.delete({
                            id: reviewers.id
                        }).done(function() {
                            getReviewers(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewReviewersButton').click(function() {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditReviewersModalSaved', function() {
            getReviewers();
        });
        $('#GetReviewersButton').click(function(e) {
            e.preventDefault();
            getReviewers();
        });
        $(document).keypress(function(e) {
            if (e.which === 13) {
                getReviewers();
            }
        });
    });
})();