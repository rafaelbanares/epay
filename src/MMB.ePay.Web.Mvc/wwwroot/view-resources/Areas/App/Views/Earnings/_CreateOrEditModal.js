(function($) {
    app.modals.CreateOrEditEarningsModal = function() {
        var _earningsService = abp.services.app.earnings;
        var _modalManager;
        var _$earningsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$earningsInformationForm = _modalManager.getModal().find('form[name=EarningsInformationsForm]');
            _$earningsInformationForm.validate();
        };
        this.save = function() {
            if (!_$earningsInformationForm.valid()) {
                return;
            }
            var earnings = _$earningsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _earningsService.createOrEdit(
                earnings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEarningsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);