(function($) {
    app.modals.CreateOrEditLeaveCreditsModal = function() {
        var _leaveCreditsService = abp.services.app.leaveCredits;
        var _modalManager;
        var _$leaveCreditsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$leaveCreditsInformationForm = _modalManager.getModal().find('form[name=LeaveCreditsInformationsForm]');
            _$leaveCreditsInformationForm.validate();
        };
        this.save = function() {
            if (!_$leaveCreditsInformationForm.valid()) {
                return;
            }
            var leaveCredits = _$leaveCreditsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _leaveCreditsService.createOrEdit(
                leaveCredits
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveCreditsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);