(function($) {
    app.modals.CreateOrEditLoanPostedModal = function() {
        var _loanPostedService = abp.services.app.loanPosted;
        var _modalManager;
        var _$loanPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$loanPostedInformationForm = _modalManager.getModal().find('form[name=LoanPostedInformationsForm]');
            _$loanPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$loanPostedInformationForm.valid()) {
                return;
            }
            var loanPosted = _$loanPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _loanPostedService.createOrEdit(
                loanPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLoanPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);