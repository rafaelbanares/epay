﻿(function () {
    $(function () {

        var _$loanPostedTable = $('#LoanPostedTable');
        var _loanPostedService = abp.services.app.loanPosted;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.LoanPosted.Create'),
            edit: abp.auth.hasPermission('Pages.LoanPosted.Edit'),
            'delete': abp.auth.hasPermission('Pages.LoanPosted.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/LoanPosted/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LoanPosted/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditLoanPostedModal'
                });
                   

		 var _viewLoanPostedModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LoanPosted/ViewloanPostedModal',
            modalClass: 'ViewLoanPostedModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$loanPostedTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _loanPostedService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#LoanPostedTableFilter').val(),
					minPayrollNoFilter: $('#MinPayrollNoFilterId').val(),
					maxPayrollNoFilter: $('#MaxPayrollNoFilterId').val(),
					minPayDateFilter:  getDateFilter($('#MinPayDateFilterId')),
					maxPayDateFilter:  getMaxDateFilter($('#MaxPayDateFilterId')),
					minAmountFilter: $('#MinAmountFilterId').val(),
					maxAmountFilter: $('#MaxAmountFilterId').val(),
					remarksFilter: $('#RemarksFilterId').val(),
					minCreatedByFilter: $('#MinCreatedByFilterId').val(),
					maxCreatedByFilter: $('#MaxCreatedByFilterId').val(),
					minCreatedDateFilter:  getDateFilter($('#MinCreatedDateFilterId')),
					maxCreatedDateFilter:  getMaxDateFilter($('#MaxCreatedDateFilterId'))
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLoanPostedModal.open({ id: data.record.loanPosted.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.loanPosted.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteLoanPosted(data.record.loanPosted);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "loanPosted.payrollNo",
						 name: "payrollNo"   
					},
					{
						targets: 3,
						 data: "loanPosted.payDate",
						 name: "payDate" ,
					render: function (payDate) {
						if (payDate) {
							return moment(payDate).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 4,
						 data: "loanPosted.amount",
						 name: "amount"   
					},
					{
						targets: 5,
						 data: "loanPosted.remarks",
						 name: "remarks"   
					},
					{
						targets: 6,
						 data: "loanPosted.createdBy",
						 name: "createdBy"   
					},
					{
						targets: 7,
						 data: "loanPosted.createdDate",
						 name: "createdDate" ,
					render: function (createdDate) {
						if (createdDate) {
							return moment(createdDate).format('L');
						}
						return "";
					}
			  
					}
            ]
        });

        function getLoanPosted() {
            dataTable.ajax.reload();
        }

        function deleteLoanPosted(loanPosted) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _loanPostedService.delete({
                            id: loanPosted.id
                        }).done(function () {
                            getLoanPosted(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewLoanPostedButton').click(function () {
            _createOrEditModal.open();
        });        

		$('#ExportToExcelButton').click(function () {
            _loanPostedService
                .getLoanPostedToExcel({
				filter : $('#LoanPostedTableFilter').val(),
					minPayrollNoFilter: $('#MinPayrollNoFilterId').val(),
					maxPayrollNoFilter: $('#MaxPayrollNoFilterId').val(),
					minPayDateFilter:  getDateFilter($('#MinPayDateFilterId')),
					maxPayDateFilter:  getMaxDateFilter($('#MaxPayDateFilterId')),
					minAmountFilter: $('#MinAmountFilterId').val(),
					maxAmountFilter: $('#MaxAmountFilterId').val(),
					remarksFilter: $('#RemarksFilterId').val(),
					minCreatedByFilter: $('#MinCreatedByFilterId').val(),
					maxCreatedByFilter: $('#MaxCreatedByFilterId').val(),
					minCreatedDateFilter:  getDateFilter($('#MinCreatedDateFilterId')),
					maxCreatedDateFilter:  getMaxDateFilter($('#MaxCreatedDateFilterId'))
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditLoanPostedModalSaved', function () {
            getLoanPosted();
        });

		$('#GetLoanPostedButton').click(function (e) {
            e.preventDefault();
            getLoanPosted();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getLoanPosted();
		  }
		});
		
		
		
    });
})();
