(function($) {
    app.modals.CreateOrEditSssTablesModal = function() {
        var _sssTablesService = abp.services.app.sssTables;
        var _modalManager;
        var _$sssTablesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$sssTablesInformationForm = _modalManager.getModal().find('form[name=SssTablesInformationsForm]');
            _$sssTablesInformationForm.validate();
        };
        this.save = function() {
            if (!_$sssTablesInformationForm.valid()) {
                return;
            }
            var sssTables = _$sssTablesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _sssTablesService.createOrEdit(
                sssTables
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditSssTablesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);