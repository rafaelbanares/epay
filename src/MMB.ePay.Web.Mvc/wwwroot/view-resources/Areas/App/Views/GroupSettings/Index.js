﻿(function () {
    $(function () {

        var _$groupSettingsTable = $('#GroupSettingsTable');
        var _groupSettingsService = abp.services.app.groupSettings;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.GroupSettings.Create'),
            edit: abp.auth.hasPermission('Pages.GroupSettings.Edit'),
            'delete': abp.auth.hasPermission('Pages.GroupSettings.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/GroupSettings/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/GroupSettings/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditGroupSettingsModal'
                });
                   

		 var _viewGroupSettingsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/GroupSettings/ViewgroupSettingsModal',
            modalClass: 'ViewGroupSettingsModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$groupSettingsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _groupSettingsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#GroupSettingsTableFilter').val(),
					displayNameFilter: $('#DisplayNameFilterId').val(),
					minOrderNoFilter: $('#MinOrderNoFilterId').val(),
					maxOrderNoFilter: $('#MaxOrderNoFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewGroupSettingsModal.open({ id: data.record.groupSettings.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.groupSettings.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteGroupSettings(data.record.groupSettings);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "groupSettings.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "groupSettings.orderNo",
						 name: "orderNo"   
					}
            ]
        });

        function getGroupSettings() {
            dataTable.ajax.reload();
        }

        function deleteGroupSettings(groupSettings) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _groupSettingsService.delete({
                            id: groupSettings.id
                        }).done(function () {
                            getGroupSettings(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewGroupSettingsButton').click(function () {
            _createOrEditModal.open();
        });        

		$('#ExportToExcelButton').click(function () {
            _groupSettingsService
                .getGroupSettingsToExcel({
				filter : $('#GroupSettingsTableFilter').val(),
					displayNameFilter: $('#DisplayNameFilterId').val(),
					minOrderNoFilter: $('#MinOrderNoFilterId').val(),
					maxOrderNoFilter: $('#MaxOrderNoFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditGroupSettingsModalSaved', function () {
            getGroupSettings();
        });

		$('#GetGroupSettingsButton').click(function (e) {
            e.preventDefault();
            getGroupSettings();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getGroupSettings();
		  }
		});
		
		
		
    });
})();
