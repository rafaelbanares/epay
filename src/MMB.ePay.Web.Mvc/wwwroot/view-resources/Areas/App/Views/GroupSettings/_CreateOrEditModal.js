(function($) {
    app.modals.CreateOrEditGroupSettingsModal = function() {
        var _groupSettingsService = abp.services.app.groupSettings;
        var _modalManager;
        var _$groupSettingsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$groupSettingsInformationForm = _modalManager.getModal().find('form[name=GroupSettingsInformationsForm]');
            _$groupSettingsInformationForm.validate();
        };
        this.save = function() {
            if (!_$groupSettingsInformationForm.valid()) {
                return;
            }
            var groupSettings = _$groupSettingsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _groupSettingsService.createOrEdit(
                groupSettings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditGroupSettingsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);