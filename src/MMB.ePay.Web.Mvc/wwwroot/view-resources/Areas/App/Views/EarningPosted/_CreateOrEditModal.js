(function($) {
    app.modals.CreateOrEditEarningPostedModal = function() {
        var _earningPostedService = abp.services.app.earningPosted;
        var _modalManager;
        var _$earningPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$earningPostedInformationForm = _modalManager.getModal().find('form[name=EarningPostedInformationsForm]');
            _$earningPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$earningPostedInformationForm.valid()) {
                return;
            }
            var earningPosted = _$earningPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _earningPostedService.createOrEdit(
                earningPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEarningPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);