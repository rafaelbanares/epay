(function($) {
    app.modals.CreateOrEditEarningProcessModal = function() {
        var _earningProcessService = abp.services.app.earningProcess;
        var _modalManager;
        var _$earningProcessInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$earningProcessInformationForm = _modalManager.getModal().find('form[name=EarningProcessInformationsForm]');
            _$earningProcessInformationForm.validate();
        };
        this.save = function() {
            if (!_$earningProcessInformationForm.valid()) {
                return;
            }
            var earningProcess = _$earningProcessInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _earningProcessService.createOrEdit(
                earningProcess
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEarningProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);