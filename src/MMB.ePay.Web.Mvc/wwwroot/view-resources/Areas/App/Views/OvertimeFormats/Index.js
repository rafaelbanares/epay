(function() {
    $(function() {
        var _$overtimeMainTypesTable = $('#OvertimeMainTypesTable');
        var _$overtimeSubTypesTable = $('#OvertimeSubTypesTable');

        var _overtimeMainTypesService = abp.services.app.overtimeMainTypes;
        var _overtimeSubTypesService = abp.services.app.overtimeSubTypes;

        var _permissions = {
            create: abp.auth.hasPermission('Pages.OvertimeMainTypes.Create'),
            edit: abp.auth.hasPermission('Pages.OvertimeMainTypes.Edit')
        };
        var _permissions2 = {
            create: abp.auth.hasPermission('Pages.OvertimeSubTypes.Create'),
            edit: abp.auth.hasPermission('Pages.OvertimeSubTypes.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeMainTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeMainTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeMainTypesModal'
        });
        var _createOrEditModal2 = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeSubTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeSubTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeSubTypesModal'
        });

        var _viewOvertimeMainTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeMainTypes/ViewovertimeMainTypesModal',
            modalClass: 'ViewOvertimeMainTypesModal'
        });
        var _viewOvertimeSubTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeSubTypes/ViewovertimeSubTypesModal',
            modalClass: 'ViewOvertimeSubTypesModal'
        });

        var dataTable = _$overtimeMainTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _overtimeMainTypesService.getAll
            },
            columnDefs: [{
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                width: 120,
                targets: 1,
                data: null,
                orderable: false,
                autoWidth: false,
                defaultContent: '',
                rowAction: {
                    cssClass: 'btn btn-brand dropdown-toggle',
                    text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                    items: [{
                        text: app.localize('View'),
                        iconStyle: 'far fa-eye mr-2',
                        action: function (data) {
                            _viewOvertimeMainTypesModal.open({
                                id: data.record.overtimeMainTypes.id
                            });
                        }
                    },
                    {
                        text: app.localize('Edit'),
                        iconStyle: 'far fa-edit mr-2',
                        visible: function () {
                            return _permissions.edit;
                        },
                        action: function (data) {
                            _createOrEditModal.open({
                                id: data.record.overtimeMainTypes.id
                            });
                        }
                    }
                    ]
                }
            },
            {
                targets: 2,
                data: "overtimeMainTypes.displayName",
                name: "displayName"
            },
            {
                targets: 3,
                data: "overtimeMainTypes.description",
                name: "description"
            }
            ]
        });
        var dataTable2 = _$overtimeSubTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _overtimeSubTypesService.getAll
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewOvertimeSubTypesModal.open({
                                        id: data.record.overtimeSubTypes.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions2.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal2.open({
                                        id: data.record.overtimeSubTypes.id
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "overtimeSubTypes.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "overtimeSubTypes.description",
                    name: "description"
                }
            ]
        });

        function getOvertimeMainTypes() {
            dataTable.ajax.reload();
        }
        function getOvertimeSubTypes() {
            dataTable2.ajax.reload();
        }

        $('#CreateNewOvertimeMainTypesButton').click(function () {
            _createOrEditModal.open();
        });

        $('#CreateNewOvertimeSubTypesButton').click(function() {
            _createOrEditModal2.open();
        });
    
        abp.event.on('app.createOrEditOvertimeMainTypesModalSaved', function() {
            getOvertimeMainTypes();
        });
        abp.event.on('app.createOrEditOvertimeSubTypesModalSaved', function () {
            getOvertimeSubTypes();
        });

        $('#GetOvertimeMainTypesButton').click(function (e) {
            e.preventDefault();
            getOvertimeMainTypes();
        });
        $('#GetOvertimeSubTypesButton').click(function(e) {
            e.preventDefault();
            getOvertimeSubTypes();
        });

        $(document).keypress(function(e) {
            if (e.which === 13) {
                getOvertimeMainTypes();
                getOvertimeSubTypes();
            }
        });
    });
})();