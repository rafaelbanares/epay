(function($) {
    app.modals.CreateOrEditEmployeePayrollModal = function() {
        var _employeePayrollService = abp.services.app.employeePayroll;
        var _modalManager;
        var _$employeePayrollInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$employeePayrollInformationForm = _modalManager.getModal().find('form[name=EmployeePayrollInformationsForm]');
            _$employeePayrollInformationForm.validate();
        };
        this.save = function() {
            if (!_$employeePayrollInformationForm.valid()) {
                return;
            }
            var employeePayroll = _$employeePayrollInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _employeePayrollService.createOrEdit(
                employeePayroll
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEmployeePayrollModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);