(function() {
    $(function() {
        var _holidaysService = abp.services.app.holidays;
        var _$holidaysInformationForm = $('form[name=HolidaysInformationsForm]');
        _$holidaysInformationForm.validate();
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        function save(successCallback) {
            if (!_$holidaysInformationForm.valid()) {
                return;
            }
            var holidays = _$holidaysInformationForm.serializeFormToObject();
            abp.ui.setBusy();
            _holidaysService.createOrEdit(
                holidays
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                abp.event.trigger('app.createOrEditHolidaysModalSaved');
                if (typeof(successCallback) === 'function') {
                    successCallback();
                }
            }).always(function() {
                abp.ui.clearBusy();
            });
        };

        function clearForm() {
            _$holidaysInformationForm[0].reset();
        }
        $('#saveBtn').click(function() {
            save(function() {
                window.location = "/App/Holidays";
            });
        });
        $('#saveAndNewBtn').click(function() {
            save(function() {
                if (!$('input[name=id]').val()) { //if it is create page
                    clearForm();
                }
            });
        });
    });
})();