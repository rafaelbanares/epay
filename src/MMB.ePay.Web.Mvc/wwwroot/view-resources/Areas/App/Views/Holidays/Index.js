﻿(function () {
    $(function () {

        var _$holidaysTable = $('#HolidaysTable');
        var _holidaysService = abp.services.app.holidays;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Holidays.Create'),
            edit: abp.auth.hasPermission('Pages.Holidays.Edit'),
            'delete': abp.auth.hasPermission('Pages.Holidays.Delete')
        };

               

		 var _viewHolidaysModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Holidays/ViewholidaysModal',
            modalClass: 'ViewHolidaysModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$holidaysTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _holidaysService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#HolidaysTableFilter').val(),
					displayNameFilter: $('#DisplayNameFilterId').val(),
					minDateFilter:  getDateFilter($('#MinDateFilterId')),
					maxDateFilter:  getMaxDateFilter($('#MaxDateFilterId')),
					halfDayFilter: $('#HalfDayFilterId').val(),
					holidayTypeFilter: $('#HolidayTypeFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    window.location="/App/Holidays/ViewHolidays/" + data.record.holidays.id;
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            window.location="/App/Holidays/CreateOrEdit/" + data.record.holidays.id;                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteHolidays(data.record.holidays);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "holidays.displayName",
						 name: "displayName"   
					},
					{
						targets: 3,
						 data: "holidays.date",
						 name: "date" ,
					render: function (date) {
						if (date) {
							return moment(date).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 4,
						 data: "holidays.halfDay",
						 name: "halfDay"  ,
						render: function (halfDay) {
							if (halfDay) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 5,
						 data: "holidays.holidayType",
						 name: "holidayType"   
					}
            ]
        });

        function getHolidays() {
            dataTable.ajax.reload();
        }

        function deleteHolidays(holidays) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _holidaysService.delete({
                            id: holidays.id
                        }).done(function () {
                            getHolidays(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

                

		$('#ExportToExcelButton').click(function () {
            _holidaysService
                .getHolidaysToExcel({
				filter : $('#HolidaysTableFilter').val(),
					displayNameFilter: $('#DisplayNameFilterId').val(),
					minDateFilter:  getDateFilter($('#MinDateFilterId')),
					maxDateFilter:  getMaxDateFilter($('#MaxDateFilterId')),
					halfDayFilter: $('#HalfDayFilterId').val(),
					holidayTypeFilter: $('#HolidayTypeFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditHolidaysModalSaved', function () {
            getHolidays();
        });

		$('#GetHolidaysButton').click(function (e) {
            e.preventDefault();
            getHolidays();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getHolidays();
		  }
		});
		
		
		
    });
})();
