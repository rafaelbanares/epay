(function($) {
    app.modals.CreateOrEditEarningTmpModal = function() {
        var _earningTmpService = abp.services.app.earningTmp;
        var _modalManager;
        var _$earningTmpInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$earningTmpInformationForm = _modalManager.getModal().find('form[name=EarningTmpInformationsForm]');
            _$earningTmpInformationForm.validate();
        };
        this.save = function() {
            if (!_$earningTmpInformationForm.valid()) {
                return;
            }
            var earningTmp = _$earningTmpInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _earningTmpService.createOrEdit(
                earningTmp
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEarningTmpModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);