(function($) {
    app.modals.CreateOrEditProratedEarningsModal = function() {
        var _proratedEarningsService = abp.services.app.proratedEarnings;
        var _modalManager;
        var _$proratedEarningsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$proratedEarningsInformationForm = _modalManager.getModal().find('form[name=ProratedEarningsInformationsForm]');
            _$proratedEarningsInformationForm.validate();

            if ($('#ProratedEarnings_InTimesheetTypeId').val() != '0') {
                $('#ProratedEarnings_InTimesheetTypeId').prop("disabled", false);
                $('#ProratedEarnings_BasedOnTimesheet').prop("checked", true);
            }

            if ($('#ProratedEarnings_InOvertimeTypeId').val() != '0') {
                $('#ProratedEarnings_InOvertimeTypeId').prop("disabled", false);
                $('#ProratedEarnings_BasedOnOvertime').prop("checked", true);
            }

            $('#ProratedEarnings_BasedOnTimesheet').click(function (e) {
                $('#ProratedEarnings_InTimesheetTypeId').prop("disabled", false);
                $('#ProratedEarnings_InOvertimeTypeId').prop("disabled", true);

                $('#ProratedEarnings_InTimesheetTypeId').val("0");
                $('#ProratedEarnings_InOvertimeTypeId').val("0");
            });

            $('#ProratedEarnings_BasedOnOvertime').click(function (e) {
                $('#ProratedEarnings_InTimesheetTypeId').prop("disabled", true);
                $('#ProratedEarnings_InOvertimeTypeId').prop("disabled", false);

                $('#ProratedEarnings_InTimesheetTypeId').val("0");
                $('#ProratedEarnings_InOvertimeTypeId').val("0");
            });

        };
        this.save = function() {
            if (!_$proratedEarningsInformationForm.valid()) {
                return;
            }
            var proratedEarnings = _$proratedEarningsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _proratedEarningsService.createOrEdit(
                proratedEarnings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditProratedEarningsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);
