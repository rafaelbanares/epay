(function() {
    $(function() {
        var _$proratedEarningsTable = $('#ProratedEarningsTable');
        var _proratedEarningsService = abp.services.app.proratedEarnings;
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });
        var _permissions = {
            create: abp.auth.hasPermission('Pages.ProratedEarnings.Create'),
            edit: abp.auth.hasPermission('Pages.ProratedEarnings.Edit')
        };
        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ProratedEarnings/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ProratedEarnings/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditProratedEarningsModal'
        });
        var _viewProratedEarningsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ProratedEarnings/ViewproratedEarningsModal',
            modalClass: 'ViewProratedEarningsModal'
        });

        var dataTable = _$proratedEarningsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _proratedEarningsService.getAll,
                inputFilter: function() {
                    return {
                        payrollFreqFilter: $('#PayrollFreqFilterId').val(),
                        applicableYearFilter: $('#ApplicableYearFilterId').val()
                    };
                }
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewProratedEarningsModal.open({
                                        id: data.record.proratedEarnings.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal.open({
                                        id: data.record.proratedEarnings.id
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "proratedEarnings.earningType",
                    name: "earningType"
                },
                {
                    targets: 3,
                    data: "proratedEarnings.inTimesheet",
                    name: "inTimesheet"
                },
                {
                    targets: 4,
                    data: "proratedEarnings.inOvertimeType",
                    name: "inOvertimeType"
                },
                {
                    targets: 5,
                    data: "proratedEarnings.outEarningType",
                    name: "outEarningType"
                }
                
            ]
        });

        function getProratedEarnings() {
            dataTable.ajax.reload();
        }

        $('#CreateNewProratedEarningsButton').click(function() {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditProratedEarningsModalSaved', function() {
            getProratedEarnings();
        });

        $('#GetProratedEarningsButton').click(function(e) {
            e.preventDefault();
            getProratedEarnings();
        });
    });
})();