(function($) {
    app.modals.CreateOrEditStatutoryPostedModal = function() {
        var _statutoryPostedService = abp.services.app.statutoryPosted;
        var _modalManager;
        var _$statutoryPostedInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$statutoryPostedInformationForm = _modalManager.getModal().find('form[name=StatutoryPostedInformationsForm]');
            _$statutoryPostedInformationForm.validate();
        };
        this.save = function() {
            if (!_$statutoryPostedInformationForm.valid()) {
                return;
            }
            var statutoryPosted = _$statutoryPostedInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _statutoryPostedService.createOrEdit(
                statutoryPosted
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditStatutoryPostedModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);