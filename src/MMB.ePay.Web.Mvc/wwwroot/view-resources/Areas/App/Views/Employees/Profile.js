﻿(function () {
    $(function () {
        var _employeesService = abp.services.app.employees;
        var _employeePayrollService = abp.services.app.employeePayroll;
        var _employeeStatutoryInfoService = abp.services.app.employeeStatutoryInfo;
        var _employersService = abp.services.app.employers;
        var _dependentsService = abp.services.app.dependents;

        var _$employeesInformationForm = $('form[name=EmployeesInformationsForm]');
        var _$employeePayrollForm = $('form[name=EmployeePayrollForm]');
        var _$statutoryInfoForm = $('form[name=StatutoryInfoForm]');

        var _$employersTable = $('#EmployersTable');
        var _$dependentsTable = $('#DependentsTable');

        var hasView = $('#HasView').val();

        if (hasView) {
            $('input').each(function () {
                $(this).prop("disabled", true);
            });

            $('select').each(function () {
                $(this).prop('disabled', true);
            });
        }

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _employerPermissions = {
            create: abp.auth.hasPermission('Pages.Employers.Create'),
            edit: abp.auth.hasPermission('Pages.Employers.Edit'),
            'delete': abp.auth.hasPermission('Pages.Employers.Delete')
        };
        var _createOrEditEmployersModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Employers/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Employers/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditEmployersModal'
        });
        var _viewEmployersModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Employers/ViewemployersModal',
            modalClass: 'ViewEmployersModal'
        });

        var employersTable = _$employersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _employersService.getAll,
                inputFilter: function () {
                    return {
                        employeeFilterId: $('input[name=id]').val()
                    };
                }
            },
            columnDefs: [
                //{
                //    className: 'control responsive',
                //    orderable: false,
                //    render: function () {
                //        return '';
                //    },
                //    targets: 0
                //},
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                            text: app.localize('View'),
                            iconStyle: 'far fa-eye mr-2',
                            visible: function () {
                                return !hasView;
                            },
                            action: function (data) {
                                _viewEmployersModal.open({
                                    id: data.record.employers.id
                                });
                            }
                        },
                        {
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                if (!hasView) {
                                    return _employerPermissions.edit;
                                } else {
                                    return false;
                                }
                            },
                            action: function (data) {
                                _createOrEditEmployersModal.open({
                                    id: data.record.employers.id
                                });
                            }
                        },
                        {
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                if (!hasView) {
                                    return _employerPermissions.delete;
                                } else {
                                    return false;
                                }
                            },
                            action: function (data) {
                                deleteEmployers(data.record.employers);
                            }
                        }
                        ]
                    }
                },
                {
                    targets: 1,
                    data: "employers.employerName",
                    name: "employerName"
                },
                {
                    targets: 2,
                    data: "employers.basicPay",
                    name: "basicPay"
                },
                {
                    targets: 3,
                    data: "employers.deminimis",
                    name: "deminimis"
                },
                {
                    targets: 4,
                    data: "employers.grossPay",
                    name: "grosspay"
                },
                //{
                //    targets: 5,
                //    data: "employers.txBonus",
                //    name: "txBonus"
                //},
                //{
                //    targets: 6,
                //    data: "employers.txOtherIncome",
                //    name: "txOtherIncome"
                //},
                //{
                //    targets: 7,
                //    data: "employers.wTax",
                //    name: "wTax"
                //},
                {
                    targets: 5,
                    data: "employers.sss",
                    name: "sss"
                }
            ]
        });

        function getEmployers() {
            employersTable.ajax.reload();
        }

        function deleteEmployers(employers) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _employersService.delete({
                            id: employers.id
                        }).done(function () {
                            getEmployers(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        abp.event.on('app.createOrEditEmployersModalSaved', function () {
            getEmployers();
        });

        $('#CreateNewEmployersButton').click(function () {
            _createOrEditEmployersModal.open({ employeeId: $('input[name=id]').val() });
        });

        var _dependentPermissions = {
            create: abp.auth.hasPermission('Pages.Dependents.Create'),
            edit: abp.auth.hasPermission('Pages.Dependents.Edit'),
            'delete': abp.auth.hasPermission('Pages.Dependents.Delete')
        };
        var _createOrEditDependentsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Dependents/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Dependents/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditDependentsModal'
        });
        var _viewDependentsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Dependents/ViewdependentsModal',
            modalClass: 'ViewDependentsModal'
        });

        var dependentsTable = _$dependentsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _dependentsService.getAll,
                inputFilter: function () {
                    return {
                        employeeFilterId: $('input[name=id]').val()
                    };
                }
            },
            columnDefs: [
                //{
                //    className: 'control responsive',
                //    orderable: false,
                //    render: function () {
                //        return '';
                //    },
                //    targets: 0
                //},
                {
                    width: 120,
                    targets: 0,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                            text: app.localize('View'),
                            iconStyle: 'far fa-eye mr-2',
                            visible: function () {
                                return !hasView;
                            },
                            action: function (data) {
                                _viewDependentsModal.open({
                                    id: data.record.dependents.id
                                });
                            }
                        },
                        {
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                if (!hasView) {
                                    return _dependentPermissions.edit;
                                } else {
                                    return false;
                                }

                            },
                            action: function (data) {
                                _createOrEditDependentsModal.open({
                                    id: data.record.dependents.id
                                });
                            }
                        },
                        {
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                if (!hasView) {
                                    return _dependentPermissions.delete;
                                } else {
                                    return false;
                                }
                            },
                            action: function (data) {
                                deleteDependents(data.record.dependents);
                            }
                        }
                        ]
                    }
                },
                {
                    targets: 1,
                    data: "dependents.dependentName",
                    name: "dependentName"
                },
                {
                    targets: 2,
                    data: "dependents.birthDate",
                    name: "birthDate",
                    render: function (birthDate) {
                        if (birthDate) {
                            return moment(birthDate).format('L');
                        }
                        return "";
                    }
                }
            ]
        });

        function getDependents() {
            dependentsTable.ajax.reload();
        }

        function deleteDependents(dependents) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _dependentsService.delete({
                            id: dependents.id
                        }).done(function () {
                            getDependents(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        abp.event.on('app.createOrEditDependentsModalSaved', function () {
            getDependents();
        });

        $('#CreateNewDependentsButton').click(function () {
            _createOrEditDependentsModal.open({ employeeId: $('input[name=id]').val() });
        });

        function save(successCallback) {
            if (!_$employeesInformationForm.valid() || !_$employeePayrollForm.valid() || !_$statutoryInfoForm.valid()) {
                return;
            }

            var employees = _$employeesInformationForm.serializeFormToObject();
            var employeePayroll = _$employeePayrollForm.serializeFormToObject();
            var statutoryInfo = _$statutoryInfoForm.serializeFormToObject();
            //var employee = _$employeeStatutoryForm

            abp.ui.setBusy();
            _employeesService.createOrEdit(
                employees
            ).done(function () {
                _employeePayrollService.createOrEdit(
                    employeePayroll
                ).done(function () {
                    var statutoryAccountNo = [];
                    $.each(statutoryInfo, function (i, item) {
                        statutoryAccountNo.push(item)
                    });

                    var statutoryTypes = {
                        employeeId: employees.id,
                        statutoryAccountNo: statutoryAccountNo
                    };

                    _employeeStatutoryInfoService.createOrEdit(
                        statutoryTypes
                    ).done(function () {
                        abp.event.trigger('app.createOrEditEmployeesModalSaved');

                        if (typeof (successCallback) === 'function') {
                            successCallback();
                        }
                    });
                });
            }).always(function () {
                abp.ui.clearBusy();
            });
        }

        $('#btnPersonalCancel, #btnEmployeePayrollCancel, #btnStatutoryInfoCancel, #btnEmployersCancel, #btnDependentsCancel').click(function () {
            window.location = "/App/Employees";
        });

        $('#btnPersonalSave, #btnEmployeePayrollSave, #btnStatutoryInfoSave').click(function () {
            save(function () {
                abp.notify.info(app.localize('Updated Successfully'));
            });
        });
    });
})();
