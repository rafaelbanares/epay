(function($) {
    app.modals.CreateOrEditOTSheetProcessModal = function() {
        var _otSheetProcessService = abp.services.app.otSheetProcess;
        var _modalManager;
        var _$otSheetProcessInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$otSheetProcessInformationForm = _modalManager.getModal().find('form[name=OTSheetProcessInformationsForm]');
            _$otSheetProcessInformationForm.validate();
        };
        this.save = function() {
            if (!_$otSheetProcessInformationForm.valid()) {
                return;
            }
            var otSheetProcess = _$otSheetProcessInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _otSheetProcessService.createOrEdit(
                otSheetProcess
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOTSheetProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);