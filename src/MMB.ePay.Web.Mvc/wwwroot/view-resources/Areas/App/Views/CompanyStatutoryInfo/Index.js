﻿(function () {
    $(function () {

        var _$companyStatutoryInfoTable = $('#CompanyStatutoryInfoTable');
        var _companyStatutoryInfoService = abp.services.app.companyStatutoryInfo;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.CompanyStatutoryInfo.Create'),
            edit: abp.auth.hasPermission('Pages.CompanyStatutoryInfo.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/CompanyStatutoryInfo/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/CompanyStatutoryInfo/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditCompanyStatutoryInfoModal'
        });


        var _viewCompanyStatutoryInfoModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/CompanyStatutoryInfo/ViewcompanyStatutoryInfoModal',
            modalClass: 'ViewCompanyStatutoryInfoModal'
        });

        var dataTable = _$companyStatutoryInfoTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _companyStatutoryInfoService.getAll,
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewCompanyStatutoryInfoModal.open({ id: data.record.companyStatutoryInfo.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.companyStatutoryInfo.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "companyStatutoryInfo.signatoryName",
                    name: "signatoryName"
                },
                {
                    targets: 3,
                    data: "companyStatutoryInfo.statutoryAcctNo",
                    name: "statutoryAcctNo"
                },
                {
                    targets: 4,
                    data: "companyStatutoryInfo.branch",
                    name: "branch"
                },
                {
                    targets: 5,
                    data: "companyStatutoryInfo.statutoryType",
                    name: "statutoryType"
                }
            ]
        });

        function getCompanyStatutoryInfo() {
            dataTable.ajax.reload();
        }

        $('#CreateNewCompanyStatutoryInfoButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditCompanyStatutoryInfoModalSaved', function () {
            getCompanyStatutoryInfo();
        });

        $('#GetCompanyStatutoryInfoButton').click(function (e) {
            e.preventDefault();
            getCompanyStatutoryInfo();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getCompanyStatutoryInfo();
            }
        });
    });
})();
