(function($) {
    app.modals.CreateOrEditCompanyStatutoryInfoModal = function () {
        var _companyStatutoryInfoService = abp.services.app.companyStatutoryInfo;
        var _modalManager;
        var _$companyStatutoryInfoInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$companyStatutoryInfoInformationForm = _modalManager.getModal().find('form[name=CompanyStatutoryInfoInformationsForm]');
            _$companyStatutoryInfoInformationForm.validate();
        };
        this.save = function() {
            if (!_$companyStatutoryInfoInformationForm.valid()) {
                return;
            }
            var companyStatutoryInfo = _$companyStatutoryInfoInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _companyStatutoryInfoService.createOrEdit(
                companyStatutoryInfo
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditCompanyStatutoryInfoModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);

