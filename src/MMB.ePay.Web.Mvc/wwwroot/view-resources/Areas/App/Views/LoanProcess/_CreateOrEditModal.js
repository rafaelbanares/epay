(function($) {
    app.modals.CreateOrEditLoanProcessModal = function() {
        var _loanProcessService = abp.services.app.loanProcess;
        var _modalManager;
        var _$loanProcessInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$loanProcessInformationForm = _modalManager.getModal().find('form[name=LoanProcessInformationsForm]');
            _$loanProcessInformationForm.validate();
        };
        this.save = function() {
            if (!_$loanProcessInformationForm.valid()) {
                return;
            }
            var loanProcess = _$loanProcessInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _loanProcessService.createOrEdit(
                loanProcess
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLoanProcessModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);