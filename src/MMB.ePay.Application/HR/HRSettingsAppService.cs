﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize(AppPermissions.Pages_HRSettings)]
    public class HRSettingsAppService : ePayAppServiceBase, IHRSettingsAppService
    {
        private readonly IRepository<HRSettings> _hrSettingsRepository;

        public HRSettingsAppService(IRepository<HRSettings> hrSettingsRepository)
        {
            _hrSettingsRepository = hrSettingsRepository;
        }

        public async Task<PagedResultDto<GetHRSettingsForViewDto>> GetAll(GetAllHRSettingsInput input)
        {

            var filteredHRSettings = _hrSettingsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Key.Contains(input.Filter) || e.Value.Contains(input.Filter) || e.DataType.Contains(input.Filter) || e.Caption.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.KeyFilter), e => e.Key == input.KeyFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ValueFilter), e => e.Value == input.ValueFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DataTypeFilter), e => e.DataType == input.DataTypeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CaptionFilter), e => e.Caption == input.CaptionFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
                        .WhereIf(input.MinDisplayOrderFilter != null, e => e.DisplayOrder >= input.MinDisplayOrderFilter)
                        .WhereIf(input.MaxDisplayOrderFilter != null, e => e.DisplayOrder <= input.MaxDisplayOrderFilter)
                        .WhereIf(input.GroupSettingIdFilter.HasValue, e => e.GroupSettingId == input.GroupSettingIdFilter);

            var pagedAndFilteredHRSettings = filteredHRSettings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var hrSettings = from o in pagedAndFilteredHRSettings
                             select new GetHRSettingsForViewDto()
                             {
                                 HRSettings = new HRSettingsDto
                                 {
                                     Key = o.Key,
                                     Value = o.Value,
                                     DataType = o.DataType,
                                     Caption = o.Caption,
                                     Description = o.Description,
                                     DisplayOrder = o.DisplayOrder,
                                     GroupSetting = o.GroupSettings.DisplayName,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredHRSettings.CountAsync();

            return new PagedResultDto<GetHRSettingsForViewDto>(
                totalCount,
                await hrSettings.ToListAsync()
            );
        }

        public async Task<GetHRSettingsForViewDto> GetHRSettingsForView(int id)
        {
            var hrSettings = await _hrSettingsRepository.GetAsync(id);

            var output = new GetHRSettingsForViewDto { HRSettings = ObjectMapper.Map<HRSettingsDto>(hrSettings) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_HRSettings_Edit)]
        public async Task<GetHRSettingsForEditOutput> GetHRSettingsForEdit(EntityDto input)
        {
            var hrSettings = await _hrSettingsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetHRSettingsForEditOutput { HRSettings = ObjectMapper.Map<CreateOrEditHRSettingsDto>(hrSettings) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditHRSettingsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_HRSettings_Create)]
        protected virtual async Task Create(CreateOrEditHRSettingsDto input)
        {
            var hrSettings = ObjectMapper.Map<HRSettings>(input);

            await _hrSettingsRepository.InsertAsync(hrSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_HRSettings_Edit)]
        protected virtual async Task Update(CreateOrEditHRSettingsDto input)
        {
            var hrSettings = await _hrSettingsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, hrSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_HRSettings_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _hrSettingsRepository.DeleteAsync(input.Id);
        }
    }
}