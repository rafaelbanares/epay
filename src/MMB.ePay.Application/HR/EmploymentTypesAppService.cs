﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize(AppPermissions.Pages_EmploymentTypes)]
    public class EmploymentTypesAppService : ePayAppServiceBase, IEmploymentTypesAppService
    {
        private readonly IRepository<EmploymentTypes, string> _employmentTypesRepository;

        public EmploymentTypesAppService(IRepository<EmploymentTypes, string> employmentTypesRepository)
        {
            _employmentTypesRepository = employmentTypesRepository;
        }

        public async Task<IList<SelectListItem>> GetEmploymentTypeList()
        {
            var output = await _employmentTypesRepository.GetAll()
                .Select(e => new SelectListItem
                {
                    Value = e.Id,
                    Text = e.DisplayName
                }).ToListAsync();

            output.Insert(0, new SelectListItem { Value = string.Empty, Text = "Select" });

            return output;
        }

        public async Task<PagedResultDto<GetEmploymentTypesForViewDto>> GetAll(GetAllEmploymentTypesInput input)
        {

            var filteredEmploymentTypes = _employmentTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter);

            var pagedAndFilteredEmploymentTypes = filteredEmploymentTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var employmentTypes = from o in pagedAndFilteredEmploymentTypes
                                  select new GetEmploymentTypesForViewDto()
                                  {
                                      EmploymentTypes = new EmploymentTypesDto
                                      {
                                          Description = o.Description,
                                          Id = o.Id
                                      }
                                  };

            var totalCount = await filteredEmploymentTypes.CountAsync();

            return new PagedResultDto<GetEmploymentTypesForViewDto>(
                totalCount,
                await employmentTypes.ToListAsync()
            );
        }

        public async Task<GetEmploymentTypesForViewDto> GetEmploymentTypesForView(string id)
        {
            var employmentTypes = await _employmentTypesRepository.GetAsync(id);

            var output = new GetEmploymentTypesForViewDto { EmploymentTypes = ObjectMapper.Map<EmploymentTypesDto>(employmentTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Edit)]
        public async Task<GetEmploymentTypesForEditOutput> GetEmploymentTypesForEdit(EntityDto<string> input)
        {
            var employmentTypes = await _employmentTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEmploymentTypesForEditOutput { EmploymentTypes = ObjectMapper.Map<CreateOrEditEmploymentTypesDto>(employmentTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmploymentTypesDto input)
        {
            if (input.Id.IsNullOrWhiteSpace())
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Create)]
        protected virtual async Task Create(CreateOrEditEmploymentTypesDto input)
        {
            var employmentTypes = ObjectMapper.Map<EmploymentTypes>(input);

            await _employmentTypesRepository.InsertAsync(employmentTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Edit)]
        protected virtual async Task Update(CreateOrEditEmploymentTypesDto input)
        {
            var employmentTypes = await _employmentTypesRepository.FirstOrDefaultAsync(input.Id);
            ObjectMapper.Map(input, employmentTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Delete)]
        public async Task Delete(EntityDto<string> input)
        {
            await _employmentTypesRepository.DeleteAsync(input.Id);
        }
    }
}