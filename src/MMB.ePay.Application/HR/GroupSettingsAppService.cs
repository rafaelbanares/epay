﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize(AppPermissions.Pages_GroupSettings)]
    public class GroupSettingsAppService : ePayAppServiceBase, IGroupSettingsAppService
    {
        private readonly IRepository<GroupSettings> _groupSettingsRepository;

        public GroupSettingsAppService(IRepository<GroupSettings> groupSettingsRepository)
        {
            _groupSettingsRepository = groupSettingsRepository;
        }

        public async Task<PagedResultDto<GetGroupSettingsForViewDto>> GetAll(GetAllGroupSettingsInput input)
        {

            var filteredGroupSettings = _groupSettingsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                        .WhereIf(input.MinOrderNoFilter != null, e => e.OrderNo >= input.MinOrderNoFilter)
                        .WhereIf(input.MaxOrderNoFilter != null, e => e.OrderNo <= input.MaxOrderNoFilter);

            var pagedAndFilteredGroupSettings = filteredGroupSettings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var groupSettings = from o in pagedAndFilteredGroupSettings
                                select new GetGroupSettingsForViewDto()
                                {
                                    GroupSettings = new GroupSettingsDto
                                    {
                                        DisplayName = o.DisplayName,
                                        OrderNo = o.OrderNo,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredGroupSettings.CountAsync();

            return new PagedResultDto<GetGroupSettingsForViewDto>(
                totalCount,
                await groupSettings.ToListAsync()
            );
        }

        public async Task<GetGroupSettingsForViewDto> GetGroupSettingsForView(int id)
        {
            var groupSettings = await _groupSettingsRepository.GetAsync(id);

            var output = new GetGroupSettingsForViewDto { GroupSettings = ObjectMapper.Map<GroupSettingsDto>(groupSettings) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Edit)]
        public async Task<GetGroupSettingsForEditOutput> GetGroupSettingsForEdit(EntityDto input)
        {
            var groupSettings = await _groupSettingsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetGroupSettingsForEditOutput { GroupSettings = ObjectMapper.Map<CreateOrEditGroupSettingsDto>(groupSettings) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditGroupSettingsDto input)
        {
            if (!input.Id.HasValue)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Create)]
        protected virtual async Task Create(CreateOrEditGroupSettingsDto input)
        {
            var groupSettings = ObjectMapper.Map<GroupSettings>(input);

            await _groupSettingsRepository.InsertAsync(groupSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Edit)]
        protected virtual async Task Update(CreateOrEditGroupSettingsDto input)
        {
            var groupSettings = await _groupSettingsRepository.FirstOrDefaultAsync(input.Id.Value);
            ObjectMapper.Map(input, groupSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _groupSettingsRepository.DeleteAsync(input.Id);
        }
    }
}