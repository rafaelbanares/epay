﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize(AppPermissions.Pages_Employees)]
    public class EmployeesAppService : ePayAppServiceBase, IEmployeesAppService
    {
        private readonly IRepository<Employees> _employeesRepository;

        public EmployeesAppService(IRepository<Employees> employeesRepository)
        {
            _employeesRepository = employeesRepository;

        }

        public IList<SelectListItem> GetEmployeeStatusList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "ACTIVE", Text = "ACTIVE" },
                new SelectListItem { Value = "RESIGNED", Text = "RESIGNED" },
                new SelectListItem { Value = "HOLD", Text = "HOLD" }
            };
        }

        public IList<SelectListItem> GetCurrencyList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "PHP", Text = "PHP" },
                new SelectListItem { Value = "USD", Text = "USD"}
            };
        }

        public async Task<IList<SelectListItem>> GetEmployeeUserList()
        {
            var employees = await _employeesRepository.GetAll()
                .Where(e => e.User != null)
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = o.FullName
                }).ToListAsync();

            employees.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            return employees;
        }

        public async Task<IList<SelectListItem>> GetEmployeeList()
        {
            var employees = await _employeesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = string.Format("{0} | {1}", o.EmployeeCode, o.FullName)
                }).ToListAsync();

            employees.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            return employees;
        }

        public async Task<IList<SelectListItem>> GetEmployeeFilterList()
        {
            var employees = await _employeesRepository.GetAll()
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = o.FullName
                }).ToListAsync();

            employees.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "All Employees"
            });

            return employees;
        }

        public async Task<PagedResultDto<GetEmployeesForViewDto>> GetAll(GetAllEmployeesInput input)
        {
            var filteredEmployees = _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                    (e.EmployeeCode + " | " + e.LastName + ", " + e.FirstName + " " + e.MiddleName).Contains(input.Filter));

            var pagedAndFilteredEmployees = filteredEmployees
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var employees = from o in pagedAndFilteredEmployees
                            select new GetEmployeesForViewDto()
                            {
                                Employees = new EmployeesDto
                                {
                                    EmployeeId = o.EmployeeCode,
                                    FullName = o.FullName,
                                    Email = o.Email,
                                    Address = o.Address,
                                    Position = o.Position,
                                    DateOfBirth = o.DateOfBirth,
                                    DateEmployed = o.DateEmployed,
                                    DateTerminated = o.DateTerminated,
                                    Gender = o.Gender,
                                    EmployeeStatus = o.EmployeeStatus,
                                    EmploymentType = o.EmploymentTypes.DisplayName,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredEmployees.CountAsync();

            return new PagedResultDto<GetEmployeesForViewDto>(
                totalCount,
                await employees.ToListAsync()
            );
        }

        public async Task<GetEmployeesForViewDto> GetEmployeesForView(int id)
        {
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.EmployeeCode,
                    e.FullName,
                    e.Email,
                    e.Address,
                    e.Position,
                    e.DateOfBirth,
                    e.DateEmployed,
                    e.DateTerminated,
                    e.Gender,
                    e.EmployeeStatus,
                    EmploymentType = e.EmploymentTypes.DisplayName
                }).FirstOrDefaultAsync();

            var output = new GetEmployeesForViewDto
            {
                Employees = new EmployeesDto
                {
                    EmployeeId = employee.EmployeeCode,
                    FullName = employee.FullName,
                    Email = employee.Email,
                    Address = employee.Address,
                    Position = employee.Position,
                    DateOfBirth = employee.DateOfBirth,
                    DateEmployed = employee.DateEmployed,
                    DateTerminated = employee.DateTerminated,
                    Gender = employee.Gender,
                    EmployeeStatus = employee.EmployeeStatus,
                    EmploymentType = employee.EmploymentType
                }
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Edit)]
        public async Task<GetEmployeesForEditOutput> GetEmployeesForEdit(EntityDto input)
        {
            var employees = await _employeesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEmployeesForEditOutput { Employees = ObjectMapper.Map<CreateOrEditEmployeesDto>(employees) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmployeesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Create)]
        protected virtual async Task Create(CreateOrEditEmployeesDto input)
        {
            var employees = ObjectMapper.Map<Employees>(input);
            employees.EmployeeStatus = "ACTIVE";

            await _employeesRepository.InsertAsync(employees);
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Edit)]
        protected virtual async Task Update(CreateOrEditEmployeesDto input)
        {
            var employees = await _employeesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, employees);
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _employeesRepository.DeleteAsync(input.Id);
        }
    }
}