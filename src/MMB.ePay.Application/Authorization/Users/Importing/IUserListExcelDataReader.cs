﻿using System.Collections.Generic;
using MMB.ePay.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace MMB.ePay.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}
