﻿using System.Collections.Generic;
using MMB.ePay.Authorization.Users.Importing.Dto;
using MMB.ePay.Dto;

namespace MMB.ePay.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
