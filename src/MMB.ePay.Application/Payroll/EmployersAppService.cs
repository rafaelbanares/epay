﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Employers)]
    public class EmployersAppService : ePayAppServiceBase, IEmployersAppService
    {
        private readonly IRepository<Employers> _employersRepository;

        public EmployersAppService(IRepository<Employers> employersRepository)
        {
            _employersRepository = employersRepository;
        }

        public async Task<PagedResultDto<GetEmployersForViewDto>> GetAll(GetAllEmployersInput input)
        {

            var filteredEmployers = _employersRepository.GetAll()
                .Where(e => input.EmployeeFilterId == e.EmployeeId);;

            var pagedAndFilteredEmployers = filteredEmployers
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var employers = from o in pagedAndFilteredEmployers
                            select new GetEmployersForViewDto()
                            {
                                Employers = new EmployersDto
                                {
                                    EmployerName = o.EmployerName,
                                    EmployedFrom = o.EmployedFrom,
                                    EmployedTo = o.EmployedTo,
                                    TIN = o.TIN,
                                    Address1 = o.Address1,
                                    Address2 = o.Address2,
                                    BasicPay = o.BasicPay,
                                    Deminimis = o.Deminimis,
                                    GrossPay = o.GrossPay,
                                    TxBonus = o.TxBonus,
                                    TxOtherIncome = o.TxOtherIncome,
                                    //NtxBonus = o.NtxBonus,
                                    //NtxOtherIncome = o.NtxOtherIncome,
                                    //WTax = o.WTax,
                                    SSS = o.SSS,
                                    Philhealth = o.Philhealth,
                                    Pagibig = o.Pagibig,
                                    EmployeeId = o.EmployeeId,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredEmployers.CountAsync();

            return new PagedResultDto<GetEmployersForViewDto>(
                totalCount,
                await employers.ToListAsync()
            );
        }

        public async Task<GetEmployersForViewDto> GetEmployersForView(int id)
        {
            var employers = await _employersRepository.GetAsync(id);

            var output = new GetEmployersForViewDto { Employers = ObjectMapper.Map<EmployersDto>(employers) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Employers_Edit)]
        public async Task<GetEmployersForEditOutput> GetEmployersForEdit(EntityDto input)
        {
            var employers = await _employersRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEmployersForEditOutput { Employers = ObjectMapper.Map<CreateOrEditEmployersDto>(employers) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmployersDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Employers_Create)]
        protected virtual async Task Create(CreateOrEditEmployersDto input)
        {
            var employers = ObjectMapper.Map<Employers>(input);
            await _employersRepository.InsertAsync(employers);
        }

        [AbpAuthorize(AppPermissions.Pages_Employers_Edit)]
        protected virtual async Task Update(CreateOrEditEmployersDto input)
        {
            var employers = await _employersRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, employers);
        }

        [AbpAuthorize(AppPermissions.Pages_Employers_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _employersRepository.DeleteAsync(input.Id);
        }
    }
}