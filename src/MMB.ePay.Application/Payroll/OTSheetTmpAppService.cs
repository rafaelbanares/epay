﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OTSheetTmp)]
    public class OTSheetTmpAppService : ePayAppServiceBase, IOTSheetTmpAppService
    {
        private readonly IRepository<OTSheetTmp> _otSheetTmpRepository;

        public OTSheetTmpAppService(IRepository<OTSheetTmp> otSheetTmpRepository)
        {
            _otSheetTmpRepository = otSheetTmpRepository;
        }

        public async Task<PagedResultDto<GetOTSheetTmpForViewDto>> GetAll(GetAllOTSheetTmpInput input)
        {

            var filteredOTSheetTmp = _otSheetTmpRepository.GetAll()
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.OvertimeTypeFilter.HasValue, e => e.OvertimeTypeId == input.OvertimeTypeFilter)
                        .WhereIf(input.MinCostCenterFilter != null, e => e.CostCenter >= input.MinCostCenterFilter)
                        .WhereIf(input.MaxCostCenterFilter != null, e => e.CostCenter <= input.MaxCostCenterFilter)
                        .WhereIf(input.MinHrsFilter != null, e => e.Hrs >= input.MinHrsFilter)
                        .WhereIf(input.MaxHrsFilter != null, e => e.Hrs <= input.MaxHrsFilter)
                        .WhereIf(input.MinMinsFilter != null, e => e.Mins >= input.MinMinsFilter)
                        .WhereIf(input.MaxMinsFilter != null, e => e.Mins <= input.MaxMinsFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredOTSheetTmp = filteredOTSheetTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var otSheetTmp = from o in pagedAndFilteredOTSheetTmp
                             select new GetOTSheetTmpForViewDto()
                             {
                                 OTSheetTmp = new OTSheetTmpDto
                                 {
                                     PayrollNo = o.PayrollNo,
                                     OvertimeType = o.OvertimeTypes.DisplayName,
                                     CostCenter = o.CostCenter,
                                     Hrs = o.Hrs,
                                     Mins = o.Mins,
                                     EmployeeId = o.EmployeeId,
                                     SessionId = o.SessionId,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredOTSheetTmp.CountAsync();

            return new PagedResultDto<GetOTSheetTmpForViewDto>(
                totalCount,
                await otSheetTmp.ToListAsync()
            );
        }

        public async Task<GetOTSheetTmpForViewDto> GetOTSheetTmpForView(int id)
        {
            var otSheetTmp = await _otSheetTmpRepository.GetAsync(id);

            var output = new GetOTSheetTmpForViewDto { OTSheetTmp = ObjectMapper.Map<OTSheetTmpDto>(otSheetTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetTmp_Edit)]
        public async Task<GetOTSheetTmpForEditOutput> GetOTSheetTmpForEdit(EntityDto input)
        {
            var otSheetTmp = await _otSheetTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOTSheetTmpForEditOutput { OTSheetTmp = ObjectMapper.Map<CreateOrEditOTSheetTmpDto>(otSheetTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditOTSheetTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetTmp_Create)]
        protected virtual async Task Create(CreateOrEditOTSheetTmpDto input)
        {
            var otSheetTmp = ObjectMapper.Map<OTSheetTmp>(input);
            await _otSheetTmpRepository.InsertAsync(otSheetTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetTmp_Edit)]
        protected virtual async Task Update(CreateOrEditOTSheetTmpDto input)
        {
            var otSheetTmp = await _otSheetTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, otSheetTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _otSheetTmpRepository.DeleteAsync(input.Id);
        }
    }
}