﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_DeductionProcess)]
    public class DeductionProcessAppService : ePayAppServiceBase, IDeductionProcessAppService
    {
        private readonly IRepository<DeductionProcess> _deductionProcessRepository;

        public DeductionProcessAppService(IRepository<DeductionProcess> deductionProcessRepository)
        {
            _deductionProcessRepository = deductionProcessRepository;
        }

        public async Task<PagedResultDto<GetDeductionProcessForViewDto>> GetAll(GetAllDeductionProcessInput input)
        {

            var filteredDeductionProcess = _deductionProcessRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinDeductionTypeIdFilter != null, e => e.DeductionTypeId >= input.MinDeductionTypeIdFilter)
                        .WhereIf(input.MaxDeductionTypeIdFilter != null, e => e.DeductionTypeId <= input.MaxDeductionTypeIdFilter)
                        .WhereIf(input.MinRecurDeductionIdFilter != null, e => e.RecurDeductionId >= input.MinRecurDeductionIdFilter)
                        .WhereIf(input.MaxRecurDeductionIdFilter != null, e => e.RecurDeductionId <= input.MaxRecurDeductionIdFilter);

            var pagedAndFilteredDeductionProcess = filteredDeductionProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var deductionProcess = from o in pagedAndFilteredDeductionProcess
                                   select new GetDeductionProcessForViewDto()
                                   {
                                       DeductionProcess = new DeductionProcessDto
                                       {
                                           PayrollNo = o.PayrollProcess.PayrollNo,
                                           Amount = o.Amount,
                                           EmployeeId = o.PayrollProcess.EmployeeId,
                                           DeductionTypeId = o.DeductionTypeId,
                                           RecurDeductionId = o.RecurDeductionId,
                                           Id = o.Id
                                       }
                                   };

            var totalCount = await filteredDeductionProcess.CountAsync();

            return new PagedResultDto<GetDeductionProcessForViewDto>(
                totalCount,
                await deductionProcess.ToListAsync()
            );
        }

        public async Task<GetDeductionProcessForViewDto> GetDeductionProcessForView(int id)
        {
            var deductionProcess = await _deductionProcessRepository.GetAsync(id);

            var output = new GetDeductionProcessForViewDto { DeductionProcess = ObjectMapper.Map<DeductionProcessDto>(deductionProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionProcess_Edit)]
        public async Task<GetDeductionProcessForEditOutput> GetDeductionProcessForEdit(EntityDto input)
        {
            var deductionProcess = await _deductionProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDeductionProcessForEditOutput { DeductionProcess = ObjectMapper.Map<CreateOrEditDeductionProcessDto>(deductionProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDeductionProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionProcess_Create)]
        protected virtual async Task Create(CreateOrEditDeductionProcessDto input)
        {
            var deductionProcess = ObjectMapper.Map<DeductionProcess>(input);
            await _deductionProcessRepository.InsertAsync(deductionProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionProcess_Edit)]
        protected virtual async Task Update(CreateOrEditDeductionProcessDto input)
        {
            var deductionProcess = await _deductionProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, deductionProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _deductionProcessRepository.DeleteAsync(input.Id);
        }
    }
}