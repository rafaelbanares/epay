﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EarningPosted)]
    public class EarningPostedAppService : ePayAppServiceBase, IEarningPostedAppService
    {
        private readonly IRepository<EarningPosted> _earningPostedRepository;

        public EarningPostedAppService(IRepository<EarningPosted> earningPostedRepository)
        {
            _earningPostedRepository = earningPostedRepository;
        }

        public async Task<PagedResultDto<GetEarningPostedForViewDto>> GetAll(GetAllEarningPostedInput input)
        {

            var filteredEarningPosted = _earningPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollPosted.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollPosted.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollPosted.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollPosted.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinEarningTypeIdFilter != null, e => e.EarningTypeId >= input.MinEarningTypeIdFilter)
                        .WhereIf(input.MaxEarningTypeIdFilter != null, e => e.EarningTypeId <= input.MaxEarningTypeIdFilter);

            var pagedAndFilteredEarningPosted = filteredEarningPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var earningPosted = from o in pagedAndFilteredEarningPosted
                                select new GetEarningPostedForViewDto()
                                {
                                    EarningPosted = new EarningPostedDto
                                    {
                                        PayrollNo = o.PayrollPosted.PayrollNo,
                                        Amount = o.Amount,
                                        EmployeeId = o.PayrollPosted.EmployeeId,
                                        EarningTypeId = o.EarningTypeId,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredEarningPosted.CountAsync();

            return new PagedResultDto<GetEarningPostedForViewDto>(
                totalCount,
                await earningPosted.ToListAsync()
            );
        }

        public async Task<GetEarningPostedForViewDto> GetEarningPostedForView(int id)
        {
            var earningPosted = await _earningPostedRepository.GetAsync(id);

            var output = new GetEarningPostedForViewDto { EarningPosted = ObjectMapper.Map<EarningPostedDto>(earningPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EarningPosted_Edit)]
        public async Task<GetEarningPostedForEditOutput> GetEarningPostedForEdit(EntityDto input)
        {
            var earningPosted = await _earningPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEarningPostedForEditOutput { EarningPosted = ObjectMapper.Map<CreateOrEditEarningPostedDto>(earningPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEarningPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EarningPosted_Create)]
        protected virtual async Task Create(CreateOrEditEarningPostedDto input)
        {
            var earningPosted = ObjectMapper.Map<EarningPosted>(input);

            await _earningPostedRepository.InsertAsync(earningPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningPosted_Edit)]
        protected virtual async Task Update(CreateOrEditEarningPostedDto input)
        {
            var earningPosted = await _earningPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, earningPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _earningPostedRepository.DeleteAsync(input.Id);
        }
    }
}