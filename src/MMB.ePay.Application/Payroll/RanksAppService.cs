﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Ranks)]
    public class RanksAppService : ePayAppServiceBase, IRanksAppService
    {
        private readonly IRepository<Ranks> _ranksRepository;

        public RanksAppService(IRepository<Ranks> ranksRepository)
        {
            _ranksRepository = ranksRepository;
        }

        public async Task<IList<SelectListItem>> GetRankList()
        {
            var ranks = await _ranksRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            ranks.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return ranks;
        }


        public async Task<PagedResultDto<GetRanksForViewDto>> GetAll(GetAllRanksInput input)
        {

            var filteredRanks = _ranksRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter);

            var pagedAndFilteredRanks = filteredRanks
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var ranks = from o in pagedAndFilteredRanks
                        select new GetRanksForViewDto()
                        {
                            Ranks = new RanksDto
                            {
                                Description = o.Description,
                                Id = o.Id
                            }
                        };

            var totalCount = await filteredRanks.CountAsync();

            return new PagedResultDto<GetRanksForViewDto>(
                totalCount,
                await ranks.ToListAsync()
            );
        }

        public async Task<GetRanksForViewDto> GetRanksForView(int id)
        {
            var ranks = await _ranksRepository.GetAsync(id);

            var output = new GetRanksForViewDto { Ranks = ObjectMapper.Map<RanksDto>(ranks) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Ranks_Edit)]
        public async Task<GetRanksForEditOutput> GetRanksForEdit(EntityDto input)
        {
            var ranks = await _ranksRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRanksForEditOutput { Ranks = ObjectMapper.Map<CreateOrEditRanksDto>(ranks) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditRanksDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Ranks_Create)]
        protected virtual async Task Create(CreateOrEditRanksDto input)
        {
            var ranks = ObjectMapper.Map<Ranks>(input);

            await _ranksRepository.InsertAsync(ranks);
        }

        [AbpAuthorize(AppPermissions.Pages_Ranks_Edit)]
        protected virtual async Task Update(CreateOrEditRanksDto input)
        {
            var ranks = await _ranksRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, ranks);
        }

        [AbpAuthorize(AppPermissions.Pages_Ranks_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _ranksRepository.DeleteAsync(input.Id);
        }
    }
}