﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EarningTmp)]
    public class EarningTmpAppService : ePayAppServiceBase, IEarningTmpAppService
    {
        private readonly IRepository<EarningTmp> _earningTmpRepository;

        public EarningTmpAppService(IRepository<EarningTmp> earningTmpRepository)
        {
            _earningTmpRepository = earningTmpRepository;
        }

        public async Task<PagedResultDto<GetEarningTmpForViewDto>> GetAll(GetAllEarningTmpInput input)
        {

            var filteredEarningTmp = _earningTmpRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinEarningTypeIdFilter != null, e => e.EarningTypeId >= input.MinEarningTypeIdFilter)
                        .WhereIf(input.MaxEarningTypeIdFilter != null, e => e.EarningTypeId <= input.MaxEarningTypeIdFilter)
                        .WhereIf(input.MinRecurEarningIdFilter != null, e => e.RecurEarningId >= input.MinRecurEarningIdFilter)
                        .WhereIf(input.MaxRecurEarningIdFilter != null, e => e.RecurEarningId <= input.MaxRecurEarningIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredEarningTmp = filteredEarningTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var earningTmp = from o in pagedAndFilteredEarningTmp
                             select new GetEarningTmpForViewDto()
                             {
                                 EarningTmp = new EarningTmpDto
                                 {
                                     PayrollNo = o.PayrollNo,
                                     Amount = o.Amount,
                                     EmployeeId = o.EmployeeId,
                                     EarningTypeId = o.EarningTypeId,
                                     RecurEarningId = o.RecurEarningId,
                                     SessionId = o.SessionId,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredEarningTmp.CountAsync();

            return new PagedResultDto<GetEarningTmpForViewDto>(
                totalCount,
                await earningTmp.ToListAsync()
            );
        }

        public async Task<GetEarningTmpForViewDto> GetEarningTmpForView(int id)
        {
            var earningTmp = await _earningTmpRepository.GetAsync(id);

            var output = new GetEarningTmpForViewDto { EarningTmp = ObjectMapper.Map<EarningTmpDto>(earningTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTmp_Edit)]
        public async Task<GetEarningTmpForEditOutput> GetEarningTmpForEdit(EntityDto input)
        {
            var earningTmp = await _earningTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEarningTmpForEditOutput { EarningTmp = ObjectMapper.Map<CreateOrEditEarningTmpDto>(earningTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEarningTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTmp_Create)]
        protected virtual async Task Create(CreateOrEditEarningTmpDto input)
        {
            var earningTmp = ObjectMapper.Map<EarningTmp>(input);
            await _earningTmpRepository.InsertAsync(earningTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTmp_Edit)]
        protected virtual async Task Update(CreateOrEditEarningTmpDto input)
        {
            var earningTmp = await _earningTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, earningTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _earningTmpRepository.DeleteAsync(input.Id);
        }
    }
}