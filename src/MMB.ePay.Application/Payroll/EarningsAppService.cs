﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Earnings)]
    public class EarningsAppService : ePayAppServiceBase, IEarningsAppService
    {
        private readonly IRepository<Earnings> _earningsRepository;
        private readonly IRepository<PayPeriod> _payPeriodRepository;

        public EarningsAppService(IRepository<Earnings> earningsRepository, IRepository<PayPeriod> payPeriodRepository)
        {
            _earningsRepository = earningsRepository;
            _payPeriodRepository = payPeriodRepository;
        }

        public async Task<PagedResultDto<GetEarningsForViewDto>> GetAll(GetAllEarningsInput input)
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var filteredEarnings = _earningsRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeIdFilter && e.Process.PayrollNo == currentPayrollNo);

            var pagedAndFilteredEarnings = filteredEarnings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var earnings = from o in pagedAndFilteredEarnings
                           select new GetEarningsForViewDto()
                           {
                               Earnings = new EarningsDto
                               {
                                   PayrollNo = o.Process.PayrollNo,
                                   Amount = o.Amount,
                                   EmployeeId = o.EmployeeId,
                                   EarningTypeId = o.EarningTypeId,
                                   EarningType = o.EarningTypes.DisplayName,
                                   Id = o.Id
                               }
                           };

            var totalCount = await filteredEarnings.CountAsync();

            return new PagedResultDto<GetEarningsForViewDto>(
                totalCount,
                await earnings.ToListAsync()
            );
        }

        public async Task<GetEarningsForViewDto> GetEarningsForView(int id)
        {
            var earnings = await _earningsRepository.GetAsync(id);

            var output = new GetEarningsForViewDto { Earnings = ObjectMapper.Map<EarningsDto>(earnings) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Earnings_Edit)]
        public async Task<GetEarningsForEditOutput> GetEarningsForEdit(EntityDto input)
        {
            var earnings = await _earningsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEarningsForEditOutput { Earnings = ObjectMapper.Map<CreateOrEditEarningsDto>(earnings) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEarningsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Earnings_Create)]
        protected virtual async Task Create(CreateOrEditEarningsDto input)
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            input.PayrollNo = currentPayrollNo;

            var earnings = ObjectMapper.Map<Earnings>(input);
            await _earningsRepository.InsertAsync(earnings);
        }

        [AbpAuthorize(AppPermissions.Pages_Earnings_Edit)]
        protected virtual async Task Update(CreateOrEditEarningsDto input)
        {
            var earnings = await _earningsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, earnings);
        }

        [AbpAuthorize(AppPermissions.Pages_Earnings_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _earningsRepository.DeleteAsync(input.Id);
        }
    }
}