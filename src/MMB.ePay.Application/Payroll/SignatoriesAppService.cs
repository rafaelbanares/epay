﻿using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    public class SignatoriesAppService : ePayAppServiceBase, ISignatoriesAppService
    {
        private readonly IRepository<Signatories> _signatoriesRepository;

        public SignatoriesAppService(IRepository<Signatories> signatoriesRepository)
        {
            _signatoriesRepository = signatoriesRepository;
        }

        public async Task<IList<SelectListItem>> GetSignatoryList()
        {
            var signatories = await _signatoriesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.SignatoryName
                }).ToListAsync();

            signatories.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return signatories;
        }
    }
}