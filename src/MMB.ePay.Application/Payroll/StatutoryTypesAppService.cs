﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_StatutoryTypes)]
    public class StatutoryTypesAppService : ePayAppServiceBase, IStatutoryTypesAppService
    {
        private readonly IRepository<StatutoryTypes> _statutoryTypesRepository;

        public StatutoryTypesAppService(IRepository<StatutoryTypes> statutoryTypesRepository)
        {
            _statutoryTypesRepository = statutoryTypesRepository;
        }

        public async Task<IList<SelectListItem>> GetStatutoryTypeList()
        {
            var statutoryTypes = await _statutoryTypesRepository.GetAll()
                .OrderBy(e => e.Code)
                .Select(e => new SelectListItem
                {
                    Value = e.Code,
                    Text = e.Description
                }).ToListAsync();

            statutoryTypes.Insert(0, new SelectListItem { Value = "", Text = "Select" });

            return statutoryTypes;
        }

        public async Task<IList<string>> GetStatutoryTypeCodeList()
        {
            var statutoryTypes = await _statutoryTypesRepository.GetAll()
                .OrderBy(e => e.Code)
                .Select(e => e.Code)
                .ToListAsync();

            return statutoryTypes;
        }

        public async Task<PagedResultDto<GetStatutoryTypesForViewDto>> GetAll(GetAllStatutoryTypesInput input)
        {

            var filteredStatutoryTypes = _statutoryTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code == input.CodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter);

            var pagedAndFilteredStatutoryTypes = filteredStatutoryTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var statutoryTypes = from o in pagedAndFilteredStatutoryTypes
                                 select new GetStatutoryTypesForViewDto()
                                 {
                                     StatutoryTypes = new StatutoryTypesDto
                                     {
                                         Code = o.Code,
                                         Description = o.Description,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredStatutoryTypes.CountAsync();

            return new PagedResultDto<GetStatutoryTypesForViewDto>(
                totalCount,
                await statutoryTypes.ToListAsync()
            );
        }

        public async Task<GetStatutoryTypesForViewDto> GetStatutoryTypesForView(int id)
        {
            var statutoryTypes = await _statutoryTypesRepository.GetAsync(id);

            var output = new GetStatutoryTypesForViewDto { StatutoryTypes = ObjectMapper.Map<StatutoryTypesDto>(statutoryTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTypes_Edit)]
        public async Task<GetStatutoryTypesForEditOutput> GetStatutoryTypesForEdit(EntityDto input)
        {
            var statutoryTypes = await _statutoryTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStatutoryTypesForEditOutput { StatutoryTypes = ObjectMapper.Map<CreateOrEditStatutoryTypesDto>(statutoryTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditStatutoryTypesDto input)
        {
            if (!input.Id.HasValue)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTypes_Create)]
        protected virtual async Task Create(CreateOrEditStatutoryTypesDto input)
        {
            var statutoryTypes = ObjectMapper.Map<StatutoryTypes>(input);

            await _statutoryTypesRepository.InsertAsync(statutoryTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTypes_Edit)]
        protected virtual async Task Update(CreateOrEditStatutoryTypesDto input)
        {
            var statutoryTypes = await _statutoryTypesRepository.FirstOrDefaultAsync(input.Id.Value);
            ObjectMapper.Map(input, statutoryTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTypes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _statutoryTypesRepository.DeleteAsync(input.Id);
        }
    }
}