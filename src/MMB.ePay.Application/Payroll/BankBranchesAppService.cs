﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_BankBranches)]
    public class BankBranchesAppService : ePayAppServiceBase, IBankBranchesAppService
    {
        private readonly IRepository<BankBranches> _bankBranchesRepository;
        private readonly IRepository<Banks, string> _banksRepository;

        public BankBranchesAppService(IRepository<BankBranches> bankBranchesRepository, IRepository<Banks, string> banksRepository)
        {
            _bankBranchesRepository = bankBranchesRepository;
            _banksRepository = banksRepository;
        }

        public async Task<IList<SelectListItem>> GetBankBranchList(string bankId)
        {
            var bankBranches = await _bankBranchesRepository.GetAll()
                .WhereIf(!bankId.IsNullOrWhiteSpace(), e => e.BankId == bankId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.BranchName
                }).ToListAsync();

            bankBranches.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return bankBranches;
        }

        public async Task<IList<SelectListItem>> GetBranchList(string bankId)
        {
            var bankBranches = await _bankBranchesRepository.GetAll()
                .WhereIf(!bankId.IsNullOrWhiteSpace(), e => e.BankId == bankId)
                .Select(e => new SelectListItem
                {
                    Value = e.BranchCode,
                    Text = e.BranchName
                }).ToListAsync();

            bankBranches.Insert(0, new SelectListItem { Value = "", Text = "Select" });

            return bankBranches;
        }

        public async Task<PagedResultDto<GetBankBranchesForViewDto>> GetAll(GetAllBankBranchesInput input)
        {
            var filteredBankBranches = _bankBranchesRepository.GetAll()
                .Where(e => e.BankId == input.BankId);

            var pagedAndFilteredBankBranches = filteredBankBranches
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var bankBranches = from o in pagedAndFilteredBankBranches
                               select new GetBankBranchesForViewDto()
                               {
                                   BankBranches = new BankBranchesDto
                                   {
                                       BranchName = o.BranchName,
                                       Address1 = o.Address1,
                                       Address2 = o.Address2,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredBankBranches.CountAsync();

            return new PagedResultDto<GetBankBranchesForViewDto>(
                totalCount,
                await bankBranches.ToListAsync()
            );
        }

        public async Task<GetBankBranchesForViewDto> GetBankBranchesForView(int id)
        {
            var bankBranches = await _bankBranchesRepository.GetAsync(id);

            var output = new GetBankBranchesForViewDto { BankBranches = ObjectMapper.Map<BankBranchesDto>(bankBranches) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_BankBranches_Create)]
        public async Task<GetBankBranchesForEditOutput> GetBankBranchesForCreate(string bankId)
        {
            var banks = await _banksRepository.FirstOrDefaultAsync(bankId);

            var output = new GetBankBranchesForEditOutput
            {
                BankBranches = new CreateOrEditBankBranchesDto
                {
                    BankId = banks.Id,
                    DisplayName = banks.DisplayName
                }
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_BankBranches_Edit)]
        public async Task<GetBankBranchesForEditOutput> GetBankBranchesForEdit(EntityDto input)
        {
            var bankBranches = await _bankBranchesRepository.FirstOrDefaultAsync(input.Id);
            var banks = await _banksRepository.FirstOrDefaultAsync(bankBranches.BankId);

            var output = new GetBankBranchesForEditOutput { BankBranches = ObjectMapper.Map<CreateOrEditBankBranchesDto>(bankBranches) };
            output.BankBranches.DisplayName = banks.DisplayName;

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditBankBranchesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_BankBranches_Create)]
        protected virtual async Task Create(CreateOrEditBankBranchesDto input)
        {
            var bankBranches = ObjectMapper.Map<BankBranches>(input);
            await _bankBranchesRepository.InsertAsync(bankBranches);
        }

        [AbpAuthorize(AppPermissions.Pages_BankBranches_Edit)]
        protected virtual async Task Update(CreateOrEditBankBranchesDto input)
        {
            var bankBranches = await _bankBranchesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, bankBranches);
        }
    }
}