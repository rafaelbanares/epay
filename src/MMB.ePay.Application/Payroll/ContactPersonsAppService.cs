﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_ContactPersons)]
    public class ContactPersonsAppService : ePayAppServiceBase, IContactPersonsAppService
    {
        private readonly IRepository<ContactPersons> _contactPersonsRepository;
        public ContactPersonsAppService(IRepository<ContactPersons> contactPersonsRepository)
        {
            _contactPersonsRepository = contactPersonsRepository;
        }

        public async Task<PagedResultDto<GetContactPersonsForViewDto>> GetAll(GetAllContactPersonsInput input)
        {
            var filteredContactPersons = _contactPersonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId);

            var pagedAndFilteredContactPersons = filteredContactPersons
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var contactPersons = from o in pagedAndFilteredContactPersons
                               select new GetContactPersonsForViewDto()
                               {
                                   ContactPersons = new ContactPersonsDto
                                   {
                                       FullName = o.FullName,
                                       ContactNo = o.ContactNo,
                                       Position = o.Position,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredContactPersons.CountAsync();

            return new PagedResultDto<GetContactPersonsForViewDto>(
                totalCount,
                await contactPersons.ToListAsync()
            );
        }

        public async Task<GetContactPersonsForViewDto> GetContactPersonsForView(int id)
        {
            var output = await _contactPersonsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetContactPersonsForViewDto
                {
                    ContactPersons = new ContactPersonsDto
                    {
                        FullName = e.FullName,
                        ContactNo = e.ContactNo,
                        Position = e.Position
                    }
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ContactPersons_Edit)]
        public async Task<GetContactPersonsForEditOutput> GetContactPersonsForEdit(EntityDto input)
        {
            var contactPersons = await _contactPersonsRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetContactPersonsForEditOutput { ContactPersons = ObjectMapper.Map<CreateOrEditContactPersonsDto>(contactPersons) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditContactPersonsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ContactPersons_Create)]
        protected virtual async Task Create(CreateOrEditContactPersonsDto input)
        {
            var contactPersons = ObjectMapper.Map<ContactPersons>(input);
            contactPersons.OrderNo = (short)((await _contactPersonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .CountAsync()) + 1);

            await _contactPersonsRepository.InsertAsync(contactPersons);
        }

        [AbpAuthorize(AppPermissions.Pages_ContactPersons_Edit)]
        protected virtual async Task Update(CreateOrEditContactPersonsDto input)
        {
            var contactPersons = await _contactPersonsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, contactPersons);
        }
    }
}