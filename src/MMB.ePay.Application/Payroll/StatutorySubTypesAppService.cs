﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_StatutorySubTypes)]
    public class StatutorySubTypesAppService : ePayAppServiceBase, IStatutorySubTypesAppService
    {
        private readonly IRepository<StatutorySubTypes> _statutorySubTypesRepository;

        public StatutorySubTypesAppService(IRepository<StatutorySubTypes> statutorySubTypesRepository)
        {
            _statutorySubTypesRepository = statutorySubTypesRepository;
        }

        public async Task<PagedResultDto<GetStatutorySubTypesForViewDto>> GetAll(GetAllStatutorySubTypesInput input)
        {

            var filteredStatutorySubTypes = _statutorySubTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter);

            var pagedAndFilteredStatutorySubTypes = filteredStatutorySubTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var statutorySubTypes = from o in pagedAndFilteredStatutorySubTypes
                                 select new GetStatutorySubTypesForViewDto()
                                 {
                                     StatutorySubTypes = new StatutorySubTypesDto
                                     {
                                         DisplayName = o.DisplayName,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredStatutorySubTypes.CountAsync();

            return new PagedResultDto<GetStatutorySubTypesForViewDto>(
                totalCount,
                await statutorySubTypes.ToListAsync()
            );
        }

        public async Task<GetStatutorySubTypesForViewDto> GetStatutorySubTypesForView(int id)
        {
            var statutorySubTypes = await _statutorySubTypesRepository.GetAsync(id);

            var output = new GetStatutorySubTypesForViewDto { StatutorySubTypes = ObjectMapper.Map<StatutorySubTypesDto>(statutorySubTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StatutorySubTypes_Edit)]
        public async Task<GetStatutorySubTypesForEditOutput> GetStatutorySubTypesForEdit(EntityDto input)
        {
            var statutorySubTypes = await _statutorySubTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStatutorySubTypesForEditOutput { StatutorySubTypes = ObjectMapper.Map<CreateOrEditStatutorySubTypesDto>(statutorySubTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditStatutorySubTypesDto input)
        {
            if (!input.Id.HasValue)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StatutorySubTypes_Create)]
        protected virtual async Task Create(CreateOrEditStatutorySubTypesDto input)
        {
            var statutorySubTypes = ObjectMapper.Map<StatutorySubTypes>(input);

            await _statutorySubTypesRepository.InsertAsync(statutorySubTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutorySubTypes_Edit)]
        protected virtual async Task Update(CreateOrEditStatutorySubTypesDto input)
        {
            var statutorySubTypes = await _statutorySubTypesRepository.FirstOrDefaultAsync(input.Id.Value);
            ObjectMapper.Map(input, statutorySubTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutorySubTypes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _statutorySubTypesRepository.DeleteAsync(input.Id);
        }
    }
}