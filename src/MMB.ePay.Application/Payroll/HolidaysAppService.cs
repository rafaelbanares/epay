﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Holidays)]
    public class HolidaysAppService : ePayAppServiceBase, IHolidaysAppService
    {
        private readonly IRepository<Holidays> _holidaysRepository;

        public HolidaysAppService(IRepository<Holidays> holidaysRepository)
        {
            _holidaysRepository = holidaysRepository;
        }

        public async Task<PagedResultDto<GetHolidaysForViewDto>> GetAll(GetAllHolidaysInput input)
        {

            var filteredHolidays = _holidaysRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                        .WhereIf(input.MinDateFilter != null, e => e.Date >= input.MinDateFilter)
                        .WhereIf(input.MaxDateFilter != null, e => e.Date <= input.MaxDateFilter)
                        .WhereIf(input.HalfDayFilter.HasValue && input.HalfDayFilter > -1, e => (input.HalfDayFilter == 1 && e.HalfDay) || (input.HalfDayFilter == 0 && !e.HalfDay))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.HolidayTypeIdFilter), e => e.HolidayTypeId == input.HolidayTypeIdFilter);

            var pagedAndFilteredHolidays = filteredHolidays
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var holidays = from o in pagedAndFilteredHolidays
                           select new GetHolidaysForViewDto()
                           {
                               Holidays = new HolidaysDto
                               {
                                   DisplayName = o.DisplayName,
                                   Date = o.Date,
                                   HalfDay = o.HalfDay,
                                   HolidayType = o.HolidayTypes.DisplayName,
                                   Id = o.Id
                               }
                           };

            var totalCount = await filteredHolidays.CountAsync();

            return new PagedResultDto<GetHolidaysForViewDto>(
                totalCount,
                await holidays.ToListAsync()
            );
        }

        public async Task<GetHolidaysForViewDto> GetHolidaysForView(int id)
        {
            var holidays = await _holidaysRepository.GetAsync(id);

            var output = new GetHolidaysForViewDto { Holidays = ObjectMapper.Map<HolidaysDto>(holidays) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Edit)]
        public async Task<GetHolidaysForEditOutput> GetHolidaysForEdit(EntityDto input)
        {
            var holidays = await _holidaysRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetHolidaysForEditOutput { Holidays = ObjectMapper.Map<CreateOrEditHolidaysDto>(holidays) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditHolidaysDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Create)]
        protected virtual async Task Create(CreateOrEditHolidaysDto input)
        {
            var holidays = ObjectMapper.Map<Holidays>(input);
            await _holidaysRepository.InsertAsync(holidays);
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Edit)]
        protected virtual async Task Update(CreateOrEditHolidaysDto input)
        {
            var holidays = await _holidaysRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, holidays);
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _holidaysRepository.DeleteAsync(input.Id);
        }
    }
}