﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_RecurDeductions)]
    public class RecurDeductionsAppService : ePayAppServiceBase, IRecurDeductionsAppService
    {
        private readonly IRepository<RecurDeductions> _recurDeductionsRepository;
        private readonly IRepository<EmployeePayroll> _employeePayrollRepository;

        public RecurDeductionsAppService(IRepository<RecurDeductions> recurDeductionsRepository, IRepository<EmployeePayroll> employeePayrollRepository)
        {
            _recurDeductionsRepository = recurDeductionsRepository;
            _employeePayrollRepository = employeePayrollRepository;
        }

        public async Task<PagedResultDto<GetRecurDeductionsForViewDto>> GetAll(GetAllRecurDeductionsInput input)
        {
            var filteredRecurDeductions = _recurDeductionsRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeIdFilter);

            var pagedAndFilteredRecurDeductions = filteredRecurDeductions
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var recurDeductions = from o in pagedAndFilteredRecurDeductions
                                select new GetRecurDeductionsForViewDto()
                                {
                                    RecurDeductions = new RecurDeductionsDto
                                    {
                                        DeductionType = o.DeductionTypes.DisplayName,
                                        Amount = o.Amount,
                                        StartDate = o.StartDate,
                                        EndDate = o.EndDate,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredRecurDeductions.CountAsync();

            return new PagedResultDto<GetRecurDeductionsForViewDto>(
                totalCount,
                await recurDeductions.ToListAsync()
            );
        }

        public async Task<GetRecurDeductionsForViewDto> GetRecurDeductionsForView(int id)
        {
            var recurDeductions = await _recurDeductionsRepository.GetAsync(id);

            var output = new GetRecurDeductionsForViewDto { RecurDeductions = ObjectMapper.Map<RecurDeductionsDto>(recurDeductions) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_RecurDeductions_Create)]
        public async Task<GetRecurDeductionsForEditOutput> GetRecurDeductionsForCreate(int employeeId)
        {
            var payFreq = await _employeePayrollRepository.GetAll()
                .Where(e => e.EmployeeId == employeeId)
                .Select(e => e.PayCodes.PayFreq)
                .FirstOrDefaultAsync();

            return new GetRecurDeductionsForEditOutput
            {
                RecurDeductions = new CreateOrEditRecurDeductionsDto
                {
                    EmployeeId = employeeId,
                    PayFreq = payFreq
                }
            };
        }

        [AbpAuthorize(AppPermissions.Pages_RecurDeductions_Edit)]
        public async Task<GetRecurDeductionsForEditOutput> GetRecurDeductionsForEdit(EntityDto input)
        {
            var recurDeductions = await _recurDeductionsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRecurDeductionsForEditOutput { RecurDeductions = ObjectMapper.Map<CreateOrEditRecurDeductionsDto>(recurDeductions) };
            var freq = output.RecurDeductions.Frequency.ToCharArray();
            output.RecurDeductions.FirstPeriod = freq.Contains('1');
            output.RecurDeductions.SecondPeriod = freq.Contains('2');
            output.RecurDeductions.ThirdPeriod = freq.Contains('3');
            output.RecurDeductions.FourthPeriod = freq.Contains('4');
            output.RecurDeductions.LastPeriod = freq.Contains('5');

            output.RecurDeductions.PayFreq = await _employeePayrollRepository.GetAll()
                .Where(e => e.EmployeeId == recurDeductions.EmployeeId)
                .Select(e => e.PayCodes.PayFreq)
                .FirstOrDefaultAsync();

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditRecurDeductionsDto input)
        {
            input.Frequency += input.FirstPeriod ? "1" : string.Empty;
            input.Frequency += input.SecondPeriod ? "2" : string.Empty;
            input.Frequency += input.ThirdPeriod ? "3" : string.Empty;
            input.Frequency += input.FourthPeriod ? "4" : string.Empty;
            input.Frequency += input.LastPeriod ? "5" : string.Empty;

            if (string.IsNullOrWhiteSpace(input.Frequency))
            {
                throw new UserFriendlyException("Your request is not valid!", "The Frequency field is required.");
            }

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_RecurDeductions_Create)]
        protected virtual async Task Create(CreateOrEditRecurDeductionsDto input)
        {
            var recurDeductions = ObjectMapper.Map<RecurDeductions>(input);
            await _recurDeductionsRepository.InsertAsync(recurDeductions);
        }

        [AbpAuthorize(AppPermissions.Pages_RecurDeductions_Edit)]
        protected virtual async Task Update(CreateOrEditRecurDeductionsDto input)
        {
            var recurDeductions = await _recurDeductionsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, recurDeductions);
        }

        [AbpAuthorize(AppPermissions.Pages_RecurDeductions_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _recurDeductionsRepository.DeleteAsync(input.Id);
        }
    }
}