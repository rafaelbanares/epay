﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_PayCodes)]
    public class PayCodesAppService : ePayAppServiceBase, IPayCodesAppService
    {
        private readonly IRepository<PayCodes, Guid> _payCodesRepository;
        private readonly IHelperAppService _helperAppService;

        public PayCodesAppService(IRepository<PayCodes, Guid> payCodesRepository, IHelperAppService helperAppService)
        {
            _payCodesRepository = payCodesRepository;
            _helperAppService = helperAppService;
        }

        public async Task<IList<SelectListItem>> GetPayCodeList()
        {
            var payCodes = await _payCodesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            payCodes.Insert(0, new SelectListItem { Value = string.Empty, Text = "Select" });

            return payCodes;
        }

        public async Task<PagedResultDto<GetPayCodesForViewDto>> GetAll(GetAllPayCodesInput input)
        {

            var filteredPayCodes = _payCodesRepository.GetAll();

            var pagedAndFilteredPayCodes = filteredPayCodes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var payCodes = await pagedAndFilteredPayCodes
                .Select(o => new
                {
                    o.DisplayName,
                    o.Description,
                    o.PayFreq,
                    o.TaxFreq,
                    o.TaxMethod,
                    o.Nd1Rate,
                    o.Nd2Rate,
                    o.Id
                }).ToListAsync();

            var output = payCodes
                .Select(o => new GetPayCodesForViewDto
                {
                    PayCodes = new PayCodesDto
                    {
                        DisplayName = o.DisplayName,
                        Description = o.Description,
                        PayFreq = _helperAppService.GetFrequencyByCode(o.PayFreq),
                        TaxFreq = _helperAppService.GetTaxFrequencyByCode(o.TaxFreq),
                        TaxMethod = _helperAppService.GetTaxMethodByCode(o.TaxMethod),
                        Nd1Rate = o.Nd1Rate,
                        Nd2Rate = o.Nd2Rate,
                        Id = o.Id
                    }
                }).ToList();

            var totalCount = await filteredPayCodes.CountAsync();

            return new PagedResultDto<GetPayCodesForViewDto>(
                totalCount,
                output
            );
        }

        public async Task<GetPayCodesForViewDto> GetPayCodesForView(Guid id)
        {
            var payCodes = await _payCodesRepository.GetAsync(id);

            var output = new GetPayCodesForViewDto { PayCodes = ObjectMapper.Map<PayCodesDto>(payCodes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PayCodes_Edit)]
        public async Task<GetPayCodesForEditOutput> GetPayCodesForEdit(EntityDto<Guid> input)
        {
            var payCodes = await _payCodesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayCodesForEditOutput { PayCodes = ObjectMapper.Map<CreateOrEditPayCodesDto>(payCodes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayCodesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PayCodes_Create)]
        protected virtual async Task Create(CreateOrEditPayCodesDto input)
        {
            var payCodes = ObjectMapper.Map<PayCodes>(input);
            await _payCodesRepository.InsertAsync(payCodes);
        }

        [AbpAuthorize(AppPermissions.Pages_PayCodes_Edit)]
        protected virtual async Task Update(CreateOrEditPayCodesDto input)
        {
            var payCodes = await _payCodesRepository.FirstOrDefaultAsync((Guid)input.Id);
            ObjectMapper.Map(input, payCodes);
        }
    }
}