﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Banks)]
    public class BanksAppService : ePayAppServiceBase, IBanksAppService
    {
        private readonly IRepository<Banks, string> _banksRepository;

        public BanksAppService(IRepository<Banks, string> banksRepository)
        {
            _banksRepository = banksRepository;
        }

        public async Task<IList<SelectListItem>> GetBankList()
        {
            var banks = await _banksRepository.GetAll()
                .Select(e => new SelectListItem
                {
                    Value = e.Id,
                    Text = e.DisplayName,
                }).ToListAsync();

            banks.Insert(0, new SelectListItem { Value = string.Empty, Text = "Select" });

            return banks;
        }

        public IList<SelectListItem> GetBankAccountTypeList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "CA", Text = "CA" },
                new SelectListItem { Value = "SA", Text = "SA" }
            };
        }

        public async Task<PagedResultDto<GetBanksForViewDto>> GetAll(GetAllBanksInput input)
        {
            var filteredBanks = _banksRepository.GetAll(); ;

            var pagedAndFilteredBanks = filteredBanks
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var banks = from o in pagedAndFilteredBanks
                        select new GetBanksForViewDto()
                        {
                            Banks = new BanksDto
                            {
                                DisplayName = o.DisplayName,
                                Description = o.Description,
                                Id = o.Id
                            }
                        };

            var totalCount = await filteredBanks.CountAsync();

            return new PagedResultDto<GetBanksForViewDto>(
                totalCount,
                await banks.ToListAsync()
            );
        }

        public async Task<GetBanksForViewDto> GetBanksForView(string id)
        {
            var banks = await _banksRepository.GetAsync(id);

            var output = new GetBanksForViewDto { Banks = ObjectMapper.Map<BanksDto>(banks) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Banks_Edit)]
        public async Task<GetBanksForEditOutput> GetBanksForEdit(EntityDto<string> input)
        {
            var banks = await _banksRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetBanksForEditOutput { Banks = ObjectMapper.Map<CreateOrEditBanksDto>(banks) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditBanksDto input)
        {
            var hasBank = await _banksRepository.GetAll().AnyAsync(e => e.Id == input.Id);

            if (!hasBank)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Banks_Create)]
        protected virtual async Task Create(CreateOrEditBanksDto input)
        {
            var banks = ObjectMapper.Map<Banks>(input);

            await _banksRepository.InsertAsync(banks);
        }

        [AbpAuthorize(AppPermissions.Pages_Banks_Edit)]
        protected virtual async Task Update(CreateOrEditBanksDto input)
        {
            var banks = await _banksRepository.FirstOrDefaultAsync(input.Id);
            ObjectMapper.Map(input, banks);
        }
    }
}