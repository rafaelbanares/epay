﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_StatutoryTmp)]
    public class StatutoryTmpAppService : ePayAppServiceBase, IStatutoryTmpAppService
    {
        private readonly IRepository<StatutoryTmp> _statutoryTmpRepository;

        public StatutoryTmpAppService(IRepository<StatutoryTmp> statutoryTmpRepository)
        {
            _statutoryTmpRepository = statutoryTmpRepository;
        }

        public async Task<PagedResultDto<GetStatutoryTmpForViewDto>> GetAll(GetAllStatutoryTmpInput input)
        {

            var filteredStatutoryTmp = _statutoryTmpRepository.GetAll()
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.StatutoryCode.Contains(input.Filter))
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.StatutoryCodeFilter), e => e.StatutoryCode == input.StatutoryCodeFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredStatutoryTmp = filteredStatutoryTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var statutoryTmp = from o in pagedAndFilteredStatutoryTmp
                               select new GetStatutoryTmpForViewDto()
                               {
                                   StatutoryTmp = new StatutoryTmpDto
                                   {
                                       PayrollNo = o.PayrollNo,
                                       StatutorySubTypeId = o.StatutorySubTypeId,
                                       Amount = o.Amount,
                                       EmployeeId = o.EmployeeId,
                                       SessionId = o.SessionId,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredStatutoryTmp.CountAsync();

            return new PagedResultDto<GetStatutoryTmpForViewDto>(
                totalCount,
                await statutoryTmp.ToListAsync()
            );
        }

        public async Task<GetStatutoryTmpForViewDto> GetStatutoryTmpForView(int id)
        {
            var statutoryTmp = await _statutoryTmpRepository.GetAsync(id);

            var output = new GetStatutoryTmpForViewDto { StatutoryTmp = ObjectMapper.Map<StatutoryTmpDto>(statutoryTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTmp_Edit)]
        public async Task<GetStatutoryTmpForEditOutput> GetStatutoryTmpForEdit(EntityDto input)
        {
            var statutoryTmp = await _statutoryTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStatutoryTmpForEditOutput { StatutoryTmp = ObjectMapper.Map<CreateOrEditStatutoryTmpDto>(statutoryTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditStatutoryTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTmp_Create)]
        protected virtual async Task Create(CreateOrEditStatutoryTmpDto input)
        {
            var statutoryTmp = ObjectMapper.Map<StatutoryTmp>(input);
            await _statutoryTmpRepository.InsertAsync(statutoryTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTmp_Edit)]
        protected virtual async Task Update(CreateOrEditStatutoryTmpDto input)
        {
            var statutoryTmp = await _statutoryTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, statutoryTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _statutoryTmpRepository.DeleteAsync(input.Id);
        }
    }
}