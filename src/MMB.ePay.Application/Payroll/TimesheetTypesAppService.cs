﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_TimesheetTypes)]
    public class TimesheetTypesAppService : ePayAppServiceBase, ITimesheetTypesAppService
    {
        private readonly IRepository<TimesheetTypes> _timesheetTypesRepository;

        public TimesheetTypesAppService(IRepository<TimesheetTypes> timesheetTypesRepository)
        {
            _timesheetTypesRepository = timesheetTypesRepository;
        }

        public async Task<IList<SelectListItem>> GetTimesheetTypesListFilter()
        {
            var timesheetTypes = await _timesheetTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.DisplayOrder)
                .Select(e => new SelectListItem
                {
                    Text = e.DisplayName,
                    Value = e.Id.ToString()
                }).ToListAsync();

            timesheetTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return timesheetTypes;
        }

        public async Task<IList<SelectListItem>> GetTimesheetTypesList() =>
            await _timesheetTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.DisplayOrder)
                .Select(e => new SelectListItem
                {
                    Text = e.DisplayName,
                    Value = e.Id.ToString()
                }).ToListAsync();

        public async Task<IList<string>> GetTimesheetTypesMenu() =>
            await _timesheetTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.DisplayOrder)
                .Select(e => e.DisplayName)
                .ToListAsync();

        public async Task<PagedResultDto<GetTimesheetTypesForViewDto>> GetAll(GetAllTimesheetTypesInput input)
        {
            var filteredTimesheetTypes = _timesheetTypesRepository.GetAll();

            var pagedAndFilteredTimesheetTypes = filteredTimesheetTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var timesheetTypes = from o in pagedAndFilteredTimesheetTypes
                                 select new GetTimesheetTypesForViewDto()
                                 {
                                     TimesheetTypes = new TimesheetTypesDto
                                     {
                                         DisplayName = o.DisplayName,
                                         Description = o.Description,
                                         Multiplier = o.Multiplier == 1 ? "+1" : "-1",
                                         Taxable = o.Taxable,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredTimesheetTypes.CountAsync();

            return new PagedResultDto<GetTimesheetTypesForViewDto>(
                totalCount,
                await timesheetTypes.ToListAsync()
            );
        }

        public async Task<GetTimesheetTypesForViewDto> GetTimesheetTypesForView(int id)
        {
            var timesheetTypes = await _timesheetTypesRepository.GetAsync(id);

            var output = new GetTimesheetTypesForViewDto { TimesheetTypes = ObjectMapper.Map<TimesheetTypesDto>(timesheetTypes) };

            output.TimesheetTypes.Multiplier = output.TimesheetTypes.Multiplier switch
            {
                "1.00" => "+1",
                "-1.00" => "-1",
                _ => "0",
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTypes_Edit)]
        public async Task<GetTimesheetTypesForEditOutput> GetTimesheetTypesForEdit(EntityDto input)
        {
            var timesheetTypes = await _timesheetTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTimesheetTypesForEditOutput { TimesheetTypes = ObjectMapper.Map<CreateOrEditTimesheetTypesDto>(timesheetTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTimesheetTypesDto input)
        {
            input.SysCode = input.SysCode == string.Empty ? null : input.SysCode;
            input.AcctCode = input.AcctCode == string.Empty ? null : input.AcctCode;

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTypes_Create)]
        protected virtual async Task Create(CreateOrEditTimesheetTypesDto input)
        {
            var timesheetTypes = ObjectMapper.Map<TimesheetTypes>(input);
            timesheetTypes.Active = true;
            
            timesheetTypes.DisplayOrder = await _timesheetTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .CountAsync() + 1;

            await _timesheetTypesRepository.InsertAsync(timesheetTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTypes_Edit)]
        protected virtual async Task Update(CreateOrEditTimesheetTypesDto input)
        {
            var timesheetTypes = await _timesheetTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, timesheetTypes);
        }
    }
}