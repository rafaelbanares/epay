﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Companies)]
    public class CompaniesAppService : ePayAppServiceBase, ICompaniesAppService
    {
        private readonly IRepository<Companies> _companiesRepository;

        public CompaniesAppService(IRepository<Companies> companiesRepository)
        {
            _companiesRepository = companiesRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Companies_Edit)]
        public async Task<GetCompaniesForEditOutput> GetCompaniesForEdit()
        {
            var companies = await _companiesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .FirstOrDefaultAsync();

            var output = new GetCompaniesForEditOutput { Companies = ObjectMapper.Map<CreateOrEditCompaniesDto>(companies) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditCompaniesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Companies_Create)]
        protected virtual async Task Create(CreateOrEditCompaniesDto input)
        {
            var companies = ObjectMapper.Map<Companies>(input);
            await _companiesRepository.InsertAsync(companies);
        }

        [AbpAuthorize(AppPermissions.Pages_Companies_Edit)]
        protected virtual async Task Update(CreateOrEditCompaniesDto input)
        {
            var companies = await _companiesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, companies);
        }
    }
}