﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_LeaveCredits)]
    public class LeaveCreditsAppService : ePayAppServiceBase, ILeaveCreditsAppService
    {
        private readonly IRepository<LeaveCredits> _leaveCreditsRepository;

        public LeaveCreditsAppService(IRepository<LeaveCredits> leaveCreditsRepository)
        {
            _leaveCreditsRepository = leaveCreditsRepository;
        }

        public async Task<PagedResultDto<GetLeaveCreditsForViewDto>> GetAll(GetAllLeaveCreditsInput input)
        {

            var filteredLeaveCredits = _leaveCreditsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Remarks.Contains(input.Filter))
                        .WhereIf(input.MinAppYearFilter != null, e => e.AppYear >= input.MinAppYearFilter)
                        .WhereIf(input.MaxAppYearFilter != null, e => e.AppYear <= input.MaxAppYearFilter)
                        .WhereIf(input.MinLeaveDaysFilter != null, e => e.LeaveDays >= input.MinLeaveDaysFilter)
                        .WhereIf(input.MaxLeaveDaysFilter != null, e => e.LeaveDays <= input.MaxLeaveDaysFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RemarksFilter), e => e.Remarks == input.RemarksFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinLeaveTypeIdFilter != null, e => e.LeaveTypeId >= input.MinLeaveTypeIdFilter)
                        .WhereIf(input.MaxLeaveTypeIdFilter != null, e => e.LeaveTypeId <= input.MaxLeaveTypeIdFilter);

            var pagedAndFilteredLeaveCredits = filteredLeaveCredits
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var leaveCredits = from o in pagedAndFilteredLeaveCredits
                               select new GetLeaveCreditsForViewDto()
                               {
                                   LeaveCredits = new LeaveCreditsDto
                                   {
                                       AppYear = o.AppYear,
                                       LeaveDays = o.LeaveDays,
                                       Remarks = o.Remarks,
                                       EmployeeId = o.EmployeeId,
                                       LeaveTypeId = o.LeaveTypeId,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredLeaveCredits.CountAsync();

            return new PagedResultDto<GetLeaveCreditsForViewDto>(
                totalCount,
                await leaveCredits.ToListAsync()
            );
        }

        public async Task<GetLeaveCreditsForViewDto> GetLeaveCreditsForView(int id)
        {
            var leaveCredits = await _leaveCreditsRepository.GetAsync(id);

            var output = new GetLeaveCreditsForViewDto { LeaveCredits = ObjectMapper.Map<LeaveCreditsDto>(leaveCredits) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveCredits_Edit)]
        public async Task<GetLeaveCreditsForEditOutput> GetLeaveCreditsForEdit(EntityDto input)
        {
            var leaveCredits = await _leaveCreditsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeaveCreditsForEditOutput { LeaveCredits = ObjectMapper.Map<CreateOrEditLeaveCreditsDto>(leaveCredits) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLeaveCreditsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveCredits_Create)]
        protected virtual async Task Create(CreateOrEditLeaveCreditsDto input)
        {
            var leaveCredits = ObjectMapper.Map<LeaveCredits>(input);
            await _leaveCreditsRepository.InsertAsync(leaveCredits);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveCredits_Edit)]
        protected virtual async Task Update(CreateOrEditLeaveCreditsDto input)
        {
            var leaveCredits = await _leaveCreditsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, leaveCredits);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveCredits_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _leaveCreditsRepository.DeleteAsync(input.Id);
        }
    }
}