﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

////FIX THIS
namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Reports)]
    public class ReportsAppService : ePayAppServiceBase, IReportsAppService
    {
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<Reports> _reportsRepository;
        private readonly IRepository<ReportSignatories> _reportSignatoriesRepository;
        private readonly IRepository<PayrollPosted> _payrollPostedRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<Employers> _employersRepository;
        private readonly IRepository<EmployeePayroll> _employeePayrollRepository;
        private readonly IRepository<DeductionPosted> _deductionPostedRepository;
        private readonly IRepository<EarningPosted> _earningPostedRepository;
        private readonly IRepository<LoanPosted> _loanPostedRepository;
        private readonly IRepository<OTSheetPosted> _otSheetPostedRepository;
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IRepository<TimesheetTypes> _timesheetTypesRepository;
        private readonly IRepository<EmployeeLoans, Guid> _employeeLoansRepository;


        public ReportsAppService(
            IRepository<Tenant> tenantRepository,
            IRepository<Reports> reportsRepository,
            IRepository<ReportSignatories> reportSignatoriesRepository,
            IRepository<PayrollPosted> payrollPostedRepository,
            IRepository<Employees> employeesRepository,
            IRepository<Employers> employersRepository,
            IRepository<EmployeePayroll> employeePayrollRepository,
            IRepository<DeductionPosted> deductionPostedRepository,
            IRepository<EarningPosted> earningPostedRepository,
            IRepository<LoanPosted> loanPostedRepository,
            IRepository<OTSheetPosted> otSheetPostedRepository,
            IRepository<OvertimeTypes> overtimeTypesRepository,
            IRepository<TimesheetTypes> timesheetTypesRepository,
            IRepository<EmployeeLoans, Guid> employeeLoansRepository)
        {
            _tenantRepository = tenantRepository;
            _reportsRepository = reportsRepository;
            _reportSignatoriesRepository = reportSignatoriesRepository;
            _payrollPostedRepository = payrollPostedRepository;
            _employeesRepository = employeesRepository;
            _employersRepository = employersRepository;
            _employeePayrollRepository = employeePayrollRepository;
            _deductionPostedRepository = deductionPostedRepository;
            _earningPostedRepository = earningPostedRepository;
            _loanPostedRepository = loanPostedRepository;
            _otSheetPostedRepository = otSheetPostedRepository;
            _overtimeTypesRepository = overtimeTypesRepository;
            _timesheetTypesRepository = timesheetTypesRepository;
            _employeeLoansRepository = employeeLoansRepository;
        }

        public async Task<IList<SelectListItem>> GetReportList(string reportTypeId)
        {
            var reports = await _reportsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.ReportTypeId == reportTypeId && e.Enabled)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            return reports.OrderBy(e => e.Text).ToList();
        }

        public async Task<string> GetReportName(int id)
        {
            return await _reportsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => e.DisplayName)
                .FirstOrDefaultAsync();
        }

        public async Task<GetReportsForViewDto> GetReportsForView(int id)
        {
            return await _reportsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetReportsForViewDto
                {
                    Reports = new ReportsDto
                    {
                        ReportId = e.Id,
                        Format = e.Format,
                        IsLandscape = e.IsLandscape,
                        Output = e.Output,
                        //HasEmployeeFilter = e.HasEmployeeFilter,
                        //HasPeriodFilter = e.HasPeriodFilter,
                        ActionName = e.ControllerName
                    }
                }).FirstOrDefaultAsync();
        }

        [RemoteService(false)]
        public async Task<GetReportsForBankAdvise> GetPeriodicBankAdvise(ReportsDto dto)
        {
            var tenancyName = await _tenantRepository.GetAll()
                .Where(e => e.Id == AbpSession.TenantId)
                .Select(e => e.TenancyName)
                .FirstOrDefaultAsync();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position,
                }).FirstOrDefaultAsync();

            var bankAdvise = await _employeePayrollRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.PayCodeId.ToString() == dto.PayCodeId)
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.PaymentType == "BANK")
                .Select(e => new
                {
                    e.BankBranches.BankId,
                    e.BankBranchId,
                    BankName = e.BankBranches.Banks.DisplayName,
                    e.BankBranches.BranchName,
                    TenantAccountNo = e.BankBranches.TenantBanks.FirstOrDefault().BankAccountNumber,
                    EmployeeName = e.Employees.FullName,
                    e.BankAccountNo,
                    Amount = e.Employees.PayrollPosted.Sum(f => f.NetPay)
                }).ToListAsync();

            var bankBranches = bankAdvise
                .GroupBy(e => new { e.BankId, e.BankBranchId })
                .Select(e => new BankBranchTable
                {
                    BankName = e.FirstOrDefault().BankName,
                    BranchName = e.FirstOrDefault().BranchName,
                    TenantAccountNo = e.FirstOrDefault().TenantAccountNo,
                    BankAdvise = e
                        .Where(f => f.Amount > 0)
                        .OrderBy(e => e.EmployeeName)
                        .Select(f => new AdviseTable
                        {
                            EmployeeName = f.EmployeeName,
                            BankAccountNo = f.BankAccountNo,
                            Amount = f.Amount
                        }).ToList()
                }).ToList();

            return new GetReportsForBankAdvise()
            {
                TenantName = tenancyName,
                Signatory = signatory,
                BankBranches = bankBranches
                    .OrderBy(e => e.BankName)
                    .ThenBy(e => e.BranchName)
                    .Where(e => e.BankAdvise.Sum(f => f.Amount) > 0)
                    .ToList()
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForCashAdvise> GetPeriodicCashAdvise(ReportsDto dto)
        {
            var cashAdvise = await _employeePayrollRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.PayCodeId.ToString() == dto.PayCodeId)
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.PaymentType == "CASH")
                .Select(e => new
                {
                    EmployeeName = e.Employees.FullName,
                    e.BankAccountNo,
                    Amount = e.Employees.PayrollPosted.Sum(f => f.NetPay)
                }).ToListAsync();

            return new GetReportsForCashAdvise()
            {
                CashAdvise = cashAdvise
                    .Where(e => e.Amount > 0)
                    .OrderBy(e => e.EmployeeName)
                    .Select(e => new AdviseTable
                    {
                        EmployeeName = e.EmployeeName,
                        BankAccountNo = e.BankAccountNo,
                        Amount = e.Amount
                    }).ToList()
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForChequeAdvise> GetPeriodicChequeAdvise(ReportsDto dto)
        {
            var chequeAdvise = await _employeePayrollRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.PayPeriodId != null,
                    e => e.Employees.PayrollPosted.Any(f => f.PayrollNo == dto.PayPeriodId))
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.PayCodeId.ToString() == dto.PayCodeId)
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.PaymentType == "CHEQUE")
                .Select(e => new
                {
                    EmployeeName = e.Employees.FullName,
                    e.BankAccountNo,
                    Amount = e.Employees.PayrollPosted.Sum(f => f.NetPay)
                }).ToListAsync();

            return new GetReportsForChequeAdvise()
            {
                ChequeAdvise = chequeAdvise
                    .Where(e => e.Amount > 0)
                    .OrderBy(e => e.EmployeeName)
                    .Select(e => new AdviseTable
                    {
                        EmployeeName = e.EmployeeName,
                        BankAccountNo = e.BankAccountNo,
                        Amount = e.Amount
                    }).ToList()
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForDeductionRegister> GetPeriodicDeductionRegister(ReportsDto dto)
        {
            var deductionPosted = await _deductionPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.PayrollPosted.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.PayrollNo > 0, e => e.PayrollPosted.PayrollNo == dto.PayrollNo)
                .Select(e => new
                {
                    EmployeeId = e.PayrollPosted.Employees.EmployeeCode,
                    PayCode = e.PayrollPosted.Employees.EmployeePayroll.Select(f => f.PayCodes.DisplayName).FirstOrDefault(),
                    Department = e.PayrollPosted.Employees.User.OrganizationUnits.Select(f => f.OrganizationUnitId.ToString()).FirstOrDefault(),
                    EmployeeName = e.PayrollPosted.Employees.FullName,
                    DeductionCode = e.DeductionTypes.DisplayName,
                    e.Amount
                }).ToListAsync();

            var groupedOutput = dto.Grouping switch
            {
                "None" => deductionPosted.GroupBy(e => e.DeductionCode),
                "PayCode" => deductionPosted.GroupBy(e => e.PayCode),
                "Department" => deductionPosted.GroupBy(e => e.Department),
                _ => throw new NotImplementedException()
            };

            var output = groupedOutput.Select(e => new DeductionRegisterGroup
            {
                GroupBy = e.Key,
                DeductionRegister = e
                .OrderBy(e => e.EmployeeName)
                .Select(f => new DeductionRegisterTable
                {
                    EmployeeId = f.EmployeeId,
                    EmployeeName = f.EmployeeName,
                    DeductionCode = f.DeductionCode,
                    Amount = f.Amount
                }).ToList()
            }).ToList();

            return new GetReportsForDeductionRegister()
            {
                Grouping = dto.Grouping,
                DeductionRegisterGroup = output
                    .OrderBy(e => e.GroupBy)
                    .ToList()
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForEarningRegister> GetPeriodicEarningRegister(ReportsDto dto)
        {
            var earningPosted = await _earningPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.PayrollPosted.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.PayrollNo > 0, e => e.PayrollPosted.PayrollNo == dto.PayrollNo)
                .Select(e => new
                {
                    EmployeeId = e.PayrollPosted.Employees.EmployeeCode,
                    PayCode = e.PayrollPosted.Employees.EmployeePayroll.Select(f => f.PayCodes.DisplayName).FirstOrDefault(),
                    Department = e.PayrollPosted.Employees.User.OrganizationUnits.Select(f => f.OrganizationUnitId.ToString()).FirstOrDefault(),
                    EmployeeName = e.PayrollPosted.Employees.FullName,
                    EarningCode = e.EarningTypes.DisplayName,
                    e.Amount
                }).ToListAsync();

            var groupedOutput = dto.Grouping switch
            {
                "None" => earningPosted.GroupBy(e => e.EarningCode),
                "PayCode" => earningPosted.GroupBy(e => e.PayCode),
                "Department" => earningPosted.GroupBy(e => e.Department),
                _ => throw new NotImplementedException()
            };

            var output = groupedOutput.Select(e => new EarningRegisterGroup
            {
                GroupBy = e.Key,
                EarningRegister = e
                .OrderBy(e => e.EmployeeName)
                .Select(f => new EarningRegisterTable
                {
                    EmployeeId = f.EmployeeId,
                    EmployeeName = f.EmployeeName,
                    EarningCode = f.EarningCode,
                    Amount = f.Amount
                }).ToList()
            }).ToList();

            return new GetReportsForEarningRegister()
            {
                Grouping = dto.Grouping,
                EarningRegisterGroup = output
                    .OrderBy(e => e.GroupBy)
                    .ToList()
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForEmployeePayslip> GetPeriodicEmployeePayslip(ReportsDto dto)
        {
            var posted = await _payrollPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.PayrollNo > 0, e => e.PayrollNo == dto.PayrollNo)
                .Select(e => new EmployeePayslipTable
                {
                    CompanyName = e.Employees.Tenant.TenancyName,
                    Employee = e.Employees.EmployeeCode + " " + e.Employees.FullName,
                    Period = string.Format("{0} - {1}", e.PayPeriod.PeriodFrom.ToString("MM/dd/yyyy"), e.PayPeriod.PeriodTo.ToString("MM/dd/yyyy")),
                    CutOffPeriod = string.Format("{0} - {1}", e.PayPeriod.CutOffFrom.ToString("MM/dd/yyyy"), e.PayPeriod.CutOffTo.ToString("MM/dd/yyyy")),
                    PayDate = e.PayPeriod.PayDate,
                    Income = e.EarningPosted.Select(e => new Tuple<string, string, decimal>
                        (
                            e.EarningTypes.DisplayName,
                            string.Empty,
                            e.Amount
                        )).ToList(),
                    Deductions = e.DeductionPosted.Select(e => new Tuple<string, decimal>
                        (
                            e.DeductionTypes.DisplayName,
                            e.Amount
                        )).ToList(),
                    NetPay = e.TimesheetPosted.Select(e => new Tuple<string, decimal>
                        (
                            e.TimesheetTypes.DisplayName,
                            e.Amount
                        )).ToList(),
                    YtdSummary = e.StatutoryPosted.Select(e => new Tuple<string, decimal>
                        (
                            e.StatutorySubTypes.DisplayName,
                            e.Amount
                        )).ToList(),
                    LoanBalances = e.LoanPosted.Select(e => new Tuple<string, decimal, decimal>
                        (
                            e.EmployeeLoans.Loans.DisplayName,
                            e.Amount,
                            e.EmployeeLoans.LoanAmount
                        )).ToList()
                }).ToListAsync();


            return new GetReportsForEmployeePayslip()
            {
                EmployeePayslip = posted
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForGrossAndTax> GetPeriodicGrossAndTax(ReportsDto dto)
        {
            var grossAndTax = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    TIN = e.EmployeePayroll.Select(f => f.TIN).ToList(),
                    PayrollPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .Select(f => new { f.GrossPay, f.TxEarningAmt })
                        .ToList(),
                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(g => g.StatutorySubTypes.EmployeeShare)
                            .Select(g => new { g.StatutorySubTypes.SysCode, g.Amount }
                        )).ToList()
                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Where(f => f.StatutorySubTypes.EmployeeShare)
                    //    .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = grossAndTax
                .Select(e => new GrossAndTaxTable
                {
                    EmployeeId = e.EmployeeId,
                    EmployeeName = e.EmployeeName,
                    TIN = e.TIN.Count == 0 ? "" : e.TIN.FirstOrDefault(),
                    GrossPay = e.PayrollPosted.Sum(f => f.GrossPay),
                    TaxableInc = e.PayrollPosted.Sum(f => f.TxEarningAmt),
                    Total = e.StatutoryPosted.Where(f => f.SysCode != "WTAX").Sum(f => f.Amount),
                    WTax = e.StatutoryPosted.Where(f => f.SysCode == "WTAX").Sum(f => f.Amount)
                }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position =  e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForGrossAndTax()
            {
                Signatory = signatory,
                GrossAndTax = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPagibigContribution> GetPeriodicPagibigContribution(ReportsDto dto)
        {
            var pagibigContribution = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    PagibigNo = e.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "PAGIBIG")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),

                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(g => g.StatutorySubTypes.SysCode == "EEPAG" || g.StatutorySubTypes.SysCode == "ERPAG")
                            .Select(g => new { g.StatutorySubTypes.SysCode, g.Amount })
                        ).ToList()
                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(e => e.StatutorySubTypes.SysCode == "EEPAG" || e.StatutorySubTypes.SysCode == "ERPAG")
                    //    .Select(e => new { e.StatutorySubTypes.SysCode, e.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = pagibigContribution
                .Select(e => new PagibigContributionTable
                {
                    EmployeeId = e.EmployeeId,
                    PagibigNo = e.PagibigNo,
                    EmployeeName = e.EmployeeName,
                    Employer = e.StatutoryPosted.Where(f => f.SysCode == "ERPAG").Sum(f => f.Amount),
                    Employee = e.StatutoryPosted.Where(f => f.SysCode == "EEPAG").Sum(f => f.Amount)
                }).ToList();

            output = output.Select(e => { e.Total = e.Employer + e.Employee; return e; }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new
                {
                    e.Signatories.SignatoryName,
                    e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForPagibigContribution()
            {
                Person = signatory.SignatoryName,
                Position = signatory.Position,
                PagibigContribution = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPayrollRegister> GetPeriodicPayrollRegister(ReportsDto dto)
        {
            var payrollRegister = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new 
                {
                    EmployeeName = e.FullName,
                    EmployeeId = e.EmployeeCode,

                    Statutory = e.PayrollPosted
                        .Where(e => e.PayrollNo == dto.PayrollNo)
                        .SelectMany(e => e.StatutoryPosted
                            .Select(f => new
                            {
                                StatutoryName = f.StatutorySubTypes.DisplayName,
                                f.StatutorySubTypes.SysCode,
                                Value = f.Amount
                            }).ToList()
                        ).ToList(),
                    Timesheet = e.PayrollPosted
                        .Where(e => e.PayrollNo == dto.PayrollNo)
                        .SelectMany(e => e.TimesheetPosted
                            .Select(f => new PayrollRegisterStatutory
                            {
                                StatutoryName = f.TimesheetTypes.DisplayName,
                                Value = f.Amount
                            }).ToList()
                        ).ToList(),
                    Earnings = e.PayrollPosted
                        .Where(e => e.PayrollNo == dto.PayrollNo)
                        .SelectMany(e => e.EarningPosted
                            .Select(f => new PayrollRegisterStatutory
                            {
                                StatutoryName = f.EarningTypes.DisplayName,
                                Value = f.Amount
                            }).ToList()
                        ).ToList(),
                    Loans = e.PayrollPosted
                        .Where(e => e.PayrollNo == dto.PayrollNo)
                        .SelectMany(e => e.LoanPosted
                            .Select(f => new PayrollRegisterStatutory
                            {
                                StatutoryName = f.PayDate.ToString("MM/dd/yyyy"),
                                Value = f.Amount
                            }).ToList()
                        ).ToList(),
                    Deductions = e.PayrollPosted
                        .Where(e => e.PayrollNo == dto.PayrollNo)
                        .SelectMany(e => e.DeductionPosted
                            .Select(f => new PayrollRegisterStatutory
                            {
                                StatutoryName = f.DeductionTypes.DisplayName,
                                Value = f.Amount
                            }).ToList()
                        ).ToList()

                    //Statutory = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Select(f => new 
                    //    {
                    //        StatutoryName = f.StatutorySubTypes.DisplayName,
                    //        f.StatutorySubTypes.SysCode,
                    //        Value = f.Amount
                    //    }).ToList(),
                    //Timesheet = e.TimesheetPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Select(f => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = f.TimesheetTypes.DisplayName,
                    //        Value = f.Amount
                    //    }).ToList(),
                    //Earnings = e.EarningPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Select(f => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = f.EarningTypes.DisplayName,
                    //        Value = f.Amount
                    //    }).ToList(),
                    //Loans = e.LoanPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Select(e => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = e.PayDate.ToString("MM/dd/yyyy"),
                    //        Value = e.Amount
                    //    }).ToList(),
                    //Deductions = e.DeductionPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Select(e => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = e.DeductionTypes.DisplayName,
                    //        Value = e.Amount
                    //    }).ToList(),
                }).ToListAsync();

            var output = payrollRegister
                .Select(e => new PayrollRegisterTable
                {
                    EmployeeName = e.EmployeeName,
                    EmployeeId = e.EmployeeId,
                    PayrollRegisterTimesheet = e.Timesheet,
                    PayrollRegisterEarnings = e.Earnings,
                    PayrollRegisterLoans = e.Loans,
                    PayrollRegisterDeductions = e.Deductions,
                    WTax = e.Statutory.Where(f => f.SysCode == "WTAX").Select(f => f.Value).FirstOrDefault(),
                    Sss = e.Statutory.Where(f => f.SysCode == "EESSS").Select(f => f.Value).FirstOrDefault(),
                    Philhealth = e.Statutory.Where(f => f.SysCode == "EEPH").Select(f => f.Value).FirstOrDefault(),
                    Pagibig = e.Statutory.Where(f => f.SysCode == "EEPAG").Select(f => f.Value).FirstOrDefault(),
                    SssEr = e.Statutory.Where(f => f.SysCode == "ERSSS").Select(f => f.Value).FirstOrDefault(),
                    Ecc = e.Statutory.Where(f => f.SysCode == "ERECC").Select(f => f.Value).FirstOrDefault(),
                    PhEr = e.Statutory.Where(f => f.SysCode == "ERPH").Select(f => f.Value).FirstOrDefault(),
                    PagEr = e.Statutory.Where(f => f.SysCode == "ERPAG").Select(f => f.Value).FirstOrDefault(),
                }).ToList();

            return new GetReportsForPayrollRegister()
            {
                PayrollRegister = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPhilhealthContribution> GetPeriodicPhilhealthContribution(ReportsDto dto)
        {
            var philhealthContribution = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    PhilhealthNo = e.EmployeeStatutoryInfo
                        .Where(f => f.StatutoryTypes.Code == "PHEALTH")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault(),

                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(f => f.StatutorySubTypes.SysCode == "EEPH" || f.StatutorySubTypes.SysCode == "ERPH")
                            .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount }))
                            .ToList()
                        .ToList()

                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Where(f => f.StatutorySubTypes.SysCode == "EEPH" || f.StatutorySubTypes.SysCode == "ERPH")
                    //    .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = philhealthContribution
                .Select(e => new PhilhealthContributionTable
                {
                    EmployeeId = e.EmployeeId,
                    PhilhealthNo = e.PhilhealthNo,
                    EmployeeName = e.EmployeeName,
                    Employer = e.StatutoryPosted.Where(f => f.SysCode == "ERPH").Sum(f => f.Amount),
                    Employee = e.StatutoryPosted.Where(f => f.SysCode == "EEPH").Sum(f => f.Amount)
                }).ToList();

            output = output.Select(e => { e.Total = e.Employer + e.Employee; return e; }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForPhilhealthContribution()
            {
                Signatory = signatory,
                PhilhealthContribution = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForSssContribution> GetPeriodicSssContribution(ReportsDto dto)
        {
            var sssContribution = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    SssNo = e.EmployeeStatutoryInfo
                        .Where(f => f.StatutoryTypes.Code == "SSS")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault(),
                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(f => f.StatutorySubTypes.SysCode == "EESSS" || f.StatutorySubTypes.SysCode == "ERSSS")
                            .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                            .ToList()
                        ).ToList()

                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayrollNo == dto.PayrollNo)
                    //    .Where(f => f.StatutorySubTypes.SysCode == "EESSS" || f.StatutorySubTypes.SysCode == "ERSSS")
                    //    .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = sssContribution
                .Select(e => new SssContributionTable
                {
                    EmployeeId = e.EmployeeId,
                    SssNo = e.SssNo,
                    EmployeeName = e.EmployeeName,
                    Employer = e.StatutoryPosted.Where(f => f.SysCode == "ERSSS").Sum(f => f.Amount),
                    Employee = e.StatutoryPosted.Where(f => f.SysCode == "EESSS").Sum(f => f.Amount)
                }).ToList();

            output = output.Select(e => { e.Total = e.Employer + e.Employee; return e; }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position
                }).FirstOrDefaultAsync();

            return new GetReportsForSssContribution()
            {
                Signatory = signatory,
                SssContribution = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForTimesheetRegister> GetPeriodicTimesheetRegister(ReportsDto dto)
        {
            var header = await _timesheetTypesRepository.GetAll()
                .OrderBy(e => e.DisplayOrder)
                .Select(e => new TimesheetRegisterHeader
                {
                    SysCode = e.SysCode,
                    HeaderName = e.DisplayName
                }).ToListAsync();

            var output = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new TimesheetRegisterTable
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    TimesheetRegisterValue = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.TimesheetPosted
                            .Select(g => new TimesheetRegisterValue
                            {
                                SysCode = g.TimesheetTypes.SysCode,
                                Hours = g.Hrs,
                                Value = g.Amount
                            })
                        ).ToList()

                    //TimesheetRegisterValue = e.TimesheetPosted
                    //    .Select(f => new TimesheetRegisterValue
                    //    {
                    //        SysCode = f.TimesheetTypes.SysCode,
                    //        Hours = f.Hrs,
                    //        Value = f.Amount
                    //    }).ToList()
                }).ToListAsync();

            return new GetReportsForTimesheetRegister()
            {
                TimesheetRegisterHeader = header,
                TimesheetRegister = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForDeductionRegister> GetMonthlyDeductionRegister(ReportsDto dto)
        {
            var deductionPosted = await _deductionPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.PayrollPosted.EmployeeId == dto.EmployeeId)
                .Where(e => e.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && e.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                .Select(e => new
                {
                    EmployeeId = e.PayrollPosted.Employees.EmployeeCode,
                    PayCode = e.PayrollPosted.Employees.EmployeePayroll.Select(f => f.PayCodes.DisplayName).FirstOrDefault(),
                    Department = e.PayrollPosted.Employees.User.OrganizationUnits.Select(f => f.OrganizationUnitId.ToString()).FirstOrDefault(),
                    EmployeeName = e.PayrollPosted.Employees.FullName,
                    DeductionCode = e.DeductionTypes.DisplayName,
                    e.Amount
                }).ToListAsync();

            var groupedOutput = dto.Grouping switch
            {
                "None" => deductionPosted.GroupBy(e => e.DeductionCode),
                "PayCode" => deductionPosted.GroupBy(e => e.PayCode),
                "Department" => deductionPosted.GroupBy(e => e.Department),
                _ => throw new NotImplementedException()
            };

            var output = groupedOutput.Select(e => new DeductionRegisterGroup
            {
                GroupBy = e.Key,
                DeductionRegister = e.Select(f => new DeductionRegisterTable
                {
                    EmployeeId = f.EmployeeId,
                    EmployeeName = f.EmployeeName,
                    DeductionCode = f.DeductionCode,
                    Amount = f.Amount
                }).ToList()
            }).ToList();

            return new GetReportsForDeductionRegister()
            {
                Grouping = dto.Grouping,
                DeductionRegisterGroup = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForEarningRegister> GetMonthlyEarningRegister(ReportsDto dto)
        {
            var deductionPosted = await _earningPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.PayrollPosted.EmployeeId == dto.EmployeeId)
                .Where(e => e.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && e.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                .Select(e => new
                {
                    EmployeeId = e.PayrollPosted.Employees.EmployeeCode,
                    PayCode = e.PayrollPosted.Employees.EmployeePayroll.Select(f => f.PayCodes.DisplayName).FirstOrDefault(),
                    Department = e.PayrollPosted.Employees.User.OrganizationUnits.Select(f => f.OrganizationUnitId.ToString()).FirstOrDefault(),
                    EmployeeName = e.PayrollPosted.Employees.FullName,
                    EarningCode = e.EarningTypes.DisplayName,
                    e.Amount
                }).ToListAsync();

            var groupedOutput = dto.Grouping switch
            {
                "None" => deductionPosted.GroupBy(e => e.EarningCode),
                "PayCode" => deductionPosted.GroupBy(e => e.PayCode),
                "Department" => deductionPosted.GroupBy(e => e.Department),
                _ => throw new NotImplementedException()
            };

            var output = groupedOutput.Select(e => new EarningRegisterGroup
            {
                GroupBy = e.Key,
                EarningRegister = e.Select(f => new EarningRegisterTable
                {
                    EmployeeId = f.EmployeeId,
                    EmployeeName = f.EmployeeName,
                    EarningCode = f.EarningCode,
                    Amount = f.Amount
                }).ToList()
            }).ToList();

            return new GetReportsForEarningRegister()
            {
                Grouping = dto.Grouping,
                EarningRegisterGroup = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForGrossAndTax> GetMonthlyGrossAndTax(ReportsDto dto)
        {
            var grossAndTax = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    TIN = e.EmployeePayroll.Select(f => f.TIN).ToList(),
                    PayrollPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        //.Where(f => f.PayPeriod.PayDate.Year == dto.Year && f.PayPeriod.PayDate.Month == dto.Month)
                        .Select(f => new { f.GrossPay, f.TxEarningAmt })
                        .ToList(),
                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(g => g.StatutorySubTypes.EmployeeShare)
                            .Select(g => new { g.StatutorySubTypes.SysCode, g.Amount })
                            .ToList()
                        ).ToList()

                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Where(f => f.StatutorySubTypes.EmployeeShare)
                    //    .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = grossAndTax
                .Select(e => new GrossAndTaxTable
                {
                    EmployeeId = e.EmployeeId,
                    EmployeeName = e.EmployeeName,
                    TIN = e.TIN.Count == 0 ? "" : e.TIN.FirstOrDefault(),
                    GrossPay = e.PayrollPosted.Sum(f => f.GrossPay),
                    TaxableInc = e.PayrollPosted.Sum(f => f.TxEarningAmt),
                    Total = e.StatutoryPosted.Where(f => f.SysCode != "WTAX").Sum(f => f.Amount),
                    WTax = e.StatutoryPosted.Where(f => f.SysCode == "WTAX").Sum(f => f.Amount)
                }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForGrossAndTax()
            {
                Signatory = signatory,
                GrossAndTax = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForLoanDetailRegister> GetMonthlyLoanDetailRegister(ReportsDto dto)
        {
            var loanDetailPosted = await _loanPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.PayrollPosted.EmployeeId == dto.EmployeeId)
                .Where(e => e.PayPeriod.PayDate.Year == dto.Year && e.PayPeriod.PayDate.Month == dto.Month)
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.PayrollPosted.Employees.EmployeeCode,
                    PayCode = e.PayrollPosted.Employees.EmployeePayroll.Select(f => f.PayCodes.DisplayName).FirstOrDefault(),
                    Department = e.PayrollPosted.Employees.User.OrganizationUnits.Select(f => f.OrganizationUnitId.ToString()).FirstOrDefault(),
                    EmployeeName = e.PayrollPosted.Employees.FullName,
                    DateStart = e.EmployeeLoans.EffectStart,
                    LoanCode = e.EmployeeLoans.Loans.LoanTypes.Description,
                    LoanPayment = e.EmployeeLoans.LoanAmount,
                    Balance = e.EmployeeLoans.Principal
                }).ToListAsync();

            var groupedOutput = dto.Grouping switch
            {
                "None" => loanDetailPosted.GroupBy(e => e.LoanCode),
                "PayCode" => loanDetailPosted.GroupBy(e => e.PayCode),
                "Department" => loanDetailPosted.GroupBy(e => e.Department),
                _ => throw new NotImplementedException()
            };

            var output = groupedOutput.Select(e => new LoanDetailRegisterGroup
            {
                GroupBy = e.Key,
                LoanDetailRegister = e.Select(f => new LoanDetailRegisterTable
                {
                    EmployeeId = f.EmployeeId,
                    EmployeeName = f.EmployeeName,
                    DateStart = f.DateStart,
                    LoanCode = f.LoanCode,
                    LoanPayment = f.LoanPayment,
                    Balance = f.Balance
                }).ToList()
            }).ToList();

            return new GetReportsForLoanDetailRegister()
            {
                Grouping = dto.Grouping,
                LoanDetailRegisterGroup = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForOvertimeRegister> GetMonthlyOvertimeRegister(ReportsDto dto)
        {
            var overtimePosted = await _otSheetPostedRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.PayrollPosted.EmployeeId == dto.EmployeeId)
                .Where(e => e.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && e.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                .OrderBy(e => e.OvertimeTypes.OvertimeMainTypes.DisplayOrder)
                .Select(e => new
                {
                    EmployeeId = e.PayrollPosted.Employees.EmployeeCode,
                    PayCode = e.PayrollPosted.Employees.EmployeePayroll.Select(f => f.PayCodes.DisplayName).FirstOrDefault(),
                    Department = e.PayrollPosted.Employees.User.OrganizationUnits.Select(f => f.OrganizationUnitId.ToString()).FirstOrDefault(),
                    EmployeeName = e.PayrollPosted.Employees.FullName,
                    OvertimeType = e.OvertimeTypes.DisplayName,
                    OvertimeCode = e.OvertimeTypes.OvertimeMainTypes.DisplayName,
                    e.Amount
                }).ToListAsync();

            var groupedOutput = overtimePosted.GroupBy(e => e.OvertimeCode).ToList();

            var output = groupedOutput.Select(e => new OvertimeRegisterGroup
            {
                GroupBy = e.Key,
                OvertimeRegister = e.Select(f => new OvertimeRegisterTable
                {
                    EmployeeId = f.EmployeeId,
                    EmployeeName = f.EmployeeName,
                    OvertimeCode = f.OvertimeCode
                }).ToList(),
                Amounts = e.Select(f => new Tuple<string, decimal>
                (
                    f.OvertimeType, 
                    f.Amount)
                ).ToList()
            }).ToList();

            var overtimeTypes = await _overtimeTypesRepository.GetAll()
                .Select(e => e.DisplayName)
                .ToListAsync();

            return new GetReportsForOvertimeRegister()
            {
                Grouping = dto.Grouping,
                OvertimeRegisterGroup = output,
                OvertimeTypes = overtimeTypes
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPagibigM11> GetMonthlyPagibigM11(ReportsDto dto)
        {
            var pagibigM11 = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeName = e.FullName,
                    PagibigNo = e.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "PAGIBIG")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),
                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(g => g.StatutorySubTypes.SysCode == "EEPAG" || g.StatutorySubTypes.SysCode == "ERPAG")
                            .Select(g => new { g.StatutorySubTypes.SysCode, g.Amount })
                            .ToList()
                        ).ToList()
                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(e => e.StatutorySubTypes.SysCode == "EEPAG" || e.StatutorySubTypes.SysCode == "ERPAG")
                    //    .Select(e => new { e.StatutorySubTypes.SysCode, e.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = pagibigM11
                .Select(e => new PagibigM11Table
                {
                    PagibigNo = e.PagibigNo,
                    BorrowerName = e.EmployeeName,
                    Employer = e.StatutoryPosted.Where(f => f.SysCode == "ERPAG").Sum(f => f.Amount),
                    Employee = e.StatutoryPosted.Where(f => f.SysCode == "EEPAG").Sum(f => f.Amount)
                }).ToList();

            var employer = await _employersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new ReportsEmployerDto
                {
                    EmployerName = e.EmployerName,
                    EmployerAddress = e.Address1,
                    TIN = e.TIN
                }).FirstOrDefaultAsync();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForPagibigM11()
            {
                PagibigM11 = output.Where(e => e.Employee > 0 || e.Employer > 0).ToList(),
                Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dto.Month.Value),
                Year = dto.Year,
                Employer = employer,
                Signatory = signatory
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPagibigMRCRF> GetMonthlyPagibigMRCRF(ReportsDto dto)
        {
            var pagibigMRCRF = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    e.LastName,
                    e.FirstName,
                    e.MiddleName,
                    PagibigNo = e.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "PAGIBIG")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),

                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(f => f.StatutorySubTypes.SysCode == "EEPAG" || f.StatutorySubTypes.SysCode == "ERPAG")
                            .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                            .ToList())
                        .ToList()

                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(e => e.StatutorySubTypes.SysCode == "EEPAG" || e.StatutorySubTypes.SysCode == "ERPAG")
                    //    .Select(e => new { e.StatutorySubTypes.SysCode, e.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = pagibigMRCRF
                .Select(e => new PagibigMRCRFTable
                {
                    PagibigNo = e.PagibigNo,
                    LastName = e.LastName,
                    FirstName = e.FirstName,
                    MiddleName = e.MiddleName,
                    PeriodCovered = dto.PeriodName,
                    EEShare = e.StatutoryPosted.Where(f => f.SysCode == "EEPAG").Sum(f => f.Amount),
                    ERShare = e.StatutoryPosted.Where(f => f.SysCode == "ERPAG").Sum(f => f.Amount),
                }).ToList();

            var employer = await _employersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new ReportsEmployerDto
                {
                    EmployerName = e.EmployerName,
                    EmployerAddress = e.Address1
                }).FirstOrDefaultAsync();

            var reportSignatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new
                {
                    Signatory = new ReportsSignatoryDto
                    {
                        SignatoryName = e.Signatories.SignatoryName,
                        Position = e.Signatories.Position
                    },
                    PagibigEmployerNo = e.Signatories.CompanyStatutoryInfo
                        .Where(f => f.StatutoryType == "PAGIBIG")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault()
                }).FirstOrDefaultAsync();

            return new GetReportsForPagibigMRCRF()
            {
                PagibigMRCRF = output.Where(e => e.EEShare > 0 || e.ERShare > 0).ToList(),
                Employer = employer,
                Signatory = reportSignatory.Signatory,
                PagibigEmployerNo = reportSignatory.PagibigEmployerNo
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPayrollRegister> GetMonthlyPayrollRegister(ReportsDto dto)
        {
            var payrollRegister = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeName = e.FullName,
                    EmployeeId = e.EmployeeCode,

                    Statutory = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Select(g => new {
                                StatutoryName = g.StatutorySubTypes.DisplayName,
                                g.StatutorySubTypes.SysCode,
                                Value = g.Amount
                            }))
                        .ToList(),
                    Timesheet = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.TimesheetPosted
                            .Select(g => new PayrollRegisterStatutory
                            {
                                StatutoryName = g.TimesheetTypes.DisplayName,
                                Value = g.Amount
                            }))
                        .ToList(),
                    Earnings = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.TimesheetPosted
                            .Select(g => new PayrollRegisterStatutory
                            {
                                StatutoryName = g.TimesheetTypes.DisplayName,
                                Value = g.Amount
                            }))
                        .ToList(),
                    Deductions = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.DeductionPosted
                            .Select(g => new PayrollRegisterStatutory
                            {
                                StatutoryName = g.DeductionTypes.DisplayName,
                                Value = g.Amount
                            }))
                        .ToList(),
                    //Statutory = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Select(f => new
                    //    {
                    //        StatutoryName = f.StatutorySubTypes.DisplayName,
                    //        f.StatutorySubTypes.SysCode,
                    //        Value = f.Amount
                    //    }).ToList(),
                    //Timesheet = e.TimesheetPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Select(f => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = f.TimesheetTypes.DisplayName,
                    //        Value = f.Amount
                    //    }).ToList(),
                    //Earnings = e.EarningPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Select(f => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = f.EarningTypes.DisplayName,
                    //        Value = f.Amount
                    //    }).ToList(),
                    //Deductions = e.DeductionPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Select(e => new PayrollRegisterStatutory
                    //    {
                    //        StatutoryName = e.DeductionTypes.DisplayName,
                    //        Value = e.Amount
                    //    }).ToList(),
                    Loans = e.LoanPosted
                        .Where(f => f.PayPeriod.PayDate.Year == dto.Year && f.PayPeriod.PayDate.Month == dto.Month)
                        .Select(e => new PayrollRegisterStatutory
                        {
                            StatutoryName = e.PayDate.ToString("MM/dd/yyyy"),
                            Value = e.Amount
                        }).ToList()
                }).ToListAsync();

            var output = payrollRegister
                .Select(e => new PayrollRegisterTable
                {
                    EmployeeName = e.EmployeeName,
                    EmployeeId = e.EmployeeId,
                    PayrollRegisterTimesheet = e.Timesheet,
                    PayrollRegisterEarnings = e.Earnings,
                    PayrollRegisterLoans = e.Loans,
                    PayrollRegisterDeductions = e.Deductions,
                    WTax = e.Statutory.Where(f => f.SysCode == "WTAX").Select(f => f.Value).FirstOrDefault(),
                    Sss = e.Statutory.Where(f => f.SysCode == "EESSS").Select(f => f.Value).FirstOrDefault(),
                    Philhealth = e.Statutory.Where(f => f.SysCode == "EEPH").Select(f => f.Value).FirstOrDefault(),
                    Pagibig = e.Statutory.Where(f => f.SysCode == "EEPAG").Select(f => f.Value).FirstOrDefault(),
                    SssEr = e.Statutory.Where(f => f.SysCode == "ERSSS").Select(f => f.Value).FirstOrDefault(),
                    Ecc = e.Statutory.Where(f => f.SysCode == "ERECC").Select(f => f.Value).FirstOrDefault(),
                    PhEr = e.Statutory.Where(f => f.SysCode == "ERPH").Select(f => f.Value).FirstOrDefault(),
                    PagEr = e.Statutory.Where(f => f.SysCode == "ERPAG").Select(f => f.Value).FirstOrDefault(),
                }).ToList();

            return new GetReportsForPayrollRegister()
            {
                PayrollRegister = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPhilhealthER2> GetMonthlyPhilhealthER2(ReportsDto dto)
        {
            var philhealthER2 = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new PhilhealthER2Table
                {
                    PhilhealthNo = e.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "PHEALTH")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),
                    EmployeeName = e.FullName,
                    Position = e.Position,
                    Salary = e.EmployeePayroll.Select(f => f.Salary).FirstOrDefault(),
                    DateOfEmployment = e.DateEmployed,
                    PreviousEmployer = e.Employers.Select(f => f.EmployerName).FirstOrDefault()
                }).ToListAsync();

            var employer = await _employersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new ReportsEmployerDto
                {
                    EmployerName = e.EmployerName,
                    EmployerAddress = e.Address1
                }).FirstOrDefaultAsync();

            var reportSignatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new
                {
                    Signatory = new ReportsSignatoryDto
                    {
                        SignatoryName = e.Signatories.SignatoryName,
                    },
                    PhilhealtEmployerNo = e.Signatories.CompanyStatutoryInfo
                        .Where(f => f.StatutoryType == "PAGIBIG")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault()
                }).FirstOrDefaultAsync();

            return new GetReportsForPhilhealthER2()
            {
                PhilhealthER2 = philhealthER2,
                Employer = employer,
                Signatory = reportSignatory.Signatory,
                PhilhealtEmployerNo = reportSignatory.PhilhealtEmployerNo
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPhilhealthRF1> GetMonthlyPhilhealthRF1(ReportsDto dto)
        {
            var philhealthRF1 = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new PhilhealthRF1Table
                {
                    PhilhealthNo = e.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "PHEALTH")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),
                    LastName = e.LastName,
                    FirstName = e.FirstName,
                    MiddleName = e.MiddleName,
                    DateOfBirth = e.DateOfBirth,
                    Sex = e.Gender,
                    EmployeeStatus = e.EmployeeStatus
                }).ToListAsync();

            return new GetReportsForPhilhealthRF1()
            {
                PhilhealthRF1 = philhealthRF1
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForPhilhealthContribution> GetMonthlyPhilhealthContribution(ReportsDto dto)
        {
            var philhealthContribution = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    PhilhealthNo = e.EmployeeStatutoryInfo
                        .Where(f => f.StatutoryTypes.Code == "PHEALTH")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault(),
                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(g => g.StatutorySubTypes.SysCode == "EEPH" || g.StatutorySubTypes.SysCode == "ERPH")
                            .Select(g => new { g.StatutorySubTypes.SysCode, g.Amount })
                            .ToList()
                        ).ToList()

                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Where(f => f.StatutorySubTypes.SysCode == "EEPH" || f.StatutorySubTypes.SysCode == "ERPH")
                    //    .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = philhealthContribution
                .Select(e => new PhilhealthContributionTable
                {
                    EmployeeId = e.EmployeeId,
                    PhilhealthNo = e.PhilhealthNo,
                    EmployeeName = e.EmployeeName,
                    Employer = e.StatutoryPosted.Where(f => f.SysCode == "ERPH").Sum(f => f.Amount),
                    Employee = e.StatutoryPosted.Where(f => f.SysCode == "EEPH").Sum(f => f.Amount)
                }).ToList();

            output = output.Select(e => { e.Total = e.Employer + e.Employee; return e; }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForPhilhealthContribution()
            {
                Signatory = signatory,
                PhilhealthContribution = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForSssContribution> GetMonthlySssContribution(ReportsDto dto)
        {
            var sssContribution = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    SssNo = e.EmployeeStatutoryInfo
                        .Where(f => f.StatutoryTypes.Code == "SSS")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault(),
                    StatutoryPosted = e.PayrollPosted
                        .Where(f => f.PayrollNo == dto.PayrollNo)
                        .SelectMany(f => f.StatutoryPosted
                            .Where(g => g.StatutorySubTypes.SysCode == "EESSS" || g.StatutorySubTypes.SysCode == "ERSSS")
                            .Select(g => new { g.StatutorySubTypes.SysCode, g.Amount })
                            .ToList()
                        ).ToList()

                    //StatutoryPosted = e.StatutoryPosted
                    //    .Where(f => f.PayrollPosted.PayPeriod.PayDate.Year == dto.Year && f.PayrollPosted.PayPeriod.PayDate.Month == dto.Month)
                    //    .Where(f => f.StatutorySubTypes.SysCode == "EESSS" || f.StatutorySubTypes.SysCode == "ERSSS")
                    //    .Select(f => new { f.StatutorySubTypes.SysCode, f.Amount })
                    //    .ToList()
                }).ToListAsync();

            var output = sssContribution
                .Select(e => new SssContributionTable
                {
                    EmployeeId = e.EmployeeId,
                    SssNo = e.SssNo,
                    EmployeeName = e.EmployeeName,
                    Employer = e.StatutoryPosted.Where(f => f.SysCode == "ERSSS").Sum(f => f.Amount),
                    Employee = e.StatutoryPosted.Where(f => f.SysCode == "EESSS").Sum(f => f.Amount)
                }).ToList();

            output = output.Select(e => { e.Total = e.Employer + e.Employee; return e; }).ToList();

            var signatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new ReportsSignatoryDto
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    Position = e.Signatories.Position,
                }).FirstOrDefaultAsync();

            return new GetReportsForSssContribution()
            {
                Signatory = signatory,
                SssContribution = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForSSSML2> GetMonthlySSSML2(ReportsDto dto)
        {
            var sssML2 = await _employeeLoansRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.Employees.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SSSML2Table
                {
                    SSNo = e.Employees.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "SSS")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),
                    BorrowerName = e.Employees.FullName,
                    LoanType = e.Loans.LoanTypes.SysCode,
                    DateGranted = e.ApprovedDate,
                    LoanAmount = e.LoanAmount,
                    CurrentAmountDue = 0,
                    OverdueAmountDue = 0,
                    Remarks = e.Remarks
                }).ToListAsync();

            var employer = await _employersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new ReportsEmployerDto
                {
                    EmployerName = e.EmployerName
                }).FirstOrDefaultAsync();

            var reportSignatory = await _reportSignatoriesRepository.GetAll()
                .Where(e => e.ReportId == dto.ReportId)
                .Select(e => new
                {
                    Signatory = new ReportsSignatoryDto
                    {
                        SignatoryName = e.Signatories.SignatoryName,
                    },
                    SSSEmployerNo = e.Signatories.CompanyStatutoryInfo
                        .Where(f => f.StatutoryType == "SSS")
                        .Select(f => f.StatutoryAcctNo)
                        .FirstOrDefault()
                }).FirstOrDefaultAsync();

            return new GetReportsForSSSML2()
            {
                SSSML2 = sssML2,
                Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dto.Month.Value),
                Year = dto.Year,
                Employer = employer,
                Signatory = reportSignatory.Signatory,
                SSSEmployerNo = reportSignatory.SSSEmployerNo
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForSSSR3> GetQuarterlySSSR3(ReportsDto dto)
        {
            var sssR3 = await _employeeLoansRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.Employees.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SSSR3Table
                {
                    SSSNo = e.Employees.EmployeeStatutoryInfo
                        .Where(e => e.StatutoryTypes.Code == "SSS")
                        .Select(e => e.StatutoryAcctNo)
                        .FirstOrDefault(),
                    NameOfMember = e.Employees.FullName
                    //NameOfMember = e.Loans.LoanTypes.SysCode
                }).ToListAsync();

            return new GetReportsForSSSR3()
            {
                SSSR3 = sssR3
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalist> GetAnnualAlphalist(ReportsDto dto)
        {
            var alphalist = await _employeeLoansRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.PayCodeId),
                    e => e.Employees.EmployeePayroll.Any(f => f.PayCodeId.ToString() == dto.PayCodeId))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistTable
                {
                    //SSSNo = e.PayrollPosted.Employees.EmployeeStatutoryInfo
                    //    .Where(e => e.StatutorySubTypes.SysCode == "EESSS")
                    //    .Select(e => e.StatutoryAcctNo)
                    //    .FirstOrDefault(),
                    NameOfEmployees = e.Employees.FullName
                    //NameOfMember = e.Loans.LoanTypes.SysCode
                }).ToListAsync();

            return new GetReportsForAlphalist()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalistNonMWE1> GetAnnualAlphalistNonMWE1(ReportsDto dto)
        {
            var alphalist = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistNonMWE1Table
                {
                    LastName = e.LastName,
                    FirstName = e.FirstName,
                    MiddleName = e.MiddleName
                }).ToListAsync();

            return new GetReportsForAlphalistNonMWE1()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalistNonMWE2> GetAnnualAlphalistNonMWE2(ReportsDto dto)
        {
            var alphalist = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistNonMWE2Table
                {
                    NameOfEmployees = e.FullName
                }).ToListAsync();

            return new GetReportsForAlphalistNonMWE2()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalistNonMWE3> GetAnnualAlphalistNonMWE3(ReportsDto dto)
        {
            var alphalist = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistNonMWE3Table
                {
                    NameOfEmployees = e.FullName
                }).ToListAsync();

            return new GetReportsForAlphalistNonMWE3()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalistMWE1> GetAnnualAlphalistMWE1(ReportsDto dto)
        {
            var alphalist = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistMWE1Table
                {
                    LastName = e.LastName,
                    FirstName = e.FirstName,
                    MiddleName = e.MiddleName
                }).ToListAsync();

            return new GetReportsForAlphalistMWE1()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalistMWE2> GetAnnualAlphalistMWE2(ReportsDto dto)
        {
            var alphalist = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistMWE2Table
                {
                    NameOfEmployees = e.FullName
                }).ToListAsync();

            return new GetReportsForAlphalistMWE2()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAlphalistMWE3> GetAnnualAlphalistMWE3(ReportsDto dto)
        {
            var alphalist = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new AlphalistMWE3Table
                {
                    NameOfEmployees = e.FullName
                }).ToListAsync();

            return new GetReportsForAlphalistMWE3()
            {
                Alphalist = alphalist
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForBIR2316> GetAnnualBIR2316(ReportsDto dto)
        {
            var bir2316 = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new BIR2316Table
                {
                    EmployeeName = e.FullName
                }).ToListAsync();

            return new GetReportsForBIR2316()
            {
                BIR2316 = bir2316
            };
        }
    }
}
