﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_CompanyStatutoryInfo)]
    public class CompanyStatutoryInfoAppService : ePayAppServiceBase, ICompanyStatutoryInfoAppService
    {
        private readonly IRepository<CompanyStatutoryInfo> _companyStatutoryInfoRepository;
        private readonly IRepository<StatutoryTypes> _statutoryTypesRepository;

        public CompanyStatutoryInfoAppService(IRepository<CompanyStatutoryInfo> companyStatutoryInfoRepository,
             IRepository<StatutoryTypes> statutoryTypesRepository)
        {
            _companyStatutoryInfoRepository = companyStatutoryInfoRepository;
            _statutoryTypesRepository = statutoryTypesRepository;
        }

        public async Task<PagedResultDto<GetCompanyStatutoryInfoForViewDto>> GetAll(GetAllCompanyStatutoryInfoInput input)
        {
            var companyStatutoryInfo = await _companyStatutoryInfoRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(o => new
                {
                    o.Signatories.SignatoryName,
                    o.StatutoryAcctNo,
                    o.Branch,
                    o.StatutoryType
                }).ToListAsync();

            var statutoryTypes = await _statutoryTypesRepository.GetAll()
                .OrderBy(e => e.Code)
                .Select(e => new { e.Code, e.Description })
                .ToListAsync();

            var joinedStatutoryTypesAndCompanyStatutoryInfo = statutoryTypes
                .GroupJoin(companyStatutoryInfo, type => type.Code, info => info.StatutoryType,
                (type, info) => new GetCompanyStatutoryInfoForViewDto
                {
                    CompanyStatutoryInfo = new CompanyStatutoryInfoDto
                    {
                        StatutoryType = type.Description,
                        StatutoryAcctNo = info.Count() > 0 ? info.First().StatutoryAcctNo : null,
                        SignatoryName = info.Count() > 0 ? info?.First()?.SignatoryName : null,
                        Branch = info.Count() > 0 ? info?.First()?.Branch : null,
                        Id = type.Code
                    }
                }).ToList();

            return new PagedResultDto<GetCompanyStatutoryInfoForViewDto>(
                statutoryTypes.Count,
                joinedStatutoryTypesAndCompanyStatutoryInfo
            );
        }

        public async Task<GetCompanyStatutoryInfoForViewDto> GetCompanyStatutoryInfoForView(string id)
        {
            var output = await _statutoryTypesRepository.GetAll()
                .Where(e => e.Code == id)
                .OrderBy(e => e.Code)
                .Select(e => new CompanyStatutoryInfoDto
                {
                    StatutoryType = e.Description,
                    Id = e.Code
                }).FirstOrDefaultAsync();

            var companyStatutoryInfo = await _companyStatutoryInfoRepository.GetAll()
                .Where(e => e.StatutoryType == id)
                .Select(e => new
                {
                    SignatoryName = e.Signatories.SignatoryName,
                    e.StatutoryAcctNo,
                    e.Branch
                }).FirstOrDefaultAsync();

            if (companyStatutoryInfo != null)
            {
                output.SignatoryName = companyStatutoryInfo.SignatoryName;
                output.StatutoryAcctNo = companyStatutoryInfo.StatutoryAcctNo;
                output.Branch = companyStatutoryInfo.Branch;
            }

            return new GetCompanyStatutoryInfoForViewDto
            {
                CompanyStatutoryInfo = output
            };
        }

        [AbpAuthorize(AppPermissions.Pages_CompanyStatutoryInfo_Edit)]
        public async Task<GetCompanyStatutoryInfoForEditOutput> GetCompanyStatutoryInfoForEdit(string id)
        {
            var companyStatutoryInfo = await _companyStatutoryInfoRepository.GetAll().Where(e => e.StatutoryType == id).FirstOrDefaultAsync();
            var output = await _statutoryTypesRepository.GetAll()
                .Where(e => e.Code == id)
                .OrderBy(e => e.Code)
                .Select(e => new CreateOrEditCompanyStatutoryInfoDto
                {
                    StatutoryType = e.Description,
                }).FirstOrDefaultAsync();

            if (companyStatutoryInfo != null)
            {
                output.SignatoryId = companyStatutoryInfo.SignatoryId;
                output.StatutoryAcctNo = companyStatutoryInfo.StatutoryAcctNo;
                output.Branch = companyStatutoryInfo.Branch;
                output.Id = companyStatutoryInfo.StatutoryType;
            }

            //var output = new GetCompanyStatutoryInfoForEditOutput { CompanyStatutoryInfo = ObjectMapper.Map<CreateOrEditCompanyStatutoryInfoDto>(companyStatutoryInfo) };

            return new GetCompanyStatutoryInfoForEditOutput
            {
                CompanyStatutoryInfo = output
            };
        }

        public async Task CreateOrEdit(CreateOrEditCompanyStatutoryInfoDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_CompanyStatutoryInfo_Create)]
        protected virtual async Task Create(CreateOrEditCompanyStatutoryInfoDto input)
        {
            var companyStatutoryInfo = ObjectMapper.Map<CompanyStatutoryInfo>(input);
            await _companyStatutoryInfoRepository.InsertAsync(companyStatutoryInfo);
        }

        [AbpAuthorize(AppPermissions.Pages_CompanyStatutoryInfo_Edit)]
        protected virtual async Task Update(CreateOrEditCompanyStatutoryInfoDto input)
        {
            var companyStatutoryInfo = await _companyStatutoryInfoRepository.GetAll()
                .Where(e => e.StatutoryType == input.Id)
                .FirstOrDefaultAsync();

            companyStatutoryInfo.StatutoryAcctNo = input.StatutoryAcctNo;
            companyStatutoryInfo.Branch = input.Branch;
            companyStatutoryInfo.SignatoryId = input.SignatoryId;

            //ObjectMapper.Map(input, companyStatutoryInfo);
        }
    }
}