﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OTSheet)]
    public class OTSheetAppService : ePayAppServiceBase, IOTSheetAppService
    {
        private readonly IRepository<OTSheet> _otSheetRepository;
        private readonly IRepository<PayPeriod> _payPeriodRepository;
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IRepository<Process> _processRepository;

        public OTSheetAppService(
            IRepository<OTSheet> otSheetRepository, 
            IRepository<PayPeriod> payPeriodRepository,
            IRepository<OvertimeTypes> overtimeTypesRepository,
            IRepository<Process> processRepository)
        {
            _otSheetRepository = otSheetRepository;
            _payPeriodRepository = payPeriodRepository;
            _overtimeTypesRepository = overtimeTypesRepository;
            _processRepository = processRepository;
        }

        public async Task<IList<decimal>> GetAll(GetAllOTSheetInput input)
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            return await _otSheetRepository.GetAll()
                .OrderBy(e => e.OvertimeTypes.OvertimeMainTypes.DisplayOrder)
                .ThenBy(e => e.OvertimeTypes.OvertimeSubTypes.DisplayOrder)
                .Where(e => e.EmployeeId == input.EmployeeIdFilter && e.Process.PayrollNo == currentPayrollNo)
                .Select(e => e.Hrs)
                .ToListAsync();
        }

        public async Task CreateOrEdit(CreateOrEditOTSheetDto input)
        {
            if (input.EmployeeId == 0)
                return;

            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var otsheet = await _otSheetRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeId && e.Process.PayrollNo == currentPayrollNo)
                .OrderBy(e => e.OvertimeTypes.OvertimeMainTypes.DisplayOrder)
                .ThenBy(e => e.OvertimeTypes.OvertimeSubTypes.DisplayOrder)
                .ToListAsync();

            if (otsheet.Count > 0)
            {
                await Update(otsheet, input);
            }
            else
            {
                await Create(currentPayrollNo, input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheet_Create)]
        protected virtual async Task Create(int payrollNo, CreateOrEditOTSheetDto input)
        {
            var otTypes = await _overtimeTypesRepository.GetAll()
                .OrderBy(e => e.OvertimeMainTypes.DisplayOrder)
                .ThenBy(e => e.OvertimeSubTypes.DisplayOrder)
                .Select(e => e.Id)
                .ToListAsync();

            var processId = await _processRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.PayrollNo == payrollNo)
                    .Select(e => e.Id)
                    .FirstOrDefaultAsync();

            for (var i = 0; i < otTypes.Count; i++)
            {
                var otsheet = new OTSheet
                {
                    EmployeeId = input.EmployeeId,
                    ProcessId = processId,
                    OvertimeTypeId = otTypes[i],
                    Hrs = input.Rate[i] ?? 0,
                    Mins = 0,
                };

                await _otSheetRepository.InsertAsync(otsheet);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheet_Edit)]
        protected virtual Task Update(IList<OTSheet> otsheet, CreateOrEditOTSheetDto input)
        {
            for (var i = 0; i < otsheet.Count; i++)
            {
                otsheet[i].Hrs = input.Rate[i] ?? 0;
            }

            return Task.CompletedTask;
        }
    }
}