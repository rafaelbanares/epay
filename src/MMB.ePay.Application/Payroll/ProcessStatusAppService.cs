﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_ProcessStatus)]
    public class ProcessStatusAppService : ePayAppServiceBase, IProcessStatusAppService
    {
        private readonly IRepository<ProcessStatus> _processStatusRepository;

        public ProcessStatusAppService(IRepository<ProcessStatus> processStatusRepository)
        {
            _processStatusRepository = processStatusRepository;
        }

        public async Task<PagedResultDto<GetProcessStatusForViewDto>> GetAll(GetAllProcessStatusInput input)
        {
            var filteredProcessStatus = _processStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter) || e.Responsible.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                        .WhereIf(input.MinStepFilter != null, e => e.Step >= input.MinStepFilter)
                        .WhereIf(input.MaxStepFilter != null, e => e.Step <= input.MaxStepFilter)
                        .WhereIf(input.NextStatusFilter != null, e => e.NextStatus == input.NextStatusFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ResponsibleFilter), e => e.Responsible == input.ResponsibleFilter);

            var pagedAndFilteredProcessStatus = filteredProcessStatus
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var processStatus = from o in pagedAndFilteredProcessStatus
                                select new GetProcessStatusForViewDto()
                                {
                                    ProcessStatus = new ProcessStatusDto
                                    {
                                        DisplayName = o.DisplayName,
                                        Step = o.Step,
                                        NextStatus = o.NextStatusNavigation.DisplayName,
                                        Responsible = o.Responsible,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredProcessStatus.CountAsync();

            return new PagedResultDto<GetProcessStatusForViewDto>(
                totalCount,
                await processStatus.ToListAsync()
            );
        }

        public async Task<GetProcessStatusForViewDto> GetProcessStatusForView(int id)
        {
            var processStatus = await _processStatusRepository.GetAsync(id);

            var output = new GetProcessStatusForViewDto { ProcessStatus = ObjectMapper.Map<ProcessStatusDto>(processStatus) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessStatus_Edit)]
        public async Task<GetProcessStatusForEditOutput> GetProcessStatusForEdit(EntityDto input)
        {
            var processStatus = await _processStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProcessStatusForEditOutput { ProcessStatus = ObjectMapper.Map<CreateOrEditProcessStatusDto>(processStatus) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProcessStatusDto input)
        {
            if (!input.Id.HasValue)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessStatus_Create)]
        protected virtual async Task Create(CreateOrEditProcessStatusDto input)
        {
            var processStatus = ObjectMapper.Map<ProcessStatus>(input);
            await _processStatusRepository.InsertAsync(processStatus);
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessStatus_Edit)]
        protected virtual async Task Update(CreateOrEditProcessStatusDto input)
        {
            var processStatus = await _processStatusRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, processStatus);
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessStatus_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _processStatusRepository.DeleteAsync(input.Id);
        }
    }
}