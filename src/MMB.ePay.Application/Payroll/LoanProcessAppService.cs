﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_LoanProcess)]
    public class LoanProcessAppService : ePayAppServiceBase, ILoanProcessAppService
    {
        private readonly IRepository<LoanProcess> _loanProcessRepository;

        public LoanProcessAppService(IRepository<LoanProcess> loanProcessRepository)
        {
            _loanProcessRepository = loanProcessRepository;
        }

        public async Task<PagedResultDto<GetLoanProcessForViewDto>> GetAll(GetAllLoanProcessInput input)
        {

            var filteredLoanProcess = _loanProcessRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter);

            var pagedAndFilteredLoanProcess = filteredLoanProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var loanProcess = from o in pagedAndFilteredLoanProcess
                              select new GetLoanProcessForViewDto()
                              {
                                  LoanProcess = new LoanProcessDto
                                  {
                                      PayrollNo = o.PayrollNo,
                                      Amount = o.Amount,
                                      EmployeeId = o.EmployeeId,
                                      EmployeeLoanId = o.EmployeeLoanId,
                                      Id = o.Id
                                  }
                              };

            var totalCount = await filteredLoanProcess.CountAsync();

            return new PagedResultDto<GetLoanProcessForViewDto>(
                totalCount,
                await loanProcess.ToListAsync()
            );
        }

        public async Task<GetLoanProcessForViewDto> GetLoanProcessForView(int id)
        {
            var loanProcess = await _loanProcessRepository.GetAsync(id);

            var output = new GetLoanProcessForViewDto { LoanProcess = ObjectMapper.Map<LoanProcessDto>(loanProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LoanProcess_Edit)]
        public async Task<GetLoanProcessForEditOutput> GetLoanProcessForEdit(EntityDto input)
        {
            var loanProcess = await _loanProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLoanProcessForEditOutput { LoanProcess = ObjectMapper.Map<CreateOrEditLoanProcessDto>(loanProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLoanProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LoanProcess_Create)]
        protected virtual async Task Create(CreateOrEditLoanProcessDto input)
        {
            var loanProcess = ObjectMapper.Map<LoanProcess>(input);

            await _loanProcessRepository.InsertAsync(loanProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanProcess_Edit)]
        protected virtual async Task Update(CreateOrEditLoanProcessDto input)
        {
            var loanProcess = await _loanProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, loanProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _loanProcessRepository.DeleteAsync(input.Id);
        }
    }
}