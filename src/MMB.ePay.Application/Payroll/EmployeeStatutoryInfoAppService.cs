﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EmployeeStatutoryInfo)]
    public class EmployeeStatutoryInfoAppService : ePayAppServiceBase, IEmployeeStatutoryInfoAppService
    {
        private readonly IRepository<EmployeeStatutoryInfo> _employeeStatutoryInfoRepository;
        private readonly IRepository<StatutoryTypes> _statutoryTypesRepository;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;

        public EmployeeStatutoryInfoAppService(
            IRepository<EmployeeStatutoryInfo> employeeStatutoryInfoRepository,
            IRepository<StatutoryTypes> statutoryTypesRepository,
            IDbContextProvider<ePayDbContext> dbContextProvider)
        {
            _employeeStatutoryInfoRepository = employeeStatutoryInfoRepository;
            _statutoryTypesRepository = statutoryTypesRepository;
            _dbContextProvider = dbContextProvider;
        }

        //[AbpAuthorize(AppPermissions.Pages_EmployeeStatutoryInfo_Edit)]
        //public async Task<GetEmployeeStatutoryInfoForEditOutput> GetEmployeeStatutoryInfoForEdit(EntityDto input)
        //{
        //    var employeeStatutoryInfo = await _employeeStatutoryInfoRepository.GetAll()
        //        .Where(e => e.EmployeeId == input.Id)
        //        .Select(e => new CreateOrEditEmployeeStatutoryInfoDto
        //        {
        //            StatutoryType = e.StatutoryType,
        //            StatutoryAcctNo = e.StatutoryAcctNo
        //        }).ToListAsync();

        //    var output = new GetEmployeeStatutoryInfoForEditOutput
        //    {
        //        EmployeeStatutoryInfo = employeeStatutoryInfo
        //    };

        //    return output;
        //}

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_EmployeeStatutoryInfo_Create, AppPermissions.Pages_EmployeeStatutoryInfo_Edit)]
        public async Task CreateOrEdit(GetEmployeeStatutoryInfoForEditOutput input)
        {
            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);

            var delete = await _employeeStatutoryInfoRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.EmployeeId == input.EmployeeId)
                .ToListAsync();

            var dbContext = _dbContextProvider.GetDbContext();
            dbContext.EmployeeStatutoryInfo.RemoveRange(delete);
            await UnitOfWorkManager.Current.SaveChangesAsync();

            var employeeStatutoryInfos = await _statutoryTypesRepository.GetAll()
                .OrderBy(e => e.Code)
                .Select(e => new 
                {
                    e.Code,
                }).ToListAsync();

            for(var i = 0;  i < employeeStatutoryInfos.Count; i++)
            {
                var employeeStatutoryInfo = new EmployeeStatutoryInfo
                {
                    TenantId = AbpSession.TenantId.Value,
                    EmployeeId = input.EmployeeId,
                    StatutoryAcctNo = input.StatutoryAccountNo[i],
                    StatutoryType = employeeStatutoryInfos[i].Code,
                };

                await _employeeStatutoryInfoRepository.InsertAsync(employeeStatutoryInfo);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeStatutoryInfo_Edit)]
        public async Task<IList<string>> GetEmployeeStatutoryInfoForEdit(EntityDto input)
        {
            var output = await _statutoryTypesRepository.GetAll()
                .OrderBy(e => e.Code)
                .Select(e =>  e.EmployeeStatutoryInfo.Where(f => f.EmployeeId == input.Id).Select(f => f.StatutoryAcctNo).First())
                .ToListAsync();

            return output;
        }
    }
}