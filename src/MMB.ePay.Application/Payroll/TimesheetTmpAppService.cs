﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_TimesheetTmp)]
    public class TimesheetTmpAppService : ePayAppServiceBase, ITimesheetTmpAppService
    {
        private readonly IRepository<TimesheetTmp> _timesheetTmpRepository;

        public TimesheetTmpAppService(IRepository<TimesheetTmp> timesheetTmpRepository)
        {
            _timesheetTmpRepository = timesheetTmpRepository;
        }

        public async Task<PagedResultDto<GetTimesheetTmpForViewDto>> GetAll(GetAllTimesheetTmpInput input)
        {

            var filteredTimesheetTmp = _timesheetTmpRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinCostCenterFilter != null, e => e.CostCenter >= input.MinCostCenterFilter)
                        .WhereIf(input.MaxCostCenterFilter != null, e => e.CostCenter <= input.MaxCostCenterFilter)
                        .WhereIf(input.MinDaysFilter != null, e => e.Days >= input.MinDaysFilter)
                        .WhereIf(input.MaxDaysFilter != null, e => e.Days <= input.MaxDaysFilter)
                        .WhereIf(input.MinHrsFilter != null, e => e.Hrs >= input.MinHrsFilter)
                        .WhereIf(input.MaxHrsFilter != null, e => e.Hrs <= input.MaxHrsFilter)
                        .WhereIf(input.MinMinsFilter != null, e => e.Mins >= input.MinMinsFilter)
                        .WhereIf(input.MaxMinsFilter != null, e => e.Mins <= input.MaxMinsFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinTimesheetTypeIdFilter != null, e => e.TimesheetTypeId >= input.MinTimesheetTypeIdFilter)
                        .WhereIf(input.MaxTimesheetTypeIdFilter != null, e => e.TimesheetTypeId <= input.MaxTimesheetTypeIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredTimesheetTmp = filteredTimesheetTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var timesheetTmp = from o in pagedAndFilteredTimesheetTmp
                               select new GetTimesheetTmpForViewDto()
                               {
                                   TimesheetTmp = new TimesheetTmpDto
                                   {
                                       PayrollNo = o.PayrollNo,
                                       CostCenter = o.CostCenter,
                                       Days = o.Days,
                                       Hrs = o.Hrs,
                                       Mins = o.Mins,
                                       Amount = o.Amount,
                                       EmployeeId = o.EmployeeId,
                                       TimesheetTypeId = o.TimesheetTypeId,
                                       SessionId = o.SessionId,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredTimesheetTmp.CountAsync();

            return new PagedResultDto<GetTimesheetTmpForViewDto>(
                totalCount,
                await timesheetTmp.ToListAsync()
            );
        }

        public async Task<GetTimesheetTmpForViewDto> GetTimesheetTmpForView(int id)
        {
            var timesheetTmp = await _timesheetTmpRepository.GetAsync(id);

            var output = new GetTimesheetTmpForViewDto { TimesheetTmp = ObjectMapper.Map<TimesheetTmpDto>(timesheetTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTmp_Edit)]
        public async Task<GetTimesheetTmpForEditOutput> GetTimesheetTmpForEdit(EntityDto input)
        {
            var timesheetTmp = await _timesheetTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTimesheetTmpForEditOutput { TimesheetTmp = ObjectMapper.Map<CreateOrEditTimesheetTmpDto>(timesheetTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTimesheetTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTmp_Create)]
        protected virtual async Task Create(CreateOrEditTimesheetTmpDto input)
        {
            var timesheetTmp = ObjectMapper.Map<TimesheetTmp>(input);
            await _timesheetTmpRepository.InsertAsync(timesheetTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTmp_Edit)]
        protected virtual async Task Update(CreateOrEditTimesheetTmpDto input)
        {
            var timesheetTmp = await _timesheetTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, timesheetTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _timesheetTmpRepository.DeleteAsync(input.Id);
        }
    }
}