﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_TenantBanks)]
    public class TenantBanksAppService : ePayAppServiceBase, ITenantBanksAppService
    {
        private readonly IRepository<TenantBanks> _tenantBanksRepository;
        private readonly IRepository<BankBranches> _bankBranchesRepository;
        public TenantBanksAppService(IRepository<TenantBanks> tenantBanksRepository, 
            IRepository<BankBranches> bankBranchesRepository)
        {
            _tenantBanksRepository = tenantBanksRepository;
            _bankBranchesRepository = bankBranchesRepository;
        }

        public async Task<PagedResultDto<GetTenantBanksForViewDto>> GetAll(GetAllTenantBanksInput input)
        {

            var filteredTenantBanks = _tenantBanksRepository.GetAll();

            var pagedAndFilteredTenantBanks = filteredTenantBanks
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var tenantBanks = from o in pagedAndFilteredTenantBanks
                               select new GetTenantBanksForViewDto()
                               {
                                   TenantBanks = new TenantBanksDto
                                   {
                                       BranchName = o.BankBranches.BranchName,
                                       BankAccountNumber = o.BankAccountNumber,
                                       BankAccountType = o.BankAccountType,
                                       BranchNumber = o.BranchNumber,
                                       CorporateId = o.CorporateId,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredTenantBanks.CountAsync();

            return new PagedResultDto<GetTenantBanksForViewDto>(
                totalCount,
                await tenantBanks.ToListAsync()
            );
        }

        public async Task<GetTenantBanksForViewDto> GetTenantBanksForView(int id)
        {
            var output = await _tenantBanksRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetTenantBanksForViewDto
                {
                    TenantBanks = new TenantBanksDto
                    {
                        BankAccountNumber = e.BankAccountNumber,
                        BankAccountType = e.BankAccountType,
                        BranchNumber = e.BranchNumber,
                        BankBranch = e.BankBranches.BranchName,
                        CorporateId = e.CorporateId
                    }
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_TenantBanks_Edit)]
        public async Task<GetTenantBanksForEditOutput> GetTenantBanksForEdit(EntityDto input)
        {
            var tenantBanks = await _tenantBanksRepository.FirstOrDefaultAsync(input.Id);
            var bankId = await _bankBranchesRepository.GetAll()
                .Where(e => e.Id == tenantBanks.BankBranchId)
                .Select(e => e.BankId)
                .FirstOrDefaultAsync();

            var output = new GetTenantBanksForEditOutput { TenantBanks = ObjectMapper.Map<CreateOrEditTenantBanksDto>(tenantBanks) };
            output.TenantBanks.BankId = bankId;

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTenantBanksDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TenantBanks_Create)]
        protected virtual async Task Create(CreateOrEditTenantBanksDto input)
        {
            var tenantBanks = ObjectMapper.Map<TenantBanks>(input);
            await _tenantBanksRepository.InsertAsync(tenantBanks);
        }

        [AbpAuthorize(AppPermissions.Pages_TenantBanks_Edit)]
        protected virtual async Task Update(CreateOrEditTenantBanksDto input)
        {
            var tenantBanks = await _tenantBanksRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, tenantBanks);
        }
    }
}