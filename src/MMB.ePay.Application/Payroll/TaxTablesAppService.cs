﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_TaxTables)]
    public class TaxTablesAppService : ePayAppServiceBase, ITaxTablesAppService
    {
        private readonly IRepository<TaxTables> _taxTablesRepository;

        public TaxTablesAppService(IRepository<TaxTables> taxTablesRepository)
        {
            _taxTablesRepository = taxTablesRepository;
        }

        public IList<SelectListItem> GetYearList()
        {
            var currentYear = DateTimeOffset.Now.DateTime.Year;

            var year = Enumerable.Range(2019, 7)
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = e.ToString(),
                    Selected = (e == currentYear)
                }).ToList();

            return year;
        }

        public IList<SelectListItem> GetCodeList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "FA", Text = "FA" },
                new SelectListItem { Value = "PCT", Text = "PCT" },
                new SelectListItem { Value = "SAL", Text = "SAL" }
            };
        }

        public async Task<PagedResultDto<GetTaxTablesForViewDto>> GetAll(GetAllTaxTablesInput input)
        {
            var filteredTaxTables = _taxTablesRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.PayFreqFilter), e => e.PayFreq == input.PayFreqFilter)
                .Where(e => e.Year == input.YearFilter);

            var pagedAndFilteredTaxTables = filteredTaxTables
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var taxTables = from o in pagedAndFilteredTaxTables
                            select new GetTaxTablesForViewDto()
                            {
                                TaxTables = new TaxTablesDto
                                {
                                    Year = o.Year,
                                    PayFreq = o.PayFreq,
                                    Code = o.Code,
                                    Bracket1 = o.Bracket1,
                                    Bracket2 = o.Bracket2,
                                    Bracket3 = o.Bracket3,
                                    Bracket4 = o.Bracket4,
                                    Bracket5 = o.Bracket5,
                                    Bracket6 = o.Bracket6,
                                    Bracket7 = o.Bracket7,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredTaxTables.CountAsync();

            return new PagedResultDto<GetTaxTablesForViewDto>(
                totalCount,
                await taxTables.ToListAsync()
            );
        }

        public async Task<GetTaxTablesForViewDto> GetTaxTablesForView(int id)
        {
            var taxTables = await _taxTablesRepository.GetAsync(id);

            var output = new GetTaxTablesForViewDto { TaxTables = ObjectMapper.Map<TaxTablesDto>(taxTables) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_TaxTables_Edit)]
        public async Task<GetTaxTablesForEditOutput> GetTaxTablesForEdit(EntityDto input)
        {
            var taxTables = await _taxTablesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTaxTablesForEditOutput { TaxTables = ObjectMapper.Map<CreateOrEditTaxTablesDto>(taxTables) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTaxTablesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TaxTables_Create)]
        protected virtual async Task Create(CreateOrEditTaxTablesDto input)
        {
            var taxTables = ObjectMapper.Map<TaxTables>(input);
            await _taxTablesRepository.InsertAsync(taxTables);
        }

        [AbpAuthorize(AppPermissions.Pages_TaxTables_Edit)]
        protected virtual async Task Update(CreateOrEditTaxTablesDto input)
        {
            var taxTables = await _taxTablesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, taxTables);
        }
    }
}