﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_StatutoryPosted)]
    public class StatutoryPostedAppService : ePayAppServiceBase, IStatutoryPostedAppService
    {
        private readonly IRepository<StatutoryPosted> _statutoryPostedRepository;

        public StatutoryPostedAppService(IRepository<StatutoryPosted> statutoryPostedRepository)
        {
            _statutoryPostedRepository = statutoryPostedRepository;
        }

        public async Task<PagedResultDto<GetStatutoryPostedForViewDto>> GetAll(GetAllStatutoryPostedInput input)
        {

            var filteredStatutoryPosted = _statutoryPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollPosted.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollPosted.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.StatutoryTypeFilter.HasValue, e => e.StatutorySubTypeId == input.StatutoryTypeFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollPosted.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollPosted.EmployeeId <= input.MaxEmployeeIdFilter);

            var pagedAndFilteredStatutoryPosted = filteredStatutoryPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var statutoryPosted = from o in pagedAndFilteredStatutoryPosted
                                  select new GetStatutoryPostedForViewDto()
                                  {
                                      StatutoryPosted = new StatutoryPostedDto
                                      {
                                          PayrollNo = o.PayrollPosted.PayrollNo,
                                          StatutoryType = o.StatutorySubTypes.DisplayName,
                                          Amount = o.Amount,
                                          EmployeeId = o.PayrollPosted.EmployeeId,
                                          Id = o.Id
                                      }
                                  };

            var totalCount = await filteredStatutoryPosted.CountAsync();

            return new PagedResultDto<GetStatutoryPostedForViewDto>(
                totalCount,
                await statutoryPosted.ToListAsync()
            );
        }

        public async Task<GetStatutoryPostedForViewDto> GetStatutoryPostedForView(int id)
        {
            var statutoryPosted = await _statutoryPostedRepository.GetAsync(id);

            var output = new GetStatutoryPostedForViewDto { StatutoryPosted = ObjectMapper.Map<StatutoryPostedDto>(statutoryPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryPosted_Edit)]
        public async Task<GetStatutoryPostedForEditOutput> GetStatutoryPostedForEdit(EntityDto input)
        {
            var statutoryPosted = await _statutoryPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStatutoryPostedForEditOutput { StatutoryPosted = ObjectMapper.Map<CreateOrEditStatutoryPostedDto>(statutoryPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditStatutoryPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryPosted_Create)]
        protected virtual async Task Create(CreateOrEditStatutoryPostedDto input)
        {
            var statutoryPosted = ObjectMapper.Map<StatutoryPosted>(input);
            await _statutoryPostedRepository.InsertAsync(statutoryPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryPosted_Edit)]
        protected virtual async Task Update(CreateOrEditStatutoryPostedDto input)
        {
            var statutoryPosted = await _statutoryPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, statutoryPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _statutoryPostedRepository.DeleteAsync(input.Id);
        }
    }
}