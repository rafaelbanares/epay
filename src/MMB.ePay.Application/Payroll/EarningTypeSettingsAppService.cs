﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Dto;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EarningTypeSettings)]
    public class EarningTypeSettingsAppService : ePayAppServiceBase, IEarningTypeSettingsAppService
    {
        private readonly IRepository<EarningTypeSettings> _earningTypeSettingsRepository;

        public EarningTypeSettingsAppService(IRepository<EarningTypeSettings> earningTypeSettingsRepository)
        {
            _earningTypeSettingsRepository = earningTypeSettingsRepository;
        }

        public async Task<PagedResultDto<GetEarningTypeSettingsForViewDto>> GetAll(GetAllEarningTypeSettingsInput input)
        {

            var filteredEarningTypeSettings = _earningTypeSettingsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Key.Contains(input.Filter) || e.Value.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.KeyFilter), e => e.Key == input.KeyFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ValueFilter), e => e.Value == input.ValueFilter)
                        .WhereIf(input.MinEarningTypeIdFilter != null, e => e.EarningTypeId >= input.MinEarningTypeIdFilter)
                        .WhereIf(input.MaxEarningTypeIdFilter != null, e => e.EarningTypeId <= input.MaxEarningTypeIdFilter);

            var pagedAndFilteredEarningTypeSettings = filteredEarningTypeSettings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var earningTypeSettings = from o in pagedAndFilteredEarningTypeSettings
                                      select new GetEarningTypeSettingsForViewDto()
                                      {
                                          EarningTypeSettings = new EarningTypeSettingsDto
                                          {
                                              Key = o.Key,
                                              Value = o.Value,
                                              EarningTypeId = o.EarningTypeId,
                                              Id = o.Id
                                          }
                                      };

            var totalCount = await filteredEarningTypeSettings.CountAsync();

            return new PagedResultDto<GetEarningTypeSettingsForViewDto>(
                totalCount,
                await earningTypeSettings.ToListAsync()
            );
        }

        public async Task<GetEarningTypeSettingsForViewDto> GetEarningTypeSettingsForView(int id)
        {
            var earningTypeSettings = await _earningTypeSettingsRepository.GetAsync(id);

            var output = new GetEarningTypeSettingsForViewDto { EarningTypeSettings = ObjectMapper.Map<EarningTypeSettingsDto>(earningTypeSettings) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypeSettings_Edit)]
        public async Task<GetEarningTypeSettingsForEditOutput> GetEarningTypeSettingsForEdit(EntityDto input)
        {
            var earningTypeSettings = await _earningTypeSettingsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEarningTypeSettingsForEditOutput { EarningTypeSettings = ObjectMapper.Map<CreateOrEditEarningTypeSettingsDto>(earningTypeSettings) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEarningTypeSettingsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypeSettings_Create)]
        protected virtual async Task Create(CreateOrEditEarningTypeSettingsDto input)
        {
            var earningTypeSettings = ObjectMapper.Map<EarningTypeSettings>(input);
            await _earningTypeSettingsRepository.InsertAsync(earningTypeSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypeSettings_Edit)]
        protected virtual async Task Update(CreateOrEditEarningTypeSettingsDto input)
        {
            var earningTypeSettings = await _earningTypeSettingsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, earningTypeSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypeSettings_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _earningTypeSettingsRepository.DeleteAsync(input.Id);
        }
    }
}