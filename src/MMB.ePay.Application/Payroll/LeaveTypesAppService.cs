﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_LeaveTypes)]
    public class LeaveTypesAppService : ePayAppServiceBase, ILeaveTypesAppService
    {
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;

        public LeaveTypesAppService(IRepository<LeaveTypes> leaveTypesRepository)
        {
            _leaveTypesRepository = leaveTypesRepository;
        }

        public async Task<PagedResultDto<GetLeaveTypesForViewDto>> GetAll(GetAllLeaveTypesInput input)
        {

            var filteredLeaveTypes = _leaveTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
                        .WhereIf(input.WithPayFilter.HasValue && input.WithPayFilter > -1, e => (input.WithPayFilter == 1 && e.WithPay) || (input.WithPayFilter == 0 && !e.WithPay));

            var pagedAndFilteredLeaveTypes = filteredLeaveTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var leaveTypes = from o in pagedAndFilteredLeaveTypes
                             select new GetLeaveTypesForViewDto()
                             {
                                 LeaveTypes = new LeaveTypesDto
                                 {
                                     DisplayName = o.DisplayName,
                                     Description = o.Description,
                                     WithPay = o.WithPay,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredLeaveTypes.CountAsync();

            return new PagedResultDto<GetLeaveTypesForViewDto>(
                totalCount,
                await leaveTypes.ToListAsync()
            );
        }

        public async Task<GetLeaveTypesForViewDto> GetLeaveTypesForView(int id)
        {
            var leaveTypes = await _leaveTypesRepository.GetAsync(id);

            var output = new GetLeaveTypesForViewDto { LeaveTypes = ObjectMapper.Map<LeaveTypesDto>(leaveTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Edit)]
        public async Task<GetLeaveTypesForEditOutput> GetLeaveTypesForEdit(EntityDto input)
        {
            var leaveTypes = await _leaveTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeaveTypesForEditOutput { LeaveTypes = ObjectMapper.Map<CreateOrEditLeaveTypesDto>(leaveTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLeaveTypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Create)]
        protected virtual async Task Create(CreateOrEditLeaveTypesDto input)
        {
            var leaveTypes = ObjectMapper.Map<LeaveTypes>(input);
            await _leaveTypesRepository.InsertAsync(leaveTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Edit)]
        protected virtual async Task Update(CreateOrEditLeaveTypesDto input)
        {
            var leaveTypes = await _leaveTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, leaveTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _leaveTypesRepository.DeleteAsync(input.Id);
        }
    }
}