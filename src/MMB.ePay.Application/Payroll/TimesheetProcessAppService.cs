﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_TimesheetProcess)]
    public class TimesheetProcessAppService : ePayAppServiceBase, ITimesheetProcessAppService
    {
        private readonly IRepository<TimesheetProcess> _timesheetProcessRepository;

        public TimesheetProcessAppService(IRepository<TimesheetProcess> timesheetProcessRepository)
        {
            _timesheetProcessRepository = timesheetProcessRepository;
        }

        public async Task<PagedResultDto<GetTimesheetProcessForViewDto>> GetAll(GetAllTimesheetProcessInput input)
        {

            var filteredTimesheetProcess = _timesheetProcessRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinCostCenterFilter != null, e => e.CostCenter >= input.MinCostCenterFilter)
                        .WhereIf(input.MaxCostCenterFilter != null, e => e.CostCenter <= input.MaxCostCenterFilter)
                        .WhereIf(input.MinDaysFilter != null, e => e.Days >= input.MinDaysFilter)
                        .WhereIf(input.MaxDaysFilter != null, e => e.Days <= input.MaxDaysFilter)
                        .WhereIf(input.MinHrsFilter != null, e => e.Hrs >= input.MinHrsFilter)
                        .WhereIf(input.MaxHrsFilter != null, e => e.Hrs <= input.MaxHrsFilter)
                        .WhereIf(input.MinMinsFilter != null, e => e.Mins >= input.MinMinsFilter)
                        .WhereIf(input.MaxMinsFilter != null, e => e.Mins <= input.MaxMinsFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinTimesheetTypeIdFilter != null, e => e.TimesheetTypeId >= input.MinTimesheetTypeIdFilter)
                        .WhereIf(input.MaxTimesheetTypeIdFilter != null, e => e.TimesheetTypeId <= input.MaxTimesheetTypeIdFilter);

            var pagedAndFilteredTimesheetProcess = filteredTimesheetProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var timesheetProcess = from o in pagedAndFilteredTimesheetProcess
                                   select new GetTimesheetProcessForViewDto()
                                   {
                                       TimesheetProcess = new TimesheetProcessDto
                                       {
                                           PayrollNo = o.PayrollProcess.PayrollNo,
                                           CostCenter = o.CostCenter,
                                           Days = o.Days,
                                           Hrs = o.Hrs,
                                           Mins = o.Mins,
                                           Amount = o.Amount,
                                           EmployeeId = o.PayrollProcess.EmployeeId,
                                           TimesheetTypeId = o.TimesheetTypeId,
                                           Id = o.Id
                                       }
                                   };

            var totalCount = await filteredTimesheetProcess.CountAsync();

            return new PagedResultDto<GetTimesheetProcessForViewDto>(
                totalCount,
                await timesheetProcess.ToListAsync()
            );
        }

        public async Task<GetTimesheetProcessForViewDto> GetTimesheetProcessForView(int id)
        {
            var timesheetProcess = await _timesheetProcessRepository.GetAsync(id);

            var output = new GetTimesheetProcessForViewDto { TimesheetProcess = ObjectMapper.Map<TimesheetProcessDto>(timesheetProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetProcess_Edit)]
        public async Task<GetTimesheetProcessForEditOutput> GetTimesheetProcessForEdit(EntityDto input)
        {
            var timesheetProcess = await _timesheetProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTimesheetProcessForEditOutput { TimesheetProcess = ObjectMapper.Map<CreateOrEditTimesheetProcessDto>(timesheetProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTimesheetProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetProcess_Create)]
        protected virtual async Task Create(CreateOrEditTimesheetProcessDto input)
        {
            var timesheetProcess = ObjectMapper.Map<TimesheetProcess>(input);
            await _timesheetProcessRepository.InsertAsync(timesheetProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetProcess_Edit)]
        protected virtual async Task Update(CreateOrEditTimesheetProcessDto input)
        {
            var timesheetProcess = await _timesheetProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, timesheetProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _timesheetProcessRepository.DeleteAsync(input.Id);
        }
    }
}