﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_PayPeriod)]
    public class PayPeriodAppService : ePayAppServiceBase, IPayPeriodAppService
    {
        private readonly IRepository<PayPeriod> _payPeriodRepository;

        public PayPeriodAppService(IRepository<PayPeriod> payPeriodRepository)
        {
            _payPeriodRepository = payPeriodRepository;
        }

        public async Task<IList<SelectListItem>> GetYearList()
        {
            var currentYear = DateTime.Today.Year;
            var payPeriods = await _payPeriodRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => e.ApplicableYear)
                .Distinct()
                .ToListAsync();

            return payPeriods
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = e.ToString(),
                    Selected = e == currentYear
                }).ToList();
        }

        public async Task<IList<SelectListItem>> GetPayPeriodListByYear(int year, int month)
        {
            return await _payPeriodRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.ApplicableYear == year
                    && e.ApplicableMonth == month)
                .OrderBy(e => e.PeriodFrom)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = string.Format("{0} - {1}",
                        e.PeriodFrom.ToString("yyyy/MM/dd"),
                        e.PeriodTo.ToString("yyyy/MM/dd"))
                }).ToListAsync();
        }

        public async Task<IList<SelectListItem>> GetPayPeriodList()
        {
            return await _payPeriodRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.PeriodFrom)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = string.Format("{0} - {1}",
                        e.PeriodFrom.ToString("yyyy/MM/dd"),
                        e.PeriodTo.ToString("yyyy/MM/dd"))
                }).ToListAsync();
        }

        public async Task<IList<SelectListItem>> GetApplicableYearList()
        {
            var currentYear = DateTime.Now.Year;
            var applicableYear = await _payPeriodRepository.GetAll()
                .Select(e => e.ApplicableYear)
                .Distinct()
                .OrderBy(e => e)
                .ToListAsync();

            return applicableYear
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = e.ToString(),
                    Selected = (e == currentYear)
                }).ToList();
        }

        public IList<SelectListItem> GetPayrollStatusList()
        {
            var listStatus = new List<SelectListItem>
            {
                new SelectListItem { Value = "ACTIVE", Text = "ACTIVE" },
                new SelectListItem { Value = "HOLD", Text = "HOLD" },
                new SelectListItem { Value = "PAID", Text = "PAID" }
            };

            listStatus.Insert(0, new SelectListItem { Value = string.Empty, Text = "ALL" });

            return listStatus;
        }

        public IList<SelectListItem> GetPayrollYearList()
        {
            var year = DateTimeOffset.Now.DateTime.Year;

            var listYear = new List<SelectListItem>
            {
                new SelectListItem { Value = (year - 1).ToString(), Text = (year - 1).ToString() },
                new SelectListItem { Value = year.ToString(), Text = year.ToString(), Selected = true },
                new SelectListItem { Value = (year + 1).ToString(), Text = (year + 1).ToString() }
            };

            listYear.Insert(0, new SelectListItem { Value = "0", Text = "ALL" });

            return listYear;
        }

        private static string GetPayrollFrequency(string frequencyCode)
        {
            if (frequencyCode == "M")
            {
                return "Monthly";
            }
            else if (frequencyCode == "S")
            {
                return "Semi-Monthly";
            }
            else if (frequencyCode == "W")
            {
                return "Weekly";
            }

            return string.Empty;
        }

        public async Task<GetPayPeriodForProcessDto> GetPayPeriodForProcess(GetPayPeriodForProcessInput input)
        {
            return await _payPeriodRepository.GetAll()
                .Where(e => e.ProcessType == input.ProcessType
                    && e.PayrollFreq == input.PayrollFreq
                    && e.ApplicableYear == input.ApplicableYear
                    && e.PayrollStatus != "POSTED"
                    && e.Process.Count == 0)
                .OrderBy(e => e.PayDate)
                .Select(e => new GetPayPeriodForProcessDto
                {
                    Id = e.Id,
                    PeriodRange = string.Format("{0} - {1}",
                        e.PeriodFrom.ToString("MM/dd/yyyy"),
                        e.PeriodTo.ToString("MM/dd/yyyy"))
                }).FirstOrDefaultAsync();
        }
        public async Task<PagedResultDto<GetPayPeriodForViewDto>> GetAll(GetAllPayPeriodInput input)
        {
            var filteredPayPeriod = _payPeriodRepository.GetAll()
                .Where(e => e.ApplicableYear == input.ApplicableYearFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.PayrollFreqFilter), e => e.PayrollFreq == input.PayrollFreqFilter);

            var pagedAndFilteredPayPeriod = filteredPayPeriod
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var payPeriod = await pagedAndFilteredPayPeriod
                .Select(o => new GetPayPeriodForViewDto()
                {
                    PayPeriod = new PayPeriodDto
                    {
                        PeriodFrom = o.PeriodFrom,
                        PeriodTo = o.PeriodTo,
                        PayDate = o.PayDate,
                        PayrollFreq = o.PayrollFreq,
                        ProcessType = o.ProcessType,
                        Remarks = o.Remarks,
                        PayrollStatus = o.PayrollStatus,
                        Id = o.Id
                    }
                }).ToListAsync();

            payPeriod = payPeriod.Select(e => {
                e.PayPeriod.PayrollFreq = GetPayrollFrequency(e.PayPeriod.PayrollFreq);        
                return e; 
            }).ToList();

            var totalCount = await filteredPayPeriod.CountAsync();

            return new PagedResultDto<GetPayPeriodForViewDto>(
                totalCount,
                payPeriod
            );
        }

        public async Task<GetPayPeriodForViewDto> GetPayPeriodForView(int id)
        {
            //var payPeriod = await _payPeriodRepository.GetAll()
            //    .Where(e => e.Id == id)
            //    .Select(e => new
            //    {
            //        e.PayrollFreq

            //    }).FirstOrDefaultAsync();

            //var output = new GetPayPeriodForViewDto
            //{
            //    PayPeriod = new PayPeriodDto
            //    {

            //    }
            //};

            var payPeriod = await _payPeriodRepository.GetAsync(id);

            var output = new GetPayPeriodForViewDto { PayPeriod = ObjectMapper.Map<PayPeriodDto>(payPeriod) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PayPeriod_Edit)]
        public async Task<GetPayPeriodForEditOutput> GetPayPeriodForEdit(EntityDto input)
        {
            var payPeriod = await _payPeriodRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayPeriodForEditOutput { PayPeriod = ObjectMapper.Map<CreateOrEditPayPeriodDto>(payPeriod) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayPeriodDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PayPeriod_Create)]
        protected virtual async Task Create(CreateOrEditPayPeriodDto input)
        {
            var payPeriod = ObjectMapper.Map<PayPeriod>(input);
            payPeriod.PayrollStatus = "NEW";

            await _payPeriodRepository.InsertAsync(payPeriod);
        }

        [AbpAuthorize(AppPermissions.Pages_PayPeriod_Edit)]
        protected virtual async Task Update(CreateOrEditPayPeriodDto input)
        {
            var payPeriod = await _payPeriodRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, payPeriod);
        }
    }
}