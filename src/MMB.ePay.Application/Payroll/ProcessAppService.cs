﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Process)]
    public class ProcessAppService : ePayAppServiceBase, IProcessAppService
    {
        private readonly IRepository<Process> _processRepository;
        private readonly IRepository<ProcessStatus> _processStatusRepository;
        private readonly IRepository<Reviewers> _reviewersRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<StatutoryProcess> _statutoryProcessRepository;
        private readonly IRepository<PayPeriod> _payPeriodRepository;

        public ProcessAppService(IRepository<Process> processRepository,
            IRepository<ProcessStatus> processStatusRepository,
            IRepository<Reviewers> reviewersRepository,
            IRepository<Employees> employeesRepository,
            IRepository<StatutoryProcess> statutoryProcessRepository,
            IRepository<PayPeriod> payPeriodRepository)
        {
            _processRepository = processRepository;
            _processStatusRepository = processStatusRepository;
            _reviewersRepository = reviewersRepository;
            _employeesRepository = employeesRepository;
            _statutoryProcessRepository = statutoryProcessRepository;
            _payPeriodRepository = payPeriodRepository;
        }

        public IList<SelectListItem> GetProcessTypeList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "REGULAR", Text = "REGULAR" },
                new SelectListItem { Value = "SPECIAL", Text = "SPECIAL" },
                new SelectListItem { Value = "RESIGNED", Text = "RESIGNED" }
            };
        }

        public async Task<GetProcessForProcessingViewDto> GetProcessForProcessingView(int id)
        {
            return await _processRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetProcessForProcessingViewDto
                {
                    Id = e.Id,
                    Step = e.ProcessStatus.Step,
                    ProcessStatus = e.ProcessStatus.DisplayName,
                    IsProcessor = e.CreatorUserId == AbpSession.UserId,
                    PayrollPeriod = string.Format("{0} - {1}",
                        e.PayPeriod.PeriodFrom.ToString("MM/dd/yyyy"),
                        e.PayPeriod.PeriodTo.ToString("MM/dd/yyyy")),
                    PayDate = e.PayPeriod.PayDate
                }).FirstOrDefaultAsync();
        }

        public async Task<GetProcessForPayrollEntryStepDto> GetPayrollEntryStep(int id)
        {
            var output = await _processRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetProcessForPayrollEntryStepDto
                {
                    Step = e.ProcessStatus.Step,
                    Status = e.ProcessStatus.DisplayName,
                    ProcessStatus = e.ProcessStatus.DisplayName,
                }).FirstOrDefaultAsync();

            return output;
        }

        public async Task<GetProcessForProcessingStepDto> GetProcessingStep(int id)
        {
            var process = await _processRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.ProcessStatus.Step,
                    Status = e.ProcessStatus.DisplayName,
                    e.ProcessStatusId,
                    NextStatus = e.ProcessStatus.NextStatusNavigation.DisplayName,
                    CreationTime = e.ProcessHistory.Select(f => f.CreationTime).OrderByDescending(f => f).FirstOrDefault()
                }).FirstOrDefaultAsync();

            var output = new GetProcessForProcessingStepDto
            {
                Step = process.Step,
                Status = process.Status,
                ProcessStatusId = process.ProcessStatusId,
                NextStatus = process.NextStatus,
                ElapsedTime = process.CreationTime.ToString(@"hh\:mm\:ss\:fff")
            };

            return output;
        }

        public async Task<GetProcessForReviewStepDto> GetReviewStep(int id)
        {
            var process = await _processRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.CreatorUserId,
                    e.ProcessStatus.Step,
                    Status = e.ProcessStatus.DisplayName,
                    e.ProcessStatus.SysCode,
                    e.ProcessStatusId,
                    e.PayrollNo,
                    PayrollPeriod = string.Format("{0} - {1}",
                        e.PayPeriod.PeriodFrom.ToString("MM/dd/yyyy"),
                        e.PayPeriod.PeriodTo.ToString("MM/dd/yyyy")),
                    e.PayPeriod.PayDate,
                    HeadCount = 9999,
                    BasicPay = (decimal)9999999.99,
                    Taxable = (decimal)9999999.99,
                    NonTaxable = (decimal)9999999.99,
                    Loan = (decimal)9999999.99,
                    GrossPay = (decimal)9999999.99,
                    NetPay = (decimal)9999999.99
                }).FirstOrDefaultAsync();

            var reviewerType = await _reviewersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.ReviewedBy == AbpSession.UserId)
                .Select(e => e.ReviewerType)
                .ToListAsync();

            var reviewerStatus = await _processStatusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && reviewerType.Any(f => f == e.Responsible))
                .Select(e => e.Id)
                .ToListAsync();

            var isApprover = ((process.SysCode == "FIR" && process.CreatorUserId == AbpSession.UserId)
                || (reviewerStatus.Any(e => e == process.ProcessStatusId)));

            var employeeId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var statutories = await _statutoryProcessRepository.GetAll()
                .Where(e => e.PayrollProcess.EmployeeId == employeeId)
                .Select(e => new Tuple<string, decimal>
                (
                    e.StatutorySubTypes.DisplayName,
                    e.Amount
                )).ToListAsync();

            var output = new GetProcessForReviewStepDto
            {
                Step = process.Step,
                Status = process.Status,
                SysCode = process.SysCode,
                Statutories = statutories ??= new List<Tuple<string, decimal>>(),
                HeadCount = process.HeadCount,
                BasicPay = process.BasicPay,
                Taxable = process.Taxable,
                NonTaxable = process.NonTaxable,
                Loan = process.Loan,
                GrossPay = process.GrossPay,
                NetPay = process.NetPay,
                IsApprover = isApprover
            };

            return output;
        }

        public async Task<GetProcessForPostingStepDto> GetPostingStep(int id)
        {
            var output = await _processRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetProcessForPostingStepDto
                {
                    Step = e.ProcessStatus.Step,
                    Status = e.ProcessStatus.DisplayName,
                    SysCode = e.ProcessStatus.SysCode
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Process_Edit)]
        public async Task UpdateProcessToNextStatus(int id)
        {
            var process = await _processRepository.FirstOrDefaultAsync(id);

            var nextStatus = await _processRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => e.ProcessStatus.NextStatus)
                .FirstOrDefaultAsync();

            process.ProcessStatusId = nextStatus.Value;
        }

        public async Task ResetProcess(int id)
        {
            var process = await _processRepository.FirstOrDefaultAsync(id);
            var initialStatus = await _processStatusRepository.GetAll()
                .Where(e => e.Step == 1)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            process.ProcessStatusId = initialStatus;
        }

        public async Task<GetPayrollProcessForViewDto> GetProcesses()
        {
            var processes = await _processRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    e.Id,
                    e.PayrollNo,
                    e.PayPeriod.ProcessType,
                    Period = string.Format("{0} - {1}",
                        e.PayPeriod.PeriodFrom.ToString("MM/dd/yyyy"),
                        e.PayPeriod.PeriodTo.ToString("MM/dd/yyyy")
                    ),
                    e.PayPeriod.PayDate,
                    e.ProcessStatus.SysCode,
                    Status = e.ProcessStatus.DisplayName,
                    e.CreatorUserId,
                    //FIX THIS
                    Owner = string.Format("{0}, {1} {2}",
                        e.Owner.LastName,
                        e.Owner.FirstName,
                        e.Owner.MiddleName),
                    CreatedDate = e.CreationTime
                }).ToListAsync();

            var reviewerType = await _reviewersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.ReviewedBy == AbpSession.UserId)
                .Select(e => e.ReviewerType)
                .FirstOrDefaultAsync();

            var lookUp1 = processes.ToLookup(e => e.CreatorUserId == AbpSession.UserId);
            var lookUp2 = lookUp1[false].ToLookup(e => reviewerType != null
                && (e.SysCode == "FSR" || e.SysCode == "FFR"));

            return new GetPayrollProcessForViewDto
            {
                IsReviewer = await _reviewersRepository.GetAll().AnyAsync(e => e.TenantId == AbpSession.TenantId && e.ReviewedBy == AbpSession.UserId),
                MyProcesses = lookUp1[true]
                    .Select(e => new ProcessInfoDto
                    {
                        Id = e.Id,
                        PayrollNo = e.PayrollNo,
                        ProcessType = e.ProcessType,
                        Period = e.Period,
                        PayDate = e.PayDate,
                        Status = e.Status,
                        Owner = e.Owner,
                        CreatedDate = e.CreatedDate
                    }).ToList(),
                MyReviews = lookUp2[true]
                    .Select(e => new ProcessInfoDto
                    {
                        Id = e.Id,
                        PayrollNo = e.PayrollNo,
                        ProcessType = e.ProcessType,
                        Period = e.Period,
                        PayDate = e.PayDate,
                        Status = e.Status,
                        Owner = e.Owner,
                        CreatedDate = e.CreatedDate
                    }).ToList(),
                OtherProcesses = lookUp2[false]
                    .Select(e => new ProcessInfoDto
                    {
                        Id = e.Id,
                        PayrollNo = e.PayrollNo,
                        ProcessType = e.ProcessType,
                        Period = e.Period,
                        PayDate = e.PayDate,
                        Status = e.Status,
                        Owner = e.Owner,
                        CreatedDate = e.CreatedDate
                    }).ToList()
            };
        }

        public async Task<PagedResultDto<GetProcessForViewDto>> GetAll(GetAllProcessInput input)
        {
            var filteredProcess = _processRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.ProcessStatusFilter != null, e => e.Id == input.ProcessStatusFilter);

            var pagedAndFilteredProcess = filteredProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var process = from o in pagedAndFilteredProcess
                          select new GetProcessForViewDto()
                          {
                              Process = new ProcessDto
                              {
                                  PayrollNo = o.PayrollNo,
                                  SysCode = o.ProcessStatus.SysCode,
                                  Id = o.Id
                              }
                          };

            var totalCount = await filteredProcess.CountAsync();

            return new PagedResultDto<GetProcessForViewDto>(
                totalCount,
                await process.ToListAsync()
            );
        }

        public async Task<GetProcessForViewDto> GetProcessForView(int id)
        {
            var process = await _processRepository.GetAsync(id);

            var output = new GetProcessForViewDto { Process = ObjectMapper.Map<ProcessDto>(process) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Process_Create)]
        public async Task<GetProcessForCreateOutput> GetProcessForCreate()
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var output = new GetProcessForCreateOutput
            {
                Process = new CreateOrEditProcessDto
                {
                    PayrollNo = currentPayrollNo
                }
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Process_Edit)]
        public async Task<GetProcessForEditOutput> GetProcessForEdit(EntityDto input)
        {
            var process = await _processRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProcessForEditOutput { Process = ObjectMapper.Map<CreateOrEditProcessDto>(process) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Process_Create)]
        protected virtual async Task Create(CreateOrEditProcessDto input)
        {
            var employeeId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var processStatusId = await _processStatusRepository.GetAll()
                .Where(e => e.SysCode == "PE")
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var process = new Process
            {
                TenantId = AbpSession.TenantId.Value,
                PayrollNo = input.PayrollNo.Value,
                ProcessStatusId = processStatusId,
                CreatorUserId = AbpSession.UserId,
                CreationTime = DateTime.Now
            };

            await _processRepository.InsertAsync(process);
        }

        [AbpAuthorize(AppPermissions.Pages_Process_Edit)]
        protected virtual async Task Update(CreateOrEditProcessDto input)
        {
            var process = await _processRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, process);
        }

        [AbpAuthorize(AppPermissions.Pages_Process_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _processRepository.DeleteAsync(input.Id);
        }
    }
}