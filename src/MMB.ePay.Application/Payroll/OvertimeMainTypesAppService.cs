﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OvertimeMainTypes)]
    public class OvertimeMainTypesAppService : ePayAppServiceBase, IOvertimeMainTypesAppService
    {
        private readonly IRepository<OvertimeMainTypes> _overtimeMainTypesRepository;

        public OvertimeMainTypesAppService(IRepository<OvertimeMainTypes> overtimeMainTypesRepository)
        {
            _overtimeMainTypesRepository = overtimeMainTypesRepository;
        }

        public async Task<IList<Tuple<string, string>>> GetOvertimeMainTypesMenu()
        {
            return await _overtimeMainTypesRepository.GetAll()
                .OrderBy(e => e.DisplayOrder)
                .Select(e => new Tuple<string, string>
                (
                    e.DisplayName,
                    e.Description
                )).ToListAsync();
        }

        public async Task<PagedResultDto<GetOvertimeMainTypesForViewDto>> GetAll(GetAllOvertimeMainTypesInput input)
        {
            var filteredOvertimeMainTypes = _overtimeMainTypesRepository.GetAll();

            var pagedAndFilteredOvertimeMainTypes = filteredOvertimeMainTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var overtimeMainTypes = from o in pagedAndFilteredOvertimeMainTypes
                                     select new GetOvertimeMainTypesForViewDto()
                                     {
                                         OvertimeMainTypes = new OvertimeMainTypesDto
                                         {
                                             DisplayName = o.DisplayName,
                                             Description = o.Description,
                                             Id = o.Id
                                         }
                                     };

            var totalCount = await filteredOvertimeMainTypes.CountAsync();

            return new PagedResultDto<GetOvertimeMainTypesForViewDto>(
                totalCount,
                await overtimeMainTypes.ToListAsync()
            );
        }

        public async Task<GetOvertimeMainTypesForViewDto> GetOvertimeMainTypesForView(int id)
        {
            var overtimeMainTypes = await _overtimeMainTypesRepository.GetAsync(id);

            var output = new GetOvertimeMainTypesForViewDto { OvertimeMainTypes = ObjectMapper.Map<OvertimeMainTypesDto>(overtimeMainTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeMainTypes_Edit)]
        public async Task<GetOvertimeMainTypesForEditOutput> GetOvertimeMainTypesForEdit(EntityDto input)
        {
            var overtimeMainTypes = await _overtimeMainTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOvertimeMainTypesForEditOutput { OvertimeMainTypes = ObjectMapper.Map<CreateOrEditOvertimeMainTypesDto>(overtimeMainTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditOvertimeMainTypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeMainTypes_Create)]
        protected virtual async Task Create(CreateOrEditOvertimeMainTypesDto input)
        {
            var overtimeMainTypes = ObjectMapper.Map<OvertimeMainTypes>(input);
            var totalCount = await _overtimeMainTypesRepository.GetAll().CountAsync();

            overtimeMainTypes.DisplayOrder = totalCount + 1;
            
            await _overtimeMainTypesRepository.InsertAsync(overtimeMainTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeMainTypes_Edit)]
        protected virtual async Task Update(CreateOrEditOvertimeMainTypesDto input)
        {
            var overtimeMainTypes = await _overtimeMainTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, overtimeMainTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeMainTypes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _overtimeMainTypesRepository.DeleteAsync(input.Id);
        }
    }
}