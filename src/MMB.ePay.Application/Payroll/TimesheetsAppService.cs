﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Timesheets)]
    public class TimesheetsAppService : ePayAppServiceBase, ITimesheetsAppService
    {
        private readonly IRepository<Timesheets> _timesheetsRepository;
        private readonly IRepository<PayPeriod> _payPeriodRepository;
        private readonly IRepository<TimesheetTypes> _timesheetTypesRepository;
        private readonly IRepository<Process> _processRepository;

        public TimesheetsAppService(IRepository<Timesheets> timesheetsRepository, 
            IRepository<PayPeriod> payPeriodRepository,
            IRepository<TimesheetTypes> timesheetTypesRepository,
            IRepository<Process> processRepository)
        {
            _timesheetsRepository = timesheetsRepository;
            _payPeriodRepository = payPeriodRepository;
            _timesheetTypesRepository = timesheetTypesRepository;
            _processRepository = processRepository;
        }

        public async Task<GetTimesheetsForViewDto> GetAll(GetAllTimesheetsInput input)
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var timesheets = await _timesheetsRepository.GetAll()
                .OrderBy(e => e.TimesheetTypes.DisplayOrder)
                .Where(e => e.EmployeeId == input.EmployeeIdFilter && e.Process.PayrollNo == currentPayrollNo)
                .Select(e => new 
                {
                    e.Days,
                    e.Hrs
                }).ToListAsync();

            return new GetTimesheetsForViewDto
            {
                Days = timesheets.Select(e => e.Days).ToList(),
                Hrs = timesheets.Select(e => e.Hrs).ToList()
            };
        }

        public async Task CreateOrEdit(CreateOrEditTimesheetsDto input)
        {
            if (input.EmployeeId == 0)
                return;

            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var timesheets = await _timesheetsRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeId && e.Process.PayrollNo == currentPayrollNo)
                .ToListAsync();

            if (timesheets.Count > 0)
            {
                await Update(timesheets, input);
            }
            else
            {
                await Create(currentPayrollNo, input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Timesheets_Create)]
        protected virtual async Task Create(int payrollNo, CreateOrEditTimesheetsDto input)
        {
            var timesheetTypes = await _timesheetTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.DisplayOrder)
                .Select(e => e.Id)
                .ToListAsync();

            var processId = await _processRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.PayrollNo == payrollNo)
                    .Select(e => e.Id)
                    .FirstOrDefaultAsync();

            for (var i = 0; i < timesheetTypes.Count; i++)
            {
                var timesheets = new Timesheets
                {
                    EmployeeId = input.EmployeeId,
                    ProcessId = processId,
                    TimesheetTypeId = timesheetTypes[i],
                    Days = input.Days[i] ?? 0,
                    Hrs = input.Hrs[i] ?? 0,
                    Mins = 0
                };

                await _timesheetsRepository.InsertAsync(timesheets);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Timesheets_Edit)]
        protected virtual Task Update(IList<Timesheets> timesheets, CreateOrEditTimesheetsDto input)
        {
            for (var i = 0; i < timesheets.Count; i++)
            {
                timesheets[i].Days = input.Days[i] ?? 0;
                timesheets[i].Hrs = input.Hrs[i] ?? 0;
            }

            return Task.CompletedTask;
        }
    }
}