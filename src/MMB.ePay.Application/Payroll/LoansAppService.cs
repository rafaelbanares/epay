﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Loans)]
    public class LoansAppService : ePayAppServiceBase, ILoansAppService
    {
        private readonly IRepository<Loans> _loansRepository;

        public LoansAppService(IRepository<Loans> loansRepository)
        {
            _loansRepository = loansRepository;
        }

        public async Task<IList<SelectListItem>> GetLoanList()
        {
            return await _loansRepository.GetAll()
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = o.DisplayName
                }).ToListAsync();
        }

        public async Task<PagedResultDto<GetLoansForViewDto>> GetAll(GetAllLoansInput input)
        {
            var filteredLoans = _loansRepository.GetAll();

            var pagedAndFilteredLoans = filteredLoans
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var loans = from o in pagedAndFilteredLoans
                        select new GetLoansForViewDto()
                        {
                            Loans = new LoansDto
                            {
                                DisplayName = o.DisplayName,
                                Description = o.Description,
                                LoanType = o.LoanTypes.Description,
                                Currency = o.Currency,
                                Id = o.Id
                            }
                        };

            var totalCount = await filteredLoans.CountAsync();

            return new PagedResultDto<GetLoansForViewDto>(
                totalCount,
                await loans.ToListAsync()
            );
        }

        public async Task<GetLoansForViewDto> GetLoansForView(int id)
        {
            var output = await _loansRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetLoansForViewDto
                {
                    Loans = new LoansDto
                    {
                        DisplayName = e.DisplayName,
                        Description = e.Description,
                        LoanType = e.LoanTypes.Description,
                        Currency = e.Currency,
                        RefCode = e.RefCode,
                        AcctCode = e.AcctCode
                    }
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Loans_Edit)]
        public async Task<GetLoansForEditOutput> GetLoansForEdit(EntityDto input)
        {
            var loans = await _loansRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLoansForEditOutput { Loans = ObjectMapper.Map<CreateOrEditLoansDto>(loans) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLoansDto input)
        {
            input.RefCode = input.RefCode == string.Empty ? null : input.RefCode;
            input.AcctCode = input.AcctCode == string.Empty ? null : input.AcctCode;

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Loans_Create)]
        protected virtual async Task Create(CreateOrEditLoansDto input)
        {
            var loans = ObjectMapper.Map<Loans>(input);
            await _loansRepository.InsertAsync(loans);
        }

        [AbpAuthorize(AppPermissions.Pages_Loans_Edit)]
        protected virtual async Task Update(CreateOrEditLoansDto input)
        {
            var loans = await _loansRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, loans);
        }
    }
}