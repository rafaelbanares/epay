﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_DeductionTmp)]
    public class DeductionTmpAppService : ePayAppServiceBase, IDeductionTmpAppService
    {
        private readonly IRepository<DeductionTmp> _deductionTmpRepository;

        public DeductionTmpAppService(IRepository<DeductionTmp> deductionTmpRepository)
        {
            _deductionTmpRepository = deductionTmpRepository;
        }

        public async Task<PagedResultDto<GetDeductionTmpForViewDto>> GetAll(GetAllDeductionTmpInput input)
        {

            var filteredDeductionTmp = _deductionTmpRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinDeductionTypeIdFilter != null, e => e.DeductionTypeId >= input.MinDeductionTypeIdFilter)
                        .WhereIf(input.MaxDeductionTypeIdFilter != null, e => e.DeductionTypeId <= input.MaxDeductionTypeIdFilter)
                        .WhereIf(input.MinRecurDeductionIdFilter != null, e => e.RecurDeductionId >= input.MinRecurDeductionIdFilter)
                        .WhereIf(input.MaxRecurDeductionIdFilter != null, e => e.RecurDeductionId <= input.MaxRecurDeductionIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredDeductionTmp = filteredDeductionTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var deductionTmp = from o in pagedAndFilteredDeductionTmp
                               select new GetDeductionTmpForViewDto()
                               {
                                   DeductionTmp = new DeductionTmpDto
                                   {
                                       PayrollNo = o.PayrollNo,
                                       Amount = o.Amount,
                                       EmployeeId = o.EmployeeId,
                                       DeductionTypeId = o.DeductionTypeId,
                                       RecurDeductionId = o.RecurDeductionId,
                                       SessionId = o.SessionId,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredDeductionTmp.CountAsync();

            return new PagedResultDto<GetDeductionTmpForViewDto>(
                totalCount,
                await deductionTmp.ToListAsync()
            );
        }

        public async Task<GetDeductionTmpForViewDto> GetDeductionTmpForView(int id)
        {
            var deductionTmp = await _deductionTmpRepository.GetAsync(id);

            var output = new GetDeductionTmpForViewDto { DeductionTmp = ObjectMapper.Map<DeductionTmpDto>(deductionTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTmp_Edit)]
        public async Task<GetDeductionTmpForEditOutput> GetDeductionTmpForEdit(EntityDto input)
        {
            var deductionTmp = await _deductionTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDeductionTmpForEditOutput { DeductionTmp = ObjectMapper.Map<CreateOrEditDeductionTmpDto>(deductionTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDeductionTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTmp_Create)]
        protected virtual async Task Create(CreateOrEditDeductionTmpDto input)
        {
            var deductionTmp = ObjectMapper.Map<DeductionTmp>(input);
            await _deductionTmpRepository.InsertAsync(deductionTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTmp_Edit)]
        protected virtual async Task Update(CreateOrEditDeductionTmpDto input)
        {
            var deductionTmp = await _deductionTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, deductionTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _deductionTmpRepository.DeleteAsync(input.Id);
        }
    }
}