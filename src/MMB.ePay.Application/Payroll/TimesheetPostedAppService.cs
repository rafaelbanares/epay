﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_TimesheetPosted)]
    public class TimesheetPostedAppService : ePayAppServiceBase, ITimesheetPostedAppService
    {
        private readonly IRepository<TimesheetPosted> _timesheetPostedRepository;

        public TimesheetPostedAppService(IRepository<TimesheetPosted> timesheetPostedRepository)
        {
            _timesheetPostedRepository = timesheetPostedRepository;
        }

        public async Task<PagedResultDto<GetTimesheetPostedForViewDto>> GetAll(GetAllTimesheetPostedInput input)
        {

            var filteredTimesheetPosted = _timesheetPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinCostCenterFilter != null, e => e.CostCenter >= input.MinCostCenterFilter)
                        .WhereIf(input.MaxCostCenterFilter != null, e => e.CostCenter <= input.MaxCostCenterFilter)
                        .WhereIf(input.MinDaysFilter != null, e => e.Days >= input.MinDaysFilter)
                        .WhereIf(input.MaxDaysFilter != null, e => e.Days <= input.MaxDaysFilter)
                        .WhereIf(input.MinHrsFilter != null, e => e.Hrs >= input.MinHrsFilter)
                        .WhereIf(input.MaxHrsFilter != null, e => e.Hrs <= input.MaxHrsFilter)
                        .WhereIf(input.MinMinsFilter != null, e => e.Mins >= input.MinMinsFilter)
                        .WhereIf(input.MaxMinsFilter != null, e => e.Mins <= input.MaxMinsFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinTimesheetTypeIdFilter != null, e => e.TimesheetTypeId >= input.MinTimesheetTypeIdFilter)
                        .WhereIf(input.MaxTimesheetTypeIdFilter != null, e => e.TimesheetTypeId <= input.MaxTimesheetTypeIdFilter);

            var pagedAndFilteredTimesheetPosted = filteredTimesheetPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var timesheetPosted = from o in pagedAndFilteredTimesheetPosted
                                  select new GetTimesheetPostedForViewDto()
                                  {
                                      TimesheetPosted = new TimesheetPostedDto
                                      {
                                          CostCenter = o.CostCenter,
                                          Days = o.Days,
                                          Hrs = o.Hrs,
                                          Mins = o.Mins,
                                          Amount = o.Amount,
                                          TimesheetTypeId = o.TimesheetTypeId,
                                          Id = o.Id
                                      }
                                  };

            var totalCount = await filteredTimesheetPosted.CountAsync();

            return new PagedResultDto<GetTimesheetPostedForViewDto>(
                totalCount,
                await timesheetPosted.ToListAsync()
            );
        }

        public async Task<GetTimesheetPostedForViewDto> GetTimesheetPostedForView(int id)
        {
            var timesheetPosted = await _timesheetPostedRepository.GetAsync(id);

            var output = new GetTimesheetPostedForViewDto { TimesheetPosted = ObjectMapper.Map<TimesheetPostedDto>(timesheetPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetPosted_Edit)]
        public async Task<GetTimesheetPostedForEditOutput> GetTimesheetPostedForEdit(EntityDto input)
        {
            var timesheetPosted = await _timesheetPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTimesheetPostedForEditOutput { TimesheetPosted = ObjectMapper.Map<CreateOrEditTimesheetPostedDto>(timesheetPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTimesheetPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetPosted_Create)]
        protected virtual async Task Create(CreateOrEditTimesheetPostedDto input)
        {
            var timesheetPosted = ObjectMapper.Map<TimesheetPosted>(input);
            await _timesheetPostedRepository.InsertAsync(timesheetPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetPosted_Edit)]
        protected virtual async Task Update(CreateOrEditTimesheetPostedDto input)
        {
            var timesheetPosted = await _timesheetPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, timesheetPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_TimesheetPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _timesheetPostedRepository.DeleteAsync(input.Id);
        }
    }
}