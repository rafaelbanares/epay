﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_PayrollProcess)]
    public class PayrollProcessAppService : ePayAppServiceBase, IPayrollProcessAppService
    {
        private readonly IRepository<PayrollProcess> _payrollProcessRepository;

        public PayrollProcessAppService(IRepository<PayrollProcess> payrollProcessRepository)
        {
            _payrollProcessRepository = payrollProcessRepository;
        }

        public async Task<PagedResultDto<GetPayrollProcessForViewDto>> GetAll(GetAllPayrollProcessInput input)
        {

            var filteredPayrollProcess = _payrollProcessRepository.GetAll();

            var pagedAndFilteredPayrollProcess = filteredPayrollProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var payrollProcess = from o in pagedAndFilteredPayrollProcess
                                 select new GetPayrollProcessForViewDto()
                                 {
                                     PayrollProcess = new PayrollProcessDto
                                     {
                                         PayrollNo = o.PayrollNo,
                                         Salary = o.Salary,
                                         DailyRate = o.DailyRate,
                                         HourlyRate = o.HourlyRate,
                                         Timesheet = o.Timesheet,
                                         Overtime = o.Overtime,
                                         TxEarningAmt = o.TxEarningAmt,
                                         NtxEarningAmt = o.NtxEarningAmt,
                                         DedTaxAmt = o.DedTaxAmt,
                                         DeductionAmt = o.DeductionAmt,
                                         LoanAmt = o.LoanAmt,
                                         BasicPay = o.BasicPay,
                                         GrossPay = o.GrossPay,
                                         NetPay = o.NetPay,
                                         EmployeeId = o.EmployeeId,
                                         PayCodeId = o.PayCodeId,
                                         BankBranchId = o.BankBranchId,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredPayrollProcess.CountAsync();

            return new PagedResultDto<GetPayrollProcessForViewDto>(
                totalCount,
                await payrollProcess.ToListAsync()
            );
        }

        public async Task<GetPayrollProcessForViewDto> GetPayrollProcessForView(int id)
        {
            var payrollProcess = await _payrollProcessRepository.GetAsync(id);

            var output = new GetPayrollProcessForViewDto { PayrollProcess = ObjectMapper.Map<PayrollProcessDto>(payrollProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollProcess_Edit)]
        public async Task<GetPayrollProcessForEditOutput> GetPayrollProcessForEdit(EntityDto input)
        {
            var payrollProcess = await _payrollProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayrollProcessForEditOutput { PayrollProcess = ObjectMapper.Map<CreateOrEditPayrollProcessDto>(payrollProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayrollProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollProcess_Create)]
        protected virtual async Task Create(CreateOrEditPayrollProcessDto input)
        {
            var payrollProcess = ObjectMapper.Map<PayrollProcess>(input);
            await _payrollProcessRepository.InsertAsync(payrollProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollProcess_Edit)]
        protected virtual async Task Update(CreateOrEditPayrollProcessDto input)
        {
            var payrollProcess = await _payrollProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, payrollProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _payrollProcessRepository.DeleteAsync(input.Id);
        }
    }
}