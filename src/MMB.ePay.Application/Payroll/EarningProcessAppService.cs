﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EarningProcess)]
    public class EarningProcessAppService : ePayAppServiceBase, IEarningProcessAppService
    {
        private readonly IRepository<EarningProcess> _earningProcessRepository;

        public EarningProcessAppService(IRepository<EarningProcess> earningProcessRepository)
        {
            _earningProcessRepository = earningProcessRepository;
        }

        public async Task<PagedResultDto<GetEarningProcessForViewDto>> GetAll(GetAllEarningProcessInput input)
        {

            var filteredEarningProcess = _earningProcessRepository.GetAll()
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinEarningTypeIdFilter != null, e => e.EarningTypeId >= input.MinEarningTypeIdFilter)
                        .WhereIf(input.MaxEarningTypeIdFilter != null, e => e.EarningTypeId <= input.MaxEarningTypeIdFilter);

            var pagedAndFilteredEarningProcess = filteredEarningProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var earningProcess = from o in pagedAndFilteredEarningProcess
                                 select new GetEarningProcessForViewDto()
                                 {
                                     EarningProcess = new EarningProcessDto
                                     {
                                         PayrollNo = o.PayrollProcess.PayrollNo,
                                         Amount = o.Amount,
                                         EmployeeId = o.PayrollProcess.EmployeeId,
                                         EarningTypeId = o.EarningTypeId,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredEarningProcess.CountAsync();

            return new PagedResultDto<GetEarningProcessForViewDto>(
                totalCount,
                await earningProcess.ToListAsync()
            );
        }

        public async Task<GetEarningProcessForViewDto> GetEarningProcessForView(int id)
        {
            var earningProcess = await _earningProcessRepository.GetAsync(id);

            var output = new GetEarningProcessForViewDto { EarningProcess = ObjectMapper.Map<EarningProcessDto>(earningProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EarningProcess_Edit)]
        public async Task<GetEarningProcessForEditOutput> GetEarningProcessForEdit(EntityDto input)
        {
            var earningProcess = await _earningProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEarningProcessForEditOutput { EarningProcess = ObjectMapper.Map<CreateOrEditEarningProcessDto>(earningProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEarningProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EarningProcess_Create)]
        protected virtual async Task Create(CreateOrEditEarningProcessDto input)
        {
            var earningProcess = ObjectMapper.Map<EarningProcess>(input);
            await _earningProcessRepository.InsertAsync(earningProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningProcess_Edit)]
        protected virtual async Task Update(CreateOrEditEarningProcessDto input)
        {
            var earningProcess = await _earningProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, earningProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _earningProcessRepository.DeleteAsync(input.Id);
        }
    }
}