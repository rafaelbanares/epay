﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_PayrollTmp)]
    public class PayrollTmpAppService : ePayAppServiceBase, IPayrollTmpAppService
    {
        private readonly IRepository<PayrollTmp> _payrollTmpRepository;

        public PayrollTmpAppService(IRepository<PayrollTmp> payrollTmpRepository)
        {
            _payrollTmpRepository = payrollTmpRepository;
        }

        public async Task<PagedResultDto<GetPayrollTmpForViewDto>> GetAll(GetAllPayrollTmpInput input)
        {

            var filteredPayrollTmp = _payrollTmpRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinSalaryFilter != null, e => e.Salary >= input.MinSalaryFilter)
                        .WhereIf(input.MaxSalaryFilter != null, e => e.Salary <= input.MaxSalaryFilter)
                        .WhereIf(input.MinDailyRateFilter != null, e => e.DailyRate >= input.MinDailyRateFilter)
                        .WhereIf(input.MaxDailyRateFilter != null, e => e.DailyRate <= input.MaxDailyRateFilter)
                        .WhereIf(input.MinHourlyRateFilter != null, e => e.HourlyRate >= input.MinHourlyRateFilter)
                        .WhereIf(input.MaxHourlyRateFilter != null, e => e.HourlyRate <= input.MaxHourlyRateFilter)
                        .WhereIf(input.MinTimesheetFilter != null, e => e.Timesheet >= input.MinTimesheetFilter)
                        .WhereIf(input.MaxTimesheetFilter != null, e => e.Timesheet <= input.MaxTimesheetFilter)
                        .WhereIf(input.MinOvertimeFilter != null, e => e.Overtime >= input.MinOvertimeFilter)
                        .WhereIf(input.MaxOvertimeFilter != null, e => e.Overtime <= input.MaxOvertimeFilter)
                        .WhereIf(input.MinTxEarningAmtFilter != null, e => e.TxEarningAmt >= input.MinTxEarningAmtFilter)
                        .WhereIf(input.MaxTxEarningAmtFilter != null, e => e.TxEarningAmt <= input.MaxTxEarningAmtFilter)
                        .WhereIf(input.MinNtxEarningAmtFilter != null, e => e.NtxEarningAmt >= input.MinNtxEarningAmtFilter)
                        .WhereIf(input.MaxNtxEarningAmtFilter != null, e => e.NtxEarningAmt <= input.MaxNtxEarningAmtFilter)
                        .WhereIf(input.MinDedTaxAmtFilter != null, e => e.DedTaxAmt >= input.MinDedTaxAmtFilter)
                        .WhereIf(input.MaxDedTaxAmtFilter != null, e => e.DedTaxAmt <= input.MaxDedTaxAmtFilter)
                        .WhereIf(input.MinDeductionAmtFilter != null, e => e.DeductionAmt >= input.MinDeductionAmtFilter)
                        .WhereIf(input.MaxDeductionAmtFilter != null, e => e.DeductionAmt <= input.MaxDeductionAmtFilter)
                        .WhereIf(input.MinLoanAmtFilter != null, e => e.LoanAmt >= input.MinLoanAmtFilter)
                        .WhereIf(input.MaxLoanAmtFilter != null, e => e.LoanAmt <= input.MaxLoanAmtFilter)
                        .WhereIf(input.MinBasicPayFilter != null, e => e.BasicPay >= input.MinBasicPayFilter)
                        .WhereIf(input.MaxBasicPayFilter != null, e => e.BasicPay <= input.MaxBasicPayFilter)
                        .WhereIf(input.MinGrossPayFilter != null, e => e.GrossPay >= input.MinGrossPayFilter)
                        .WhereIf(input.MaxGrossPayFilter != null, e => e.GrossPay <= input.MaxGrossPayFilter)
                        .WhereIf(input.MinNetPayFilter != null, e => e.NetPay >= input.MinNetPayFilter)
                        .WhereIf(input.MaxNetPayFilter != null, e => e.NetPay <= input.MaxNetPayFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinPayCodeIdFilter != null, e => e.PayCodeId >= input.MinPayCodeIdFilter)
                        .WhereIf(input.MaxPayCodeIdFilter != null, e => e.PayCodeId <= input.MaxPayCodeIdFilter)
                        .WhereIf(input.MinBankBranchIdFilter != null, e => e.BankBranchId >= input.MinBankBranchIdFilter)
                        .WhereIf(input.MaxBankBranchIdFilter != null, e => e.BankBranchId <= input.MaxBankBranchIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredPayrollTmp = filteredPayrollTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var payrollTmp = from o in pagedAndFilteredPayrollTmp
                             select new GetPayrollTmpForViewDto()
                             {
                                 PayrollTmp = new PayrollTmpDto
                                 {
                                     PayrollNo = o.PayrollNo,
                                     Salary = o.Salary,
                                     DailyRate = o.DailyRate,
                                     HourlyRate = o.HourlyRate,
                                     Timesheet = o.Timesheet,
                                     Overtime = o.Overtime,
                                     TxEarningAmt = o.TxEarningAmt,
                                     NtxEarningAmt = o.NtxEarningAmt,
                                     DedTaxAmt = o.DedTaxAmt,
                                     DeductionAmt = o.DeductionAmt,
                                     LoanAmt = o.LoanAmt,
                                     BasicPay = o.BasicPay,
                                     GrossPay = o.GrossPay,
                                     NetPay = o.NetPay,
                                     EmployeeId = o.EmployeeId,
                                     PayCodeId = o.PayCodeId,
                                     BankBranchId = o.BankBranchId,
                                     SessionId = o.SessionId,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredPayrollTmp.CountAsync();

            return new PagedResultDto<GetPayrollTmpForViewDto>(
                totalCount,
                await payrollTmp.ToListAsync()
            );
        }

        public async Task<GetPayrollTmpForViewDto> GetPayrollTmpForView(int id)
        {
            var payrollTmp = await _payrollTmpRepository.GetAsync(id);

            var output = new GetPayrollTmpForViewDto { PayrollTmp = ObjectMapper.Map<PayrollTmpDto>(payrollTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollTmp_Edit)]
        public async Task<GetPayrollTmpForEditOutput> GetPayrollTmpForEdit(EntityDto input)
        {
            var payrollTmp = await _payrollTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayrollTmpForEditOutput { PayrollTmp = ObjectMapper.Map<CreateOrEditPayrollTmpDto>(payrollTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayrollTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollTmp_Create)]
        protected virtual async Task Create(CreateOrEditPayrollTmpDto input)
        {
            var payrollTmp = ObjectMapper.Map<PayrollTmp>(input);
            await _payrollTmpRepository.InsertAsync(payrollTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollTmp_Edit)]
        protected virtual async Task Update(CreateOrEditPayrollTmpDto input)
        {
            var payrollTmp = await _payrollTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, payrollTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _payrollTmpRepository.DeleteAsync(input.Id);
        }
    }
}