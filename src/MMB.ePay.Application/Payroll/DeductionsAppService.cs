﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Deductions)]
    public class DeductionsAppService : ePayAppServiceBase, IDeductionsAppService
    {
        private readonly IRepository<Deductions> _deductionsRepository;
        private readonly IRepository<PayPeriod> _payPeriodRepository;

        public DeductionsAppService(IRepository<Deductions> deductionsRepository, IRepository<PayPeriod> payPeriodRepository)
        {
            _deductionsRepository = deductionsRepository;
            _payPeriodRepository = payPeriodRepository;
        }

        public async Task<PagedResultDto<GetDeductionsForViewDto>> GetAll(GetAllDeductionsInput input)
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var filteredDeductions = _deductionsRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeIdFilter && e.Process.PayrollNo == currentPayrollNo);

            var pagedAndFilteredDeductions = filteredDeductions
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var deductions = from o in pagedAndFilteredDeductions
                           select new GetDeductionsForViewDto()
                           {
                               Deductions = new DeductionsDto
                               {
                                   PayrollNo = o.Process.PayrollNo,
                                   Amount = o.Amount,
                                   EmployeeId = o.EmployeeId,
                                   DeductionTypeId = o.DeductionTypeId,
                                   DeductionType = o.DeductionTypes.DisplayName,
                                   Id = o.Id
                               }
                           };

            var totalCount = await filteredDeductions.CountAsync();

            return new PagedResultDto<GetDeductionsForViewDto>(
                totalCount,
                await deductions.ToListAsync()
            );
        }

        public async Task<GetDeductionsForViewDto> GetDeductionsForView(int id)
        {
            var deductions = await _deductionsRepository.GetAsync(id);

            var output = new GetDeductionsForViewDto { Deductions = ObjectMapper.Map<DeductionsDto>(deductions) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Deductions_Edit)]
        public async Task<GetDeductionsForEditOutput> GetDeductionsForEdit(EntityDto input)
        {
            var deductions = await _deductionsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDeductionsForEditOutput { Deductions = ObjectMapper.Map<CreateOrEditDeductionsDto>(deductions) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDeductionsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Deductions_Create)]
        protected virtual async Task Create(CreateOrEditDeductionsDto input)
        {
            var currentPayrollNo = await _payPeriodRepository.GetAll()
                .Where(e => e.PayrollStatus == "NEW")
                .OrderBy(e => e.PayDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            input.PayrollNo = currentPayrollNo;

            var deductions = ObjectMapper.Map<Deductions>(input);
            await _deductionsRepository.InsertAsync(deductions);
        }

        [AbpAuthorize(AppPermissions.Pages_Deductions_Edit)]
        protected virtual async Task Update(CreateOrEditDeductionsDto input)
        {
            var deductions = await _deductionsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, deductions);
        }

        [AbpAuthorize(AppPermissions.Pages_Deductions_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _deductionsRepository.DeleteAsync(input.Id);
        }
    }
}