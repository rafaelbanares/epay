﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OTSheetPosted)]
    public class OTSheetPostedAppService : ePayAppServiceBase, IOTSheetPostedAppService
    {
        private readonly IRepository<OTSheetPosted> _otSheetPostedRepository;

        public OTSheetPostedAppService(IRepository<OTSheetPosted> otSheetPostedRepository)
        {
            _otSheetPostedRepository = otSheetPostedRepository;
        }

        public async Task<PagedResultDto<GetOTSheetPostedForViewDto>> GetAll(GetAllOTSheetPostedInput input)
        {

            var filteredOTSheetPosted = _otSheetPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollPosted.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollPosted.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.OvertimeTypeFilter.HasValue, e => e.OvertimeTypeId == input.OvertimeTypeFilter)
                        .WhereIf(input.MinCostCenterFilter != null, e => e.CostCenter >= input.MinCostCenterFilter)
                        .WhereIf(input.MaxCostCenterFilter != null, e => e.CostCenter <= input.MaxCostCenterFilter)
                        .WhereIf(input.MinHrsFilter != null, e => e.Hrs >= input.MinHrsFilter)
                        .WhereIf(input.MaxHrsFilter != null, e => e.Hrs <= input.MaxHrsFilter)
                        .WhereIf(input.MinMinsFilter != null, e => e.Mins >= input.MinMinsFilter)
                        .WhereIf(input.MaxMinsFilter != null, e => e.Mins <= input.MaxMinsFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollPosted.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollPosted.EmployeeId <= input.MaxEmployeeIdFilter);

            var pagedAndFilteredOTSheetPosted = filteredOTSheetPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var otSheetPosted = from o in pagedAndFilteredOTSheetPosted
                                select new GetOTSheetPostedForViewDto()
                                {
                                    OTSheetPosted = new OTSheetPostedDto
                                    {
                                        PayrollNo = o.PayrollPosted.PayrollNo,
                                        OvertimeType = o.OvertimeTypes.DisplayName,
                                        CostCenter = o.CostCenter,
                                        Hrs = o.Hrs,
                                        Mins = o.Mins,
                                        Amount = o.Amount,
                                        EmployeeId = o.PayrollPosted.EmployeeId,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredOTSheetPosted.CountAsync();

            return new PagedResultDto<GetOTSheetPostedForViewDto>(
                totalCount,
                await otSheetPosted.ToListAsync()
            );
        }

        public async Task<GetOTSheetPostedForViewDto> GetOTSheetPostedForView(int id)
        {
            var otSheetPosted = await _otSheetPostedRepository.GetAsync(id);

            var output = new GetOTSheetPostedForViewDto { OTSheetPosted = ObjectMapper.Map<OTSheetPostedDto>(otSheetPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetPosted_Edit)]
        public async Task<GetOTSheetPostedForEditOutput> GetOTSheetPostedForEdit(EntityDto input)
        {
            var otSheetPosted = await _otSheetPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOTSheetPostedForEditOutput { OTSheetPosted = ObjectMapper.Map<CreateOrEditOTSheetPostedDto>(otSheetPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditOTSheetPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetPosted_Create)]
        protected virtual async Task Create(CreateOrEditOTSheetPostedDto input)
        {
            var otSheetPosted = ObjectMapper.Map<OTSheetPosted>(input);
            await _otSheetPostedRepository.InsertAsync(otSheetPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetPosted_Edit)]
        protected virtual async Task Update(CreateOrEditOTSheetPostedDto input)
        {
            var otSheetPosted = await _otSheetPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, otSheetPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _otSheetPostedRepository.DeleteAsync(input.Id);
        }
    }
}