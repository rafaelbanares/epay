﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OvertimeSubTypes)]
    public class OvertimeSubTypesAppService : ePayAppServiceBase, IOvertimeSubTypesAppService
    {
        private readonly IRepository<OvertimeSubTypes> _overtimeSubTypesRepository;

        public OvertimeSubTypesAppService(IRepository<OvertimeSubTypes> overtimeSubTypesRepository)
        {
            _overtimeSubTypesRepository = overtimeSubTypesRepository;
        }

        public async Task<IList<string>> GetOvertimeSubTypesMenu()
        {
            return await _overtimeSubTypesRepository.GetAll()
                .Select(e => e.DisplayName)
                .ToListAsync();
        }

        public async Task<PagedResultDto<GetOvertimeSubTypesForViewDto>> GetAll(GetAllOvertimeSubTypesInput input)
        {

            var filteredOvertimeSubTypes = _overtimeSubTypesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter)
                        .WhereIf(input.MinDisplayOrderFilter != null, e => e.DisplayOrder >= input.MinDisplayOrderFilter)
                        .WhereIf(input.MaxDisplayOrderFilter != null, e => e.DisplayOrder <= input.MaxDisplayOrderFilter);

            var pagedAndFilteredOvertimeSubTypes = filteredOvertimeSubTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var overtimeSubTypes = from o in pagedAndFilteredOvertimeSubTypes
                                     select new GetOvertimeSubTypesForViewDto()
                                     {
                                         OvertimeSubTypes = new OvertimeSubTypesDto
                                         {
                                             DisplayName = o.DisplayName,
                                             Description = o.Description,
                                             Id = o.Id
                                         }
                                     };

            var totalCount = await filteredOvertimeSubTypes.CountAsync();

            return new PagedResultDto<GetOvertimeSubTypesForViewDto>(
                totalCount,
                await overtimeSubTypes.ToListAsync()
            );
        }

        public async Task<GetOvertimeSubTypesForViewDto> GetOvertimeSubTypesForView(int id)
        {
            var overtimeSubTypes = await _overtimeSubTypesRepository.GetAsync(id);

            var output = new GetOvertimeSubTypesForViewDto { OvertimeSubTypes = ObjectMapper.Map<OvertimeSubTypesDto>(overtimeSubTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeSubTypes_Edit)]
        public async Task<GetOvertimeSubTypesForEditOutput> GetOvertimeSubTypesForEdit(EntityDto input)
        {
            var overtimeSubTypes = await _overtimeSubTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOvertimeSubTypesForEditOutput { OvertimeSubTypes = ObjectMapper.Map<CreateOrEditOvertimeSubTypesDto>(overtimeSubTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditOvertimeSubTypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeSubTypes_Create)]
        protected virtual async Task Create(CreateOrEditOvertimeSubTypesDto input)
        {
            var overtimeSubTypes = ObjectMapper.Map<OvertimeSubTypes>(input);
            var totalCount = await _overtimeSubTypesRepository.GetAll().CountAsync();

            overtimeSubTypes.DisplayOrder = totalCount + 1;

            await _overtimeSubTypesRepository.InsertAsync(overtimeSubTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeSubTypes_Edit)]
        protected virtual async Task Update(CreateOrEditOvertimeSubTypesDto input)
        {
            var overtimeSubTypes = await _overtimeSubTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, overtimeSubTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeSubTypes_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _overtimeSubTypesRepository.DeleteAsync(input.Id);
        }
    }
}