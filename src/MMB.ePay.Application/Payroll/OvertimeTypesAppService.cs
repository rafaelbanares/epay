﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OvertimeTypes)]
    public class OvertimeTypesAppService : ePayAppServiceBase, IOvertimeTypesAppService
    {
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IRepository<OvertimeMainTypes> _overtimeMainTypes;
        private readonly IRepository<OvertimeSubTypes> _overtimeSubTypes;

        public OvertimeTypesAppService(IRepository<OvertimeTypes> overtimeTypesRepository,
            IRepository<OvertimeMainTypes> overtimeMainTypes,
            IRepository<OvertimeSubTypes> overtimeSubTypes)
        {
            _overtimeTypesRepository = overtimeTypesRepository;
            _overtimeMainTypes = overtimeMainTypes;
            _overtimeSubTypes = overtimeSubTypes;
        }

        public async Task<IList<SelectListItem>> GetOvertimeTypesListFilter()
        {
            var overtimeTypes = await _overtimeTypesRepository.GetAll()
                .OrderBy(e => e.DisplayName)
                .Select(e => new SelectListItem
                {
                    Text = e.DisplayName,
                    Value = e.Id.ToString()
                }).ToListAsync();

            overtimeTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return overtimeTypes;
        }

        public async Task<IList<SelectListItem>> GetOvertimeTypesList() =>
            await _overtimeTypesRepository.GetAll()
                .OrderBy(e => e.DisplayName)
                .Select(e => new SelectListItem
                {
                    Text = e.DisplayName,
                    Value = e.Id.ToString()
                }).ToListAsync();

        public async Task<GetOvertimeTypesForViewDto> GetAll()
        {
            var output = new GetOvertimeTypesForViewDto
            {
                Group1 = await _overtimeMainTypes.GetAll()
                    .OrderBy(e => e.DisplayOrder)
                    .Select(e => new Tuple<string, string>
                    (
                        e.DisplayName,
                        e.Description
                    )).ToListAsync(),

                Group2 = await _overtimeSubTypes.GetAll()
                    .OrderBy(e => e.DisplayOrder)
                    .Select(e => e.DisplayName)
                    .ToListAsync(),

                OvertimeTypes = await _overtimeTypesRepository.GetAll()
                    .OrderBy(e => e.OvertimeMainTypes.DisplayOrder)
                    .ThenBy(e => e.OvertimeSubTypes.DisplayOrder)
                    .Select(e => e.Rate)
                    .ToListAsync()
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes_Edit)]
        public virtual async Task Update(IList<decimal> input)
        {
            var overtimeTypes = await _overtimeTypesRepository.GetAll()
                .OrderBy(e => e.OvertimeMainTypes.DisplayOrder)
                .ThenBy(e => e.OvertimeSubTypes.DisplayOrder)
                .ToListAsync();

            for(var i = 0; i < overtimeTypes.Count; i++)
            {
                overtimeTypes[i].Rate = input[i];
            }
        }
    }
}