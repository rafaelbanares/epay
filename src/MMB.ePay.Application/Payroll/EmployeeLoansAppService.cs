﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Core.Extensions;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EmployeeLoans)]
    public class EmployeeLoansAppService : ePayAppServiceBase, IEmployeeLoansAppService
    {
        private readonly IRepository<EmployeeLoans, Guid> _employeeLoansRepository;

        public EmployeeLoansAppService(IRepository<EmployeeLoans, Guid> employeeLoansRepository)
        {
            _employeeLoansRepository = employeeLoansRepository;
        }

        public async Task<PagedResultDto<GetEmployeeLoansForViewDto>> GetAll(GetAllEmployeeLoansInput input)
        {
            var initialFilteredEmployeeLoans = _employeeLoansRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                    (e.Employees.EmployeeCode + " | " + e.Employees.LastName + ", " + e.Employees.FirstName + " " + e.Employees.MiddleName)
                        .Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.StatusIdFilter), e => e.Status == input.StatusIdFilter)
                .WhereIf(input.YearIdFilter > 0, e => e.ApprovedDate.Year == input.YearIdFilter);

            var employeeLoans = await initialFilteredEmployeeLoans
                .Select(o => new EmployeeLoansDto
                {
                    Employee = o.Employees.FullName,
                    Loan = o.Loans.Description,
                    LoanAmount = o.LoanAmount,
                    Frequency = o.Frequency.GetPayrollFrequencyName(),
                    ApprovedDate = o.ApprovedDate,
                    Amortization = o.Amortization,
                    Balance = o.LoanAmount - o.LoanPosted.Sum(l => l.Amount),
                    Status = o.Status,
                    Id = o.Id
                }).ToListAsync();

            var pagedAndFilteredEmployeeLoans = employeeLoans.AsQueryable()
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var totalCount = employeeLoans.Count;

            var output =  pagedAndFilteredEmployeeLoans
                .Select(o => new GetEmployeeLoansForViewDto
                {
                    EmployeeLoans = o
                }).ToList();

            return new PagedResultDto<GetEmployeeLoansForViewDto>(
                totalCount,
                output
            );
        }

        public async Task<GetEmployeeLoansForViewDto> GetEmployeeLoansForView(Guid id)
        {
            var output = await _employeeLoansRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetEmployeeLoansForViewDto
                {
                    EmployeeLoans = new EmployeeLoansDto
                    {
                        Employee = e.Employees.FullName,
                        ApprovedDate = e.ApprovedDate,
                        EffectStart = e.EffectStart,
                        Principal = e.Principal,
                        LoanAmount = e.LoanAmount,
                        Amortization = e.Amortization,
                        Remarks = e.Remarks,
                        HoldStart = e.HoldStart,
                        HoldEnd = e.HoldEnd
                    }
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeLoans_Edit)]
        public async Task<GetEmployeeLoansForEditOutput> GetEmployeeLoansForEdit(EntityDto<Guid> input)
        {
            var employeeLoans = await _employeeLoansRepository
                .GetAll()
                .Select(o => new
                {
                    o.Id,
                    TotalDeducted = o.LoanPosted.Sum(o2 => o2.Amount),
                    EmployeeName = o.Employees.FullName,
                    o.Employees.EmployeePayroll.First().PayCodes.PayFreq,
                    All = o
                }).FirstOrDefaultAsync(e => e.Id == input.Id);

            var output = new GetEmployeeLoansForEditOutput { EmployeeLoans = ObjectMapper.Map<CreateOrEditEmployeeLoansDto>(employeeLoans.All) };
            output.EmployeeLoans.EmployeeName = employeeLoans.EmployeeName;
            output.EmployeeLoans.TotalDeducted = employeeLoans.TotalDeducted;
            output.EmployeeLoans.PayFreq = employeeLoans.PayFreq;

            var freq = output.EmployeeLoans.Frequency.ToCharArray();
            output.EmployeeLoans.FirstPeriod = freq.Contains('1');
            output.EmployeeLoans.SecondPeriod = freq.Contains('2');
            output.EmployeeLoans.ThirdPeriod = freq.Contains('3');
            output.EmployeeLoans.FourthPeriod = freq.Contains('4');
            output.EmployeeLoans.LastPeriod = freq.Contains('5');

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmployeeLoansDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeLoans_Create)]
        protected virtual async Task Create(CreateOrEditEmployeeLoansDto input)
        {
            input.Frequency += input.FirstPeriod ? "1" : string.Empty;
            input.Frequency += input.SecondPeriod ? "2" : string.Empty;
            input.Frequency += input.ThirdPeriod ? "3" : string.Empty;
            input.Frequency += input.FourthPeriod ? "4" : string.Empty;
            input.Frequency += input.LastPeriod ? "5" : string.Empty;
            input.Status = "ACTIVE";

            var employeeLoans = ObjectMapper.Map<EmployeeLoans>(input);
            employeeLoans.TenantId = AbpSession.TenantId ?? 1;

            await _employeeLoansRepository.InsertAsync(employeeLoans);
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeLoans_Edit)]
        protected virtual async Task Update(CreateOrEditEmployeeLoansDto input)
        {
            input.Frequency += input.FirstPeriod ? "1" : string.Empty;
            input.Frequency += input.SecondPeriod ? "2" : string.Empty;
            input.Frequency += input.ThirdPeriod ? "3" : string.Empty;
            input.Frequency += input.FourthPeriod ? "4" : string.Empty;
            input.Frequency += input.LastPeriod ? "5" : string.Empty;
            input.Status = "ACTIVE";

            var employeeLoans = await _employeeLoansRepository.FirstOrDefaultAsync((Guid)input.Id);

            ObjectMapper.Map(input, employeeLoans);
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeLoans_Delete)]
        public async Task Delete(EntityDto<Guid> input)
        {
            await _employeeLoansRepository.DeleteAsync(input.Id);
        }

    }
}