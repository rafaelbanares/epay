﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_ProcessHistory)]
    public class ProcessHistoryAppService : ePayAppServiceBase, IProcessHistoryAppService
    {
        private readonly IRepository<ProcessHistory> _processHistoryRepository;

        public ProcessHistoryAppService(IRepository<ProcessHistory> processHistoryRepository)
        {
            _processHistoryRepository = processHistoryRepository;
        }

        public async Task<PagedResultDto<GetProcessHistoryForViewDto>> GetAll(GetAllProcessHistoryInput input)
        {

            var filteredProcessHistory = _processHistoryRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Remarks.Contains(input.Filter))
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.Process.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.Process.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.ProcessStatusFilter != null, e => e.ProcessStatusId == input.ProcessStatusFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RemarksFilter), e => e.Remarks == input.RemarksFilter);

            var pagedAndFilteredProcessHistory = filteredProcessHistory
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var processHistory = from o in pagedAndFilteredProcessHistory
                                 select new GetProcessHistoryForViewDto()
                                 {
                                     ProcessHistory = new ProcessHistoryDto
                                     {
                                         PayrollNo = o.Process.PayrollNo,
                                         ProcessStatusId = o.ProcessStatusId,
                                         Remarks = o.Remarks,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredProcessHistory.CountAsync();

            return new PagedResultDto<GetProcessHistoryForViewDto>(
                totalCount,
                await processHistory.ToListAsync()
            );
        }

        public async Task<GetProcessHistoryForViewDto> GetProcessHistoryForView(int id)
        {
            var processHistory = await _processHistoryRepository.GetAsync(id);

            var output = new GetProcessHistoryForViewDto { ProcessHistory = ObjectMapper.Map<ProcessHistoryDto>(processHistory) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessHistory_Edit)]
        public async Task<GetProcessHistoryForEditOutput> GetProcessHistoryForEdit(EntityDto input)
        {
            var processHistory = await _processHistoryRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProcessHistoryForEditOutput { ProcessHistory = ObjectMapper.Map<CreateOrEditProcessHistoryDto>(processHistory) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProcessHistoryDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessHistory_Create)]
        protected virtual async Task Create(CreateOrEditProcessHistoryDto input)
        {
            var processHistory = ObjectMapper.Map<ProcessHistory>(input);
            await _processHistoryRepository.InsertAsync(processHistory);
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessHistory_Edit)]
        protected virtual async Task Update(CreateOrEditProcessHistoryDto input)
        {
            var processHistory = await _processHistoryRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, processHistory);
        }

        [AbpAuthorize(AppPermissions.Pages_ProcessHistory_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _processHistoryRepository.DeleteAsync(input.Id);
        }
    }
}