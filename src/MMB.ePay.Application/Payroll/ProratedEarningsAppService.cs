﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_ProratedEarnings)]
    public class ProratedEarningsAppService : ePayAppServiceBase, IProratedEarningsAppService
    {
        private readonly IRepository<ProratedEarnings> _proratedEarningsRepository;

        public ProratedEarningsAppService(IRepository<ProratedEarnings> proratedEarningsRepository)
        {
            _proratedEarningsRepository = proratedEarningsRepository;
        }

        public async Task<PagedResultDto<GetProratedEarningsForViewDto>> GetAll(GetAllProratedEarningsInput input)
        {
            var filteredProratedEarnings = _proratedEarningsRepository.GetAll();

            var pagedAndFilteredProratedEarnings = filteredProratedEarnings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var proratedEarnings = await pagedAndFilteredProratedEarnings
                .Select(e => new GetProratedEarningsForViewDto()
                {
                    ProratedEarnings = new ProratedEarningsDto
                    {
                        EarningType = e.EarningTypes.DisplayName,
                        InTimesheet = e.TimesheetTypes.DisplayName,
                        InOvertimeType = e.OvertimeTypes.DisplayName,
                        OutEarningType = e.OutEarningTypes.DisplayName,
                        Id = e.Id
                    }
                }).ToListAsync();

            var totalCount = await filteredProratedEarnings.CountAsync();

            return new PagedResultDto<GetProratedEarningsForViewDto>(
                totalCount,
                proratedEarnings
            );
        }

        public async Task<GetProratedEarningsForViewDto> GetProratedEarningsForView(int id)
        {
            var output = await _proratedEarningsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetProratedEarningsForViewDto
                {
                    ProratedEarnings = new ProratedEarningsDto
                    {
                        EarningType = e.EarningTypes.DisplayName,
                        InTimesheet = e.TimesheetTypes.DisplayName,
                        InOvertimeType = e.OvertimeTypes.DisplayName,
                        OutEarningType = e.OutEarningTypes.DisplayName,
                        Id = e.Id
                    }
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ProratedEarnings_Edit)]
        public async Task<GetProratedEarningsForEditOutput> GetProratedEarningsForEdit(EntityDto input)
        {
            var proratedEarnings = await _proratedEarningsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProratedEarningsForEditOutput 
            { 
                ProratedEarnings = ObjectMapper.Map<CreateOrEditProratedEarningsDto>(proratedEarnings) 
            };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProratedEarningsDto input)
        {
            input.InTimesheetTypeId = input.InTimesheetTypeId == 0 ? null : input.InTimesheetTypeId;
            input.InOvertimeTypeId = input.InOvertimeTypeId == 0 ? null : input.InOvertimeTypeId;

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ProratedEarnings_Create)]
        protected virtual async Task Create(CreateOrEditProratedEarningsDto input)
        {
            var proratedEarnings = ObjectMapper.Map<ProratedEarnings>(input);

            await _proratedEarningsRepository.InsertAsync(proratedEarnings);
        }

        [AbpAuthorize(AppPermissions.Pages_ProratedEarnings_Edit)]
        protected virtual async Task Update(CreateOrEditProratedEarningsDto input)
        {
            var proratedEarnings = await _proratedEarningsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, proratedEarnings);
        }
    }
}