﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_LoanTmp)]
    public class LoanTmpAppService : ePayAppServiceBase, ILoanTmpAppService
    {
        private readonly IRepository<LoanTmp> _loanTmpRepository;

        public LoanTmpAppService(IRepository<LoanTmp> loanTmpRepository)
        {
            _loanTmpRepository = loanTmpRepository;
        }

        public async Task<PagedResultDto<GetLoanTmpForViewDto>> GetAll(GetAllLoanTmpInput input)
        {

            var filteredLoanTmp = _loanTmpRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinEmployeeLoanIdFilter != null, e => e.EmployeeLoanId >= input.MinEmployeeLoanIdFilter)
                        .WhereIf(input.MaxEmployeeLoanIdFilter != null, e => e.EmployeeLoanId <= input.MaxEmployeeLoanIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SessionIdFilter.ToString()), e => e.SessionId.ToString() == input.SessionIdFilter.ToString());

            var pagedAndFilteredLoanTmp = filteredLoanTmp
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var loanTmp = from o in pagedAndFilteredLoanTmp
                          select new GetLoanTmpForViewDto()
                          {
                              LoanTmp = new LoanTmpDto
                              {
                                  PayrollNo = o.PayrollNo,
                                  Amount = o.Amount,
                                  EmployeeId = o.EmployeeId,
                                  EmployeeLoanId = o.EmployeeLoanId,
                                  SessionId = o.SessionId,
                                  Id = o.Id
                              }
                          };

            var totalCount = await filteredLoanTmp.CountAsync();

            return new PagedResultDto<GetLoanTmpForViewDto>(
                totalCount,
                await loanTmp.ToListAsync()
            );
        }

        public async Task<GetLoanTmpForViewDto> GetLoanTmpForView(int id)
        {
            var loanTmp = await _loanTmpRepository.GetAsync(id);

            var output = new GetLoanTmpForViewDto { LoanTmp = ObjectMapper.Map<LoanTmpDto>(loanTmp) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTmp_Edit)]
        public async Task<GetLoanTmpForEditOutput> GetLoanTmpForEdit(EntityDto input)
        {
            var loanTmp = await _loanTmpRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLoanTmpForEditOutput { LoanTmp = ObjectMapper.Map<CreateOrEditLoanTmpDto>(loanTmp) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLoanTmpDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTmp_Create)]
        protected virtual async Task Create(CreateOrEditLoanTmpDto input)
        {
            var loanTmp = ObjectMapper.Map<LoanTmp>(input);
            await _loanTmpRepository.InsertAsync(loanTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTmp_Edit)]
        protected virtual async Task Update(CreateOrEditLoanTmpDto input)
        {
            var loanTmp = await _loanTmpRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, loanTmp);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTmp_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _loanTmpRepository.DeleteAsync(input.Id);
        }
    }
}