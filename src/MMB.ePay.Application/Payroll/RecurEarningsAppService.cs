﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_RecurEarnings)]
    public class RecurEarningsAppService : ePayAppServiceBase, IRecurEarningsAppService
    {
        private readonly IRepository<RecurEarnings> _recurEarningsRepository;
        private readonly IRepository<EmployeePayroll> _employeePayrollRepository;

        public RecurEarningsAppService(IRepository<RecurEarnings> recurEarningsRepository, IRepository<EmployeePayroll> employeePayrollRepository)
        {
            _recurEarningsRepository = recurEarningsRepository;
            _employeePayrollRepository = employeePayrollRepository;
        }

        public async Task<PagedResultDto<GetRecurEarningsForViewDto>> GetAll(GetAllRecurEarningsInput input)
        {
            var filteredRecurEarnings = _recurEarningsRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeIdFilter);

            var pagedAndFilteredRecurEarnings = filteredRecurEarnings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var recurEarnings = from o in pagedAndFilteredRecurEarnings
                                select new GetRecurEarningsForViewDto()
                                {
                                    RecurEarnings = new RecurEarningsDto
                                    {
                                        EarningType = o.EarningTypes.DisplayName,
                                        Amount = o.Amount,
                                        StartDate = o.StartDate,
                                        EndDate = o.EndDate,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredRecurEarnings.CountAsync();

            return new PagedResultDto<GetRecurEarningsForViewDto>(
                totalCount,
                await recurEarnings.ToListAsync()
            );
        }

        public async Task<GetRecurEarningsForViewDto> GetRecurEarningsForView(int id)
        {
            var recurEarnings = await _recurEarningsRepository.GetAsync(id);

            var output = new GetRecurEarningsForViewDto { RecurEarnings = ObjectMapper.Map<RecurEarningsDto>(recurEarnings) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_RecurEarnings_Create)]
        public async Task<GetRecurEarningsForEditOutput> GetRecurEarningsForCreate(int employeeId)
        {
            var payFreq = await _employeePayrollRepository.GetAll()
                .Where(e => e.EmployeeId == employeeId)
                .Select(e => e.PayCodes.PayFreq)
                .FirstOrDefaultAsync();

            return new GetRecurEarningsForEditOutput
            {
                RecurEarnings = new CreateOrEditRecurEarningsDto
                {
                    EmployeeId = employeeId,
                    PayFreq = payFreq
                }
            };
        }

        [AbpAuthorize(AppPermissions.Pages_RecurEarnings_Edit)]
        public async Task<GetRecurEarningsForEditOutput> GetRecurEarningsForEdit(EntityDto input)
        {
            var recurEarnings = await _recurEarningsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRecurEarningsForEditOutput { RecurEarnings = ObjectMapper.Map<CreateOrEditRecurEarningsDto>(recurEarnings) };
            var freq = output.RecurEarnings.Frequency.ToCharArray();
            output.RecurEarnings.FirstPeriod = freq.Contains('1');
            output.RecurEarnings.SecondPeriod = freq.Contains('2');
            output.RecurEarnings.ThirdPeriod = freq.Contains('3');
            output.RecurEarnings.FourthPeriod = freq.Contains('4');
            output.RecurEarnings.LastPeriod = freq.Contains('5');

            output.RecurEarnings.PayFreq = await _employeePayrollRepository.GetAll()
                .Where(e => e.EmployeeId == recurEarnings.EmployeeId)
                .Select(e => e.PayCodes.PayFreq)
                .FirstOrDefaultAsync();

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditRecurEarningsDto input)
        {
            input.Frequency += input.FirstPeriod ? "1" : string.Empty;
            input.Frequency += input.SecondPeriod ? "2" : string.Empty;
            input.Frequency += input.ThirdPeriod ? "3" : string.Empty;
            input.Frequency += input.FourthPeriod ? "4" : string.Empty;
            input.Frequency += input.LastPeriod ? "5" : string.Empty;

            if (string.IsNullOrWhiteSpace(input.Frequency))
            {
                throw new UserFriendlyException("Your request is not valid!", "The Frequency field is required.");
            }

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_RecurEarnings_Create)]
        protected virtual async Task Create(CreateOrEditRecurEarningsDto input)
        {
            var recurEarnings = ObjectMapper.Map<RecurEarnings>(input);
            await _recurEarningsRepository.InsertAsync(recurEarnings);
        }

        [AbpAuthorize(AppPermissions.Pages_RecurEarnings_Edit)]
        protected virtual async Task Update(CreateOrEditRecurEarningsDto input)
        {
            var recurEarnings = await _recurEarningsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, recurEarnings);
        }

        [AbpAuthorize(AppPermissions.Pages_RecurEarnings_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _recurEarningsRepository.DeleteAsync(input.Id);
        }
    }
}