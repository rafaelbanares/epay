﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_PayrollPosted)]
    public class PayrollPostedAppService : ePayAppServiceBase, IPayrollPostedAppService
    {
        private readonly IRepository<PayrollPosted> _payrollPostedRepository;

        public PayrollPostedAppService(IRepository<PayrollPosted> payrollPostedRepository)
        {
            _payrollPostedRepository = payrollPostedRepository;
        }

        public async Task<PagedResultDto<GetPayrollPostedForViewDto>> GetAll(GetAllPayrollPostedInput input)
        {

            var filteredPayrollPosted = _payrollPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinSalaryFilter != null, e => e.Salary >= input.MinSalaryFilter)
                        .WhereIf(input.MaxSalaryFilter != null, e => e.Salary <= input.MaxSalaryFilter)
                        .WhereIf(input.MinDailyRateFilter != null, e => e.DailyRate >= input.MinDailyRateFilter)
                        .WhereIf(input.MaxDailyRateFilter != null, e => e.DailyRate <= input.MaxDailyRateFilter)
                        .WhereIf(input.MinHourlyRateFilter != null, e => e.HourlyRate >= input.MinHourlyRateFilter)
                        .WhereIf(input.MaxHourlyRateFilter != null, e => e.HourlyRate <= input.MaxHourlyRateFilter)
                        .WhereIf(input.MinTimesheetFilter != null, e => e.Timesheet >= input.MinTimesheetFilter)
                        .WhereIf(input.MaxTimesheetFilter != null, e => e.Timesheet <= input.MaxTimesheetFilter)
                        .WhereIf(input.MinOvertimeFilter != null, e => e.Overtime >= input.MinOvertimeFilter)
                        .WhereIf(input.MaxOvertimeFilter != null, e => e.Overtime <= input.MaxOvertimeFilter)
                        .WhereIf(input.MinTxEarningAmtFilter != null, e => e.TxEarningAmt >= input.MinTxEarningAmtFilter)
                        .WhereIf(input.MaxTxEarningAmtFilter != null, e => e.TxEarningAmt <= input.MaxTxEarningAmtFilter)
                        .WhereIf(input.MinNtxEarningAmtFilter != null, e => e.NtxEarningAmt >= input.MinNtxEarningAmtFilter)
                        .WhereIf(input.MaxNtxEarningAmtFilter != null, e => e.NtxEarningAmt <= input.MaxNtxEarningAmtFilter)
                        .WhereIf(input.MinDedTaxAmtFilter != null, e => e.DedTaxAmt >= input.MinDedTaxAmtFilter)
                        .WhereIf(input.MaxDedTaxAmtFilter != null, e => e.DedTaxAmt <= input.MaxDedTaxAmtFilter)
                        .WhereIf(input.MinDeductionAmtFilter != null, e => e.DeductionAmt >= input.MinDeductionAmtFilter)
                        .WhereIf(input.MaxDeductionAmtFilter != null, e => e.DeductionAmt <= input.MaxDeductionAmtFilter)
                        .WhereIf(input.MinLoanAmtFilter != null, e => e.LoanAmt >= input.MinLoanAmtFilter)
                        .WhereIf(input.MaxLoanAmtFilter != null, e => e.LoanAmt <= input.MaxLoanAmtFilter)
                        .WhereIf(input.MinBasicPayFilter != null, e => e.BasicPay >= input.MinBasicPayFilter)
                        .WhereIf(input.MaxBasicPayFilter != null, e => e.BasicPay <= input.MaxBasicPayFilter)
                        .WhereIf(input.MinGrossPayFilter != null, e => e.GrossPay >= input.MinGrossPayFilter)
                        .WhereIf(input.MaxGrossPayFilter != null, e => e.GrossPay <= input.MaxGrossPayFilter)
                        .WhereIf(input.MinNetPayFilter != null, e => e.NetPay >= input.MinNetPayFilter)
                        .WhereIf(input.MaxNetPayFilter != null, e => e.NetPay <= input.MaxNetPayFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinBankBranchIdFilter != null, e => e.BankBranchId >= input.MinBankBranchIdFilter)
                        .WhereIf(input.MaxBankBranchIdFilter != null, e => e.BankBranchId <= input.MaxBankBranchIdFilter);

            var pagedAndFilteredPayrollPosted = filteredPayrollPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var payrollPosted = from o in pagedAndFilteredPayrollPosted
                                select new GetPayrollPostedForViewDto()
                                {
                                    PayrollPosted = new PayrollPostedDto
                                    {
                                        PayrollNo = o.PayrollNo,
                                        Salary = o.Salary,
                                        DailyRate = o.DailyRate,
                                        HourlyRate = o.HourlyRate,
                                        Timesheet = o.Timesheet,
                                        Overtime = o.Overtime,
                                        TxEarningAmt = o.TxEarningAmt,
                                        NtxEarningAmt = o.NtxEarningAmt,
                                        DedTaxAmt = o.DedTaxAmt,
                                        DeductionAmt = o.DeductionAmt,
                                        LoanAmt = o.LoanAmt,
                                        BasicPay = o.BasicPay,
                                        GrossPay = o.GrossPay,
                                        NetPay = o.NetPay,
                                        EmployeeId = o.EmployeeId,
                                        PayCodeId = o.PayCodeId,
                                        BankBranchId = o.BankBranchId,
                                        Id = o.Id
                                    }
                                };

            var totalCount = await filteredPayrollPosted.CountAsync();

            return new PagedResultDto<GetPayrollPostedForViewDto>(
                totalCount,
                await payrollPosted.ToListAsync()
            );
        }

        public async Task<GetPayrollPostedForViewDto> GetPayrollPostedForView(int id)
        {
            var payrollPosted = await _payrollPostedRepository.GetAsync(id);

            var output = new GetPayrollPostedForViewDto { PayrollPosted = ObjectMapper.Map<PayrollPostedDto>(payrollPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollPosted_Edit)]
        public async Task<GetPayrollPostedForEditOutput> GetPayrollPostedForEdit(EntityDto input)
        {
            var payrollPosted = await _payrollPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPayrollPostedForEditOutput { PayrollPosted = ObjectMapper.Map<CreateOrEditPayrollPostedDto>(payrollPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPayrollPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollPosted_Create)]
        protected virtual async Task Create(CreateOrEditPayrollPostedDto input)
        {
            var payrollPosted = ObjectMapper.Map<PayrollPosted>(input);

            await _payrollPostedRepository.InsertAsync(payrollPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollPosted_Edit)]
        protected virtual async Task Update(CreateOrEditPayrollPostedDto input)
        {
            var payrollPosted = await _payrollPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, payrollPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_PayrollPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _payrollPostedRepository.DeleteAsync(input.Id);
        }
    }
}