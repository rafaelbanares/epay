﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_LoanTypes)]
    public class LoanTypesAppService : ePayAppServiceBase, ILoanTypesAppService
    {
        private readonly IRepository<LoanTypes> _loanTypesRepository;

        public LoanTypesAppService(IRepository<LoanTypes> loanTypesRepository)
        {
            _loanTypesRepository = loanTypesRepository;
        }

        public async Task<IList<SelectListItem>> GetLoanTypeList()
        {
            var loanTypes = await _loanTypesRepository.GetAll()
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.Description
                }).ToListAsync();

            loanTypes.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            return loanTypes;
        }

        public async Task<PagedResultDto<GetLoanTypesForViewDto>> GetAll(GetAllLoanTypesInput input)
        {

            var filteredLoanTypes = _loanTypesRepository.GetAll();

            var pagedAndFilteredLoanTypes = filteredLoanTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var loanTypes = from o in pagedAndFilteredLoanTypes
                            select new GetLoanTypesForViewDto()
                            {
                                LoanTypes = new LoanTypesDto
                                {
                                    SysCode = o.SysCode,
                                    Description = o.Description,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredLoanTypes.CountAsync();

            return new PagedResultDto<GetLoanTypesForViewDto>(
                totalCount,
                await loanTypes.ToListAsync()
            );
        }

        public async Task<GetLoanTypesForViewDto> GetLoanTypesForView(int id)
        {
            var loanTypes = await _loanTypesRepository.GetAsync(id);

            var output = new GetLoanTypesForViewDto { LoanTypes = ObjectMapper.Map<LoanTypesDto>(loanTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTypes_Edit)]
        public async Task<GetLoanTypesForEditOutput> GetLoanTypesForEdit(EntityDto input)
        {
            var loanTypes = await _loanTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLoanTypesForEditOutput { LoanTypes = ObjectMapper.Map<CreateOrEditLoanTypesDto>(loanTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLoanTypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTypes_Create)]
        protected virtual async Task Create(CreateOrEditLoanTypesDto input)
        {
            var loanTypes = ObjectMapper.Map<LoanTypes>(input);
            await _loanTypesRepository.InsertAsync(loanTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanTypes_Edit)]
        protected virtual async Task Update(CreateOrEditLoanTypesDto input)
        {
            var loanTypes = await _loanTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, loanTypes);
        }
    }
}