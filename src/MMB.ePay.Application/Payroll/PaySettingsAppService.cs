﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_PaySettings)]
    public class PaySettingsAppService : ePayAppServiceBase, IPaySettingsAppService
    {
        private readonly IRepository<PaySettings> _paySettingsRepository;

        public PaySettingsAppService(IRepository<PaySettings> paySettingsRepository)
        {
            _paySettingsRepository = paySettingsRepository;
        }

        public async Task<PagedResultDto<GetPaySettingsForViewDto>> GetAll(GetAllPaySettingsInput input)
        {

            var filteredPaySettings = _paySettingsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Key.Contains(input.Filter) || e.Value.Contains(input.Filter) || e.DataType.Contains(input.Filter) || e.Caption.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.KeyFilter), e => e.Key == input.KeyFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ValueFilter), e => e.Value == input.ValueFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DataTypeFilter), e => e.DataType == input.DataTypeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CaptionFilter), e => e.Caption == input.CaptionFilter)
                        .WhereIf(input.MinDisplayOrderFilter != null, e => e.DisplayOrder >= input.MinDisplayOrderFilter)
                        .WhereIf(input.MaxDisplayOrderFilter != null, e => e.DisplayOrder <= input.MaxDisplayOrderFilter)
                        .WhereIf(input.GroupSettingIdFilter.HasValue, e => e.GroupSettingId == input.GroupSettingIdFilter);

            var pagedAndFilteredPaySettings = filteredPaySettings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var paySettings = from o in pagedAndFilteredPaySettings
                              select new GetPaySettingsForViewDto()
                              {
                                  PaySettings = new PaySettingsDto
                                  {
                                      Key = o.Key,
                                      Value = o.Value,
                                      DataType = o.DataType,
                                      Caption = o.Caption,
                                      DisplayOrder = o.DisplayOrder,
                                      GroupSetting = o.GroupSettings.DisplayName,
                                      Id = o.Id
                                  }
                              };

            var totalCount = await filteredPaySettings.CountAsync();

            return new PagedResultDto<GetPaySettingsForViewDto>(
                totalCount,
                await paySettings.ToListAsync()
            );
        }

        public async Task<GetPaySettingsForViewDto> GetPaySettingsForView(int id)
        {
            var paySettings = await _paySettingsRepository.GetAsync(id);

            var output = new GetPaySettingsForViewDto { PaySettings = ObjectMapper.Map<PaySettingsDto>(paySettings) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_PaySettings_Edit)]
        public async Task<GetPaySettingsForEditOutput> GetPaySettingsForEdit(EntityDto input)
        {
            var paySettings = await _paySettingsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPaySettingsForEditOutput { PaySettings = ObjectMapper.Map<CreateOrEditPaySettingsDto>(paySettings) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditPaySettingsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_PaySettings_Create)]
        protected virtual async Task Create(CreateOrEditPaySettingsDto input)
        {
            var paySettings = ObjectMapper.Map<PaySettings>(input);
            await _paySettingsRepository.InsertAsync(paySettings);
        }

        [AbpAuthorize(AppPermissions.Pages_PaySettings_Edit)]
        protected virtual async Task Update(CreateOrEditPaySettingsDto input)
        {
            var paySettings = await _paySettingsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, paySettings);
        }

        [AbpAuthorize(AppPermissions.Pages_PaySettings_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _paySettingsRepository.DeleteAsync(input.Id);
        }
    }
}