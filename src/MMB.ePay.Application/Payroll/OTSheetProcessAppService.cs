﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_OTSheetProcess)]
    public class OTSheetProcessAppService : ePayAppServiceBase, IOTSheetProcessAppService
    {
        private readonly IRepository<OTSheetProcess> _otSheetProcessRepository;

        public OTSheetProcessAppService(IRepository<OTSheetProcess> otSheetProcessRepository)
        {
            _otSheetProcessRepository = otSheetProcessRepository;
        }

        public async Task<PagedResultDto<GetOTSheetProcessForViewDto>> GetAll(GetAllOTSheetProcessInput input)
        {

            var filteredOTSheetProcess = _otSheetProcessRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.OvertimeTypeFilter.HasValue, e => e.OvertimeTypeId == input.OvertimeTypeFilter)
                        .WhereIf(input.MinCostCenterFilter != null, e => e.CostCenter >= input.MinCostCenterFilter)
                        .WhereIf(input.MaxCostCenterFilter != null, e => e.CostCenter <= input.MaxCostCenterFilter)
                        .WhereIf(input.MinHrsFilter != null, e => e.Hrs >= input.MinHrsFilter)
                        .WhereIf(input.MaxHrsFilter != null, e => e.Hrs <= input.MaxHrsFilter)
                        .WhereIf(input.MinMinsFilter != null, e => e.Mins >= input.MinMinsFilter)
                        .WhereIf(input.MaxMinsFilter != null, e => e.Mins <= input.MaxMinsFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId <= input.MaxEmployeeIdFilter);

            var pagedAndFilteredOTSheetProcess = filteredOTSheetProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var otSheetProcess = from o in pagedAndFilteredOTSheetProcess
                                 select new GetOTSheetProcessForViewDto()
                                 {
                                     OTSheetProcess = new OTSheetProcessDto
                                     {
                                         PayrollNo = o.PayrollProcess.PayrollNo,
                                         OvertimeType = o.OvertimeTypes.DisplayName,
                                         CostCenter = o.CostCenter,
                                         Hrs = o.Hrs,
                                         Mins = o.Mins,
                                         Amount = o.Amount,
                                         EmployeeId = o.PayrollProcess.EmployeeId,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredOTSheetProcess.CountAsync();

            return new PagedResultDto<GetOTSheetProcessForViewDto>(
                totalCount,
                await otSheetProcess.ToListAsync()
            );
        }

        public async Task<GetOTSheetProcessForViewDto> GetOTSheetProcessForView(int id)
        {
            var otSheetProcess = await _otSheetProcessRepository.GetAsync(id);

            var output = new GetOTSheetProcessForViewDto { OTSheetProcess = ObjectMapper.Map<OTSheetProcessDto>(otSheetProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetProcess_Edit)]
        public async Task<GetOTSheetProcessForEditOutput> GetOTSheetProcessForEdit(EntityDto input)
        {
            var otSheetProcess = await _otSheetProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetOTSheetProcessForEditOutput { OTSheetProcess = ObjectMapper.Map<CreateOrEditOTSheetProcessDto>(otSheetProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditOTSheetProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetProcess_Create)]
        protected virtual async Task Create(CreateOrEditOTSheetProcessDto input)
        {
            var otSheetProcess = ObjectMapper.Map<OTSheetProcess>(input);
            await _otSheetProcessRepository.InsertAsync(otSheetProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetProcess_Edit)]
        protected virtual async Task Update(CreateOrEditOTSheetProcessDto input)
        {
            var otSheetProcess = await _otSheetProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, otSheetProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_OTSheetProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _otSheetProcessRepository.DeleteAsync(input.Id);
        }
    }
}