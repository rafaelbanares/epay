﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MMB.ePay.Payroll
{
    public class HelperAppService : ePayAppServiceBase, IHelperAppService
    {
        public IList<SelectListItem> GetMonthList()
        {
            return Enumerable.Range(1, 12)
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = DateTimeFormatInfo.CurrentInfo.GetMonthName(e)
                }).ToList();
        }

        public IList<SelectListItem> GetQuarterList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "0", Text = "1st" },
                new SelectListItem { Value = "1", Text = "2nd" },
                new SelectListItem { Value = "2", Text = "3rd" },
                new SelectListItem { Value = "3", Text = "4th" }
            };
        }

        public IList<SelectListItem> GetPaymentTypeList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "BANK", Text = "Bank" },
                new SelectListItem { Value = "CASH", Text = "Cash" },
                new SelectListItem { Value = "CHEQUE", Text = "Cheque" }
            };
        }

        public IList<SelectListItem> GetFrequencyList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "S", Text = "Semi-Monthly" },
                new SelectListItem { Value = "M", Text = "Monthly" },
                new SelectListItem { Value = "W", Text = "Weekly" }
            };
        }

        public IList<SelectListItem> GetTaxFrequencyList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "P", Text = "Every Payroll" },
                new SelectListItem { Value = "M", Text = "End of the Month" }
            };
        }

        public IList<SelectListItem> GetTaxMethodList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "NORMAL", Text = "Normal" },
                new SelectListItem { Value = "PERCENTAGE", Text = "Percentage" }
            };
        }

        public string GetFrequencyByCode(string code)
        {
            return GetFrequencyList().First(e => e.Value == code).Text;
        }

        public string GetTaxFrequencyByCode(string code)
        {
            return GetTaxFrequencyList().First(e => e.Value == code).Text;
        }

        public string GetTaxMethodByCode(string code)
        {
            return GetTaxMethodList().First(e => e.Value == code).Text;
        }
    }
}