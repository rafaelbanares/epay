﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_DeductionTypes)]
    public class DeductionTypesAppService : ePayAppServiceBase, IDeductionTypesAppService
    {
        private readonly IRepository<DeductionTypes> _deductionTypesRepository;
        private readonly IRepository<Deductions> _deductionsRepository;
        private readonly IRepository<RecurDeductions> _recurDeductionsRepository;

        public DeductionTypesAppService(IRepository<DeductionTypes> deductionTypesRepository,
            IRepository<Deductions> deductionsRepository,
            IRepository<RecurDeductions> recurDeductionsRepository)
        {
            _deductionTypesRepository = deductionTypesRepository;
            _deductionsRepository = deductionsRepository;
            _recurDeductionsRepository = recurDeductionsRepository;
        }

        //public async Task<IList<SelectListItem>> GetDeductionTypeList()
        //{
        //    var deductionTypes = await _deductionTypesRepository.GetAll()
        //        .Where(e => e.Active)
        //        .Select(e => new SelectListItem
        //        {
        //            Value = e.Id.ToString(),
        //            Text = e.DisplayName
        //        }).ToListAsync();

        //    deductionTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

        //    return deductionTypes;
        //}

        public async Task<IList<SelectListItem>> GetDeductionTypeListForDeductions(int? deductionId, int? employeeId)
        {
            if (!employeeId.HasValue)
            {
                employeeId = await _deductionsRepository.GetAll()
                    .Where(e => e.Id == deductionId)
                    .Select(e => e.EmployeeId)
                    .FirstOrDefaultAsync();
            }

            var deductionTypes = await _deductionTypesRepository.GetAll()
                .WhereIf(deductionId.HasValue, e => !e.Deductions.Any(f => f.EmployeeId == employeeId && f.Id != deductionId))
                .WhereIf(!deductionId.HasValue, e => !e.Deductions.Any(f => f.EmployeeId == employeeId))
                .Where(e => e.Active)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            deductionTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return deductionTypes;
        }

        public async Task<IList<SelectListItem>> GetDeductionTypeListForRecurDeductions(int? recurDeductionId, int? employeeId)
        {
            if (!employeeId.HasValue)
            {
                employeeId = await _recurDeductionsRepository.GetAll()
                    .Where(e => e.Id == recurDeductionId)
                    .Select(e => e.EmployeeId)
                    .FirstOrDefaultAsync();
            }

            var deductionTypes = await _deductionTypesRepository.GetAll()
                .WhereIf(recurDeductionId.HasValue, e => !e.RecurDeductions.Any(f => f.EmployeeId == employeeId && f.Id != recurDeductionId))
                .WhereIf(!recurDeductionId.HasValue, e => !e.RecurDeductions.Any(f => f.EmployeeId == employeeId))
                .Where(e => e.Active)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            deductionTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return deductionTypes;
        }

        public async Task<PagedResultDto<GetDeductionTypesForViewDto>> GetAll(GetAllDeductionTypesInput input)
        {

            var filteredDeductionTypes = _deductionTypesRepository.GetAll();

            var pagedAndFilteredDeductionTypes = filteredDeductionTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var deductionTypes = from o in pagedAndFilteredDeductionTypes
                                 select new GetDeductionTypesForViewDto()
                                 {
                                     DeductionTypes = new DeductionTypesDto
                                     {
                                         DisplayName = o.DisplayName,
                                         Description = o.Description,
                                         Currency = o.Currency,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await filteredDeductionTypes.CountAsync();

            return new PagedResultDto<GetDeductionTypesForViewDto>(
                totalCount,
                await deductionTypes.ToListAsync()
            );
        }

        public async Task<GetDeductionTypesForViewDto> GetDeductionTypesForView(int id)
        {
            var deductionTypes = await _deductionTypesRepository.GetAsync(id);

            var output = new GetDeductionTypesForViewDto { DeductionTypes = ObjectMapper.Map<DeductionTypesDto>(deductionTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTypes_Edit)]
        public async Task<GetDeductionTypesForEditOutput> GetDeductionTypesForEdit(EntityDto input)
        {
            var deductionTypes = await _deductionTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDeductionTypesForEditOutput { DeductionTypes = ObjectMapper.Map<CreateOrEditDeductionTypesDto>(deductionTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDeductionTypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTypes_Create)]
        protected virtual async Task Create(CreateOrEditDeductionTypesDto input)
        {
            var deductionTypes = ObjectMapper.Map<DeductionTypes>(input);
            deductionTypes.Active = true;

            await _deductionTypesRepository.InsertAsync(deductionTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionTypes_Edit)]
        protected virtual async Task Update(CreateOrEditDeductionTypesDto input)
        {
            var deductionTypes = await _deductionTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, deductionTypes);
        }
    }
}