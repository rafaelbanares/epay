﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_SssTables)]
    public class SssTablesAppService : ePayAppServiceBase, ISssTablesAppService
    {
        private readonly IRepository<SssTables> _sssTablesRepository;

        public SssTablesAppService(IRepository<SssTables> sssTablesRepository)
        {
            _sssTablesRepository = sssTablesRepository;
        }

        public async Task<PagedResultDto<GetSssTablesForViewDto>> GetAll(GetAllSssTablesInput input)
        {

            var filteredSssTables = _sssTablesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinMonthlySalCreditFilter != null, e => e.MonthlySalCredit >= input.MinMonthlySalCreditFilter)
                        .WhereIf(input.MaxMonthlySalCreditFilter != null, e => e.MonthlySalCredit <= input.MaxMonthlySalCreditFilter)
                        .WhereIf(input.MinLBFilter != null, e => e.LB >= input.MinLBFilter)
                        .WhereIf(input.MaxLBFilter != null, e => e.LB <= input.MaxLBFilter)
                        .WhereIf(input.MinUBFilter != null, e => e.UB >= input.MinUBFilter)
                        .WhereIf(input.MaxUBFilter != null, e => e.UB <= input.MaxUBFilter)
                        .WhereIf(input.MinEmployerSSSFilter != null, e => e.EmployerSSS >= input.MinEmployerSSSFilter)
                        .WhereIf(input.MaxEmployerSSSFilter != null, e => e.EmployerSSS <= input.MaxEmployerSSSFilter)
                        .WhereIf(input.MinEmployerECCFilter != null, e => e.EmployerECC >= input.MinEmployerECCFilter)
                        .WhereIf(input.MaxEmployerECCFilter != null, e => e.EmployerECC <= input.MaxEmployerECCFilter)
                        .WhereIf(input.MinEmployeeSSSFilter != null, e => e.EmployeeSSS >= input.MinEmployeeSSSFilter)
                        .WhereIf(input.MaxEmployeeSSSFilter != null, e => e.EmployeeSSS <= input.MaxEmployeeSSSFilter);

            var pagedAndFilteredSssTables = filteredSssTables
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var sssTables = from o in pagedAndFilteredSssTables
                            select new GetSssTablesForViewDto()
                            {
                                SssTables = new SssTablesDto
                                {
                                    MonthlySalCredit = o.MonthlySalCredit,
                                    LB = o.LB,
                                    UB = o.UB,
                                    EmployerSSS = o.EmployerSSS,
                                    EmployerECC = o.EmployerECC,
                                    EmployeeSSS = o.EmployeeSSS,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredSssTables.CountAsync();

            return new PagedResultDto<GetSssTablesForViewDto>(
                totalCount,
                await sssTables.ToListAsync()
            );
        }

        public async Task<GetSssTablesForViewDto> GetSssTablesForView(int id)
        {
            var sssTables = await _sssTablesRepository.GetAsync(id);

            var output = new GetSssTablesForViewDto { SssTables = ObjectMapper.Map<SssTablesDto>(sssTables) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_SssTables_Edit)]
        public async Task<GetSssTablesForEditOutput> GetSssTablesForEdit(EntityDto input)
        {
            var sssTables = await _sssTablesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSssTablesForEditOutput { SssTables = ObjectMapper.Map<CreateOrEditSssTablesDto>(sssTables) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditSssTablesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_SssTables_Create)]
        protected virtual async Task Create(CreateOrEditSssTablesDto input)
        {
            var sssTables = ObjectMapper.Map<SssTables>(input);
            await _sssTablesRepository.InsertAsync(sssTables);
        }

        [AbpAuthorize(AppPermissions.Pages_SssTables_Edit)]
        protected virtual async Task Update(CreateOrEditSssTablesDto input)
        {
            var sssTables = await _sssTablesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, sssTables);
        }

        [AbpAuthorize(AppPermissions.Pages_SssTables_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _sssTablesRepository.DeleteAsync(input.Id);
        }
    }
}