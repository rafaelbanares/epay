﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Dependents)]
    public class DependentsAppService : ePayAppServiceBase, IDependentsAppService
    {
        private readonly IRepository<Dependents> _dependentsRepository;

        public DependentsAppService(IRepository<Dependents> dependentsRepository)
        {
            _dependentsRepository = dependentsRepository;
        }

        public async Task<PagedResultDto<GetDependentsForViewDto>> GetAll(GetAllDependentsInput input)
        {

            var filteredDependents = _dependentsRepository.GetAll()
                .Where(e => input.EmployeeFilterId == e.EmployeeId);;

            var pagedAndFilteredDependents = filteredDependents
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var dependents = from o in pagedAndFilteredDependents
                            select new GetDependentsForViewDto()
                            {
                                Dependents = new DependentsDto
                                {
                                    DependentName = o.DependentName,
                                    BirthDate = o.BirthDate,
                                    EmployeeId = o.EmployeeId,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredDependents.CountAsync();

            return new PagedResultDto<GetDependentsForViewDto>(
                totalCount,
                await dependents.ToListAsync()
            );
        }

        public async Task<GetDependentsForViewDto> GetDependentsForView(int id)
        {
            var dependents = await _dependentsRepository.GetAsync(id);

            var output = new GetDependentsForViewDto { Dependents = ObjectMapper.Map<DependentsDto>(dependents) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Dependents_Edit)]
        public async Task<GetDependentsForEditOutput> GetDependentsForEdit(EntityDto input)
        {
            var dependents = await _dependentsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDependentsForEditOutput { Dependents = ObjectMapper.Map<CreateOrEditDependentsDto>(dependents) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDependentsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Dependents_Create)]
        protected virtual async Task Create(CreateOrEditDependentsDto input)
        {
            var dependents = ObjectMapper.Map<Dependents>(input);
            await _dependentsRepository.InsertAsync(dependents);
        }

        [AbpAuthorize(AppPermissions.Pages_Dependents_Edit)]
        protected virtual async Task Update(CreateOrEditDependentsDto input)
        {
            var dependents = await _dependentsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, dependents);
        }

        [AbpAuthorize(AppPermissions.Pages_Dependents_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _dependentsRepository.DeleteAsync(input.Id);
        }
    }
}