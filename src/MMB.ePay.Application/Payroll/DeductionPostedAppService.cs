﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_DeductionPosted)]
    public class DeductionPostedAppService : ePayAppServiceBase, IDeductionPostedAppService
    {
        private readonly IRepository<DeductionPosted> _deductionPostedRepository;

        public DeductionPostedAppService(IRepository<DeductionPosted> deductionPostedRepository)
        {
            _deductionPostedRepository = deductionPostedRepository;
        }

        public async Task<PagedResultDto<GetDeductionPostedForViewDto>> GetAll(GetAllDeductionPostedInput input)
        {

            var filteredDeductionPosted = _deductionPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinDeductionTypeIdFilter != null, e => e.DeductionTypeId >= input.MinDeductionTypeIdFilter)
                        .WhereIf(input.MaxDeductionTypeIdFilter != null, e => e.DeductionTypeId <= input.MaxDeductionTypeIdFilter);

            var pagedAndFilteredDeductionPosted = filteredDeductionPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var deductionPosted = from o in pagedAndFilteredDeductionPosted
                                  select new GetDeductionPostedForViewDto()
                                  {
                                      DeductionPosted = new DeductionPostedDto
                                      {
                                          Amount = o.Amount,
                                          DeductionTypeId = o.DeductionTypeId,
                                          Id = o.Id
                                      }
                                  };

            var totalCount = await filteredDeductionPosted.CountAsync();

            return new PagedResultDto<GetDeductionPostedForViewDto>(
                totalCount,
                await deductionPosted.ToListAsync()
            );
        }

        public async Task<GetDeductionPostedForViewDto> GetDeductionPostedForView(int id)
        {
            var deductionPosted = await _deductionPostedRepository.GetAsync(id);

            var output = new GetDeductionPostedForViewDto { DeductionPosted = ObjectMapper.Map<DeductionPostedDto>(deductionPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionPosted_Edit)]
        public async Task<GetDeductionPostedForEditOutput> GetDeductionPostedForEdit(EntityDto input)
        {
            var deductionPosted = await _deductionPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDeductionPostedForEditOutput { DeductionPosted = ObjectMapper.Map<CreateOrEditDeductionPostedDto>(deductionPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditDeductionPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionPosted_Create)]
        protected virtual async Task Create(CreateOrEditDeductionPostedDto input)
        {
            var deductionPosted = ObjectMapper.Map<DeductionPosted>(input);
            await _deductionPostedRepository.InsertAsync(deductionPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionPosted_Edit)]
        protected virtual async Task Update(CreateOrEditDeductionPostedDto input)
        {
            var deductionPosted = await _deductionPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, deductionPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_DeductionPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _deductionPostedRepository.DeleteAsync(input.Id);
        }
    }
}