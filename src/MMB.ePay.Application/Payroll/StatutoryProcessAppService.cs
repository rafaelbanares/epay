﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_StatutoryProcess)]
    public class StatutoryProcessAppService : ePayAppServiceBase, IStatutoryProcessAppService
    {
        private readonly IRepository<StatutoryProcess> _statutoryProcessRepository;

        public StatutoryProcessAppService(IRepository<StatutoryProcess> statutoryProcessRepository)
        {
            _statutoryProcessRepository = statutoryProcessRepository;
        }

        public async Task<PagedResultDto<GetStatutoryProcessForViewDto>> GetAll(GetAllStatutoryProcessInput input)
        {

            var filteredStatutoryProcess = _statutoryProcessRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollProcess.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.StatutoryTypeFilter.HasValue, e => e.StatutorySubTypeId == input.StatutoryTypeFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.PayrollProcess.EmployeeId <= input.MaxEmployeeIdFilter);

            var pagedAndFilteredStatutoryProcess = filteredStatutoryProcess
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var statutoryProcess = from o in pagedAndFilteredStatutoryProcess
                                   select new GetStatutoryProcessForViewDto()
                                   {
                                       StatutoryProcess = new StatutoryProcessDto
                                       {
                                           PayrollNo = o.PayrollProcess.PayrollNo,
                                           StatutoryType = o.StatutorySubTypes.DisplayName,
                                           Amount = o.Amount,
                                           EmployeeId = o.PayrollProcess.EmployeeId,
                                           Id = o.Id
                                       }
                                   };

            var totalCount = await filteredStatutoryProcess.CountAsync();

            return new PagedResultDto<GetStatutoryProcessForViewDto>(
                totalCount,
                await statutoryProcess.ToListAsync()
            );
        }

        public async Task<GetStatutoryProcessForViewDto> GetStatutoryProcessForView(int id)
        {
            var statutoryProcess = await _statutoryProcessRepository.GetAsync(id);

            var output = new GetStatutoryProcessForViewDto { StatutoryProcess = ObjectMapper.Map<StatutoryProcessDto>(statutoryProcess) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryProcess_Edit)]
        public async Task<GetStatutoryProcessForEditOutput> GetStatutoryProcessForEdit(EntityDto input)
        {
            var statutoryProcess = await _statutoryProcessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStatutoryProcessForEditOutput { StatutoryProcess = ObjectMapper.Map<CreateOrEditStatutoryProcessDto>(statutoryProcess) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditStatutoryProcessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryProcess_Create)]
        protected virtual async Task Create(CreateOrEditStatutoryProcessDto input)
        {
            var statutoryProcess = ObjectMapper.Map<StatutoryProcess>(input);
            await _statutoryProcessRepository.InsertAsync(statutoryProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryProcess_Edit)]
        protected virtual async Task Update(CreateOrEditStatutoryProcessDto input)
        {
            var statutoryProcess = await _statutoryProcessRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, statutoryProcess);
        }

        [AbpAuthorize(AppPermissions.Pages_StatutoryProcess_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _statutoryProcessRepository.DeleteAsync(input.Id);
        }
    }
}