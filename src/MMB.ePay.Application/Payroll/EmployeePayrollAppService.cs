﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EmployeePayroll)]
    public class EmployeePayrollAppService : ePayAppServiceBase, IEmployeePayrollAppService
    {
        private readonly IRepository<EmployeePayroll> _employeePayrollRepository;

        public EmployeePayrollAppService(IRepository<EmployeePayroll> employeePayrollRepository)
        {
            _employeePayrollRepository = employeePayrollRepository;
        }

        public async Task<string> GetPayFreqByEmployeeId(int employeeId)
        {
            var payFreq = await _employeePayrollRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Where(e => e.EmployeeId == employeeId)
                .Select(e => e.PayCodes.PayFreq)
                .FirstOrDefaultAsync();

            return payFreq;
        }

        public async Task<PagedResultDto<GetEmployeePayrollForViewDto>> GetAll(GetAllEmployeePayrollInput input)
        {

            var filteredEmployeePayroll = _employeePayrollRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PayrollTypeFilter), e => e.PayrollType == input.PayrollTypeFilter)
                        .WhereIf(input.MinSalaryFilter != null, e => e.Salary >= input.MinSalaryFilter)
                        .WhereIf(input.MaxSalaryFilter != null, e => e.Salary <= input.MaxSalaryFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PagibigNumberFilter), e => e.PagibigNumber == input.PagibigNumberFilter)
                        .WhereIf(input.MinPagibigAddFilter != null, e => e.PagibigAdd >= input.MinPagibigAddFilter)
                        .WhereIf(input.MaxPagibigAddFilter != null, e => e.PagibigAdd <= input.MaxPagibigAddFilter)
                        .WhereIf(input.MinPagibigRateFilter != null, e => e.PagibigRate >= input.MinPagibigRateFilter)
                        .WhereIf(input.MaxPagibigRateFilter != null, e => e.PagibigRate <= input.MaxPagibigRateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SSSNumberFilter), e => e.SSSNumber == input.SSSNumberFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhilhealthNumberFilter), e => e.PhilhealthNumber == input.PhilhealthNumberFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PaymentTypeFilter), e => e.PaymentType == input.PaymentTypeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BankAccountNoFilter), e => e.BankAccountNo == input.BankAccountNoFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TINFilter), e => e.TIN == input.TINFilter)
                        .WhereIf(input.MinTaxRateFilter != null, e => e.TaxRate >= input.MinTaxRateFilter)
                        .WhereIf(input.MaxTaxRateFilter != null, e => e.TaxRate <= input.MaxTaxRateFilter)
                        .WhereIf(input.RankFilter != null, e => e.RankId == input.RankFilter)
                        .WhereIf(input.OT_exemptFilter.HasValue && input.OT_exemptFilter > -1, e => (input.OT_exemptFilter == 1 && e.OT_exempt) || (input.OT_exemptFilter == 0 && !e.OT_exempt))
                        .WhereIf(input.SSS_exemptFilter.HasValue && input.SSS_exemptFilter > -1, e => (input.SSS_exemptFilter == 1 && e.SSS_exempt) || (input.SSS_exemptFilter == 0 && !e.SSS_exempt))
                        .WhereIf(input.PH_exemptFilter.HasValue && input.PH_exemptFilter > -1, e => (input.PH_exemptFilter == 1 && e.PH_exempt) || (input.PH_exemptFilter == 0 && !e.PH_exempt))
                        .WhereIf(input.PAG_exemptFilter.HasValue && input.PAG_exemptFilter > -1, e => (input.PAG_exemptFilter == 1 && e.PAG_exempt) || (input.PAG_exemptFilter == 0 && !e.PAG_exempt))
                        .WhereIf(input.Tax_exemptFilter.HasValue && input.Tax_exemptFilter > -1, e => (input.Tax_exemptFilter == 1 && e.Tax_exempt) || (input.Tax_exemptFilter == 0 && !e.Tax_exempt))
                        .WhereIf(input.ComputeAsDailyFilter.HasValue && input.ComputeAsDailyFilter > -1, e => (input.ComputeAsDailyFilter == 1 && e.ComputeAsDaily) || (input.ComputeAsDailyFilter == 0 && !e.ComputeAsDaily));

            var pagedAndFilteredEmployeePayroll = filteredEmployeePayroll
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var employeePayroll = from o in pagedAndFilteredEmployeePayroll
                                  select new GetEmployeePayrollForViewDto()
                                  {
                                      EmployeePayroll = new EmployeePayrollDto
                                      {
                                          PayrollType = o.PayrollType,
                                          Salary = o.Salary,
                                          PagibigNumber = o.PagibigNumber,
                                          PagibigAdd = o.PagibigAdd,
                                          PagibigRate = o.PagibigRate,
                                          SSSNumber = o.SSSNumber,
                                          PhilhealthNumber = o.PhilhealthNumber,
                                          PaymentType = o.PaymentType,
                                          BankAccountNo = o.BankAccountNo,
                                          TIN = o.TIN,
                                          TaxRate = o.TaxRate,
                                          EmploymentType = o.Employees.EmploymentTypes.DisplayName,
                                          Rank = o.Ranks.DisplayName,
                                          OT_exempt = o.OT_exempt,
                                          SSS_exempt = o.SSS_exempt,
                                          PH_exempt = o.PH_exempt,
                                          PAG_exempt = o.PAG_exempt,
                                          Tax_exempt = o.Tax_exempt,
                                          ComputeAsDaily = o.ComputeAsDaily,
                                          Id = o.Id
                                      }
                                  };

            var totalCount = await filteredEmployeePayroll.CountAsync();

            return new PagedResultDto<GetEmployeePayrollForViewDto>(
                totalCount,
                await employeePayroll.ToListAsync()
            );
        }

        public async Task<GetEmployeePayrollForViewDto> GetEmployeePayrollForView(int id)
        {
            var employeePayroll = await _employeePayrollRepository.GetAsync(id);

            var output = new GetEmployeePayrollForViewDto { EmployeePayroll = ObjectMapper.Map<EmployeePayrollDto>(employeePayroll) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeePayroll_Edit)]
        public async Task<GetEmployeePayrollForEditOutput> GetEmployeePayrollForEdit(EntityDto input)
        {
            //var employeePayroll = await _employeePayrollRepository.FirstOrDefaultAsync(input.Id);
            var employeePayroll = await _employeePayrollRepository.GetAll()
                .FirstOrDefaultAsync(e => e.EmployeeId == input.Id);

            var output = new GetEmployeePayrollForEditOutput { EmployeePayroll = ObjectMapper.Map<CreateOrEditEmployeePayrollDto>(employeePayroll) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmployeePayrollDto input)
        {
            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeePayroll_Create)]
        protected virtual async Task Create(CreateOrEditEmployeePayrollDto input)
        {
            var employeePayroll = ObjectMapper.Map<EmployeePayroll>(input);
            await _employeePayrollRepository.InsertAsync(employeePayroll);
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeePayroll_Edit)]
        protected virtual async Task Update(CreateOrEditEmployeePayrollDto input)
        {
            var employeePayroll = await _employeePayrollRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeId)
                .FirstOrDefaultAsync();

            ObjectMapper.Map(input, employeePayroll);
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeePayroll_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _employeePayrollRepository.DeleteAsync(input.Id);
        }
    }
}