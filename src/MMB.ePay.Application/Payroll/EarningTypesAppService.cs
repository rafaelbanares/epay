﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_EarningTypes)]
    public class EarningTypesAppService : ePayAppServiceBase, IEarningTypesAppService
    {
        private readonly IRepository<EarningTypes> _earningTypesRepository;
        private readonly IRepository<Earnings> _earningsRepository;
        private readonly IRepository<RecurEarnings> _recurEarningsRepository;

        public EarningTypesAppService(IRepository<EarningTypes> earningTypesRepository,
            IRepository<Earnings> earningsRepository,
            IRepository<RecurEarnings> recurEarningsRepository)
        {
            _earningTypesRepository = earningTypesRepository;
            _earningsRepository = earningsRepository;
            _recurEarningsRepository = recurEarningsRepository;
        }

        public async Task<IList<SelectListItem>> GetEarningTypesList()
        {
            var test = await _earningTypesRepository.GetAll().ToListAsync();


            return await _earningTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.DisplayName)
                .Select(e => new SelectListItem
                {
                    Text = e.DisplayName,
                    Value = e.Id.ToString()
                }).ToListAsync();




        }

        //public async Task<IList<SelectListItem>> GetEarningTypeList()
        //{
        //    var earningTypes = await _earningTypesRepository.GetAll()
        //        .Where(e => e.Active)
        //        .Select(e => new SelectListItem
        //        {
        //            Value = e.Id.ToString(),
        //            Text = e.DisplayName
        //        }).ToListAsync();

        //    earningTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

        //    return earningTypes;
        //}

        public async Task<IList<SelectListItem>> GetEarningTypeListForEarnings(int? earningId, int? employeeId)
        {
            if (!employeeId.HasValue)
            {
                employeeId = await _earningsRepository.GetAll()
                    .Where(e => e.Id == earningId)
                    .Select(e => e.EmployeeId)
                    .FirstOrDefaultAsync();
            }

            var earningTypes = await _earningTypesRepository.GetAll()
                .WhereIf(earningId.HasValue, e => !e.Earnings.Any(f => f.EmployeeId == employeeId && f.Id != earningId))
                .WhereIf(!earningId.HasValue, e => !e.Earnings.Any(f => f.EmployeeId == employeeId))
                .Where(e => e.Active)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            earningTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return earningTypes;
        }

        public async Task<IList<SelectListItem>> GetEarningTypeListForRecurEarnings(int? recurEarningId, int? employeeId)
        {
            if (!employeeId.HasValue)
            {
                employeeId = await _recurEarningsRepository.GetAll()
                    .Where(e => e.Id == recurEarningId)
                    .Select(e => e.EmployeeId)
                    .FirstOrDefaultAsync();
            }

            var earningTypes = await _earningTypesRepository.GetAll()
                .WhereIf(recurEarningId.HasValue, e => !e.RecurEarnings.Any(f => f.EmployeeId == employeeId && f.Id != recurEarningId))
                .WhereIf(!recurEarningId.HasValue, e => !e.RecurEarnings.Any(f => f.EmployeeId == employeeId))
                .Where(e => e.Active)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            earningTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return earningTypes;
        }

        public async Task<PagedResultDto<GetEarningTypesForViewDto>> GetAll(GetAllEarningTypesInput input)
        {

            var filteredEarningTypes = _earningTypesRepository.GetAll(); ;

            var pagedAndFilteredEarningTypes = filteredEarningTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var earningTypes = from o in pagedAndFilteredEarningTypes
                               select new GetEarningTypesForViewDto()
                               {
                                   EarningTypes = new EarningTypesDto
                                   {
                                       DisplayName = o.DisplayName,
                                       Description = o.Description,
                                       Currency = o.Currency,
                                       Taxable = o.Taxable,
                                       Id = o.Id
                                   }
                               };

            var totalCount = await filteredEarningTypes.CountAsync();

            return new PagedResultDto<GetEarningTypesForViewDto>(
                totalCount,
                await earningTypes.ToListAsync()
            );
        }

        public async Task<GetEarningTypesForViewDto> GetEarningTypesForView(int id)
        {
            var earningTypes = await _earningTypesRepository.GetAsync(id);

            var output = new GetEarningTypesForViewDto { EarningTypes = ObjectMapper.Map<EarningTypesDto>(earningTypes) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypes_Edit)]
        public async Task<GetEarningTypesForEditOutput> GetEarningTypesForEdit(EntityDto input)
        {
            var earningTypes = await _earningTypesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEarningTypesForEditOutput { EarningTypes = ObjectMapper.Map<CreateOrEditEarningTypesDto>(earningTypes) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEarningTypesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypes_Create)]
        protected virtual async Task Create(CreateOrEditEarningTypesDto input)
        {
            var earningTypes = ObjectMapper.Map<EarningTypes>(input);
            earningTypes.Active = true;

            await _earningTypesRepository.InsertAsync(earningTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_EarningTypes_Edit)]
        protected virtual async Task Update(CreateOrEditEarningTypesDto input)
        {
            var earningTypes = await _earningTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, earningTypes);
        }
    }
}