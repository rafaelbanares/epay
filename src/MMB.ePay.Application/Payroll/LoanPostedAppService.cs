﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_LoanPosted)]
    public class LoanPostedAppService : ePayAppServiceBase, ILoanPostedAppService
    {
        private readonly IRepository<LoanPosted> _loanPostedRepository;

        public LoanPostedAppService(IRepository<LoanPosted> loanPostedRepository)
        {
            _loanPostedRepository = loanPostedRepository;
        }

        public async Task<PagedResultDto<GetLoanPostedForViewDto>> GetAll(GetAllLoanPostedInput input)
        {

            var filteredLoanPosted = _loanPostedRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Remarks.Contains(input.Filter))
                        .WhereIf(input.MinPayrollNoFilter != null, e => e.PayrollNo >= input.MinPayrollNoFilter)
                        .WhereIf(input.MaxPayrollNoFilter != null, e => e.PayrollNo <= input.MaxPayrollNoFilter)
                        .WhereIf(input.MinPayDateFilter != null, e => e.PayDate >= input.MinPayDateFilter)
                        .WhereIf(input.MaxPayDateFilter != null, e => e.PayDate <= input.MaxPayDateFilter)
                        .WhereIf(input.MinAmountFilter != null, e => e.Amount >= input.MinAmountFilter)
                        .WhereIf(input.MaxAmountFilter != null, e => e.Amount <= input.MaxAmountFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RemarksFilter), e => e.Remarks == input.RemarksFilter)
                        .WhereIf(input.MinCreatedByFilter != null, e => e.CreatedBy >= input.MinCreatedByFilter)
                        .WhereIf(input.MaxCreatedByFilter != null, e => e.CreatedBy <= input.MaxCreatedByFilter)
                        .WhereIf(input.MinCreatedDateFilter != null, e => e.CreatedDate >= input.MinCreatedDateFilter)
                        .WhereIf(input.MaxCreatedDateFilter != null, e => e.CreatedDate <= input.MaxCreatedDateFilter);

            var pagedAndFilteredLoanPosted = filteredLoanPosted
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var loanPosted = from o in pagedAndFilteredLoanPosted
                             select new GetLoanPostedForViewDto()
                             {
                                 LoanPosted = new LoanPostedDto
                                 {
                                     PayrollNo = o.PayrollNo,
                                     PayDate = o.PayDate,
                                     Amount = o.Amount,
                                     Remarks = o.Remarks,
                                     CreatedBy = o.CreatedBy,
                                     CreatedDate = o.CreatedDate,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredLoanPosted.CountAsync();

            return new PagedResultDto<GetLoanPostedForViewDto>(
                totalCount,
                await loanPosted.ToListAsync()
            );
        }

        public async Task<GetLoanPostedForViewDto> GetLoanPostedForView(int id)
        {
            var loanPosted = await _loanPostedRepository.GetAsync(id);

            var output = new GetLoanPostedForViewDto { LoanPosted = ObjectMapper.Map<LoanPostedDto>(loanPosted) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LoanPosted_Edit)]
        public async Task<GetLoanPostedForEditOutput> GetLoanPostedForEdit(EntityDto input)
        {
            var loanPosted = await _loanPostedRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLoanPostedForEditOutput { LoanPosted = ObjectMapper.Map<CreateOrEditLoanPostedDto>(loanPosted) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLoanPostedDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LoanPosted_Create)]
        protected virtual async Task Create(CreateOrEditLoanPostedDto input)
        {
            var loanPosted = ObjectMapper.Map<LoanPosted>(input);
            await _loanPostedRepository.InsertAsync(loanPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanPosted_Edit)]
        protected virtual async Task Update(CreateOrEditLoanPostedDto input)
        {
            var loanPosted = await _loanPostedRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, loanPosted);
        }

        [AbpAuthorize(AppPermissions.Pages_LoanPosted_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _loanPostedRepository.DeleteAsync(input.Id);
        }
    }
}