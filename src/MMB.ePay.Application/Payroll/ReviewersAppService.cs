﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Payroll.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MMB.ePay.Payroll
{
    [AbpAuthorize(AppPermissions.Pages_Reviewers)]
    public class ReviewersAppService : ePayAppServiceBase, IReviewersAppService
    {
        private readonly IRepository<Reviewers> _reviewersRepository;

        public ReviewersAppService(IRepository<Reviewers> reviewersRepository)
        {
            _reviewersRepository = reviewersRepository;
        }

        public async Task<IList<SelectListItem>> GetReviewerTypeList()
        {
            var list = new List<SelectListItem> {
                new SelectListItem { Value = null, Text = "Select" },
                new SelectListItem { Value = "REVIEWER2", Text = "REVIEWER2" }
            };

            var reviewType = await _reviewersRepository.GetAll()
                .Select(e => e.ReviewerType)
                .OrderByDescending(e => e)
                .FirstOrDefaultAsync();

            if (reviewType != null)
            {
                var reviewNo = int.Parse(Regex.Match(reviewType, @"\d+").Value);

                for (var i = 3; i <= reviewNo + 1; i++)
                {
                    var val = "REVIEWER" + i.ToString();

                    list.Add(new SelectListItem { Value = val, Text = val });
                }
            }

            return list;
        }

        public async Task<PagedResultDto<GetReviewersForViewDto>> GetAll(GetAllReviewersInput input)
        {

            var filteredReviewers = _reviewersRepository.GetAll();

            var pagedAndFilteredReviewers = filteredReviewers
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var reviewers = from o in pagedAndFilteredReviewers
                            select new GetReviewersForViewDto()
                            {
                                Reviewers = new ReviewersDto
                                {
                                    ReviewerType = o.ReviewerType,
                                    ReviewedBy = o.Employees.FullName,
                                    Id = o.Id
                                }
                            };

            var totalCount = await filteredReviewers.CountAsync();

            return new PagedResultDto<GetReviewersForViewDto>(
                totalCount,
                await reviewers.ToListAsync()
            );
        }

        public async Task<GetReviewersForViewDto> GetReviewersForView(int id)
        {
            var output = await _reviewersRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetReviewersForViewDto
                {
                    Reviewers = new ReviewersDto
                    {
                        ReviewerType = e.ReviewerType,
                        ReviewedBy = e.Employees.FullName
                    }
                }).FirstOrDefaultAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Reviewers_Edit)]
        public async Task<GetReviewersForEditOutput> GetReviewersForEdit(EntityDto input)
        {
            var reviewers = await _reviewersRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetReviewersForEditOutput { Reviewers = ObjectMapper.Map<CreateOrEditReviewersDto>(reviewers) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditReviewersDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Reviewers_Create)]
        protected virtual async Task Create(CreateOrEditReviewersDto input)
        {
            var reviewers = ObjectMapper.Map<Reviewers>(input);
            await _reviewersRepository.InsertAsync(reviewers);
        }

        [AbpAuthorize(AppPermissions.Pages_Reviewers_Edit)]
        protected virtual async Task Update(CreateOrEditReviewersDto input)
        {
            var reviewers = await _reviewersRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, reviewers);
        }
    }
}