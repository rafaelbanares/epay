using System.Threading.Tasks;

namespace MMB.ePay.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}