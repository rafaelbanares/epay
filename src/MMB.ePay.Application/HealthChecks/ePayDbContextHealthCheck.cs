﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MMB.ePay.EntityFrameworkCore;

namespace MMB.ePay.HealthChecks
{
    public class ePayDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public ePayDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("ePayDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("ePayDbContext could not connect to database"));
        }
    }
}
