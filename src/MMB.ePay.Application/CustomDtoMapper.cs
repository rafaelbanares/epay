﻿using MMB.ePay.HR.Dtos;
using MMB.ePay.HR;
using MMB.ePay.Payroll.Dtos;
using MMB.ePay.Payroll;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityProperties;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using MMB.ePay.Auditing.Dto;
using MMB.ePay.Authorization.Accounts.Dto;
using MMB.ePay.Authorization.Delegation;
using MMB.ePay.Authorization.Permissions.Dto;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Roles.Dto;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Authorization.Users.Delegation.Dto;
using MMB.ePay.Authorization.Users.Dto;
using MMB.ePay.Authorization.Users.Importing.Dto;
using MMB.ePay.Authorization.Users.Profile.Dto;
using MMB.ePay.Chat;
using MMB.ePay.Chat.Dto;
using MMB.ePay.DynamicEntityProperties.Dto;
using MMB.ePay.Editions;
using MMB.ePay.Editions.Dto;
using MMB.ePay.Friendships;
using MMB.ePay.Friendships.Cache;
using MMB.ePay.Friendships.Dto;
using MMB.ePay.Localization.Dto;
using MMB.ePay.MultiTenancy;
using MMB.ePay.MultiTenancy.Dto;
using MMB.ePay.MultiTenancy.HostDashboard.Dto;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.MultiTenancy.Payments.Dto;
using MMB.ePay.Notifications.Dto;
using MMB.ePay.Organizations.Dto;
using MMB.ePay.Persons;
using MMB.ePay.Persons.Dto;
using MMB.ePay.Sessions.Dto;
using MMB.ePay.WebHooks.Dto;

namespace MMB.ePay
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateOrEditHolidaysDto, Holidays>().ReverseMap();
            configuration.CreateMap<HolidaysDto, Holidays>().ReverseMap();
            configuration.CreateMap<CreateOrEditCompaniesDto, Companies>().ReverseMap();
            configuration.CreateMap<CompaniesDto, Companies>().ReverseMap();
            configuration.CreateMap<CreateOrEditTimesheetTypesDto, TimesheetTypes>().ReverseMap();
            configuration.CreateMap<TimesheetTypesDto, TimesheetTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmploymentTypesDto, EmploymentTypes>().ReverseMap();
            configuration.CreateMap<EmploymentTypesDto, EmploymentTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditBanksDto, Banks>().ReverseMap();
            configuration.CreateMap<BanksDto, Banks>().ReverseMap();
            configuration.CreateMap<CreateOrEditStatutoryTypesDto, StatutorySubTypes>().ReverseMap();
            configuration.CreateMap<StatutoryTypesDto, StatutorySubTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditProcessStatusDto, ProcessStatus>().ReverseMap();
            configuration.CreateMap<ProcessStatusDto, ProcessStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditGroupSettingsDto, GroupSettings>().ReverseMap();
            configuration.CreateMap<GroupSettingsDto, GroupSettings>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeTypesDto, OvertimeTypes>().ReverseMap();
            configuration.CreateMap<OvertimeTypesDto, OvertimeTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditRanksDto, Ranks>().ReverseMap();
            configuration.CreateMap<RanksDto, Ranks>().ReverseMap();
            configuration.CreateMap<CreateOrEditTimesheetTmpDto, TimesheetTmp>().ReverseMap();
            configuration.CreateMap<TimesheetTmpDto, TimesheetTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditTimesheetsDto, Timesheets>().ReverseMap();
            configuration.CreateMap<TimesheetsDto, Timesheets>().ReverseMap();
            configuration.CreateMap<CreateOrEditTimesheetProcessDto, TimesheetProcess>().ReverseMap();
            configuration.CreateMap<TimesheetProcessDto, TimesheetProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditTimesheetPostedDto, TimesheetPosted>().ReverseMap();
            configuration.CreateMap<TimesheetPostedDto, TimesheetPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditTaxTablesDto, TaxTables>().ReverseMap();
            configuration.CreateMap<TaxTablesDto, TaxTables>().ReverseMap();
            configuration.CreateMap<CreateOrEditStatutoryTmpDto, StatutoryTmp>().ReverseMap();
            configuration.CreateMap<StatutoryTmpDto, StatutoryTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditStatutoryProcessDto, StatutoryProcess>().ReverseMap();
            configuration.CreateMap<StatutoryProcessDto, StatutoryProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditStatutoryPostedDto, StatutoryPosted>().ReverseMap();
            configuration.CreateMap<StatutoryPostedDto, StatutoryPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditSssTablesDto, SssTables>().ReverseMap();
            configuration.CreateMap<SssTablesDto, SssTables>().ReverseMap();
            configuration.CreateMap<CreateOrEditReviewersDto, Reviewers>().ReverseMap();
            configuration.CreateMap<ReviewersDto, Reviewers>().ReverseMap();
            configuration.CreateMap<CreateOrEditRecurEarningsDto, RecurEarnings>().ReverseMap();
            configuration.CreateMap<RecurEarningsDto, RecurEarnings>().ReverseMap();
            configuration.CreateMap<CreateOrEditRecurDeductionsDto, RecurDeductions>().ReverseMap();
            configuration.CreateMap<RecurDeductionsDto, RecurDeductions>().ReverseMap();
            configuration.CreateMap<CreateOrEditProcessHistoryDto, ProcessHistory>().ReverseMap();
            configuration.CreateMap<ProcessHistoryDto, ProcessHistory>().ReverseMap();
            configuration.CreateMap<CreateOrEditProcessDto, Process>().ReverseMap();
            configuration.CreateMap<ProcessDto, Process>().ReverseMap();
            configuration.CreateMap<CreateOrEditPaySettingsDto, PaySettings>().ReverseMap();
            configuration.CreateMap<PaySettingsDto, PaySettings>().ReverseMap();
            configuration.CreateMap<CreateOrEditPayrollTmpDto, PayrollTmp>().ReverseMap();
            configuration.CreateMap<PayrollTmpDto, PayrollTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditPayrollProcessDto, PayrollProcess>().ReverseMap();
            configuration.CreateMap<PayrollProcessDto, PayrollProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditPayrollPostedDto, PayrollPosted>().ReverseMap();
            configuration.CreateMap<PayrollPostedDto, PayrollPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditPayPeriodDto, PayPeriod>().ReverseMap();
            configuration.CreateMap<PayPeriodDto, PayPeriod>().ReverseMap();
            configuration.CreateMap<CreateOrEditProratedEarningsDto, ProratedEarnings>().ReverseMap();
            configuration.CreateMap<ProratedEarningsDto, ProratedEarnings>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeSubTypesDto, OvertimeSubTypesDto>().ReverseMap();
            configuration.CreateMap<OvertimeSubTypesDto, OvertimeSubTypesDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeMainTypesDto, OvertimeMainTypesDto>().ReverseMap();
            configuration.CreateMap<OvertimeMainTypesDto, OvertimeMainTypesDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditOTSheetTmpDto, OTSheetTmp>().ReverseMap();
            configuration.CreateMap<OTSheetTmpDto, OTSheetTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditOTSheetProcessDto, OTSheetProcess>().ReverseMap();
            configuration.CreateMap<OTSheetProcessDto, OTSheetProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditOTSheetPostedDto, OTSheetPosted>().ReverseMap();
            configuration.CreateMap<OTSheetPostedDto, OTSheetPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditOTSheetDto, OTSheet>().ReverseMap();
            configuration.CreateMap<OTSheetDto, OTSheet>().ReverseMap();
            configuration.CreateMap<CreateOrEditLoanTmpDto, LoanTmp>().ReverseMap();
            configuration.CreateMap<LoanTmpDto, LoanTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditLoanProcessDto, LoanProcess>().ReverseMap();
            configuration.CreateMap<LoanProcessDto, LoanProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveCreditsDto, LeaveCredits>().ReverseMap();
            configuration.CreateMap<LeaveCreditsDto, LeaveCredits>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployersDto, Employers>().ReverseMap();
            configuration.CreateMap<EmployersDto, Employers>().ReverseMap();
            configuration.CreateMap<CreateOrEditDependentsDto, Dependents>().ReverseMap();
            configuration.CreateMap<DependentsDto, Dependents>().ReverseMap();
            configuration.CreateMap<CreateOrEditEarningTypeSettingsDto, EarningTypeSettings>().ReverseMap();
            configuration.CreateMap<EarningTypeSettingsDto, EarningTypeSettings>().ReverseMap();
            configuration.CreateMap<CreateOrEditEarningTmpDto, EarningTmp>().ReverseMap();
            configuration.CreateMap<EarningTmpDto, EarningTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditEarningsDto, Earnings>().ReverseMap();
            configuration.CreateMap<EarningsDto, Earnings>().ReverseMap();
            configuration.CreateMap<CreateOrEditEarningProcessDto, EarningProcess>().ReverseMap();
            configuration.CreateMap<EarningProcessDto, EarningProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditEarningPostedDto, EarningPosted>().ReverseMap();
            configuration.CreateMap<EarningPostedDto, EarningPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditDeductionTmpDto, DeductionTmp>().ReverseMap();
            configuration.CreateMap<DeductionTmpDto, DeductionTmp>().ReverseMap();
            configuration.CreateMap<CreateOrEditDeductionsDto, Deductions>().ReverseMap();
            configuration.CreateMap<DeductionsDto, Deductions>().ReverseMap();
            configuration.CreateMap<CreateOrEditDeductionProcessDto, DeductionProcess>().ReverseMap();
            configuration.CreateMap<DeductionProcessDto, DeductionProcess>().ReverseMap();
            configuration.CreateMap<CreateOrEditDeductionPostedDto, DeductionPosted>().ReverseMap();
            configuration.CreateMap<DeductionPostedDto, DeductionPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditTenantBanksDto, TenantBanks>().ReverseMap();
            configuration.CreateMap<TenantBanksDto, TenantBanks>().ReverseMap();
            configuration.CreateMap<CreateOrEditContactPersonsDto, ContactPersons>().ReverseMap();
            configuration.CreateMap<ContactPersonsDto, ContactPersons>().ReverseMap();
            configuration.CreateMap<CreateOrEditCompanyStatutoryInfoDto, CompanyStatutoryInfo>().ReverseMap();
            configuration.CreateMap<CompanyStatutoryInfoDto, CompanyStatutoryInfo>().ReverseMap();
            configuration.CreateMap<CreateOrEditBankBranchesDto, BankBranches>().ReverseMap();
            configuration.CreateMap<BankBranchesDto, BankBranches>().ReverseMap();
            configuration.CreateMap<CreateOrEditHRSettingsDto, HRSettings>().ReverseMap();
            configuration.CreateMap<HRSettingsDto, HRSettings>().ReverseMap();
            configuration.CreateMap<CreateOrEditPayCodesDto, PayCodes>().ReverseMap();
            configuration.CreateMap<PayCodesDto, PayCodes>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeePayrollDto, EmployeePayroll>().ReverseMap();
            configuration.CreateMap<EmployeePayrollDto, EmployeePayroll>().ReverseMap();
            configuration.CreateMap<CreateOrEditEarningTypesDto, EarningTypes>().ReverseMap();
            configuration.CreateMap<EarningTypesDto, EarningTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeeLoansDto, EmployeeLoans>().ReverseMap();
            configuration.CreateMap<EmployeeLoansDto, EmployeeLoans>().ReverseMap();
            configuration.CreateMap<CreateOrEditLoanPostedDto, LoanPosted>().ReverseMap();
            configuration.CreateMap<LoanPostedDto, LoanPosted>().ReverseMap();
            configuration.CreateMap<CreateOrEditLoansDto, Loans>().ReverseMap();
            configuration.CreateMap<LoansDto, Loans>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeesDto, Employees>().ReverseMap();
            configuration.CreateMap<EmployeesDto, Employees>().ReverseMap();
            configuration.CreateMap<CreateOrEditLoanTypesDto, LoanTypes>().ReverseMap();
            configuration.CreateMap<LoanTypesDto, LoanTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditDeductionTypesDto, DeductionTypes>().ReverseMap();
            configuration.CreateMap<DeductionTypesDto, DeductionTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveTypesDto, LeaveTypes>().ReverseMap();
            configuration.CreateMap<LeaveTypesDto, LeaveTypes>().ReverseMap();
            //Persons
            configuration.CreateMap<Person, PersonListDto>();
            configuration.CreateMap<CreatePersonInput, Person>();

            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicProperty, DynamicPropertyDto>().ReverseMap();
            configuration.CreateMap<DynamicPropertyValue, DynamicPropertyValueDto>().ReverseMap();
            configuration.CreateMap<DynamicEntityProperty, DynamicEntityPropertyDto>()
                .ForMember(dto => dto.DynamicPropertyName,
                    options => options.MapFrom(entity => entity.DynamicProperty.DisplayName ?? entity.DynamicProperty.PropertyName));
            configuration.CreateMap<DynamicEntityPropertyDto, DynamicEntityProperty>();

            configuration.CreateMap<DynamicEntityPropertyValue, DynamicEntityPropertyValueDto>().ReverseMap();

            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
        }
    }
}