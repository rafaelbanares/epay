﻿using System.Collections.Generic;
using Abp;
using MMB.ePay.Chat.Dto;
using MMB.ePay.Dto;

namespace MMB.ePay.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(UserIdentifier user, List<ChatMessageExportDto> messages);
    }
}
