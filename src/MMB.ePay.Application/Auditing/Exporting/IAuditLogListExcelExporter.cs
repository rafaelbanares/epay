﻿using System.Collections.Generic;
using MMB.ePay.Auditing.Dto;
using MMB.ePay.Dto;

namespace MMB.ePay.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
