﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using MMB.ePay.MultiTenancy.Accounting.Dto;

namespace MMB.ePay.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
