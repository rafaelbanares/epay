﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMB.ePay.MultiTenancy.HostDashboard.Dto;

namespace MMB.ePay.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}