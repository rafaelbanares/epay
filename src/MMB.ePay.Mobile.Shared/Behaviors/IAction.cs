﻿using Xamarin.Forms.Internals;

namespace MMB.ePay.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}