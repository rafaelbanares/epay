﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MMB.ePay.Sessions.Dto;

namespace MMB.ePay.Models.Common
{
    [AutoMapFrom(typeof(TenantLoginInfoDto)),
     AutoMapTo(typeof(TenantLoginInfoDto))]
    public class TenantLoginInfoPersistanceModel : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}