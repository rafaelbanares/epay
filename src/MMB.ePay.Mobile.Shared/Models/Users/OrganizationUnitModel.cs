﻿using Abp.AutoMapper;
using MMB.ePay.Organizations.Dto;

namespace MMB.ePay.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}