﻿namespace MMB.ePay.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_Holidays = "Pages.Holidays";
        public const string Pages_Holidays_Create = "Pages.Holidays.Create";
        public const string Pages_Holidays_Edit = "Pages.Holidays.Edit";
        public const string Pages_Holidays_Delete = "Pages.Holidays.Delete";

        public const string Pages_Reports = "Pages.Reports";

        public const string Pages_PayrollTransactions = "Pages.PayrollTransactions";
        public const string Pages_OvertimeFormats = "Pages.OvertimeFormats";

        public const string Pages_TimesheetTypes = "Pages.TimesheetTypes";
        public const string Pages_TimesheetTypes_Create = "Pages.TimesheetTypes.Create";
        public const string Pages_TimesheetTypes_Edit = "Pages.TimesheetTypes.Edit";
        public const string Pages_TimesheetTypes_Delete = "Pages.TimesheetTypes.Delete";

        public const string Pages_EmploymentTypes = "Pages.EmploymentTypes";
        public const string Pages_EmploymentTypes_Create = "Pages.EmploymentTypes.Create";
        public const string Pages_EmploymentTypes_Edit = "Pages.EmploymentTypes.Edit";
        public const string Pages_EmploymentTypes_Delete = "Pages.EmploymentTypes.Delete";

        public const string Pages_Banks = "Pages.Banks";
        public const string Pages_Banks_Create = "Pages.Banks.Create";
        public const string Pages_Banks_Edit = "Pages.Banks.Edit";
        public const string Pages_Banks_Delete = "Pages.Banks.Delete";

        public const string Pages_StatutoryTypes = "Pages.StatutoryTypes";
        public const string Pages_StatutoryTypes_Create = "Pages.StatutoryTypes.Create";
        public const string Pages_StatutoryTypes_Edit = "Pages.StatutoryTypes.Edit";
        public const string Pages_StatutoryTypes_Delete = "Pages.StatutoryTypes.Delete";

        public const string Pages_StatutorySubTypes = "Pages.StatutorySubTypes";
        public const string Pages_StatutorySubTypes_Create = "Pages.StatutorySubTypes.Create";
        public const string Pages_StatutorySubTypes_Edit = "Pages.StatutorySubTypes.Edit";
        public const string Pages_StatutorySubTypes_Delete = "Pages.StatutorySubTypes.Delete";

        public const string Pages_ProcessStatus = "Pages.ProcessStatus";
        public const string Pages_ProcessStatus_Create = "Pages.ProcessStatus.Create";
        public const string Pages_ProcessStatus_Edit = "Pages.ProcessStatus.Edit";
        public const string Pages_ProcessStatus_Delete = "Pages.ProcessStatus.Delete";

        public const string Pages_GroupSettings = "Pages.GroupSettings";
        public const string Pages_GroupSettings_Create = "Pages.GroupSettings.Create";
        public const string Pages_GroupSettings_Edit = "Pages.GroupSettings.Edit";
        public const string Pages_GroupSettings_Delete = "Pages.GroupSettings.Delete";

        public const string Pages_OvertimeTypes = "Pages.OvertimeTypes";
        public const string Pages_OvertimeTypes_Create = "Pages.OvertimeTypes.Create";
        public const string Pages_OvertimeTypes_Edit = "Pages.OvertimeTypes.Edit";
        public const string Pages_OvertimeTypes_Delete = "Pages.OvertimeTypes.Delete";

        public const string Pages_Ranks = "Pages.Ranks";
        public const string Pages_Ranks_Create = "Pages.Ranks.Create";
        public const string Pages_Ranks_Edit = "Pages.Ranks.Edit";
        public const string Pages_Ranks_Delete = "Pages.Ranks.Delete";

        public const string Pages_TimesheetTmp = "Pages.TimesheetTmp";
        public const string Pages_TimesheetTmp_Create = "Pages.TimesheetTmp.Create";
        public const string Pages_TimesheetTmp_Edit = "Pages.TimesheetTmp.Edit";
        public const string Pages_TimesheetTmp_Delete = "Pages.TimesheetTmp.Delete";

        public const string Pages_Timesheets = "Pages.Timesheets";
        public const string Pages_Timesheets_Create = "Pages.Timesheets.Create";
        public const string Pages_Timesheets_Edit = "Pages.Timesheets.Edit";
        public const string Pages_Timesheets_Delete = "Pages.Timesheets.Delete";

        public const string Pages_TimesheetProcess = "Pages.TimesheetProcess";
        public const string Pages_TimesheetProcess_Create = "Pages.TimesheetProcess.Create";
        public const string Pages_TimesheetProcess_Edit = "Pages.TimesheetProcess.Edit";
        public const string Pages_TimesheetProcess_Delete = "Pages.TimesheetProcess.Delete";

        public const string Pages_TimesheetPosted = "Pages.TimesheetPosted";
        public const string Pages_TimesheetPosted_Create = "Pages.TimesheetPosted.Create";
        public const string Pages_TimesheetPosted_Edit = "Pages.TimesheetPosted.Edit";
        public const string Pages_TimesheetPosted_Delete = "Pages.TimesheetPosted.Delete";

        public const string Pages_TaxTables = "Pages.TaxTables";
        public const string Pages_TaxTables_Create = "Pages.TaxTables.Create";
        public const string Pages_TaxTables_Edit = "Pages.TaxTables.Edit";
        public const string Pages_TaxTables_Delete = "Pages.TaxTables.Delete";

        public const string Pages_StatutoryTmp = "Pages.StatutoryTmp";
        public const string Pages_StatutoryTmp_Create = "Pages.StatutoryTmp.Create";
        public const string Pages_StatutoryTmp_Edit = "Pages.StatutoryTmp.Edit";
        public const string Pages_StatutoryTmp_Delete = "Pages.StatutoryTmp.Delete";

        public const string Pages_StatutoryProcess = "Pages.StatutoryProcess";
        public const string Pages_StatutoryProcess_Create = "Pages.StatutoryProcess.Create";
        public const string Pages_StatutoryProcess_Edit = "Pages.StatutoryProcess.Edit";
        public const string Pages_StatutoryProcess_Delete = "Pages.StatutoryProcess.Delete";

        public const string Pages_StatutoryPosted = "Pages.StatutoryPosted";
        public const string Pages_StatutoryPosted_Create = "Pages.StatutoryPosted.Create";
        public const string Pages_StatutoryPosted_Edit = "Pages.StatutoryPosted.Edit";
        public const string Pages_StatutoryPosted_Delete = "Pages.StatutoryPosted.Delete";

        public const string Pages_SssTables = "Pages.SssTables";
        public const string Pages_SssTables_Create = "Pages.SssTables.Create";
        public const string Pages_SssTables_Edit = "Pages.SssTables.Edit";
        public const string Pages_SssTables_Delete = "Pages.SssTables.Delete";

        public const string Pages_Reviewers = "Pages.Reviewers";
        public const string Pages_Reviewers_Create = "Pages.Reviewers.Create";
        public const string Pages_Reviewers_Edit = "Pages.Reviewers.Edit";
        public const string Pages_Reviewers_Delete = "Pages.Reviewers.Delete";

        public const string Pages_RecurEarnings = "Pages.RecurEarnings";
        public const string Pages_RecurEarnings_Create = "Pages.RecurEarnings.Create";
        public const string Pages_RecurEarnings_Edit = "Pages.RecurEarnings.Edit";
        public const string Pages_RecurEarnings_Delete = "Pages.RecurEarnings.Delete";

        public const string Pages_RecurDeductions = "Pages.RecurDeductions";
        public const string Pages_RecurDeductions_Create = "Pages.RecurDeductions.Create";
        public const string Pages_RecurDeductions_Edit = "Pages.RecurDeductions.Edit";
        public const string Pages_RecurDeductions_Delete = "Pages.RecurDeductions.Delete";

        public const string Pages_ProcessHistory = "Pages.ProcessHistory";
        public const string Pages_ProcessHistory_Create = "Pages.ProcessHistory.Create";
        public const string Pages_ProcessHistory_Edit = "Pages.ProcessHistory.Edit";
        public const string Pages_ProcessHistory_Delete = "Pages.ProcessHistory.Delete";

        public const string Pages_Process = "Pages.Process";
        public const string Pages_Process_Create = "Pages.Process.Create";
        public const string Pages_Process_Edit = "Pages.Process.Edit";
        public const string Pages_Process_Delete = "Pages.Process.Delete";

        public const string Pages_PaySettings = "Pages.PaySettings";
        public const string Pages_PaySettings_Create = "Pages.PaySettings.Create";
        public const string Pages_PaySettings_Edit = "Pages.PaySettings.Edit";
        public const string Pages_PaySettings_Delete = "Pages.PaySettings.Delete";

        public const string Pages_PayrollTmp = "Pages.PayrollTmp";
        public const string Pages_PayrollTmp_Create = "Pages.PayrollTmp.Create";
        public const string Pages_PayrollTmp_Edit = "Pages.PayrollTmp.Edit";
        public const string Pages_PayrollTmp_Delete = "Pages.PayrollTmp.Delete";

        public const string Pages_PayrollProcess = "Pages.PayrollProcess";
        public const string Pages_PayrollProcess_Create = "Pages.PayrollProcess.Create";
        public const string Pages_PayrollProcess_Edit = "Pages.PayrollProcess.Edit";
        public const string Pages_PayrollProcess_Delete = "Pages.PayrollProcess.Delete";

        public const string Pages_PayrollPosted = "Pages.PayrollPosted";
        public const string Pages_PayrollPosted_Create = "Pages.PayrollPosted.Create";
        public const string Pages_PayrollPosted_Edit = "Pages.PayrollPosted.Edit";
        public const string Pages_PayrollPosted_Delete = "Pages.PayrollPosted.Delete";

        public const string Pages_PayPeriod = "Pages.PayPeriod";
        public const string Pages_PayPeriod_Create = "Pages.PayPeriod.Create";
        public const string Pages_PayPeriod_Edit = "Pages.PayPeriod.Edit";
        public const string Pages_PayPeriod_Delete = "Pages.PayPeriod.Delete";

        public const string Pages_ProratedEarnings = "Pages.ProratedEarnings";
        public const string Pages_ProratedEarnings_Create = "Pages.ProratedEarnings.Create";
        public const string Pages_ProratedEarnings_Edit = "Pages.ProratedEarnings.Edit";
        public const string Pages_ProratedEarnings_Delete = "Pages.ProratedEarnings.Delete";

        public const string Pages_OvertimeSubTypes = "Pages.OvertimeSubTypes";
        public const string Pages_OvertimeSubTypes_Create = "Pages.OvertimeSubTypes.Create";
        public const string Pages_OvertimeSubTypes_Edit = "Pages.OvertimeSubTypes.Edit";
        public const string Pages_OvertimeSubTypes_Delete = "Pages.OvertimeSubTypes.Delete";

        public const string Pages_OvertimeMainTypes = "Pages.OvertimeMainTypes";
        public const string Pages_OvertimeMainTypes_Create = "Pages.OvertimeMainTypes.Create";
        public const string Pages_OvertimeMainTypes_Edit = "Pages.OvertimeMainTypes.Edit";
        public const string Pages_OvertimeMainTypes_Delete = "Pages.OvertimeMainTypes.Delete";

        public const string Pages_OTSheetTmp = "Pages.OTSheetTmp";
        public const string Pages_OTSheetTmp_Create = "Pages.OTSheetTmp.Create";
        public const string Pages_OTSheetTmp_Edit = "Pages.OTSheetTmp.Edit";
        public const string Pages_OTSheetTmp_Delete = "Pages.OTSheetTmp.Delete";

        public const string Pages_OTSheetProcess = "Pages.OTSheetProcess";
        public const string Pages_OTSheetProcess_Create = "Pages.OTSheetProcess.Create";
        public const string Pages_OTSheetProcess_Edit = "Pages.OTSheetProcess.Edit";
        public const string Pages_OTSheetProcess_Delete = "Pages.OTSheetProcess.Delete";

        public const string Pages_OTSheetPosted = "Pages.OTSheetPosted";
        public const string Pages_OTSheetPosted_Create = "Pages.OTSheetPosted.Create";
        public const string Pages_OTSheetPosted_Edit = "Pages.OTSheetPosted.Edit";
        public const string Pages_OTSheetPosted_Delete = "Pages.OTSheetPosted.Delete";

        public const string Pages_OTSheet = "Pages.OTSheet";
        public const string Pages_OTSheet_Create = "Pages.OTSheet.Create";
        public const string Pages_OTSheet_Edit = "Pages.OTSheet.Edit";
        public const string Pages_OTSheet_Delete = "Pages.OTSheet.Delete";

        public const string Pages_LoanTmp = "Pages.LoanTmp";
        public const string Pages_LoanTmp_Create = "Pages.LoanTmp.Create";
        public const string Pages_LoanTmp_Edit = "Pages.LoanTmp.Edit";
        public const string Pages_LoanTmp_Delete = "Pages.LoanTmp.Delete";

        public const string Pages_LoanProcess = "Pages.LoanProcess";
        public const string Pages_LoanProcess_Create = "Pages.LoanProcess.Create";
        public const string Pages_LoanProcess_Edit = "Pages.LoanProcess.Edit";
        public const string Pages_LoanProcess_Delete = "Pages.LoanProcess.Delete";

        public const string Pages_LeaveCredits = "Pages.LeaveCredits";
        public const string Pages_LeaveCredits_Create = "Pages.LeaveCredits.Create";
        public const string Pages_LeaveCredits_Edit = "Pages.LeaveCredits.Edit";
        public const string Pages_LeaveCredits_Delete = "Pages.LeaveCredits.Delete";

        public const string Pages_Employers = "Pages.Employers";
        public const string Pages_Employers_Create = "Pages.Employers.Create";
        public const string Pages_Employers_Edit = "Pages.Employers.Edit";
        public const string Pages_Employers_Delete = "Pages.Employers.Delete";

        public const string Pages_Dependents = "Pages.Dependents";
        public const string Pages_Dependents_Create = "Pages.Dependents.Create";
        public const string Pages_Dependents_Edit = "Pages.Dependents.Edit";
        public const string Pages_Dependents_Delete = "Pages.Dependents.Delete";

        public const string Pages_EarningTypeSettings = "Pages.EarningTypeSettings";
        public const string Pages_EarningTypeSettings_Create = "Pages.EarningTypeSettings.Create";
        public const string Pages_EarningTypeSettings_Edit = "Pages.EarningTypeSettings.Edit";
        public const string Pages_EarningTypeSettings_Delete = "Pages.EarningTypeSettings.Delete";

        public const string Pages_EarningTmp = "Pages.EarningTmp";
        public const string Pages_EarningTmp_Create = "Pages.EarningTmp.Create";
        public const string Pages_EarningTmp_Edit = "Pages.EarningTmp.Edit";
        public const string Pages_EarningTmp_Delete = "Pages.EarningTmp.Delete";

        public const string Pages_Earnings = "Pages.Earnings";
        public const string Pages_Earnings_Create = "Pages.Earnings.Create";
        public const string Pages_Earnings_Edit = "Pages.Earnings.Edit";
        public const string Pages_Earnings_Delete = "Pages.Earnings.Delete";

        public const string Pages_EarningProcess = "Pages.EarningProcess";
        public const string Pages_EarningProcess_Create = "Pages.EarningProcess.Create";
        public const string Pages_EarningProcess_Edit = "Pages.EarningProcess.Edit";
        public const string Pages_EarningProcess_Delete = "Pages.EarningProcess.Delete";

        public const string Pages_EarningPosted = "Pages.EarningPosted";
        public const string Pages_EarningPosted_Create = "Pages.EarningPosted.Create";
        public const string Pages_EarningPosted_Edit = "Pages.EarningPosted.Edit";
        public const string Pages_EarningPosted_Delete = "Pages.EarningPosted.Delete";

        public const string Pages_DeductionTmp = "Pages.DeductionTmp";
        public const string Pages_DeductionTmp_Create = "Pages.DeductionTmp.Create";
        public const string Pages_DeductionTmp_Edit = "Pages.DeductionTmp.Edit";
        public const string Pages_DeductionTmp_Delete = "Pages.DeductionTmp.Delete";

        public const string Pages_Deductions = "Pages.Deductions";
        public const string Pages_Deductions_Create = "Pages.Deductions.Create";
        public const string Pages_Deductions_Edit = "Pages.Deductions.Edit";
        public const string Pages_Deductions_Delete = "Pages.Deductions.Delete";

        public const string Pages_DeductionProcess = "Pages.DeductionProcess";
        public const string Pages_DeductionProcess_Create = "Pages.DeductionProcess.Create";
        public const string Pages_DeductionProcess_Edit = "Pages.DeductionProcess.Edit";
        public const string Pages_DeductionProcess_Delete = "Pages.DeductionProcess.Delete";

        public const string Pages_DeductionPosted = "Pages.DeductionPosted";
        public const string Pages_DeductionPosted_Create = "Pages.DeductionPosted.Create";
        public const string Pages_DeductionPosted_Edit = "Pages.DeductionPosted.Edit";
        public const string Pages_DeductionPosted_Delete = "Pages.DeductionPosted.Delete";

        public const string Pages_TenantBanks = "Pages.TenantBanks";
        public const string Pages_TenantBanks_Create = "Pages.TenantBanks.Create";
        public const string Pages_TenantBanks_Edit = "Pages.TenantBanks.Edit";
        public const string Pages_TenantBanks_Delete = "Pages.TenantBanks.Delete";

        public const string Pages_ContactPersons = "Pages.ContactPersons";
        public const string Pages_ContactPersons_Create = "Pages.ContactPersons.Create";
        public const string Pages_ContactPersons_Edit = "Pages.ContactPersons.Edit";
        public const string Pages_ContactPersons_Delete = "Pages.ContactPersons.Delete";

        public const string Pages_CompanyStatutoryInfo = "Pages.CompanyStatutoryInfo";
        public const string Pages_CompanyStatutoryInfo_Create = "Pages.CompanyStatutoryInfo.Create";
        public const string Pages_CompanyStatutoryInfo_Edit = "Pages.CompanyStatutoryInfo.Edit";
        public const string Pages_CompanyStatutoryInfo_Delete = "Pages.CompanyStatutoryInfo.Delete";

        public const string Pages_BankBranches = "Pages.BankBranches";
        public const string Pages_BankBranches_Create = "Pages.BankBranches.Create";
        public const string Pages_BankBranches_Edit = "Pages.BankBranches.Edit";
        public const string Pages_BankBranches_Delete = "Pages.BankBranches.Delete";

        public const string Pages_HRSettings = "Pages.HRSettings";
        public const string Pages_HRSettings_Create = "Pages.HRSettings.Create";
        public const string Pages_HRSettings_Edit = "Pages.HRSettings.Edit";
        public const string Pages_HRSettings_Delete = "Pages.HRSettings.Delete";

        public const string Pages_PayCodes = "Pages.PayCodes";
        public const string Pages_PayCodes_Create = "Pages.PayCodes.Create";
        public const string Pages_PayCodes_Edit = "Pages.PayCodes.Edit";
        public const string Pages_PayCodes_Delete = "Pages.PayCodes.Delete";

        public const string Pages_EmployeePayroll = "Pages.EmployeePayroll";
        public const string Pages_EmployeePayroll_Create = "Pages.EmployeePayroll.Create";
        public const string Pages_EmployeePayroll_Edit = "Pages.EmployeePayroll.Edit";
        public const string Pages_EmployeePayroll_Delete = "Pages.EmployeePayroll.Delete";

        public const string Pages_EmployeeStatutoryInfo = "Pages.EmployeeStatutoryInfo";
        public const string Pages_EmployeeStatutoryInfo_Create = "Pages.EmployeeStatutoryInfo.Create";
        public const string Pages_EmployeeStatutoryInfo_Edit = "Pages.EmployeeStatutoryInfo.Edit";
        public const string Pages_EmployeeStatutoryInfo_Delete = "Pages.EmployeeStatutoryInfo.Delete";

        public const string Pages_EarningTypes = "Pages.EarningTypes";
        public const string Pages_EarningTypes_Create = "Pages.EarningTypes.Create";
        public const string Pages_EarningTypes_Edit = "Pages.EarningTypes.Edit";
        public const string Pages_EarningTypes_Delete = "Pages.EarningTypes.Delete";

        public const string Pages_EmployeeLoans = "Pages.EmployeeLoans";
        public const string Pages_EmployeeLoans_Create = "Pages.EmployeeLoans.Create";
        public const string Pages_EmployeeLoans_Edit = "Pages.EmployeeLoans.Edit";
        public const string Pages_EmployeeLoans_Delete = "Pages.EmployeeLoans.Delete";

        public const string Pages_LoanPosted = "Pages.LoanPosted";
        public const string Pages_LoanPosted_Create = "Pages.LoanPosted.Create";
        public const string Pages_LoanPosted_Edit = "Pages.LoanPosted.Edit";
        public const string Pages_LoanPosted_Delete = "Pages.LoanPosted.Delete";

        public const string Pages_Loans = "Pages.Loans";
        public const string Pages_Loans_Create = "Pages.Loans.Create";
        public const string Pages_Loans_Edit = "Pages.Loans.Edit";
        public const string Pages_Loans_Delete = "Pages.Loans.Delete";

        public const string Pages_Employees = "Pages.Employees";
        public const string Pages_Employees_Create = "Pages.Employees.Create";
        public const string Pages_Employees_Edit = "Pages.Employees.Edit";
        public const string Pages_Employees_Delete = "Pages.Employees.Delete";

        public const string Pages_Companies = "Pages.Companies";
        public const string Pages_Companies_Create = "Pages.Companies.Create";
        public const string Pages_Companies_Edit = "Pages.Companies.Edit";
        public const string Pages_Companies_Delete = "Pages.Companies.Delete";

        public const string Pages_LoanTypes = "Pages.LoanTypes";
        public const string Pages_LoanTypes_Create = "Pages.LoanTypes.Create";
        public const string Pages_LoanTypes_Edit = "Pages.LoanTypes.Edit";
        public const string Pages_LoanTypes_Delete = "Pages.LoanTypes.Delete";

        public const string Pages_DeductionTypes = "Pages.DeductionTypes";
        public const string Pages_DeductionTypes_Create = "Pages.DeductionTypes.Create";
        public const string Pages_DeductionTypes_Edit = "Pages.DeductionTypes.Edit";
        public const string Pages_DeductionTypes_Delete = "Pages.DeductionTypes.Delete";

        public const string Pages_LeaveTypes = "Pages.LeaveTypes";
        public const string Pages_LeaveTypes_Create = "Pages.LeaveTypes.Create";
        public const string Pages_LeaveTypes_Edit = "Pages.LeaveTypes.Edit";
        public const string Pages_LeaveTypes_Delete = "Pages.LeaveTypes.Delete";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Companies = "Pages.Administration.Companies";
        public const string Pages_Administration_Companies_Create = "Pages.Administration.Companies.Create";
        public const string Pages_Administration_Companies_Edit = "Pages.Administration.Companies.Edit";
        public const string Pages_Administration_Companies_Delete = "Pages.Administration.Companies.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicProperties = "Pages.Administration.DynamicProperties";
        public const string Pages_Administration_DynamicProperties_Create = "Pages.Administration.DynamicProperties.Create";
        public const string Pages_Administration_DynamicProperties_Edit = "Pages.Administration.DynamicProperties.Edit";
        public const string Pages_Administration_DynamicProperties_Delete = "Pages.Administration.DynamicProperties.Delete";

        public const string Pages_Administration_DynamicPropertyValue = "Pages.Administration.DynamicPropertyValue";
        public const string Pages_Administration_DynamicPropertyValue_Create = "Pages.Administration.DynamicPropertyValue.Create";
        public const string Pages_Administration_DynamicPropertyValue_Edit = "Pages.Administration.DynamicPropertyValue.Edit";
        public const string Pages_Administration_DynamicPropertyValue_Delete = "Pages.Administration.DynamicPropertyValue.Delete";

        public const string Pages_Administration_DynamicEntityProperties = "Pages.Administration.DynamicEntityProperties";
        public const string Pages_Administration_DynamicEntityProperties_Create = "Pages.Administration.DynamicEntityProperties.Create";
        public const string Pages_Administration_DynamicEntityProperties_Edit = "Pages.Administration.DynamicEntityProperties.Edit";
        public const string Pages_Administration_DynamicEntityProperties_Delete = "Pages.Administration.DynamicEntityProperties.Delete";

        public const string Pages_Administration_DynamicEntityPropertyValue = "Pages.Administration.DynamicEntityPropertyValue";
        public const string Pages_Administration_DynamicEntityPropertyValue_Create = "Pages.Administration.DynamicEntityPropertyValue.Create";
        public const string Pages_Administration_DynamicEntityPropertyValue_Edit = "Pages.Administration.DynamicEntityPropertyValue.Edit";
        public const string Pages_Administration_DynamicEntityPropertyValue_Delete = "Pages.Administration.DynamicEntityPropertyValue.Delete";

        //TEST-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_ePay = "Pages.Tenant.ePay";
        public const string Pages_Tenant_ePay_CreatePerson = "Pages.Tenant.ePay.CreatePerson";
        public const string Pages_Tenant_ePay_DeletePerson = "Pages.Tenant.ePay.DeletePerson";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    }
}