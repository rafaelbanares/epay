﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace MMB.ePay.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            pages.CreateChildPermission(AppPermissions.Pages_Reports, L("Reports"));

            var holidays = pages.CreateChildPermission(AppPermissions.Pages_Holidays, L("Holidays"));
            holidays.CreateChildPermission(AppPermissions.Pages_Holidays_Create, L("CreateNewHolidays"));
            holidays.CreateChildPermission(AppPermissions.Pages_Holidays_Edit, L("EditHolidays"));
            holidays.CreateChildPermission(AppPermissions.Pages_Holidays_Delete, L("DeleteHolidays"));

            var overtimeFormats = pages.CreateChildPermission(AppPermissions.Pages_OvertimeFormats, L("OvertimeFormats"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes, L("OvertimeSubTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes_Create, L("CreateNewOvertimeSubTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes_Edit, L("EditOvertimeSubTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes_Delete, L("DeleteOvertimeSubTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes, L("OvertimeMainTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes_Create, L("CreateNewOvertimeMainTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes_Edit, L("EditOvertimeMainTypes"));
            overtimeFormats.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes_Delete, L("DeleteOvertimeMainTypes"));

            var timesheetTypes = pages.CreateChildPermission(AppPermissions.Pages_TimesheetTypes, L("TimesheetTypes"));
            timesheetTypes.CreateChildPermission(AppPermissions.Pages_TimesheetTypes_Create, L("CreateNewTimesheetTypes"));
            timesheetTypes.CreateChildPermission(AppPermissions.Pages_TimesheetTypes_Edit, L("EditTimesheetTypes"));
            timesheetTypes.CreateChildPermission(AppPermissions.Pages_TimesheetTypes_Delete, L("DeleteTimesheetTypes"));

            var employmentTypes = pages.CreateChildPermission(AppPermissions.Pages_EmploymentTypes, L("EmploymentTypes"));
            employmentTypes.CreateChildPermission(AppPermissions.Pages_EmploymentTypes_Create, L("CreateNewEmploymentTypes"));
            employmentTypes.CreateChildPermission(AppPermissions.Pages_EmploymentTypes_Edit, L("EditEmploymentTypes"));
            employmentTypes.CreateChildPermission(AppPermissions.Pages_EmploymentTypes_Delete, L("DeleteEmploymentTypes"));

            var companies = pages.CreateChildPermission(AppPermissions.Pages_Companies, L("Companies"));
            companies.CreateChildPermission(AppPermissions.Pages_Companies_Create, L("CreateNewCompanies"));
            companies.CreateChildPermission(AppPermissions.Pages_Companies_Edit, L("EditCompanies"));
            companies.CreateChildPermission(AppPermissions.Pages_Companies_Delete, L("DeleteCompanies"));

            var banks = pages.CreateChildPermission(AppPermissions.Pages_Banks, L("Banks"));
            banks.CreateChildPermission(AppPermissions.Pages_Banks_Create, L("CreateNewBanks"));
            banks.CreateChildPermission(AppPermissions.Pages_Banks_Edit, L("EditBanks"));
            banks.CreateChildPermission(AppPermissions.Pages_Banks_Delete, L("DeleteBanks"));

            var statutoryTypes = pages.CreateChildPermission(AppPermissions.Pages_StatutoryTypes, L("StatutoryTypes"));
            statutoryTypes.CreateChildPermission(AppPermissions.Pages_StatutoryTypes_Create, L("CreateNewStatutoryTypes"));
            statutoryTypes.CreateChildPermission(AppPermissions.Pages_StatutoryTypes_Edit, L("EditStatutoryTypes"));
            statutoryTypes.CreateChildPermission(AppPermissions.Pages_StatutoryTypes_Delete, L("DeleteStatutoryTypes"));

            var processStatus = pages.CreateChildPermission(AppPermissions.Pages_ProcessStatus, L("ProcessStatus"));
            processStatus.CreateChildPermission(AppPermissions.Pages_ProcessStatus_Create, L("CreateNewProcessStatus"));
            processStatus.CreateChildPermission(AppPermissions.Pages_ProcessStatus_Edit, L("EditProcessStatus"));
            processStatus.CreateChildPermission(AppPermissions.Pages_ProcessStatus_Delete, L("DeleteProcessStatus"));

            var groupSettings = pages.CreateChildPermission(AppPermissions.Pages_GroupSettings, L("GroupSettings"));
            groupSettings.CreateChildPermission(AppPermissions.Pages_GroupSettings_Create, L("CreateNewGroupSettings"));
            groupSettings.CreateChildPermission(AppPermissions.Pages_GroupSettings_Edit, L("EditGroupSettings"));
            groupSettings.CreateChildPermission(AppPermissions.Pages_GroupSettings_Delete, L("DeleteGroupSettings"));

            var overtimeTypes = pages.CreateChildPermission(AppPermissions.Pages_OvertimeTypes, L("OvertimeTypes"));
            overtimeTypes.CreateChildPermission(AppPermissions.Pages_OvertimeTypes_Create, L("CreateNewOvertimeTypes"));
            overtimeTypes.CreateChildPermission(AppPermissions.Pages_OvertimeTypes_Edit, L("EditOvertimeTypes"));
            overtimeTypes.CreateChildPermission(AppPermissions.Pages_OvertimeTypes_Delete, L("DeleteOvertimeTypes"));

            var ranks = pages.CreateChildPermission(AppPermissions.Pages_Ranks, L("Ranks"));
            ranks.CreateChildPermission(AppPermissions.Pages_Ranks_Create, L("CreateNewRanks"));
            ranks.CreateChildPermission(AppPermissions.Pages_Ranks_Edit, L("EditRanks"));
            ranks.CreateChildPermission(AppPermissions.Pages_Ranks_Delete, L("DeleteRanks"));

            var timesheetTmp = pages.CreateChildPermission(AppPermissions.Pages_TimesheetTmp, L("TimesheetTmp"));
            timesheetTmp.CreateChildPermission(AppPermissions.Pages_TimesheetTmp_Create, L("CreateNewTimesheetTmp"));
            timesheetTmp.CreateChildPermission(AppPermissions.Pages_TimesheetTmp_Edit, L("EditTimesheetTmp"));
            timesheetTmp.CreateChildPermission(AppPermissions.Pages_TimesheetTmp_Delete, L("DeleteTimesheetTmp"));

            var timesheets = pages.CreateChildPermission(AppPermissions.Pages_Timesheets, L("Timesheets"));
            timesheets.CreateChildPermission(AppPermissions.Pages_Timesheets_Create, L("CreateNewTimesheets"));
            timesheets.CreateChildPermission(AppPermissions.Pages_Timesheets_Edit, L("EditTimesheets"));
            timesheets.CreateChildPermission(AppPermissions.Pages_Timesheets_Delete, L("DeleteTimesheets"));

            var timesheetProcess = pages.CreateChildPermission(AppPermissions.Pages_TimesheetProcess, L("TimesheetProcess"));
            timesheetProcess.CreateChildPermission(AppPermissions.Pages_TimesheetProcess_Create, L("CreateNewTimesheetProcess"));
            timesheetProcess.CreateChildPermission(AppPermissions.Pages_TimesheetProcess_Edit, L("EditTimesheetProcess"));
            timesheetProcess.CreateChildPermission(AppPermissions.Pages_TimesheetProcess_Delete, L("DeleteTimesheetProcess"));

            var timesheetPosted = pages.CreateChildPermission(AppPermissions.Pages_TimesheetPosted, L("TimesheetPosted"));
            timesheetPosted.CreateChildPermission(AppPermissions.Pages_TimesheetPosted_Create, L("CreateNewTimesheetPosted"));
            timesheetPosted.CreateChildPermission(AppPermissions.Pages_TimesheetPosted_Edit, L("EditTimesheetPosted"));
            timesheetPosted.CreateChildPermission(AppPermissions.Pages_TimesheetPosted_Delete, L("DeleteTimesheetPosted"));

            var taxTables = pages.CreateChildPermission(AppPermissions.Pages_TaxTables, L("TaxTables"));
            taxTables.CreateChildPermission(AppPermissions.Pages_TaxTables_Create, L("CreateNewTaxTables"));
            taxTables.CreateChildPermission(AppPermissions.Pages_TaxTables_Edit, L("EditTaxTables"));
            taxTables.CreateChildPermission(AppPermissions.Pages_TaxTables_Delete, L("DeleteTaxTables"));

            var statutoryTmp = pages.CreateChildPermission(AppPermissions.Pages_StatutoryTmp, L("StatutoryTmp"));
            statutoryTmp.CreateChildPermission(AppPermissions.Pages_StatutoryTmp_Create, L("CreateNewStatutoryTmp"));
            statutoryTmp.CreateChildPermission(AppPermissions.Pages_StatutoryTmp_Edit, L("EditStatutoryTmp"));
            statutoryTmp.CreateChildPermission(AppPermissions.Pages_StatutoryTmp_Delete, L("DeleteStatutoryTmp"));

            var statutoryProcess = pages.CreateChildPermission(AppPermissions.Pages_StatutoryProcess, L("StatutoryProcess"));
            statutoryProcess.CreateChildPermission(AppPermissions.Pages_StatutoryProcess_Create, L("CreateNewStatutoryProcess"));
            statutoryProcess.CreateChildPermission(AppPermissions.Pages_StatutoryProcess_Edit, L("EditStatutoryProcess"));
            statutoryProcess.CreateChildPermission(AppPermissions.Pages_StatutoryProcess_Delete, L("DeleteStatutoryProcess"));

            var statutoryPosted = pages.CreateChildPermission(AppPermissions.Pages_StatutoryPosted, L("StatutoryPosted"));
            statutoryPosted.CreateChildPermission(AppPermissions.Pages_StatutoryPosted_Create, L("CreateNewStatutoryPosted"));
            statutoryPosted.CreateChildPermission(AppPermissions.Pages_StatutoryPosted_Edit, L("EditStatutoryPosted"));
            statutoryPosted.CreateChildPermission(AppPermissions.Pages_StatutoryPosted_Delete, L("DeleteStatutoryPosted"));

            var sssTables = pages.CreateChildPermission(AppPermissions.Pages_SssTables, L("SssTables"));
            sssTables.CreateChildPermission(AppPermissions.Pages_SssTables_Create, L("CreateNewSssTables"));
            sssTables.CreateChildPermission(AppPermissions.Pages_SssTables_Edit, L("EditSssTables"));
            sssTables.CreateChildPermission(AppPermissions.Pages_SssTables_Delete, L("DeleteSssTables"));

            var reviewers = pages.CreateChildPermission(AppPermissions.Pages_Reviewers, L("Reviewers"));
            reviewers.CreateChildPermission(AppPermissions.Pages_Reviewers_Create, L("CreateNewReviewers"));
            reviewers.CreateChildPermission(AppPermissions.Pages_Reviewers_Edit, L("EditReviewers"));
            reviewers.CreateChildPermission(AppPermissions.Pages_Reviewers_Delete, L("DeleteReviewers"));

            var recurEarnings = pages.CreateChildPermission(AppPermissions.Pages_RecurEarnings, L("RecurEarnings"));
            recurEarnings.CreateChildPermission(AppPermissions.Pages_RecurEarnings_Create, L("CreateNewRecurEarnings"));
            recurEarnings.CreateChildPermission(AppPermissions.Pages_RecurEarnings_Edit, L("EditRecurEarnings"));
            recurEarnings.CreateChildPermission(AppPermissions.Pages_RecurEarnings_Delete, L("DeleteRecurEarnings"));

            var recurDeductions = pages.CreateChildPermission(AppPermissions.Pages_RecurDeductions, L("RecurDeductions"));
            recurDeductions.CreateChildPermission(AppPermissions.Pages_RecurDeductions_Create, L("CreateNewRecurDeductions"));
            recurDeductions.CreateChildPermission(AppPermissions.Pages_RecurDeductions_Edit, L("EditRecurDeductions"));
            recurDeductions.CreateChildPermission(AppPermissions.Pages_RecurDeductions_Delete, L("DeleteRecurDeductions"));

            var processHistory = pages.CreateChildPermission(AppPermissions.Pages_ProcessHistory, L("ProcessHistory"));
            processHistory.CreateChildPermission(AppPermissions.Pages_ProcessHistory_Create, L("CreateNewProcessHistory"));
            processHistory.CreateChildPermission(AppPermissions.Pages_ProcessHistory_Edit, L("EditProcessHistory"));
            processHistory.CreateChildPermission(AppPermissions.Pages_ProcessHistory_Delete, L("DeleteProcessHistory"));

            var process = pages.CreateChildPermission(AppPermissions.Pages_Process, L("Process"));
            process.CreateChildPermission(AppPermissions.Pages_Process_Create, L("CreateNewProcess"));
            process.CreateChildPermission(AppPermissions.Pages_Process_Edit, L("EditProcess"));
            process.CreateChildPermission(AppPermissions.Pages_Process_Delete, L("DeleteProcess"));

            var paySettings = pages.CreateChildPermission(AppPermissions.Pages_PaySettings, L("PaySettings"));
            paySettings.CreateChildPermission(AppPermissions.Pages_PaySettings_Create, L("CreateNewPaySettings"));
            paySettings.CreateChildPermission(AppPermissions.Pages_PaySettings_Edit, L("EditPaySettings"));
            paySettings.CreateChildPermission(AppPermissions.Pages_PaySettings_Delete, L("DeletePaySettings"));

            var payrollTmp = pages.CreateChildPermission(AppPermissions.Pages_PayrollTmp, L("PayrollTmp"));
            payrollTmp.CreateChildPermission(AppPermissions.Pages_PayrollTmp_Create, L("CreateNewPayrollTmp"));
            payrollTmp.CreateChildPermission(AppPermissions.Pages_PayrollTmp_Edit, L("EditPayrollTmp"));
            payrollTmp.CreateChildPermission(AppPermissions.Pages_PayrollTmp_Delete, L("DeletePayrollTmp"));

            var payrollProcess = pages.CreateChildPermission(AppPermissions.Pages_PayrollProcess, L("PayrollProcess"));
            payrollProcess.CreateChildPermission(AppPermissions.Pages_PayrollProcess_Create, L("CreateNewPayrollProcess"));
            payrollProcess.CreateChildPermission(AppPermissions.Pages_PayrollProcess_Edit, L("EditPayrollProcess"));
            payrollProcess.CreateChildPermission(AppPermissions.Pages_PayrollProcess_Delete, L("DeletePayrollProcess"));

            var payrollPosted = pages.CreateChildPermission(AppPermissions.Pages_PayrollPosted, L("PayrollPosted"));
            payrollPosted.CreateChildPermission(AppPermissions.Pages_PayrollPosted_Create, L("CreateNewPayrollPosted"));
            payrollPosted.CreateChildPermission(AppPermissions.Pages_PayrollPosted_Edit, L("EditPayrollPosted"));
            payrollPosted.CreateChildPermission(AppPermissions.Pages_PayrollPosted_Delete, L("DeletePayrollPosted"));

            var payPeriod = pages.CreateChildPermission(AppPermissions.Pages_PayPeriod, L("PayPeriod"));
            payPeriod.CreateChildPermission(AppPermissions.Pages_PayPeriod_Create, L("CreateNewPayPeriod"));
            payPeriod.CreateChildPermission(AppPermissions.Pages_PayPeriod_Edit, L("EditPayPeriod"));
            payPeriod.CreateChildPermission(AppPermissions.Pages_PayPeriod_Delete, L("DeletePayPeriod"));

            var proratedEarnings = pages.CreateChildPermission(AppPermissions.Pages_ProratedEarnings, L("ProratedEarnings"));
            proratedEarnings.CreateChildPermission(AppPermissions.Pages_ProratedEarnings_Create, L("CreateNewProratedEarnings"));
            proratedEarnings.CreateChildPermission(AppPermissions.Pages_ProratedEarnings_Edit, L("EditProratedEarnings"));
            proratedEarnings.CreateChildPermission(AppPermissions.Pages_ProratedEarnings_Delete, L("DeleteProratedEarnings"));

            //var overtimeSubTypes = pages.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes, L("OvertimeSubTypes"));
            //overtimeSubTypes.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes_Create, L("CreateNewOvertimeSubTypes"));
            //overtimeSubTypes.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes_Edit, L("EditOvertimeSubTypes"));
            //overtimeSubTypes.CreateChildPermission(AppPermissions.Pages_OvertimeSubTypes_Delete, L("DeleteOvertimeSubTypes"));

            //var overtimeMainTypes = pages.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes, L("OvertimeMainTypes"));
            //overtimeMainTypes.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes_Create, L("CreateNewOvertimeMainTypes"));
            //overtimeMainTypes.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes_Edit, L("EditOvertimeMainTypes"));
            //overtimeMainTypes.CreateChildPermission(AppPermissions.Pages_OvertimeMainTypes_Delete, L("DeleteOvertimeMainTypes"));

            var otSheetTmp = pages.CreateChildPermission(AppPermissions.Pages_OTSheetTmp, L("OTSheetTmp"));
            otSheetTmp.CreateChildPermission(AppPermissions.Pages_OTSheetTmp_Create, L("CreateNewOTSheetTmp"));
            otSheetTmp.CreateChildPermission(AppPermissions.Pages_OTSheetTmp_Edit, L("EditOTSheetTmp"));
            otSheetTmp.CreateChildPermission(AppPermissions.Pages_OTSheetTmp_Delete, L("DeleteOTSheetTmp"));

            var otSheetProcess = pages.CreateChildPermission(AppPermissions.Pages_OTSheetProcess, L("OTSheetProcess"));
            otSheetProcess.CreateChildPermission(AppPermissions.Pages_OTSheetProcess_Create, L("CreateNewOTSheetProcess"));
            otSheetProcess.CreateChildPermission(AppPermissions.Pages_OTSheetProcess_Edit, L("EditOTSheetProcess"));
            otSheetProcess.CreateChildPermission(AppPermissions.Pages_OTSheetProcess_Delete, L("DeleteOTSheetProcess"));

            var otSheetPosted = pages.CreateChildPermission(AppPermissions.Pages_OTSheetPosted, L("OTSheetPosted"));
            otSheetPosted.CreateChildPermission(AppPermissions.Pages_OTSheetPosted_Create, L("CreateNewOTSheetPosted"));
            otSheetPosted.CreateChildPermission(AppPermissions.Pages_OTSheetPosted_Edit, L("EditOTSheetPosted"));
            otSheetPosted.CreateChildPermission(AppPermissions.Pages_OTSheetPosted_Delete, L("DeleteOTSheetPosted"));

            var otSheet = pages.CreateChildPermission(AppPermissions.Pages_OTSheet, L("OTSheet"));
            otSheet.CreateChildPermission(AppPermissions.Pages_OTSheet_Create, L("CreateNewOTSheet"));
            otSheet.CreateChildPermission(AppPermissions.Pages_OTSheet_Edit, L("EditOTSheet"));
            otSheet.CreateChildPermission(AppPermissions.Pages_OTSheet_Delete, L("DeleteOTSheet"));

            var loanTmp = pages.CreateChildPermission(AppPermissions.Pages_LoanTmp, L("LoanTmp"));
            loanTmp.CreateChildPermission(AppPermissions.Pages_LoanTmp_Create, L("CreateNewLoanTmp"));
            loanTmp.CreateChildPermission(AppPermissions.Pages_LoanTmp_Edit, L("EditLoanTmp"));
            loanTmp.CreateChildPermission(AppPermissions.Pages_LoanTmp_Delete, L("DeleteLoanTmp"));

            var loanProcess = pages.CreateChildPermission(AppPermissions.Pages_LoanProcess, L("LoanProcess"));
            loanProcess.CreateChildPermission(AppPermissions.Pages_LoanProcess_Create, L("CreateNewLoanProcess"));
            loanProcess.CreateChildPermission(AppPermissions.Pages_LoanProcess_Edit, L("EditLoanProcess"));
            loanProcess.CreateChildPermission(AppPermissions.Pages_LoanProcess_Delete, L("DeleteLoanProcess"));

            var leaveCredits = pages.CreateChildPermission(AppPermissions.Pages_LeaveCredits, L("LeaveCredits"));
            leaveCredits.CreateChildPermission(AppPermissions.Pages_LeaveCredits_Create, L("CreateNewLeaveCredits"));
            leaveCredits.CreateChildPermission(AppPermissions.Pages_LeaveCredits_Edit, L("EditLeaveCredits"));
            leaveCredits.CreateChildPermission(AppPermissions.Pages_LeaveCredits_Delete, L("DeleteLeaveCredits"));

            var employers = pages.CreateChildPermission(AppPermissions.Pages_Employers, L("Employers"));
            employers.CreateChildPermission(AppPermissions.Pages_Employers_Create, L("CreateNewEmployers"));
            employers.CreateChildPermission(AppPermissions.Pages_Employers_Edit, L("EditEmployers"));
            employers.CreateChildPermission(AppPermissions.Pages_Employers_Delete, L("DeleteEmployers"));

            var dependents = pages.CreateChildPermission(AppPermissions.Pages_Dependents, L("Dependents"));
            dependents.CreateChildPermission(AppPermissions.Pages_Dependents_Create, L("CreateNewDependents"));
            dependents.CreateChildPermission(AppPermissions.Pages_Dependents_Edit, L("EditDependents"));
            dependents.CreateChildPermission(AppPermissions.Pages_Dependents_Delete, L("DeleteDependents"));

            var earningTypeSettings = pages.CreateChildPermission(AppPermissions.Pages_EarningTypeSettings, L("EarningTypeSettings"));
            earningTypeSettings.CreateChildPermission(AppPermissions.Pages_EarningTypeSettings_Create, L("CreateNewEarningTypeSettings"));
            earningTypeSettings.CreateChildPermission(AppPermissions.Pages_EarningTypeSettings_Edit, L("EditEarningTypeSettings"));
            earningTypeSettings.CreateChildPermission(AppPermissions.Pages_EarningTypeSettings_Delete, L("DeleteEarningTypeSettings"));

            var earningTmp = pages.CreateChildPermission(AppPermissions.Pages_EarningTmp, L("EarningTmp"));
            earningTmp.CreateChildPermission(AppPermissions.Pages_EarningTmp_Create, L("CreateNewEarningTmp"));
            earningTmp.CreateChildPermission(AppPermissions.Pages_EarningTmp_Edit, L("EditEarningTmp"));
            earningTmp.CreateChildPermission(AppPermissions.Pages_EarningTmp_Delete, L("DeleteEarningTmp"));

            var earnings = pages.CreateChildPermission(AppPermissions.Pages_Earnings, L("Earnings"));
            earnings.CreateChildPermission(AppPermissions.Pages_Earnings_Create, L("CreateNewEarnings"));
            earnings.CreateChildPermission(AppPermissions.Pages_Earnings_Edit, L("EditEarnings"));
            earnings.CreateChildPermission(AppPermissions.Pages_Earnings_Delete, L("DeleteEarnings"));

            var earningProcess = pages.CreateChildPermission(AppPermissions.Pages_EarningProcess, L("EarningProcess"));
            earningProcess.CreateChildPermission(AppPermissions.Pages_EarningProcess_Create, L("CreateNewEarningProcess"));
            earningProcess.CreateChildPermission(AppPermissions.Pages_EarningProcess_Edit, L("EditEarningProcess"));
            earningProcess.CreateChildPermission(AppPermissions.Pages_EarningProcess_Delete, L("DeleteEarningProcess"));

            var earningPosted = pages.CreateChildPermission(AppPermissions.Pages_EarningPosted, L("EarningPosted"));
            earningPosted.CreateChildPermission(AppPermissions.Pages_EarningPosted_Create, L("CreateNewEarningPosted"));
            earningPosted.CreateChildPermission(AppPermissions.Pages_EarningPosted_Edit, L("EditEarningPosted"));
            earningPosted.CreateChildPermission(AppPermissions.Pages_EarningPosted_Delete, L("DeleteEarningPosted"));

            var deductionTmp = pages.CreateChildPermission(AppPermissions.Pages_DeductionTmp, L("DeductionTmp"));
            deductionTmp.CreateChildPermission(AppPermissions.Pages_DeductionTmp_Create, L("CreateNewDeductionTmp"));
            deductionTmp.CreateChildPermission(AppPermissions.Pages_DeductionTmp_Edit, L("EditDeductionTmp"));
            deductionTmp.CreateChildPermission(AppPermissions.Pages_DeductionTmp_Delete, L("DeleteDeductionTmp"));

            var deductions = pages.CreateChildPermission(AppPermissions.Pages_Deductions, L("Deductions"));
            deductions.CreateChildPermission(AppPermissions.Pages_Deductions_Create, L("CreateNewDeductions"));
            deductions.CreateChildPermission(AppPermissions.Pages_Deductions_Edit, L("EditDeductions"));
            deductions.CreateChildPermission(AppPermissions.Pages_Deductions_Delete, L("DeleteDeductions"));

            var deductionProcess = pages.CreateChildPermission(AppPermissions.Pages_DeductionProcess, L("DeductionProcess"));
            deductionProcess.CreateChildPermission(AppPermissions.Pages_DeductionProcess_Create, L("CreateNewDeductionProcess"));
            deductionProcess.CreateChildPermission(AppPermissions.Pages_DeductionProcess_Edit, L("EditDeductionProcess"));
            deductionProcess.CreateChildPermission(AppPermissions.Pages_DeductionProcess_Delete, L("DeleteDeductionProcess"));

            var deductionPosted = pages.CreateChildPermission(AppPermissions.Pages_DeductionPosted, L("DeductionPosted"));
            deductionPosted.CreateChildPermission(AppPermissions.Pages_DeductionPosted_Create, L("CreateNewDeductionPosted"));
            deductionPosted.CreateChildPermission(AppPermissions.Pages_DeductionPosted_Edit, L("EditDeductionPosted"));
            deductionPosted.CreateChildPermission(AppPermissions.Pages_DeductionPosted_Delete, L("DeleteDeductionPosted"));

            var tenantBanks = pages.CreateChildPermission(AppPermissions.Pages_TenantBanks, L("TenantBanks"));
            tenantBanks.CreateChildPermission(AppPermissions.Pages_TenantBanks_Create, L("CreateNewTenantBanks"));
            tenantBanks.CreateChildPermission(AppPermissions.Pages_TenantBanks_Edit, L("EditTenantBanks"));
            tenantBanks.CreateChildPermission(AppPermissions.Pages_TenantBanks_Delete, L("DeleteTenantBanks"));

            var contactPersons = pages.CreateChildPermission(AppPermissions.Pages_ContactPersons, L("ContactPersons"));
            contactPersons.CreateChildPermission(AppPermissions.Pages_ContactPersons_Create, L("CreateNewContactPersons"));
            contactPersons.CreateChildPermission(AppPermissions.Pages_ContactPersons_Edit, L("EditContactPersons"));
            contactPersons.CreateChildPermission(AppPermissions.Pages_ContactPersons_Delete, L("DeleteContactPersons"));

            var companyStatutoryInfo = pages.CreateChildPermission(AppPermissions.Pages_CompanyStatutoryInfo, L("CompanyStatutoryInfo"));
            companyStatutoryInfo.CreateChildPermission(AppPermissions.Pages_CompanyStatutoryInfo_Create, L("CreateNewCompanyStatutoryInfo"));
            companyStatutoryInfo.CreateChildPermission(AppPermissions.Pages_CompanyStatutoryInfo_Edit, L("EditCompanyStatutoryInfo"));
            companyStatutoryInfo.CreateChildPermission(AppPermissions.Pages_CompanyStatutoryInfo_Delete, L("DeleteCompanyStatutoryInfo"));

            var bankBranches = pages.CreateChildPermission(AppPermissions.Pages_BankBranches, L("BankBranches"));
            bankBranches.CreateChildPermission(AppPermissions.Pages_BankBranches_Create, L("CreateNewBankBranches"));
            bankBranches.CreateChildPermission(AppPermissions.Pages_BankBranches_Edit, L("EditBankBranches"));
            bankBranches.CreateChildPermission(AppPermissions.Pages_BankBranches_Delete, L("DeleteBankBranches"));

            var hrSettings = pages.CreateChildPermission(AppPermissions.Pages_HRSettings, L("HRSettings"));
            hrSettings.CreateChildPermission(AppPermissions.Pages_HRSettings_Create, L("CreateNewHRSettings"));
            hrSettings.CreateChildPermission(AppPermissions.Pages_HRSettings_Edit, L("EditHRSettings"));
            hrSettings.CreateChildPermission(AppPermissions.Pages_HRSettings_Delete, L("DeleteHRSettings"));

            var payCodes = pages.CreateChildPermission(AppPermissions.Pages_PayCodes, L("PayCodes"));
            payCodes.CreateChildPermission(AppPermissions.Pages_PayCodes_Create, L("CreateNewPayCodes"));
            payCodes.CreateChildPermission(AppPermissions.Pages_PayCodes_Edit, L("EditPayCodes"));
            payCodes.CreateChildPermission(AppPermissions.Pages_PayCodes_Delete, L("DeletePayCodes"));

            var employeePayroll = pages.CreateChildPermission(AppPermissions.Pages_EmployeePayroll, L("EmployeePayroll"));
            employeePayroll.CreateChildPermission(AppPermissions.Pages_EmployeePayroll_Create, L("CreateNewEmployeePayroll"));
            employeePayroll.CreateChildPermission(AppPermissions.Pages_EmployeePayroll_Edit, L("EditEmployeePayroll"));
            employeePayroll.CreateChildPermission(AppPermissions.Pages_EmployeePayroll_Delete, L("DeleteEmployeePayroll"));

            var employeeStatutoryInfo = pages.CreateChildPermission(AppPermissions.Pages_EmployeeStatutoryInfo, L("EmployeeStatutoryInfo"));
            employeePayroll.CreateChildPermission(AppPermissions.Pages_EmployeeStatutoryInfo_Create, L("CreateNewEmployeeStatutoryInfo"));
            employeePayroll.CreateChildPermission(AppPermissions.Pages_EmployeeStatutoryInfo_Edit, L("EditEmployeeStatutoryInfo"));
            employeePayroll.CreateChildPermission(AppPermissions.Pages_EmployeeStatutoryInfo_Delete, L("DeleteEmployeeStatutoryInfo"));

            var earningTypes = pages.CreateChildPermission(AppPermissions.Pages_EarningTypes, L("EarningTypes"));
            earningTypes.CreateChildPermission(AppPermissions.Pages_EarningTypes_Create, L("CreateNewEarningTypes"));
            earningTypes.CreateChildPermission(AppPermissions.Pages_EarningTypes_Edit, L("EditEarningTypes"));
            earningTypes.CreateChildPermission(AppPermissions.Pages_EarningTypes_Delete, L("DeleteEarningTypes"));

            var employeeLoans = pages.CreateChildPermission(AppPermissions.Pages_EmployeeLoans, L("EmployeeLoans"));
            employeeLoans.CreateChildPermission(AppPermissions.Pages_EmployeeLoans_Create, L("CreateNewEmployeeLoans"));
            employeeLoans.CreateChildPermission(AppPermissions.Pages_EmployeeLoans_Edit, L("EditEmployeeLoans"));
            employeeLoans.CreateChildPermission(AppPermissions.Pages_EmployeeLoans_Delete, L("DeleteEmployeeLoans"));

            var loanPosted = pages.CreateChildPermission(AppPermissions.Pages_LoanPosted, L("LoanPosted"));
            loanPosted.CreateChildPermission(AppPermissions.Pages_LoanPosted_Create, L("CreateNewLoanPosted"));
            loanPosted.CreateChildPermission(AppPermissions.Pages_LoanPosted_Edit, L("EditLoanPosted"));
            loanPosted.CreateChildPermission(AppPermissions.Pages_LoanPosted_Delete, L("DeleteLoanPosted"));

            var loans = pages.CreateChildPermission(AppPermissions.Pages_Loans, L("Loans"));
            loans.CreateChildPermission(AppPermissions.Pages_Loans_Create, L("CreateNewLoans"));
            loans.CreateChildPermission(AppPermissions.Pages_Loans_Edit, L("EditLoans"));
            loans.CreateChildPermission(AppPermissions.Pages_Loans_Delete, L("DeleteLoans"));

            var employees = pages.CreateChildPermission(AppPermissions.Pages_Employees, L("Employees"));
            employees.CreateChildPermission(AppPermissions.Pages_Employees_Create, L("CreateNewEmployees"));
            employees.CreateChildPermission(AppPermissions.Pages_Employees_Edit, L("EditEmployees"));
            employees.CreateChildPermission(AppPermissions.Pages_Employees_Delete, L("DeleteEmployees"));

            var loanTypes = pages.CreateChildPermission(AppPermissions.Pages_LoanTypes, L("LoanTypes"));
            loanTypes.CreateChildPermission(AppPermissions.Pages_LoanTypes_Create, L("CreateNewLoanTypes"));
            loanTypes.CreateChildPermission(AppPermissions.Pages_LoanTypes_Edit, L("EditLoanTypes"));
            loanTypes.CreateChildPermission(AppPermissions.Pages_LoanTypes_Delete, L("DeleteLoanTypes"));

            var deductionTypes = pages.CreateChildPermission(AppPermissions.Pages_DeductionTypes, L("DeductionTypes"));
            deductionTypes.CreateChildPermission(AppPermissions.Pages_DeductionTypes_Create, L("CreateNewDeductionTypes"));
            deductionTypes.CreateChildPermission(AppPermissions.Pages_DeductionTypes_Edit, L("EditDeductionTypes"));
            deductionTypes.CreateChildPermission(AppPermissions.Pages_DeductionTypes_Delete, L("DeleteDeductionTypes"));

            var leaveTypes = pages.CreateChildPermission(AppPermissions.Pages_LeaveTypes, L("LeaveTypes"));
            leaveTypes.CreateChildPermission(AppPermissions.Pages_LeaveTypes_Create, L("CreateNewLeaveTypes"));
            leaveTypes.CreateChildPermission(AppPermissions.Pages_LeaveTypes_Edit, L("EditLeaveTypes"));
            leaveTypes.CreateChildPermission(AppPermissions.Pages_LeaveTypes_Delete, L("DeleteLeaveTypes"));

            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

            var dynamicProperties = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties, L("DynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Create, L("CreatingDynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Edit, L("EditingDynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Delete, L("DeletingDynamicProperties"));

            var dynamicPropertyValues = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue, L("DynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Create, L("CreatingDynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Edit, L("EditingDynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Delete, L("DeletingDynamicPropertyValue"));

            var dynamicEntityProperties = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties, L("DynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Create, L("CreatingDynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Edit, L("EditingDynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Delete, L("DeletingDynamicEntityProperties"));

            var dynamicEntityPropertyValues = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue, L("EntityDynamicPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Create, L("CreatingDynamicEntityPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Edit, L("EditingDynamicEntityPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Delete, L("DeletingDynamicEntityPropertyValue"));

            //PAYROLL-SPECIFIC PERMISSIONS

            var ePay = pages.CreateChildPermission(AppPermissions.Pages_Tenant_ePay, L("ePay"), multiTenancySides: MultiTenancySides.Tenant);
            ePay.CreateChildPermission(AppPermissions.Pages_Tenant_ePay_CreatePerson, L("CreateNewPerson"), multiTenancySides: MultiTenancySides.Tenant);
            ePay.CreateChildPermission(AppPermissions.Pages_Tenant_ePay_DeletePerson, L("DeletePerson"), multiTenancySides: MultiTenancySides.Tenant);

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ePayConsts.LocalizationSourceName);
        }
    }
}