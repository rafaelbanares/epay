﻿using Abp;
using Abp.Domain.Services;
using System.Threading.Tasks;

namespace MMB.ePay.Authorization.Users.Profile
{
    public interface IProfileImageService : IDomainService
    {
        Task<string> GetProfilePictureContentForUser(UserIdentifier userIdentifier);
    }
}