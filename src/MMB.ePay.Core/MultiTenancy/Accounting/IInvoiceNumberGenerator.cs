﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace MMB.ePay.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}