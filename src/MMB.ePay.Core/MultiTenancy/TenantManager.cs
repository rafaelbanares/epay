﻿using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Abp;
using Abp.Application.Features;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Editions;
using MMB.ePay.MultiTenancy.Demo;
using Abp.Extensions;
using Abp.Notifications;
using Abp.Runtime.Security;
using Microsoft.AspNetCore.Identity;
using MMB.ePay.Notifications;
using System;
using System.Diagnostics;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using MMB.ePay.MultiTenancy.Payments;
using System.Collections.Generic;
using MMB.ePay.Payroll;
using MMB.ePay.HR;

namespace MMB.ePay.MultiTenancy
{
    /// <summary>
    /// Tenant manager.
    /// </summary>
    public class TenantManager : AbpTenantManager<Tenant, User>
    {
        public IAbpSession AbpSession { get; set; }

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IUserEmailer _userEmailer;
        private readonly TenantDemoDataBuilder _demoDataBuilder;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<SubscribableEdition> _subscribableEditionRepository;

        private readonly IRepository<ProcessStatus> _processStatusRepository;
        private readonly IRepository<Banks, string> _banksRepository;
        private readonly IRepository<BankBranches> _bankBranchesRepository;
        private readonly IRepository<TenantBanks> _tenantBanksRepository;
        private readonly IRepository<HolidayTypes, string> _holidayTypesRepository;
        private readonly IRepository<Holidays> _holidaysRepository;
        private readonly IRepository<PayPeriod> _payPeriodRepository;
        private readonly IRepository<TaxTables> _taxTablesRepository;
        private readonly IRepository<Ranks> _ranksRepository;
        private readonly IRepository<Reports> _reportsRepository;
        private readonly IRepository<GroupSettings> _groupSettingsRepository;
        private readonly IRepository<PaySettings> _paySettingsRepository;

        private readonly IRepository<DeductionTypes> _deductionTypesRepository;
        private readonly IRepository<EarningTypes> _earningTypesRepository;
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;
        private readonly IRepository<LoanTypes> _loanTypesRepository;
        private readonly IRepository<OvertimeMainTypes> _overtimeMainTypesRepository;
        private readonly IRepository<OvertimeSubTypes> _overtimeSubTypesRepository;
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IRepository<StatutorySubTypes> _statutoryTypesRepository;
        private readonly IRepository<TimesheetTypes> _timesheetTypesRepository;
        private readonly IRepository<EmploymentTypes, string> _employmentTypesRepository;
        private readonly IRepository<TenantEmploymentTypes> _tenantEmploymentTypesRepository;

        public TenantManager(
            IRepository<Tenant> tenantRepository,
            IRepository<TenantFeatureSetting, long> tenantFeatureRepository,
            EditionManager editionManager,
            IUnitOfWorkManager unitOfWorkManager,
            RoleManager roleManager,
            IUserEmailer userEmailer,
            TenantDemoDataBuilder demoDataBuilder,
            UserManager userManager,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IAbpZeroFeatureValueStore featureValueStore,
            IAbpZeroDbMigrator abpZeroDbMigrator,
            IPasswordHasher<User> passwordHasher,
            IRepository<SubscribableEdition> subscribableEditionRepository,

            IRepository<ProcessStatus> processStatusRepository,
            IRepository<Banks, string> banksRepository,
            IRepository<BankBranches> bankBranchesRepository,
            IRepository<TenantBanks> tenantBanksRepository,
            IRepository<HolidayTypes, string> holidayTypesRepository,
            IRepository<Holidays> holidaysRepository,
            IRepository<PayPeriod> payPeriodRepository,
            IRepository<TaxTables> taxTablesRepository,
            IRepository<Ranks> ranksRepository,
            IRepository<Reports> reportsRepository,
            IRepository<GroupSettings> groupSettingsRepository,
            IRepository<PaySettings> paySettingsRepository,

            IRepository<DeductionTypes> deductionTypesRepository,
            IRepository<EarningTypes> earningTypesRepository,
            IRepository<LeaveTypes> leaveTypesRepository,
            IRepository<LoanTypes> loanTypesRepository,
            IRepository<OvertimeMainTypes> overtimeMainTypesRepository,
            IRepository<OvertimeSubTypes> overtimeSubTypesRepository,
            IRepository<OvertimeTypes> overtimeTypesRepository,
            IRepository<StatutorySubTypes> statutoryTypesRepository,
            IRepository<TimesheetTypes> timesheetTypesRepository,
            IRepository<EmploymentTypes, string> employmentTypesRepository,
            IRepository<TenantEmploymentTypes> tenantEmploymentTypesRepository

            ) : base(
                tenantRepository,
                tenantFeatureRepository,
                editionManager,
                featureValueStore
            )
        {
            AbpSession = NullAbpSession.Instance;

            _unitOfWorkManager = unitOfWorkManager;
            _roleManager = roleManager;
            _userEmailer = userEmailer;
            _demoDataBuilder = demoDataBuilder;
            _userManager = userManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _abpZeroDbMigrator = abpZeroDbMigrator;
            _passwordHasher = passwordHasher;
            _subscribableEditionRepository = subscribableEditionRepository;

            _processStatusRepository = processStatusRepository;
            _banksRepository = banksRepository;
            _bankBranchesRepository = bankBranchesRepository;
            _tenantBanksRepository = tenantBanksRepository;
            _holidayTypesRepository = holidayTypesRepository;
            _holidaysRepository = holidaysRepository;
            _payPeriodRepository = payPeriodRepository;
            _taxTablesRepository = taxTablesRepository;
            _ranksRepository = ranksRepository;
            _reportsRepository = reportsRepository;
            _groupSettingsRepository = groupSettingsRepository;
            _paySettingsRepository = paySettingsRepository;

            _deductionTypesRepository = deductionTypesRepository;
            _earningTypesRepository = earningTypesRepository;
            _leaveTypesRepository = leaveTypesRepository;
            _loanTypesRepository = loanTypesRepository;
            _overtimeMainTypesRepository = overtimeMainTypesRepository;
            _overtimeSubTypesRepository = overtimeSubTypesRepository;
            _overtimeTypesRepository = overtimeTypesRepository;
            _statutoryTypesRepository = statutoryTypesRepository;
            _timesheetTypesRepository = timesheetTypesRepository;
            _employmentTypesRepository = employmentTypesRepository;
            _tenantEmploymentTypesRepository = tenantEmploymentTypesRepository;
        }

        protected virtual async Task CreateDefaultOther()
        {
            //ProcessStatus
            var posted = new ProcessStatus
            {
                DisplayName = "POSTED",
                Description = "POSTED",
                SysCode = "POSTED",
                Step = 4,
                NextStatus = null,
                Responsible = "PROCESSOR"
            };

            await _processStatusRepository.InsertAsync(posted);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var readyForPosting = new ProcessStatus
            {
                DisplayName = "Ready for Posting",
                Description = "Ready for Posting",
                SysCode = "RFP",
                Step = 4,
                Responsible = "PROCESSOR"
            };

            readyForPosting.NextStatus = posted.Id;

            await _processStatusRepository.InsertAsync(readyForPosting);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var forFinalReview = new ProcessStatus
            {
                DisplayName = "For Final Review",
                Description = "For Final Review",
                SysCode = "FFR",
                Step = 3,
                Responsible = "REVIEWER3"
            };

            forFinalReview.NextStatus = readyForPosting.Id;

            await _processStatusRepository.InsertAsync(forFinalReview);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var forSecondReview = new ProcessStatus
            {
                DisplayName = "For Second Review",
                Description = "For Second Review",
                SysCode = "FSR",
                Step = 3,
                Responsible = "REVIEWER2"
            };

            forSecondReview.NextStatus = forFinalReview.Id;

            await _processStatusRepository.InsertAsync(forSecondReview);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var forInitialReview = new ProcessStatus
            {
                DisplayName = "For Initial Review",
                Description = "For Initial Review",
                SysCode = "FIR",
                Step = 3,
                Responsible = "PROCESSOR"
            };

            forInitialReview.NextStatus = forSecondReview.Id;

            await _processStatusRepository.InsertAsync(forInitialReview);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var ongoingProcess = new ProcessStatus
            {
                DisplayName = "Ongoing Process",
                Description = "Ongoing Process",
                SysCode = "OP",
                Step = 2,
                Responsible = "SYSTEM"
            };

            ongoingProcess.NextStatus = forInitialReview.Id;

            await _processStatusRepository.InsertAsync(ongoingProcess);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var waitingToProcess = new ProcessStatus
            {
                DisplayName = "Waiting to Process",
                Description = "Waiting to Process",
                SysCode = "WTP",
                Step = 2,
                Responsible = "SYSTEM"
            };

            waitingToProcess.NextStatus = ongoingProcess.Id;

            await _processStatusRepository.InsertAsync(waitingToProcess);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var payrollEntry = new ProcessStatus
            {
                DisplayName = "Payroll Entry",
                Description = "Payroll Entry",
                SysCode = "PE",
                Step = 1,
                Responsible = "PROCESSOR"
            };

            payrollEntry.NextStatus = waitingToProcess.Id;

            await _processStatusRepository.InsertAsync(payrollEntry);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            //Banks
            var banks = new List<Tuple<string, string, string>>
            {
                new Tuple<string, string, string> ("BDO", "BANCO DE ORO", "SA"),
                new Tuple<string, string, string> ("BPI", "Bank of the Philippines Island", "CA"),
                new Tuple<string, string, string> ("CHINABANK", "China Bank Corporation", "SA"),
                new Tuple<string, string, string> ("EWBC", "EAST WEST BANKING CORP.", "CA"),
                new Tuple<string, string, string> ("IEB", "International Exchange Bank", "SA"),
                new Tuple<string, string, string> ("METROB", "Metro Bank", "CA"),
                new Tuple<string, string, string> ("UBP", "UNIONBANK OF THE PHILIPPINES", "SA")
            };

            foreach (var bank in banks)
            {

                await _banksRepository.InsertAsync(new Banks
                {
                    Id = bank.Item1,
                    DisplayName = bank.Item2,
                    Description = bank.Item2
                });
                await _unitOfWorkManager.Current.SaveChangesAsync();

                var bankBranch = new BankBranches
                {
                    BankId = bank.Item1,
                    BranchCode = bank.Item1,
                    BranchName = bank.Item1 + " Branch 1",
                    Address1 = "Test 1",
                    Address2 = "Test 2"
                };

                await _bankBranchesRepository.InsertAsync(bankBranch);
                await _unitOfWorkManager.Current.SaveChangesAsync();

                var bankAccountNumber = bank.Item1 + "123456789";

                await _tenantBanksRepository.InsertAsync(new TenantBanks
                {
                    BankAccountNumber = bankAccountNumber,
                    BankAccountType = bank.Item3,
                    BranchNumber = bank.Item1.Substring(0, 3) + "001",
                    BankBranchId = bankBranch.Id,
                    CorporateId = "456789123"
                });
                await _unitOfWorkManager.Current.SaveChangesAsync();
            }

            //Holidays
            var holidayTypes = new List<Tuple<string, string, bool>>
            {
                new Tuple<string, string, bool> ("C", "Company Holiday", true),
                new Tuple<string, string, bool> ("D", "Double Legal", false),
                new Tuple<string, string, bool> ("L", "Legal", false),
                new Tuple<string, string, bool> ("LS", "Legal & Special", false),
                new Tuple<string, string, bool> ("R", "Regional Holiday", false),
                new Tuple<string, string, bool> ("S", "Special", false)
            };

            foreach (var holidayType in holidayTypes)
            {
                await _holidayTypesRepository.InsertAsync(new HolidayTypes
                {
                    Id = holidayType.Item1,
                    DisplayName = holidayType.Item2,
                    Description = holidayType.Item2,
                    HalfDayEnabled = holidayType.Item3
                });
            }
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var holidays = new List<Holidays>
            {
                new Holidays { DisplayName = "New Year's Day", Date = new DateTime(DateTime.Today.Year, 1, 1), HolidayTypeId = "L" },
                new Holidays { DisplayName = "Christmas Eve", Date = new DateTime(DateTime.Today.Year, 12, 24), HolidayTypeId = "S" },
                new Holidays { DisplayName = "Christmas Day", Date = new DateTime(DateTime.Today.Year, 12, 25), HolidayTypeId = "L" }
            };

            foreach (var holiday in holidays)
            {
                await _holidaysRepository.InsertAsync(holiday);
            }

            //PayPeriod
            var currentYear = DateTime.Today.Year;
            var payPeriods = new List<Tuple<DateTime, DateTime, DateTime, string>>
            {
                new Tuple<DateTime, DateTime, DateTime, string>
                (
                    new DateTime(currentYear - 1, 12, 24),
                    new DateTime(currentYear, 12, 8),
                    new DateTime(currentYear - 1, 12, 31),
                    "2"
                )
            };

            var months = Enumerable.Range(1, 12).ToList();

            foreach (var month in months)
            {
                var lastDay = DateTime.DaysInMonth(currentYear, month);
                var nextMonth = month == 12 ? 1 : month + 1;
                var nextYear = month == 12 ? currentYear + 1 : currentYear;

                payPeriods.Add(new Tuple<DateTime, DateTime, DateTime, string>
                (
                    new DateTime(currentYear, month, 9),
                    new DateTime(currentYear, month, 23),
                    new DateTime(currentYear, month, lastDay),
                    "1"
                ));

                payPeriods.Add(new Tuple<DateTime, DateTime, DateTime, string>
                (
                    new DateTime(currentYear, month, 24),
                    new DateTime(nextYear, nextMonth, 8),
                    new DateTime(currentYear, month, lastDay),
                    "2"
                ));
            }

            foreach (var payPeriod in payPeriods)
            {
                await _payPeriodRepository.InsertAsync(new PayPeriod
                {
                    PayrollFreq = "S",
                    ProcessType = "REGULAR",
                    PeriodFrom = payPeriod.Item1,
                    PeriodTo = payPeriod.Item2,
                    PayDate = payPeriod.Item3,
                    ApplicableMonth = payPeriod.Item1.Month,
                    ApplicableYear = payPeriod.Item1.Year,
                    CutOffFrom = payPeriod.Item1,
                    CutOffTo = payPeriod.Item2,
                    Currency = "PHP",
                    FiscalYear = payPeriod.Item1.Year,
                    WeekNo = payPeriod.Item4,
                    PayrollStatus = "NEW"
                });
            }

            //TaxTables
            var taxTables = new List<TaxTables>
            {
                new TaxTables { PayFreq = "S", Code = "SAL" },
                new TaxTables { PayFreq = "S", Code = "FA" },
                new TaxTables { PayFreq = "W", Code = "SAL" }
            };

            foreach (var taxTable in taxTables)
            {
                taxTable.Year = currentYear;
                taxTable.Bracket1 = 1;
                taxTable.Bracket2 = 2;
                taxTable.Bracket3 = 3;
                taxTable.Bracket4 = 4;
                taxTable.Bracket5 = 5;
                taxTable.Bracket6 = 6;
                taxTable.Bracket7 = 7;

                await _taxTablesRepository.InsertAsync(taxTable);
            }

            //Ranks
            var ranks = new List<string>
            {
                "Executive",
                "Manager",
                "Rank & File",
                "Supervisor"
            };

            foreach (var rank in ranks)
            {
                var sysCode = rank.Substring(0, 1);

                await _ranksRepository.InsertAsync(new Ranks
                {
                    DisplayName = rank,
                    Description = rank,
                    SysCode = sysCode
                });
            }

            //Reports
            var reports = new List<Reports>
            {
                new Reports { DisplayName = "Bank Advise", ControllerName = "BankAdvise", ReportTypeId = "P" },
                new Reports { DisplayName = "Cash Advise", ControllerName = "CashAdvise", ReportTypeId = "P" },
                new Reports { DisplayName = "Cheque Advise", ControllerName = "ChequeAdvise", ReportTypeId = "P" },
                new Reports { DisplayName = "Deduction Register", ControllerName = "DeductionRegister", ReportTypeId = "P" },
                new Reports { DisplayName = "Earning Register", ControllerName = "EarningRegister", ReportTypeId = "P" },
                new Reports { DisplayName = "Employee Payslip", ControllerName = "EmployeePayslip", ReportTypeId = "P" },
                new Reports { DisplayName = "Gross and Tax",  ControllerName = "GrossAndTax", ReportTypeId = "P" },
                new Reports { DisplayName = "Pag-ibig Contribution", ControllerName = "PagibigContribution", ReportTypeId = "P" },
                new Reports { DisplayName = "Payroll Register", ControllerName = "PayrollRegister", ReportTypeId = "P" },
                new Reports { DisplayName = "Philhealth Contribution", ControllerName = "PhilhealthContribution", ReportTypeId = "P" },
                new Reports { DisplayName = "SSS Contribution", ControllerName = "SssContribution", ReportTypeId = "P" },
                new Reports { DisplayName = "Timesheet Register", ControllerName = "TimesheetRegister", ReportTypeId = "P" }
            };

            foreach (var report in reports)
            {
                report.Format = "Letter";
                report.Output = "P";
                report.Enabled = true;

                await _reportsRepository.InsertAsync(report);
            }

            //Settings
            var orgGroup = new GroupSettings
            {
                SysCode = "ORG",
                DisplayName = "Org. Group Settings",
                OrderNo = 1,
            };

            await _groupSettingsRepository.InsertAsync(orgGroup);

            var miscGroup = new GroupSettings
            {
                SysCode = "MISC",
                DisplayName = "Miscellaneous",
                OrderNo = 2,
            };

            await _groupSettingsRepository.InsertAsync(miscGroup);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            var paySettings = new List<PaySettings>
            {
                new PaySettings
                {
                    Key = "DEPARTMENT_LEVEL",
                    Value = "2",
                    DataType = "int",
                    Caption = "Department Level",
                    GroupSettingId = orgGroup.Id
                }
            };

            foreach (var paySetting in paySettings)
            {
                await _paySettingsRepository.InsertAsync(paySetting);
            }

            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        protected virtual async Task CreateDefaultTypes()
        {
            //DeductionTypes
            var deductionTypes = new List<string>
            {
                "VALE ADJUSTMENT",
                "CARD REPLACEMENT FEE",
                "REPAIR & MAINTENANCE",
                "SAFETY SHOES HIGH",
                "SAFETY SHOES LOW",
                "PAGIBIG - DEDUCTION ADJUSTMENT",
                "PHIL - DEDUCTION ADJUSTMENT",
                "SS - DEDUCTION ADJUSTMENT",
                "TAX - DEDUCTION ADJUSTMENT",
                "ADVANCES"
            };

            foreach (var deductionType in deductionTypes)
            {
                await _deductionTypesRepository.InsertAsync(new DeductionTypes
                {
                    DisplayName = deductionType,
                    Description = deductionType,
                    Active = true,
                    Currency = "PHP"
                });
            }

            //EarningTypes
            var earningTypes = new List<EarningTypes>
            {
                new EarningTypes { DisplayName = "ADVANCE234", Taxable = true },
            };

            foreach (var earningType in earningTypes)
            {
                earningType.Description = earningType.DisplayName;
                earningType.Active = true;
                earningType.Currency = "PHP";

                await _earningTypesRepository.InsertAsync(earningType);
            }

            //LeaveTypes
            var leaveTypes = new List<LeaveTypes>
            {
                new LeaveTypes { DisplayName = "SL", Description = "Sick Leave", WithPay = true },
                new LeaveTypes { DisplayName = "VL", Description = "Vacation Leave", WithPay = true },
                new LeaveTypes { DisplayName = "TOIL", Description = "Toil Leave", WithPay = true },
                new LeaveTypes { DisplayName = "ML", Description = "Maternity Leave", WithPay = true },
                new LeaveTypes { DisplayName = "PL", Description = "Paternity Leave", WithPay = true },
                new LeaveTypes { DisplayName = "LWOP", Description = "Leave Without Pay", WithPay = false },
                new LeaveTypes { DisplayName = "BRL", Description = "Bereavement Leave", WithPay = true },
                new LeaveTypes { DisplayName = "UL", Description = "Union Leave", WithPay = false },
                new LeaveTypes { DisplayName = "LL", Description = "Longevity Leave", WithPay = false },
                new LeaveTypes { DisplayName = "SPL", Description = "Solo Parent Leave", WithPay = false },
                new LeaveTypes { DisplayName = "MRL", Description = "Marriage Leave", WithPay = false },
                new LeaveTypes { DisplayName = "CL", Description = "Calamity Leave", WithPay = false },
                new LeaveTypes { DisplayName = "STL", Description = "Study Leave", WithPay = false },
                new LeaveTypes { DisplayName = "SPLW", Description = "Special Leave for Women", WithPay = false }
            };

            foreach (var leaveType in leaveTypes)
            {
                await _leaveTypesRepository.InsertAsync(leaveType);
            }

            var loanTypes = new List<LoanTypes>
            {
                new LoanTypes { SysCode = "OTHERS", Description = "OTHERS" },
                new LoanTypes { SysCode = "PAGIBIG", Description = "PAGIBIG LOAN" },
                new LoanTypes { SysCode = "SSS", Description = "SSS LOAN" }
            };

            foreach (var loanType in loanTypes)
            {
                await _loanTypesRepository.InsertAsync(loanType);
            }

            var overtimeGroup1s = new List<OvertimeMainTypes>();
            var overtimeGroup2s = new List<OvertimeSubTypes>();

            //OvertimeGroup1
            var reg = new OvertimeMainTypes { DisplayName = "REG", Description = "Regular OT", DisplayOrder = 1 };
            overtimeGroup1s.Add(reg);

            var lgl = new OvertimeMainTypes { DisplayName = "LGL", Description = "Legal OT", DisplayOrder = 2 };
            overtimeGroup1s.Add(lgl);

            var rst = new OvertimeMainTypes { DisplayName = "RST", Description = "Rest Day OT", DisplayOrder = 3 };
            overtimeGroup1s.Add(rst);

            var spl = new OvertimeMainTypes { DisplayName = "SPL", Description = "Special OT", DisplayOrder = 4 };
            overtimeGroup1s.Add(spl);

            var lglrst = new OvertimeMainTypes { DisplayName = "LGLRST", Description = "Legal Rest Day", DisplayOrder = 5 };
            overtimeGroup1s.Add(lglrst);

            var splrst = new OvertimeMainTypes { DisplayName = "SPLRST", Description = "Special Rest Day", DisplayOrder = 6 };
            overtimeGroup1s.Add(splrst);

            foreach (var overtimeGroup1 in overtimeGroup1s)
            {
                await _overtimeMainTypesRepository.InsertAsync(overtimeGroup1);
            }

            //OvertimeGroup2
            var first8 = new OvertimeSubTypes { DisplayName = "First8", Description = "First8", DisplayOrder = 1 };
            overtimeGroup2s.Add(first8);

            var nd1 = new OvertimeSubTypes { DisplayName = "ND1", Description = "ND1", DisplayOrder = 2 };
            overtimeGroup2s.Add(nd1);

            var beyond8 = new OvertimeSubTypes { DisplayName = "Beyond8", Description = "Beyond8", DisplayOrder = 3 };
            overtimeGroup2s.Add(beyond8);

            var nd2 = new OvertimeSubTypes { DisplayName = "ND2", Description = "ND2", DisplayOrder = 4 };
            overtimeGroup2s.Add(nd2);

            foreach (var overtimeGroup2 in overtimeGroup2s)
            {
                await _overtimeSubTypesRepository.InsertAsync(overtimeGroup2);
            }

            await _unitOfWorkManager.Current.SaveChangesAsync();

            //OvertimeTypes
            var overtimeTypes = new List<Tuple<int, string, string>>
            {
                new Tuple<int, string, string> ( reg.Id, "REG", "Regular" ),
                new Tuple<int, string, string> ( lgl.Id, "LGL", "Legal" ),
                new Tuple<int, string, string> ( rst.Id, "RST", "Rest Day" ),
                new Tuple<int, string, string> ( spl.Id, "SPL", "Special" ),
                new Tuple<int, string, string> ( lglrst.Id, "LGLRST", "Legal Rest Day" ),
                new Tuple<int, string, string> ( splrst.Id, "SPLRST", "Special Rest Day" )
            };

            foreach (var overtimeType in overtimeTypes)
            {

                await _overtimeTypesRepository.InsertAsync(new OvertimeTypes
                {
                    OvertimeMainTypeId  = overtimeType.Item1,
                    OvertimeSubTypeId = first8.Id,
                    DisplayName = overtimeType.Item2,
                    Description = overtimeType.Item3 + " OT",
                    Rate = 999
                });

                await _overtimeTypesRepository.InsertAsync(new OvertimeTypes
                {
                    OvertimeMainTypeId  = overtimeType.Item1,
                    OvertimeSubTypeId = nd1.Id,
                    DisplayName = overtimeType.Item2 + " ND1",
                    Description = overtimeType.Item3 + " ND1",
                    Rate = 999
                });

                await _overtimeTypesRepository.InsertAsync(new OvertimeTypes
                {
                    OvertimeMainTypeId  = overtimeType.Item1,
                    OvertimeSubTypeId = beyond8.Id,
                    DisplayName = overtimeType.Item2 + "8",
                    Description = overtimeType.Item3 + " >8",
                    Rate = 999
                });

                await _overtimeTypesRepository.InsertAsync(new OvertimeTypes
                {
                    OvertimeMainTypeId  = overtimeType.Item1,
                    OvertimeSubTypeId = nd2.Id,
                    DisplayName = overtimeType.Item2 + " ND2",
                    Description = overtimeType.Item3 + " ND2",
                    Rate = 999
                });
            }

            //StatutoryTypes
            var statutoryTypes = new List<string>
            {
                "EE PAGIBIG",
                "EE PH",
                "EE SSS",
                "ER ECC",
                "ER PAGIBIG",
                "ER PH",
                "ER SSS",
                "WTAX"
            };

            foreach (var statutoryType in statutoryTypes)
            {
                await _statutoryTypesRepository.InsertAsync(new StatutorySubTypes
                {
                    DisplayName = statutoryType,
                    Description = statutoryType,
                    SysCode = statutoryType.Replace(" ", ""),
                    EmployeeShare = statutoryType.Contains("EE") || statutoryType == "WTAX"
                });
            }

            //TimesheetTypes
            var timesheetTypes = new List<Tuple<string, short>>
            {
                new Tuple<string, short> ("Regular", 1),
                new Tuple<string, short> ("Absences", -1),
                new Tuple<string, short> ("Tardy", -1),
                new Tuple<string, short> ("Undertime", -1),
                new Tuple<string, short> ("Regular ND1", 1),
                new Tuple<string, short> ("Regular ND2", 1),
                new Tuple<string, short> ("Paid Holiday", 1),
                new Tuple<string, short> ("Sick Leave", 1),
                new Tuple<string, short> ("Vacation Leave", 1)
            };

            foreach (var timesheetType in timesheetTypes.Select((value, i) => new { DisplayOrder = i + 1, DisplayName = value.Item1, Multiplier = value.Item2 }))
            {
                await _timesheetTypesRepository.InsertAsync(new TimesheetTypes
                {
                    DisplayName = timesheetType.DisplayName,
                    Description = timesheetType.DisplayName,
                    DisplayOrder = timesheetType.DisplayOrder,
                    Multiplier = timesheetType.Multiplier,
                    Taxable = true,
                    Active = true
                });
            }

            await _unitOfWorkManager.Current.SaveChangesAsync();

            //EmploymentTypes
            var employmentTypes = new List<Tuple<string, string>>
            {
                new Tuple<string, string> ("C", "Contractual"),
                new Tuple<string, string> ("P", "Probationary"),
                new Tuple<string, string> ("R", "Regular"),
                new Tuple<string, string> ("S", "Consultant")
            };

            foreach (var employmentType in employmentTypes)
            {
                var input = new EmploymentTypes
                {
                    Id = employmentType.Item1,
                    DisplayName = employmentType.Item2,
                    Description = employmentType.Item2
                };

                await _employmentTypesRepository.InsertAsync(input);
                await _unitOfWorkManager.Current.SaveChangesAsync();

                await _tenantEmploymentTypesRepository.InsertAsync(new TenantEmploymentTypes
                {
                    EmploymentTypeId = employmentType.Item1
                });
                await _unitOfWorkManager.Current.SaveChangesAsync();
            }
        }

        public async Task<int> CreateWithAdminUserAsync(
            string tenancyName,
            string name,
            string adminPassword,
            string adminEmailAddress,
            string connectionString,
            bool isActive,
            int? editionId,
            bool shouldChangePasswordOnNextLogin,
            bool sendActivationEmail,
            DateTime? subscriptionEndDate,
            bool isInTrialPeriod,
            string emailActivationLink)
        {
            int newTenantId;
            long newAdminId;

            await CheckEditionAsync(editionId, isInTrialPeriod);

            if (isInTrialPeriod && !subscriptionEndDate.HasValue)
            {
                throw new UserFriendlyException(LocalizationManager.GetString(ePayConsts.LocalizationSourceName, "TrialWithoutEndDateErrorMessage"));
            }

            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
            {
                //Create tenant
                var tenant = new Tenant(tenancyName, name)
                {
                    IsActive = isActive,
                    EditionId = editionId,
                    SubscriptionEndDateUtc = subscriptionEndDate?.ToUniversalTime(),
                    IsInTrialPeriod = isInTrialPeriod,
                    ConnectionString = connectionString.IsNullOrWhiteSpace() ? null : SimpleStringCipher.Instance.Encrypt(connectionString)
                };

                await CreateAsync(tenant);
                await _unitOfWorkManager.Current.SaveChangesAsync(); //To get new tenant's id.

                //Create tenant database
                _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

                //We are working entities of new tenant, so changing tenant filter
                using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                {
                    //Create static roles for new tenant
                    CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get static role ids

                    //grant all permissions to admin role
                    var adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
                    await _roleManager.GrantAllPermissionsAsync(adminRole);

                    //User role should be default
                    var userRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.User);
                    userRole.IsDefault = true;
                    CheckErrors(await _roleManager.UpdateAsync(userRole));

                    //Create admin user for the tenant
                    var adminUser = User.CreateTenantAdminUser(tenant.Id, adminEmailAddress);
                    adminUser.ShouldChangePasswordOnNextLogin = shouldChangePasswordOnNextLogin;
                    adminUser.IsActive = true;

                    if (adminPassword.IsNullOrEmpty())
                    {
                        adminPassword = await _userManager.CreateRandomPassword();
                    }
                    else
                    {
                        await _userManager.InitializeOptionsAsync(AbpSession.TenantId);
                        foreach (var validator in _userManager.PasswordValidators)
                        {
                            CheckErrors(await validator.ValidateAsync(_userManager, adminUser, adminPassword));
                        }

                    }

                    adminUser.Password = _passwordHasher.HashPassword(adminUser, adminPassword);

                    CheckErrors(await _userManager.CreateAsync(adminUser));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get admin user's id

                    //Assign admin user to admin role!
                    CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));

                    //Notifications
                    await _appNotifier.WelcomeToTheApplicationAsync(adminUser);

                    //Send activation email
                    if (sendActivationEmail)
                    {
                        adminUser.SetNewEmailConfirmationCode();
                        await _userEmailer.SendEmailActivationLinkAsync(adminUser, emailActivationLink, adminPassword);
                    }

                    await _unitOfWorkManager.Current.SaveChangesAsync();

                    await CreateDefaultOther();
                    await CreateDefaultTypes();

                    await _demoDataBuilder.BuildForAsync(tenant);

                    newTenantId = tenant.Id;
                    newAdminId = adminUser.Id;
                }

                await uow.CompleteAsync();
            }

            //Used a second UOW since UOW above sets some permissions and _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync needs these permissions to be saved.
            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
            {
                using (_unitOfWorkManager.Current.SetTenantId(newTenantId))
                {
                    await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(new UserIdentifier(newTenantId, newAdminId));
                    await _unitOfWorkManager.Current.SaveChangesAsync();
                    await uow.CompleteAsync();
                }
            }

            return newTenantId;
        }

        public async Task CheckEditionAsync(int? editionId, bool isInTrialPeriod)
        {
            if (!editionId.HasValue || !isInTrialPeriod)
            {
                return;
            }

            var edition = await _subscribableEditionRepository.GetAsync(editionId.Value);
            if (!edition.IsFree)
            {
                return;
            }

            var error = LocalizationManager.GetSource(ePayConsts.LocalizationSourceName).GetString("FreeEditionsCannotHaveTrialVersions");
            throw new UserFriendlyException(error);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public decimal GetUpgradePrice(SubscribableEdition currentEdition, SubscribableEdition targetEdition, int totalRemainingHourCount, PaymentPeriodType paymentPeriodType)
        {
            int numberOfHoursPerDay = 24;

            var totalRemainingDayCount = totalRemainingHourCount / numberOfHoursPerDay;
            var unusedPeriodCount = totalRemainingDayCount / (int)paymentPeriodType;
            var unusedHoursCount = totalRemainingHourCount % ((int)paymentPeriodType * numberOfHoursPerDay);

            decimal currentEditionPriceForUnusedPeriod = 0;
            decimal targetEditionPriceForUnusedPeriod = 0;

            var currentEditionPrice = currentEdition.GetPaymentAmount(paymentPeriodType);
            var targetEditionPrice = targetEdition.GetPaymentAmount(paymentPeriodType);

            if (currentEditionPrice > 0)
            {
                currentEditionPriceForUnusedPeriod = currentEditionPrice * unusedPeriodCount;
                currentEditionPriceForUnusedPeriod += (currentEditionPrice / (int)paymentPeriodType) / numberOfHoursPerDay * unusedHoursCount;
            }

            if (targetEditionPrice > 0)
            {
                targetEditionPriceForUnusedPeriod = targetEditionPrice * unusedPeriodCount;
                targetEditionPriceForUnusedPeriod += (targetEditionPrice / (int)paymentPeriodType) / numberOfHoursPerDay * unusedHoursCount;
            }

            return targetEditionPriceForUnusedPeriod - currentEditionPriceForUnusedPeriod;
        }

        public async Task<Tenant> UpdateTenantAsync(int tenantId, bool isActive, bool? isInTrialPeriod, PaymentPeriodType? paymentPeriodType, int editionId, EditionPaymentType editionPaymentType)
        {
            var tenant = await FindByIdAsync(tenantId);

            tenant.IsActive = isActive;

            if (isInTrialPeriod.HasValue)
            {
                tenant.IsInTrialPeriod = isInTrialPeriod.Value;
            }

            tenant.EditionId = editionId;

            if (paymentPeriodType.HasValue)
            {
                tenant.UpdateSubscriptionDateForPayment(paymentPeriodType.Value, editionPaymentType);
            }

            return tenant;
        }

        public async Task<EndSubscriptionResult> EndSubscriptionAsync(Tenant tenant, SubscribableEdition edition, DateTime nowUtc)
        {
            if (tenant.EditionId == null || tenant.HasUnlimitedTimeSubscription())
            {
                throw new Exception($"Can not end tenant {tenant.TenancyName} subscription for {edition.DisplayName} tenant has unlimited time subscription!");
            }

            Debug.Assert(tenant.SubscriptionEndDateUtc != null, "tenant.SubscriptionEndDateUtc != null");

            var subscriptionEndDateUtc = tenant.SubscriptionEndDateUtc.Value;
            if (!tenant.IsInTrialPeriod)
            {
                subscriptionEndDateUtc = tenant.SubscriptionEndDateUtc.Value.AddDays(edition.WaitingDayAfterExpire ?? 0);
            }

            if (subscriptionEndDateUtc >= nowUtc)
            {
                throw new Exception($"Can not end tenant {tenant.TenancyName} subscription for {edition.DisplayName} since subscription has not expired yet!");
            }

            if (!tenant.IsInTrialPeriod && edition.ExpiringEditionId.HasValue)
            {
                tenant.EditionId = edition.ExpiringEditionId.Value;
                tenant.SubscriptionEndDateUtc = null;

                await UpdateAsync(tenant);

                return EndSubscriptionResult.AssignedToAnotherEdition;
            }

            tenant.IsActive = false;
            tenant.IsInTrialPeriod = false;

            await UpdateAsync(tenant);

            return EndSubscriptionResult.TenantSetInActive;
        }

        public override Task UpdateAsync(Tenant tenant)
        {
            if (tenant.IsInTrialPeriod && !tenant.SubscriptionEndDateUtc.HasValue)
            {
                throw new UserFriendlyException(LocalizationManager.GetString(ePayConsts.LocalizationSourceName, "TrialWithoutEndDateErrorMessage"));
            }

            return base.UpdateAsync(tenant);
        }
    }
}
