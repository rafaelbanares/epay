﻿using Microsoft.Extensions.Configuration;

namespace MMB.ePay.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
