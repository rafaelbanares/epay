﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("CompanyStatutoryInfo")]
    public class CompanyStatutoryInfo : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(CompanyStatutoryInfoConsts.MaxStatutoryTypeLength, MinimumLength = CompanyStatutoryInfoConsts.MinStatutoryTypeLength)]
        public virtual string StatutoryType { get; set; }

        [Required]
        [StringLength(CompanyStatutoryInfoConsts.MaxStatutoryAcctNoLength, MinimumLength = CompanyStatutoryInfoConsts.MinStatutoryAcctNoLength)]
        public virtual string StatutoryAcctNo { get; set; }

        [Required]
        public virtual int SignatoryId { get; set; }

        [Required]
        [StringLength(CompanyStatutoryInfoConsts.MaxBranchLength, MinimumLength = CompanyStatutoryInfoConsts.MinBranchLength)]
        public virtual string Branch { get; set; }

        public virtual StatutoryTypes StatutoryTypes { get; set; }
        public virtual Signatories Signatories { get; set; }
    }
}