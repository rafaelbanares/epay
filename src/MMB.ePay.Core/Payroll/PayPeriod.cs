﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("PayPeriod")]
    public class PayPeriod : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(PayPeriodConsts.MaxPayrollFreqLength, MinimumLength = PayPeriodConsts.MinPayrollFreqLength)]
        public virtual string PayrollFreq { get; set; }

        [Required]
        [StringLength(PayPeriodConsts.MaxProcessTypeLength, MinimumLength = PayPeriodConsts.MinProcessTypeLength)]
        public virtual string ProcessType { get; set; }

        [Required]
        public virtual DateTime PeriodFrom { get; set; }

        [Required]
        public virtual DateTime PeriodTo { get; set; }

        [Required]
        public virtual DateTime PayDate { get; set; }

        [Required]
        public virtual int ApplicableMonth { get; set; }

        [Required]
        public virtual int ApplicableYear { get; set; }

        public virtual int? ManDays { get; set; }

        [Required]
        public virtual DateTime CutOffFrom { get; set; }

        [Required]
        public virtual DateTime CutOffTo { get; set; }

        [StringLength(PayPeriodConsts.MaxCurrencyLength, MinimumLength = PayPeriodConsts.MinCurrencyLength)]
        public virtual string Currency { get; set; }

        public virtual decimal? ExchangeRate { get; set; }

        [Required]
        public virtual int FiscalYear { get; set; }

        public virtual int? PayrollWeekNo { get; set; }

        [Required]
        [StringLength(PayPeriodConsts.MaxWeekNoLength, MinimumLength = PayPeriodConsts.MinWeekNoLength)]
        public virtual string WeekNo { get; set; }

        [StringLength(PayPeriodConsts.MaxPayrollStatusLength, MinimumLength = PayPeriodConsts.MinPayrollStatusLength)]
        public virtual string PayrollStatus { get; set; }

        [StringLength(PayPeriodConsts.MaxRemarksLength, MinimumLength = PayPeriodConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual ICollection<LoanPayments> LoanPayments { get; set; }
        public virtual ICollection<LoanPosted> LoanPosted { get; set; }
        public virtual ICollection<Process> Process { get; set; }
        public virtual ICollection<PayrollProcess> PayrollProcesses { get; set; }
        public virtual ICollection<PayrollPosted> PayrollPosted { get; set; }
    }
}