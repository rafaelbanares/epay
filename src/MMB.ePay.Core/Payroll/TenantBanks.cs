﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("TenantBanks")]
    public class TenantBanks : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(TenantBanksConsts.MaxBankAccountNumberLength, MinimumLength = TenantBanksConsts.MinBankAccountNumberLength)]
        public virtual string BankAccountNumber { get; set; }

        [Required]
        [StringLength(TenantBanksConsts.MaxBankAccountTypeLength, MinimumLength = TenantBanksConsts.MinBankAccountTypeLength)]
        public virtual string BankAccountType { get; set; }

        [StringLength(TenantBanksConsts.MaxBranchNumberLength, MinimumLength = TenantBanksConsts.MinBranchNumberLength)]
        public virtual string BranchNumber { get; set; }

        [Required]
        public virtual int BankBranchId { get; set; }

        [StringLength(TenantBanksConsts.MaxCorporateIdLength, MinimumLength = TenantBanksConsts.MinCorporateIdLength)]
        public virtual string CorporateId { get; set; }

        public virtual BankBranches BankBranches { get; set; }
    }
}