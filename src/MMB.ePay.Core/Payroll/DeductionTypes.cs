﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("DeductionTypes")]
    public class DeductionTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(DeductionTypesConsts.MaxDisplayNameLength, MinimumLength = DeductionTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(DeductionTypesConsts.MaxDescriptionLength, MinimumLength = DeductionTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual bool Active { get; set; }

        [Required]
        [StringLength(DeductionTypesConsts.MaxCurrencyLength, MinimumLength = DeductionTypesConsts.MinCurrencyLength)]
        public virtual string Currency { get; set; }

        public virtual ICollection<DeductionPosted> DeductionPosted { get; set; }
        public virtual ICollection<DeductionProcess> DeductionProcess { get; set; }
        public virtual ICollection<Deductions> Deductions { get; set; }
        public virtual ICollection<RecurDeductions> RecurDeductions { get; set; }
    }
}