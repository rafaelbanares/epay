﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EarningProcess")]
    public class EarningProcess : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollProcess PayrollProcess { get; set; }
        public virtual EarningTypes EarningTypes { get; set; }
    }
}