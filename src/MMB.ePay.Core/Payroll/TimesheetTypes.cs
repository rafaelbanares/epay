﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("TimesheetTypes")]
    public class TimesheetTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(TimesheetTypesConsts.MaxDisplayNameLength, MinimumLength = TimesheetTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(TimesheetTypesConsts.MaxDescriptionLength, MinimumLength = TimesheetTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [StringLength(TimesheetTypesConsts.MaxSysCodeLength, MinimumLength = TimesheetTypesConsts.MinSysCodeLength)]
        public virtual string SysCode { get; set; }

        [Required]
        public virtual int DisplayOrder { get; set; }

        [Required]
        public virtual decimal Multiplier { get; set; }

        [Required]
        public virtual bool Taxable { get; set; }

        [StringLength(TimesheetTypesConsts.MaxAcctCodeLength, MinimumLength = TimesheetTypesConsts.MinAcctCodeLength)]
        public virtual string AcctCode { get; set; }

        [Required]
        public virtual bool Active { get; set; }

        [StringLength(TimesheetTypesConsts.MaxCategoryLength, MinimumLength = TimesheetTypesConsts.MinCategoryLength)]
        public virtual string Category { get; set; }

        public virtual ICollection<ProratedEarnings> ProratedEarnings { get; set; }
        public virtual ICollection<TimesheetPosted> TimesheetPosted { get; set; }
        public virtual ICollection<TimesheetProcess> TimesheetProcess { get; set; }
        public virtual ICollection<Timesheets> Timesheets { get; set; }
    }
}