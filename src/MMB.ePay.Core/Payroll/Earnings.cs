﻿using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Earnings")]
    public class Earnings : AuditedEntity
    {

        [Required]
        public virtual int ProcessId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual DateTime? TranDate { get; set; }

        public virtual Process Process { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual EarningTypes EarningTypes { get; set; }
    }
}