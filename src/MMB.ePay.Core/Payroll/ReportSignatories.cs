﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("ReportSignatories")]
    public class ReportSignatories : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int ReportId { get; set; }

        public virtual int SignatoryId { get; set; }

        public virtual Reports Reports { get; set; }
        public virtual Signatories Signatories { get; set; }
    }
}