﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("TaxTables")]
    public class TaxTables : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int Year { get; set; }

        [Required]
        [StringLength(TaxTablesConsts.MaxPayFreqLength, MinimumLength = TaxTablesConsts.MinPayFreqLength)]
        public virtual string PayFreq { get; set; }

        [Required]
        [StringLength(TaxTablesConsts.MaxCodeLength, MinimumLength = TaxTablesConsts.MinCodeLength)]
        public virtual string Code { get; set; }

        [Required]
        public virtual decimal Bracket1 { get; set; }

        [Required]
        public virtual decimal Bracket2 { get; set; }

        [Required]
        public virtual decimal Bracket3 { get; set; }

        [Required]
        public virtual decimal Bracket4 { get; set; }

        [Required]
        public virtual decimal Bracket5 { get; set; }

        [Required]
        public virtual decimal Bracket6 { get; set; }

        [Required]
        public virtual decimal Bracket7 { get; set; }

    }
}