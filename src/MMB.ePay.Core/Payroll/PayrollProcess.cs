﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("PayrollProcess")]
    public class PayrollProcess : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual Guid PayCodeId { get; set; }

        [Required]
        public virtual int BankBranchId { get; set; }

        [Required]
        public virtual int PayrollNo { get; set; }

        [Required]
        public virtual decimal Salary { get; set; }

        [Required]
        public virtual decimal DailyRate { get; set; }

        [Required]
        public virtual decimal HourlyRate { get; set; }

        [Required]
        public virtual decimal Timesheet { get; set; }

        [Required]
        public virtual decimal Overtime { get; set; }

        [Required]
        public virtual decimal TxEarningAmt { get; set; }

        [Required]
        public virtual decimal NtxEarningAmt { get; set; }

        [Required]
        public virtual decimal DedTaxAmt { get; set; }

        [Required]
        public virtual decimal DeductionAmt { get; set; }

        [Required]
        public virtual decimal LoanAmt { get; set; }

        [Required]
        public virtual decimal BasicPay { get; set; }

        [Required]
        public virtual decimal GrossPay { get; set; }

        [Required]
        public virtual decimal NetPay { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual PayCodes PayCodes { get; set; }
        public virtual PayPeriod PayPeriod { get; set; }

        public virtual ICollection<LoanProcess> LoanProcess { get; set; }
        public virtual ICollection<EarningProcess> EarningProcess { get; set; }
        public virtual ICollection<DeductionProcess> DeductionProcess { get; set; }
        public virtual ICollection<TimesheetProcess> TimesheetProcess { get; set; }
        public virtual ICollection<OTSheetProcess> OTSheetProcess { get; set; }
        public virtual ICollection<StatutoryProcess> StatutoryProcess { get; set; }
        public virtual ICollection<EarningTmp> EarningTmp { get; set; }
        public virtual ICollection<DeductionTmp> DeductionTmp { get; set; }
        public virtual ICollection<TimesheetTmp> TimesheetTmp { get; set; }
        public virtual ICollection<OTSheetTmp> OTSheetTmp { get; set; }
        public virtual ICollection<StatutoryTmp> StatutoryTmp { get; set; }
    }
}