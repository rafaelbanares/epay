﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("StatutoryPosted")]
    public class StatutoryPosted : Entity
    {
        [Required]
        public virtual int PayrollPostedId { get; set; }

        [Required]
        public virtual int StatutorySubTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual StatutorySubTypes StatutorySubTypes { get; set; }
        public virtual PayrollPosted PayrollPosted { get; set; }
    }
}