﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("PayCodes")]
    public class PayCodes : AuditedEntity<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(PayCodesConsts.MaxDisplayNameLength, MinimumLength = PayCodesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(PayCodesConsts.MaxDescriptionLength, MinimumLength = PayCodesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        [StringLength(PayCodesConsts.MaxPayFreqLength, MinimumLength = PayCodesConsts.MinPayFreqLength)]
        public virtual string PayFreq { get; set; }

        [StringLength(PayCodesConsts.MaxTaxFreqLength, MinimumLength = PayCodesConsts.MinTaxFreqLength)]
        public virtual string TaxFreq { get; set; }

        [Required]
        [StringLength(PayCodesConsts.MaxTaxMethodLength, MinimumLength = PayCodesConsts.MinTaxMethodLength)]
        public virtual string TaxMethod { get; set; }

        [Required]
        public virtual decimal WorkingDays { get; set; }

        [Required]
        public virtual decimal HrsPerDay { get; set; }

        [Required]
        public virtual int PayPeriodPerYear { get; set; }

        public virtual int? DaysPerWeek { get; set; }

        [Required]
        public virtual bool RequireTimesheet { get; set; }

        [Required]
        public virtual bool IncBonTaxDiv { get; set; }

        [Required]
        public virtual bool TaxableOT { get; set; }

        public virtual decimal? Nd1Rate { get; set; }

        public virtual decimal? Nd2Rate { get; set; }

        [Required]
        public virtual bool MinimumWager { get; set; }

        public virtual ICollection<EmployeePayroll> EmployeePayroll { get; set; }
        public virtual ICollection<StatutorySettings> StatutorySettings { get; set; }
        public virtual ICollection<TaxMethodByProcessType> TaxMethodByProcessType { get; set; }
        public virtual ICollection<PayrollPosted> PayrollPosted { get; set; }
        public virtual ICollection<PayrollProcess> PayrollProcess { get; set; }
        public virtual ICollection<StatutoryTypes> StatutoryTypes { get; set; }
    }
}