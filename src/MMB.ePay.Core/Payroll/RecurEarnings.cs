﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("RecurEarnings")]
    public class RecurEarnings : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        [Required]
        [StringLength(RecurEarningsConsts.MaxFrequencyLength, MinimumLength = RecurEarningsConsts.MinFrequencyLength)]
        public virtual string Frequency { get; set; }

        [Required]
        public virtual DateTime StartDate { get; set; }

        [Required]
        public virtual DateTime EndDate { get; set; }

        public virtual EarningTypes EarningTypes { get; set; }
        public virtual ICollection<EarningPosted> EarningPosted { get; set; }
    }
}