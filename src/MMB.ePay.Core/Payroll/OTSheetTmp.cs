﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("OTSheetTmp")]
    public class OTSheetTmp : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual Guid SessionId { get; set; }

        [Required]
        public virtual int OvertimeTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        public virtual OvertimeTypes OvertimeTypes { get; set; }
        public virtual PayrollProcess PayrollProcess { get; set; }
    }
}