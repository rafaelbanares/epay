﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("BankBranches")]
    public class BankBranches : AuditedEntity
    {
        [Required]
        [StringLength(BankBranchesConsts.MaxBankIdLength, MinimumLength = BankBranchesConsts.MinBankIdLength)]
        public virtual string BankId { get; set; }

        [StringLength(BankBranchesConsts.MaxBranchCodeLength, MinimumLength = BankBranchesConsts.MinBranchCodeLength)]
        public virtual string BranchCode { get; set; }

        [Required]
        [StringLength(BankBranchesConsts.MaxBranchNameLength, MinimumLength = BankBranchesConsts.MinBranchNameLength)]
        public virtual string BranchName { get; set; }

        [StringLength(BankBranchesConsts.MaxAddress1Length, MinimumLength = BankBranchesConsts.MinAddress1Length)]
        public virtual string Address1 { get; set; }

        [StringLength(BankBranchesConsts.MaxAddress2Length, MinimumLength = BankBranchesConsts.MinAddress2Length)]
        public virtual string Address2 { get; set; }

        public virtual Banks Banks { get; set; }

        public virtual ICollection<EmployeePayroll> EmployeePayroll { get; set; }
        public virtual ICollection<TenantBanks> TenantBanks { get; set; }
    }
}