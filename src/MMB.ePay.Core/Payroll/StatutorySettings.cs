﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("StatutorySettings")]
    public class StatutorySettings : CreationAuditedEntity
    {
        public int TenantId { get; set; }

        [Required]
        public Guid? PaycodeId { get; set; }

        [Required]
        [StringLength(StatutorySettingsConsts.MaxStatutoryTypeLength, MinimumLength = StatutorySettingsConsts.MinStatutoryTypeLength)]
        public string StatutoryType { get; set; }

        [Required]
        [StringLength(StatutorySettingsConsts.MaxBasisLength, MinimumLength = StatutorySettingsConsts.MinBasisLength)]
        public string Basis { get; set; }

        [Required]
        [StringLength(StatutorySettingsConsts.MaxFrequencyLength, MinimumLength = StatutorySettingsConsts.MinFrequencyLength)]
        public string Frequency { get; set; }

        public decimal BasisDays { get; set; }

        public PayCodes PayCodes { get; set; }
    }
}