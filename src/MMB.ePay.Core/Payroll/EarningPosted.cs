﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EarningPosted")]
    public class EarningPosted : Entity
    {
        [Required]
        public virtual int PayrollPostedId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollPosted PayrollPosted { get; set; }
        public virtual EarningTypes EarningTypes { get; set; }
    }
}