﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EmployeePayroll")]
    public class EmployeePayroll : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }


        [Required]
        public virtual Guid PayCodeId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        public virtual int? BankBranchId { get; set; }

        [Required]
        [StringLength(EmployeePayrollConsts.MaxPayrollTypeLength, MinimumLength = EmployeePayrollConsts.MinPayrollTypeLength)]
        public virtual string PayrollType { get; set; }

        [Required]
        public virtual decimal Salary { get; set; }

        [StringLength(EmployeePayrollConsts.MaxPagibigNumberLength, MinimumLength = EmployeePayrollConsts.MinPagibigNumberLength)]
        public virtual string PagibigNumber { get; set; }

        public virtual decimal? PagibigAdd { get; set; }

        public virtual decimal? PagibigRate { get; set; }

        [Required]
        [StringLength(EmployeePayrollConsts.MaxSSSNumberLength, MinimumLength = EmployeePayrollConsts.MinSSSNumberLength)]
        public virtual string SSSNumber { get; set; }

        [StringLength(EmployeePayrollConsts.MaxPhilhealthNumberLength, MinimumLength = EmployeePayrollConsts.MinPhilhealthNumberLength)]
        public virtual string PhilhealthNumber { get; set; }

        [Required]
        [StringLength(EmployeePayrollConsts.MaxPaymentTypeLength, MinimumLength = EmployeePayrollConsts.MinPaymentTypeLength)]
        public virtual string PaymentType { get; set; }

        [StringLength(EmployeePayrollConsts.MaxBankAccountNoLength, MinimumLength = EmployeePayrollConsts.MinBankAccountNoLength)]
        public virtual string BankAccountNo { get; set; }

        [StringLength(EmployeePayrollConsts.MaxTINLength, MinimumLength = EmployeePayrollConsts.MinTINLength)]
        public virtual string TIN { get; set; }

        public virtual decimal? TaxRate { get; set; }

        public virtual int? RankId { get; set; }

        [Required]
        public virtual bool OT_exempt { get; set; }

        [Required]
        public virtual bool SSS_exempt { get; set; }

        [Required]
        public virtual bool PH_exempt { get; set; }

        [Required]
        public virtual bool PAG_exempt { get; set; }

        [Required]
        public virtual bool Tax_exempt { get; set; }

        [Required]
        public virtual bool ComputeAsDaily { get; set; }

        public virtual BankBranches BankBranches { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual PayCodes PayCodes { get; set; }
        public virtual Ranks Ranks { get; set; }
        public virtual ICollection<Employers> Employers { get; set; }
    }
}