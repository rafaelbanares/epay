﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("ProratedEarnings")]
    public class ProratedEarnings : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        public virtual int? InTimesheetTypeId { get; set; }

        public virtual int? InOvertimeTypeId { get; set; }

        [Required]
        public virtual int OutEarningTypeId { get; set; }

        public virtual EarningTypes EarningTypes { get; set; }
        public virtual TimesheetTypes TimesheetTypes { get; set; }
        public virtual OvertimeTypes OvertimeTypes { get; set; }
        public virtual EarningTypes OutEarningTypes { get; set; }
    }
}