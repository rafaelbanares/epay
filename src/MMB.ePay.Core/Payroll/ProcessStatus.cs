﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("ProcessStatus")]
    public class ProcessStatus : AuditedEntity<int>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ProcessStatusConsts.MaxDisplayNameLength, MinimumLength = ProcessStatusConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(ProcessStatusConsts.MaxDescriptionLength, MinimumLength = ProcessStatusConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        [StringLength(ProcessStatusConsts.MaxSysCodeLength, MinimumLength = ProcessStatusConsts.MinSysCodeLength)]
        public virtual string SysCode { get; set; }

        [Required]
        public virtual int Step { get; set; }

        public virtual int? NextStatus { get; set; }

        [Required]
        [StringLength(ProcessStatusConsts.MaxResponsibleLength, MinimumLength = ProcessStatusConsts.MinResponsibleLength)]
        public virtual string Responsible { get; set; }

        public virtual ProcessStatus NextStatusNavigation { get; set; }
        public virtual ICollection<ProcessStatus> InverseProcessStatusNavigation { get; set; }
        public virtual ICollection<Process> Process { get; set; }
        public virtual ICollection<ProcessHistory> ProcessHistory { get; set; }
    }
}