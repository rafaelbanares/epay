﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("SssTables")]
    public class SssTables : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int MonthlySalCredit { get; set; }

        [Required]
        public virtual decimal LB { get; set; }

        [Required]
        public virtual decimal UB { get; set; }

        [Required]
        public virtual decimal EmployerSSS { get; set; }

        [Required]
        public virtual decimal EmployerECC { get; set; }

        [Required]
        public virtual decimal EmployeeSSS { get; set; }

    }
}