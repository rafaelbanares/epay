﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Loans")]
    public class Loans : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int LoanTypeId { get; set; }

        [StringLength(LoansConsts.MaxRefCodeLength, MinimumLength = LoansConsts.MinRefCodeLength)]
        public virtual string RefCode { get; set; }

        [StringLength(LoansConsts.MaxDisplayNameLength, MinimumLength = LoansConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(LoansConsts.MaxDescriptionLength, MinimumLength = LoansConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [StringLength(LoansConsts.MaxCurrencyLength, MinimumLength = LoansConsts.MinCurrencyLength)]
        public virtual string Currency { get; set; }

        [StringLength(LoansConsts.MaxAcctCodeLength, MinimumLength = LoansConsts.MinAcctCodeLength)]
        public virtual string AcctCode { get; set; }

        public virtual LoanTypes LoanTypes { get; set; }
        public virtual ICollection<EmployeeLoans> EmployeeLoans { get; set; }
    }
}