﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("LeaveTypes")]
    public class LeaveTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(LeaveTypesConsts.MaxDisplayNameLength, MinimumLength = LeaveTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(LeaveTypesConsts.MaxDescriptionLength, MinimumLength = LeaveTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual bool WithPay { get; set; }

        public virtual ICollection<LeaveCredits> LeaveCredits { get; set; }
    }
}