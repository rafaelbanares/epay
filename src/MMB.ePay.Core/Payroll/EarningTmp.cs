﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EarningTmp")]
    public class EarningTmp : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        [Required]
        public virtual Guid SessionId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollProcess PayrollProcess { get; set; }
    }
}