﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("TaxMethodByProcessType")]
    public class TaxMethodByProcessType : CreationAuditedEntity
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(TaxMethodByProcessTypeConsts.MaxProcessTypeLength, MinimumLength = TaxMethodByProcessTypeConsts.MinProcessTypeLength)]
        public string ProcessType { get; set; }

        [Required]
        public Guid? PaycodeId { get; set; }

        [Required]
        [StringLength(TaxMethodByProcessTypeConsts.MaxTaxMethodLength, MinimumLength = TaxMethodByProcessTypeConsts.MinTaxMethodLength)]
        public string TaxMethod { get; set; }

        public virtual PayCodes PayCodes { get; set; }
    }
}