﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Process")]
    public class Process : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int PayrollNo { get; set; }

        [Required]
        public virtual int ProcessStatusId { get; set; }

        [Required]
        public virtual int OwnerId { get; set; }

        public virtual PayPeriod PayPeriod { get; set; }
        public virtual ProcessStatus ProcessStatus { get; set; }
        public virtual Employees Owner { get; set; }

        public virtual ICollection<Timesheets> Timesheets { get; set; }
        public virtual ICollection<OTSheet> OTSheet { get; set; }
        public virtual ICollection<Earnings> Earnings { get; set; }
        public virtual ICollection<Deductions> Deductions { get; set; }
        public virtual ICollection<ProcessHistory> ProcessHistory { get; set; }
    }
}