﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("OTSheetProcess")]
    public class OTSheetProcess : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual int OvertimeTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollProcess PayrollProcess { get; set; }
        public virtual OvertimeTypes OvertimeTypes { get; set; }
    }
}