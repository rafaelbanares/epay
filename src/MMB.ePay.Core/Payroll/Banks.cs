﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Banks")]
    public class Banks : AuditedEntity<string>
    {
        [Key]
        [StringLength(BanksConsts.MaxIdLength, MinimumLength = BanksConsts.MinIdLength)]
        public override string Id { get; set; }

        [Required]
        [StringLength(BanksConsts.MaxDisplayNameLength, MinimumLength = BanksConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(BanksConsts.MaxDescriptionLength, MinimumLength = BanksConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<BankBranches> BankBranches { get; set; }
    }
}