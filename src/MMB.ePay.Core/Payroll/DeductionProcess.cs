﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("DeductionProcess")]
    public class DeductionProcess : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual int DeductionTypeId { get; set; }

        [Required]
        public virtual int RecurDeductionId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollProcess PayrollProcess { get; set; }
        public virtual DeductionTypes DeductionTypes { get; set; }
    }
}