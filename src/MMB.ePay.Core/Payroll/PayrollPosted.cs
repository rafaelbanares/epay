﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("PayrollPosted")]
    public class PayrollPosted : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public Guid PayCodeId { get; set; }

        [Required]
        public int BankBranchId { get; set; }

        [Required]
        public virtual int PayrollNo { get; set; }

        [Required]
        public virtual decimal Salary { get; set; }

        [Required]
        public virtual decimal DailyRate { get; set; }

        [Required]
        public virtual decimal HourlyRate { get; set; }

        [Required]
        public virtual decimal Timesheet { get; set; }

        [Required]
        public virtual decimal Overtime { get; set; }

        [Required]
        public virtual decimal TxEarningAmt { get; set; }

        [Required]
        public virtual decimal NtxEarningAmt { get; set; }

        [Required]
        public virtual decimal DedTaxAmt { get; set; }

        [Required]
        public virtual decimal DeductionAmt { get; set; }

        [Required]
        public virtual decimal LoanAmt { get; set; }

        [Required]
        public virtual decimal BasicPay { get; set; }

        [Required]
        public virtual decimal GrossPay { get; set; }

        [Required]
        public virtual decimal NetPay { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual PayCodes PayCodes { get; set; }
        public virtual PayPeriod PayPeriod { get; set; }

        public virtual ICollection<DeductionPosted> DeductionPosted { get; set; }
        public virtual ICollection<EarningPosted> EarningPosted { get; set; }
        public virtual ICollection<LoanPosted> LoanPosted { get; set; }
        public virtual ICollection<OTSheetPosted> OTSheetPosted { get; set; }
        public virtual ICollection<StatutoryPosted> StatutoryPosted { get; set; }
        public virtual ICollection<TimesheetPosted> TimesheetPosted { get; set; }
    }
}