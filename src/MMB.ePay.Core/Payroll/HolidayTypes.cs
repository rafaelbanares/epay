﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("HolidayTypes")]
    public class HolidayTypes : Entity<string>
    {
        [Key]
        [StringLength(HolidayTypesConsts.MaxIdLength, MinimumLength = HolidayTypesConsts.MinIdLength)]
        public override string Id { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxDisplayNameLength, MinimumLength = HolidayTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxDescriptionLength, MinimumLength = HolidayTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual bool HalfDayEnabled { get; set; }

        public virtual ICollection<Holidays> Holidays { get; set; }
    }
}