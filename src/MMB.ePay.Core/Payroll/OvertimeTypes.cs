﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("OvertimeTypes")]
    public class OvertimeTypes : AuditedEntity
    {
        public virtual int OvertimeMainTypeId { get; set; }

        public virtual int OvertimeSubTypeId { get; set; }

        [Required]
        [StringLength(OvertimeTypesConsts.MaxDisplayNameLength, MinimumLength = OvertimeTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(OvertimeTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual decimal Rate { get; set; }

        public virtual OvertimeMainTypes OvertimeMainTypes { get; set; }
        public virtual OvertimeSubTypes OvertimeSubTypes { get; set; }

        public virtual ICollection<OTSheet> OTSheet { get; set; }
        public virtual ICollection<OTSheetPosted> OTSheetPosted { get; set; }
        public virtual ICollection<OTSheetProcess> OTSheetProcess { get; set; }
        public virtual ICollection<OTSheetTmp> OTSheetTmp { get; set; }
        public virtual ICollection<ProratedEarnings> ProratedEarnings { get; set; }
    }
}