﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("StatutoryTmp")]
    public class StatutoryTmp : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual Guid SessionId { get; set; }

        [Required]
        public virtual int StatutorySubTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual StatutorySubTypes StatutorySubTypes { get; set; }
        public virtual PayrollProcess PayrollProcess { get; set; }
    }
}