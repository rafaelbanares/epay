﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("LoanPayments")]
    public class LoanPayments : Entity
    {
        [Required]
        public virtual Guid EmployeeLoanId { get; set; }

        [Required]
        public virtual int PayrollNo { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual string Remarks { get; set; }

        public virtual EmployeeLoans EmployeeLoans { get; set; }
        public virtual PayPeriod PayPeriod { get; set; }
    }
}