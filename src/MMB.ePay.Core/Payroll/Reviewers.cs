﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Reviewers")]
    public class Reviewers : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ReviewersConsts.MaxReviewerTypeLength, MinimumLength = ReviewersConsts.MinReviewerTypeLength)]
        public virtual string ReviewerType { get; set; }

        [Required]
        public virtual int ReviewedBy { get; set; }

        public virtual Employees Employees { get; set; }
    }
}