﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("LeaveCredits")]
    public class LeaveCredits : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int LeaveTypeId { get; set; }

        [Required]
        public virtual int AppYear { get; set; }

        [Required]
        public virtual decimal LeaveDays { get; set; }

        [StringLength(LeaveCreditsConsts.MaxRemarksLength, MinimumLength = LeaveCreditsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual LeaveTypes LeaveTypes { get; set; }
    }
}