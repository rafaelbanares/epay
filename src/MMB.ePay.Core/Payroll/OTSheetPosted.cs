﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("OTSheetPosted")]
    public class OTSheetPosted : Entity
    {
        [Required]
        public virtual int PayrollPostedId { get; set; }

        [Required]
        public virtual int OvertimeTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual OvertimeTypes OvertimeTypes { get; set; }
        public virtual PayrollPosted PayrollPosted { get; set; }
    }
}