﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("LoanTypes")]
    public class LoanTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(LoanTypesConsts.MaxSysCodeLength, MinimumLength = LoanTypesConsts.MinSysCodeLength)]
        public virtual string SysCode { get; set; }

        [Required]
        [StringLength(LoanTypesConsts.MaxDescriptionLength, MinimumLength = LoanTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<Loans> Loans { get; set; }
    }
}