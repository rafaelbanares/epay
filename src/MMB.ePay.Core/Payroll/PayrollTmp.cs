﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("PayrollTmp")]
    public class PayrollTmp : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int PayCodeId { get; set; }

        [Required]
        public virtual int BankBranchId { get; set; }

        [Required]
        public virtual Guid SessionId { get; set; }

        [Required]
        public virtual int PayrollNo { get; set; }

        [Required]
        public virtual decimal Salary { get; set; }

        [Required]
        public virtual decimal DailyRate { get; set; }

        [Required]
        public virtual decimal HourlyRate { get; set; }

        [Required]
        public virtual decimal Timesheet { get; set; }

        [Required]
        public virtual decimal Overtime { get; set; }

        [Required]
        public virtual decimal TxEarningAmt { get; set; }

        [Required]
        public virtual decimal NtxEarningAmt { get; set; }

        [Required]
        public virtual decimal DedTaxAmt { get; set; }

        [Required]
        public virtual decimal DeductionAmt { get; set; }

        [Required]
        public virtual decimal LoanAmt { get; set; }

        [Required]
        public virtual decimal BasicPay { get; set; }

        [Required]
        public virtual decimal GrossPay { get; set; }

        [Required]
        public virtual decimal NetPay { get; set; }

    }
}