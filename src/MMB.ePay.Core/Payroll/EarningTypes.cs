﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EarningTypes")]
    public class EarningTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(EarningTypesConsts.MaxDisplayNameLength, MinimumLength = EarningTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(EarningTypesConsts.MaxDescriptionLength, MinimumLength = EarningTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual bool Active { get; set; }

        [Required]
        public virtual bool Taxable { get; set; }

        [Required]
        [StringLength(EarningTypesConsts.MaxCurrencyLength, MinimumLength = EarningTypesConsts.MinCurrencyLength)]
        public virtual string Currency { get; set; }

        public virtual ICollection<EarningPosted> EarningPosted { get; set; }
        public virtual ICollection<EarningProcess> EarningProcess { get; set; }
        public virtual ICollection<Earnings> Earnings { get; set; }
        public virtual ICollection<ProratedEarnings> ProratedEarningsNavigation { get; set; }
        public virtual ICollection<ProratedEarnings> OutProratedEarningsNavigation { get; set; }
        public virtual ICollection<RecurEarnings> RecurEarnings { get; set; }
    }
}