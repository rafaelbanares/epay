﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("TimesheetPosted")]
    public class TimesheetPosted : Entity
    {
        [Required]
        public virtual int PayrollPostedId { get; set; }

        [Required]
        public virtual int TimesheetTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Days { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual TimesheetTypes TimesheetTypes { get; set; }
        public virtual PayrollPosted PayrollPosted { get; set; }
    }
}