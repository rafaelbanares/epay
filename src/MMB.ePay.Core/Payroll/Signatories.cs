﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Signatories")]
    public class Signatories : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(SignatoriesConsts.MaxSignatoryNameLength, MinimumLength = SignatoriesConsts.MinSignatoryNameLength)]
        public virtual string SignatoryName { get; set; }

        [Required]
        [StringLength(SignatoriesConsts.MaxPositionLength, MinimumLength = SignatoriesConsts.MinPositionLength)]
        public virtual string Position { get; set; }

        public virtual ICollection<CompanyStatutoryInfo> CompanyStatutoryInfo { get; set; }
        public virtual ICollection<ReportSignatories> ReportSignatories { get; set; }
    }
}