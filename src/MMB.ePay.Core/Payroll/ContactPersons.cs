﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("ContactPersons")]
    public class ContactPersons : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ContactPersonsConsts.MaxFullNameLength, MinimumLength = ContactPersonsConsts.MinFullNameLength)]
        public virtual string FullName { get; set; }

        [Required]
        [StringLength(ContactPersonsConsts.MaxContactNoLength, MinimumLength = ContactPersonsConsts.MinContactNoLength)]
        public virtual string ContactNo { get; set; }

        [Required]
        [StringLength(ContactPersonsConsts.MaxPositionLength, MinimumLength = ContactPersonsConsts.MinPositionLength)]
        public virtual string Position { get; set; }

        public short OrderNo { get; set; }
    }
}