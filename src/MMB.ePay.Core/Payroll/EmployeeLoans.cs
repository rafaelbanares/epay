﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EmployeeLoans")]
    public class EmployeeLoans : AuditedEntity<Guid>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public int LoanId { get; set; }

        [Required]
        public virtual DateTime ApprovedDate { get; set; }

        [Required]
        public virtual DateTime EffectStart { get; set; }

        [Required]
        public virtual decimal Principal { get; set; }

        [Required]
        public virtual decimal LoanAmount { get; set; }

        [Required]
        public virtual decimal Amortization { get; set; }

        [Required]
        [StringLength(EmployeeLoansConsts.MaxFrequencyLength, MinimumLength = EmployeeLoansConsts.MinFrequencyLength)]
        public virtual string Frequency { get; set; }

        [StringLength(EmployeeLoansConsts.MaxRemarksLength, MinimumLength = EmployeeLoansConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual DateTime? HoldStart { get; set; }

        public virtual DateTime? HoldEnd { get; set; }

        [Required]
        [StringLength(EmployeeLoansConsts.MaxStatusLength, MinimumLength = EmployeeLoansConsts.MinStatusLength)]
        public virtual string Status { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual Loans Loans { get; set; }

        public virtual ICollection<LoanPayments> LoanPayments { get; set; }
        public virtual ICollection<LoanPosted> LoanPosted { get; set; }
        public virtual ICollection<LoanProcess> LoanProcess { get; set; }
    }
}