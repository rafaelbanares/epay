﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Employers")]
    public class Employers : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        [StringLength(EmployersConsts.MaxEmployerNameLength, MinimumLength = EmployersConsts.MinEmployerNameLength)]
        public virtual string EmployerName { get; set; }

        [Required]
        public virtual DateTime EmployedFrom { get; set; }

        public virtual DateTime? EmployedTo { get; set; }

        [StringLength(EmployersConsts.MaxTINLength, MinimumLength = EmployersConsts.MinTINLength)]
        public virtual string TIN { get; set; }

        [StringLength(EmployersConsts.MaxAddress1Length, MinimumLength = EmployersConsts.MinAddress1Length)]
        public virtual string Address1 { get; set; }

        [StringLength(EmployersConsts.MaxAddress2Length, MinimumLength = EmployersConsts.MinAddress2Length)]
        public virtual string Address2 { get; set; }

        [Required]
        public virtual decimal BasicPay { get; set; }

        [Required]
        public virtual decimal Deminimis { get; set; }

        [Required]
        public virtual decimal GrossPay { get; set; }

        [Required]
        public virtual decimal TxBonus { get; set; }

        [Required]
        public virtual decimal TxOtherIncome { get; set; }

        [Required]
        public virtual decimal NtxBonus { get; set; }

        [Required]
        public virtual decimal NtxOtherIncome { get; set; }

        [Required]
        public virtual decimal WTax { get; set; }

        [Required]
        public virtual decimal SSS { get; set; }

        [Required]
        public virtual decimal Philhealth { get; set; }

        [Required]
        public virtual decimal Pagibig { get; set; }

        public virtual Employees Employees { get; set; }
    }
}