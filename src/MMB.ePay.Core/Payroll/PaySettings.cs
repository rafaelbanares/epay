﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("PaySettings")]
    public class PaySettings : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxKeyLength, MinimumLength = PaySettingsConsts.MinKeyLength)]
        public virtual string Key { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxValueLength, MinimumLength = PaySettingsConsts.MinValueLength)]
        public virtual string Value { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxDataTypeLength, MinimumLength = PaySettingsConsts.MinDataTypeLength)]
        public virtual string DataType { get; set; }

        [Required]
        [StringLength(PaySettingsConsts.MaxCaptionLength, MinimumLength = PaySettingsConsts.MinCaptionLength)]
        public virtual string Caption { get; set; }

        public virtual int? DisplayOrder { get; set; }

        [Required]
        public virtual int GroupSettingId { get; set; }

        public virtual GroupSettings GroupSettings { get; set; }
    }
}