﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EmployeeStatutoryInfo")]
    public class EmployeeStatutoryInfo : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int EmployeeId { get; set; }

        [Required]
        [StringLength(EmnployeeStatutoryInfoConsts.MaxStatutoryTypeLength, MinimumLength = EmnployeeStatutoryInfoConsts.MinStatutoryTypeLength)]
        public virtual string StatutoryType { get; set; }

        [Required]
        [StringLength(EmnployeeStatutoryInfoConsts.MaxStatutoryAcctNoLength, MinimumLength = EmnployeeStatutoryInfoConsts.MinStatutoryAcctNoLength)]
        public virtual string StatutoryAcctNo { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual StatutoryTypes StatutoryTypes { get; set; }
    }
}