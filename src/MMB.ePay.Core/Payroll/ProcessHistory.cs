﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("ProcessHistory")]
    public class ProcessHistory : CreationAuditedEntity
    {
        [Required]
        public virtual int ProcessId { get; set; }

        [Required]
        public virtual int ProcessStatusId { get; set; }

        [StringLength(ProcessHistoryConsts.MaxRemarksLength, MinimumLength = ProcessHistoryConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual Process Process { get; set; }
        public virtual ProcessStatus ProcessStatus { get; set; }
    }
}