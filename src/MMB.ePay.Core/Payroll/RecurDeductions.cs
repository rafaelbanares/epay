﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("RecurDeductions")]
    public class RecurDeductions : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int DeductionTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        [Required]
        [StringLength(RecurDeductionsConsts.MaxFrequencyLength, MinimumLength = RecurDeductionsConsts.MinFrequencyLength)]
        public virtual string Frequency { get; set; }

        [Required]
        public virtual DateTime StartDate { get; set; }

        [Required]
        public virtual DateTime EndDate { get; set; }

        public virtual DeductionTypes DeductionTypes { get; set; }
        public virtual ICollection<DeductionPosted> DeductionPosted { get; set; }
    }
}