﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("EarningTypeSettings")]
    public class EarningTypeSettings : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EarningTypeId { get; set; }

        [Required]
        [StringLength(EarningTypeSettingsConsts.MaxKeyLength, MinimumLength = EarningTypeSettingsConsts.MinKeyLength)]
        public virtual string Key { get; set; }

        [Required]
        [StringLength(EarningTypeSettingsConsts.MaxValueLength, MinimumLength = EarningTypeSettingsConsts.MinValueLength)]
        public virtual string Value { get; set; }

    }
}