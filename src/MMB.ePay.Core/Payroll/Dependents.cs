﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Dependents")]
    public class Dependents : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        [StringLength(DependentsConsts.MaxDependentNameLength, MinimumLength = DependentsConsts.MinDependentNameLength)]
        public virtual string DependentName  { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        public virtual Employees Employees { get; set; }
    }
}