﻿using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Timesheets")]
    public class Timesheets : AuditedEntity
    {
        [Required]
        public virtual int ProcessId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int TimesheetTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Days { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        public virtual Process Process { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual TimesheetTypes TimesheetTypes { get; set; }
    }
}