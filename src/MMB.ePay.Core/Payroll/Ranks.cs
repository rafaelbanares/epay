﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Ranks")]
    public class Ranks : AuditedEntity<int>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(RanksConsts.MaxDisplayNameLength, MinimumLength = RanksConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(RanksConsts.MaxDescriptionLength, MinimumLength = RanksConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        [StringLength(RanksConsts.MaxSysCodeLength, MinimumLength = RanksConsts.MinSysCodeLength)]
        public virtual string SysCode { get; set; }

        public virtual ICollection<EmployeePayroll> EmployeePayroll { get; set; }
    }
}