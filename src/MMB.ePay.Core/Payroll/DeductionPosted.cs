﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("DeductionPosted")]
    public class DeductionPosted : Entity
    {
        [Required]
        public virtual int PayrollPostedId { get; set; }

        [Required]
        public virtual int DeductionTypeId { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollPosted PayrollPosted { get; set; }
        public virtual DeductionTypes DeductionTypes { get; set; }
    }
}