﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("TimesheetProcess")]
    public class TimesheetProcess : Entity
    {
        [Required]
        public virtual int PayrollProcessId { get; set; }

        [Required]
        public virtual int TimesheetTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Days { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        public virtual PayrollProcess PayrollProcess { get; set; }
        public virtual TimesheetTypes TimesheetTypes { get; set; }
    }
}