﻿using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("OTSheet")]
    public class OTSheet : AuditedEntity
    {
        [Required]
        public virtual int ProcessId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int OvertimeTypeId { get; set; }

        public virtual int? CostCenter { get; set; }

        [Required]
        public virtual decimal Hrs { get; set; }

        [Required]
        public virtual int Mins { get; set; }

        public virtual Process Process { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual OvertimeTypes OvertimeTypes { get; set; }
    }
}