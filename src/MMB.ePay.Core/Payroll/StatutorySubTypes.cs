﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("StatutorySubTypes")]
    public class StatutorySubTypes : AuditedEntity
    {
        [Required]
        [StringLength(StatutorySubTypesConsts.MaxDisplayNameLength, MinimumLength = StatutorySubTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(StatutorySubTypesConsts.MaxSysCodeLength, MinimumLength = StatutorySubTypesConsts.MinSysCodeLength)]
        public virtual string SysCode { get; set; }

        [Required]
        [StringLength(StatutorySubTypesConsts.MaxDescriptionLength, MinimumLength = StatutorySubTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual bool EmployeeShare { get; set; }

        public virtual string StatutoryType { get; set; }

        public virtual StatutoryTypes StatutoryTypes { get; set; }

        public virtual ICollection<StatutoryPosted> StatutoryPosted { get; set; }
        public virtual ICollection<StatutoryProcess> StatutoryProcess { get; set; }
        public virtual ICollection<StatutoryTmp> StatutoryTmp { get; set; }
    }
}