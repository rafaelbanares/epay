﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("LoanPosted")]
    public class LoanPosted : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual Guid EmployeeLoanId { get; set; }

        public virtual int? PayrollNo { get; set; }

        [Required]
        public virtual DateTime PayDate { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

        [StringLength(LoanPostedConsts.MaxRemarksLength, MinimumLength = LoanPostedConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [Required]
        public virtual int CreatedBy { get; set; }

        [Required]
        public virtual DateTime CreatedDate { get; set; }

        public virtual EmployeeLoans EmployeeLoans { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual PayPeriod PayPeriod { get; set; }
        public virtual PayrollPosted PayrollPosted { get; set; }
    }
}