﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Reports")]
    public class Reports : Entity<int>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxDisplayNameLength, MinimumLength = ReportsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxControllerNameLength, MinimumLength = ReportsConsts.MinControllerNameLength)]
        public virtual string ControllerName { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxReportTypeIdLength, MinimumLength = ReportsConsts.MinReportTypeIdLength)]
        public virtual string ReportTypeId { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxFormatLength, MinimumLength = ReportsConsts.MinFormatLength)]
        public virtual string Format { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxOutputLength, MinimumLength = ReportsConsts.MinOutputLength)]
        public virtual string Output { get; set; }

        public virtual bool IsLandscape { get; set; }

        public virtual bool Enabled { get; set; }

        public virtual ICollection<ReportSignatories> ReportSignatories { get; set; }
    }
}