﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("OvertimeSubTypes")]
    public class OvertimeSubTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(OvertimeSubTypesConsts.MaxDisplayNameLength, MinimumLength = OvertimeSubTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(OvertimeSubTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeSubTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual int DisplayOrder { get; set; }

        public virtual ICollection<OvertimeTypes> OvertimeTypes { get; set; }
    }
}