﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("StatutoryTypes")]
    public class StatutoryTypes : CreationAuditedEntity
    {
        [Required]
        [StringLength(StatutoryTypesConsts.MaxCodeLength, MinimumLength = StatutoryTypesConsts.MinCodeLength)]
        public virtual string Code { get; set; }

        [Required]
        [StringLength(StatutoryTypesConsts.MaxDescriptionLength, MinimumLength = StatutoryTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<CompanyStatutoryInfo> CompanyStatutoryInfo { get; set; }
        public virtual ICollection<EmployeeStatutoryInfo> EmployeeStatutoryInfo { get; set; }
        public virtual ICollection<StatutorySubTypes> StatutorySubTypes { get; set; }
    }
}