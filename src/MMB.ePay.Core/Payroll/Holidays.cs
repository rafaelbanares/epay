﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("Holidays")]
    public class Holidays : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(HolidaysConsts.MaxDisplayNameLength, MinimumLength = HolidaysConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        [Required]
        public virtual bool HalfDay { get; set; }

        [Required]
        [StringLength(HolidaysConsts.MaxHolidayTypeIdLength, MinimumLength = HolidaysConsts.MinHolidayTypeIdLength)]
        public virtual string HolidayTypeId { get; set; }

        public virtual HolidayTypes HolidayTypes { get; set; }
    }
}