﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Payroll
{
    [Table("LoanTmp")]
    public class LoanTmp : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual int EmployeeLoanId { get; set; }

        [Required]
        public virtual Guid SessionId { get; set; }

        [Required]
        public virtual int PayrollNo { get; set; }

        [Required]
        public virtual decimal Amount { get; set; }

    }
}