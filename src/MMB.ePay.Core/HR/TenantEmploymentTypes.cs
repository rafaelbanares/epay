﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("TenantEmploymentTypes")]
    public class TenantEmploymentTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(TenantEmploymentTypesConsts.MaxEmploymentTypeIdLength, MinimumLength = TenantEmploymentTypesConsts.MinEmploymentTypeIdLength)]
        public virtual string EmploymentTypeId { get; set; }

        public virtual EmploymentTypes EmploymentTypes { get; set; }
    }
}