﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.Authorization.Users;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("Employees")]
    public class Employees : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmployeeCodeLength, MinimumLength = EmployeesConsts.MinEmployeeCodeLength)]
        public virtual string EmployeeCode { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxLastNameLength, MinimumLength = EmployeesConsts.MinLastNameLength)]
        public virtual string LastName { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxFirstNameLength, MinimumLength = EmployeesConsts.MinFirstNameLength)]
        public virtual string FirstName { get; set; }

        [StringLength(EmployeesConsts.MaxMiddleNameLength, MinimumLength = EmployeesConsts.MinMiddleNameLength)]
        public virtual string MiddleName { get; set; }

        [StringLength(EmployeesConsts.MaxEmailLength, MinimumLength = EmployeesConsts.MinEmailLength)]
        public virtual string Email { get; set; }

        [StringLength(EmployeesConsts.MaxAddressLength, MinimumLength = EmployeesConsts.MinAddressLength)]
        public virtual string Address { get; set; }

        [StringLength(EmployeesConsts.MaxPositionLength, MinimumLength = EmployeesConsts.MinPositionLength)]
        public virtual string Position { get; set; }

        public virtual DateTime? DateOfBirth { get; set; }

        public virtual DateTime? DateEmployed { get; set; }

        public virtual DateTime? DateTerminated { get; set; }

        [StringLength(EmployeesConsts.MaxGenderLength, MinimumLength = EmployeesConsts.MinGenderLength)]
        public virtual string Gender { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmployeeStatusLength, MinimumLength = EmployeesConsts.MinEmployeeStatusLength)]
        public virtual string EmployeeStatus { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmploymentTypeIdLength, MinimumLength = EmployeesConsts.MinEmploymentTypeIdLength)]
        public virtual string EmploymentTypeId { get; set; }

        public virtual string FullName
        {
            get { return string.Format("{0}, {1} {2}", LastName, FirstName, MiddleName).TrimEnd(); }
        }

        public virtual EmploymentTypes EmploymentTypes { get; set; }
        public virtual User User { get; set; }
        public virtual Tenant Tenant { get; set; }

        public virtual ICollection<EmployeeLoans> EmployeeLoans { get; set; }
        public virtual ICollection<EmployeePayroll> EmployeePayroll { get; set; }
        public virtual ICollection<Employers> Employers { get; set; }
        public virtual ICollection<EmployeeStatutoryInfo> EmployeeStatutoryInfo { get; set; }
        public virtual ICollection<Dependents> Dependents { get; set; }
        public virtual ICollection<LoanPosted> LoanPosted { get; set; }
        public virtual ICollection<PayrollPosted> PayrollPosted { get; set; }
        public virtual ICollection<PayrollProcess> PayrollProcess { get; set; }
        public virtual ICollection<Process> Process { get; set; }
        public virtual ICollection<ProcSessionBatches> ProcSessionBatches { get; set; }
        public virtual ICollection<Reviewers> Reviewers { get; set; }
    }
}