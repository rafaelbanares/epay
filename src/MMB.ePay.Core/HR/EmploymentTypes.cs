﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("EmploymentTypes")]
    public class EmploymentTypes : AuditedEntity<string>
    {
        [Key]
        [StringLength(EmploymentTypesConsts.MaxIdLength, MinimumLength = EmploymentTypesConsts.MinIdLength)]
        public override string Id { get; set; }

        [Required]
        [StringLength(EmploymentTypesConsts.MaxDisplayNameLength, MinimumLength = EmploymentTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(EmploymentTypesConsts.MaxDescriptionLength, MinimumLength = EmploymentTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<Employees> Employees { get; set; }
        public virtual ICollection<TenantEmploymentTypes> TenantEmploymentTypes { get; set; }
    }
}