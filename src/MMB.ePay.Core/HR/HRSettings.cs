﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("HRSettings")]
    public class HRSettings : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxKeyLength, MinimumLength = HRSettingsConsts.MinKeyLength)]
        public virtual string Key { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxValueLength, MinimumLength = HRSettingsConsts.MinValueLength)]
        public virtual string Value { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxDataTypeLength, MinimumLength = HRSettingsConsts.MinDataTypeLength)]
        public virtual string DataType { get; set; }

        [Required]
        [StringLength(HRSettingsConsts.MaxCaptionLength, MinimumLength = HRSettingsConsts.MinCaptionLength)]
        public virtual string Caption { get; set; }

        [StringLength(HRSettingsConsts.MaxDescriptionLength, MinimumLength = HRSettingsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual int? DisplayOrder { get; set; }

        [Required]
        public virtual int GroupSettingId { get; set; }

        public virtual GroupSettings GroupSettings { get; set; }
    }
}