﻿using System.Threading.Tasks;

namespace MMB.ePay.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
