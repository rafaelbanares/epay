﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MMB.ePay.Migrations
{
    public partial class Update_ProratedEarnings2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "InTimesheetId",
                schema: "pay",
                table: "ProratedEarnings",
                newName: "InTimesheetTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_ProratedEarnings_InTimesheetId",
                schema: "pay",
                table: "ProratedEarnings",
                newName: "IX_ProratedEarnings_InTimesheetTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "InTimesheetTypeId",
                schema: "pay",
                table: "ProratedEarnings",
                newName: "InTimesheetId");

            migrationBuilder.RenameIndex(
                name: "IX_ProratedEarnings_InTimesheetTypeId",
                schema: "pay",
                table: "ProratedEarnings",
                newName: "IX_ProratedEarnings_InTimesheetId");
        }
    }
}
