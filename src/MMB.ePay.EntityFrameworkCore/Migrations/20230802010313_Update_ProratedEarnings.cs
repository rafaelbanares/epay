﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MMB.ePay.Migrations
{
    public partial class Update_ProratedEarnings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                schema: "pay",
                table: "ProratedEarnings",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                schema: "pay",
                table: "ProratedEarnings",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProratedEarnings_TenantId_EarningTypeId",
                schema: "pay",
                table: "ProratedEarnings",
                columns: new[] { "TenantId", "EarningTypeId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProratedEarnings_TenantId_EarningTypeId",
                schema: "pay",
                table: "ProratedEarnings");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                schema: "pay",
                table: "ProratedEarnings");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                schema: "pay",
                table: "ProratedEarnings");
        }
    }
}
