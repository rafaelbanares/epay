﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialHolidaysCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialHolidaysCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var holidayTypes = new List<Tuple<string, string, bool>>
            {
                new Tuple<string, string, bool> ("C", "Company Holiday", true),
                new Tuple<string, string, bool> ("D", "Double Legal", false),
                new Tuple<string, string, bool> ("L", "Legal", false),
                new Tuple<string, string, bool> ("LS", "Legal & Special", false),
                new Tuple<string, string, bool> ("R", "Regional Holiday", false),
                new Tuple<string, string, bool> ("S", "Special", false)
            };

            foreach(var holidayType in holidayTypes)
            {
                if (!_context.HolidayTypes.Any(e => e.Id == holidayType.Item1))
                {
                    _context.HolidayTypes.Add(new HolidayTypes
                    {
                        Id = holidayType.Item1,
                        DisplayName = holidayType.Item2,
                        Description = holidayType.Item2,
                        HalfDayEnabled = holidayType.Item3
                    });
                }
            }
            _context.SaveChanges();

            var holidays = new List<Holidays>
            {
                new Holidays { TenantId = _tenantId, DisplayName = "New Year's Day", Date = new DateTime(DateTime.Today.Year, 1, 1), HolidayTypeId = "L" },
                new Holidays { TenantId = _tenantId, DisplayName = "Christmas Eve", Date = new DateTime(DateTime.Today.Year, 12, 24), HolidayTypeId = "S" },
                new Holidays { TenantId = _tenantId, DisplayName = "Christmas Day", Date = new DateTime(DateTime.Today.Year, 12, 25), HolidayTypeId = "L" }
            };

            foreach(var holiday in holidays)
            {
                if (!_context.Holidays.Any(e => e.TenantId == _tenantId && e.DisplayName == holiday.DisplayName && e.Date == holiday.Date))
                {
                    _context.Holidays.Add(holiday);
                }
            }
            _context.SaveChanges();

        }
    }
}
