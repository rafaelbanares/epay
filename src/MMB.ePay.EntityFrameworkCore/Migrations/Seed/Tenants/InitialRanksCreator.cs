﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialRanksCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialRanksCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var ranks = new List<string>
            {
                "Executive",
                "Manager",
                "Rank & File",
                "Supervisor"
            };

            foreach(var rank in ranks)
            {
                var sysCode = rank.Substring(0, 1);

                if (!_context.Ranks.Any(e => e.TenantId == _tenantId && e.SysCode == sysCode))
                {
                    _context.Ranks.Add(new Ranks
                    {
                        TenantId = _tenantId,
                        DisplayName = rank,
                        Description = rank,
                        SysCode = sysCode
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
