﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEmploymentTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmploymentTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employmentTypes = new List<Tuple<string, string>>
            {
                new Tuple<string, string> ("C", "Contractual"),
                new Tuple<string, string> ("P", "Probationary"),
                new Tuple<string, string> ("R", "Regular"),
                new Tuple<string, string> ("S", "Consultant")
            };

            foreach(var employmentType in employmentTypes)
            {
                if (!_context.EmploymentTypes.Any(e => e.Id == employmentType.Item1))
                {
                    var input = new EmploymentTypes
                    {
                        Id = employmentType.Item1,
                        DisplayName = employmentType.Item2,
                        Description = employmentType.Item2
                    };

                    _context.EmploymentTypes.Add(input);
                    _context.SaveChanges();
                }

                if (!_context.TenantEmploymentTypes.Any(e => e.EmploymentTypeId == employmentType.Item1))
                {
                    _context.TenantEmploymentTypes.Add(new TenantEmploymentTypes
                    {
                        TenantId = _tenantId,
                        EmploymentTypeId = employmentType.Item1
                    });
                    _context.SaveChanges();
                }
            }
        }
    }
}
