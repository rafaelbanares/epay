﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialContactPersonsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialContactPersonsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.ContactPersons.Any(e => e.TenantId == _tenantId && e.FullName == "Rafael F. Bañares"))
            {
                _context.ContactPersons.Add(new ContactPersons
                {
                    TenantId = _tenantId,
                    FullName = "Rafael F. Bañares",
                    Position = ".Net Developer",
                    ContactNo = "+639771459827",
                    OrderNo = 1
                });;

                _context.SaveChanges();
            }
        }
    }
}
