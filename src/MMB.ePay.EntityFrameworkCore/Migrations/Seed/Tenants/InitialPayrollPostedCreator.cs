﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialPayrollPostedCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialPayrollPostedCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employeeIds = _context.Employees.Where(e => e.TenantId == _tenantId).Select(e => e.Id).ToList();
            var months = Enumerable.Range(1, 12).ToList();
            var payCodeId = _context.PayCodes.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var bankBranchId = _context.BankBranches.Where(e => e.TenantBanks.Any(f => f.TenantId == _tenantId)).Select(e => e.Id).First();

            foreach (var employeeId in employeeIds)
            {
                if (!_context.PayrollPosted.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    foreach (var month in months)
                    {
                        var payPeriodId = _context.PayPeriod.Where(e => e.TenantId == _tenantId && e.ApplicableMonth == month).Select(e => e.Id).First();

                        _context.PayrollPosted.Add(new PayrollPosted
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            PayCodeId = payCodeId,
                            BankBranchId = bankBranchId,
                            PayrollNo = payPeriodId,
                            Salary = 1 * month,
                            DailyRate = 2 * month,
                            HourlyRate = 3 * month,
                            Timesheet = 4 * month,
                            Overtime = 5 * month,
                            TxEarningAmt = 6 * month,
                            NtxEarningAmt = 7 * month,
                            DedTaxAmt = 8 * month,
                            LoanAmt = 9 * month,
                            BasicPay = 10 * month,
                            GrossPay = 11 * month,
                            NetPay = 12 * month
                        });
                    };
                }
            }

            _context.SaveChanges();
        }
    }
}
