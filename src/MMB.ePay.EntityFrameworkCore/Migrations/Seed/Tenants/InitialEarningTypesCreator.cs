﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEarningTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEarningTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var earningTypes = new List<Tuple<string, bool>>
            {
                new Tuple<string, bool> ("ADVANCE234", true),
                new Tuple<string, bool> ("ALLOW-TX", true),
                new Tuple<string, bool> ("ALLOW_TX", true),
                new Tuple<string, bool> ("APLE LOAN", false),
                new Tuple<string, bool> ("BASIC PAY", true),
                new Tuple<string, bool> ("BONUS TX", true),
                new Tuple<string, bool> ("CAR INSURANCE", false),
                new Tuple<string, bool> ("CAR PLAN", false),
                new Tuple<string, bool> ("CAR SUB RETRO", false),
                new Tuple<string, bool> ("CAR SUBSIDY", false),
            };

            foreach (var earningType in earningTypes)
            {
                if (!_context.EarningTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == earningType.Item1))
                {
                    _context.EarningTypes.Add(new EarningTypes
                    {
                        TenantId = _tenantId,
                        DisplayName = earningType.Item1,
                        Description = earningType.Item1,
                        Active = true,
                        Taxable = earningType.Item2,
                        Currency = "PHP"
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
