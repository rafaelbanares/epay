﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialTaxTablesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialTaxTablesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var currentYear = DateTime.Today.Year;

            var taxTables = new List<TaxTables>
            {
                new TaxTables { PayFreq = "S", Code = "SAL" }, 
                new TaxTables { PayFreq = "S", Code = "FA" }, 
                new TaxTables { PayFreq = "W", Code = "SAL" }
            };

            foreach(var taxTable in taxTables)
            {
                if (!_context.TaxTables.Any(e => e.TenantId == _tenantId && e.PayFreq == taxTable.PayFreq && e.Code == taxTable.Code))
                {
                    taxTable.TenantId = _tenantId;
                    taxTable.Year = currentYear;
                    taxTable.Bracket1 = 1;
                    taxTable.Bracket2 = 2;
                    taxTable.Bracket3 = 3;
                    taxTable.Bracket4 = 4;
                    taxTable.Bracket5 = 5;
                    taxTable.Bracket6 = 6;
                    taxTable.Bracket7 = 7;

                    _context.TaxTables.Add(taxTable);
                }
            }
        }
    }
}
