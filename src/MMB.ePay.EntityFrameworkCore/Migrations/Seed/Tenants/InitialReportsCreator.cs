﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialReportsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialReportsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var reports = new List<Reports>
            {
                //Periodic
                new Reports { DisplayName = "Bank Advise", ControllerName = "BankAdvise", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Cash Advise", ControllerName = "CashAdvise", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Cheque Advise", ControllerName = "ChequeAdvise", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Deduction Register", ControllerName = "DeductionRegister", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Earning Register", ControllerName = "EarningRegister", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Employee Payslip", ControllerName = "EmployeePayslip", Format = "Legal", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Gross and Tax",  ControllerName = "GrossAndTax", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Pag-ibig Contribution", ControllerName = "PagibigContribution", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Payroll Register", ControllerName = "PayrollRegister", Format = "Legal", ReportTypeId = "P", IsLandscape = true },
                new Reports { DisplayName = "Philhealth Contribution", ControllerName = "PhilhealthContribution", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "SSS Contribution", ControllerName = "SSSContribution", Format = "Letter", ReportTypeId = "P", IsLandscape = false },
                new Reports { DisplayName = "Timesheet Register", ControllerName = "TimesheetRegister", Format = "Letter", ReportTypeId = "P", IsLandscape = true },

                //Monthly
                new Reports { DisplayName = "Deduction Register", ControllerName = "DeductionRegister", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Earning Register", ControllerName = "EarningRegister", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Gross and Tax",  ControllerName = "GrossAndTax", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Loan Detail Register",  ControllerName = "LoanDetailRegister", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Overtime Register",  ControllerName = "OvertimeRegister", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Pag-ibig M1-1",  ControllerName = "PagibigM11", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Pag-ibig MRCRF",  ControllerName = "PagibigMRCRF", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Payroll Register", ControllerName = "PayrollRegister", Format = "Legal", ReportTypeId = "M", IsLandscape = true },
                new Reports { DisplayName = "Philhealth ER2",  ControllerName = "PhilhealthER2", Format = "Letter", ReportTypeId = "M", IsLandscape = true },
                new Reports { DisplayName = "Philhealth Contribution",  ControllerName = "PhilhealthContribution", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "Philhealth RF-1",  ControllerName = "PhilhealthRF1", Format = "Legal", ReportTypeId = "M", IsLandscape = true },
                new Reports { DisplayName = "SSS Employment",  ControllerName = "SSSEmployment", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "SSS ML-2",  ControllerName = "SSSML2", Format = "Letter", ReportTypeId = "M", IsLandscape = false },
                new Reports { DisplayName = "SSS Contribution",  ControllerName = "SSSContribution", Format = "Letter", ReportTypeId = "M", IsLandscape = false },

                //Quarterly
                new Reports { DisplayName = "SSS R-3",  ControllerName = "SSSR3", Format = "Letter", ReportTypeId = "Q", IsLandscape = true },

                //Annual
                new Reports { DisplayName = "Alphalist",  ControllerName = "Alphalist", Format = "Legal", ReportTypeId = "A", IsLandscape = true },
            };

            var signatoryId = _context.Signatories.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();

            foreach(var report in reports)
            {
                if (!_context.Reports.Any(e => e.TenantId == _tenantId && e.ControllerName == report.ControllerName && e.ReportTypeId == report.ReportTypeId))
                {
                    report.TenantId = _tenantId;
                    report.Output = "P";
                    report.Enabled = true;

                    _context.Reports.Add(report);
                    _context.SaveChanges();

                    _context.ReportSignatories.Add(new ReportSignatories
                    {
                        TenantId = _tenantId,
                        ReportId = report.Id,
                        SignatoryId = signatoryId
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
