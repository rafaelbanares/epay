﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialProcessCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialProcessCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var payrollPostedId = _context.PayrollPosted.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var processStatusId = _context.ProcessStatus.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var ownerId = _context.Employees.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var timesheetTypeIds = _context.TimesheetTypes.Select(e => e.Id).ToList();
            var months = Enumerable.Range(1, timesheetTypeIds.Count).ToList();

            foreach (var month in months)
            {
                var payPeriodId = _context.PayPeriod.Where(e => e.TenantId == _tenantId && e.ApplicableMonth == month).Select(e => e.Id).First();

                if (!_context.Process.Any(e => e.TenantId == _tenantId && e.PayrollNo == payPeriodId))
                {
                    _context.Process.Add(new Process
                    {
                        TenantId = _tenantId,
                        PayrollNo = payPeriodId,
                        ProcessStatusId = processStatusId,
                        OwnerId = ownerId
                    });

                    _context.SaveChanges();
                }
            };
        }

    }
}
