﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialTimesheetTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialTimesheetTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var timesheetypes = new List<TimesheetTypes>
            {
                new TimesheetTypes { DisplayName = "Reg", Description = "Regular", Multiplier = 1, SysCode = "REG" },
                new TimesheetTypes { DisplayName = "Abs", Description = "Absence", Multiplier = -1, SysCode = "ABS" },
                new TimesheetTypes { DisplayName = "Tardy", Description = "Tardy", Multiplier = -1, SysCode = "TARDY" },
                new TimesheetTypes { DisplayName = "UT", Description = "Undertime", Multiplier = -1, SysCode = "UT" },
                new TimesheetTypes { DisplayName = "ND1", Description = "Reg ND1", Multiplier = 1, SysCode = "REGND1" },
                new TimesheetTypes { DisplayName = "ND2", Description = "Reg ND2", Multiplier = 1, SysCode = "REGND2" },
                new TimesheetTypes { DisplayName = "Paid Hol.", Description = "Paid Holiday", Multiplier = 1, SysCode = "PAIDHOL" },
                new TimesheetTypes { DisplayName = "SL", Description = "Sick Leave", Multiplier = 1, SysCode = "SL" },
                new TimesheetTypes { DisplayName = "VL", Description = "Vacation Leave", Multiplier = 1, SysCode = "VL" }
            };

            for(var i = 0; i < timesheetypes.Count; i++)
            {
                if (!_context.TimesheetTypes.Any(e => e.TenantId == _tenantId && e.SysCode == timesheetypes[i].SysCode))
                {
                    timesheetypes[i].TenantId = _tenantId;
                    timesheetypes[i].Taxable = true;
                    timesheetypes[i].DisplayOrder = i + 1;
                    timesheetypes[i].Active = true;

                    _context.TimesheetTypes.Add(timesheetypes[i]);
                }
            }

            _context.SaveChanges();
        }
    }
}
