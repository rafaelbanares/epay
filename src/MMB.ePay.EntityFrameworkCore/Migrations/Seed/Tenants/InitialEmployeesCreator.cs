﻿using Abp.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEmployeesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmployeesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employees = new List<Employees>
            {
                new Employees
                {
                    EmployeeCode = "000001",
                    LastName = "Barnachea",
                    FirstName = "Rene",
                    MiddleName = "Sudo",
                    Email = "rene.barnachea@mmbisolutions.com",
                    Address = "Blk.221 Lot5 Metrogate Estates Silang Cavite",
                    Position = "President",
                    Gender = "M",
                    DateOfBirth = new DateTime(1974, 2, 24),
                    DateEmployed = null
                },
                new Employees
                {
                    EmployeeCode = "000002",
                    LastName = "Barnachea",
                    FirstName = "Remedios",
                    MiddleName = "Barnachea",
                    Email = "remie.barnachea@mmbisolutions.com",
                    Address = "Blk.221 Lot5 Metrogate Estates Silang Cavite",
                    Position = "Managing Director",
                    Gender = "F",
                    DateOfBirth = new DateTime(1975, 5, 25),
                    DateEmployed = null
                },
                new Employees
                {
                    EmployeeCode = "000012",
                    LastName = "Barnachea",
                    FirstName = "May",
                    MiddleName = "Villanueva",
                    Email = "may.villanueva@mmbisolutions.com",
                    Address = "#962-D Betina St. Brgy. 574 Sampaloc Manila",
                    Position = "HR/Admin Staff",
                    Gender = "F",
                    DateOfBirth = new DateTime(1979, 5, 28),
                    DateEmployed = new DateTime(2016, 4, 6)
                },
                new Employees
                {
                    EmployeeCode = "000013",
                    LastName = "Barnachea",
                    FirstName = "Rodel",
                    MiddleName = "Sudo",
                    Email = "rodel.barnachea@mmbisolutions.com",
                    Address = "#173 M.L.Q. St., Bagumbayan, Taguig City",
                    Position = "IT",
                    Gender = "M",
                    DateOfBirth = new DateTime(1971, 11, 28),
                    DateEmployed = new DateTime(2013, 6, 17)
                },
                new Employees
                {
                    EmployeeCode = "000014",
                    LastName = "Barnachea",
                    FirstName = "Rogelio",
                    MiddleName = "Belandres",
                    Email = "rogelio.barnachea@mmbisolutions.com",
                    Address = "Block 30 Lot 21 #855 Kabisig San Andres Cainta Rizal",
                    Position = "Messenger",
                    Gender = "M",
                    DateOfBirth = new DateTime(1977, 5, 18),
                    DateEmployed = new DateTime(2014, 10, 1)
                },
                new Employees
                {
                    EmployeeCode = "000017",
                    LastName = "Mendoza",
                    FirstName = "Alma Liza",
                    MiddleName = "Manalili",
                    Email = "alma.mendoza@mmbisolutions.com",
                    Address = "#1270-A  G. Apacible St. Paco, Manila",
                    Position = "Operations Head  ",
                    Gender = "F",
                    DateOfBirth = new DateTime(1980, 4, 19),
                    DateEmployed = new DateTime(2014, 9, 4)
                },
                new Employees
                {
                    EmployeeCode = "000022",
                    LastName = "Espiritu",
                    FirstName = "Joan",
                    MiddleName = "Pablo",
                    Email = "joan.espiritu@mmbisolutions.com",
                    Address = "#70 A.V Cruz St. Dolmar Sub. Kalawaan Pasig City",
                    Position = "Payroll Processor",
                    Gender = "F",
                    DateOfBirth = new DateTime(1990, 7, 15),
                    DateEmployed = new DateTime(2015, 2, 18)
                },
                new Employees
                {
                    EmployeeCode = "000023",
                    LastName = "Lagura",
                    FirstName = "Ernesto Jr.",
                    MiddleName = "Dela Rosa",
                    Email = "ernesto.lagura@mmbisolutions.com",
                    Address = "872 Block 31 Kabisig San Andres Cainta, Rizal",
                    Position = "Payroll Processor",
                    Gender = "M",
                    DateOfBirth = new DateTime(1982, 7, 2),
                    DateEmployed = new DateTime(2015, 4, 15)
                },
                new Employees
                {
                    EmployeeCode = "000026",
                    LastName = "Belandres",
                    FirstName = "Maria Donna",
                    MiddleName = "Tejada",
                    Email = "ipayroll@mmbisolutions.com",
                    Address = "#72 Acasia St. Cembo Makati City",
                    Position = "Payroll Processor",
                    Gender = "F",
                    DateOfBirth = new DateTime(1996, 6, 22),
                    DateEmployed = new DateTime(2018, 2, 16)
                },
                new Employees
                {
                    EmployeeCode = "000027",
                    LastName = "Aguilar",
                    FirstName = "Bonn Elexis",
                    MiddleName = "Gamboa",
                    Email = "bonn.aguilar@mmbisolutions.com",
                    Address = "53 Manapat St. Tanong St. Malabon City",
                    Position = "IT/ Implementation Analyst",
                    Gender = "M",
                    DateOfBirth = new DateTime(1989, 11, 20),
                    DateEmployed = new DateTime(2017, 3, 1)
                },
                new Employees
                {
                    EmployeeCode = "000030",
                    LastName = "Bañares",
                    FirstName = "Rafael",
                    MiddleName = "Franco",
                    Email = "rafael.banares@mmbisolutions.com",
                    Address = "#164 Dolores St. Pasay City",
                    Position = ".Net Developer",
                    Gender = "M",
                    DateOfBirth = new DateTime(1992, 9, 10),
                    DateEmployed = new DateTime(2018, 2, 19)
                },
                new Employees
                {
                    EmployeeCode = "000034",
                    LastName = "Mangbanag",
                    FirstName = "Lyka",
                    MiddleName = "Murao",
                    Email = "ipayroll@mmbisolutions.com",
                    Address = "#132 2nd St. 9th Ave., East Grace Park, Caloocan City",
                    Position = "Payroll Processor",
                    Gender = "F",
                    DateOfBirth = new DateTime(1995, 2, 10),
                    DateEmployed = new DateTime(2020, 2, 1)
                },
                new Employees
                {
                    EmployeeCode = "000036",
                    LastName = "Mercado",
                    FirstName = "Kathleen Jane",
                    MiddleName = "P.",
                    Email = "kath.mercado@mmbisolutions.com",
                    Address = "Block 38 Lot 7 Asia 1 Canlubang, Calamba Laguna",
                    Position = "Implementation Analyst",
                    Gender = "F",
                    DateOfBirth = null,
                    DateEmployed = new DateTime(2019, 9, 2)
                }
            };

            var employmentTypeId = _context.EmploymentTypes
                .Where(e => e.TenantEmploymentTypes.First().TenantId == _tenantId && e.Id == "R")
                .Select(e => e.Id)
                .FirstOrDefault();

            var userRoleId = _context.Roles.IgnoreQueryFilters()
                .Where(e => e.TenantId == _tenantId && e.Name == StaticRoleNames.Tenants.User)
                .Select(e => e.Id)
                .FirstOrDefault();

            foreach(var employee in employees)
            {
                if (!_context.Employees.Any(e => e.TenantId == _tenantId && e.EmployeeCode == employee.EmployeeCode))
                {
                    employee.TenantId = _tenantId;
                    employee.EmploymentTypeId = employmentTypeId;
                    employee.EmployeeStatus = "ACTIVE";

                    _context.Employees.Add(employee);
                    _context.SaveChanges();

                    var user = new User
                    {
                        TenantId = _tenantId,
                        EmployeeId = employee.Id,
                        UserName = employee.Email,
                        EmailAddress = employee.Email,
                        Name = employee.FirstName,
                        Surname = employee.LastName,
                        IsEmailConfirmed = true,
                        ShouldChangePasswordOnNextLogin = false,
                        IsActive = true
                    };

                    if (!_context.Users.Any(e => e.TenantId == _tenantId && e.UserName == employee.Email))
                    {
                        user.SetNormalizedNames();
                        user.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions()))
                            .HashPassword(user, "passwordI1!");

                        _context.Users.Add(user);
                        _context.SaveChanges();

                        _context.UserRoles.Add(new UserRole
                        {
                            TenantId = _tenantId,
                            UserId = user.Id,
                            RoleId = userRoleId
                        });

                        _context.SaveChanges();

                    }
                }
            }
        }
    }
}
