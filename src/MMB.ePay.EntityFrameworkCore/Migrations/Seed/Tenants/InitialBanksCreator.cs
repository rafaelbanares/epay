﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialBanksCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialBanksCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var banks = new List<Tuple<string, string, string>>
            {
                new Tuple<string, string, string> ("BDO", "BANCO DE ORO", "SA"),
                new Tuple<string, string, string> ("BPI", "Bank of the Philippines Island", "CA"),
                new Tuple<string, string, string> ("CHINABANK", "China Bank Corporation", "SA"),
                new Tuple<string, string, string> ("EWBC", "EAST WEST BANKING CORP.", "CA"),
                new Tuple<string, string, string> ("IEB", "International Exchange Bank", "SA"),
                new Tuple<string, string, string> ("METROB", "Metro Bank", "CA"),
                new Tuple<string, string, string> ("UBP", "UNIONBANK OF THE PHILIPPINES", "SA")
            };

            foreach(var bank in banks)
            {

                if (!_context.Banks.Any(e => e.Id == bank.Item1))
                {
                    _context.Banks.Add(new Banks
                    {
                        Id = bank.Item1,
                        DisplayName = bank.Item2,
                        Description = bank.Item2
                    });
                    _context.SaveChanges();
                }

                if (!_context.BankBranches.Any(e => e.BankId == bank.Item1))
                {
                    var bankBranch = new BankBranches
                    {
                        BankId = bank.Item1,
                        BranchCode = bank.Item1,
                        BranchName = bank.Item1 + " Branch 1",
                        Address1 = "Test 1",
                        Address2 = "Test 2"
                    };

                    _context.BankBranches.Add(bankBranch);
                    _context.SaveChanges();

                    var bankAccountNumber = bank.Item1 + "123456789";

                    if (!_context.TenantBanks.Any(e => e.TenantId == _tenantId && e.BankAccountNumber == bankAccountNumber))
                    {
                        _context.TenantBanks.Add(new TenantBanks
                        {
                            TenantId = _tenantId,
                            BankAccountNumber = bankAccountNumber,
                            BankAccountType = bank.Item3,
                            BranchNumber = bank.Item1.Substring(0, 3) + "001",
                            BankBranchId = bankBranch.Id,
                            CorporateId = "456789123"
                        });
                        _context.SaveChanges();
                    }
                }
            }
        }
    }
}
