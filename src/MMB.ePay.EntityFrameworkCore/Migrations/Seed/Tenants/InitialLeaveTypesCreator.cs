﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialLeaveTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialLeaveTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var leaveTypes = new List<LeaveTypes>
            {
                new LeaveTypes { DisplayName = "SL", Description = "Sick Leave", WithPay = true },
                new LeaveTypes { DisplayName = "VL", Description = "Vacation Leave", WithPay = true },
                new LeaveTypes { DisplayName = "TOIL", Description = "Toil Leave", WithPay = true },
                new LeaveTypes { DisplayName = "ML", Description = "Maternity Leave", WithPay = true },
                new LeaveTypes { DisplayName = "PL", Description = "Paternity Leave", WithPay = true },
                new LeaveTypes { DisplayName = "LWOP", Description = "Leave Without Pay", WithPay = false },
                new LeaveTypes { DisplayName = "BRL", Description = "Bereavement Leave", WithPay = true },
                new LeaveTypes { DisplayName = "UL", Description = "Union Leave", WithPay = false },
                new LeaveTypes { DisplayName = "LL", Description = "Longevity Leave", WithPay = false },
                new LeaveTypes { DisplayName = "SPL", Description = "Solo Parent Leave", WithPay = false },
                new LeaveTypes { DisplayName = "MRL", Description = "Marriage Leave", WithPay = false },
                new LeaveTypes { DisplayName = "CL", Description = "Calamity Leave", WithPay = false },
                new LeaveTypes { DisplayName = "STL", Description = "Study Leave", WithPay = false },
                new LeaveTypes { DisplayName = "SPLW", Description = "Special Leave for Women", WithPay = false }
            };

            foreach(var leaveType in leaveTypes)
            {
                if (_context.LeaveTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == leaveType.DisplayName))
                {
                    leaveType.TenantId = _tenantId;
                    _context.LeaveTypes.Add(leaveType);
                };
            }

            _context.SaveChanges();
        }
    }
}
