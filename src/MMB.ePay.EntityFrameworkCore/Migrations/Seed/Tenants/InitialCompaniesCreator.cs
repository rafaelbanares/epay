﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialCompaniesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialCompaniesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.Companies.Any(e => e.TenantId == _tenantId))
            {
                var bankBranchId = _context.BankBranches
                    .Where(e => e.TenantBanks.Any(f => f.TenantId == _tenantId))
                    .Select(e => e.Id)
                    .FirstOrDefault();

                _context.Companies.Add(new Companies
                {
                    TenantId = _tenantId,
                    DisplayName = "Default",
                    OrgUnitLevel1Name = "Group 1",
                    OrgUnitLevel2Name = "Group 2",
                    OrgUnitLevel3Name = "Group 3"
                    //BankAccountType = "CA",
                    //BankBranchId = bankBranchId,
                    //CorporateCode = "MMB",
                    //BankAccountNo = "1234567890",
                    //BankBranchCode = "BDO"
                });;

                _context.SaveChanges();
            }
        }
    }
}
