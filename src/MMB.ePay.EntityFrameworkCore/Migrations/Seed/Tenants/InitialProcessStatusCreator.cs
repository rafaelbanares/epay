﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialProcessStatusCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialProcessStatusCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var posted = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "POSTED",
                Description = "POSTED",
                SysCode = "POSTED",
                Step = 4,
                NextStatus = null,
                Responsible = "PROCESSOR"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "POSTED"))
            {
                _context.ProcessStatus.Add(posted);
                _context.SaveChanges();
            }

            var readyForPosting = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "Ready for Posting",
                Description = "Ready for Posting",
                SysCode = "RFP",
                Step = 4,
                Responsible = "PROCESSOR"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "RFP"))
            {
                readyForPosting.NextStatus = posted.Id;

                _context.ProcessStatus.Add(readyForPosting);
                _context.SaveChanges();
            }

            var forFinalReview = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "For Final Review",
                Description = "For Final Review",
                SysCode = "FFR",
                Step = 3,
                Responsible = "REVIEWER3"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "FFR"))
            {
                forFinalReview.NextStatus = readyForPosting.Id;

                _context.ProcessStatus.Add(forFinalReview);
                _context.SaveChanges();
            }

            var forSecondReview = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "For Second Review",
                Description = "For Second Review",
                SysCode = "FSR",
                Step = 3,
                Responsible = "REVIEWER2"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "FSR"))
            {
                forSecondReview.NextStatus = forFinalReview.Id;

                _context.ProcessStatus.Add(forSecondReview);
                _context.SaveChanges();
            }

            var forInitialReview = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "For Initial Review",
                Description = "For Initial Review",
                SysCode = "FIR",
                Step = 3,
                Responsible = "PROCESSOR"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "FIR"))
            {
                forInitialReview.NextStatus = forSecondReview.Id;

                _context.ProcessStatus.Add(forInitialReview);
                _context.SaveChanges();
            }

            var ongoingProcess = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "Ongoing Process",
                Description = "Ongoing Process",
                SysCode = "OP",
                Step = 2,
                Responsible = "SYSTEM"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "OP"))
            {
                ongoingProcess.NextStatus = forInitialReview.Id;

                _context.ProcessStatus.Add(ongoingProcess);
                _context.SaveChanges();
            }

            var waitingToProcess = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "Waiting to Process",
                Description = "Waiting to Process",
                SysCode = "WTP",
                Step = 2,
                Responsible = "SYSTEM"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "WTP"))
            {
                waitingToProcess.NextStatus = ongoingProcess.Id;

                _context.ProcessStatus.Add(waitingToProcess);
                _context.SaveChanges();
            }

            var payrollEntry = new ProcessStatus
            {
                TenantId = _tenantId,
                DisplayName = "Payroll Entry",
                Description = "Payroll Entry",
                SysCode = "PE",
                Step = 1,
                Responsible = "PROCESSOR"
            };

            if (!_context.ProcessStatus.Any(e => e.TenantId == _tenantId && e.SysCode == "PE"))
            {
                payrollEntry.NextStatus = waitingToProcess.Id;

                _context.ProcessStatus.Add(payrollEntry);
                _context.SaveChanges();
            }
        }
    }
}
