﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialLoanTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialLoanTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var loanTypes = new List<LoanTypes>
            {
                new LoanTypes { TenantId = _tenantId, SysCode = "OTHERS", Description = "OTHERS" }, 
                new LoanTypes { TenantId = _tenantId, SysCode = "PAGIBIG", Description = "PAGIBIG LOAN" }, 
                new LoanTypes { TenantId = _tenantId, SysCode = "SSS", Description = "SSS LOAN" }
            };

            foreach(var loanType in loanTypes)
            {
                if (_context.LoanTypes.Any(e => e.TenantId == _tenantId && e.SysCode == loanType.SysCode))
                {
                    _context.LoanTypes.Add(loanType);
                };
            }

            _context.SaveChanges();
        }
    }
}
