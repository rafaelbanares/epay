﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialTimesheetPostedCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialTimesheetPostedCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var payrollPostedId = _context.PayrollPosted.Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .FirstOrDefault();

            var timesheetTypeIds = _context.TimesheetTypes.Select(e => e.Id).ToList();
            var months = Enumerable.Range(1, timesheetTypeIds.Count).ToList();

            foreach (var month in months)
            {
                _context.TimesheetPosted.Add(new TimesheetPosted
                {
                    PayrollPostedId = payrollPostedId,
                    TimesheetTypeId = timesheetTypeIds[month - 1],
                    Days = 1,
                    Hrs = 3,
                    Mins = 30,
                    Amount = month * 100
                });
            };

            _context.SaveChanges();
        }
    }
}
