﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEarningsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEarningsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employeeId = _context.Employees.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var earningTypeIds = _context.EarningTypes.Where(e => e.TenantId == _tenantId).Select(e => e.Id).ToList();
            var processId = _context.Process.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var payrollPostedId = _context.PayrollPosted.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var currentYear = DateTime.Today.Year;

            for(var i = 0; i < earningTypeIds.Count && i <= 12; i++)
            {
                var earningTypeId = earningTypeIds[i];
                var payPeriodId = _context.PayPeriod.Where(e => e.TenantId == _tenantId && e.ApplicableMonth == i + 1).Select(e => e.Id).First();
                var amount = (i + 1) * 100;

                if (!_context.Earnings.Any(e => e.EarningTypeId == earningTypeId))
                {
                    _context.Earnings.Add(new Earnings
                    {
                        EmployeeId = employeeId,
                        EarningTypeId = earningTypeId,
                        Amount = amount,
                        TranDate = DateTime.Now,
                        ProcessId = processId
                    });
                    _context.SaveChanges();
                }

                if (!_context.RecurEarnings.Any(e => e.TenantId == _tenantId && e.EarningTypeId == earningTypeId))
                {
                    var lastDay = DateTime.DaysInMonth(currentYear, i + 1);

                    var recurEarnings = new RecurEarnings
                    {
                        TenantId = _tenantId,
                        EmployeeId = employeeId,
                        EarningTypeId = earningTypeId,
                        Amount = amount,
                        Frequency = "1",
                        StartDate = new DateTime(currentYear, i + 1, 1),
                        EndDate = new DateTime(currentYear, i + 1, lastDay)
                    };

                    _context.RecurEarnings.Add(recurEarnings);
                    _context.SaveChanges();

                    _context.EarningPosted.Add(new EarningPosted
                    {
                        PayrollPostedId = payrollPostedId,
                        EarningTypeId = earningTypeId,
                        Amount = amount
                    });
                    _context.SaveChanges();
                }
            }

        }
    }
}
