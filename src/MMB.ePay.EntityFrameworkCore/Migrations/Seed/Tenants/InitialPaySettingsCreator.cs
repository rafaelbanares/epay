﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using MMB.ePay.Payroll;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialSettingsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialSettingsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var orgGroup = new GroupSettings
            {
                TenantId = _tenantId,
                SysCode = "ORG",
                DisplayName = "Org. Group Settings",
                OrderNo = 1,
            };

            if (!_context.GroupSettings.Any(e => e.TenantId == _tenantId && e.SysCode == "ORG"))
            {
                _context.GroupSettings.Add(orgGroup);
            }

            var miscGroup = new GroupSettings
            {
                TenantId = _tenantId,
                SysCode = "MISC",
                DisplayName = "Miscellaneous",
                OrderNo = 2,
            };

            if (!_context.GroupSettings.Any(e => e.TenantId == _tenantId && e.SysCode == "MISC"))
            {
                _context.GroupSettings.Add(miscGroup);
            }

            _context.SaveChanges();

            var paySettings = new List<PaySettings>
            {
                new PaySettings
                {
                    Key = "DEPARTMENT_LEVEL",
                    Value = "2",
                    DataType = "int",
                    Caption = "Department Level",
                    GroupSettingId = orgGroup.Id
                }
            };

            foreach(var paySetting in paySettings)
            {
                if (!_context.PaySettings.Any(e => e.TenantId == _tenantId && e.Key == paySetting.Key))
                {
                    paySetting.TenantId = _tenantId;

                    _context.PaySettings.Add(paySetting);
                }
            }

            _context.SaveChanges();
        }
    }
}
