﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialDeductionTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialDeductionTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var deductionTypes = new List<string>
            {
                "VALE ADJUSTMENT",
                "CARD REPLACEMENT FEE",
                "REPAIR & MAINTENANCE",
                "SAFETY SHOES HIGH",
                "SAFETY SHOES LOW",
                "PAGIBIG - DEDUCTION ADJUSTMENT",
                "PHIL - DEDUCTION ADJUSTMENT",
                "SS - DEDUCTION ADJUSTMENT",
                "TAX - DEDUCTION ADJUSTMENT",
                "ADVANCES"
            };

            foreach (var deductionType in deductionTypes)
            {
                if (!_context.DeductionTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == deductionType))
                {
                    _context.DeductionTypes.Add(new DeductionTypes
                    {
                        TenantId = _tenantId,
                        DisplayName = deductionType,
                        Description = deductionType,
                        Active = true,
                        Currency = "PHP"
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
