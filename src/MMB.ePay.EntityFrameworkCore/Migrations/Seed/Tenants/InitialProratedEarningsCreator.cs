﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialProratedEarningsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialProratedEarningsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var earningTypeIds = _context.EarningTypes
                .Where(e => e.TenantId == _tenantId)
                .OrderBy(e => e.DisplayName)
                .Select(e => e.Id)
                .ToList();

            var timesheetTypeId = _context.TimesheetTypes
                .Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .First();

            var maxCount = earningTypeIds.Count / 2;

            for (var i = 0; i <= maxCount; i++)
            {
                if (!_context.ProratedEarnings.Any(e => e.TenantId == _tenantId && e.EarningTypeId == earningTypeIds[i]))
                {
                    var proratedEarnings = new ProratedEarnings
                    {
                        EarningTypeId = earningTypeIds[i],
                        InTimesheetTypeId = timesheetTypeId,
                        OutEarningTypeId = earningTypeIds[i],
                        TenantId = _tenantId
                    };

                    _context.ProratedEarnings.Add(proratedEarnings);
                    _context.SaveChanges();
                }
            }
        }
    }
}
