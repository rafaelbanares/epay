﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialStatutoryTypesCreator
    {
        private readonly ePayDbContext _context;

        public InitialStatutoryTypesCreator(ePayDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            var statutoryTypes = new List<StatutoryTypes>
            {
                new StatutoryTypes { Code = "SSS", Description = "SSS" },
                new StatutoryTypes { Code = "PHEALTH", Description = "PHILHEALTH" },
                new StatutoryTypes { Code = "PAGIBIG", Description = "PAGIBIG" },
                new StatutoryTypes { Code = "TAX", Description = "WTAX" },
            };

            foreach(var statutoryType in statutoryTypes)
            {
                if (!_context.StatutoryTypes.Any(e => e.Code == statutoryType.Code))
                {
                    _context.StatutoryTypes.Add(statutoryType);
                }
            }

            _context.SaveChanges();

            var statutorySubTypes = new List<StatutorySubTypes>
            {
                new StatutorySubTypes { SysCode = "EESSS", DisplayName = "EE SSS", EmployeeShare = true, StatutoryType = "SSS" },
                new StatutorySubTypes { SysCode = "ERSSS", DisplayName = "ER SSS", EmployeeShare = false, StatutoryType = "SSS" },
                new StatutorySubTypes { SysCode = "EEPH", DisplayName = "EE PH", EmployeeShare = true, StatutoryType = "PHEALTH" },
                new StatutorySubTypes { SysCode = "ERPH", DisplayName = "ER PH", EmployeeShare = false, StatutoryType = "PHEALTH" },
                new StatutorySubTypes { SysCode = "EEPAG", DisplayName = "EE PAG", EmployeeShare = true, StatutoryType = "PAGIBIG" },
                new StatutorySubTypes { SysCode = "ERPAG", DisplayName = "ER PAG", EmployeeShare = false, StatutoryType = "PAGIBIG" },
                new StatutorySubTypes { SysCode = "PAGADD", DisplayName = "ADDTL PAG", EmployeeShare = true, StatutoryType = "PAGIBIG" },
                new StatutorySubTypes { SysCode = "WTAX", DisplayName = "WTAX", EmployeeShare = true, StatutoryType = "TAX" }
            };

            foreach(var statutorySubType in statutorySubTypes)
            {
                if (!_context.StatutorySubTypes.Any(e => e.SysCode == statutorySubType.SysCode))
                {
                    statutorySubType.Description = statutorySubType.DisplayName;
                    _context.StatutorySubTypes.Add(statutorySubType);
                }
            }

            _context.SaveChanges();
        }
    }
}
