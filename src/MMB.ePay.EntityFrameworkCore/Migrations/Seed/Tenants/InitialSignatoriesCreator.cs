﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialSignatoriesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialSignatoriesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.Signatories.Any(e => e.TenantId == _tenantId && e.SignatoryName == "May Villanueva Barnachea"))
            {
                _context.Signatories.Add(new Signatories
                {
                    TenantId = _tenantId,
                    SignatoryName = "May Villanueva Barnachea",
                    Position = "HR/Admin Staff"
                });

                _context.SaveChanges();
            }
        }
    }
}
