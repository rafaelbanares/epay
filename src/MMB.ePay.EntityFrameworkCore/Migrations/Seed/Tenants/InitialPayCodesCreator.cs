﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialPayCodesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialPayCodesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var payCodes = new List<PayCodes>
            {
                new PayCodes { PayFreq = "W", TaxFreq = "M", TaxMethod = "PERCENTAGE", RequireTimesheet = false, IncBonTaxDiv = true, TaxableOT = true, MinimumWager = true },
                new PayCodes { PayFreq = "S", TaxFreq = "P", TaxMethod = "NORMAL", RequireTimesheet = true, IncBonTaxDiv = false, TaxableOT = true, MinimumWager = true },
                new PayCodes { PayFreq = "M", TaxFreq = "P", TaxMethod = "PERCENTAGE", RequireTimesheet = true, IncBonTaxDiv = true, TaxableOT = false, MinimumWager = true },
                new PayCodes { PayFreq = "S", TaxFreq = "M", TaxMethod = "NORMAL", RequireTimesheet = true, IncBonTaxDiv = true, TaxableOT = true, MinimumWager = false },
            };

            foreach(var payCode in payCodes.Select((value, i) => new { i, value }))
            {
                var displayName = "Test" + payCode.i;

                if (!_context.PayCodes.Any(e => e.DisplayName == displayName && e.TenantId == _tenantId))
                {
                    var newPayCode = payCode.value;
                    newPayCode.TenantId = _tenantId;
                    newPayCode.DisplayName = displayName;
                    newPayCode.Description = displayName;

                    newPayCode.WorkingDays = payCode.i;
                    newPayCode.HrsPerDay = payCode.i + 1;
                    newPayCode.PayPeriodPerYear = payCode.i + 2;
                    newPayCode.DaysPerWeek = payCode.i + 3;
                    newPayCode.Nd1Rate = payCode.i + 4;
                    newPayCode.Nd2Rate = payCode.i + 5;

                    _context.PayCodes.Add(newPayCode);
                    _context.SaveChanges();
                }
            }
        }
    }
}
