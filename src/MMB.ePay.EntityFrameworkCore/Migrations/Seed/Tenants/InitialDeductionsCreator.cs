﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialDeductionsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialDeductionsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employeeId = _context.Employees.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var deductionTypeIds = _context.DeductionTypes.Where(e => e.TenantId == _tenantId).Select(e => e.Id).ToList();
            var processId = _context.Process.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var payrollPostedId = _context.PayrollPosted.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var currentYear = DateTime.Today.Year;

            for (var i = 0; i < deductionTypeIds.Count && i <= 12; i++)
            {
                var deductionTypeId = deductionTypeIds[i];
                var payPeriodId = _context.PayPeriod.Where(e => e.TenantId == _tenantId && e.ApplicableMonth == i + 1).Select(e => e.Id).First();
                var amount = (i + 1) * 100;

                if (!_context.Deductions.Any(e => e.DeductionTypeId == deductionTypeId))
                {
                    _context.Deductions.Add(new Deductions
                    {
                        EmployeeId = employeeId,
                        DeductionTypeId = deductionTypeId,
                        Amount = amount,
                        ProcessId = processId
                    });
                    _context.SaveChanges();
                }

                if (!_context.RecurDeductions.Any(e => e.TenantId == _tenantId && e.DeductionTypeId == deductionTypeId))
                {
                    var lastDay = DateTime.DaysInMonth(currentYear, i + 1);

                    var recurDeductions = new RecurDeductions
                    {
                        TenantId = _tenantId,
                        EmployeeId = employeeId,
                        DeductionTypeId = deductionTypeId,
                        Amount = amount,
                        Frequency = "1",
                        StartDate = new DateTime(currentYear, i + 1, 1),
                        EndDate = new DateTime(currentYear, i + 1, lastDay)
                    };

                    _context.RecurDeductions.Add(recurDeductions);
                    _context.SaveChanges();

                    _context.DeductionPosted.Add(new DeductionPosted
                    {
                        PayrollPostedId = payrollPostedId,
                        DeductionTypeId = deductionTypeId,
                        Amount = amount
                    });
                    _context.SaveChanges();
                }
            }

        }
    }
}
