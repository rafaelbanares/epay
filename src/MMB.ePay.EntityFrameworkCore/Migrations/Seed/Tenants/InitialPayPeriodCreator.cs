﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialPayPeriodCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialPayPeriodCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var currentYear = DateTime.Today.Year;

            var payPeriods = new List<Tuple<DateTime, DateTime, DateTime, string>>
            {
                new Tuple<DateTime, DateTime, DateTime, string>
                (
                    new DateTime(currentYear - 1, 12, 24),
                    new DateTime(currentYear, 12, 8),
                    new DateTime(currentYear - 1, 12, 31),
                    "2"
                )
            };

            var months = Enumerable.Range(1, 12).ToList();

            foreach(var month in months)
            {
                var lastDay = DateTime.DaysInMonth(currentYear, month);
                var nextMonth = month == 12 ? 1 : month + 1;
                var nextYear = month == 12 ? currentYear + 1 : currentYear;

                payPeriods.Add(new Tuple<DateTime, DateTime, DateTime, string>
                (
                    new DateTime(currentYear, month, 9),
                    new DateTime(currentYear, month, 23),
                    new DateTime(currentYear, month, lastDay),
                    "1"
                ));

                payPeriods.Add(new Tuple<DateTime, DateTime, DateTime, string>
                (
                    new DateTime(currentYear, month, 24),
                    new DateTime(nextYear, nextMonth, 8),
                    new DateTime(currentYear, month, lastDay),
                    "2"
                ));
            }

            foreach(var payPeriod in payPeriods)
            {
                if (!_context.PayPeriod.Any(e => e.TenantId == _tenantId && e.PeriodFrom == payPeriod.Item1))
                {
                    _context.PayPeriod.Add(new PayPeriod
                    {
                        TenantId = _tenantId,
                        PayrollFreq = "S",
                        ProcessType = "REGULAR",
                        PeriodFrom = payPeriod.Item1,
                        PeriodTo = payPeriod.Item2,
                        PayDate = payPeriod.Item3,
                        ApplicableMonth = payPeriod.Item1.Month,
                        ApplicableYear = payPeriod.Item1.Year,
                        CutOffFrom = payPeriod.Item1,
                        CutOffTo = payPeriod.Item2,
                        Currency = "PHP",
                        FiscalYear = payPeriod.Item1.Year,
                        WeekNo = payPeriod.Item4,
                        PayrollStatus = "NEW"
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
