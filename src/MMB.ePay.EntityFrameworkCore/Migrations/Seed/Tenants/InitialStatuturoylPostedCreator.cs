﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialStatutoryPostedCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialStatutoryPostedCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var payrollPostedId = _context.PayrollPosted.Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .FirstOrDefault();

            var statutoryTypeIds = _context.StatutorySubTypes.Select(e => e.Id).ToList();
            var months = Enumerable.Range(1, statutoryTypeIds.Count).ToList();

            foreach (var month in months)
            {
                _context.StatutoryPosted.Add(new StatutoryPosted
                {
                    PayrollPostedId = payrollPostedId,
                    StatutorySubTypeId = statutoryTypeIds[month - 1],
                    Amount = month * 100
                });
            };

            _context.SaveChanges();
        }
    }
}
