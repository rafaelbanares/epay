﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEmployeeStatutoryInfoCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmployeeStatutoryInfoCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var statutoryTypeCodes = _context.StatutoryTypes
                .Select(e => e.Code)
                .ToList();

            var employeeIds = _context.Employees.Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .ToList();

            foreach(var employeeId in employeeIds)
            {
                if (!_context.EmployeeStatutoryInfo.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    foreach(var statutoryTypeCode in statutoryTypeCodes)
                    {
                        _context.EmployeeStatutoryInfo.Add(new EmployeeStatutoryInfo
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            StatutoryType = statutoryTypeCode,
                            StatutoryAcctNo = "1234567890" + employeeId
                        });
                    }
                }
            }

            _context.SaveChanges();
        }
    }
}
