﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEmployeePayrollCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmployeePayrollCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employeeIds = _context.Employees.Select(e => e.Id).ToList();
            var payCodeId = _context.PayCodes.Where(e => e.TenantId == _tenantId).Select(e => e.Id).First();
            var bankBranchId = _context.BankBranches.Where(e => e.TenantBanks.Any(f => f.TenantId == _tenantId)).Select(e => e.Id).First();

            foreach (var employeeId in employeeIds)
            {
                if (!_context.EmployeePayroll.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    _context.EmployeePayroll.Add(new EmployeePayroll
                    {
                        TenantId = _tenantId,
                        PayCodeId = payCodeId,
                        EmployeeId = employeeId,
                        BankBranchId = bankBranchId,
                        PayrollType = "P",
                        Salary = 10000,
                        PagibigNumber = "0123456789",
                        PagibigAdd = 100,
                        PagibigRate = 1,
                        SSSNumber = "1234567890",
                        PaymentType = "BANK",
                        BankAccountNo = "9876543210",
                        OT_exempt = false,
                        SSS_exempt = false,
                        PH_exempt = false,
                        PAG_exempt = false,
                        Tax_exempt = false,
                        ComputeAsDaily = false
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
