﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Payroll;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialOvertimeTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialOvertimeTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var overtimeGroup1s = new List<OvertimeMainTypes>();
            var overtimeGroup2s = new List<OvertimeSubTypes>();

            //OvertimeGroup1
            var reg = new OvertimeMainTypes {DisplayName = "REG", Description = "Regular OT", DisplayOrder = 1 };
            overtimeGroup1s.Add(reg);

            var lgl = new OvertimeMainTypes { DisplayName = "LGL", Description = "Legal OT", DisplayOrder = 2 };
            overtimeGroup1s.Add(lgl);

            var rst = new OvertimeMainTypes {DisplayName = "RST", Description = "Rest Day OT", DisplayOrder = 3 };
            overtimeGroup1s.Add(rst);

            var spl = new OvertimeMainTypes {DisplayName = "SPL", Description = "Special OT", DisplayOrder = 4 };
            overtimeGroup1s.Add(spl);

            var lglrst = new OvertimeMainTypes {DisplayName = "LGLRST", Description = "Legal Rest Day", DisplayOrder = 5 };
            overtimeGroup1s.Add(lglrst);

            var splrst = new OvertimeMainTypes {DisplayName = "SPLRST", Description = "Special Rest Day", DisplayOrder = 6 };
            overtimeGroup1s.Add(splrst);

            foreach(var overtimeGroup1 in overtimeGroup1s)
            {
                if (!_context.OvertimeMainTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == overtimeGroup1.DisplayName))
                {
                    overtimeGroup1.TenantId = _tenantId;
                    _context.OvertimeMainTypes.Add(overtimeGroup1);
                }
            }

            //OvertimeGroup2
            var first8 = new OvertimeSubTypes { DisplayName = "First8", Description = "First8", DisplayOrder = 1 };
            overtimeGroup2s.Add(first8);

            var nd1 = new OvertimeSubTypes { DisplayName = "ND1", Description = "ND1", DisplayOrder = 2 };
            overtimeGroup2s.Add(nd1);

            var beyond8 = new OvertimeSubTypes { DisplayName = "Beyond8", Description = "Beyond8", DisplayOrder = 3 };
            overtimeGroup2s.Add(beyond8);

            var nd2 = new OvertimeSubTypes { DisplayName = "ND2", Description = "ND2", DisplayOrder = 4 };
            overtimeGroup2s.Add(nd2);

            foreach(var overtimeGroup2 in overtimeGroup2s)
            {
                if (!_context.OvertimeSubTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == overtimeGroup2.DisplayName))
                {
                    overtimeGroup2.TenantId = _tenantId;
                    _context.OvertimeSubTypes.Add(overtimeGroup2);
                }
            }

            _context.SaveChanges();

            //OvertimeTypes
            var overtimeTypes = new List<Tuple<int, string, string>>
            {
                new Tuple<int, string, string> ( reg.Id, "REG", "Regular" ),
                new Tuple<int, string, string> ( lgl.Id, "LGL", "Legal" ),
                new Tuple<int, string, string> ( rst.Id, "RST", "Rest Day" ),
                new Tuple<int, string, string> ( spl.Id, "SPL", "Special" ),
                new Tuple<int, string, string> ( lglrst.Id, "LGLRST", "Legal Rest Day" ),
                new Tuple<int, string, string> ( splrst.Id, "SPLRST", "Special Rest Day" )
            };

            foreach(var overtimeType in overtimeTypes)
            {
                if (!_context.OvertimeTypes.Any(e => e.OvertimeMainTypes.TenantId == _tenantId && e.OvertimeSubTypes.TenantId == _tenantId 
                    && e.DisplayName == overtimeType.Item2))
                {
                    _context.OvertimeTypes.Add(new OvertimeTypes
                    {
                        OvertimeMainTypeId = overtimeType.Item1,
                        OvertimeSubTypeId = first8.Id,
                        DisplayName = overtimeType.Item2,
                        Description = overtimeType.Item3 + " OT",
                        Rate = 999
                    });

                    _context.OvertimeTypes.Add(new OvertimeTypes
                    {
                        OvertimeMainTypeId = overtimeType.Item1,
                        OvertimeSubTypeId = nd1.Id,
                        DisplayName = overtimeType.Item2 + " ND1",
                        Description = overtimeType.Item3 + " ND1",
                        Rate = 999
                    });

                    _context.OvertimeTypes.Add(new OvertimeTypes
                    {
                        OvertimeMainTypeId = overtimeType.Item1,
                        OvertimeSubTypeId = beyond8.Id,
                        DisplayName = overtimeType.Item2 + "8",
                        Description = overtimeType.Item3 + " >8",
                        Rate = 999
                    });

                    _context.OvertimeTypes.Add(new OvertimeTypes
                    {
                        OvertimeMainTypeId = overtimeType.Item1,
                        OvertimeSubTypeId = nd2.Id,
                        DisplayName = overtimeType.Item2 + " ND2",
                        Description = overtimeType.Item3 + " ND2",
                        Rate = 999
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
