﻿using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Migrations.Seed.Host;
using MMB.ePay.Migrations.Seed.Tenants;
using System;
using System.Transactions;

namespace MMB.ePay.Migrations.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<ePayDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(ePayDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            //Host seed
            new InitialHostDbBuilder(context).Create();

            //Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();

            //Payroll
            new InitialProcessStatusCreator(context, 1).Create();
            new InitialBanksCreator(context, 1).Create();
            new InitialHolidaysCreator(context, 1).Create();
            new InitialPayPeriodCreator(context, 1).Create();
            new InitialTaxTablesCreator(context, 1).Create();
            new InitialRanksCreator(context, 1).Create();
            new InitialSignatoriesCreator(context, 1).Create();
            new InitialReportsCreator(context, 1).Create();
            new InitialSettingsCreator(context, 1).Create();
            new InitialContactPersonsCreator(context, 1).Create();
            new InitialCompaniesCreator(context, 1).Create();

            new InitialDeductionTypesCreator(context, 1).Create();
            new InitialEarningTypesCreator(context, 1).Create();
            new InitialLeaveTypesCreator(context, 1).Create();
            new InitialLoanTypesCreator(context, 1).Create();
            new InitialOvertimeTypesCreator(context, 1).Create();
            new InitialStatutoryTypesCreator(context).Create();
            new InitialTimesheetTypesCreator(context, 1).Create();

            //HR
            new InitialEmploymentTypesCreator(context, 1).Create();
            new InitialEmployeesCreator(context, 1).Create();

            //Posted
            new InitialPayCodesCreator(context, 1).Create();
            new InitialEmployeeStatutoryInfoCreator(context, 1).Create();
            new InitialEmployeePayrollCreator(context, 1).Create();
            new InitialPayrollPostedCreator(context, 1).Create();
            new InitialStatutoryPostedCreator(context, 1).Create();
            new InitialTimesheetPostedCreator(context, 1).Create();
            new InitialProcessCreator(context, 1).Create();
            new InitialDeductionsCreator(context, 1).Create();
            new InitialEarningsCreator(context, 1).Create();

            //New
            new InitialProratedEarningsCreator(context, 1).Create();
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>();
            using var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress);
            var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

            contextAction(context);

            uow.Complete();
        }
    }
}
