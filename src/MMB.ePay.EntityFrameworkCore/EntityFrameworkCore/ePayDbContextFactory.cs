﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MMB.ePay.Configuration;
using MMB.ePay.Web;

namespace MMB.ePay.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ePayDbContextFactory : IDesignTimeDbContextFactory<ePayDbContext>
    {
        public ePayDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ePayDbContext>();
            var configuration = AppConfigurations.Get(
                WebContentDirectoryFinder.CalculateContentRootFolder(),
                addUserSecrets: true
            );

            ePayDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ePayConsts.ConnectionStringName));

            return new ePayDbContext(builder.Options);
        }
    }
}