﻿using Abp.IdentityServer4vNext;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization.Delegation;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Chat;
using MMB.ePay.Editions;
using MMB.ePay.Friendships;
using MMB.ePay.HR;
using MMB.ePay.MultiTenancy;
using MMB.ePay.MultiTenancy.Accounting;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.Payroll;
using MMB.ePay.Persons;
using MMB.ePay.Storage;

namespace MMB.ePay.EntityFrameworkCore
{
#pragma warning disable IDE1006 // Naming Styles
    public class ePayDbContext : AbpZeroDbContext<Tenant, Role, User, ePayDbContext>, IAbpPersistedGrantDbContext
#pragma warning restore IDE1006 // Naming Styles
    {
        public virtual DbSet<Holidays> Holidays { get; set; }

        public virtual DbSet<HolidayTypes> HolidayTypes { get; set; }

        public virtual DbSet<TimesheetTypes> TimesheetTypes { get; set; }

        public virtual DbSet<EmployeeStatutoryInfo> EmployeeStatutoryInfo { get; set; }

        public virtual DbSet<EmploymentTypes> EmploymentTypes { get; set; }

        public virtual DbSet<Banks> Banks { get; set; }

        public virtual DbSet<Companies> Companies { get; set; }

        public virtual DbSet<CompanyStatutoryInfo> CompanyStatutoryInfo { get; set; }

        public virtual DbSet<ContactPersons> ContactPersons { get; set; }

        public virtual DbSet<StatutorySubTypes> StatutorySubTypes { get; set; }

        public virtual DbSet<StatutoryTypes> StatutoryTypes { get; set; }

        public virtual DbSet<ProcessStatus> ProcessStatus { get; set; }

        public virtual DbSet<GroupSettings> GroupSettings { get; set; }

        public virtual DbSet<OvertimeTypes> OvertimeTypes { get; set; }

        public virtual DbSet<Ranks> Ranks { get; set; }

        public virtual DbSet<Reports> Reports { get; set; }

        public virtual DbSet<ReportSignatories> ReportSignatories { get; set; }

        public virtual DbSet<TimesheetTmp> TimesheetTmp { get; set; }

        public virtual DbSet<Timesheets> Timesheets { get; set; }

        public virtual DbSet<TimesheetProcess> TimesheetProcess { get; set; }

        public virtual DbSet<TimesheetPosted> TimesheetPosted { get; set; }

        public virtual DbSet<TaxTables> TaxTables { get; set; }

        public virtual DbSet<Signatories> Signatories { get; set; }

        public virtual DbSet<StatutoryTmp> StatutoryTmp { get; set; }

        public virtual DbSet<StatutoryProcess> StatutoryProcess { get; set; }

        public virtual DbSet<StatutoryPosted> StatutoryPosted { get; set; }

        public virtual DbSet<StatutorySettings> StatutorySettings { get; set; }

        public virtual DbSet<TaxMethodByProcessType> TaxMethodByProcessType { get; set; }

        public virtual DbSet<SssTables> SssTables { get; set; }

        public virtual DbSet<Reviewers> Reviewers { get; set; }

        public virtual DbSet<RecurEarnings> RecurEarnings { get; set; }

        public virtual DbSet<RecurDeductions> RecurDeductions { get; set; }

        public virtual DbSet<ProratedEarnings> ProratedEarnings { get; set; }

        public virtual DbSet<ProcSessionBatches> ProcSessionBatches { get; set; }

        public virtual DbSet<ProcSessions> ProcSessions { get; set; }

        public virtual DbSet<ProcessHistory> ProcessHistory { get; set; }

        public virtual DbSet<Process> Process { get; set; }

        public virtual DbSet<PaySettings> PaySettings { get; set; }

        public virtual DbSet<PayrollTmp> PayrollTmp { get; set; }

        public virtual DbSet<PayrollProcess> PayrollProcess { get; set; }

        public virtual DbSet<PayrollPosted> PayrollPosted { get; set; }

        public virtual DbSet<PayPeriod> PayPeriod { get; set; }

        public virtual DbSet<OvertimeMainTypes> OvertimeMainTypes { get; set; }

        public virtual DbSet<OvertimeSubTypes> OvertimeSubTypes { get; set; }

        public virtual DbSet<OTSheetTmp> OTSheetTmp { get; set; }

        public virtual DbSet<OTSheetProcess> OTSheetProcess { get; set; }

        public virtual DbSet<OTSheetPosted> OTSheetPosted { get; set; }

        public virtual DbSet<OTSheet> OTSheet { get; set; }

        public virtual DbSet<LoanPayments> LoanPayments { get; set; }

        public virtual DbSet<LoanTmp> LoanTmp { get; set; }

        public virtual DbSet<LoanProcess> LoanProcess { get; set; }

        public virtual DbSet<LeaveCredits> LeaveCredits { get; set; }

        public virtual DbSet<Employers> Employers { get; set; }

        public virtual DbSet<EarningTypeSettings> EarningTypeSettings { get; set; }

        public virtual DbSet<EarningTmp> EarningTmp { get; set; }

        public virtual DbSet<Earnings> Earnings { get; set; }

        public virtual DbSet<EarningProcess> EarningProcess { get; set; }

        public virtual DbSet<EarningPosted> EarningPosted { get; set; }

        public virtual DbSet<DeductionTmp> DeductionTmp { get; set; }

        public virtual DbSet<Deductions> Deductions { get; set; }

        public virtual DbSet<DeductionProcess> DeductionProcess { get; set; }

        public virtual DbSet<DeductionPosted> DeductionPosted { get; set; }

        public virtual DbSet<Dependents> Dependeds { get; set; }

        public virtual DbSet<TenantBanks> TenantBanks { get; set; }

        public virtual DbSet<TenantEmploymentTypes> TenantEmploymentTypes { get; set; }

        public virtual DbSet<BankBranches> BankBranches { get; set; }

        public virtual DbSet<HRSettings> HRSettings { get; set; }

        public virtual DbSet<PayCodes> PayCodes { get; set; }

        public virtual DbSet<EmployeePayroll> EmployeePayroll { get; set; }

        public virtual DbSet<EarningTypes> EarningTypes { get; set; }

        public virtual DbSet<EmployeeLoans> EmployeeLoans { get; set; }

        public virtual DbSet<LoanPosted> LoanPosted { get; set; }

        public virtual DbSet<Loans> Loans { get; set; }

        public virtual DbSet<Employees> Employees { get; set; }

        public virtual DbSet<LoanTypes> LoanTypes { get; set; }

        public virtual DbSet<DeductionTypes> DeductionTypes { get; set; }

        public virtual DbSet<LeaveTypes> LeaveTypes { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<Person> Persons { get; set; }

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

        public virtual DbSet<UserDelegation> UserDelegations { get; set; }

        public ePayDbContext(DbContextOptions<ePayDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Holidays>(h =>
            {
                h.ToTable("Holidays", "pay");

                h.HasIndex(e => new { e.TenantId });

                h.Property(e => e.HolidayTypeId).IsUnicode(false);

                h.HasOne(d => d.HolidayTypes)
                    .WithMany(p => p.Holidays)
                    .HasForeignKey(d => d.HolidayTypeId)
                    .HasConstraintName("FK_Holidays_HolidayTypes");
            });
            modelBuilder.Entity<HolidayTypes>(h =>
            {
                h.ToTable("HolidayTypes", "pay");

                h.Property(e => e.Id).IsUnicode(false);
            });
            modelBuilder.Entity<TimesheetTypes>(t =>
                        {
                            t.ToTable("TimesheetTypes", "pay");

                            t.HasIndex(e => new { e.TenantId });

                            t.Property(e => e.SysCode).IsUnicode(false);
                        });
            modelBuilder.Entity<EmploymentTypes>(x =>
                        {
                            x.ToTable("EmploymentTypes", "hr");

                            x.Property(e => e.Id).IsUnicode(false);
                        });
            modelBuilder.Entity<Banks>(b =>
                       {
                           b.ToTable("Banks", "pay");

                           b.Property(e => e.Id).IsUnicode(false);
                       });
            modelBuilder.Entity<Companies>(c =>
            {
                c.ToTable("Companies", "hr");

                c.HasIndex(e => new { e.TenantId });

                c.Property(e => e.DisplayName).IsUnicode(false);
                c.Property(e => e.Address1).IsUnicode(false);
                c.Property(e => e.Address2).IsUnicode(false);

                c.HasIndex(e => new { e.TenantId, e.DisplayName }).IsUnique();
            });

            modelBuilder.Entity<Signatories>(s =>
            {
                s.ToTable("Signatories", "pay");

                s.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<ReportSignatories>(r =>
            {
                r.ToTable("ReportSignatories", "pay");

                r.HasIndex(e => new { e.TenantId });

                r.HasOne(d => d.Reports)
                    .WithMany(p => p.ReportSignatories)
                    .HasForeignKey(d => d.ReportId)
                    .HasConstraintName("FK_ReportSignatories_ReportId");

                r.HasOne(d => d.Signatories)
                    .WithMany(p => p.ReportSignatories)
                    .HasForeignKey(d => d.SignatoryId)
                    .HasConstraintName("FK_ReportSignatories_Signatories");
            });
            modelBuilder.Entity<CompanyStatutoryInfo>(c =>
            {
                c.ToTable("CompanyStatutoryInfo", "pay");

                c.HasIndex(e => new { e.TenantId });

                c.HasOne(d => d.Signatories)
                    .WithMany(p => p.CompanyStatutoryInfo)
                    .HasForeignKey(d => d.SignatoryId)
                    .HasConstraintName("FK_CompanyStatutoryInfo_Signatories");

                c.HasOne(d => d.StatutoryTypes)
                    .WithMany(p => p.CompanyStatutoryInfo)
                    .HasPrincipalKey(d => d.Code)
                    .HasForeignKey(d => d.StatutoryType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CompanyStatutoryInfo_StatutoryTypes");

                c.HasIndex(e => new { e.TenantId, e.StatutoryType })
                    .HasDatabaseName("IX_CompanyStatutoryInfo_TenantId_StatutoryType")
                    .IsUnique();
            });
            modelBuilder.Entity<EmployeeStatutoryInfo>(e =>
            {
                e.ToTable("EmployeeStatutoryInfo", "pay");

                e.HasIndex(e => new { e.TenantId });

                e.HasOne(d => d.Employees)
                    .WithMany(p => p.EmployeeStatutoryInfo)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_EmployeeStatutoryInfo_Employees");

                e.HasOne(d => d.StatutoryTypes)
                    .WithMany(p => p.EmployeeStatutoryInfo)
                    .HasPrincipalKey(d => d.Code)
                    .HasForeignKey(d => d.StatutoryType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeStatutoryInfo_StatutoryTypes");
            });

            modelBuilder.Entity<ContactPersons>(c =>
            {
                c.ToTable("ContactPersons", "pay");

                c.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<StatutorySubTypes>(s =>
            {
                s.ToTable("StatutorySubTypes", "pay");

                s.Property(e => e.SysCode).IsUnicode(false);

                s.HasOne(d => d.StatutoryTypes)
                    .WithMany(p => p.StatutorySubTypes)
                    .HasPrincipalKey(d => d.Code)
                    .HasForeignKey(d => d.StatutoryType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StatutorySubTypes_StatutoryTypes");
            });
            modelBuilder.Entity<StatutoryTypes>(s =>
            {
                s.ToTable("StatutoryTypes", "pay");

                s.Property(e => e.Code).IsUnicode(false);

                s.HasIndex(e => e.Code)
                    .HasDatabaseName("IX_StatutoryType_Code")
                    .IsUnique();
            });
            modelBuilder.Entity<ProcessStatus>(p =>
                       {
                           p.ToTable("ProcessStatus", "pay");

                           p.HasIndex(e => new { e.TenantId });

                           p.HasOne(d => d.NextStatusNavigation)
                                .WithMany(p => p.InverseProcessStatusNavigation)
                                .HasForeignKey(d => d.NextStatus)
                                .HasConstraintName("FK_ProcessStatus_ProcessStatus")
                                .OnDelete(DeleteBehavior.NoAction);

                           p.Property(e => e.SysCode).IsUnicode(false);
                       });
            modelBuilder.Entity<GroupSettings>(g =>
                       {
                           g.ToTable("GroupSettings", "hr");

                           g.HasIndex(e => new { e.TenantId });

                           g.Property(e => e.SysCode).IsUnicode(false);
                           g.Property(e => e.DisplayName).IsUnicode(false);
                       });
            modelBuilder.Entity<OvertimeTypes>(o =>
                       {
                           o.ToTable("OvertimeTypes", "pay");

                           o.HasOne(d => d.OvertimeMainTypes)
                                .WithMany(p => p.OvertimeTypes)
                                .HasForeignKey(d => d.OvertimeMainTypeId)
                                .HasConstraintName("FK_OvertimeTypes_OvertimeMainTypes");

                           o.HasOne(d => d.OvertimeSubTypes)
                               .WithMany(p => p.OvertimeTypes)
                               .HasForeignKey(d => d.OvertimeSubTypeId)
                               .HasConstraintName("FK_OvertimeTypes_OvertimeSubTypes");
                       });
            modelBuilder.Entity<Ranks>(r =>
                        {
                            r.ToTable("Ranks", "pay");

                            r.HasIndex(e => new { e.TenantId });

                            r.Property(e => e.SysCode).IsUnicode(false);
                        });
            modelBuilder.Entity<Reports>(r =>
            {
                r.ToTable("Reports", "pay");

                r.HasIndex(e => new { e.TenantId });

                r.Property(e => e.ControllerName).IsUnicode(false);

                r.Property(e => e.ReportTypeId).IsUnicode(false);

                r.Property(e => e.Format).IsUnicode(false);

                r.Property(e => e.Output).IsUnicode(false);
            });
            modelBuilder.Entity<PayPeriod>(p =>
                        {
                            p.ToTable("PayPeriod", "pay");

                            p.HasIndex(e => new { e.TenantId });
                        });
            modelBuilder.Entity<TimesheetTmp>(t =>
                       {
                           t.ToTable("TimesheetTmp", "pay");

                           t.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.TimesheetTmp)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_TimesheetTmp_PayrollProcess");
                       });
            modelBuilder.Entity<Timesheets>(t =>
                       {
                           t.ToTable("Timesheets", "pay");

                           t.HasOne(d => d.Process)
                                .WithMany(p => p.Timesheets)
                                .HasForeignKey(d => d.ProcessId)
                                .HasConstraintName("FK_Timesheets_Process");

                           t.HasOne(d => d.TimesheetTypes)
                                .WithMany(p => p.Timesheets)
                                .HasForeignKey(d => d.TimesheetTypeId)
                                .HasConstraintName("FK_Timesheets_TimesheetTypes");
                       });
            modelBuilder.Entity<TimesheetProcess>(t =>
                       {
                           t.ToTable("TimesheetProcess", "pay");

                           t.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.TimesheetProcess)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_TimesheetProcess_PayrollProcess");

                           t.HasOne(d => d.TimesheetTypes)
                                .WithMany(p => p.TimesheetProcess)
                                .HasForeignKey(d => d.TimesheetTypeId)
                                .HasConstraintName("FK_TimesheetProcess_TimesheetTypes");
                       });
            modelBuilder.Entity<TimesheetPosted>(t =>
                       {
                           t.ToTable("TimesheetPosted", "pay");

                           t.HasOne(d => d.PayrollPosted)
                                .WithMany(p => p.TimesheetPosted)
                                .HasForeignKey(d => d.PayrollPostedId)
                                .HasConstraintName("FK_TimesheetPosted_PayrollPosted");

                           t.HasOne(d => d.TimesheetTypes)
                                .WithMany(p => p.TimesheetPosted)
                                .HasForeignKey(d => d.TimesheetTypeId)
                                .HasConstraintName("FK_TimesheetPosted_TimesheetTypes");
                       });
            modelBuilder.Entity<TaxTables>(t =>
                       {
                           t.ToTable("TaxTables", "pay");

                           t.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<StatutoryTmp>(s =>
                       {
                           s.ToTable("StatutoryTmp", "pay");

                           s.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.StatutoryTmp)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_StatutoryTmp_PayrollProcess");

                           s.HasOne(d => d.StatutorySubTypes)
                                .WithMany(p => p.StatutoryTmp)
                                .HasForeignKey(d => d.StatutorySubTypeId)
                                .HasConstraintName("FK_StatutoryTmp_StatutorySubTypes");
                       });
            modelBuilder.Entity<StatutoryProcess>(s =>
                       {
                           s.ToTable("StatutoryProcess", "pay");

                           s.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.StatutoryProcess)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_StatutoryProcess_PayrollProcessId");

                           s.HasOne(d => d.StatutorySubTypes)
                                .WithMany(p => p.StatutoryProcess)
                                .HasForeignKey(d => d.StatutorySubTypeId)
                                .HasConstraintName("FK_StatutoryProcess_StatutorySubTypes");
                       });
            modelBuilder.Entity<StatutoryPosted>(s =>
                       {
                           s.ToTable("StatutoryPosted", "pay");

                           s.HasOne(d => d.PayrollPosted)
                                .WithMany(p => p.StatutoryPosted)
                                .HasForeignKey(d => d.PayrollPostedId)
                                .HasConstraintName("FK_StatutoryPosted_PayrollPosted");

                           s.HasOne(d => d.StatutorySubTypes)
                                .WithMany(p => p.StatutoryPosted)
                                .HasForeignKey(d => d.StatutorySubTypeId)
                                .HasConstraintName("FK_StatutoryPosted_StatutorySubTypes");
                       });

            modelBuilder.Entity<StatutorySettings>(s =>
            {
                s.ToTable("StatutorySettings", "pay");

                s.Property(e => e.StatutoryType).IsUnicode(false);
                s.Property(e => e.Basis).IsUnicode(false);
                s.Property(e => e.Frequency).IsUnicode(false);
                s.Property(e => e.BasisDays).HasColumnType("decimal(12, 2)");

                s.HasOne(d => d.PayCodes)
                    .WithMany(p => p.StatutorySettings)
                    .HasForeignKey(d => d.PaycodeId)
                    .HasConstraintName("FK_StatutorySettings_PayCodes");

                s.HasIndex(e => new { e.TenantId, e.PaycodeId, e.StatutoryType })
                    .HasDatabaseName("IX_StatutorySettings_TenantId_PaycodeId_StatutoryType")
                    .IsUnique();

                s.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<TaxMethodByProcessType>(s =>
            {
                s.ToTable("TaxMethodByProcessType", "pay");

                s.Property(e => e.ProcessType).IsUnicode(false);
                s.Property(e => e.TaxMethod).IsUnicode(false);

                s.HasOne(d => d.PayCodes)
                    .WithMany(p => p.TaxMethodByProcessType)
                    .HasForeignKey(d => d.PaycodeId)
                    .HasConstraintName("FK_TaxMethodByProcessType_PayCodes");

                s.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<SssTables>(s =>
                       {
                           s.ToTable("SssTables", "pay");

                           s.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<Reviewers>(r =>
                       {
                           r.ToTable("Reviewers", "pay");

                           r.HasIndex(e => new { e.TenantId });

                           r.HasOne(d => d.Employees)
                                .WithMany(p => p.Reviewers)
                                .HasForeignKey(d => d.ReviewedBy)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Reviewers_Employees");
                       });
            modelBuilder.Entity<RecurEarnings>(r =>
                       {
                           r.ToTable("RecurEarnings", "pay");

                           r.HasIndex(e => new { e.TenantId });

                           r.HasOne(d => d.EarningTypes)
                                .WithMany(p => p.RecurEarnings)
                                .HasForeignKey(d => d.EarningTypeId)
                                .HasConstraintName("FK_RecurEarnings_EarningTypes");

                           r.Property(e => e.StartDate).HasColumnType("date");
                           r.Property(e => e.EndDate).HasColumnType("date");
                       });
            modelBuilder.Entity<RecurDeductions>(r =>
                       {
                           r.ToTable("RecurDeductions", "pay");

                           r.HasIndex(e => new { e.TenantId });

                           r.HasOne(d => d.DeductionTypes)
                                .WithMany(p => p.RecurDeductions)
                                .HasForeignKey(d => d.DeductionTypeId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_RecurDeductions_DeductionTypes");
                       });
            modelBuilder.Entity<ProratedEarnings>(x =>
            {
                x.ToTable("ProratedEarnings", "pay");

                x.HasIndex(e => new { e.TenantId });

                x.HasOne(d => d.EarningTypes)
                    .WithMany(p => p.ProratedEarningsNavigation)
                    .HasForeignKey(d => d.EarningTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.TimesheetTypes)
                    .WithMany(p => p.ProratedEarnings)
                    .HasForeignKey(d => d.InTimesheetTypeId)
                    .HasConstraintName("FK_ProratedEarnings_TimesheetTypes");

                x.HasOne(d => d.OvertimeTypes)
                    .WithMany(p => p.ProratedEarnings)
                    .HasForeignKey(d => d.InOvertimeTypeId)
                    .HasConstraintName("FK_ProratedEarnings_OvertimeTypes");

                x.HasOne(d => d.OutEarningTypes)
                    .WithMany(p => p.OutProratedEarningsNavigation)
                    .HasForeignKey(d => d.OutEarningTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasIndex(e => new { e.TenantId, e.EarningTypeId }).IsUnique();
            });
            modelBuilder.Entity<ProcSessions>(x =>
            {
                x.ToTable("ProcSessions", "pay");

                x.Property(e => e.ProcessStart).HasColumnType("smalldatetime");
                x.Property(e => e.ProcessEnd).HasColumnType("smalldatetime");
            });
            modelBuilder.Entity<ProcSessionBatches>(x =>
            {
                x.ToTable("ProcSessionBatches", "pay");

                x.HasIndex(e => new { e.PsId, e.EmployeeId }).IsUnique();

                x.HasOne(d => d.ProcSessions)
                    .WithMany(p => p.ProcSessionBatches)
                    .HasForeignKey(d => d.PsId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.Employees)
                    .WithMany(p => p.ProcSessionBatches)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });
            modelBuilder.Entity<ProcessHistory>(p =>
                       {
                           p.ToTable("ProcessHistory", "pay");

                           p.HasOne(d => d.Process)
                                .WithMany(p => p.ProcessHistory)
                                .HasForeignKey(d => d.ProcessId)
                                .HasConstraintName("FK_ProcessHistory_Process");

                           p.HasOne(d => d.ProcessStatus)
                                .WithMany(p => p.ProcessHistory)
                                .HasForeignKey(d => d.ProcessStatusId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_ProcessHistory_ProcessStatus");
                       });
            modelBuilder.Entity<Process>(p =>
                       {
                           p.ToTable("Process", "pay");

                           p.HasIndex(e => new { e.TenantId });

                           p.HasOne(d => d.ProcessStatus)
                                .WithMany(p => p.Process)
                                .HasForeignKey(d => d.ProcessStatusId)
                                .HasConstraintName("FK_Process_ProcessStatus");

                           p.HasOne(d => d.Owner)
                                .WithMany(p => p.Process)
                                .HasForeignKey(d => d.OwnerId)
                                .HasConstraintName("FK_Process_Owner");

                           p.HasOne(d => d.PayPeriod)
                               .WithMany(p => p.Process)
                               .HasForeignKey(d => d.PayrollNo)
                               .OnDelete(DeleteBehavior.ClientSetNull)
                               .HasConstraintName("FK_Process_PayPeriod");

                           p.HasIndex(e => new { e.TenantId, e.PayrollNo })
                                .HasDatabaseName("IX_Process_TenantId_PayrollNo")
                                .IsUnique();
                       });
            modelBuilder.Entity<PaySettings>(p =>
                       {
                           p.ToTable("PaySettings", "pay");

                           p.HasIndex(e => new { e.TenantId });

                           p.Property(e => e.Key).IsUnicode(false);
                           p.Property(e => e.Value).IsUnicode(false);
                           p.Property(e => e.DataType).IsUnicode(false);
                           p.Property(e => e.Caption).IsUnicode(false);

                           p.HasOne(d => d.GroupSettings)
                                .WithMany(p => p.PaySettings)
                                .HasForeignKey(d => d.GroupSettingId)
                                .HasConstraintName("FK_GroupSettings_HRSettings");
                       });
            modelBuilder.Entity<PayrollTmp>(p =>
                       {
                           p.ToTable("PayrollTmp", "pay");

                           p.HasIndex(e => new { e.TenantId });

                           p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.PayrollNo })
                                .HasDatabaseName("IX_PayrollTmp_TenantId_EmployeeId_PayrollNo")
                                .IsUnique();
                       });
            modelBuilder.Entity<PayrollProcess>(p =>
                       {
                           p.ToTable("PayrollProcess", "pay");

                           p.HasIndex(e => new { e.TenantId });

                           p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.PayrollNo })
                                .HasDatabaseName("IX_PayrollProcess_TenantId_EmployeeId_PayrollNo")
                                .IsUnique();

                           p.HasOne(d => d.Employees)
                                .WithMany(p => p.PayrollProcess)
                                .HasForeignKey(d => d.EmployeeId)
                                .HasConstraintName("FK_PayrollProcess_Employees");

                           p.HasOne(d => d.PayCodes)
                               .WithMany(p => p.PayrollProcess)
                               .HasForeignKey(d => d.PayCodeId)
                               .HasConstraintName("FK_PayrollProcess_PayCodes");
                       });
            modelBuilder.Entity<PayrollPosted>(p =>
                       {
                           p.ToTable("PayrollPosted", "pay");

                           p.HasIndex(e => new { e.TenantId });

                           p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.PayrollNo })
                                .HasDatabaseName("IX_PayrollPosted_TenantId_EmployeeId_PayrollNo")
                                .IsUnique();

                           p.HasOne(d => d.Employees)
                                .WithMany(p => p.PayrollPosted)
                                .HasForeignKey(d => d.EmployeeId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_PayrollPosted_Employees");

                           p.HasOne(d => d.PayCodes)
                               .WithMany(p => p.PayrollPosted)
                               .HasForeignKey(d => d.PayCodeId)
                               .HasConstraintName("FK_PayrollPosted_PayCodes");

                           p.HasOne(d => d.PayPeriod)
                               .WithMany(p => p.PayrollPosted)
                               .HasForeignKey(d => d.PayrollNo)
                               .OnDelete(DeleteBehavior.ClientSetNull)
                               .HasConstraintName("FK_PayrollPosted_PayPeriod");
                       });
            modelBuilder.Entity<PayPeriod>(p =>
                       {
                           p.ToTable("PayPeriod", "pay");

                           p.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<OvertimeMainTypes>(o =>
                       {
                           o.ToTable("OvertimeMainTypes", "pay");

                           o.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<OvertimeSubTypes>(o =>
            {
                o.ToTable("OvertimeSubTypes", "pay");

                o.HasIndex(e => new { e.TenantId });
            });
            modelBuilder.Entity<OTSheetTmp>(o =>
                       {
                           o.ToTable("OTSheetTmp", "pay");

                           o.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.OTSheetTmp)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_OTSheetTmp_PayrollProcess");

                           o.HasOne(d => d.OvertimeTypes)
                                .WithMany(p => p.OTSheetTmp)
                                .HasForeignKey(d => d.OvertimeTypeId)
                                .HasConstraintName("FK_OTSheetTmp_OvertimeTypes");
                       });
            modelBuilder.Entity<OTSheetProcess>(o =>
                       {
                           o.ToTable("OTSheetProcess", "pay");

                           o.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.OTSheetProcess)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_OTSheetProcess_PayrollProcess");

                           o.HasOne(d => d.OvertimeTypes)
                                .WithMany(p => p.OTSheetProcess)
                                .HasForeignKey(d => d.OvertimeTypeId)
                                .HasConstraintName("FK_OTSheetProcess_OvertimeTypes");
                       });
            modelBuilder.Entity<OTSheetPosted>(o =>
                       {
                           o.ToTable("OTSheetPosted", "pay");

                           o.HasOne(d => d.PayrollPosted)
                                .WithMany(p => p.OTSheetPosted)
                                .HasForeignKey(d => d.PayrollPostedId)
                                .HasConstraintName("FK_OTSheetPosted_PayrollPosted");

                           o.HasOne(d => d.OvertimeTypes)
                                .WithMany(p => p.OTSheetPosted)
                                .HasForeignKey(d => d.OvertimeTypeId)
                                .HasConstraintName("FK_OTSheetPosted_OvertimeTypes");
                       });
            modelBuilder.Entity<OTSheet>(o =>
                       {
                           o.ToTable("OTSheet", "pay");

                           o.HasOne(d => d.Process)
                                .WithMany(p => p.OTSheet)
                                .HasForeignKey(d => d.ProcessId)
                                .HasConstraintName("FK_OTSheet_Process");

                           o.HasOne(d => d.OvertimeTypes)
                                .WithMany(p => p.OTSheet)
                                .HasForeignKey(d => d.OvertimeTypeId)
                                .HasConstraintName("FK_OTSheet_OvertimeTypes");
                       });
            modelBuilder.Entity<LoanPayments>(l =>
            {
                l.ToTable("LoanPayments", "pay");

                l.HasOne(d => d.EmployeeLoans)
                    .WithMany(p => p.LoanPayments)
                    .HasForeignKey(d => d.EmployeeLoanId)
                    .HasConstraintName("FK_LoanPayments_EmployeeLoans");

                l.HasOne(d => d.PayPeriod)
                    .WithMany(p => p.LoanPayments)
                    .HasForeignKey(d => d.PayrollNo)
                    .HasConstraintName("FK_LoanPayments_PayPeriod");

                l.HasIndex(e => e.EmployeeLoanId).IsUnique();
            });
            modelBuilder.Entity<LoanTmp>(l =>
                       {
                           l.ToTable("LoanTmp", "pay");

                           l.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<LoanProcess>(l =>
                       {
                           l.ToTable("LoanProcess", "pay");

                           l.HasIndex(e => new { e.TenantId });

                           l.HasOne(d => d.EmployeeLoans)
                                .WithMany(p => p.LoanProcess)
                                .HasForeignKey(d => d.EmployeeLoanId)
                                .HasConstraintName("FK_LoanProcess_EmployeeLoans");

                           l.HasOne(d => d.PayrollProcess)
                               .WithMany(p => p.LoanProcess)
                               .HasPrincipalKey(d => new { d.TenantId, d.EmployeeId, d.PayrollNo })
                               .HasForeignKey(d => new { d.TenantId, d.EmployeeId, d.PayrollNo })
                               .OnDelete(DeleteBehavior.ClientSetNull)
                               .HasConstraintName("FK_LoanProcess_PayrollProcess");
                       });
            modelBuilder.Entity<LeaveCredits>(l =>
                       {
                           l.ToTable("LeaveCredits", "pay");

                           l.HasIndex(e => new { e.TenantId });

                           l.HasOne(d => d.LeaveTypes)
                                .WithMany(p => p.LeaveCredits)
                                .HasForeignKey(d => d.LeaveTypeId)
                                .HasConstraintName("FK_LeaveCredits_LeaveTypes");
                       });
            modelBuilder.Entity<Employers>(x =>
                       {
                           x.ToTable("Employers", "pay");

                           x.HasIndex(e => new { e.TenantId });

                           x.HasOne(d => d.Employees)
                                .WithMany(p => p.Employers)
                                .HasForeignKey(d => d.EmployeeId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_Employers_Employees");
                       });
            modelBuilder.Entity<EarningTypeSettings>(x =>
                       {
                           x.ToTable("EarningTypeSettings", "pay");

                           x.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<EarningTmp>(x =>
                       {
                           x.ToTable("EarningTmp", "pay");

                           x.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.EarningTmp)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_EarningTmp_PayrollProcess");
                       });
            modelBuilder.Entity<Earnings>(x =>
                       {
                           x.ToTable("Earnings", "pay");

                           x.HasOne(d => d.Process)
                                .WithMany(p => p.Earnings)
                                .HasForeignKey(d => d.ProcessId)
                                .HasConstraintName("FK_Earnings_Process");

                           x.HasOne(d => d.EarningTypes)
                                .WithMany(p => p.Earnings)
                                .HasForeignKey(d => d.EarningTypeId)
                                .HasConstraintName("FK_Earnings_EarningTypes");

                           x.Property(e => e.TranDate).HasColumnType("smalldatetime");
                       });
            modelBuilder.Entity<EarningProcess>(x =>
                       {
                           x.ToTable("EarningProcess", "pay");

                           x.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.EarningProcess)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_EarningProcess_PayrollProcess");

                           x.HasOne(d => d.EarningTypes)
                                .WithMany(p => p.EarningProcess)
                                .HasForeignKey(d => d.EarningTypeId)
                                .HasConstraintName("FK_EarningProcess_EarningTypes");
                       });
            modelBuilder.Entity<EarningPosted>(e =>
                       {
                           e.ToTable("EarningPosted", "pay");

                           e.HasOne(d => d.PayrollPosted)
                                .WithMany(p => p.EarningPosted)
                                .HasForeignKey(d => d.PayrollPostedId)
                                .HasConstraintName("FK_EarningPosted_PayrollPosted");

                           e.HasOne(d => d.EarningTypes)
                                .WithMany(p => p.EarningPosted)
                                .HasForeignKey(d => d.EarningTypeId)
                                .HasConstraintName("FK_EarningPosted_EarningTypes");
                       });
            modelBuilder.Entity<DeductionTypes>(d =>
                       {
                           d.ToTable("DeductionTypes", "pay");

                           d.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<DeductionTmp>(d =>
                       {
                           d.ToTable("DeductionTmp", "pay");

                           d.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.DeductionTmp)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_DeductionTmp_PayrollProcess");
                       });
            modelBuilder.Entity<Deductions>(d =>
                       {
                           d.ToTable("Deductions", "pay");

                           d.HasOne(d => d.Process)
                                .WithMany(p => p.Deductions)
                                .HasForeignKey(d => d.ProcessId)
                                .HasConstraintName("FK_Deductions_Process");

                           d.HasOne(d => d.DeductionTypes)
                                .WithMany(p => p.Deductions)
                                .HasForeignKey(d => d.DeductionTypeId)
                                .HasConstraintName("FK_Deductions_DeductionTypes");
                       });
            modelBuilder.Entity<DeductionProcess>(d =>
                       {
                           d.ToTable("DeductionProcess", "pay");

                           d.HasOne(d => d.PayrollProcess)
                                .WithMany(p => p.DeductionProcess)
                                .HasForeignKey(d => d.PayrollProcessId)
                                .HasConstraintName("FK_DeductionProcess_PayrollProcess");

                           d.HasOne(d => d.DeductionTypes)
                                .WithMany(p => p.DeductionProcess)
                                .HasForeignKey(d => d.DeductionTypeId)
                                .HasConstraintName("FK_DeductionProcess_DeductionTypes");
                       });
            modelBuilder.Entity<DeductionPosted>(d =>
                       {
                           d.ToTable("DeductionPosted", "pay");

                           d.HasOne(d => d.PayrollPosted)
                                .WithMany(p => p.DeductionPosted)
                                .HasForeignKey(d => d.PayrollPostedId)
                                .HasConstraintName("FK_DeductionPosted_PayrollPosted");

                           d.HasOne(d => d.DeductionTypes)
                                .WithMany(p => p.DeductionPosted)
                                .HasForeignKey(d => d.DeductionTypeId)
                                .HasConstraintName("FK_DeductionPosted_DeductionTypes");
                       });

            modelBuilder.Entity<Dependents>(d =>
            {
                d.ToTable("Dependents", "pay");

                d.HasIndex(e => new { e.TenantId });

                d.HasOne(d => d.Employees)
                    .WithMany(p => p.Dependents)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_Dependents_Employees");
            });

            modelBuilder.Entity<TenantBanks>(c =>
                       {
                           c.ToTable("TenantBanks", "pay");

                           c.HasIndex(e => new { e.TenantId });

                           c.HasOne(d => d.BankBranches)
                                .WithMany(p => p.TenantBanks)
                                .HasForeignKey(d => d.BankBranchId)
                                .HasConstraintName("FK_TenantBanks_BankBranches");
                       });
            modelBuilder.Entity<TenantEmploymentTypes>(c =>
            {
                c.ToTable("TenantEmploymentTypes", "pay");

                c.HasIndex(e => new { e.TenantId });

                c.Property(e => e.EmploymentTypeId).IsUnicode(false);

                c.HasOne(d => d.EmploymentTypes)
                     .WithMany(p => p.TenantEmploymentTypes)
                     .HasForeignKey(d => d.EmploymentTypeId)
                     .HasConstraintName("FK_TenantEmploymentTypes_EmploymentTypes");
            });
            modelBuilder.Entity<BankBranches>(b =>
                       {
                           b.ToTable("BankBranches", "pay");

                           b.Property(e => e.BankId).IsUnicode(false);

                           b.HasOne(d => d.Banks)
                                .WithMany(p => p.BankBranches)
                                .HasForeignKey(d => d.BankId)
                                .HasConstraintName("FK_BankBranches_Banks");

                       });
            modelBuilder.Entity<HRSettings>(h =>
                       {
                           h.ToTable("HRSettings", "hr");

                           h.HasIndex(e => new { e.TenantId });

                           h.HasOne(d => d.GroupSettings)
                                .WithMany(p => p.HRSettings)
                                .HasForeignKey(d => d.GroupSettingId)
                                .HasConstraintName("FK_GroupSettings_HRSettings");
                       });
            modelBuilder.Entity<PayCodes>(p =>
                       {
                           p.ToTable("PayCodes", "pay");

                           p.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<EmployeePayroll>(x =>
                       {
                           x.ToTable("EmployeePayroll", "pay");

                           x.HasIndex(e => new { e.TenantId });

                           x.HasOne(d => d.BankBranches)
                                .WithMany(p => p.EmployeePayroll)
                                .HasForeignKey(d => d.BankBranchId)
                                .HasConstraintName("FK_EmployeePayroll_BankBranches");

                           x.HasOne(d => d.Ranks)
                                .WithMany(p => p.EmployeePayroll)
                                .HasForeignKey(d => d.RankId)
                                .HasConstraintName("FK_EmployeePayroll_Ranks");

                           x.HasOne(d => d.Employees)
                                .WithMany(p => p.EmployeePayroll)
                                .HasForeignKey(d => d.EmployeeId)
                                .HasConstraintName("FK_EmployeePayroll_Employees");

                           x.HasOne(d => d.PayCodes)
                               .WithMany(p => p.EmployeePayroll)
                               .HasForeignKey(d => d.PayCodeId)
                               .HasConstraintName("FK_EmployeePayroll_PayCodes");
                       });
            modelBuilder.Entity<EarningTypes>(x =>
                       {
                           x.ToTable("EarningTypes", "pay");

                           x.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<EmployeeLoans>(x =>
                        {
                            x.ToTable("EmployeeLoans", "pay");

                            x.HasIndex(e => new { e.TenantId });

                            x.HasOne(d => d.Employees)
                                .WithMany(p => p.EmployeeLoans)
                                .HasForeignKey(d => d.EmployeeId)
                                .HasConstraintName("FK_EmployeeLoans_Employees");

                            x.HasOne(d => d.Loans)
                                .WithMany(p => p.EmployeeLoans)
                                .HasForeignKey(d => d.LoanId)
                                .HasConstraintName("FK_EmployeeLoans_Loans");
                        });
            modelBuilder.Entity<LoanPosted>(l =>
                       {
                           l.ToTable("LoanPosted", "pay");

                           l.HasIndex(e => new { e.TenantId });

                           l.HasOne(d => d.EmployeeLoans)
                                .WithMany(p => p.LoanPosted)
                                .HasForeignKey(d => d.EmployeeLoanId)
                                .HasConstraintName("FK_LoanPosted_EmployeeLoans");

                           l.HasOne(d => d.Employees)
                                .WithMany(p => p.LoanPosted)
                                .HasForeignKey(d => d.EmployeeId)
                                .OnDelete(DeleteBehavior.ClientSetNull)
                                .HasConstraintName("FK_LoanPosted_Employees");

                           l.HasOne(d => d.PayPeriod)
                               .WithMany(p => p.LoanPosted)
                               .HasForeignKey(d => d.PayrollNo)
                               .HasConstraintName("FK_LoanPosted_PayPeriod");

                           l.HasOne(d => d.PayrollPosted)
                               .WithMany(p => p.LoanPosted)
                               .HasPrincipalKey(d => new { d.TenantId, d.EmployeeId, d.PayrollNo })
                               .HasForeignKey(d => new { d.TenantId, d.EmployeeId, d.PayrollNo })
                               .HasConstraintName("FK_LoanPosted_PayrollPosted");
                       });
            modelBuilder.Entity<Loans>(l =>
                       {
                           l.ToTable("Loans", "pay");

                           l.HasIndex(e => new { e.TenantId });

                           l.HasOne(d => d.LoanTypes)
                                .WithMany(p => p.Loans)
                                .HasForeignKey(d => d.LoanTypeId)
                                .HasConstraintName("FK_Loans_LoanTypes");
                       });
            modelBuilder.Entity<Employees>(x =>
                       {
                           x.ToTable("Employees", "hr");

                           x.HasIndex(e => new { e.TenantId });

                           x.Property(e => e.DateOfBirth).HasColumnType("date");
                           x.Property(e => e.DateEmployed).HasColumnType("date");
                           x.Property(e => e.DateTerminated).HasColumnType("date");
                           x.Property(e => e.EmployeeCode).IsUnicode(false);
                           x.Property(e => e.LastName).IsUnicode(false);
                           x.Property(e => e.FirstName).IsUnicode(false);
                           x.Property(e => e.MiddleName).IsUnicode(false);
                           x.Property(e => e.Position).IsUnicode(false);
                           x.Property(e => e.Gender).IsUnicode(false);
                           x.Property(e => e.EmployeeStatus).IsUnicode(false);

                           x.Property(e => e.EmploymentTypeId).IsUnicode(false);

                           x.HasOne(d => d.Tenant)
                                .WithMany(p => p.Employees)
                                .HasForeignKey(d => d.TenantId)
                                .HasConstraintName("FK_Employees_Tenant");

                           x.HasOne(d => d.EmploymentTypes)
                                .WithMany(p => p.Employees)
                                .HasForeignKey(d => d.EmploymentTypeId)
                                .HasConstraintName("FK_Employees_EmploymentTypes");
                       });
            modelBuilder.Entity<LoanTypes>(l =>
                       {
                           l.ToTable("LoanTypes", "pay");

                           l.HasIndex(e => new { e.TenantId });

                           l.Property(e => e.SysCode).IsUnicode(false);
                       });
            modelBuilder.Entity<DeductionTypes>(d =>
                       {
                           d.ToTable("DeductionTypes", "pay");

                           d.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<LeaveTypes>(l =>
                       {
                           l.ToTable("LeaveTypes", "pay");

                           l.HasIndex(e => new { e.TenantId });
                       });
            modelBuilder.Entity<BinaryObject>(b =>
                        {
                            b.HasIndex(e => new { e.TenantId });
                        });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
            {
                b.HasQueryFilter(m => !m.IsDeleted)
                    .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
                    .IsUnique();
            });

            modelBuilder.Entity<UserDelegation>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.SourceUserId });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId });
            });

            modelBuilder.Entity<User>(b =>
            {
                b.HasOne(d => d.Employees)
                    .WithOne(p => p.User)
                    .HasForeignKey<User>(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Employees");
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}