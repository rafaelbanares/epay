using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace MMB.ePay.EntityFrameworkCore
{
    public static class ePayDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ePayDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ePayDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}