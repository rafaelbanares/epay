﻿using Microsoft.AspNetCore.Antiforgery;

namespace MMB.ePay.Web.Controllers
{
    public class AntiForgeryController : ePayControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
